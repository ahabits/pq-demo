﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoodleModels.Models
{

    public class GetVendorsByClientResponse
    {
        public List<VendorCode> vendorcodes { get; set; }
        public int status { get; set; }
        public string message { get; set; }
    }

    public class VendorCode
    {
        public string vendorcode { get; set; }
    }

    public class EmployeeCourse
    {
        public int userid { get; set; }
        public int courseid { get; set; }
    }

    public class LastMonthTrainingPaymentsRequest
    {
        public List<EmployeeCourse> EmployeeCourses { get; set; }
    }
    public  class LastMonthTrainingPayments
    {
        public List<EmployeeQuizDetails> EmployeeDetails { get; set; }
    }
    public class EmployeeQuizDetails
    {
        public string clientname { get; set; }
        public string vendorname { get; set; }
        public string employeename { get; set; }
        public string quizstart { get; set; }
        public string quizsubmitteddate { get; set; }
        public string previousquizstatus { get; set; }
        public string previousquizsubmitteddate { get; set; }

    }
}
