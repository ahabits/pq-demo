﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.LocalModels
{
  public  class LocalEmailtemplateConfig
    {
        public int ID { get; set; }   
        public long ClientId { get; set; }
        public string ClientName { get; set; }
        public int? EmailTemplateId { get; set; }
        public string TemplateName { get; set; }
        public int? InvitationType { get; set; }
        public string InvitationName { get; set; }
        
    }
}
