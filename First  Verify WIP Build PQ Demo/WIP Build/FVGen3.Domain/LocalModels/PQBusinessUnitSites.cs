﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.LocalModels
{
    public class PQBusinessUnitSites
    {
        public string BusinessUnitName { get; set; }
        public long? ClientBusinessUnitId { get; set; }

        public string BusinesUnitsiteName { get; set; }
        public long? ClientBusinessUnitSiteID { get; set; }

        public long? ClientId { get; set; }
        public string ClientName { get; set; }
        public string SiteStatus { get; set; }
        public string Comment { get; set; }
        public long PrequalificationSitesId { get; set; }
        public long? PQSiteStatus { get; set; }
        public long? PrequalificationId { get; set; } 
    }
}
