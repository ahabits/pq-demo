﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.LocalModels
{
  public  class PQBusinessUnitDocments
    {
        public List<BuDocs> Bus { get; set; }
        public List<BuSiteDocs> BuSites { get; set; }

    }

}
public class BuDocs
{
    public string BusinessUnitName { get; set; }
    public long BusinessUnitId { get; set; }
   
    public string ClientName { get; set; }
    public List<DocumentsList> documents { get; set; }
}
public class BuSiteDocs
{
    public string BusinessUnitName { get; set; }
    public long BusinessUnitId { get; set; }
    public string BusinessUnitsiteName { get; set; }
    public long BusinessUnitSiteId { get; set; }

    public string ClientName { get; set; }
    public List<DocumentsList> documents { get; set; }
}
public class DocumentsList
{
    public long ClientId { get; set; }
    public Guid DocumentId { get; set; }
    public string DocumentName { get; set; }
}