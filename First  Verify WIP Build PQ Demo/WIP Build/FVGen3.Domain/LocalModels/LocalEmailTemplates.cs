﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace FVGen3.Domain.LocalModels
{
  public class LocalEmailTemplates
    {
        public int EmailTemplateId { get; set; }
        public long LanguageId { get; set; }
        [AllowHtml]
        public string EmailSubject { get; set; }
        public string Emailbody { get; set; }
    }
}
