﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.LocalModels
{ 
   public  class CompanyAccessRoles
    {
        public string Name { get; set; }
        public Guid Userid { get; set;}
        public long SysUserOrganizationId { get; set; }
        public List<string> RoleName { get; set; }
    }
}
