﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.LocalModels
{
    public class QuestionAndAnswers
    {
        public string QuestionName { get; set; }
        public List<string> Answers { get; set; }
        public decimal? Value { get; set; }
        public bool? IsMax { get; set; }
    }
}
