﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.LocalModels
{
    public class DocumentInfo
    {
        public DateTime? ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        public string Status { get; set; }
        public string ExpirationDate { get; set; }
        public string DocumentType { get; set; }
        public string ModificationType { get; set; }
        public string DocumentName { get; set; }
    } 
}
