﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.LocalModels
{
 public class ClientBusinessUnitsSiteWithType
    {
        public string Text { get; set; }
        public string Value { get; set; }
        public int Type { get; set; }
    } 
}
