﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.LocalModels
{
  public class Dropdown
    {   public string Email { get; set; }
        public string Name { get; set; } 
        public long ID { get; set; }  
        public Guid? UserId { get; set; }
    }
}
