﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.LocalModels
{
  public  class StatusHistoryLog
    {
        public string SiteName { get; set; }
        public long PQSiteid { get; set; }
        public DateTime StatusChangeDate { get; set; }
        public string PrequalificationStatusName { get; set; }

        public string ChangedBy { get; set; }
        public DateTime? CreatedDate { get; set; }

    }
}
