﻿
namespace FVGen3.Domain.LocalModels
{
    public class ProficiancyWithIsEdit
    {
        public long ProficiencyId { get; set; }
        public string ProficiencyCategory { get; set; }
        public string ProficiencySubCategroy { get; set; }
        public string ProficiencyName { get; set; }
        public int? Status { get; set; }
        public bool IsEdit { get; set; }
    } 
}