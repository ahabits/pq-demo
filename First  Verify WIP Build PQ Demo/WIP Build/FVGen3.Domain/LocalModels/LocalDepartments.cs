﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.LocalModels
{
   public class LocalDepartments
    { public int Id { get; set; }
        public string DepartmentName { get; set; } 
        public long ClientId { get; set; }
        public string ClientName { get; set; }
    }
}
