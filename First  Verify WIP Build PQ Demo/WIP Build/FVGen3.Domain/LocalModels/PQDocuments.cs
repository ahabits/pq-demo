﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.LocalModels
{
    public class PQDocuments
    {
        public string DocumentName { get; set; }
        public Guid DocumentId{get;set;}
        public string UploadedBy { get; set; }
        public DateTime UploadedDate { get; set; } 
        
    }
}
