﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.LocalModels
{
   public  class FaGraphDetails
    {
        public List<Series> Series { get; set; }
        public List<string> CategoryAxis { get; set; }
    }
    public class Series
    {
        public string name { get; set; }
        public List<decimal> data { get; set; }
    }
}
