﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.LocalModels
{
    public class Statistics
    {
        public string EMR
        {
            get;
            set;
        }
        public string RIR
        {
            get;
            set;
        }
        public string DART
        {
            get;
            set;
        }
    }
}
