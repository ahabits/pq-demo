﻿using FVGen3.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.LocalModels
{
    public class LocalEmployeeQuizzes
    {
        public bool Employee { get; set; }
        public bool vendorIsExemptFromTrainingFee { get; set; } 
        public List<EmployeeQuizzesWithCategory> ClientQuizzes { get; set; }  
        public List<LocalEmployeeGuideDocumentsGrouped> LocalEmployeeGuideDocumentsGroupedByClient { get; set; }

    }
    public class EmployeeQuizzesWithCategory
    {
        //public List<IGrouping<string,SystemUsersOrganizationsQuizzes>> SystemUsersOrganizationsQuizzes { get;set;}
        public long? ClientID { get; set; }
        public string ClientName { get; set; }
        public bool? QuizResult { get; set; } 
        public DateTime? QuizDateSubmitted { get; set; }
        //public List<Templates> Tempaltes { get; set;}
        public string TemplateName { get; set;}
        public string QuizExpired { get; set; }
        public DateTime? QuizExpirationDate { get; set; }
        public long SysUserOrgQuizId { get; set; }
        public bool? waiverFormConfigured { get; set; }
        public Guid? ClientTemplateId { get; set; }
        public long quizWaiverFormCompletionLog { get; set;}
        public bool HasTrainingCertificate { get; set;}
        public decimal templateTotalPrice { get; set; }
        public string QuizStatus { get; set; }
        public bool HasQuizPayments { get; set; }
        public DateTime? QuizDateStart { get; set; }
        public string PreviousLastQuizTakenStatus { get; set; }
       // public DateTime? PreviousLastQuizSubmittedDate { get; set; }


      public List<QuizzesCategories> QuizzesCategories { get; set; }
    }
    public class LocalEmployeeGuideDocumentsGrouped
    {
        public LocalEmployeeGuideDocumentsGrouped()
        {
            FileNames = new List<string>();
        }
        public string key { get; set; }
        public List<string> FileNames { get; set; }
        public long? OrganizationId { get; set; }
        public decimal templateAnnualPrice { get; set; }
        public bool? qualified { get; set; } // kiran on 02/03/2015
        public Prequalification prequalifcation { get; set; }
        public long PqTrainingAnnualFeeCount { get; set; }
    }
    public class QuizzesCategories
    {
        public string CategoryName { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string SubCategoryInputValue { get; set; }
        public string SubCategoryEnteredDate { get; set; }
    }
}
