﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.LocalModels
{
   public class ClientUsersInfo
    {
        public string UsersEmails { get; set; } 
        public string ClientName { get; set; }
        public long ClientID { get; set; }
        public string UserNames { get; set; }
        public string UserIds { get; set; }
        public long Id { get; set; }

    }
}
