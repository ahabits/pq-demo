﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.LocalModels
{
    public class LocalCopyQuestions
    {

        public long Id { get; set; } 
        public Guid SourceTemplateId { get; set; }
        public Guid DestinationTemplateId { get; set; }
        public long SourceSectionId { get; set; }
        public long DestionationSectionId { get; set; }
        public long SourceSubSectionId { get; set; }
        public long DestionationSubSectionId { get; set; }

        public String DestinationQuestionText { get; set; }
        public List<QuestionsList> QuestionsList { get; set; }
        public List<SDQuestion> SDQuestion { get; set; }
    }
    public class SDQuestion
    {
        public long SourceQuestionId { get; set; }
        public long DestinationQuestionId { get; set; }
        public  bool Ismapped { get; set; }
    }
    public class QuestionsList
    {
        public long SourceQuestionId { get; set; }
      //  public long? SourceQuestionBankId { get; set; }
        public long SourceNoOfColumns { get; set; }
        public long SourceBankId { get; set; }
        public int SourceDisplayorder { get; set; }
        public String SourceQuestionText { get; set; }
        public long SourceQuestionControltype { get; set;}
        public long DestinationQuestionId { get; set; }
        public long DestinationQuestionBankId { get; set; }
        public long DestinationNoOfColumns { get; set; }
        public String DestinationQuestionText { get; set; }
        public long DestinationBankId { get; set; } 
        public int DestinationDisplayorder { get; set; }

    }
}
