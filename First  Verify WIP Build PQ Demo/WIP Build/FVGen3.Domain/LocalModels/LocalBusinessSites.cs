﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.LocalModels
{
   public class LocalBusinessSites
    {
        public string BusinesUnitsiteName { get; set; }
        public long BusinessUnitSiteID { get; set; }
        public long BusinessUnitID { get; set; }
        public string BusinessUnitName { get; set; }
    }
}
