﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.LocalModels
{
  public  class FeesPaid
    {
        public FeesPaid()
        {
            IsQuiz = true;
        }
        public string ClientName { get; set; }
        public string VendorName { get; set; }
        public string Transactionid { get; set; }
        public decimal? Fee { get; set; }
        public string Templatename { get; set; }
        public DateTime? PaymentReceivedDate { get; set; }
        public bool IsQuiz { get; set; }
        public Guid Templateid { get; set; } 
    }
}
