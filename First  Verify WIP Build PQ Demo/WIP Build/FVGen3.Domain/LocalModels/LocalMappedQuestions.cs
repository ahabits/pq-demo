﻿using FVGen3.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.LocalModels
{
    public class LocalMappedQuestions
    {
        public List<QuestionsList> SourceQuestions { get; set; }
        public List<QuestionsList> DestinationQuestions { get; set; }
        public List<SDQuestion> SDQuestion { get; set; } 
    }
}
