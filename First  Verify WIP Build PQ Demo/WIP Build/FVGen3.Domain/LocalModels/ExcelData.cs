﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.LocalModels
{ 
    public class ExcelData
    {
        public string QuesionText
        {
            get;
            set;
        }
        public long SourceQuesionid
        {
            get;
            set;
        }
        public long SourceQuesioncolumnid
        {
            get;
            set;
        }
        public long DestinationQuestionId
        {
            get;
            set;
        }
        public long DestinationQuesioncolumnid
        {
            get;
            set;
        }
        public long DestinationSectionId
        {
            get;
            set;
        }
        public long SectionID
        {
            get;
            set;
        }
    }
}
