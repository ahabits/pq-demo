﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.LocalModels
{
   public class SiteRepresents
    {public long ClientId { get; set; }
        public long ClientBUSiteID { get; set; }
        public long ClientBusinessUnitId { get; set; }
        public string SiteName { get; set; }
        public string BusinessUnitname { get; set; }
    }
}
 