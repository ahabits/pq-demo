﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.LocalModels
{
    public class InjuryOrSafetyStatisticsModel
    { 
        public string QuestionText { get; set; }
        public long QuestionColumnId { get; set; }
        public int DisplayOrder { get; set; }
        public int ColumnNo { get; set; }
    }
}
