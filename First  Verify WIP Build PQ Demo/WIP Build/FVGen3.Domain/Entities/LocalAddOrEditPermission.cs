﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.Entities
{
    public class LocalAddOrEditPermission
    {
        public string PermissionName { get; set; }
        public string RoleId { get; set; }
        public string RoleName { get; set; }
        public string OrganizationType { get; set; }

        public List<Permissions> permissions { get; set; }
        public List<Permissions> otherPermissions { get; set; }
        public bool checksNotAvailable { get; set; }  // Siva on 08/07/2013
        public bool ReadSelectAll { get; set; }
        public bool InsertSelectAll { get; set; }
        public bool ModifySelectAll { get; set; }
        public bool DeleteSelectAll { get; set; }
        public bool isReadOnly { get; set; }
    }

    public class Permissions
    {
        public Permissions()
        {
            readPermission = false;
            insertPermission = false;
            modifyPermission = false;
            deletePermission = false;
        }

        public long ViewId { get; set; }
        public string ViewName { get; set; }
        public bool readPermission { get; set; }
        public bool insertPermission { get; set; }
        public bool modifyPermission { get; set; }
        public bool deletePermission { get; set; }
    }
}
