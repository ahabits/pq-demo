﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.Entities
{
    public class LocalSortingForNameStatusAndStatusPeriod
    {
        public string VendorName { get; set; }
        public string Status { get; set; }
        public string UrlToRedirect {get; set; }
        public long PrequalificationStatusId { get; set; }
        public long VendorId { get; set; }
        public long ClientId { get; set; }
        public long PrequalificationId { get; set; }
        public DateTime PrequalificationStart { get; set; }
        public DateTime PrequalificationFinish { get; set; }
        public bool? ShowInDashBoard { get; set; } // Kiran on 6/7/2014
        public string TemplateCode { get; set; }
        public bool HasPermission { get; set; }
    }
}
