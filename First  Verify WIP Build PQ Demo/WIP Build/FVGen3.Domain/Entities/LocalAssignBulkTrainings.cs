﻿/*
Page Created Date:  03/24/2015
Created By: Kiran talluri
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.Entities
{
    public class LocalAssignBulkTrainings
    {
        public long VendorId { get; set; }
        public List<CheckBoxesControlForEmployeeQuizzes> trainings { get; set; }
        public List<EmployeeSystemUsersOrganizations> employeeSysUserOrgs { get; set; }
    }
    public class EmployeeSystemUsersOrganizations
    {
        public Guid UserId { get; set; }
        public string EmployeeName { get; set; }
        public long SysUserOrgId { get; set; }
        public bool empStatus { get; set; }
        public bool empUserStatus { get; set; }
    }
}
