﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.Entities
{
    public class LocalAssignRoles
    {
        public String UserName { get; set; }
        public long SysUserOrgId { get; set; }        
        public List<String> Roles { get; set; }
        public List<String> RoleNames { get; set; }
        public List<bool> IsRoleSelected { get; set; }
    }

   
}
