﻿/*
Page Created Date:  06/11/2013
Created By: Siva Bommisetty
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FVGen3.Domain.Entities
{
    public class PrequalificationUserInput
    {
        [Key]
        public long PreQualRecId { get; set; }

        [ForeignKey("PreQualificationId")]
        public virtual Prequalification Prequalification { get; set; }
        public long PreQualificationId { get; set; }

        [ForeignKey("QuestionColumnDetails")]
        public long QuestionColumnId { get; set; }
        public virtual QuestionColumnDetails QuestionColumnDetails { get; set; }

        public string UserInput { get; set; }
    }
}
