﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.Entities
{
    public class SystemUsersBUSites
    {
        [Key]
        public long SystemUserBUSiteId { get; set; }
        [ForeignKey("SystemUsers")]
        public Guid SystemUserId { get; set; }
        public virtual SystemUsers SystemUsers { get; set; }
        //[ForeignKey("ClientBusinessUnits")]
        public string BUId { get; set; }
        //public virtual ClientBusinessUnits ClientBusinessUnits { get; set; }
        //[ForeignKey("ClientBusinessUnitSites")]
        public string BUSiteId { get; set; }

        public IEnumerable<long> BUSiteIDsList { get { return string.IsNullOrEmpty(BUSiteId) ? new List<long>() : BUSiteId.Split(',').Select(long.Parse); } }
        //public virtual ClientBusinessUnitSites ClientBusinessUnitSites { get; set; }
    }
}
