﻿/*
Page Created Date:  10/19/2013
Created By: Rajesh Pendela
Purpose:For Change password 
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace FVGen3.Domain.Entities
{
    public class LocalQuestionsModel
    {
        public LocalQuestionsModel()
        {
            HasDependantQuestions = false;
        }
        public long QuestionID { get; set; }

        
        public string QuestionText { get; set; }
        public int DisplayOrder { get; set; }
        public bool Visible { get; set; }
        [Required]
        public int NumberOfColumns { get; set; }
        public bool IsMandatory { get; set; }
        public bool IsBold { get; set; }
        public bool ResponseRequired { get; set; }
        public bool DisplayResponse { get; set; }
        public bool HasDependantQuestions { get; set; }        
        public long SubSectionId { get; set; }
        [Required]
        public string QuestionType { get; set; }
    }
}
