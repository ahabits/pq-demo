﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace FVGen3.Domain.Entities
{
    public class QuestionsBank
    {
        [Key]
        public long QuestionBankId { get; set; }
        public int? QuestionReference { get; set; }
        public long? QuestionReferenceId { get; set; }
        public string QuestionString { get; set; }
        public int? QuestionStatus { get; set; }
        
        public int UsedFor { get; set; } // Kiran on 11/29/2013

        public virtual List<Questions> Questions { get; set; }
    }
}
