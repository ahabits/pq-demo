﻿/*
Page Created Date:  05/01/2013
Created By: Siva Bommisetty
Purpose:
Version: 1.0
****************************************************
*/

using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;

namespace FVGen3.Domain.Entities
{
    public class PrequalificationEMRStatsYears
    {
        
        
        [Key]
        public long PrequalEMRStatsYearId { get; set; }        
        public string EMRStatsYear { get; set; }

        [ForeignKey("Prequalification")]
        public long PrequalificationId { get; set; }
        public virtual Prequalification Prequalification { get; set; }

        public int ColumnNo { get; set; }

        public virtual List<PrequalificationEMRStatsValues> PrequalificationEMRStatsValues { get; set; }
    }
}