﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.Entities
{
     public class LocalTemplatePricing
    {
         public string TemplateName { get; set; }
         public string TemplatePrice { get; set; }
        // public long TemplatePriceId { get; set; }
    }
}
