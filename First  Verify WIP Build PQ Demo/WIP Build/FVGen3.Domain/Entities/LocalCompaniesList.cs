﻿/*
Page Created Date:  06/11/2013
Created By: Siva Bommisetty
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace FVGen3.Domain.Entities
{
    public class LocalCompaniesList
    {
        public long OrganizationId { get; set; }
        public string Name { get; set; }
        public Guid userId { get; set; }
        public bool isEdit { get; set; }
        public long oldOrgId { get; set; }        
        public List<bool> Roles_CheckBoxStatus { get; set; }
        public List<SystemRoles> RolesList { get; set; }
    }
}
