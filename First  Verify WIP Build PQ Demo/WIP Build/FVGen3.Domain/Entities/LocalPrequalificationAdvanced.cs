﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.Entities
{
    public class LocalPrequalificationAdvanced
    {
        public string Name { get; set; }
        public string PrequalificationStatusName { get; set; }
        public DateTime PrequalificationStart { get; set; }
        public DateTime PrequalificationFinish { get; set; }
        public long VendorId { get; set; }
        public long ClientId { get; set; }
        public long PrequalificationId { get; set; }

        //Suma on 2/12/2013===>>>
        public Guid TemplateId { get; set; }
        public long PrequalificationStatusId { get; set; }
        //public virtual ClientTemplates ClientTemplates { get; set; }  // Kiran - 11/19/2013
        //=====Ends<<<
        public string TemplateCode { get; set; }

    }
}
