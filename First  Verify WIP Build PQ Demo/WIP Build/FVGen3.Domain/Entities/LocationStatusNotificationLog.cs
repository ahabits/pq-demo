﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.Entities
{
    public class LocationStatusLog
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long pqSiteStatusLogId { get; set; }
        [ForeignKey("PrequalificationSites")]
        public long pqSiteId { get; set; }
        [ForeignKey("PrequalificationStatus")]
        public long pqSiteStatusId { get; set; }
        public string EmailSentTo { get; set; }
        public string EmailSentCC { get; set; }
        [ForeignKey("SystemUsers")]
        public Guid StatusChangedBy { get; set; }
        public DateTime StatusChangeDate { get; set; }

        public virtual PrequalificationSites PrequalificationSites { get; set; }
        public virtual PrequalificationStatus PrequalificationStatus { get; set; }
        public virtual SystemUsers SystemUsers { get; set; }
    }
}
