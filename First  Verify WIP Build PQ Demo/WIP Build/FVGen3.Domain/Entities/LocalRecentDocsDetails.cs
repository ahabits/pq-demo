﻿/*
Page Created Date:  08/07/2013
Created By: Siva Bommisetty
Purpose:
Version: 1.0
****************************************************
*/


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.Entities
{
    public class LocalRecentDocsDetails
    {
        public string VendorName { get; set; }
        public string DocumentTypeName { get; set; }
        public string ClientName { get; set; }
        public DateTime DateUploaded { get; set; }
        public string UserName { get; set; }
        public string urlRecentDocs { get; set; }
        public string Initials { get; set; }
        public string DocumentId { get; set; }//sumanth on 10/05/2015 for SMI-226
        public string Email { get; set; }//sumanth on 10/05/2015 for SMI-226
        //public bool HasPrevious { get; set; }
        //public bool HasNext { get; set; }
        //public int PageNumber { get; set; }
        public string VendorStatus { get; set; }// Mani on 7/22/2015 for FV-170
        public long TemplatesectionId { get; set; }
        public long PrequalificationId { get; set; }
        public Guid TemplateID { get; set; }
        public string ExpirationDate { get; set; }
        public bool HasDelete { get; set; }
    }
}
