﻿
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FVGen3.Domain.Entities
{
    public class VendorReviewQuestionsForSection3
    {
        [Key]
        public long Id { get; set; }
        public string SectionHeader { get; set; }
        public bool HasQuestion { get; set; }
        public string QuestionText { get; set; }

        [ForeignKey("QuestionControlType")]
        public long QuestionTypeID { get; set; }
        public virtual QuestionControlType QuestionControlType { get; set; }

        public string Options { get; set; }

        [ForeignKey("ClientId")]
        public virtual Organizations Client { get; set; }
        public long ClientId { get; set; }
        
        public int Order { get; set; }
        public int? Group { get; set; }
        public bool? IsRating { get; set; }
    }
}
