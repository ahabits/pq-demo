﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FVGen3.Domain.Entities
{
    public class OnsiteOffsite
    {

        [Key]
        public Guid OnsiteOffsiteID { get; set; }

        //[ForeignKey("UserId")]
        //public virtual SystemUsers SystemUsers { get; set; }
        public Guid VendorEmployeeID { get; set; }
        public DateTime? Checkin { get; set; }
        public Guid? CheckinClientUser { get; set; }
        public DateTime? Checkout { get; set; }
        public Guid? CheckoutClientUser { get; set; }
        public Int32? VendorID { get; set; }
        public Int32? ClientID { get; set; }
        public Int32? ClientBusinessUnitID { get; set; }
        public Int32? ClientBusinessUnitSiteID { get; set; }
        public Int32? TagNumber { get; set; }
        public Int32? BadgeNumber { get; set; }
        [ForeignKey("Department")]
        public int? DepartmentId { get; set; }
        public int UserType { get; set;} // 1 - Vendor, 2- Client
        public virtual Department Department { get; set; } 
    }
}
