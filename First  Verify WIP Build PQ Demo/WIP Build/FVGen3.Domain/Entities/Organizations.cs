﻿/*
*****************************************************
Page Created Date:  05/01/2013
Created By: Siva Bommisetty
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FVGen3.Domain.Entities
{
    public class Organizations
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long OrganizationID { get; set; }

        public string Name { get; set; }
        public int CountryType { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; } // Kiran on 9/19/2014
        public string City { get; set; }
        public string State { get; set; }
        public int? FinancialMonth { get; set; }
        public int? FinancialDate { get; set; }
        public string Zip { get; set; }
        public string Country { get; set; }
        public string PhoneNumber { get; set; }
        
        public string FaxNumber { get; set; }
        public string WebsiteURL { get; set; }
        public string FederalIDNumber { get; set; }
        public System.Nullable<bool> IsClientInviteOnly { get; set; }
        public string ClientIntroContent { get; set; }
        public System.Nullable<bool> EnableSites { get; set; }
        public System.Nullable<bool> EnableEmployees { get; set; }
        public System.Nullable<bool> ShowDashboardOnLogin { get; set; }
        public string SafetyStatisticsPageNote { get; set; }
        public System.Nullable<DateTime> SubscriptionSignupDate { get; set; }
        public System.Nullable<DateTime> SubscriptionCurrentTermEndDate { get; set; }
        public System.Nullable<DateTime> InsertDateTime { get; set; }
        public System.Nullable<DateTime> UpdateDateTime { get; set; }
        public System.Nullable<bool> EnableSiteContacts { get; set; }
        public string OrganizationType { get; set; }
        public int? CopyAnswers { get; set; }
        public System.Nullable<bool>  TrainingCertificate { get; set; }

        public string SingleClientCode { get; set; }
        public string ClientSecret { get; set; }
        // Kiran on 12/30/2013
        public string OrgRepresentativeName { get; set; }
        public string OrgRepresentativeEmail { get; set; }
        public string OrgInfusionSoftContactId { get; set; }
        // Ends<<<<

        public string PrincipalCompanyOfficerName { get; set; } // Kiran on 2/13/2014
        public System.Nullable<bool> ShowInApplication { get; set; } // Kiran on 6/5/2014
        public string TaxID { get; set; } // Kiran on 8/5/2014
        public string GeographicArea { get; set; }
        public string ExtNumber { get; set; } // Kiran on 8/26/2014

        public System.Nullable<bool> TrainingRequired { get; set; } // Kiran on 10/29/2014
        public Decimal? TrainingFeeCap { get; set; }

        public long? Language { get; set; }
        public int ? ContractorEvaluationExpirationPeriod { get; set; }
        public virtual List<SystemUsersOrganizations> SystemUsersOrganizations { get; set; }
        public virtual List<ClientBusinessUnits> ClientBusinessUnits { get; set; }

        public virtual List<PrequalificationSites> PrequalificationSitesClientList { get; set; }
        public virtual List<PrequalificationSites> PrequalificationSitesVendorList { get; set; }

        public virtual List<PrequalificationReportingData> PrequalificationReportingDataClientList { get; set; }
        public virtual List<PrequalificationReportingData> PrequalificationReportingDataVendorList { get; set; }

        public virtual List<OrganizationsNotificationsSent> OrganizationsNotificationsSentClientList { get; set; }
        public virtual List<OrganizationsNotificationsEmailLogs> OrganizationsNotificationsSentVendorList { get; set; }

        public virtual List<OrganizationsPrequalificationOpenPeriod> OrganizationsPrequalificationOpenPeriod { get; set; } //Siva on 29th Mar 2014
        public virtual List<ClientTemplateProfiles> ClientTemplateProfiles { get; set; } //Siva on 29th Mar 2014
        public virtual List<ClientTemplates> ClientTemplates { get; set; } //Siva on 29th Mar 2014
        public virtual List<OrganizationsEmailSetupNotifications> OrganizationsEmailSetupNotifications { get; set; } //Siva on 29th Mar 2014
        public virtual List<TemplateSubSections> TemplateSubSections { get; set; }
        public virtual List<CSIWebResponseDetails> CSIWebResponseDetails { get; set; } // kiran on 8/7/2014
        public virtual List<ArchiveOrganizationDetails> ArchiveOrganizationDetails { get; set; } // kiran on 8/8/2014

        //public virtual List<SubClients> SubClients { get; set; }

    }
}
