﻿/*
*****************************************************
Page Created Date:  12/18/2014
Created By: Kiran Talluri
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FVGen3.Domain.Entities
{
    public class SystemUsersOrganizationsEmpQuizChecklist
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long SysUsersOrgEmpChkID { get; set; }

        [ForeignKey("SystemUsersOrganizationsQuizzes")]
        public long SysUserOrgQuizId { get; set; }
        public virtual SystemUsersOrganizationsQuizzes SystemUsersOrganizationsQuizzes { get; set; }

        [ForeignKey("CheckListStatus")]
        public long CheckListStatusID { get; set; }
        public virtual CheckListStatus CheckListStatus { get; set; }
    }
}
