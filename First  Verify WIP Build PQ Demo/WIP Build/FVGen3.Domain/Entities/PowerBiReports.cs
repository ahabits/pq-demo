﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.Entities 
{
   public class PowerBiReports
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public long Id { get; set;}
        public string ReportName { get; set; }
        public string Url { get; set; }
        public string DisplayName { get; set; }
        public bool? IsActive { get; set; }
        public  int Order { get; set; }
        public bool Hasfilter { get; set; }
        public bool IsPrequalificationAnalytics { get; set; }
    }
}
