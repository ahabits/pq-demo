﻿/*
Page Created Date:  06/11/2013
Created By: Sumalatha Jeldi
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.Entities
{
     public class LocalSearchVendors
     {
  
        public string VendorName{ get; set; }
        public string PrequalificationStatusName { get; set; }
        public DateTime PrequalificationStart { get; set; }
        public DateTime PrequalificationFinish { get; set; }
        public string clientScope { get; set; }
        public long BUid { get; set; }
        public long BUSiteid { get; set; }
        public long Name { get; set; }
        public List<long> Names { get; set; }
        public string sortType { get; set; }
        public string periodVal { get; set; }
        public string firstLetterVal {get; set;}
        public string searchType { get; set; }
        public DateTime current { get; set; }
        public string site { get; set; }
        public string Subsite { get; set; }
        public string vendorCity { get; set; }
        public string vendorState { get; set; }
        public string MinorityBusiness { get; set; }
        public string UnionShop { get; set; }
        public string MostCurrentEMR { get; set; }
        public string CitedByOsha { get; set; }
        public string SafetyDirectorName { get; set; }
        public string ClientVendorRefId { get; set; }//sumanth on 08/12/2015
        public string contractType { get; set; }
        public string Country
        {
            get;
            set;
        }
        public string BiddingInterestName { get; set; }
        public int Status { get; set; }

        public long ClientOrganizationId { get; set; } // Kiran on 11/1/2014
        public long ClientOrganizationId_Invitation { get; set; }
        public long ClientOrganizationId_SubSec { get; set; }
        public long OOlocation { get; set; } // Kiran on 11/1/2014
        public long COlocation { get; set; } // Kiran on 11/1/2014
        public long OOVisitorlocation { get; set; }
        public long COVisitorlocation { get; set; }
        public List<Regions> GeographicArea { get; set; }
        public Guid TemplateId { get; set; }

        public List<Prequalification> Prequalification { get; set; }
        public List<PrequalificationStatus> PrequalificationStatusList { get; set; }

        public List<BiddingInterests> BiddingInterests { get; set; }
        public List<BiddingInterests> BiddingInterestList { get; set; }
        public List<Boolean> RegionsList { get; set; }
        public List<Boolean> StatusList { get; set; }
        public long[] statusIdList { get; set; }

        public List<Boolean> BiddingList { get; set; }
        public long[] biddingIdList { get; set; }
        public List<Boolean> ClaycoBiddingList { get; set; }
        // Added code on 10/2/2013 by Kiran Talluri for Proficiency Capabilities
        public List<ProficiencyCapabilities> ProficiencyCapabilitiesList { get; set; }

        public List<Boolean> ProficiencyList { get; set; }
        public long[] proficiencyIdList { get; set; }
        // Ends<<<

        public List<Boolean> ProductCategoryList { get; set; }

        public Guid questionnaireType { get; set; } // Mani on 04/10/2015
        //For SCorecard
        public int Score { get; set; }
        public int ReviewType { get; set; }
        public long ClientOrgId { get; set; }
        public Guid ClientTemplateId { get; set; }

    }

}
