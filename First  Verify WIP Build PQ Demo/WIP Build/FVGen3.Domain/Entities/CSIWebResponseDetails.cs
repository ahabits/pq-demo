﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FVGen3.Domain.Entities
{
    public class CSIWebResponseDetails
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long CSIResponseRecID { get; set; }

        public int? RequestInvokedFrom { get; set; }

        // Kiran on 8/7/2014
        [ForeignKey("Organizations")]
        public long? VendorOrgId { get; set; }
        public virtual Organizations Organizations { get; set; }
        // Ends<<<

        public long? ResponseNeededForID { get; set; }
        public long? EntityID { get; set; }
        public long? EntityNumber { get; set; }
        public string ElementNodeName { get; set; }
        public string ElementNodeValue { get; set; }
    }
}
