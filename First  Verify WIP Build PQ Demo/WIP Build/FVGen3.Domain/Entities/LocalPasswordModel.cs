﻿/*
Page Created Date:  05/01/2013
Created By: Rajesh Pendela
Purpose:For Change password 
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.Security.AccessControl;


namespace FVGen3.Domain.Entities
{
    public class LocalPasswordModel
    {

        [Required(ErrorMessageResourceName = "Common_ErrorMessage_Password", ErrorMessageResourceType = typeof(Resources.Resources))]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

        public Guid id;

        [Required(ErrorMessageResourceName = "ErrorMessage_NewPassword", ErrorMessageResourceType = typeof(Resources.Resources))]
        [RegularExpression(@"(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$", ErrorMessage = "Does not meet requirements")]
        [StringLength(100, ErrorMessageResourceName = "ChangePassword_Doesnotmeetrequirements", MinimumLength = 10, ErrorMessageResourceType = typeof(Resources.Resources))]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessageResourceName = "ErrorMessage_PasswordMatch", ErrorMessageResourceType = typeof(Resources.Resources))]
        public string ConfirmPassword { get; set; }
    }
}
