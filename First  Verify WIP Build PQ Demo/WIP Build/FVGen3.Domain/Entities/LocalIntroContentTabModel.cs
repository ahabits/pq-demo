﻿/*
 *****************************************************
Page Created Date:  08/23/2013
Created By: Siva Bommisetty
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;

namespace FVGen3.Domain.Entities
{
    public class LocalIntroContentTabModel
    {
        public long HeaderFooterId { get; set; }

        public int? PositionType { get; set; }
        public string PositionTitle { get; set; }
        public string PostionContent { get; set; }
        public byte?[] DisplayImages { get; set; }
        public bool? AttachmentExist { get; set; }

        public long TemplateSectionId { get; set; }
        public virtual TemplateSections TemplateSections { get; set; }

        public List<Document> documents { get; set; }
    }
}
