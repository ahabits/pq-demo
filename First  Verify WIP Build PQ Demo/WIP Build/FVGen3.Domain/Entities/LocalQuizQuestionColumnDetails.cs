﻿/*
Page Created Date:  10/20/2014
Created By: Kiran Talluri
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace FVGen3.Domain.Entities
{
    public class LocalQuizQuestionColumnDetails
    {
        public bool Visible { get; set; }
        public bool IsMandatory { get; set; }
        public bool IsBold { get; set; }
        public long QuestionId { get; set; }
        [Required(ErrorMessageResourceName = "ErrorMessage_QuestionOptions", ErrorMessageResourceType = typeof(Resources.Resources))]
        public string QuestionOptionValues { get; set; }
        [Required(ErrorMessageResourceName = "ErrorMessage_ChooseAnswer", ErrorMessageResourceType = typeof(Resources.Resources))]        
        public string QuestionAnswer { get; set; }
    }
}
