﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.Entities
{
    public class PrequalificationSectionsToDisplay
    {
        [Key]
        public long DisplayId { get; set; }

        public long PrequalificationId { get; set; }
        public long TemplateSectionId { get; set; }
        public bool? Visible { get; set; }

    }
}
