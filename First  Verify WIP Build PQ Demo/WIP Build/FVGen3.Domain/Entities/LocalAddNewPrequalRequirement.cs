﻿/*
Page Created Date:  12/10/2013
Created By: Siva Bommisetty
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace FVGen3.Domain.Entities
{
    public class LocalAddNewPrequalRequirement
    {
        public long ClientTemplateProfileId { get; set; }

        public long OrganizationID { get; set; }
        public Guid TemplateID { get; set; }
        public string TemplateName { get; set; }

        //[Required(ErrorMessage = "*")]
        //[MaxLength(100, ErrorMessage = "Cannot exceed 100 chars.")]
        public string PrequalRequirementTitle { get; set; }

        [Required(ErrorMessageResourceName = "ErrorMessage_Content", ErrorMessageResourceType = typeof(Resources.Resources))]
        //[AllowHtml]
        [MaxLength(3500, ErrorMessageResourceName = "ErrorMessage_Exceed3500", ErrorMessageResourceType = typeof(Resources.Resources))]
        public string PrequalRequirementContent { get; set; }
    }
}
