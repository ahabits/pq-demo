﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.Entities
{
    public class AssignLocationStatus
    {
        public long PrequalificationSiteId { get; set; }
        public long PrequalificationStatusId { get; set; }
        public long SubsectionId { get; set; }
        public string SendTo { get; set; }
        public string SendCC { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public Guid StatuschangedBy { get; set; }
    }
}
