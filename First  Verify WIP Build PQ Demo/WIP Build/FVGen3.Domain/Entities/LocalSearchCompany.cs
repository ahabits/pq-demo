﻿/*
Page Created Date:  06/11/2013
Created By: Siva Bommisetty
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.Entities
{
    public class LocalSearchCompany
    {
        public bool SearchType { get; set; }
        public string CompanyName { get; set; }
        public bool typeClient { get; set; }
        public bool typeVendor { get; set; }
        public bool typeAdmin { get; set; }

        public long OrganizationID { get; set; }

        public List<Organizations> Organizations { get; set; }
    }
}
