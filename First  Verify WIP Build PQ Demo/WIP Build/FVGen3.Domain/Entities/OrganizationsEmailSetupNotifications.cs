﻿/*
 *****************************************************
Page Created Date:  06/22/2013
Created By: Siva Bommisetty
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FVGen3.Domain.Entities
{
    public class OrganizationsEmailSetupNotifications
    {
        [Key]
        public long OrgEmailSetupId { get; set; }

        public int SetupType { get; set; }
        public long? SetupTypeId { get; set; }
        public int FirstNotification { get; set; }
        public int? SecondNotification { get; set; }
        public int? ThirdNotification { get; set; }
        public string SendNotificationTo { get; set; }

        [ForeignKey("OrganizationId")]
        public virtual Organizations Organizations { get; set; }
        public long OrganizationId { get; set; }

        // Kiran on 11/27/2013
        public int? EmailTemplateType { get; set; }
        [ForeignKey("EmailTemplateID")]
        public virtual EmailTemplates EmailTemplates { get; set; }
        public int? EmailTemplateID { get; set; }
        // Ends<<<

        public bool? Status { get; set; } // Kiran on 01/19/2015
    }
}
