﻿/*
Page Created Date:  06/11/2013
Created By: Siva Bommisetty
Purpose:
Version: 1.0
****************************************************
*/

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Helpers;
using Resources;
using System.Web.Mvc;
using FVGen3.Domain.ViewModels;

namespace FVGen3.Domain.Entities
{
    public class LocalAddNewCompany
    {
        [Required(ErrorMessage = "*")]
        public long OrganizationID { get; set; }
        //[Required(ErrorMessage = "*")]
        public int CountryType { get; set; }
        [Required(ErrorMessage = "*")]
        [MaxLength(200, ErrorMessageResourceName = "ErrorMessage_Exceed200", ErrorMessageResourceType = typeof(Resources.Resources))]
        public string Name { get; set; }

        // Kiran on 3/20/2014
        [Required(ErrorMessage = "*")]
        public string PrincipalCompanyOfficerName { get; set; }
        // Ends<<<

        // Kiran on 8/5/2014
        [Required(ErrorMessage = "*")]
        //[StringLength(50, ErrorMessage = "Invalid Tax Id", MinimumLength = 10)] // Kiran on 12/10/2014
        [RegularExpression(@"[0-9]{3}[-][0-9]{2}[-][0-9]{4}|[0-9]{2}[-][0-9]{7}", ErrorMessageResourceName = "Common_InvalidTaxID", ErrorMessageResourceType = typeof(Resources.Resources))] //Kiran on 8/20/2014
        public string TaxID { get; set; }
        //Ends<<<

        [Required(ErrorMessage = "*")]
        [MaxLength(200, ErrorMessageResourceName = "ErrorMessage_Exceed200", ErrorMessageResourceType = typeof(Resources.Resources))]
        public string Address1 { get; set; }

        [MaxLength(200, ErrorMessageResourceName = "ErrorMessage_Exceed200", ErrorMessageResourceType = typeof(Resources.Resources))]
        public string Address2 { get; set; }

        // Kiran on 9/19/2014
        [MaxLength(200, ErrorMessageResourceName = "ErrorMessage_Exceed200", ErrorMessageResourceType = typeof(Resources.Resources))]
        public string Address3 { get; set; }
        // Ends<<<

        [Required(ErrorMessage = "*")]
        [MaxLength(200, ErrorMessageResourceName = "ErrorMessage_Exceed200", ErrorMessageResourceType = typeof(Resources.Resources))]
        public string City { get; set; }

        [Required(ErrorMessage = "*")]
        [MaxLength(200, ErrorMessageResourceName = "ErrorMessage_Exceed200", ErrorMessageResourceType = typeof(Resources.Resources))]
        public string State { get; set; }
        //[Required(ErrorMessage = "*")]
        public int? FinancialMonth { get; set; }
        //[Required(ErrorMessage = "*")]
        public int? FinancialDate { get; set; }

        [Required(ErrorMessage = "*")]
        [MaxLength(20, ErrorMessageResourceName = "ErrorMessage_Exceed20", ErrorMessageResourceType = typeof(Resources.Resources))]
        public string Zip { get; set; }

        [Required(ErrorMessage = "*")]
        [MaxLength(100, ErrorMessageResourceName = "ErrorMessage_Exceed100", ErrorMessageResourceType = typeof(Resources.Resources))]
        public string Country { get; set; }

        [Required(ErrorMessage = "*")]
        //[StringLength(50, ErrorMessage = "Not a valid phone number", MinimumLength = 14)]
        public string PhoneNumber { get; set; }

        [MaxLength(100, ErrorMessageResourceName = "ErrorMessage_Exceed100", ErrorMessageResourceType = typeof(Resources.Resources))]
        public string FaxNumber { get; set; }

        [MaxLength(200, ErrorMessageResourceName = "ErrorMessage_Exceed200", ErrorMessageResourceType = typeof(Resources.Resources))]
        public string WebsiteURL { get; set; }

        [MaxLength(50, ErrorMessageResourceName = "ErrorMessage_Exceed50", ErrorMessageResourceType = typeof(Resources.Resources))]
        public string FederalIDNumber { get; set; }
        //public List<int> Regionid
        //{
        //    get;
        //    set;
        //}
        public bool AcceptVendorsViaInvite { get; set; }  // Siva - 10th Aug 2013

        public bool TrainingRequired { get; set; } // Kiran on 10/29/2014
        public string TrainingFeeCap { get; set; } // Kiran on 10/29/2014

        public string OrganizationType { get; set; }

        public bool isSuccess { get; set; }
        public bool isCompanyExist { get; set; }

        public bool ConfirmDelete { get; set; }
        public string ClientSecrte { get; set; }

        public virtual List<Prequalification> Prequalification { get; set; }
        public virtual List<Document> Documents { get; set; }

        public string QuestionarieTypeId { get; set; }
        public string IntroContentStr { get; set; }

        public string QuestionarieTypeIdForPrequalRequirements { get; set; }

        //Added on 23rd July
        public string Logo { get; set; }
        public string Photo { get; set; } 
        //ends

        // Kiran on 12/30/2013
        public string OrgRepresentativeName { get; set; }

        [RegularExpression(LocalConstants.EMAIL_FORMAT, ErrorMessageResourceName = "Common_Email_Invalid_ErrorMessage", ErrorMessageResourceType = typeof(Resources.Resources))]
        public string OrgRepresentativeEmail { get; set; }
        public string OrgInfusionSoftContactId { get; set; }
        // Ends<<<<
        public string SingleClientCode { get; set; }

        // Kiran on 3/25/2014
        [Required(ErrorMessage = "*")]
        public string CopyAnswers { get; set; }

        [RegularExpression(@"^-*[0-9,\.]+$", ErrorMessageResourceName = "ErrorMessage_PhoneNumber", ErrorMessageResourceType = typeof(Resources.Resources))]//Phani on 23/07/2015
        public string ExtNumber { get; set; } // Kiran on 8/26/2014


        public bool TrainingCertificate { get; set; }

        public List<SelectListItem> clients { get; set; }
        [Required(ErrorMessage = "*")]
        public long? Language { get; set; }
        [Required(ErrorMessage = "*")]
        public List<string> GeographicArea { get; set; }
        public List<string> DisabledClients { get; set; }
        public List<ArchiveOrganizationDetailsViewModel> VendorArchivedDetails { get; set; }
        
        public int? ContractorEvaluationExpirationPeriod { get; set; }
    }
    // Kiran on 03/10/2015
    public class LocalImages
    {
        public string Name { get; set; }
        public WebImage Image { get; set; }
    }
}
