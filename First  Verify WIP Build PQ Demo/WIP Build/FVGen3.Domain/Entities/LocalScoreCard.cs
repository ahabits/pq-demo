﻿/*
Page Created Date:  03/24/2015
Created By: Mani
Purpose:Cargil Requirement
Version: 1.0
****************************************************
*/
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FVGen3.Domain.Entities
{
   public class LocalScoreCard
    {
       [Required(ErrorMessage = "*")]
       public Guid EvaluatedBy { get; set; }
       public bool CanUserEdit { get; set; }
       public int? ReviewType { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
       public DateTime? ReviewDate { get; set; }
        [Required(ErrorMessage = "*")]
       public string ProjectLocation { get; set; }
        [Required(ErrorMessage = "*")]
       public string ContractWONumber { get; set; }
       //public bool? DoYouRecommend { get; set; }

       public List<LocalVendorReviewSection> LocalVendorReviewSection { get; set; }

       public List<LocalVendorReview> VendorReviews { get; set; }
        //[MaxLength(3500, ErrorMessage = "Cannot exceed 500 chars.")]
      // public string Comments { get; set; }
       public long? PreQualificationId { get; set; }
       public long? SelectedVendorId { get; set; }
       public long? buid { get; set; }
       public long? VendorReviewInputId { get; set; }
       public string VendorName { get; set; }
       public List<KeyValuePair<string, string>> ClientUsers { get; set; }
      
    }

   public class LocalVendorReview
   {
       public long? VendorReviewQuestionId { get; set; }
       public string QuestionString { get; set; }
        
       public int? Rating { get; set; }
       public bool? QisMandatory { get; set; }
   }

   public class LocalVendorReviewSection
   {
       public LocalVendorReviewSection()
       {
           IsEditable = true;
       }
       public long Id { get; set; }
       public string SectionHeader { get; set; }
       public bool? HasQuestion { get; set; }
       public string QuestionText { get; set; }
       public long? QuestionTypeID { get; set; }
       public string Options { get; set; }
       public long? ClientId { get; set; }
       public int? Order { get; set; }
       public string Answers { get; set; }
       public bool IsEditable { get; set; }
       public int? Group { get; set; }
   }

}
