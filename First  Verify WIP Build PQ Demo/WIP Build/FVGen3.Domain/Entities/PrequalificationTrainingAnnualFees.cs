﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace FVGen3.Domain.Entities
{
    public class PrequalificationTrainingAnnualFees
    {
        [Key]
        public long PreqAnnualFeeRecId { get; set; }

        [ForeignKey("Prequalification")]
        public long PrequalificationId { get; set; }

        [ForeignKey("ClientTemplatesForBU")]
        public long ClientTemplateForBUId { get; set; }

        [ForeignKey("ClientTemplatesBUGroupPrice")]
        public long ClientTemplateBUGroupId { get; set; }

        public decimal? AnnualFeeAmount { get; set; }
        public decimal? AnnualFeeReceivedAmount { get; set; }
        public DateTime? FeeReceivedDate { get; set; }
        public string FeeTransactionId { get; set; }
        public string FeeTransactionMetaData { get; set; }
        public bool? SkipPayment { get; set; }

        public virtual Prequalification Prequalification { get; set; }
        public virtual ClientTemplatesForBU ClientTemplatesForBU { get; set; }
        public virtual ClientTemplatesBUGroupPrice ClientTemplatesBUGroupPrice { get; set; }
    }
}
