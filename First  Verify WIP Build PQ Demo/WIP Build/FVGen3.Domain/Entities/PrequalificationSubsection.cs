﻿

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace FVGen3.Domain.Entities
{
    public class PrequalificationSubsection
    {
        public long Id { get; set; }
        [ForeignKey("Prequalification")]
        public long PQId { get; set; }
        public string SubSectionName { get; set; }
        public string SubSectionHeader { get; set; }
        public string SubSectionFooter { get; set; }
        public bool VisibleToVendor { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid CreatedBy { get; set; }
        [ForeignKey("TemplateSubSections")]
        public long SubSectionId { get; set; }
        public long GroupNumber { get; set; }
        public string SubSectionType { get; set; }
        public virtual Prequalification Prequalification { get; set; }
        public int Order { get; set; }
        public virtual List<PrequalificationQuestion> PrequalificationQuestion { get; set; }
        public virtual TemplateSubSections TemplateSubSections { get; set; }
    }
}
