﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace FVGen3.Domain.Entities
{
    public class ClientTemplatesForBUSites
    {
        [Key]
        public long  ClientTemplateForBUSitesId{get;set;}
       
        [ForeignKey("ClientTemplatesForBU")]
        public long  ClientTemplateForBUId {get;set;}
        public virtual ClientTemplatesForBU  ClientTemplatesForBU { get; set; }

        [ForeignKey("ClientBusinessUnitSites")]
        public long ClientBusinessUnitSiteId { get; set; }
        public virtual ClientBusinessUnitSites ClientBusinessUnitSites { get; set; }

        public bool? IsPaymentDone { get; set; }

        public virtual List<QuizPaymentsItems> QuizPaymentsItems { get; set; }
    }
}
