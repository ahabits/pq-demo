﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FVGen3.Domain.Entities
{
    public class RegionStates
    {
        [Key]
        public int StateId
        {
            get;
            set;
        }

        public string StateName
        {
            get;
            set;
        }
        [ForeignKey("Regions")]
        public int Regionid { get; set; }

        public virtual Regions Regions { get; set; }
      
    }
}