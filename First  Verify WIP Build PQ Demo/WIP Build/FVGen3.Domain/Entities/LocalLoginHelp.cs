﻿/*
Page Created Date:  10/13/2015
Created By: Kiran Talluri
Purpose:
Version: 1.0
****************************************************
*/

using System.ComponentModel.DataAnnotations;
using Resources;

namespace FVGen3.Domain.Entities
{
    public class LocalLoginHelp
    {
        [Required(ErrorMessage = "*")]
        public string PersonName { get; set; }
        [Required(ErrorMessage = "*")]
        public string CompanyName { get; set; }
        [Required(ErrorMessage = "*")]
        public string Address { get; set; }
        [Required(ErrorMessage = "*")]
        [StringLength(50, ErrorMessageResourceName = "ErrorMessage_Invalid_PhoneNumber", ErrorMessageResourceType = typeof(Resources.Resources), MinimumLength = 14)]
        public string TelephoneNumber { get; set; }
        // sumanth on 10/15/2015 for SMI-267
        [Required(ErrorMessage = "*")]
        [StringLength(256, ErrorMessageResourceName = "ErrorMessage_Exceed256", ErrorMessageResourceType = typeof(Resources.Resources))]
        [RegularExpression(LocalConstants.EMAIL_FORMAT, ErrorMessageResourceName = "Common_Email_Invalid_ErrorMessage", ErrorMessageResourceType = typeof(Resources.Resources))]
        public string Email { get; set; }
        public string Comments { get; set; }
    }
}
