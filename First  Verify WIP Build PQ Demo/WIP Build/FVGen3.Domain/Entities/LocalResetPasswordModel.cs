﻿/*
Page Created Date:  06/11/2013
Created By: Sumalatha Jeldi
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace FVGen3.Domain.Entities
{
    public class LocalResetPasswordModel
    {
        public Guid userId { get; set; }

        public string email { set; get; }

        [Required( ErrorMessageResourceName = "ErrorMessage_NewPassword", ErrorMessageResourceType = typeof(Resources.Resources))]
        [RegularExpression(@"(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$", ErrorMessageResourceName = "passwordReset_doseNotMeetReq", ErrorMessageResourceType = typeof(Resources.Resources))]
        [StringLength(100, ErrorMessageResourceName = "ChangePassword_Doesnotmeetrequirements", MinimumLength = 10, ErrorMessageResourceType = typeof(Resources.Resources))]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessageResourceName = "ErrorMessage_PasswordMatch", ErrorMessageResourceType = typeof(Resources.Resources))]
        public string ConfirmPassword { get; set; }
    }
}
