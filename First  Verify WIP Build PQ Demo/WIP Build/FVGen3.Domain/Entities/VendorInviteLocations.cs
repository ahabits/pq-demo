﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FVGen3.Domain.Entities
{
    public class VendorInviteLocations:ICloneable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long VendorInviteLocationId { get; set; }
        [ForeignKey("VendorInviteDetails")]        
        public Guid InvitationId { get; set; }
        public virtual VendorInviteDetails VendorInviteDetails
        {
            get;
            set;
        }
        [ForeignKey("ClientBusinessUnitSites")]
        public long ClientBusinessUnitSiteID { get; set; }
        public virtual ClientBusinessUnitSites ClientBusinessUnitSites { get; set; }
        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
