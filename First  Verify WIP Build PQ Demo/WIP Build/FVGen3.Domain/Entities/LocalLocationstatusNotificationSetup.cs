﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.Entities
{
    public class LocalLocationstatusNotificationSetup
    {
        public long TemplateSectionId { get; set; }
        public long EmailTemplateId { get; set; }
    }
}
