﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace FVGen3.Domain.Entities
{
    public class LocalAssignQuiz
    {
        public List<TemplateInfo> templateInfo { get; set; }
        public string ClientId { get; set; }

        public class TemplateInfo
        {
            public string TemplateName { get; set; }
            public Guid TemplateId { get; set; }
            public string FileName { get; set; }
            public bool IsChecked { get; set; }
            public bool IsReadOnly { get; set; }
            public string QuizPassRate { get; set; } // Kiran on 2/21/2014
        }
    }
    
}
