﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.Entities
{
 public class ClientNotifications
    {[Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        [ForeignKey("ClientId")]  
        public virtual Organizations Organizations { get; set; }
        public long ClientId { get; set; }
        public string UserEmails { get; set; }

        public string UserId { get; set; }
        public int Type { get; set; } 
      
    }
}
