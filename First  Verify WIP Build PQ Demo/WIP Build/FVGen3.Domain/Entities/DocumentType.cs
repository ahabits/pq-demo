﻿/*
Page Created Date:  06/11/2013
Created By: Siva Bommisetty
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace FVGen3.Domain.Entities
{
    public class DocumentType
    {
        [Key]
        public long DocumentTypeId { get; set; }

        public string DocumentTypeName { get; set; }
        public bool DocumentExpires { get; set; }
        public bool RestrictAccess { get; set; }
        public string DocumentInstruction { get; set; }
        public bool PrequalificationUseOnly { get; set; }

        public virtual List<Document> Documents { get; set; }

    }
}
