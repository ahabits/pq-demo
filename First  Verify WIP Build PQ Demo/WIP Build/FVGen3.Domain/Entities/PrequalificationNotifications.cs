﻿/*
Page Created Date:  2/18/2014
Created By: Rajesh Pendela
Purpose:
Version: 1.0
****************************************************
*/

using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace FVGen3.Domain.Entities
{
    public class PrequalificationNotifications
    {
        
        [Key]
        public long PrequaliNotificationId { get; set; }

        [ForeignKey("TemplateSubSectionsNotifications")]
        public long TemplateSubSectionNotificationId { get; set; }
        public virtual TemplateSubSectionsNotifications TemplateSubSectionsNotifications { get; set; }

        public DateTime NotificationDateTime { get; set; }

        [ForeignKey("Prequalification")]
        public long? PrequalificationId { get; set; }
        public virtual Prequalification Prequalification { get; set; }
        
        

    }
}