﻿

using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace FVGen3.Domain.Entities
{
    public class VendorEmailLog
    {
        public long Id { get; set; }
        [ForeignKey("SystemUsers")]
        public Guid UserId { get; set; }
        public virtual SystemUsers SystemUsers { get; set; }
        [ForeignKey("Prequalification")]
        public long PQId { get; set; }
        public virtual Prequalification Prequalification { get; set; }
        public string ToAddress { get; set; }
        public string CCAddress { get; set; }
        public string BCCAddress { get; set; }
        public string Body { get; set; }
        public bool EmailStatus { get; set; }
        public DateTime SendDate { get; set; }
    }
}
