﻿
using System.ComponentModel.DataAnnotations.Schema;

namespace FVGen3.Domain.Entities
{
    public class PrequalificationQuestion
    {
        public long Id { get; set; }
        [ForeignKey("Prequalification")]
        public long PQId { get; set; }
        public virtual Prequalification Prequalification { get; set; } 
        public string QuestionText { get; set; }
        public string QuestionInputValue { get; set; }
        public long QuestionType { get; set; }
        [ForeignKey("PrequalificationSubsection")]
        public long PQSubSectionId { get; set; }

        public virtual PrequalificationSubsection PrequalificationSubsection { get; set; } 
    }
}
