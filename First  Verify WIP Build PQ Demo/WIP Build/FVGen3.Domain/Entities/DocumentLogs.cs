﻿/*
Page Created Date:  06/11/2013
Created By: Siva Bommisetty
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FVGen3.Domain.Entities
{
    public class DocumentLogs
    { 
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public Guid DocumentId { get; set; }

        public long OrganizationId { get; set; }
        
        public string ReferenceType { get; set; }
        public System.Nullable<long> ReferenceRecordID { get; set; }
        public string DocumentName { get; set; }
        public DateTime Uploaded { get; set; }
        public DateTime? Expires { get; set; }
        public bool? EmailSent { get; set; }
        public DateTime? EmailSentDateTime { get; set; }
        public bool? ShowOnDashboard { get; set; }//sumanth for SMI-226
        [ForeignKey("DocumentType")]
        public long DocumentTypeId { get; set; }

        public virtual DocumentType DocumentType { get; set; }

        [ForeignKey("SystemUserAsUploader")]
        public virtual SystemUsers Systemusers { get; set; }
        public System.Nullable<Guid> SystemUserAsUploader { get; set; }
        public string DocumentStatus { get; set; }

        // Kiran on 8/25/2014
        public DateTime? UpdatedOn { get; set; }
        [ForeignKey("UpdatedBy")]
        public virtual SystemUsers UpdatedUser { get; set; }
        public System.Nullable<Guid> UpdatedBy { get; set; }
        // Ends<<<
        // sumanth on 10/20/2015 for FV-220 to display the log details when status changed for document
        public DateTime? StatusChangeDate { get; set; }
        [ForeignKey("StatusChangedBy")]
        public virtual SystemUsers StatusChangedUser { get; set; }
        public System.Nullable<Guid> StatusChangedBy { get; set; }
        public int? OrderBy { get; set; }
        // sumanth ends on 10/20/2015

        
        public long? ClientId { get; set; }
        [ForeignKey("ClientId")]
        public virtual Organizations ClientOrganizations { get; set; }

        public long? SectionId { get; set; }


        public DateTime? ModifiedOn { get; set; }
        [ForeignKey("ModifiedBy")]
        public virtual SystemUsers ModifiedUser { get; set; }
        public System.Nullable<Guid> ModifiedBy { get; set; }
        public string ModificationType { get; set; }
    }
}
