﻿/*
Page Created Date:  06/07/2013
Created By: Siva Bommisetty
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FVGen3.Domain.Entities
{
    public class ClientBusinessUnits
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long ClientBusinessUnitId { get; set; }

        
        public string BusinessUnitName { get; set; }

        public DateTime InsertDate { get; set; }
        public System.Nullable<DateTime> UpdateDate { get; set; }

        [ForeignKey("ClientId")]
        public virtual Organizations Organization { get; set; }
        public long ClientId { get; set; }

        public virtual List<ClientBusinessUnitSites> ClientBusinessUnitSites { get; set; }
        public virtual List<PrequalificationSites> PrequalificationSites { get; set; }   // Rajesh on 07/24/2013

        public virtual List<ClientTemplatesForBU> ClientTemplatesForBU { get; set; }  // Kiran on 08/02/2013

    }
}
