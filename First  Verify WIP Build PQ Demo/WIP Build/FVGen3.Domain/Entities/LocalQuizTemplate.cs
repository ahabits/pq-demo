﻿/*
Page Created Date:  11/27/2013
Created By: Rajesh Pendela
Purpose:
Version: 1.0
****************************************************
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace FVGen3.Domain.Entities
{
    public class LocalQuizTemplate
    {
        [Required(ErrorMessage="*")]
        public string TemplateName { get; set; }
        public string TemplateNote { get; set; }
        public Guid? TemplateId { get; set; }
    }
}
