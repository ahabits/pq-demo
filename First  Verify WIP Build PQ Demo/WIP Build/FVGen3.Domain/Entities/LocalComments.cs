﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace FVGen3.Domain.Entities
{
    public class LocalComments
    {
        [Required(ErrorMessage ="*")]
        
        public string comment { get; set; }
        public long prequalificationId { get; set; }
        public long EmailRecordId { get; set; } // kiran on 9/1/2014
    }
}
