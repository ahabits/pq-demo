﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace FVGen3.Domain.Entities
{
    public class LocalSupportedDocument
    {
        [Required(ErrorMessage = "*")]
        public string DocumentTypeId { get; set; }
        [Required(ErrorMessage = "*")]
        public string Expires { get; set; }
        [Required(ErrorMessage = "*")]
        public string file { get; set; }
        public long subSectionId { get; set; }
        public Guid? DocumentId { get; set; }
        public int? Mode { get; set; }
        public long PrequalificationId { get; set; }
        public long ClientId { get; set; }

        public long SectionId { get; set; }
        public virtual Organizations ClientOrganizations { get; set; }
    }
}
