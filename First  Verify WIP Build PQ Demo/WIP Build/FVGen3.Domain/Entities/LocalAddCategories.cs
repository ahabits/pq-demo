﻿/*
Page Created Date:  02/16/2015
Created By: Kiran Talluri
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace FVGen3.Domain.Entities
{
    public class LocalAddCategories
    {
        public long EmpQuizCategoryId { get; set; }
        public string CategoryName { get; set; }
        public int? CategoryType { get; set; }
        public int? SubCategoryControlType { get; set; }
        public string SubCategoriesValue { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        [Required(ErrorMessage="*")]
        public DateTime? SubCategoryEnteredDate { get; set; }
        public string SubCategorySelectedValue { get; set; }
        public long SysUserOrgId { get; set; }
        public long SysUserOrgQuizId { get; set; }
    }
}
