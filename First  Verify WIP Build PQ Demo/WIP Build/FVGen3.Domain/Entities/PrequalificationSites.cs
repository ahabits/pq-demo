﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FVGen3.Domain.Entities
{
    public class PrequalificationSites
    {
        [Key]
        public long PrequalificationSitesId { get; set; }

        public string ClientBULocation { get; set; }
        public string Comment { get; set; }

        [ForeignKey("Prequalification")]
        public long? PrequalificationId { get; set; }
        public virtual Prequalification Prequalification { get; set; }

        [ForeignKey("ClientId"), InverseProperty("PrequalificationSitesClientList")]
        public virtual Organizations Client { get; set; }
        public long? ClientId { get; set; }

        [ForeignKey("VendorId"), InverseProperty("PrequalificationSitesVendorList")]
        public virtual Organizations Vendor { get; set; }
        public long? VendorId { get; set; }

        [ForeignKey("ClientBusinessUnits")]
        public long? ClientBusinessUnitId { get; set; }
        public virtual ClientBusinessUnits ClientBusinessUnits { get; set; }

        [ForeignKey("ClientBusinessUnitSites")]
        public long? ClientBusinessUnitSiteId { get; set; }
        public virtual ClientBusinessUnitSites ClientBusinessUnitSites { get; set; }

        //Mani on 8/18/2015
        [ForeignKey("PrequalificationPayments")]
        public long? PrequalificationPaymentsId { get; set; }
        public virtual PrequalificationPayments PrequalificationPayments { get; set; }

        //[ForeignKey("PrequalificationStatus")]
        //public long? PrequalificationStatusId { get; set; }
        //public virtual PrequalificationStatus PrequalificationStatus { get; set; }
        

        [ForeignKey("PrequalificationStatus")]
        public long? PQSiteStatus { get; set; }
        public virtual PrequalificationStatus PrequalificationStatus { get; set; }

        [NotMapped]
        public long SiteStatusId { get { return PQSiteStatus ?? Prequalification.PrequalificationStatusId; }set { PQSiteStatus = value; } }
        public PrequalificationStatus SiteStatus { get { return PrequalificationStatus ?? Prequalification.PrequalificationStatus; } }
       // public  virtual LocationStatusLog LocationStatusLog { get; set; }

    }
}
