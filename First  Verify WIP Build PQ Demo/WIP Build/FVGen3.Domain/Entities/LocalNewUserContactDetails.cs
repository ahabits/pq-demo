﻿/*
Page Created Date:  05/18/2013
Created By: Kiran Talluri
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;

namespace FVGen3.Domain.Entities
{
    public class LocalNewUserContactDetails
    {
        public LocalNewUserContactDetails()
        {
            UserStatus = true;
        }
        //[Required(ErrorMessage = "*")]
        public System.Nullable<bool> UserStatus { get; set; }

        [Required(ErrorMessage = "*")]
        [StringLength(100, ErrorMessageResourceName = "ErrorMessage_Exceed100", ErrorMessageResourceType = typeof(Resources.Resources))]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "*")]
        [StringLength(100, ErrorMessageResourceName = "ErrorMessage_Exceed100", ErrorMessageResourceType = typeof(Resources.Resources))]
        public string LastName { get; set; }

        [Required(ErrorMessage = "*")]
        [StringLength(51, ErrorMessageResourceName = "ErrorMessage_Exceed51", ErrorMessageResourceType = typeof(Resources.Resources))]
        public string ContactTitle { get; set; }

        [Required(ErrorMessage = "Please enter phone number")]
        [StringLength(100, ErrorMessageResourceName = "LocalNewUserContactDetails_InvalidNumber", ErrorMessageResourceType = typeof(Resources.Resources))]
        public string PhoneNumber { get; set; }

        public string PhoneNumberInternational { get; set; }

        [StringLength(2000, ErrorMessageResourceName = "ErrorMessage_Exceed2000", ErrorMessageResourceType = typeof(Resources.Resources))]
        public string AdditionalDescription { get; set; }

        [Required(ErrorMessage = "*")]
        [StringLength(256, ErrorMessageResourceName = "ErrorMessage_Exceed256", ErrorMessageResourceType = typeof(Resources.Resources))]
        // Kiran on 01/20/2015
        //[RegularExpression(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$", ErrorMessageResourceName = "LocalNewUserContactDetails_InvalidEmail", ErrorMessageResourceType = typeof(Resources.Resources))]
        [RegularExpression(LocalConstants.EMAIL_FORMAT, ErrorMessageResourceName = "LocalNewUserContactDetails_InvalidEmail", ErrorMessageResourceType = typeof(Resources.Resources))]
        // Ends<<<
        public string Email { get; set; }

        //[Required(ErrorMessage = "*")]
        //[StringLength(128, ErrorMessage = "Cannot exceed 128 chars")]
        //[RegularExpression(@"(?=^.{10,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$", ErrorMessageResourceName = "LocalNewUserContactDetails_InvalidPassword", ErrorMessageResourceType = typeof(Resources.Resources))]
        public string Password { get; set; }

        //[Required(ErrorMessage = "*")]
        //[StringLength(128, ErrorMessage = "Cannot exceed 128 chars")]
        //[System.Web.Mvc.Compare("Password", ErrorMessageResourceName = "LocalNewUserContactDetails_ConfirmPassword", ErrorMessageResourceType = typeof(Resources.Resources))]
        public string ConfirmPassword { get; set; }

        public System.Nullable<DateTime> LastLoginDate { get; set; }
        public System.Nullable<DateTime> LastPasswordChangedDate { get; set; }
        public string UserRole { get; set; }
        public bool UserSearchEdit { get; set; }
        public Guid userId { get; set; }

        // Date: 7/10/2013  Added code to handle company access & deleting user if logged in as admin while editing through search
        public string currentUser_OrgType { get; set; }
        // Changed code ends

        public bool isSuperUser { get; set; }
        [RegularExpression("([1-9][0-9]*)", ErrorMessageResourceName = "ErrorMessage_PhoneNumber", ErrorMessageResourceType = typeof(Resources.Resources))]
        public string ExtNumber { get; set; } // Kiran on 8/26/2014
        public bool ForSuccessMessage { get; set; } // kiran on 9/25/2014

        public List<SystemUsersOrganizations> OrganizationsList { get; set; }

        public List<KeyValuePair<string, string>> ClientBusinessUnits { get; set; }

        //[Required(ErrorMessage = "*")]
        public List<string> ClientBU { get; set; }

        public IEnumerable<KeyValuePair<string, string>> ClientBUSites { get; set; }

        //[Required(ErrorMessage = "please select the location")]
        public List<string> ClientBUSite { get; set; }
        public long UserOrganizationId { get; set; }
        public List<KeyValuePair<string, int>> Department { get; set; }
        public int DepartmentId { get; set; }
    }
}
