﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace FVGen3.Domain.Entities
{
    public class QuestionsForMigration
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public long Id
        {
            get;
            set;
        }
        public string SectionName
        {
            get;
            set;
        }
        public string SubSection
        {
            get;
            set;
        }
        public string QuestionText
        {
            get;
            set;
        }
        public long? DestinationQuestionId
        {
            get;
            set;
        }
        public long? DestinationQuestioncolumnId
        {
            get;
            set;
        }
        public long? SourceQuestionId
        {
            get;
            set;
        }
        public long? SourceQuestioncolumnId
        {
            get;
            set;
        }
        public long? SectionID
        {
            get;
            set;
        }
        public long? DestinationSectionId
        {
            get;
            set;
        }
       
        public Guid SourceTemplateId
        {
            get;
            set;
        }
        public Guid DestinationTemplateId
        {
            get;
            set;
        }
        public long? SourceSubsectionId { get; set; }
        public long? DestinationSubsectionId { get; set; }

    }
}