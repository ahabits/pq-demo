﻿/*
Page Created Date:  05/01/2013
Created By: Siva Bommisetty
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FVGen3.Domain.Entities
{
    public class Templates
    {
        
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public Guid TemplateID { get; set; }
        public bool IsHide { get; set; }
        public string TemplateName { get; set; }
        public bool? DefaultTemplate { get; set; }
        public int? TemplateType { get; set; }
        public string TemplateNotes { get; set; }
        public string TemplateLowerName { get; set; }
        public int TemplateStatus { get; set; }//0-Inprocess 1-Completed
        public int? TemplateSequence { get; set; }// Mani on 7/31/2015 for FV-169
        public string RiskLevel { get; set; }
        public bool? LockedByDefault { get; set; }
        public bool? RenewalLock { get; set; }
        public string TemplateCode { get; set; }
        public virtual List<TemplateSections> TemplateSections { get; set; }
        public virtual List<ClientTemplates> ClientTemplates { get; set; }
        public virtual List<TemplateLog> TemplateLogs { get; set; }
        public string unlockNotificationAdminEmails { get; set; }        
        public int? EmaiTemplateId { get; set; }
        public string LockComments { get; set; }
        public bool? ExcludePQNotifications { get; set; }
        [ForeignKey("Language")]
        public long? LanguageId { get; set; }
        public virtual Language Language { get; set; }
        public bool IsERI { get; set; }
        public bool? HideClients { get; set; }
        public bool HasLocationNotification { get; set; }
        public bool HasVendorDetailLocation { get; set; }
        public bool HideCommentsSec { get; set; }
    }
}
