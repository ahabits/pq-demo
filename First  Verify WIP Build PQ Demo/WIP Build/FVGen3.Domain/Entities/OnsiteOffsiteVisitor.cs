﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace FVGen3.Domain.Entities
{
    public class OnsiteOffsiteVisitor
    {

        [Key]
        public Guid OnsiteOffsiteID { get; set; }
        public Guid VisitorID { get; set; }
        public DateTime? Checkin { get; set; }
        public Guid? CheckinClientUser { get; set; }
        public DateTime? Checkout { get; set; }
        public Guid? CheckoutClientUser { get; set; }
        public Int32? VendorID { get; set; }
        public Int32? ClientID { get; set; }
        public Int32? ClientBusinessUnitID { get; set; }
        public Int32? TagNumber { get; set; }
        public Int32? ClientBusinessUnitSiteID { get; set; }
        public Int32? BadgeNumber { get; set; }
       
    }
}

