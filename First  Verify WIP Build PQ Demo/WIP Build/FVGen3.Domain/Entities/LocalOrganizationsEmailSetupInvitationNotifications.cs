﻿/*
 *****************************************************
Page Created Date:  10/27/2015
Created By: Sumanth
Purpose:FV-223
Version: 1.0
****************************************************
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;


namespace FVGen3.Domain.Entities
{
    public class LocalOrganizationsEmailSetupInvitationNotifications
    {

        public long OrganizationId { get; set; }
        public int SetupType { get; set; }

        public string SetupTypeId { get; set; }

        [Required(ErrorMessageResourceName = "ErrorMessage_Required_NumberOfDays", ErrorMessageResourceType = typeof(Resources.Resources))]
        [RegularExpression("[0-9]{1,2}", ErrorMessageResourceName = "ErrorMessage_30Days", ErrorMessageResourceType = typeof(Resources.Resources))]
        public int? FirstNotification { get; set; }

        [RegularExpression("[0-9]{1,2}", ErrorMessageResourceName = "ErrorMessage_30Days", ErrorMessageResourceType = typeof(Resources.Resources))]
        public int? SecondNotification { get; set; }

        [RegularExpression("[0-9]{1,2}", ErrorMessageResourceName = "ErrorMessage_30Days", ErrorMessageResourceType = typeof(Resources.Resources))]
        public int? ThirdNotification { get; set; }
        
        public bool isInsert { get; set; }
        public long OrgEmailSetupId { get; set; }
    }
}
