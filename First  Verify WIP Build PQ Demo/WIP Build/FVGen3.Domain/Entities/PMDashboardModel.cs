﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.Entities
{
   public  class PMDashboardModel
    {
        public System.DateTime? DateOfFinancialStatement { get; set; }
        public String FormattedValue
        {
            get
            {
                return FinancialYearEndDate.HasValue
                    ? FinancialYearEndDate.Value.ToString("MM/dd/yy")
                    : ""; // return an empty string if SelectedDate is null
            }
        }
        public System.DateTime? FinancialYearEndDate
        {
            get { return DateOfFinancialStatement.HasValue ? DateOfFinancialStatement.Value.AddYears(1) : DateOfFinancialStatement; }

        }
        public System.DateTime? LastUpdatedOn { get; set; }
        public decimal CurrentAssets { get; set; }
        public decimal CurrentLiabilities { get; set; }
        public decimal CashReceivables { get; set; }
        public decimal WorkingCapital { get; set; }
        public decimal Revenue { get; set; }
        public decimal CashX360 { get; set; }
        public decimal NetIncome { get; set; }
        public decimal ShareholderEquity { get; set; }
        public decimal NetProfit { get; set; }
        public string CurrentRatio { get; set; }
        public string QuickRatio { get; set; }
        public string WorkingCapitalRevenue { get; set; }
        public string DaysCash { get; set; }
        public string ReturnEquity { get; set; }
        public string NetProfitMargin { get; set; }
    }
}
