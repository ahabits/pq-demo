﻿/*
Page Created Date:  08/08/2013
Created By: Siva Bommisetty
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FVGen3.Domain.Entities
{
    public class PrequalificationReportingData
    {
        [Key]
        public long PrequalificationRepDataId { get; set; }

        
        
        
        public int? ReportingType { get; set; }
        public long? ReportingDataId { get; set; }

        [ForeignKey("ClientId"), InverseProperty("PrequalificationReportingDataClientList")]
        public virtual Organizations Client { get; set; }
        public long? ClientId { get; set; }

        [ForeignKey("VendorId"), InverseProperty("PrequalificationReportingDataVendorList")]
        public virtual Organizations Vendor { get; set; }
        public long? VendorId { get; set; }

        [ForeignKey("Prequalification")]
        public long? PrequalificationId { get; set; }
        public virtual Prequalification Prequalification { get; set; }
    }
}
