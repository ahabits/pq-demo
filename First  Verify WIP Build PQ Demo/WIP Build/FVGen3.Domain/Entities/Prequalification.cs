﻿/*
Page Created Date:  05/18/2013
Created By: Suma
Purpose:
Version: 1.0
****************************************************
*/
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;


namespace FVGen3.Domain.Entities
{
    public class Prequalification
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long PrequalificationId { get; set; }

        
        public DateTime PrequalificationStart { get; set; }
        public DateTime PrequalificationFinish { get; set; }
        public DateTime PrequalificationCreate {get; set;}
        public DateTime? PrequalificationSubmit { get; set; }
        
        [ForeignKey("PrequalificationStatus")]
        public long PrequalificationStatusId { get; set; }
        public virtual PrequalificationStatus PrequalificationStatus { get; set; }

        [ForeignKey("ClientId")]//[ForeignKey("ClientId"),InverseProperty("ClientIdList")] 
        public virtual Organizations Client { get; set; }
        public long ClientId { get; set; }

        [ForeignKey("VendorId")]
        public virtual Organizations Vendor { get; set; }
        public long VendorId { get; set; }

        [ForeignKey("ClientTemplateId")]
        public virtual ClientTemplates ClientTemplates { get; set; }
        public Guid? ClientTemplateId { get; set; }

        [ForeignKey("UserIdAsCompleter")]
        public virtual SystemUsers UserIdCompleter { get; set; }
        public Guid? UserIdAsCompleter { get; set; }


        [ForeignKey("UserIdAsSigner")]
        public virtual SystemUsers UserIdSigner { get; set; } 
        public Guid? UserIdAsSigner { get; set; }

        [ForeignKey("UserIdLoggedIn")]
        public virtual SystemUsers LoggedInUserForPreQualification { get; set; }
        public Guid? UserIdLoggedIn { get; set; } 

        //public bool? OverridePayment { get; set; }
        //public decimal? OverirdeAmount { get; set; }
        public bool? PaymentReceived { get; set; }
        public bool? locked { get; set; }

        [ForeignKey("UnlockedBy")]
        public virtual SystemUsers UnlockedUser { get; set; }
        public Guid? UnlockedBy { get; set; }

        public DateTime? UnlockedDate { get; set; }

        public bool? RequestForMoreInfo { get; set; }//Rajesh on 1/11/2014        

   
        //Rajesh on 3/17/2014
        public decimal? TotalPaymentReceivedAmount { get; set; }
        //public DateTime? PaymentReceivedDateXXX { get; set; }
        //Ends<<<

        //Kiran on 7/30/2014 
        //public string PaymentTransactionIdXXX { get; set; }
        //public string TransactionMetaDataXXX { get; set; }
        // Ends<<
        // Mani on 8/18/2015
        public decimal? TotalInvoiceAmount { get; set; }
        public decimal? TotalAnnualFeeAmount { get; set; }
        public decimal? TotalAnnualFeeReceived { get; set; }
        // Ends<<
        public virtual List<PrequalificationUserInput> PrequalificationUserInputs { get; set; }
        public virtual List<PrequalificationSites> PrequalificationSites { get; set; }   // Rajesh on 07/24/2013
        public virtual List<PrequalificationCompletedSections> PrequalificationCompletedSections { get; set; } // Siva on 08/25/2013
        public virtual List<PrequalificationStatusChangeLog> PrequalificationStatusChangeLog { get; set; } // Siva on 08/25/2013
        public virtual List<VendorDetails> VendorDetails { get; set; } // Kiran on 04/22/2015

        public virtual List<FeeCapLog> FeeCapLog { get; set; }
        //public virtual List<CSIWebResponseDetails> CSIWebResponseDetails { get; set; } // Rajesh on 3/26/2014
        public virtual List<OrganizationsNotificationsEmailLogs> OrganizationsNotificationsEmailLogs { get; set; } // Rajesh on 3/26/2014
        public virtual List<PrequalificationComments> PrequalificationComments { get; set; } // Rajesh on 3/26/2014
        public virtual List<PrequalificationReportingData> PrequalificationReportingData { get; set; } // Rajesh on 3/26/2014
        public virtual List<PrequalificationEMRStatsYears> PrequalificationEMRStatsYears { get; set; } // Siva on 3/29/2014
        public virtual List<PrequalificationNotifications> PrequalificationNotifications { get; set; } // Siva on 3/29/2014

        public virtual List<PrequalificationTrainingAnnualFees> PrequalificationTrainingAnnualFees { get; set; }
        public virtual List<PrequalificationPayments> PrequalificationPayments { get; set; } // mani on 08/19/2015
        public virtual List<PrequalificationSubsection> PrequalificationSubsection { get; set; }

        public virtual List<PrequalificationClient> PrequalificationClientList { get; set; }
        [NotMapped]
        public PrequalificationClient PrequalificationClient { get
            {
                return PrequalificationClientList.Any() ? PrequalificationClientList.FirstOrDefault() : new PrequalificationClient();
            } }
        public virtual List<PrequalificationSectionsToDisplay> PrequalificationSectionsToDisplay { get; set; }
        public string Comments { get; set; }
        public bool BlockClientDocsSelection { get; set; }

        public int? InvitationFrom { get; set; }
        [ForeignKey("InvitationSentByUser")]
        public virtual SystemUsers InvitedByUser { get; set; }
        public Guid? InvitationSentByUser { get; set; }
        //public long AdminFinalStatus { get { return PrequalificationSites == null || PrequalificationSites.Count == 0 ? PrequalificationStatusId : (PrequalificationSites.Where(r=>r.PQSiteStatus != null).Select(r => r.SiteStatusId).Distinct().Count() > 1 ? -1 : PrequalificationSites.FirstOrDefault().SiteStatusId); } }
        public Guid? InvitationId { get; set; }
    }
}
