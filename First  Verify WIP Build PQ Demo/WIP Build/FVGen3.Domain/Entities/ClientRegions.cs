﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FVGen3.Domain.Entities
{
 public   class ClientRegions
    {
        [Key]
        public int id { get; set; }
        [ForeignKey("Organizations")]
        public long Clientid { get; set; }
        [ForeignKey("Regions")]
        public int Regionid { get; set; }
       
        public virtual Regions Regions { get; set; }
        public virtual Organizations Organizations { get; set; }
    }
}
