﻿/*
Page Created Date:  06/11/2013
Created By: Siva Bommisetty
Purpose:
Version: 1.0
****************************************************
*/

using System.ComponentModel.DataAnnotations;
using Resources;

namespace FVGen3.Domain.Entities
{
    public class LocalForgotPasswordModel
    {
        [Required]
        [Display(Name = "Email ID")]
        [RegularExpression(LocalConstants.EMAIL_FORMAT, ErrorMessageResourceName = "Common_Email_Invalid_ErrorMessage", ErrorMessageResourceType = typeof(Resources.Resources))]
        public string EmailId { get; set; }
    }
}
