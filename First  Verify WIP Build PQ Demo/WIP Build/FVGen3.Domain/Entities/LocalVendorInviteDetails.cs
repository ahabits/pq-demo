﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.Entities
{
    public class LocalVendorInviteDetails
    {
        public string InvitationSentBy { get; set; }
        public string InvitationSentByUser { get; set; } 
        public int? InvitationFrom { get; set; }
        public Guid? InvitationSentByUserId { get; set; }
        public Guid? InvitationId { get; set; }
    }
}
