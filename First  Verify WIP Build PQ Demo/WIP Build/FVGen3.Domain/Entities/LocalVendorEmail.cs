﻿
using System;
using System.Web.Mvc;

namespace FVGen3.Domain.Entities
{
    public class LocalVendorEmail
    {
        public string ToAddress { get; set; }
        public string CCAddress { get; set; }
        public string BCCAddress { get; set; }
        [AllowHtml]
        public string Body { get; set; }
        public string VendorIds { get; set; }
        public long ClientId { get; set; }
        public string PQIds { get; set; }
        public Guid UserId { get; set; }
        public bool EmailStatus { get; set; }
        public bool BCCSendEmailToVendorContacts { get; set; }
    }
}
