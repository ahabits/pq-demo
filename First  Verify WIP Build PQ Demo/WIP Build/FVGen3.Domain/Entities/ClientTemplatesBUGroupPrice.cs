﻿/*
Page Created Date:  02/19/2015
Created By: Siva Bommisetty
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FVGen3.Domain.Entities
{
    public class ClientTemplatesBUGroupPrice
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long ClientTemplateBUGroupId { get; set; }

        [ForeignKey("ClientTemplates")]
        public Guid? ClientTemplateID { get; set; }
        public virtual ClientTemplates ClientTemplates { get; set; }

        public int? GroupPriceType { get; set; }
        public decimal? GroupPrice { get; set; }


        public int? GroupingFor { get; set; } //  0=Prequalification Template, 1=Annual Fee, 2=Training Template

        public virtual List<ClientTemplatesForBU> ClientTemplatesForBU { get; set; }

        public virtual List<PrequalificationTrainingAnnualFees> PrequalificationTrainingAnnualFees { get; set; }

        public virtual List<QuizPaymentsItems> QuizPaymentsItems { get; set; }

    }
}
