﻿
/*
Page Created Date:  09/04/2013
Created By: Suma
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FVGen3.Domain.Entities
{
    public class OrganizationsNotificationsEmailLogs
    {
        [Key]
        public long EmailRecordId { get; set; }
        //public long NotificationId{get;,set;}

        public DateTime EmailSentDateTime { get; set; }
        public string Comments { get; set; }

        [ForeignKey("SystemUsers")]
        public Guid MailSentBy { get; set; }
        public virtual SystemUsers SystemUsers { get; set; }

        //Dec 4th
        public int NotificationStatus { get; set; }
        public int? ReadByAdmin { get; set; }
        public int? ReadByClient { get; set; }
        public int? ReadByVendor { get; set; }

        //Siva on Apr 7th
        public int? EmailTemplateId { get; set; }
        public string EmailSentTo { get; set; }
        public string EmailSentToNames { get; set; }

        //[ForeignKey("NotificationId"),InverseProperty("OrganizationsNotificationsSentNotificationIDList")] 
        [ForeignKey("OrganizationsNotificationsSent")]//Rajesh  on 9-30-2013        
        public long NotificationId { get; set; }
        public virtual OrganizationsNotificationsSent OrganizationsNotificationsSent { get; set; }

        public int NotificationType { get; set; }
        public int NotificationNo { get; set; }

        [ForeignKey("Document")]
        public Guid? DocumentId { get; set; }
        public virtual Document Document { get; set; }


        public DateTime? ExpiringDate { get; set; }

        [ForeignKey("VendorId"), InverseProperty("OrganizationsNotificationsSentVendorList")]
        public virtual Organizations Vendor { get; set; }
        public long? VendorId { get; set; }

        [ForeignKey("Prequalification")]
        public long? PrequalificationId { get; set; }
        public virtual Prequalification Prequalification { get; set; }

        public string RecordStatus { get; set; } // Kiran on 04/06/2015

        public DateTime? CommentsUpdateDateTime { get; set; } // kiran on 09/24/2015 for FV-210

        public string EmailCc { get; set; }
        public string EmailBcc { get; set; }
    }
}
