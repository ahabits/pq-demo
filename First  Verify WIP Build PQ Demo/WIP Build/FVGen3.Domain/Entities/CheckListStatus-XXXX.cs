﻿/*
Page Created Date:  11/04/2014
Created By: Kiran Talluri
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FVGen3.Domain.Entities
{
    public class CheckListStatus
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long CheckListStatusID { get; set; }
        public string StatusDescription { get; set; }

        public virtual List<SystemUsersOrganizationsEmpQuizChecklist> SystemUsersOrganizationsEmpQuizChecklist { get; set; }
    }
}
