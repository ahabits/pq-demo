﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FVGen3.Domain.Entities
{
   public  class SystemLogs
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public  Guid Id { get; set; }
        public DateTime Date { get; set; }
        public string Message { get; set; }
        public string Screen { get; set; }
        public string Params { get; set; }
        public string Level { get; set; }
    }
}
