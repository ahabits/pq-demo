﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.Entities
{
    public class LocalEmployeesListForAdminView
    {
        public long ClientId { get; set; }
        public long VendorId { get; set; }
    }
}
