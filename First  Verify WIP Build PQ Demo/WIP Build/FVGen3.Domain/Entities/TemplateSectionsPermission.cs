﻿/*
Page Created Date:  01/20/2014
Created By: Rajesh
Purpose:
Version: 1.0
****************************************************
*/

using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;

namespace FVGen3.Domain.Entities
{
    public class TemplateSectionsPermission
    {
        
        //>>DV:SB,KT
        [Key]
        public long SectionsPermissionId { get; set; }

        [ForeignKey("TemplateSections")]        
        public long TemplateSectionID { get; set; }
        public virtual TemplateSections TemplateSections { get; set; }
        //Foreign key to Contact
        public int VisibleTo { get; set; }

        //<<DV:SB,KT
        public virtual List<TemplateSubSectionsPermissions> TemplateSubSectionsPermissions { get; set; } //Rajesh on 2/11/2014
    }
}