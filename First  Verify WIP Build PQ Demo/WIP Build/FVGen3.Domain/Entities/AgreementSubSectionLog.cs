﻿

using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace FVGen3.Domain.Entities
{
    public class AgreementSubSectionLog
    {
        public long Id { get; set; }
        public long PQId { get; set; }
        public DateTime SentDate { get; set; }
        [ForeignKey("SystemUsers")]
        public Guid SentBy { get; set; }
        public virtual SystemUsers SystemUsers { get; set; } 
        public string ToAddress { get; set; }
        public string CCAddress { get; set; }
        [ForeignKey("AgreementSubSectionConfiguration")]
        public Guid AgreementSubSecId { get; set; }
        public virtual AgreementSubSectionConfiguration AgreementSubSectionConfiguration { get; set; }
    }
}
