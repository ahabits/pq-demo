﻿
//Page Created Date: 04/29/2015
//Created By: Kiran Talluri
//Purpose:
//Version: 1.0

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FVGen3.Domain.Entities
{
    public class QuizPaymentsItems
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long QuizPaymentsItemsId { get; set; }

        [ForeignKey("QuizPayments")]
        public long? QuizPaymentId { get; set; }

        [ForeignKey("ClientTemplatesBUGroupPrice")]
        public long? AsOfClientTemplateBUGroupId { get; set; }

        [ForeignKey("ClientTemplatesForBU")]
        public long? ClientTemplateForBUId { get; set; }
        public virtual ClientTemplatesForBU ClientTemplatesForBU { get; set; }

        [ForeignKey("ClientTemplatesForBUSites")]
        public long? ClientTemplateForBUSitesId { get; set; }
        public virtual ClientTemplatesForBUSites ClientTemplatesForBUSites { get; set; }
        public virtual QuizPayments QuizPayments { get; set; }
        public virtual ClientTemplatesBUGroupPrice ClientTemplatesBUGroupPrice { get; set; }   
    }
}
