﻿/*
Page Created Date:  11/04/2014
Created By: Kiran Talluri
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.Entities
{
    public class LocalAddEmployeeCheckLists
    {
        public long SysUserOrgQuizId { get; set; } // Kiran on 12/18/2014
        public List<CheckBoxesControlForEmpCheckLists> checkLists { get; set; }
    }
    public class CheckBoxesControlForEmpCheckLists
    {
        public string id { get; set; }
        public string Text { get; set; }
        public bool Status { get; set; }
    } 
}
