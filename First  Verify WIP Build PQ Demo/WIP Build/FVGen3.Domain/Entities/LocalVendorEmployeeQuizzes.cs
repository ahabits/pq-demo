﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.Entities
{
    public class LocalVendorEmployeeQuizzes
    {
        public Guid UserId { get; set; }
        public long OrganizationId { get; set; }
        public string EmployeeName { get; set; }
        public string EmpEmail { get; set; }
        public string EmpPassword { get; set; }
    }
}
