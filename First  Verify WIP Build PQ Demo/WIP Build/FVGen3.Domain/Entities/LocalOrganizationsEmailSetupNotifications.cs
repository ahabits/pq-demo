﻿/*
 *****************************************************
Page Created Date:  22/06/2013
Created By: Kiran Talluri
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace FVGen3.Domain.Entities
{
    public class LocalOrganizationsEmailSetupNotifications
    {
        public long OrganizationId { get; set; }                
        public int SetupType { get; set; } 

        [Required(ErrorMessage="*")]       
        public string SetupTypeId { get; set; }
     
        [Required(ErrorMessageResourceName = "ErrorMessage_Required_NumberOfDays", ErrorMessageResourceType = typeof(Resources.Resources))]
        [RegularExpression("[0-9]{1,2}", ErrorMessageResourceName = "ErrorMessage_99Days", ErrorMessageResourceType = typeof(Resources.Resources))] // sumanth on 11/2/2015 for FVOS-110 to display alert proper

        
        public int? FirstNotification { get; set; }

        [RegularExpression("[0-9]{1,2}", ErrorMessageResourceName = "ErrorMessage_99Days", ErrorMessageResourceType = typeof(Resources.Resources))]// sumanth on 11/2/2015 for FVOS-110 to display alert proper
        public int? SecondNotification { get; set; }

        [RegularExpression("[0-9]{1,2}", ErrorMessageResourceName = "ErrorMessage_99Days", ErrorMessageResourceType = typeof(Resources.Resources))]// sumanth on 11/2/2015 for FVOS-110 to display alert proper
        public int? ThirdNotification { get; set; }

        public bool SendNotificationToClient { get; set; }
        public bool SendNotificationToVendor { get; set; }
        public bool SendNotificationToFirstVerify { get; set; }
        public bool SendNotificationToSiteContacts { get; set; }
        public bool isInsert { get; set; }

        [Required(ErrorMessage = "*")]
        public Guid PrimaryUserId { get; set; }
        public long OrgEmailSetupId { get; set; }

        //Kiran on 11/27/2013
        public int? EmailTemplateType { get; set; }
        [Required(ErrorMessage = "*")]
        public int? EmailTemplateID { get; set; }
        public long ClientId { get; set; }
        // Ends<<<

        // Kiran on 9/22/2014
        public bool AddClientRecipients { get; set; }
        public List<string> AdditionalClientRecipients { get; set; }
        // Ends<<<

    }
}
