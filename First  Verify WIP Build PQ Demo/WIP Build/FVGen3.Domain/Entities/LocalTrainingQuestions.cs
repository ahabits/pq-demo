﻿/*
*****************************************************
Page Created Date:  10/17/2014
Created By: Kiran Talluri
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.Entities
{
    public class LocalTrainingQuestions
    {
        public long HeaderId { get; set; }
        public long FooterId { get; set; }
        public long SectionId { get; set; }
        public string HeaderContent { get; set; }
        public string FooterContent { get; set; }
        public int? SubSectionType { get; set; }
        public string SubSectionName { get; set; }
        public int NoOfColumns { get; set; }
        public bool Visible { get; set; }
        public int PermissionType { get; set; }
        public int TemplateStatus { get; set; }
        public int TemplateType { get; set; }
        public string TemplateName { get; set; }
        public string SectionName { get; set; }
        public long SubSectionID { get; set; }
        public long QuestionID { get; set; }
        public bool isEdit { get; set; }

        public List<LocalQuestions> LocalQuestions { set; get; }
    }
    public class LocalQuestions
    {
        public long QuestionId { get; set; }
        public long QuestionBankId { get; set; }
    }
}
