﻿/*
Page Created Date:  06/11/2013
Created By: Siva Bommisetty
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FVGen3.Domain.Entities
{
    public class SystemUserLogs
    {
        [Key]
        public int SystemLogId { get; set; }
        public System.Nullable<int> SystemActionId { get; set; }
        public string SystemTable { get; set; }
        public System.Nullable<int> SystemRow { get; set; }
        public System.Nullable<DateTime> SystemActionWhen { get; set; }

        //    >>DV:SB,KT
        //Foreign key to Contact
        [ForeignKey("SystemUsers")]
        public Guid SystemUserId { get; set; }
        public virtual SystemUsers SystemUsers { get; set; }
        //<<DV:SB,KT
    }
}
