﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FVGen3.Domain.Entities
{
    public class QuizzesCompletedSections
    {
        [Key]
        public long EmpQuizCompletedId { get; set; }

        // Kiran on 12/18/2014

        //[ForeignKey("EmployeeQuizzes")]
        //public long EmpQuizzesId { get; set; }
        //public virtual EmployeeQuizzes EmployeeQuizzes { get; set; }

        [ForeignKey("TemplateSections")]
        public long TemplateSectionId { get; set; }
        public virtual TemplateSections TemplateSections { get; set; }

        [ForeignKey("SystemUsersOrganizationsQuizzes")]
        public long SysUserOrgQuizId { get; set; }
        public virtual SystemUsersOrganizationsQuizzes SystemUsersOrganizationsQuizzes { get; set; }
        // Ends<<<
    }
}
