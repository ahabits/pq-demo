﻿/*
Page Created Date:  06/11/2013
Created By: Siva Bommisetty
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace FVGen3.Domain.Entities
{
    public class LocalAddClientBusinessUnit
    {
        public long ClientId { get; set; }
        [Required(ErrorMessage = "*")]
        public string BusinessUnitName { get; set; }
    }
}
