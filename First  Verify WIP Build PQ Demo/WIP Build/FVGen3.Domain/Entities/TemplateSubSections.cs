﻿/*
 *****************************************************
Page Created Date:  06/06/2013
Created By: Siva Bommisetty
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FVGen3.Domain.Entities
{
    public class TemplateSubSections
    {
        [Key]
        public long SubSectionID { get; set; }

        public string SubSectionName { get; set; }
        public int DisplayOrder { get; set; }
        public bool? Visible { get; set; }
        public bool? SubHeader { get; set; }
        public string SectionNotes { get; set; }
        public int? NoOfColumns { get; set; }
        public bool? AttachmentExist { get; set; }
        public int? SubSectionType { get; set; }
        //public string PreDefineCondition { get; set; }
        //public string DynamicPopupView { get; set; }
        public string SubSectionTypeCondition { get; set; }
        [ForeignKey("TemplateSections")]
        public long TemplateSectionID { get; set; }
        public virtual TemplateSections TemplateSections { get; set; }


        //Rajesh on 10/24/2013
        [ForeignKey("Organizations")]
        public long? ClientId { get; set; }
        public virtual Organizations Organizations { get; set; }


        public bool? NotificationExist { get; set; }
        public string MetaJSON { get; set; }
        public virtual List<Questions> Questions { get; set; }
        public virtual List<PrequalificationSubsection> PrequalificationSubsection { get; set; }
        public virtual List<TemplateSubSectionsNotifications> TemplateSubSectionsNotifications { get; set; }
        public virtual List<AgreementSubSectionConfiguration> AgreementSubSectionConfiguration { get; set; }
        public virtual List<TemplateSubSectionsPermissions> TemplateSubSectionsPermissions { get; set; } //Rajesh on 2/11/2014
    }
}
