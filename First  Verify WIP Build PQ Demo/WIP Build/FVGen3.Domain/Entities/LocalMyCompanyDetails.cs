﻿/*
*****************************************************
Page Created Date:  05/01/2013
Created By: Siva Bommisetty
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FVGen3.Domain.Entities
{
    public class LocalMyCompanyDetails
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long OrganizationID { get; set; }

        [Required(ErrorMessage = "*")]
        public string Name { get; set; }

        [Required(ErrorMessage = "*")]
        public string Address1 { get; set; }

        [MaxLength(200, ErrorMessageResourceName = "ErrorMessage_Exceed200", ErrorMessageResourceType = typeof(Resources.Resources))]
        public string Address2 { get; set; }

        // Kiran on 9/19/2014
        [MaxLength(200, ErrorMessageResourceName = "ErrorMessage_Exceed200", ErrorMessageResourceType = typeof(Resources.Resources))]
        public string Address3 { get; set; }
        // Ends<<<

        [Required(ErrorMessage = "*")]
        public string City { get; set; }

        [Required(ErrorMessage = "*")]
        public string State { get; set; }
        //[Required(ErrorMessage = "*")]
        public int? FinancialMonth { get; set; }
        //[Required(ErrorMessage = "*")]
        public int? FinancialDate { get; set; }

        [Required(ErrorMessage = "*")]
        public int CountryType { get; set; }

        [Required(ErrorMessage = "*")]
        public string Zip { get; set; }

        [Required(ErrorMessage = "*")]
        public string Country { get; set; }

        [Required(ErrorMessage = "*")]
        //[StringLength(50, ErrorMessage = "Not a valid phone number", MinimumLength = 14)]
        public string PhoneNumber { get; set; }

        public string FaxNumber { get; set; }
        public string WebsiteURL { get; set; }

        //[Required(ErrorMessage = "*")]
        public string FederalIDNumber { get; set; }

        [RegularExpression(@"^-*[0-9,\.]+$", ErrorMessageResourceName = "ErrorMessage_PhoneNumber", ErrorMessageResourceType = typeof(Resources.Resources))]//Phani on 23/07/2015
        public string ExtNumber { get; set; } // Kiran on 8/26/2014

        // Kiran on 12/24/2014
        [Required(ErrorMessage = "*")]
        //[StringLength(50, ErrorMessage = "Invalid Tax Id", MinimumLength = 10)] // Kiran on 12/10/2014
        [RegularExpression(@"[0-9]{3}[-][0-9]{2}[-][0-9]{4}|[0-9]{2}[-][0-9]{7}", ErrorMessageResourceName = "Common_InvalidTaxID", ErrorMessageResourceType = typeof(Resources.Resources))] //Kiran on 8/20/2014
        public string TaxID { get; set; }
        //Ends<<<
    }
}
