﻿/*
Page Created Date:  05/01/2013
Created By: Siva Bommisetty
Purpose:
Version: 1.0
****************************************************
*/

using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;

namespace FVGen3.Domain.Entities
{
    public class LocalTemplateSubSectionsNotifications
    {
        public LocalTemplateSubSectionsNotifications()
        {
            ListOfClientUsersForNotificationsTosent = new List<SelectListItem>();
        }
        [Required(ErrorMessage = "*")]
        public int EmaiTemplateId { get; set; }
        [Required(ErrorMessage="*")]
        public string AdminUserId { get; set; }
        [Required(ErrorMessage = "*")]
        public string ClientUserId { get; set; }
        public long TemplateSubSectionID { get; set; }
        public long TemplateSubSectionNotificationId { get; set; }

        public bool ForAdmin { get; set; }
        public bool ForClient { get; set; }

        public int SenderType { get; set; }
        public string SenderEmailId { get; set; }

        // kiran on 9/19/2014
        public List<SelectListItem> ListOfAdminUsersForNotificationsTosent { get; set; }

        [Required(ErrorMessage = "*")]
        public List<string> AdminUsers { get; set; }

        public List<SelectListItem> ListOfClientUsersForNotificationsTosent { get; set; }

        [Required(ErrorMessage = "*")]
        public List<string> ClientUsers { get; set; }

        public int RecipientUserType { get; set; }
        // Ends<<<
    }
}