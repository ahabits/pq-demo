﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.Entities
{
     public class LocalTemplateDiscountPrices
    {

       // public long TemplateDiscountId { get; set; }
       
        public int DiscountType { get; set; }
        public int MinNoOfPrequalification { get; set; }
        public int MaxNoOfPrequalification { get; set; }
          public Decimal DiscountedPrice { get; set; }

    }
}
