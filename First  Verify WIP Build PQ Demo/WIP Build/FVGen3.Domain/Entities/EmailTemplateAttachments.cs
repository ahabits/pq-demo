﻿/*
Page Created Date:  05/11/2013
Created By: Siva Bommisetty
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FVGen3.Domain.Entities
{
    public class EmailTemplateAttachments
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid AttachmentId { get; set; }
        
        public string FileName { get; set; }
        public string FileExtension { get; set; }
        public System.Nullable<Guid> UploadedByUserId { get; set; }
        public System.Nullable<DateTime> InsertDateTime { get; set; }
        public System.Nullable<DateTime> UpdateDateTime { get; set; }

        [ForeignKey("EmailTemplates")]
        public System.Nullable<int> TemplateID { get; set; }
        public virtual EmailTemplates EmailTemplates { get; set; }
    }
}
