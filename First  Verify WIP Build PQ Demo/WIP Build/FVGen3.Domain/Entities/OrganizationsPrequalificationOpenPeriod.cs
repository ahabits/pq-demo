﻿/*
Page Created Date:  03/15/2014
Created By: Rajesh Pendela
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FVGen3.Domain.Entities
{
    public class OrganizationsPrequalificationOpenPeriod
    {
       [Key]

       public long OrgPrequalificationPeriodId { get; set; }

       public DateTime PrequalificationPeriodStart { get; set; }
       public DateTime PrequalificationPeriodClose { get; set; }
       public int PeriodStatus { get; set; }
       public decimal TotalPaymentRecieved { get; set; }

       //Siva on 29th Mar 2014
       [ForeignKey("VendorId")]
       public virtual Organizations Vendor { get; set; }
       public long VendorId { get; set; }
        //End of Siva on 29th Mar 2014

    }
}
