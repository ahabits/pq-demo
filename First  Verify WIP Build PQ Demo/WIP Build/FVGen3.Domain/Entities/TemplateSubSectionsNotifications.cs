﻿/*
Page Created Date:  05/01/2013
Created By: Siva Bommisetty
Purpose:
Version: 1.0
****************************************************
*/

using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;

namespace FVGen3.Domain.Entities
{
    public class TemplateSubSectionsNotifications
    {
        
        //>>DV:SB,KT
        [Key]
        public long TemplateSubSectionNotificationId { get; set; }

        public int EmaiTemplateId { get; set; }

        [ForeignKey("TemplateSubSections")]
        public long TemplateSubSectionID { get; set; }
        public virtual TemplateSubSections TemplateSubSections { get; set; }

        // Kiran on 9/23/2014
        [ForeignKey("RecipientUserId")]
        public virtual SystemUsers AdminOrClientUser { get; set; }
        public Guid? RecipientUserId { get; set; }

        //[ForeignKey("ClientUserId")]
        //public virtual SystemUsers ClientUser { get; set; }
        //public Guid? ClientUserId { get; set; }

        public int RecipientUserType { get; set; }
        // Ends<<<

        public int SenderType { get; set; }
        public string SenderEmailId { get; set; }

        public bool NotifyUserDisabled { get; set; } // Kiran on 9/27/2014

        public virtual List<PrequalificationNotifications> PrequalificationNotifications { get; set; } // Kiran on 9/27/2014
    }
}