﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.Entities
{
    public class QuizDetails
    {
        public Guid UserId { get; set; }
        public long? VendorId { get; set; }
        public long? ClientId { get; set; }
        public bool? WaiverForm { get; set; }
        public string WaiverFormContent { get; set; }
        public DateTime? QuizSubmittedDate { get; set; } 
    }
}