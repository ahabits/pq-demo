﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FVGen3.Domain.Entities
{
    public class SysUserOrgQuizHistory
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        [ForeignKey("SystemUsersOrganizationsQuizzes")]
        public long SysUserOrgQuizId { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime QuizStart { get; set; }
        public DateTime QuizSubmitted { get; set; }
        public DateTime QuizExpiration { get; set; }
        public bool QuizResult { get; set; }


        public virtual SystemUsersOrganizationsQuizzes SystemUsersOrganizationsQuizzes { get; set; }

    }
}
