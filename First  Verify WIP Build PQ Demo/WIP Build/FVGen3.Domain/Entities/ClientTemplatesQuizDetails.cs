﻿/*
Page Created Date:  27/10/2014
Created By: Kiran Talluri
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FVGen3.Domain.Entities
{
    public class ClientTemplatesQuizDetails
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long ClientQuizDetailsID { get; set; }

        [ForeignKey("ClientTemplateID")]
        public virtual ClientTemplates ClientTemplatesForQuizzes { get; set; }
        public Guid ClientTemplateID { get; set; }

        public decimal? QuizPassRate { get; set; }
        public bool? QuizRetake { get; set; }
        public decimal? QuziRetakeFee { get; set; }
        public DateTime? FeeUpdateDate { get; set; }
        public int? NumberOfAttempts { get; set; }


        public int? PeriodLength { get; set; }
        public string PeriodLengthUnit { get; set; }
        public bool? WaiverForm { get; set; }
        public string WaiverFormContent { get; set; }

    }
}
