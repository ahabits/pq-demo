﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.Entities
{
    public class Migration
    {
        public long ClientId { get; set; }
        public Guid sourceTemplateId { get; set; }
        public Guid destinationTemplateId { get; set; }

    }
}
