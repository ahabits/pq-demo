﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FVGen3.Domain.Entities
{
    public class SystemUsersOrganizationsQuizzes
    {
        [Key]
        public long SysUserOrgQuizId { get; set; }

        [ForeignKey("SysUserOrgId")]
        public virtual SystemUsersOrganizations SystemUsersOrganizations { get; set; }
        public long? SysUserOrgId { get; set; }

        [ForeignKey("ClientTemplateId")]
        public virtual ClientTemplates ClientTemplates { get; set; }
        public Guid? ClientTemplateId { get; set; }

        public bool? QuizRemoved { get; set; } // Kiran on 11/19/2014

        // Kiran on 12/18/2014 changed the design for EmployeeCheckLists

        public DateTime? QuizDateStart { get; set; }
        public DateTime? QuizDateSubmitted { get; set; }
        public bool? QuizResult { get; set; }
        public DateTime? QuizExpirationDate { get; set; }
        public int? NoOfAttempts { get; set; }
        public bool? AllowRetakeTraining { get; set; }
        public decimal? RetakeFeeAmount { get; set; }

        [ForeignKey("ClientId")]//[ForeignKey("ClientId"),InverseProperty("ClientIdList")] 
        public virtual Organizations Client { get; set; }
        public long? ClientId { get; set; }

        [ForeignKey("VendorId")]
        public virtual Organizations Vendor { get; set; }
        public long? VendorId { get; set; }

        public bool? PreviousLastQuizTakenStatus { get; set; }
        public DateTime? PreviousLastQuizSubmittedDate { get; set; }

        public virtual List<EmployeeQuizUserInput> EmployeeQuizUserInput { get; set; }
        public virtual List<QuizzesCompletedSections> QuizzesCompletedSections { get; set; }
        // Kiran on 02/18/2015
        //public virtual List<SystemUsersOrganizationsEmpQuizChecklist> SystemUsersOrganizationsEmpQuizChecklist { get; set; }
        public virtual List<SystemUsersOrganizationsQuizzesCategories> SystemUsersOrganizationsQuizzesCategories { get; set; }
        public virtual List<QuizPayments> QuizPayments { get; set; }
        public virtual List<SysUserOrgQuizHistory> SysUserOrgQuizHistory { get; set; }
        public virtual List<QuizWaiverFormCompletionLog> QuizWaiverFormCompletionLog { get; set; }

        //public virtual List<EmployeeQuizzes> EmployeeQuizzes { get; set; } //Siva on 29th Mar 2014
        // Ends<<<
    }
}
