﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FVGen3.Domain.Entities
{
    public class SystemSideMenus
    {
        [Key]
        public long SystemSideMenuId { get; set; }

        public string SystemSideMenuName { get; set; }
        public string SysMenuNameSM { get; set; }
        public string ActionNameSM { get; set; }
        public string ControllerNameSM { get; set; }
        

        [ForeignKey("SystemHeaderMenus")]
        public long? SystemHeaderMenuId { get; set; }
        public virtual SystemHeaderMenus SystemHeaderMenus { get; set; }

        public virtual List<SystemViews> SystemViews { get; set; }
        public int? DisplayOrder { get; set; }

    }
}
