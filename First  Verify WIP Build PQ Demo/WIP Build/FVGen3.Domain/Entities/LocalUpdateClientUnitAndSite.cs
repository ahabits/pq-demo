﻿/*
 *****************************************************
Page Created Date:  07/19/2013
Created By: Kiran Talluri
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace FVGen3.Domain.Entities
{
    public class LocalUpdateClientUnitAndSite
    {
        public Guid UserId { get; set; }
        public long unitId { get; set; }
        // Added on 07/24/2013 >>>
        public long clientId { get; set; }
        //public string unitLocation { get; set; }
        // <<<<
        [Required(ErrorMessage = "*")]
        public long currentSiteId { get; set; }
        public long oldSiteId { get; set; }
        public long selectedBusUnitId { get; set; }
    }
}
