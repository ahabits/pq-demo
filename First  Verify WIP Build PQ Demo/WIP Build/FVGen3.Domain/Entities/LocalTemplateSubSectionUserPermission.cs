﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace FVGen3.Domain.Entities
{
    public class LocalTemplateSubSectionUserPermission
    {
        public List<UserPermissions> UserPermissions { get; set; }
        public long SubSectionId { get; set; }
        public long ClientId { get; set; }
    }
    public class UserPermissions
    {
        public string ClientName { get; set; }
        public long ClientId { get; set; }
        public string UserName { get; set; }
        public Guid UserId { get; set; }
        public bool Read { get; set; }
        public bool Edit { get; set; }
    }
}
