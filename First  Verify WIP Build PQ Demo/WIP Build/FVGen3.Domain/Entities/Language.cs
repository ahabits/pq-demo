﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.Entities
{
    public class Language
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public long LanguageId { get; set; }
        public string LanguageName { get; set; }
        public string LanguageCode { get; set; }

        public virtual List<CountryLanguages> CountryLanguages { get; set; }
        public virtual List<Templates> Templates { get; set; }
    }
}
