﻿/*
 *****************************************************
Page Created Date:  05/01/2013
Created By: Siva Bommisetty
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace FVGen3.Domain.Entities
{
    public class TemplateSections
    {
        [Key]
        public long TemplateSectionID { get; set; }

        public int DisplayOrder { get; set; }
        public bool Visible { get; set; }
        public string SectionName { get; set; }
        public int? TemplateSectionType { get; set; }

        [ForeignKey("Templates")]
        public Guid TemplateID { get; set; }
        public virtual Templates Templates { get; set; }
        public bool? VisibleToClient { get; set; }
        public bool? HideFromPrequalification { get; set; } 

        public virtual List<TemplateSectionsHeaderFooter> TemplateSectionsHeaderFooter { get; set; }
        public virtual List<TemplateSubSections> TemplateSubSections { get; set; }
        public virtual List<PrequalificationCompletedSections> PrequalificationCompletedSections { get; set; }
        public virtual List<ClientTemplateProfiles> ClientTemplateProfiles { get; set; }
        public virtual List<TemplateSectionsPermission> TemplateSectionsPermission { get; set; }

    }
}
