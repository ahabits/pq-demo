﻿/*
 *****************************************************
Page Created Date:  06/05/2013
Created By: Kiran Talluri
Purpose:
Version: 1.0
****************************************************
Date: 7/5/2013 Added invitation Id property
****************************************************
*/

using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Collections.Generic;

namespace FVGen3.Domain.Entities
{
    public class LocalVendorRegistrationModel
    {

        public Guid ApplicationId { get; set; }

        [Key]
        [Display(Name = "User Id")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid UserId { get; set; }

        public long ClientID { get; set; }

        [Required(ErrorMessage = "*")]
        [StringLength(200, ErrorMessageResourceName = "ErrorMessage_Exceed200", ErrorMessageResourceType = typeof(Resources.Resources))]
        public string Name { get; set; }

        [Required(ErrorMessage = "*")]
        [StringLength(200, ErrorMessageResourceName = "ErrorMessage_Exceed200", ErrorMessageResourceType = typeof(Resources.Resources))]
        public string Address1 { get; set; }

        [MaxLength(200, ErrorMessageResourceName = "ErrorMessage_Exceed200", ErrorMessageResourceType = typeof(Resources.Resources))]
        public string Address2 { get; set; }

        // Kiran on 9/19/2014
        [MaxLength(200, ErrorMessageResourceName = "ErrorMessage_Exceed200", ErrorMessageResourceType = typeof(Resources.Resources))]
        public string Address3 { get; set; }
        // Ends<<

        [StringLength(200, ErrorMessageResourceName = "ErrorMessage_Exceed200", ErrorMessageResourceType = typeof(Resources.Resources))]
        [Required(ErrorMessage = "*")]
        public string City { get; set; }

        [Required(ErrorMessage = "*")]
        [StringLength(200, ErrorMessageResourceName = "ErrorMessage_Exceed200", ErrorMessageResourceType = typeof(Resources.Resources))]
        public string State { get; set; }

        [Required(ErrorMessage = "*")]
        [StringLength(20, ErrorMessageResourceName = "ErrorMessage_Exceed20", ErrorMessageResourceType = typeof(Resources.Resources))]
        public string Zip { get; set; }

        [Required(ErrorMessage = "*")]
        [StringLength(100, ErrorMessageResourceName = "ErrorMessage_Exceed100", ErrorMessageResourceType = typeof(Resources.Resources))]
        public string Country { get; set; }

        [Required(ErrorMessageResourceName = "ErrorMessage_Required_PhoneNumber", ErrorMessageResourceType = typeof(Resources.Resources))]
        //[StringLength(100, ErrorMessageResourceName = "VendorRegistrationModel_InvalidNumber", MinimumLength = 14, ErrorMessageResourceType = typeof(Resources.Resources))]
        public string PhoneNumber { get; set; }

        public string FaxNumber { get; set; }
        public string WebsiteURL { get; set; }
        public string FederalIDNumber { get; set; }

        [Required(ErrorMessage = "*")]
        [StringLength(100, ErrorMessageResourceName = "ErrorMessage_Exceed100", ErrorMessageResourceType = typeof(Resources.Resources))]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "*")]
        [StringLength(100, ErrorMessageResourceName = "ErrorMessage_Exceed100", ErrorMessageResourceType = typeof(Resources.Resources))]
        public string LastName { get; set; }

        [Required(ErrorMessageResourceName = "ErrorMessage_Required_MobileNumber", ErrorMessageResourceType = typeof(Resources.Resources))]
        //[StringLength(50, ErrorMessageResourceName = "VendorRegistrationModel_InvalidNumber", MinimumLength = 14, ErrorMessageResourceType = typeof(Resources.Resources))]
        public string MobileNumber { get; set; }

        [Required(ErrorMessage = "*")]
        [StringLength(51, ErrorMessageResourceName = "ErrorMessage_Exceed51", ErrorMessageResourceType = typeof(Resources.Resources))]
        public string ContactTitle { get; set; }

        [Required(ErrorMessageResourceName = "ErrorMessage_Required_Email", ErrorMessageResourceType = typeof(Resources.Resources))]
        [Display(Name = "Email Id")]
        [StringLength(256, ErrorMessageResourceName = "ErrorMessage_Exceed256", ErrorMessageResourceType = typeof(Resources.Resources))]
        //[RegularExpression(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$", ErrorMessageResourceName = "VendorRegistrationModel_InvalidEmail", ErrorMessageResourceType = typeof(Resources.Resources))]
        [RegularExpression(LocalConstants.EMAIL_FORMAT, ErrorMessageResourceName = "VendorRegistrationModel_InvalidEmail", ErrorMessageResourceType = typeof(Resources.Resources))]
        // Ends<<<
        public string Email { get; set; }

        [Required(ErrorMessageResourceName = "Common_ErrorMessage_Password", ErrorMessageResourceType = typeof(Resources.Resources))]
        [StringLength(128, ErrorMessage = "Cannot exceed 128 chars")]
        [RegularExpression(@"(?=^.{10,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$", ErrorMessageResourceName = "VendorRegistrationModel_Password", ErrorMessageResourceType = typeof(Resources.Resources))]
        public string Password { get; set; }

        [Required(ErrorMessageResourceName = "ErrorMessage_ReenterPassword", ErrorMessageResourceType = typeof(Resources.Resources))]
        [System.Web.Mvc.Compare("Password", ErrorMessageResourceName = "VendorRegistrationModel_ConfirmPassword", ErrorMessageResourceType = typeof(Resources.Resources))]
        [StringLength(128, ErrorMessageResourceName = "ErrorMessage_Exceed128", ErrorMessageResourceType = typeof(Resources.Resources))]
        public string ConfirmPassword { get; set; }

        // >>>>>> Added code Dt: 07/05/2013
        public System.Nullable<Guid> InvitationId { get; set; }
        // <<<<<< Added code ends
        public bool CheckBoxStatus { get; set; }

        [Required(ErrorMessage = "*")]
        public string PrincipalCompanyOfficerName { get; set; } // Kiran on 2/13/2014

        // Kiran on 8/5/2014
        [Required(ErrorMessage = "*")]
        //[StringLength(50, ErrorMessage = "Tax id would be minimum 10 characters", MinimumLength = 10)]
        [RegularExpression(@"[0-9]{3}[-][0-9]{2}[-][0-9]{4}|[0-9]{2}[-][0-9]{7}", ErrorMessageResourceName = "Common_InvalidTaxID", ErrorMessageResourceType = typeof(Resources.Resources))] //Kiran on 8/20/2014
        public string TaxID { get; set; }
        // Ends<<<
        [RegularExpression(@"^-*[0-9,\.]+$", ErrorMessageResourceName = "ErrorMessage_PhoneNumber", ErrorMessageResourceType = typeof(Resources.Resources))]//Phani on 23/07/2015
        public string ExtNumber { get; set; } // Kiran on 8/26/2014


        [Required]
        public int CountryType { get; set; }
        [Required(ErrorMessage ="*")]
        public long Language { get; set; }
        [Required(ErrorMessage = "*")]
        public List<string> GeographicArea { get; set; }
    }
}
