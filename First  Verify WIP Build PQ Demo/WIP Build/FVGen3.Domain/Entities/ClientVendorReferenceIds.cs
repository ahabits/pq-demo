﻿//Page Created Date: 08/11/2015
//Created By: sumanth
//Purpose:Client Reference Id for Gordmans
//Version: 1.0
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FVGen3.Domain.Entities
{
   public class ClientVendorReferenceIds
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long ClientVendorRecId { get; set; }

        [ForeignKey("ClientId")]
        public virtual Organizations Client { get; set; }
        public long ClientId { get; set; }


        [ForeignKey("VendorId")]
        public virtual Organizations Vendor { get; set; }
        public long VendorId { get; set; }
   
        
       public string ClientVendorReferenceId { get; set; }

       public long? ClientContractId { get; set; }  

    }
}
