﻿/*
Page Created Date:  06/11/2013
Created By: Siva Bommisetty
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FVGen3.Domain.Entities
{
    public class QuestionColumnDetails
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long QuestionColumnId { get; set; }

        public int ColumnNo { get; set; }
        public string ColumnValues { get; set; }
        public int DisplayType { get; set; }
        public string TableReference { get; set; }
        public string TableReferenceField { get; set; }
        public string TableRefFieldValueDisplay { get; set; }
        public int? DependantColumnNo { get; set; }
        [ForeignKey("QuestionControlTypeId")]
        public virtual QuestionControlType QuestionControlType { get; set; }
        public long QuestionControlTypeId { get; set; }
        public string QFormula { get; set; }
        [ForeignKey("QuestionId")]
        public virtual Questions Questions { get; set; }
        public long QuestionId { get; set; }
        //public string CorrectAnswer { get; set; } // Kiran on 10/9/2014
        public int? ButtonTypeReferenceValue { get; set; }

        public string SortReferenceField { get; set; } // Kiran on 12/31/2014
        public virtual List<PrequalificationUserInput> PrequalificationUserInputs { get; set; }

        public virtual List<EmployeeQuizUserInput> EmployeeQuizUserInput { get; set; }
        public virtual List<ClientUserSignature> ClientUserSignature { get; set; }//Rajesh 6/26/2014

    }
}
