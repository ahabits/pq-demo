﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
namespace FVGen3.Domain.Entities
{
  public class PowerBiReportUrls
    {

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public long Id { get; set; }
        [ForeignKey("PowerBiReportId")]
        public virtual PowerBiReports PowerBiReports { get; set; }
        public long PowerBiReportId { get; set; }
        public long OrgId { get; set; }
        public string Url { get; set; }
    }
}
