﻿/*
*****************************************************
Page Created Date:  8/8/2014
Created By: Kiran Talluri
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FVGen3.Domain.Entities
{
    public class ArchiveOrganizationDetails
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long ArchiveOrgId { get; set; }
        public int VersionNo { get; set; }

        [ForeignKey("SystemUsers")]
        public Guid ChangedBy { get; set; }
        public virtual SystemUsers SystemUsers { get; set; }

        public DateTime ChangedDateTime { get; set; }

        [ForeignKey("Organizations")]
        public long OrganizationId { get; set; }
        public virtual Organizations Organizations { get; set; }


        public string LegalNameOfBusiness { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; } // Kiran on 9/19/2014
        public string City { get; set; }
        public string State { get; set; }
        public int? FinancialMonth { get; set; }
        public int? FinancialDate { get; set; }
        public string Zip { get; set; }
        public string Country { get; set; }
        public string PhoneNumber { get; set; }

        public string FaxNumber { get; set; }
        public string WebsiteURL { get; set; }
        public string FederalIDNumber { get; set; }
        public string PrincipalCompanyOfficerName { get; set; }
        public string TaxID { get; set; }
        //public int TaxType { get; set; } 

        public string OrgRepresentativeName { get; set; }
        public string OrgRepresentativeEmail { get; set; }
        public string OrgInfusionSoftContactId { get; set; }
        public string GeographicArea { get; set; }
        public string ExtNumber { get; set; } // Kiran on 8/26/2014
    }
}
