﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.Entities
{
    public class LocalQuestionColumnDetails
    {
        public bool HasVendorDetails { get; set; }
        public bool Visible { get; set; }
        public bool IsMandatory { get; set; }
        public bool IsBold { get; set; }
        public bool ResponseRequired { get; set; }
        public bool IsRepeat { get; set; }
        public long QuestionId { get; set; }
        public bool HasDependent { get; set; }
        public bool IsColor { get; set; }
        public string QuestionAnswer { get; set; }
        public string HighlightedColor { get; set; }
        // kiran on 9/11/2014
        public bool isEdit { get; set; }
        public bool? isQuesionColumnIdexistsInQuizUserInput { get; set; }
        // Ends<<<

        public string ReferenceType { get; set; }
        public int? GroupNum { get; set; }

        public List<ColumnDetails> ColumnDetails { set; get; }
        public List<CQuestion> CQuestion { get; set; }
    }
    public class CQuestion
    {public string Answer { get; set; }
       
        public long QuestionId { get; set; }
    public string Color { get; set; }
  
}
public class ColumnDetails
    {
        public string ControlType { get; set; }
        public string Value { get; set; }
        public int DisplayType { get; set; }
        public bool HasFormula { get; set; }
        public string FormulaValue { get; set; }
        public bool HasTableReference { get; set; }
        public string RefTableName { get; set; }
        public List<string> QuestionValues { get; set; }
        public int MaxQuestionColumns { get; set; }
        public int ButtonTypeReferenceValue { get; set; }
    }
}
