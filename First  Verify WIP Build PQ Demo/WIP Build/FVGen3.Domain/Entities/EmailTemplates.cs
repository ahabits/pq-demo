﻿/*
Page Created Date:  05/11/2013
Created By: Siva Bommisetty
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations.Schema;

namespace FVGen3.Domain.Entities
{
    public class EmailTemplates
    {
        [Key]
        public int EmailTemplateID { get; set; }

        public int EmailUsedFor { get; set; }

        public bool? IsClientSpecific { get; set; }

        public long? ClientOrganizationID { get; set; }

        [Required(ErrorMessage = "*")]
        public string Name { get; set; }

        [Required(ErrorMessage = "*")]
        public string EmailSubject { get; set; }

        [Required(ErrorMessage = "Enter Content")]
        [AllowHtml]
        [MaxLength(50000, ErrorMessage = "Cannot exceed 20000 chars.")]
        public string EmailBody { get; set; }

        public string CommentText { get; set; }
        public System.Nullable<DateTime> InsertDateTime { get; set; }
        public System.Nullable<DateTime> UpdateDateTime { get; set; }

        public System.Nullable<bool> IsDocumentSpecific { get; set; }

        public virtual List<EmailTemplateAttachments> EmailTemplateAttachments { get; set; }
        public virtual List<EmailTemplateContent> EmailTemplateContent { get; set; }
        public EmailTemplates()
        {
            IsClientSpecific = false;
        }
        [NotMapped]
        public long LanguageId { get; set; }
        [NotMapped]
        public string SelectedLanguageIds { get; set; }
    }
}
