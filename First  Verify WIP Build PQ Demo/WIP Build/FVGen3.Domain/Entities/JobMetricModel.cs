﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.Entities
{
   public  class JobMetricModel
    {
        public System.DateTime? DateOfFinancialStatement { get; set; }
        public String FormattedValue
        {
            get
            {
                return FinancialYearEndDate.HasValue
                    ? FinancialYearEndDate.Value.ToString("MM/dd/yy")
                    : ""; // return an empty string if SelectedDate is null
            }
        }
        public System.DateTime? FinancialYearEndDate
        {
            get { return DateOfFinancialStatement.HasValue ? DateOfFinancialStatement.Value.AddYears(1) : DateOfFinancialStatement; }

        }
        public System.DateTime? LastUpdatedOn { get; set; }
        public  string networthLoc { get; set; }

        public string FivetimesWorkingCapital { get; set; }

        public string TwntyFivePrcntPriorYearRevenue { get; set; }

        public string SeventyFivePercentLargestCompletedProject { get; set; }

        public string AvrgMeasuresForSingleJob { get; set; }

        public string NetWorthTotalLineCredit { get; set; }

        public string TenTimesWorkingCapital { get; set; }

        public string FiftyPercentPriorYearRevenue { get; set; }

        public string TenTimesNetWorth { get; set; }

        public string AvrgMeasuresForAggrigate { get; set; }

        public string TotalLinesCredit { get; set; }

        public string AvailableLineCredit { get; set; }

        public string TotalOfThreeLargestCompletedProjects { get; set; }
    }
}
