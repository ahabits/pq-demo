﻿/*
Page Created Date:  06/11/2013
Created By: Siva Bommisetty
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace FVGen3.Domain.Entities
{
    public class LocalAddNewClientTemplateProfile
    {
        public long OrganizationID { get; set; }

        public Guid TemplateId { get; set; }
        public long TemplateSectionId { get; set; }

        [Required(ErrorMessage = "*")]
        public string TemplateSectionName { get; set; }

        [Required(ErrorMessage = "*")]
        public int PositionType { get; set; }

        //[Required(ErrorMessage = "*")]
        [MaxLength(100, ErrorMessageResourceName = "ErrorMessage_Exceed100", ErrorMessageResourceType = typeof(Resources.Resources))]
        public string PositionTitle { get; set; }

        [Required(ErrorMessageResourceName = "ErrorMessage_Content", ErrorMessageResourceType = typeof(Resources.Resources))]
        [MaxLength(3500, ErrorMessageResourceName = "ErrorMessage_Exceed3500", ErrorMessageResourceType = typeof(Resources.Resources))]
        public string PostionContent { get; set; }

        public long HeaderFooterId { get; set; }
        
    }
}
