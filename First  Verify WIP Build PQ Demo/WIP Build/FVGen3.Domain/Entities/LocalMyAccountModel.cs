﻿/*
Page Created Date:  06/11/2013
Created By: Siva Bommisetty
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.ComponentModel.DataAnnotations;
using Resources;

namespace FVGen3.Domain.Entities
{
    public class LocalMyAccountModel
    {
        public Guid? ApplicationId { get; set; }

        //[Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid UserId { get; set; }

        public long ContactId { get; set; }

        public string UserName { get; set; }
        public string Password { get; set; }

        [Required(ErrorMessage = "*")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "*")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "*")]
        public string ContactTitle { get; set; }

        [Required(ErrorMessageResourceName = "ErrorMessage_Required_PhoneNumber", ErrorMessageResourceType = typeof(Resources.Resources))]
        [StringLength (50, ErrorMessageResourceName = "ErrorMessage_Invalid_PhoneNumber", ErrorMessageResourceType = typeof(Resources.Resources), MinimumLength = 14)]
        public string PhoneNumber { get; set; }

        public string MobileNumber { get; set; }

        public string AdditionalDescription { get; set; }

        [Required(ErrorMessageResourceName = "ErrorMessage_Required_Email", ErrorMessageResourceType = typeof(Resources.Resources))]
        // Kiran on 01/20/2015
        //[RegularExpression(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$", ErrorMessage = "Invalid Email")]
        [RegularExpression(LocalConstants.EMAIL_FORMAT,  ErrorMessageResourceName = "Common_Email_Invalid_ErrorMessage", ErrorMessageResourceType = typeof(Resources.Resources))]
        // Ends<<<
        public string Email { get; set; }

        public System.Nullable<DateTime> LastLoginDate { get; set; }
        public System.Nullable<DateTime> LastPasswordChangedDate { get; set; }

        public string CompanyName { get; set; }
        // Kiran on 3/25/2014
        [Required(ErrorMessage = "*")]
        public string PrincipalCompanyOfficerName { get; set; } // Kiran on 2/13/2014

        [RegularExpression(@"^-*[0-9,\.]+$", ErrorMessageResourceName = "ErrorMessage_PhoneNumber", ErrorMessageResourceType = typeof(Resources.Resources))]//Phani on 23/07/2015
        public string ExtNumber { get; set; } // Kiran on 8/26/2014
    }
}
