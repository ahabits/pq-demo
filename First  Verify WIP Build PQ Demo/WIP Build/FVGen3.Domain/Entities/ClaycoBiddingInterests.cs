﻿/*
Page Created Date:  08/08/2013
Created By: Kiran Talluri
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FVGen3.Domain.Entities
{
    public class ClaycoBiddingInterests
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long BiddingInterestId { get; set; }
        public string BiddingInterestCode { get; set; }
        public string BiddingInterestName { get; set; }
        public int? Status { get; set; }
        public long LanguageId { get; set; }
    }
}
