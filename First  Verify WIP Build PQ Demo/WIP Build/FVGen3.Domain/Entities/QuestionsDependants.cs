﻿/*
Page Created Date:  08/02/2013
Created By: Siva Bommisetty
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace FVGen3.Domain.Entities
{
    public class QuestionsDependants
    {
        [Key]
        public long QuestionsDependantId { get; set; }
        public int DependantType { get; set; }
        public long DependantId { get; set; }
        
        public string DependantValue { get; set; }
        public int? DependantDisplayOrder { get; set; }

        [ForeignKey("QuestionID")]
        public virtual Questions Questions { get; set; }
        public long QuestionID { get; set; }
        
    }
}
