﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace FVGen3.Domain.Entities
{
    public class LocalChangeStatus
    {
        public long PrequalificationId { get; set; }
        public long pqStatusId { get; set; }
        //[Required(ErrorMessage="*")]
        public string NewStatus { get; set; } // Kiran on 11/20/2013
        public string CurrentStatus { get; set; }
        public string currentStatusPeriod { get; set; }
        //[Required(ErrorMessage = "*")]
        public string NewPeriodStart { get; set; }
        //[Required(ErrorMessage = "*")]
        public string NewStatusChanged { get; set; }

        //public List<string> NotCompletedSection { get; set; }
        public List<bool> NotCompletedSection_Status { get; set; }
        public List<TemplateSections> NotCompletedSection { get; set; }

        public decimal? NewPayment { get; set; }
        public bool isStatusChanged { get; set; }
        public bool isOverridePayment { get; set; }
        public Guid templateId { get; set; }

        // Kiran on 11/20/2013
        public string PeriodLengthUnit { get; set; }
        [Required(ErrorMessageResourceName = "ErrorMessage_PeriodLength", ErrorMessageResourceType = typeof(Resources.Resources))]
        //[RegularExpression("^[1-9][0-9]*$", ErrorMessage = "Should not start with zero")]
        public int PeriodLength { get; set; }
        // Ends<<<

        public bool RequestForMoreInfo { get; set; } // Kiran on 01/11/2014
        public string adminUserId { get; set; } // Kiran on 12/03/2013

        public List<bool> DisplaySection_Status { get; set; }
        //public List<TemplateSections> DisplaySection { get; set; }
        public List<TemplateSectionsStatus> displayTemplateSections { get; set; }
        public bool SupportingDocumentsReviewStatus { get; set; }
        public List<KeyValuePair<string, string>> adminUsers { get; set; }
        public Guid? userIdAsSigner { get; set; }
    }

    public class TemplateSectionsStatus
    {
        public long TemplateSectionId { get; set; }
        public string SectionName { get; set; }
        public bool Visible { get; set; }
        public bool Status { get; set; }
    }
}
