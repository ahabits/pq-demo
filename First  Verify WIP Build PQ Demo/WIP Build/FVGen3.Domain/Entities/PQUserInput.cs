﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.Entities
{
    public class PQUserInput
    {
        public long QuestionColumnId { get; set; }
        public string UserInput { get; set; }
        public string InsertionType { get; set; }
        public long QuestionReferenceId { get; set; }
    }
}
