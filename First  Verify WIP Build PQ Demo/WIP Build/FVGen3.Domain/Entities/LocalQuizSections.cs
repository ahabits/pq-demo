﻿/*
*****************************************************
Page Created Date:  10/7/2014
Created By: Kiran Talluri
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace FVGen3.Domain.Entities
{
    public class LocalQuizSections
    {
        [Required(ErrorMessage = "*")]
        [StringLength(50, ErrorMessageResourceName = "ErrorMessage_Exceed50", ErrorMessageResourceType = typeof(Resources.Resources))]
        public string SectionName { get; set; }
        public Guid Templateid { get; set; }
        public bool VisibleToClient { get; set; }
        public bool Visible { get; set; }
        [Required(ErrorMessageResourceName = "ErrorMessage_SectionType", ErrorMessageResourceType = typeof(Resources.Resources))] // Kiran on 12/22/2014
        public int SectionType { get; set; }
        public long TemplateSectionID { get; set; }
        public string OtherSectionTypes { get; set; }
        public List<SelectListItem> Clients { get; set; }
        public int Permission { get; set; }
    }
}
