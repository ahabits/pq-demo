﻿/*
 *****************************************************
Page Created Date:  05/24/2013
Created By: Siva Bommisetty
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FVGen3.Domain.Entities
{
   public class SystemUsersOrganizationsRoles
    {
        [Key]
        public long SysUserOrgRoleId { get; set; }

        [ForeignKey("SystemUsersOrganizations")]
        public long SysUserOrgId { get; set; }

        [ForeignKey("SystemRoles")]
        public string SysRoleId { get; set; }

        public virtual SystemRoles SystemRoles { get; set; }

        public virtual SystemUsersOrganizations SystemUsersOrganizations { get; set; }
    }
}
