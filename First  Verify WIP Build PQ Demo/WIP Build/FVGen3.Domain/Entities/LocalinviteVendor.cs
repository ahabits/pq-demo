﻿/*
Page Created Date:  06/11/2013
Created By: Siva Bommisetty
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

using Resources;

namespace FVGen3.Domain.Entities
{
    public class LocalinviteVendor
    {
        
        public long client { get; set; }
        [Required(ErrorMessage = "*")]
        public List<long> Location { get; set; }
        [Required(ErrorMessage="*")]
        public string FullName { get; set; }
        [Required(ErrorMessageResourceName = "Common_ErrorMessage_UserName", ErrorMessageResourceType = typeof(Resources.Resources))]
        // Kiran on 01/20/2015
        //[RegularExpression(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$",ErrorMessage="Invalid Email")]
        [RegularExpression(LocalConstants.EMAIL_FORMAT, ErrorMessageResourceName = "Common_Email_Invalid_ErrorMessage", ErrorMessageResourceType = typeof(Resources.Resources))]
        // Ends<<<
        public string EmailAddress { get; set; }
        public bool isSendBCC { get; set; }
        public bool isSendMeCC { get; set; }
        
        public List<String> usersSendCC { get; set; }
        public List<String> usersSendBCC { get; set; }

        [Required(ErrorMessage = "*")]
        public string LastName { get; set; }
        public string ClientCode { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; } // Kiran on 9/19/2014
        public string City { get; set; } // Kiran on 4/14/2014
        public string State { get; set; }
        public string Zip { get; set; }
        public string Country { get; set; }
        public Guid InvitationId { get; set; }// sumanth on 10/09/2015 for SMI-268
        public string UserEmail { get; set; }// sumanth on 10/09/2015 for SMI-268
        [Required(ErrorMessage = "*")]
        public string VendorCompanyName { get; set; }
        [Required(ErrorMessage ="*")]
        public string RiskLevel { get; set; }
        [StringLength(1000, ErrorMessageResourceName = "ErrorMessage_Comments", ErrorMessageResourceType = typeof(Resources.Resources))]
        public string Comments { get; set; }

        public int? InvitationType { get; set; }

        //These Fields Used for pagination in ClientDashboard rajesh on 12/13/2013
        public bool HasPrevious { get; set; }
        public bool HasNext { get; set; }
        public int PageNumber { get; set; }
        public DateTime InvitationDate { get; set; }
        public long VendorId { get; set; }
        [Required(ErrorMessage = "*")]
        public long Language { get; set; }
        public string InvitationCode { get; set; }
        
    }
}

