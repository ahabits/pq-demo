﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;


namespace FVGen3.Domain.Entities
{
    public class VendorDetails
    {
        //sandhya on 11-04-2015
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long VendorDetailsId { get; set; }

        [ForeignKey("Prequalification")]
        public long PrequalificationId { get; set; }
        public virtual Prequalification Prequalification { get; set; }

        public string FullName { get; set; }
        public string Title { get; set; }
        public string EmailAddress { get; set; }
        public string TelephoneNumber { get; set; }
        public long? GroupNumber { get; set; }

    }
}
