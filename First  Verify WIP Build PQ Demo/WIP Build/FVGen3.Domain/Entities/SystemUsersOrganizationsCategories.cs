﻿/*
*****************************************************
Page Created Date:  02/16/2015
Created By: Kiran Talluri
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace FVGen3.Domain.Entities
{
    public class SystemUsersOrganizationsCategories
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long SysUsersOrgCategoryId { get; set; }

        [ForeignKey("SystemUsersOrganizations")]
        public long? SysUserOrganizationId { get; set; }
        public virtual SystemUsersOrganizations SystemUsersOrganizations { get; set; }

        [ForeignKey("EmployeeQuizCategories")]
        public long? EmpQuizCategoryId { get; set; }
        public virtual EmployeeQuizCategories EmployeeQuizCategories { get; set; }

        public string SubCategoryInputValue { get; set; }
        public DateTime? SubCategoryEnteredDate { get; set; }

        // Kiran on 03/13/2015
        [ForeignKey("SystemUsers")]
        public Guid? CategoryAssignedBy { get; set; }
        public virtual SystemUsers SystemUsers { get; set; }
        // Kiran on 03/13/2015 Ends<<<
    }
}
