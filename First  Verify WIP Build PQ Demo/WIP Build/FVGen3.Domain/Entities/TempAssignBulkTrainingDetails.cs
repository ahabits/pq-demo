﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FVGen3.Domain.Entities
{
    public class TempAssignBulkTrainingDetails
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public string BulkTrainingPaymentInfo { get; set; }

        public Guid? PaymentId { get; set; }

        public string QuizIds { get; set; }

        public string Employees { get; set; }
    }
}
