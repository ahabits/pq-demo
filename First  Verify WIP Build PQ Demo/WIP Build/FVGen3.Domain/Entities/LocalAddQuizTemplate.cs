﻿/*
*****************************************************
Page Created Date:  10/7/2014
Created By: Kiran Talluri
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace FVGen3.Domain.Entities
{
    public class LocalAddQuizTemplate
    {
        public LocalAddQuizTemplate()
        {
            isSectionSelected = true;
        }
        [Required(ErrorMessage = "*")]
        public string TemplateName { get; set; }
        public Guid? Templateid { get; set; }
        public string ClientId { get; set; }
        public string CopyTemplateId { get; set; }
        public bool isCopyTemplateSelected { get; set; }
        public bool isSectionSelected { get; set; }
        public bool isSubSectionSelected { get; set; }
        public bool isQuestionsSelected { get; set; }

        public string ClientOfTemplate { get; set; }
        public string TemplateType { get; set; }
        //[Required(ErrorMessage = "*")]
        public string TemplateNote { get; set; }

       
        public string TemplateCode { get; set; }

        // Kiran on 12/24/2014
        [Required(ErrorMessage = "*")]
        public decimal? QuizPassRate { get; set; }

        //[Required(ErrorMessage="*")]
        public bool QuizRetake { get; set; }

        //[Required(ErrorMessage = "*")]
        public decimal? QuizRetakeFee { get; set; }
        public int? NumberOfAttempts { get; set; }
        // Ends<<<

        public long ClientQuizDetailsID { get; set; }

        //Rajesh on 11/7/2014
        [Required(ErrorMessage = "*")]
        public int? PeriodLength { get; set; }
        [Required(ErrorMessage = "*")]
        public string PeriodLengthUnit { get; set; }
        public bool WaiverForm { get; set; }
        [Required(ErrorMessage = "*")]
        //[MaxLength(3500, ErrorMessageResourceName = "ErrorMessage_Exceed3500", ErrorMessageResourceType = typeof(Resources.Resources))]
        [AllowHtml]
        public string WaiverFormContent { get; set; }
        //Ends<<<
        public long? LanguageId { get; set; }

    }
}
