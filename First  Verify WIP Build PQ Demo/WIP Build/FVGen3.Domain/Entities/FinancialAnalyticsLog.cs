﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace FVGen3.Domain.Entities
{
  public  class FinancialAnalyticsLog
    {
        [Key]
        public int Id
        {
            get;
            set;
        }
        public long? VendorId
        {
            get;
            set;
        }
        public System.DateTime? DateOfFinancialStatements
        {
            get;
            set;
        }
        public System.DateTime? LastUpdatedOn
        {
            get;
            set;
        }
        public System.DateTime? Dateofinterimfinancialstatement
        {
            get;
            set;
        }
        public string SourceOfFinancialStatements
        {
            get;
            set;
        }
        public decimal? CashAndCashEquivalents
        {
            get;
            set;
        }
        public decimal? Investments
        {
            get;
            set;
        }
        public decimal? ReceivablesCurrent
        {
            get;
            set;
        }
        public decimal? ReceivableRetainage
        {
            get;
            set;
        }
        public decimal? CostsAndEstimatedEarningsInExcessOfBillingsOnUncompletedContracts
        {
            get;
            set;
        }
        public decimal? RefundableIncomeTaxes
        {
            get;
            set;
        }
        public decimal? NoteReceivable
        {
            get;
            set;
        }
        public decimal? OtherCurrentAssets
        {
            get;
            set;
        }
        public decimal? NetPropertyAndEquipment
        {
            get;
            set;
        }
        public decimal? OtherAssets
        {
            get;
            set;
        }
        public decimal? LinesOfCreditShortTermBorrowings
        {
            get;
            set;
        }
        public decimal? InstallmentNotesPayableDueWithinOneYear
        {
            get;
            set;
        }
        public decimal? AccountsPayable
        {
            get;
            set;
        }
        public decimal? RetainagePayable
        {
            get;
            set;
        }
        public decimal? AccruedExpenses
        {
            get;
            set;
        }
        public decimal? BillingsInExcessOfCostsAndEstimatedEarningsOnUncompletedContracts
        {
            get;
            set;
        }
        public decimal? CurrentIncomeTaxesPayable
        {
            get;
            set;
        }
        public decimal? DeferredIncomeTaxesPayableCurrent
        {
            get;
            set;
        }
        public decimal? NotesPayableDueAfterOneYear
        {
            get;
            set;
        }
        public decimal? EquipmentLinesOfCredit
        {
            get;
            set;
        }
        public decimal? OtherLongTermLiabilities
        {
            get;
            set;
        }
        public decimal? DeferredIncomeTaxesPayableLongTerm
        {
            get;
            set;
        }
        public decimal? TotalStockholdersEquity
        {
            get;
            set;
        }
        public decimal? ConstructionRevenues
        {
            get;
            set;
        }
        public decimal? ConstructionDirectCosts
        {
            get;
            set;
        }
        public decimal? NonConstructionGrossProfitLoss
        {
            get;
            set;
        }
        public decimal? GeneralAndAdministrativeExpenses
        {
            get;
            set;
        }
        public decimal? OtherIncomeDeductions
        {
            get;
            set;
        }
        public decimal? IncomeTaxExpenseBenifit
        {
            get;
            set;
        }
        public decimal? DepreciationAndAmortization
        {
            get;
            set;
        }
        public decimal? InterestExpense
        {
            get;
            set;
        }
        public decimal? TotalBacklog
        {
            get;
            set;
        }
        public decimal? TotalLineOfCredit
        {
            get;
            set;
        }
        public decimal? LineOfCreditAvailability
        {
            get;
            set;
        }
        public decimal? TotalThreeLargestCompletedProjects
        {
            get;
            set;
        }
        public long? PrequalificationId { get; set; }
        public long? ClientId
        {
            get;
            set;
        }
       

    }
}
