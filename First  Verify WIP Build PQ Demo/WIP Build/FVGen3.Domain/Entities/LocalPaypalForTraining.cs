﻿/*
Page Created Date:  11/7/2014
Created By: Kiran Talluri
Purpose:
Version: 1.0
****************************************************
*/


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.Entities
{
    public class LocalPaypalForTraining
    {
        public LocalPaypalForTraining()
        { }

        public string cmd { get; set; }
        public string business { get; set; }
        public string no_shipping { get; set; }
        public string @return { get; set; }
        public string cancel_return { get; set; }
        public string notify_url { get; set; }
        public string currency_code { get; set; }
        public string item_name { get; set; }
        public string amount { get; set; }
        public string custom { get; set; } // Kiran on 04/28/2015
    }
}
