﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace FVGen3.Domain.Entities
{
    public class LocalAddBiddingInterest
    {
        public LocalAddBiddingInterest()
        {
            Status = true;
        }

        public long BiddingInterestId { get; set; }

        [Required(ErrorMessage="*")]
        public string BiddingInterestName { get; set; }
        public bool Status { get; set; }
        public bool isEdit { get; set; }
        public long? LanguageId { get; set; }
    }
}
