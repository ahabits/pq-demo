﻿/*
Page Created Date:  06/11/2013
Created By: Siva Bommisetty
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace FVGen3.Domain.Entities
{
     public class BiddingInterestType
    {
         [Key]
         [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int BiddingInterestTypeId { get; set; }

        public string BiddingInterestTypeName { get; set; }

    }
}
