﻿/*
Page Created Date:  06/11/2013
Created By: Siva Bommisetty
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.Entities
{
   public class LocalHeaderMenuModel
    {
        public LocalHeaderMenuModel()
        {
        }
        public LocalHeaderMenuModel(SystemHeaderMenus menu)
        {
            this.menuDisplayname = menu.SystemHeaderMenuName;
            this.menuName = menu.SysMenuName;
            this.hasSideMenu = menu.HasSideMenu!=null && menu.HasSideMenu==true;
            this.actionName = menu.ActionName;
            this.controllerName = menu.ControllerName;
            this.isChecked = false;
            this.headerMenuId= menu.SystemHeaderMenuId;
            this.DisplayOrder = menu.DisplayOrder;
        } 
        public LocalHeaderMenuModel(string menuDisplayname, string menuName,bool hasSideMenu, string actionName, string ctrlName, bool isChecked)
        {
            this.menuDisplayname = menuDisplayname;
            this.menuName = menuName;
            this.hasSideMenu = hasSideMenu;
            this.actionName = actionName;
            this.controllerName = ctrlName;
            this.isChecked = isChecked;
        }
        public bool isChecked { get; set; }
        public string menuName { get; set; }
        public string actionName { get; set; }
        public bool hasSideMenu { get; set; }
        public string controllerName { get; set; }
        public string menuDisplayname { get; set; }
        public long headerMenuId { get; set; }
        public int? DisplayOrder { get; set; }
        public List<LocalSideMenuModel> sideMenus{ get; set; }
    }
}
