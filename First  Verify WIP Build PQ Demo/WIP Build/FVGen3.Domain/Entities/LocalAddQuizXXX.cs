﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.Entities
{
    public class LocalAddQuizXXX
    {
        public long clientId { get; set; }
        public long clientBusinessUnitId { get; set; }
        public string clientBusUnitLocation { get; set; }
        public Guid userId { get; set; }
        

        public List<ClientQuiz> ClientQuiz { get; set; }
    }

    public class ClientQuiz
    {
        public ClientQuiz()
        {
            quiz_CheckBoxStatus = false;
        }

        public bool quiz_CheckBoxStatus { get; set; }
        public string templateName { get; set; }
        public Guid clientTemplateId { get; set; }
    }
}
