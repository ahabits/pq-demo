﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.Entities
{
    public class LocalNotifications
    {
        public string VendorName { get; set; }
        public string Purpose { get; set; }
        public string ClientName { get; set; }

        public string Comments { get; set; }
        public string SmallComment { get; set; }
        public string Initial { get; set; }

        public bool HasPrevious { get; set; }
        public bool HasNext { get; set; }
        public int PageNumber { get; set; }

        public DateTime EmailSentDate { get; set; } // Kiran on 9/8/2014

    }
}
