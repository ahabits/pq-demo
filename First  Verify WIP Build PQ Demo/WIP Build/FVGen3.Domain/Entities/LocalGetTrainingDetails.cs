﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.Entities
{
    public class LocalGetTrainingDetails
    {
        public List<CheckBoxControlForEmployeeTrainings> employeeAdditionAlFeeTrainings { get; set; }
        public List<GetTrainingDetails> trainingDetails { get; set; }

    }
    public class CheckBoxControlForEmployeeTrainings
    {
        public string ClientName { get; set; }
        public string EmployeeName { get; set; }
        public string TemplateName { get; set; }
        public long PrequalificationID { get; set; }
        public decimal TrainingFee { get; set; }
        public Guid? ClientTemplateId { get; set; }
        public Guid EmpId { get; set; }
        public long? ClientId { get; set; } // Kiran on 11/25/2015
        public long SysUserOrgQuizId { get; set; }
        public List<GetPrequalificationBusinessUnitsAndSites> GetPrequalBusinessUnitsAndSites { get; set; }
    }

    public class GetTrainingDetails
    {
        public Guid? ClientTemplateId { get; set; }        
        public decimal TrainingFeeToPay { get; set; }
        public long PrequalificationID { get; set; }
        public long SysuserOrgQuizId { get; set; }
        public long? ClientId { get; set; }
        public List<GetPrequalificationBusinessUnitsAndSites> GetPrequalBusinessUnitsAndSites { get; set; }
    }
    public class GetPrequalificationBusinessUnitsAndSites
    {
        public long BusinessUnitId { get; set; }
        public long SiteId { get; set; }
    }
}
