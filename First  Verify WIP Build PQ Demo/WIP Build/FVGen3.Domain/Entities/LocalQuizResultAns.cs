﻿/*
Page Created Date:  02/11/2014
Created By: Kiran Talluri
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.Entities
{
    public class LocalQuizResultAns
    {        
        public string Question { get; set; }
        public string Options { get; set; }
        public string EmployeeAnswer { get; set; }
        public string CorrectAnswer { get; set; }
        public long QuestionControlType { get; set; }
    }
}
