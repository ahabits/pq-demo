﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace FVGen3.Domain.Entities
{
    public class LocalAssignTemplates
    {
        public List<CheckBoxesControl> Templates { get; set; }
        public List<CheckBoxesControl> Clients { get; set; }
        public List<CheckBoxesControl> DocumentTypes { get; set; }
        public List<CheckBoxesControl> ProficiencyCapabilities { get; set; }
        public List<CheckBoxesControl> BiddingInterests { get; set; }
        public string SelectedTemplate { get; set; }
        public string SelectedClientId { get; set; }
    }

    public class CheckBoxesControl
    {
        public string id { get; set; }
        public string Text { get; set; }
        //public string status { get; set; }
        public int type { get; set; }//0 Bidding 1-proficancy & category
        public string Header { get; set; }//this is For Proficiancy Category
        public bool isExpire { get; set; }//only for documents
        public bool Status { get; set; }
    } 
}
