﻿/*
Page Created Date:  08/01/2013
Created By: Siva Bommisetty
Purpose:
Version: 1.0
****************************************************
*/


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace FVGen3.Domain.Entities
{
    public class SystemHeaderMenus
    {
        [Key]
        public long SystemHeaderMenuId { get; set; }

        public string SystemHeaderMenuName { get; set; }
        public string SysMenuName { get; set; }
        public bool? HasSideMenu { get; set; }
        public string ActionName { get; set; }
        public string ControllerName { get; set; }
        
        public virtual List<SystemSideMenus> SystemSideMenus { get; set; }
        public int? DisplayOrder { get; set; }
    }
}
