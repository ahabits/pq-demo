﻿/*
Page Created Date:  05/01/2013
Created By: Siva Bommisetty
Purpose:
Version: 1.0
****************************************************
*/

using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace FVGen3.Domain.Entities
{
    public class ClientUserSignature
    {
        
        //>>DV:SB,KT
        [Key]
        public long ClientUserSignatureId { get; set; }

        //public Guid UserId { get; set; }
        [ForeignKey("Organizations")]
        public long ClientId { get; set; }
        [ForeignKey("QuestionColumnDetails")]
        public long QuestionColumnId { get; set; }
        public string QuestionText { get; set; }
        public string QuestionColumnIdValue { get; set; }

        public long? GetValueQuestionColumId { get; set; }

        public virtual Organizations Organizations { get; set; }
        public virtual QuestionColumnDetails QuestionColumnDetails { get; set; }

    }
}