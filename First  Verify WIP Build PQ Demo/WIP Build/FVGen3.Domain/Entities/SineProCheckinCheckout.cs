﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.Entities
{
    public class SineProCheckinCheckout
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public string RecId { get; set; } 
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string EmployeeEmail { get; set; }
        public string MobileNo { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public string VisitorType { get; set; }
        public string VisitorId { get; set; }
        public DateTime? CheckinTime { get; set; }
        public string CompanyName { get; set; }
        public string SiteName { get; set; }
        public string Status { get; set; }
        public string SiteId { get; set; }
        public string CheckinUser { get; set; }
        public string CheckOutUser { get; set; }
        public DateTime? CheckoutTime { get; set; } 
        public string HostId { get; set; }
        public string HostGroupId { get; set; }
        public string HostGroupName { get; set; }
        public long? ClientId { get; set; }

    }
}
