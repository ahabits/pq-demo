﻿/*
Page Created Date:  06/07/2013
Created By: Siva Bommisetty
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FVGen3.Domain.Entities
{
    public class ClientBusinessUnitSites
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long ClientBusinessUnitSiteId { get; set; }

        public string SiteName { get; set; }
        public DateTime InsertDate { get; set; }
        public System.Nullable<DateTime> UpdateDate { get; set; }

        [ForeignKey("ClientBusinessUnit")]
        public long ClientBusinessUnitId { get; set; }
        public virtual ClientBusinessUnits ClientBusinessUnit { get; set; }

        // Kiran on 10/29/2014
        public string BusinessUnitLocation { get; set; }
        public int BusinessUnitType { get; set; }
        // Ends<<<  

        public virtual List<ClientBusinessUnitSiteRepresentatives> ClientBusinessUnitSiteRepresentatives { get; set; }
        public virtual List<PrequalificationSites> PrequalificationSites { get; set; }  // Rajesh on 07/24/2013
    }
}
