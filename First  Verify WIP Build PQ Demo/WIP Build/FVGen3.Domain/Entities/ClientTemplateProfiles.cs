﻿/*
Page Created Date:  08/28/2013
Created By: Siva Bommisetty
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace FVGen3.Domain.Entities
{
    public class ClientTemplateProfiles
    {
        [Key]
        public long ClientTemplateProfileId { get; set; }

        public int PositionType { get; set; }
        public string PositionTitle { get; set; }
        public int? ContentType { get; set; }
        public string PositionContent { get; set; }
        public int AttachmentType { get; set; }
        public bool AttachmentExist { get; set; }

        [ForeignKey("ClientId")]
        public virtual Organizations Organization { get; set; }
        public long ClientId { get; set; }

        [ForeignKey("TemplateSectionId")]
        public virtual TemplateSections TemplateSections { get; set; }
        public long TemplateSectionId { get; set; }
    }
}
