﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
namespace FVGen3.Domain.Entities
{
  public  class PowerBiUserPermissions
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public long Id { get; set; }
        [ForeignKey("PowerBiPermissionId")]
        public virtual PowerBiPermissions PowerBiPermissions { get; set; }
        public long PowerBiPermissionId { get; set; }
        [ForeignKey("OrganizationId")]
        public virtual Organizations Organizations { get; set; }
        public long? OrganizationId { get; set; }
      
        public string UserId { get; set; }
    }
}
