﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.Entities
{
     public class FinancialClientViewModel
    {
        public System.DateTime? DateOfFinancialStatements { get; set; }
        public string SourceOfFinancialStatements { get; set; }
        public System.DateTime? Dateofinterimfinancialstatement
        {
            get;
            set;
        }
        public System.DateTime? LastUpdatedOn { get; set; }
        public string WorkingCapital { get; set; }
        public string DaysOverheadCoveredinWorkingCapital { get; set; }
        public string WorkingCapitalPercenttoRevenue { get; set; }
        public string WorkingCapitalTurnover { get; set; }
        public string CurrentRatio { get; set; }
        public string QuickRatio { get; set; }
        public string DefensiveIntervalRatio { get; set; }
        public string NetOverBillings { get; set; }
        public string NetOverBillingPercentageRevenue { get; set; }
        public string DaysRevenueinReceivables { get; set; }
        public string DaysCash { get; set; }
        public string CashtoOverBillings { get; set; }
        public string CashtoEquity { get; set; }
        public string CashtoRevenue { get; set; }
        public string DebtCoverageRatio { get; set; }
        public string BondingCapacity { get; set; }
        public string BacklogPercentageBondingCapacity { get; set; }
        public string BacklogPercentageRevenue { get; set; }
        public string BacklogGrossProfitGAExpense { get; set; }
        public string MonthsRevenueBacklog { get; set; }
        public string EquityRevenue { get; set; }
        public string InterestBearingDebtEquity { get; set; }
        public string DebtWorkingCapital { get; set; }
        public string TotalLiabilitiesEquity { get; set; }
        public string GrossProfitPercentage { get; set; }
        public string GAPercentage { get; set; }
        public string PretaxEarningsPercentage { get; set; }
        public string PretaxReturnCurrentYearEquity { get; set; }
        public string PretaxReturnCurrentYearAssets { get; set; }
        public string AfterTaxReturnCurrentYearEquity { get; set; }
        public string AfterTaxReturnCurrentYearAssets { get; set; }
        public string EBITDA { get; set; }
        public string AsPercentageRevenue { get; set; }
        public System.DateTime? FinancialYearEndDate
        {
            get { return DateOfFinancialStatements.HasValue ? DateOfFinancialStatements.Value.AddYears(1) : DateOfFinancialStatements; }

        }

    }
}
