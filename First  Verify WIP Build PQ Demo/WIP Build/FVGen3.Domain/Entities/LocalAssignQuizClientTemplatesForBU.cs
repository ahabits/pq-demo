﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.Entities
{
    public class LocalAssignQuizClientTemplatesForBU
    {
        public Guid ClientTemplateId { get; set; }
        public List<CheckBoxesControlForQuizzes> quizBusUnits { get; set; }
    }
    public class CheckBoxesControlForQuizzes
    {
        public string id { get; set; }
        public string Text { get; set; }
        public bool Status { get; set; }
    } 
}
