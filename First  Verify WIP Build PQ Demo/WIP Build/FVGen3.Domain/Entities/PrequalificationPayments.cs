﻿// Page Created Date:  02/24/2015
// Created By: Kiran
// Purpose:
// Version: 1.0

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace FVGen3.Domain.Entities
{
    public class PrequalificationPayments
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long PrequalificationPaymentsId { get; set; }
        public long? PrequalificationId { get; set; }
        public decimal? InvoiceAmount { get; set; }
        public decimal? PaymentReceivedAmount { get; set; }
        public DateTime? PaymentReceivedDate { get; set; }
        public string PaymentTransactionId { get; set; }
        public string TransactionMetaData { get; set; }
        public bool SkipPayment { get; set; } // mani on 4/9/2015
        public int? PaymentCategory { get; set; } // mani on 6/10/2015
        public virtual List<PrequalificationSites> PrequalificationSites { get; set; }
    }
}
