﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.Entities
{
    public class LocalEmployeeDetails
    { 
        public Guid UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        [Required(ErrorMessage="*")]
        public int? DocumentType { get; set; }
        [Required(ErrorMessage = "*")]
        public string Last4Digits { get; set; }
        //[Required(ErrorMessage ="*")]
        //public string PhoneNumber { get; set; }
        [Required(ErrorMessage ="Please check the checkbox by accepting the privacy policy")]
        public bool AckPol { get; set; }
        [Required(ErrorMessage ="*")]
        public string Signature { get; set; }
        public bool HasExistingData { get; set; }
    }
}
