﻿/*
Page Created Date:  05/07/2013
Created By: Siva Bommisetty
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FVGen3.Domain.Entities
{
    public class VendorInviteDetails
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid InvitationId { get; set; }

        public string VendorEmail { get; set; }
        public string VendorFirstName { get; set; }
        public string VendorLastName { get; set; }
        public string ClientCode { get; set; }
        public bool? VendorValidated { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; } // Kiran on 9/19/2014
        public string City { get; set; } // Kiran on 4/14/2014
        public string State { get; set; }
        public string Zip { get; set; }
        public string Country { get; set; }
        public DateTime InvitationDate { get; set; }
        public bool? NotificationRecordExist { get; set; }
        public bool? IgnoredInvitation { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long NotificationReferenceId { get; set; }
        public string VendorCompanyName { get; set; }
        [ForeignKey("ClientId")]
        public virtual Organizations Organization { get; set; }
        public long ClientId { get; set; }
        public int? InvitationSentFrom { get; set; }
        public bool? ShowOnDashboard { get; set; }//sumanth for SMI-268.
        [ForeignKey("SystemUsers")]
        public Guid? InvitationSentByUser { get; set; }
        public virtual SystemUsers SystemUsers { get; set; }

        public string RiskLevel { get; set; }

        public string Comments { get; set; }
        public virtual List<VendorInviteLocations> VendorInviteLocations
        {
            get;
            set;
        }
        public long? VendorId { get; set; }
        public int? InvitationType { get; set; }
        public long? PQId { get; set; }
        public string SendBcc { get; set; }
        public string SendCc { get; set; }
        public long? LanguageId { get; set; }
        public string InvitationCode { get; set; }
        public string CcSendTo { get; set; }
        public string BccSendTo { get; set; }
       
    }
}
