﻿using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;

namespace FVGen3.Domain.Entities
{
    public class PrequalificationClient
    {
        [Key]
        public long Id { get; set; }
        [ForeignKey("Prequalification")]
        public long PQId { get; set; }
        public virtual Prequalification Prequalification { get; set; }
        public string Region { get; set; }
        public string ClientIds { get; set; }

        public IEnumerable<long> ClientIdsList
        {
            get
            {
                return string.IsNullOrEmpty(ClientIds) ? new List<long>(): ClientIds.Split(',').Select(long.Parse);
            }
        }
        public IEnumerable<long> RegionsList
        {
            get
            {

                try { return string.IsNullOrEmpty(Region) ? new List<long>() : Region.Split(',').Select(long.Parse).ToList(); }
                catch
                {
                    return new List<long>();
                }
               
            }
        }
    }
}
