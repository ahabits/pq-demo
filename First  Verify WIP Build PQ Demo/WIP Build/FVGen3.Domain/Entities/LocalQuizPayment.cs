﻿/*
Page Created Date:  11/7/2014
Created By: Kiran Talluri
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.Entities
{
    public class LocalQuizPayment
    {
        public long  ClientId { get; set; }
        public long  VendorId { get; set; }
        public string EmpQuizIds { get; set; }
        public string PaymenyAmount { get; set; }
    }
}
