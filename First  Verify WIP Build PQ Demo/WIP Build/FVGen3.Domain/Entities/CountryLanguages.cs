﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.Entities
{
    public class CountryLanguages
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long CountryLanguageId { get; set; }
        [ForeignKey("Language")]
        public long LanguageId { get; set; }
        public virtual Language Language { get; set; }

        [ForeignKey("Countries")]
        public long CountryId { get; set; }
        public virtual Countries Countries { get; set; }
        public bool? IsDefault { get; set; }
    }
}
