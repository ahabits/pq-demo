﻿/*
Page Created Date:  06/11/2013
Created By: Siva Bommisetty
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FVGen3.Domain.Entities
{
    public class QuestionControlType
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long QuestionTypeID { get; set; }

        public string QuestionType { get; set; }
        public string QuestionTypeDisplayName { get; set; }
        public bool Visible { get; set; }
        public virtual List<QuestionColumnDetails> QuestionColumnDetails { get; set; }
    }
}
