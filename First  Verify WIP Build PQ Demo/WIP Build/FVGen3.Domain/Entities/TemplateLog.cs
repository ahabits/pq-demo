﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FVGen3.Domain.Entities
{
    public class TemplateLog
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        [ForeignKey("Templates")]
        public Guid TemplateId { get; set; }
        public DateTime ChangedDate { get; set; }
        [ForeignKey("SystemUsers")]
        public Guid ChangedBy { get; set; }
        public bool LockedByDefaultVal { get; set; }

        public virtual SystemUsers SystemUsers { get; set; }
        public virtual Templates Templates { get; set; }
    }
}
