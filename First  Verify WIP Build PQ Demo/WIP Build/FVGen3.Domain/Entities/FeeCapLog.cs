﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.Entities
{
    public class FeeCapLog
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        [ForeignKey("Prequalification")]
        public long PrequalificationId { get; set; }
        public decimal PQAmount { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string Notes { get; set; }
        public bool HasAccumulated { get; set; }
        [ForeignKey("OrganizationsPrequalificationOpenPeriod")]
        public long? FeecapPeriodId { get; set; }

        public virtual OrganizationsPrequalificationOpenPeriod OrganizationsPrequalificationOpenPeriod { get; set; }
        public virtual Prequalification Prequalification { get; set; }
    }
}
