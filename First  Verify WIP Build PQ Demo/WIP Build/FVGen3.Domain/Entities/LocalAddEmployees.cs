﻿/*
Page Created Date:  10/22/2014
Created By: Kiran Talluri
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace FVGen3.Domain.Entities
{
    public class LocalAddEmployees
    {
        public string TestFirstName { get; set; }
        public string TestLastName { get; set; }
        public string TestTitle { get; set; }
        [RegularExpression(@"(?=^.{10,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$", ErrorMessageResourceName = "AddNewUserModel_InvalidPassword", ErrorMessageResourceType = typeof(Resources.Resources))]
        public string TestEmail { get; set; }

        public List<EmployeeDetails> Employees { get; set; }
    }
    public class EmployeeDetails
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Title { get; set; }
        public string PhoneNumber { get; set; }
        public int DocumentType { get; set; }
        public string Last4Digits { get; set; }
        public string Email { get; set; }
        public bool UserHasEmail { get; set; }
    }
}
