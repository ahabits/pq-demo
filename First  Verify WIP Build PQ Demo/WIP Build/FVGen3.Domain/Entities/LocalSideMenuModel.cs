﻿/*
Page Created Date:  06/11/2013
Created By: Rajesh Pendela
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.Entities
{
    public class LocalSideMenuModel
    {
        public LocalSideMenuModel()
        {
        }
        public LocalSideMenuModel(SystemSideMenus menus)
        {
            this.menuName = menus.SysMenuNameSM;
            this.actionName = menus.ActionNameSM;
            this.controllerName = menus.ControllerNameSM;
            this.isChecked = false;
            this.menuDisplayname = menus.SystemSideMenuName;
            this.DisplayOrder = menus.DisplayOrder;
            
        }
        public LocalSideMenuModel(string menuDisplayname,string menuName, string actionName, string ctrlName, bool isChecked)
        {
            this.menuName = menuName;
            this.actionName = actionName;
            this.controllerName = ctrlName;
            this.isChecked = isChecked;
            this.menuDisplayname = menuDisplayname;
        }
        public LocalSideMenuModel(string menuName, string actionName, string ctrlName, bool isChecked)
        {
            this.menuName = menuName;
            this.actionName = actionName;
            this.controllerName = ctrlName;
            this.isChecked = isChecked;
            this.menuDisplayname = menuName;
        }
        public bool isChecked { get; set; }
        public string menuName { get; set; }
        public string actionName { get; set; }
        public string controllerName { get; set; }
        public string menuDisplayname { get; set; }
        public int? DisplayOrder { get; set; }
        public List<ViewAndPermissions> ViewsAndPermissions { get; set; }
    }
    public class ViewAndPermissions
    {
        public string ViewPermission { get; set; }
        public string ViewName { get; set; }
        public string ControllerName { get; set; }
    }
}
