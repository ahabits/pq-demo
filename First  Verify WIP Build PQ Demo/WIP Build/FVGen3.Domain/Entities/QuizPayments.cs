﻿
//Page Created Date: 04/29/2015
//Created By: Kiran Talluri
//Purpose:
//Version: 1.0

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;


namespace FVGen3.Domain.Entities
{
    public class QuizPayments
    {
        [Key]
        public long QuizPaymentId { get; set; }

        [ForeignKey("SystemUsersOrganizationsQuizzes")]
        public long SysUserOrgQuizId { get; set; }
        public virtual SystemUsersOrganizationsQuizzes SystemUsersOrganizationsQuizzes { get; set; }

        public decimal? PaymentReceivedAmount { get; set; }
        public DateTime? PaymentReceivedDate { get; set; }
        public string PaymentTransactionId { get; set; }
        public string TransactionMetaData { get; set; }
        public bool? SkipPayment { get; set; }
        public bool? IsOpen { get; set; }
        public DateTime? SubmittedDate { get; set; }
        public DateTime? ExpirationDate { get; set; }
        public bool? IsZeroFeeTraining { get; set; }
        public bool? IsFeeCapReached { get; set; }
        public virtual List<QuizPaymentsItems> QuizPaymentsItems { get; set; }
    }
}
