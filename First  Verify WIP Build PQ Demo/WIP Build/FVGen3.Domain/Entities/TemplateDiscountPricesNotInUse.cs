﻿/*
Page Created Date:  08/06/2013
Created By: Suma
Purpose:
Version: 1.0
****************************************************
*/


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FVGen3.Domain.Entities
{
    public class TemplateDiscountPricesNotInUse
    {
        [Key]
        public long TemplateDiscountId { get; set; }
        public Decimal DiscountedPrice { get; set; }
        public int DiscountType { get; set; }
        public int MinNoOfPrequalification { get; set; }
        public int MaxNoOfPrequalification { get; set; }

        [ForeignKey("TemplatePrices")]
        public long TemplatePriceId { get; set; }
        public virtual TemplatePrices TemplatePrices { get; set; }



    }
}
