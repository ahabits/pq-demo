﻿/*
*****************************************************
Page Created Date:  10/7/2014
Created By: Kiran Talluri
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace FVGen3.Domain.Entities
{
    public class LocalQuizIFrameMeta
    {   
        public string IFrameURL { get; set; }
        public string Height { get; set; }
        public string Width { get; set; }             
    }
}
