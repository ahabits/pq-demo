﻿/*
Page Created Date:  06/11/2013
Created By: Siva Bommisetty
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FVGen3.Domain.Entities
{
    public class SystemUsersOrganizations
    {
        [Key]
        public long SysUserOrganizationId { get; set; }

        [ForeignKey("SystemUsers")]
        public Guid UserId { get; set; }
        public virtual SystemUsers SystemUsers { get; set; }

        [ForeignKey("Organizations")]
        public long OrganizationId { get; set; }
        public virtual Organizations Organizations { get; set; }

        public System.Nullable<bool> PrimaryUser { get; set; }
        public virtual List<SystemUsersOrganizationsRoles> SystemUsersOrganizationsRoles { get; set; }
        public virtual List<SystemUsersOrganizationsQuizzes> SystemUsersOrganizationsQuizzes { get; set; } //Siva on 29th Mar 2014
        public virtual List<SystemUsersOrganizationsCategories> SystemUsersOrganizationsCategories { get; set; } // Kiran on 02/16/2015
        //public virtual List<SystemUsersOrganizationsEmpChecklist> SystemUsersOrganizationsEmpChecklist { get; set; }

    }
}
