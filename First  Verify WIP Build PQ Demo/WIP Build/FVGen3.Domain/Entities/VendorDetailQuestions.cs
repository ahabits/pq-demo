﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.Entities
{
    public class VendorDetailQuestions
    {
        [Key]
        public int ID
        {
            get;
            set;
        }
        public Guid TemplateId
        {
            get;
            set;
        }
        [ForeignKey("Questions")]
        public long QuestionId
        {
            get;
            set;
        }
        public int Type
        {
            get;
            set;
        }
        public decimal? Value
        {
            get;
            set;
        }
        public bool? IsMax
        {
            get;
            set;
        }
        public virtual Questions Questions { get; set; }
    }
}
