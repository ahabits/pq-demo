﻿/*
Page Created Date:  08/29/2013
Created By: Siva Bommisetty
Purpose: To compose and send email to Vendors
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace FVGen3.Domain.Entities
{
    public class LocalEmailComposeModel
    {
        public int TemplateId { get; set; }
        public long PrequalificationId { get; set; }

        [Required(ErrorMessage = "*")]
        //[StringLength(50, ErrorMessage = "Cannot exceed 50 chars")]
        //[RegularExpression(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$", ErrorMessageResourceName = "VendorRegistrationModel_InvalidEmail", ErrorMessageResourceType = typeof(Resources.Resources))]
        [RegularExpression(@"^(([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,8}|[0-9]{1,3})(\]?)(\s*,\s*|\s*$))*", ErrorMessageResourceName = "Common_Email_Invalid_ErrorMessage", ErrorMessageResourceType = typeof(Resources.Resources))]
        public string ToAddress { get; set; }

        // Kiran on 9/15/2014
        [RegularExpression(@"^(([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,8}|[0-9]{1,3})(\]?)(\s*,\s*|\s*$))*", ErrorMessageResourceName = "Common_Email_Invalid_ErrorMessage", ErrorMessageResourceType = typeof(Resources.Resources))]
        //[RegularExpression(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$", ErrorMessageResourceName = "VendorRegistrationModel_InvalidEmail", ErrorMessageResourceType = typeof(Resources.Resources))]
        public string CCAddress { get; set; }

        [Required(ErrorMessage = "*")]
        public string Subject { get; set; }

        [Required(ErrorMessage = "*")]        
        [MaxLength(3500, ErrorMessageResourceName = "ErrorMessage_Exceed3500", ErrorMessageResourceType = typeof(Resources.Resources))]
        [AllowHtml]
        public string Body { get; set; }

        public bool SendCopyToMySelf { get; set; }

        public string attachments { get; set; }

        public string EmailComment { get; set; }

        // Kiran on 8/26/2014
        [RegularExpression(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$", ErrorMessageResourceName = "VendorRegistrationModel_InvalidEmail", ErrorMessageResourceType = typeof(Resources.Resources))]
        public string BCCAddress { get; set; }
        // Ends<<<
        public List<LocalAttachmetList> AttachmentsList { set; get; }
        public long SubSectionId { get; set; }
        public Guid LoginUser { get; set; }
    }
    public class LocalAttachmetList
    {
        public String AttachmentName { get; set; }
        public Guid AttachmentId { get; set; }
        public int isActive { get; set; }
    }
}
