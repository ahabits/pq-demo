﻿/*
Page Created Date:  05/18/2013
Created By: Suma
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.Entities
{
    public class PrequalificationStatus
    {

        public long PrequalificationStatusId {get; set;}

        public string PrequalificationStatusName {get; set;}

        public string Color {get; set;}
        public int PeriodLength { get; set; }
        public string PeriodLengthUnit { get; set; }
        public string Description { get; set; }
        // Kiran on 12/10/2013
        public string IndicationTextAdmin { get; set; }
        public string IndicationTextClient { get; set; }
        public string IndicationTextVendor { get; set; }
        public bool? ShowInDashBoard { get; set; }
        public string BannerColor { get; set; } // Kiran on 3/17/2014
        public int DisplayOrder { get; set; } 
        // Ends<<<
        public virtual List<PrequalificationSites> PrequalificationSites { get; set; }  // Kiran on 02/27/2015
    }
}
