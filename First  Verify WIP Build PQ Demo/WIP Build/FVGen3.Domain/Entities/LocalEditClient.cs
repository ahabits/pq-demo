﻿/*
Page Created Date:  06/11/2013
Created By: Siva Bommisetty
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace FVGen3.Domain.Entities
{
    public class LocalEditClient
    {
        [Required(ErrorMessage = "*")]
        public long OrganizationID { get; set; }

        [Required(ErrorMessage = "*")]
        public string Name { get; set; }

        [Required(ErrorMessage = "*")]
        public string Address1 { get; set; }

        public string Address2 { get; set; }

        [Required(ErrorMessage = "*")]
        public string City { get; set; }

        [Required(ErrorMessage = "*")]
        public string State { get; set; }

        [Required(ErrorMessage = "*")]
        public string Zip { get; set; }

        [Required(ErrorMessage = "*")]
        public string Country { get; set; }

        [Required(ErrorMessage = "*")]
        public string PhoneNumber { get; set; }

        public string FaxNumber { get; set; }
        public string WebsiteURL { get; set; }
        public string FederalIDNumber { get; set; }

        [Required(ErrorMessage = "*")]
        public string OrganizationType { get; set; }

        public bool isSuccess { get; set; }
        
        public bool TabStatus { get; set; }

        public virtual List<ClientTemplates> ClientTemplates { get; set; }
        
    }
}
