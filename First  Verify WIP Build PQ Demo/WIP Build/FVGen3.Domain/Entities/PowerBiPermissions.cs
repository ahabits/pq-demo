﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text; 

namespace FVGen3.Domain.Entities
{
  public  class PowerBiPermissions
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public long Id { get; set; }
        [ForeignKey("PowerBiId")]
        public virtual PowerBiReports PowerBiReports { get; set; }
        public long PowerBiId { get; set; }
        public int PermissionType { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid? ModifiedBy { get; set; }
        public virtual List<PowerBiUserPermissions> PowerBiUserPermissions { get; set; }
    }
}
