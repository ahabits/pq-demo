﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace FVGen3.Domain.Entities
{
    public class TemplateSubSectionsPreDefinedFields
    {
        [Key]
        public long TableRecordId { get; set; }

        public string TableRefName { get; set; }
        public string TableRefField { get; set; }
        public string ReferenceLabel { get; set; }

        public string PredefinedKeyWord { get; set; }
        //[ForeignKey("SubSectionId")]
        //public virtual TemplateSubSections TemplateSubSections { get; set; }
        //public long SubSectionId { get; set; }

    }
}
