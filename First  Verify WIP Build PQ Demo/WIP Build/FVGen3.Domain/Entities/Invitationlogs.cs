﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.Entities
{
   public class Invitationlogs
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
       
        public Guid InvitationId { get; set; }
        public string OldComment { get; set; }
        public string NewComment { get; set; }
        public DateTime? Modifieddate { get; set; }
        public Guid ChangedBy { get; set; }
        public DateTime? Createddate { get; set; }
       
    }
}
