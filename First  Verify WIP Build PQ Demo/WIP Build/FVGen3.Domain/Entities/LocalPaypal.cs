﻿/*
Page Created Date:  12/14/2013
Created By: Kiran Talluri
Purpose: 
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FVGen3.Domain.Entities
{
    public class LocalPaypal
    {
        public LocalPaypal()
        { }

        public string cmd { get; set; }
        public string business { get; set; }
        public string no_shipping { get; set; }
        public string @return { get; set; }
        public string cancel_return { get; set; }
        public string notify_url { get; set; }
        public string currency_code { get; set; }
        public string item_name { get; set; }
        public string amount { get; set; }
        public long preqID { get; set; } // Kiran on 7/31/2014
        public string custom { get; set; } // Kiran on 02/24/2015
    }
}