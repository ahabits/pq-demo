﻿/*
Page Created Date:  05/18/2013
Created By: Suma
Purpose:
Version: 1.0
****************************************************
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;

namespace FVGen3.Domain.Entities
{
    public class LocalVendorAwaiting
    {
        public string VendorOrgName { get; set; }
        public DateTime SubmittedDate { get; set; }
        public string ClientOrgName { get; set; }
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }    
    }
}
