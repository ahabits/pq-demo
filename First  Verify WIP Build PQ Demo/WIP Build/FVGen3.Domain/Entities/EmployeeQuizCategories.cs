﻿/*
*****************************************************
Page Created Date:  02/16/2015
Created By: Kiran Talluri
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FVGen3.Domain.Entities
{
    public class EmployeeQuizCategories
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long EmpQuizCategoryId  { get; set; }
        public string CategoryName { get; set; }
        public int? CategoryType { get; set; }
        public int? SubCategoryControlType { get; set; }
        public string SubCategoriesValue { get; set; }
        public int? AccessType { get; set; } // Kiran on 03/13/2015

        public virtual List<SystemUsersOrganizationsCategories> SystemUsersOrganizationsCategories { get; set; }
        public virtual List<SystemUsersOrganizationsQuizzesCategories> SystemUsersOrganizationsQuizzesCategories { get; set; }
    }
}
