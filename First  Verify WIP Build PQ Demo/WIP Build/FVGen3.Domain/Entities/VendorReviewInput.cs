﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FVGen3.Domain.Entities
{
   public class VendorReviewInput
    {
       [Key]
       public long VendorReviewInputId { get; set; }

       [ForeignKey("VendorId")]
       public virtual Organizations Organization { get; set; }
       public long VendorId { get; set; }

       [ForeignKey("SystemUsers")]
       public Guid InputUserId { get; set; }
       public virtual SystemUsers SystemUsers { get; set; }

       public int UserReviewNumber { get; set; }
       public int? ReviewType { get; set; }
       public DateTime? ReviewDate { get; set; }
       public string ProjectName { get; set; }
       public string ProjectLocation { get; set; }
       public string ContractWONumber { get; set; }
       public decimal UserRating { get; set; }
       public bool? DoYouRecommend { get; set; }
       public string Comments { get; set; }
       public long? ClientId { get; set; }
       public long? PrequalificationID { get; set; }
       public long? BuId { get; set; }

       public virtual List<VendorReviewInputAnswers> VendorReviewInputAnswers { get; set; } // mani on 6/4/2015
       public virtual List<VendorReviewInputSection3Ans> VendorReviewInputSection3Ans { get; set; }
    }
}
