﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.Entities
{
    public class EmailTemplateContent
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public Guid Id { get; set; }
        public int EmailTemplateId{get;set;}
        public long LanguageId { get; set; }
        public string EmailSubject { get; set; }
        public string EmailBody{get;set;}
        public string Comments { get; set; }
    }
}
