﻿/*
Page Created Date:  03/28/2014
Created By: Pendela Rajesh 
Purpose:
Version: 1.0
****************************************************
*/

using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace FVGen3.Domain.Entities
{
    public class PrequalificationEMRStatsValues
    {
        
        
        [Key]
        public long PrequalEMRStatsValuesId { get; set; }


        [ForeignKey("PrequalificationEMRStatsYears")]
        public long PrequalEMRStatsYearId { get; set; }
        public virtual PrequalificationEMRStatsYears PrequalificationEMRStatsYears { get; set; }

        [ForeignKey("Questions")]
        public long QuestionId { get; set; }
        public virtual Questions Questions { get; set; }

        [ForeignKey("QuestionColumnDetails")]
        public long QuestionColumnId { get; set; }
        public virtual QuestionColumnDetails QuestionColumnDetails { get; set; }

        
        
        public string QuestionColumnIdValue { get; set; }
        
    }
}