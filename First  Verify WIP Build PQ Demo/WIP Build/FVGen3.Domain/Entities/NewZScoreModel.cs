﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.Entities
{
   public  class NewZScoreModel
    {
        public System.DateTime? DateOfFinancialStatement { get; set; }
        public String FormattedValue
        {
            get
            {
                return FinancialYearEndDate.HasValue
                    ? FinancialYearEndDate.Value.ToString("MM/dd/yy")
                    : ""; // return an empty string if SelectedDate is null
            }
        }
        public System.DateTime? FinancialYearEndDate
        {
            get { return DateOfFinancialStatement.HasValue ? DateOfFinancialStatement.Value.AddYears(1) : DateOfFinancialStatement; }

        }
        public System.DateTime? LastUpdatedOn { get; set; }
        public decimal EarningsBeforeIntAndTax { get; set; }
        public decimal TotalAssets { get; set; }
        public decimal TotalLiabilities { get; set; }
        public decimal NetSales { get; set; }
        public decimal Equity { get; set; }
        public decimal workingCapital { get; set; }
        public decimal RetainedEarnings { get; set; }
        public decimal ReturnOnTotalAssets { get; set; }
        public decimal SalesOfTotalAssets { get; set; }
        public decimal EquityToDebt { get; set; }
        public decimal WorkingCapitalToTotalAssets { get; set; }
        public decimal RetainedEarningsToTotalAssets { get; set; }
        public decimal CalculationForReturnOnTotalAssets { get; set; }
        public decimal CalculationForSalesOfTotalAssets { get; set; }
        public decimal CalculationForEquityToDebt { get; set; }
        public decimal CalculationForWorkingCapitalToTotalAssets { get; set; }
        public decimal CalculationForRetainedEarningsToTotalAssets { get; set; }
        public decimal ZScore { get; set; }

        public string WeightFactorForReturnOnTotalAssets { get; set; }
        public string WeightFactorForSalesOfTotalAssets { get; set; }
        public string WeightFactorForEquityToDebt { get; set; }
        public string WeightFactorForWorkingCapitalToTotalAssets { get; set; }
        public string WeightFactorForRetainedEarningsToTotalAssets { get; set; }

    }
}
