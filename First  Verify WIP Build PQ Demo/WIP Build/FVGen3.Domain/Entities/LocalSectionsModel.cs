﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace FVGen3.Domain.Entities
{
    public class LocalSectionsModel
    {
        [Required(ErrorMessage="*")]
        [StringLength(50, ErrorMessageResourceName = "ErrorMessage_Exceed50", ErrorMessageResourceType = typeof(Resources.Resources))]
        public string SectionName { get; set; }
        public Guid Templateid { get; set; }
        public bool VisibleToClient { get; set; }
        public bool Visible { get; set; }
        public int SectionType { get; set; }
        public long TemplateSectionID { get; set; }
        public string OtherSectionTypes { get; set; }
        public List<SelectListItem> Clients { get; set; }//Rajesh on 1/20/2014
        public int Permission { get; set; }//Rajesh on 2/11/2014
        //[Required(ErrorMessage = "*")]
        public string Signed { get; set; }
        //[Required(ErrorMessage = "*")]
        public string Title { get; set; }
        public bool HideFromPrequalification { get; set; }
    }
}
