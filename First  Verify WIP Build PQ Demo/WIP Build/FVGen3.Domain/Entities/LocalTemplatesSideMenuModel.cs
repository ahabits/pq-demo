﻿/*
Page Created Date:  06/11/2013
Created By: Rajesh Pendela
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.Entities
{
    public class LocalTemplatesSideMenuModel
    {
        public LocalTemplatesSideMenuModel(string menuName, string actionName, string ctrlName, bool isChecked,string param)
        {
            this.menuName = menuName;
            this.actionName = actionName;
            this.controllerName = ctrlName;
            this.isChecked = isChecked;
            this.param = param;
        }
        public bool? isSectionCompleted { get; set; }
        public bool isChecked { get; set; }
        public string menuName { get; set; }
        public string actionName { get; set; }
        public string controllerName { get; set; }
        public string param { get; set; }
        public bool? isVendorNoPaymentNoAccess { get; set; }
        public bool isVisible { get; set; }
    }
}
