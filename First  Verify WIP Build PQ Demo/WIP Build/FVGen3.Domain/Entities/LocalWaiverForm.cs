﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.Entities
{
    public class LocalWaiverForm
    {
        public long quizId { get; set; }
        [Required(ErrorMessage = "*")]
        public string VendorCompany { get; set; }
        [Required(ErrorMessage = "*")]
        public string Signature { get; set; }
        [Required(ErrorMessage = "*")]
        public DateTime Date { get; set; }
        public string Content { get; set; }
        public string EmployeeName { get; set; }
        public Guid UserId { get; set; } 
    }
}
