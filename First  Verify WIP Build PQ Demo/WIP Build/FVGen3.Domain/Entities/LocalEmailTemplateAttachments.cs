﻿/*
Page Created Date:  06/11/2013
Created By: Siva Bommisetty
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FVGen3.Domain.Entities
{
    public class LocalEmailTemplateAttachments
    {
        //[Required(ErrorMessage = "*")]
        public string FileName { get; set; }

        public System.Nullable<int> EmailTemplateID { get; set; }

        public System.Nullable<int> attachmentId { get; set; }
        
    }
}
