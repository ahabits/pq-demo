﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace FVGen3.Domain.Entities
{
    public class LocalPrequalificationComments
    {
        public long? PrequalificationId { get; set; }
        

        
        public long? TemplateSectionID { get; set; }
        
        public long? SubSectionID { get; set; }
        
        public Guid? CommentsBy { get; set; }
       
        [Required(ErrorMessageResourceName = "ErrorMessage_Required_Comments", ErrorMessageResourceType = typeof(Resources.Resources))]
        [StringLength(500, ErrorMessageResourceName = "ErrorMessage_Exceed500", ErrorMessageResourceType = typeof(Resources.Resources))]
        public string Comments { get; set; }
        public DateTime CommentsDate { get; set; }
        public DateTime CommentsTime { get; set; }
    }
}
