﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.Entities
{
    public class OrganizationRoles
    {
        [Key]
        public long Id { get; set; }
        [ForeignKey("Organizations")]
        public long OrganizationId { get; set; }
        [ForeignKey("SystemRoles")]
        public string RoleId { get; set; }


        public virtual SystemRoles SystemRoles { get; set; }
        public virtual Organizations Organizations { get; set; }
    }
}
