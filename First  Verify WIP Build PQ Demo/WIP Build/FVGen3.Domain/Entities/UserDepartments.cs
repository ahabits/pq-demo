﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.Entities
{
    public class UserDepartments
    {[Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [ForeignKey("SystemUsers")]
        public Guid UserId { get; set; }  
        [ForeignKey("Department")]
        public int DepartmentId { get; set; }
        public virtual Department Department { get;set;}
        public virtual SystemUsers SystemUsers { get; set; }
    }
}
