﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace FVGen3.Domain.Entities
{
    public class LocalAlternativeVideos
    {
        public Guid TemplateId { get; set; }
        public long TemplateSectionId { get; set; }
        public string TemplateName { get; set; }
        
        [Required(ErrorMessage="*")]
        public string File { get; set; }

        [Required(ErrorMessage="*")]
        public string FileName { get; set; }
    }
}
