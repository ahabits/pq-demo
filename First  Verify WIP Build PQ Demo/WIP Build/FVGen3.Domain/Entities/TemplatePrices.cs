﻿/*
Page Created Date:  08/06/2013
Created By: Suma
Purpose:
Version: 1.0
****************************************************
*/


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FVGen3.Domain.Entities
{
     public class TemplatePrices
    {
        [Key]
         public long TemplatePriceId { get; set; }
         public Decimal TemplatePrice { get; set; }

         
         [ForeignKey("Templates")]
         public Guid TemplateID { get; set; }
         public virtual Templates Templates { get; set; }

         public virtual List<TemplateDiscountPricesNotInUse> TemplateDiscountPrices { get; set; }
         
    }
}
