﻿/*
Page Created Date:  11/25/2014
Created By: Kiran talluri
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.Entities
{
    public class LocalEmployeeUpdateTrainings
    {
        public long sysUserOrgId { get; set; }

        public List<CheckBoxesControlForEmployeeQuizzes> employeeTrainings { get; set; }
        public List<Boolean> TrainingsList { get; set; }

        //public List<CheckBoxControlForAssigningTrainingsToEmployee> trainingsToBeAssignedToEmployee { get; set; }
    }
    public class CheckBoxesControlForEmployeeQuizzes
    {
        public string id { get; set; }
        public string Text { get; set; }
        public bool employeeTakenTraining { get; set; }
        public bool status { get; set; }
        public bool assignedStatus { get; set; }
        public string clientTemplateID { get; set; }
        public string clientOrgName { get; set; }
    }
    //public class CheckBoxControlForAssigningTrainingsToEmployee
    //{
    //    public string clientTemplateID { get; set; }
    //    public string Text { get; set; }
    //    public bool statusForTrainingToAssign { get; set; }
    //}
}
