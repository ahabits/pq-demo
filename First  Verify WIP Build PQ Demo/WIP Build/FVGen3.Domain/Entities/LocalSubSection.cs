﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.Entities
{
    public class LocalSubSection
    {
        public string SubSectionType { get; set; }
        public string SubSectionName { get; set; }
        public string SubSectionNote { get; set; }
        public int NoOfColumns { get; set; }
        public long SectionId { get; set; }
        public string Dynamic_Condition { get; set; }
        public string Predefined_Condition { get; set; }
        public string Reporting_Condition { get; set; }
        public long SubSecId { get; set; }
        public string SubSectionTypeCondition { get; set; }
        public bool isSubSubSection { get; set; }
        public bool Visible { get; set; }
        public bool NotificationExist { get; set; }
        public int PermissionType { get; set; }//Rajesh on 2/11/2014
        public int InVisibleToClientSection { get; set; } // Kiran on 4/22/2014
    }
}
