﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace FVGen3.Domain.Entities
{
    public class Visitors
    {

        [Key]
        public Guid VisitorID { get; set; }        
        public int ClientID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Company { get; set; }
        public string SiteContact { get; set; }
        public int? ClientBusinessUnitSiteID { get; set; }
        public int? ClientBusinessUnitID { get; set; }
    }
}

