﻿/*
Page Created Date:  11/22/2013
Created By: kiran Talluri
Purpose:
Version: 1.0
****************************************************
*/

using System;

using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace FVGen3.Domain.Entities
{
    public class PrequalificationStatusChangeLog
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long PrequalStatusLogId { get; set; }

        [ForeignKey("Prequalification")]
        public long PrequalificationId { get; set; }
        public virtual Prequalification Prequalification { get; set; }

        [ForeignKey("PrequalificationStatus")]
        public long PrequalificationStatusId { get; set; }
        public virtual PrequalificationStatus PrequalificationStatus { get; set; }

        public DateTime StatusChangeDate { get; set; }

        // Kiran on 10/02/2014
        [ForeignKey("StatusChangedByUser")]
        public virtual SystemUsers StatusChangedBySystemUser { get; set; }
        public Guid? StatusChangedByUser { get; set; }
        // Ends<<<

        public DateTime PrequalificationStart { get; set; }
        public DateTime PrequalificationFinish { get; set; }
      //  public virtual PrequalificationSites PrequalificationSites { get; set; }
      public DateTime? CreatedDate { get; set; }
    public string Comments { get; set; } // Kiran on 04/10/2015
    }
}
