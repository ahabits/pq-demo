﻿/*
Page Created Date:  07/06/2013
Created By: Kiran Talluri
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.Entities
{
    public class LocalManageSites
    {        
        public long ClientId { get; set; }
        public List<ClientBusinessUnits> ClientBusinessUnits{ get; set; }
    }
}
