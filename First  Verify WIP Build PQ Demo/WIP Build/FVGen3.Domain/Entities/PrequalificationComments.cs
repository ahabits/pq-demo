﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FVGen3.Domain.Entities
{
    public class PrequalificationComments
    {
        [Key]
        public long CommentsRecordId { get; set; }
        [ForeignKey("Prequalification")]
        public long? PrequalificationId { get; set; }
        public virtual Prequalification Prequalification { get; set; }

        [ForeignKey("TemplateSections")]
        public long? TemplateSectionID { get; set; }
        public virtual TemplateSections TemplateSections { get; set; }

        [ForeignKey("TemplateSubSections")]
        public long? SubSectionID { get; set; }
        public virtual TemplateSubSections TemplateSubSections { get; set; }

        [ForeignKey("SystemUsers")]
        public Guid? CommentsBy { get; set; }
        public virtual SystemUsers SystemUsers { get; set; }
        
        public string Comments { get; set; }
        public DateTime CommentsDate { get; set; }
        public TimeSpan CommentsTime { get; set; }
    }
}
