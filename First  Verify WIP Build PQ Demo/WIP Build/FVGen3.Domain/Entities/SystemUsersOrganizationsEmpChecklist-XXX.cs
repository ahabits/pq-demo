﻿/*
Page Created Date:  11/04/2014
Created By: Kiran Talluri
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace FVGen3.Domain.Entities
{
    public class SystemUsersOrganizationsEmpChecklistXXXX
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long SysUsersOrgEmpChkID { get; set; }
        [ForeignKey("SysUserOrganizationId")]
        public virtual SystemUsersOrganizations SystemUsersOrganizations { get; set; }
        public long SysUserOrganizationId { get; set; }
        
        [ForeignKey("CheckListStatusID")]
        public virtual CheckListStatus CheckListStatus { get; set; }
        public long CheckListStatusID { get; set; }
    }
}
