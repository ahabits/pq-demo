﻿
using System;
using System.Web.Mvc;

namespace FVGen3.Domain.Entities
{
    public class AddendumModel
    {
        public long PQId { get; set; }
        public long SubSectionId { get; set; }
        public string SectionName { get; set; }
        [AllowHtml]
        public string Header { get; set; }
        [AllowHtml]
        public string Footer { get; set; }
        public long PQSubSectionId { get; set; }
        public Guid UserId { get; set; }
    }
}
