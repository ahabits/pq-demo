﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.Entities
{
  public class MultipleInvitationCode
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public Guid Id { get; set; }
        //[ForeignKey("ClientId")]
        //public virtual Organizations Organizations { get; set; }
        public long ClientId { get; set;}
        public Guid TemplateId { get; set; } 
        public string ClientCode { get; set;}
        public string TemplateName { get; set; }
        public string ClientName { get; set; }
     
    }
}
