﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.WebUI.Controllers
{
    public class LocalInitialTrainingPayment
    {
        public string EmpQuizIds { get; set; }
        public decimal PaymentAmount { get; set; }
        public string custom { get; set; }
        public Guid PaymentId { get; set; } // Kiran on 12/03/2015
        public string ClientName { get; set; }
        public string EmployeeName { get; set; }
        public string VendorName { get; set; }
    }
}
