﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.Entities
{
    public class InviteTemplateConfiguration
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; } 
        [ForeignKey("ClientId")]
        public virtual Organizations Organizations { get; set; }
        public long ClientId { get; set; }
        [ForeignKey("TemplateId")]
        public virtual EmailTemplates EmailTemplates { get; set; }
        public int? TemplateId { get; set; }
        public int? InvitationType{get;set;}
       
    }
}
