﻿
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace FVGen3.Domain.Entities
{
    public class AgreementSubSectionConfiguration
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public long SubSectionId { get; set; }
        public string SendNotificationTo { get; set; }
        [ForeignKey("CreatedBy")]
        public virtual SystemUsers Created { get; set; } 
        public Guid CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        [ForeignKey("UpdatedBy")]
        public virtual SystemUsers Updated { get; set; } 
        public Guid? UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        [ForeignKey("EmailTemplates")]
        public int? EmailTemplateId { get; set; }
        public virtual EmailTemplates EmailTemplates { get; set; } 
    }
}
