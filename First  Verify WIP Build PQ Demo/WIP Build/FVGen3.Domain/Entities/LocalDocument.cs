﻿/*
Page Created Date:  06/11/2013
Created By: Siva Bommisetty
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.Entities
{
    public class LocalDocument
    {
        public List<Document> documents { get; set; }

        public int TemplateOrBUnit { get; set; }

        public string Category { get; set; }
    }
}
