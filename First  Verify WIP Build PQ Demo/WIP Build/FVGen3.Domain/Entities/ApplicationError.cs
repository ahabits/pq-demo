﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.Entities
{
  public  class ApplicationError
    {
        public int id { get; set; }
        public string ControllerName
        {
            get;
            set;
        }
        public string ActionName
        {
            get;
            set;
        }

        /*SessionDetail*/
        public bool? HasSession
        {
            get;
            set;
        }
        public string Session
        {
            get;
            set;
        }

        /*RequestDetail*/
        //public Dictionary<string, string> RequestHeader
        //{
        //    get;
        //    set;
        //}
        public string RequestData
        {
            get;
            set;
        }

        /*ExceptionDetail*/

        public string Exception
      {
          get;
           set;
        }
        public DateTime CreatedDateTime
        {
            get;
            set;
        }
        public string Type
        {
            get;
            set;
        }
    }
}
