﻿/*
Page Created Date:  05/22/2013
Created By: Kiran Talluri
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace FVGen3.Domain.Entities
{
    public class LocalUserSearchModel
    {
        [StringLength(100, ErrorMessageResourceName = "ErrorMessage_Exceed100", ErrorMessageResourceType = typeof(Resources.Resources))]
        public string FirstName { get; set; }

        [StringLength(100, ErrorMessageResourceName = "ErrorMessage_Exceed100", ErrorMessageResourceType = typeof(Resources.Resources))]
        public string LastName { get; set; }

        [StringLength(256, ErrorMessageResourceName = "ErrorMessage_Exceed256", ErrorMessageResourceType = typeof(Resources.Resources))]
        public string Email { get; set; }

        public bool SearchType { get; set; }
        public bool UserStatus_Active { get; set; }
        public bool UserStatus_InActive { get; set; }

        public long ClientId { get; set; }
        public long VendorId { get; set; }
        public long AdminId { get; set; }
        public List<SystemUsersOrganizations> SystemUsersOrganizations { get; set; }
    }
}
