﻿/*
Page Created Date:  05/01/2013
Created By: Siva Bommisetty
Purpose:
Version: 1.0
****************************************************
*/

using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace FVGen3.Domain.Entities
{
    public class Contact
    {

        //>>DV:SB,KT
        [Key]
        public long ContactId { get; set; }

        //public Guid UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string PhoneNumberInternational { get; set; }
        public string MobileNumber { get; set; }
        public string ContactTitle { get; set; }
        public string AdditionalDescription { get; set; }
        public int? DocumentType { get; set; }
        public string DriversLicenseLast4Digits { get; set; }
        public string GreenCardLast4Digits { get; set; }
        public string SocialSecurityNumberLast4Digits { get; set; }
        public string PhoneNumberLast4Digits { get; set; }
        //<<DV:SB,KT

        //    >>DV:SB,KT
        //Foreign key to Contact
        [ForeignKey("SystemUsers")]
        public Guid UserId { get; set; }
        public virtual SystemUsers SystemUsers { get; set; }
        //<<DV:SB,KT

        public string ExtNumber { get; set; } // kiran on 8/26/2014
        [NotMapped]
        public string FullName { get { return LastName.Trim() + ", " + FirstName.Trim(); } }

    }
}