﻿/*
Page Created Date:  01/20/2015
Created By: Kiran Talluri
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.Entities
{
    public class LocalEmployeeGuideDocumentsGroupedByClient
    {
        public LocalEmployeeGuideDocumentsGroupedByClient() {
            FileNames = new List<string>();
        }
        public string key { get; set; }
        public List<string> FileNames { get; set; }
        public long? OrganizationId { get; set; }
        public bool? qualified { get; set; } // kiran on 02/03/2015
        public Prequalification prequalifcation { get; set; }
    }
}
