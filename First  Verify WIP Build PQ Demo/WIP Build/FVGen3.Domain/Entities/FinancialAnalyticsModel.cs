﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace FVGen3.Domain.Entities
{
    public class FinancialAnalyticsModel
    {
        public FinancialAnalyticsModel()
        {
            IsDisabled = false;
            SourceOfFinancialStatements = "";
        }
        [Key]
        public int Id
        {
            get;
            set;
        }
        public long? VendorId
        {
            get;
            set;
        }
        public System.DateTime? DateOfFinancialStatements
        {
            get;
            set;
        }
        public System.DateTime? LastUpdatedOn
        {
            get;
            set;
        }
        public Guid? LastUpdatedBy
        {
            get;
            set;
        }
        public System.DateTime? Dateofinterimfinancialstatement
        {
            get;
            set;
        }
        [NotMapped]
        public System.DateTime? FinancialYearEndDate
        {
            get { return DateOfFinancialStatements.HasValue? DateOfFinancialStatements.Value.AddYears(1):DateOfFinancialStatements; }
            
        }
        public string SourceOfFinancialStatements
        {
            get;
            set;
        }
        public decimal? CashAndCashEquivalents
        {
            get;
            set;
        }
        public decimal? Investments
        {
            get;
            set;
        }
        public decimal? ReceivablesCurrent
        {
            get;
            set;
        }
        public decimal? ReceivableRetainage
        {
            get;
            set;
        }
        public decimal? CostsAndEstimatedEarningsInExcessOfBillingsOnUncompletedContracts
        {
            get;
            set;
        }
        public decimal? RefundableIncomeTaxes
        {
            get;
            set;
        }
        public decimal? NoteReceivable
        {
            get;
            set;
        }
        public decimal? OtherCurrentAssets
        {
            get;
            set;
        }
        public decimal? NetPropertyAndEquipment
        {
            get;
            set;
        }
        public decimal? OtherAssets
        {
            get;
            set;
        }
        public decimal? LinesOfCreditShortTermBorrowings
        {
            get;
            set;
        }
        public decimal? InstallmentNotesPayableDueWithinOneYear
        {
            get;
            set;
        }
        public decimal? AccountsPayable
        {
            get;
            set;
        }
        public decimal? RetainagePayable
        {
            get;
            set;
        }
        public decimal? AccruedExpenses
        {
            get;
            set;
        }
        public decimal? BillingsInExcessOfCostsAndEstimatedEarningsOnUncompletedContracts
        {
            get;
            set;
        }
        public decimal? CurrentIncomeTaxesPayable
        {
            get;
            set;
        }
        public decimal? DeferredIncomeTaxesPayableCurrent
        {
            get;
            set;
        }
        public decimal? NotesPayableDueAfterOneYear
        {
            get;
            set;
        }
        public decimal? EquipmentLinesOfCredit
        {
            get;
            set;
        }
        public decimal? OtherLongTermLiabilities
        {
            get;
            set;
        }
        public decimal? DeferredIncomeTaxesPayableLongTerm
        {
            get;
            set;
        }
        public decimal? TotalStockholdersEquity
        {
            get;
            set;
        }
        public decimal? ConstructionRevenues
        {
            get;
            set;
        }
        public decimal? ConstructionDirectCosts
        {
            get;
            set;
        }
        public decimal? NonConstructionGrossProfitLoss
        {
            get;
            set;
        }
        public decimal? GeneralAndAdministrativeExpenses
        {
            get;
            set;
        }
        public decimal? OtherIncomeDeductions
        {
            get;
            set;
        }
        public decimal? IncomeTaxExpenseBenifit
        {
            get;
            set;
        }
        public decimal? DepreciationAndAmortization
        {
            get;
            set;
        }
        public decimal? InterestExpense
        {
            get;
            set;
        }
        public decimal? TotalBacklog
        {
            get;
            set;
        }
        public decimal? TotalLineOfCredit
        {
            get;
            set;
        }
        public decimal? LineOfCreditAvailability
        {
            get;
            set;
        }
        public decimal? TotalThreeLargestCompletedProjects
        {
            get;
            set;
        }
        //public long? PrequalificationId { get; set; }
        public long? ClientId
        {
            get;
            set;
        }
        [NotMapped]
        public decimal Fields
        {
            get;
            set;
        }

        [NotMapped]
        public decimal TotalCurrentAssets
        {
            get
            {

                return (CashAndCashEquivalents ?? 0) + (Investments ?? 0) + (ReceivablesCurrent ?? 0) + (ReceivableRetainage ?? 0) +
                        (CostsAndEstimatedEarningsInExcessOfBillingsOnUncompletedContracts ?? 0) + (RefundableIncomeTaxes ?? 0) +
                       (NoteReceivable ?? 0) + (OtherCurrentAssets ?? 0);
            }
        }
        [NotMapped]
        public decimal TotalAssets
        {
            get
            {
                return TotalCurrentAssets + (NetPropertyAndEquipment ?? 0) + (OtherAssets ?? 0);
            }
        }

        [NotMapped]
        public decimal TotalCurrentLiabilities
        {
            get
            {
                return (LinesOfCreditShortTermBorrowings ?? 0) +
    (InstallmentNotesPayableDueWithinOneYear ?? 0) + (AccountsPayable ?? 0) + (RetainagePayable ?? 0) +
    (AccruedExpenses ?? 0) + (BillingsInExcessOfCostsAndEstimatedEarningsOnUncompletedContracts ?? 0) +
    (CurrentIncomeTaxesPayable ?? 0) + (DeferredIncomeTaxesPayableCurrent ?? 0);
            }
        }


        [NotMapped]
        public decimal TotalLongTermLiabilities
        {
            get
            {
                return (NotesPayableDueAfterOneYear ?? 0) + (EquipmentLinesOfCredit ?? 0) +
    (OtherLongTermLiabilities ?? 0) + (DeferredIncomeTaxesPayableLongTerm ?? 0);
            }
        }
        [NotMapped]
        public decimal TotalLiabilitiesAndStckHldrEqty
        {
            get
            {
                return TotalCurrentLiabilities + TotalLongTermLiabilities +
    (TotalStockholdersEquity ?? 0);
            }
        }

        [NotMapped]
        public decimal ConstructionGrossProfit
        {
            get
            {
                return (ConstructionRevenues ?? 0) - (ConstructionDirectCosts ?? 0);
            }
        }

        [NotMapped]

        public decimal OperatingIncome
        {
            get
            {
                return ConstructionGrossProfit + (NonConstructionGrossProfitLoss ?? 0) -
    (GeneralAndAdministrativeExpenses ?? 0);
            }
        }

        [NotMapped]
        public decimal EarningBeforeTaxes
        {
            get
            {
                return OperatingIncome + (OtherIncomeDeductions ?? 0);
            }
        }

        [NotMapped]
        public decimal NetIncome
        {
            get
            {
                return EarningBeforeTaxes - (IncomeTaxExpenseBenifit ?? 0);
            }
        }

        [NotMapped]
        public decimal EBIDTA
        {
            get
            {
                return EarningBeforeTaxes + (DepreciationAndAmortization ?? 0) + (InterestExpense ?? 0);
            }
        }

        [NotMapped]
        public string BacklogGrossProfit
        {
            get
            {
                if (ConstructionRevenues != 0 && ConstructionRevenues != null)
                {
                    return (ConstructionGrossProfit * (TotalBacklog ?? 0) / ConstructionRevenues).ToString();
                }
                else
                {
                    return "0";
                }
            }
        }

        [NotMapped]
        public decimal WorkingCapital
        {
            get
            {
                return TotalCurrentAssets - TotalCurrentLiabilities;
            }
        }

        [NotMapped]

        public string DaysOverheadCoveredWorkingCapitalLbl
        {
            get
            {
                if (GeneralAndAdministrativeExpenses != null && GeneralAndAdministrativeExpenses != 0)
                {
                    return String.Format("{0:#,000}", WorkingCapital / (GeneralAndAdministrativeExpenses / 365)).ToString();

                }
                else
                {
                    return "N/A";
                }
            }
        }
        [NotMapped]

        public decimal DaysOverheadCoveredWorkingCapital
        {
            get
            {
                if (GeneralAndAdministrativeExpenses != null && GeneralAndAdministrativeExpenses != 0)
                {
                    return (WorkingCapital / ((GeneralAndAdministrativeExpenses ?? 0) / 365));

                }
                else
                {
                    return 0;
                }
            }
        }

        [NotMapped]
        public string WorkingCapitalPercentageRevenueLbl
        {
            get
            {
                if (ConstructionRevenues != 0 && ConstructionRevenues != null)
                {
                    return String.Format("{0:0.00}", (WorkingCapital / ConstructionRevenues) * 100);
                }
                else
                {
                    return "N/A";
                }
            }
        }
        [NotMapped]
        public decimal WorkingCapitalPercentageRevenue
        {
            get
            {
                if (ConstructionRevenues != 0 && ConstructionRevenues != null)
                {
                    return WorkingCapital / (ConstructionRevenues ?? 0);
                }
                else
                {
                    return 0;
                }
            }
        }

        [NotMapped]
        public string WorkingCapitalTurnoverLbl
        {
            get
            {
                if (WorkingCapital != 0)
                {
                    return ((ConstructionRevenues ?? 0) / WorkingCapital).ToString("0.##");
                }
                else
                {
                    return "N/A";
                }
            }
        }
        [NotMapped]
        public decimal WorkingCapitalTurnover
        {
            get
            {
                if (WorkingCapital != 0)
                {
                    return ((ConstructionRevenues ?? 0) / WorkingCapital);
                }
                else
                {
                    return 0;
                }
            }
        }

        [NotMapped]
        public string CurrentRatioLbl
        {
            get
            {
                if (TotalCurrentLiabilities != 0)
                {
                    return (TotalCurrentAssets / TotalCurrentLiabilities).ToString("0.00");
                }
                else
                {
                    return "N/A";
                }
            }
        }
        [NotMapped]
        public decimal CurrentRatio
        {
            get
            {
                if (TotalCurrentLiabilities != 0)
                {
                    return (TotalCurrentAssets / TotalCurrentLiabilities);
                }
                else
                {
                    return 0;
                }
            }
        }
        [NotMapped]
        public decimal cashInvRec
        {
            get
            {
                return (CashAndCashEquivalents ?? 0) + (Investments ?? 0) + (ReceivablesCurrent ?? 0) + (ReceivableRetainage ?? 0);
            }
        }

        [NotMapped]
        public string QuickRatioLbl
        {
            get
            {
                if (TotalCurrentLiabilities != 0)
                {
                    return (cashInvRec / TotalCurrentLiabilities).ToString("0.##");
                }
                else
                {
                    return "N/A";
                }
            }
        }
        [NotMapped]
        public decimal QuickRatio
        {
            get
            {
                if (TotalCurrentLiabilities != 0)
                {
                    return (cashInvRec / TotalCurrentLiabilities);
                }
                else
                {
                    return 0;
                }
            }
        }
        [NotMapped]
        public decimal ConDirCostAndGenraAdm
        {
            get
            {
                return (ConstructionDirectCosts ?? 0) + (GeneralAndAdministrativeExpenses ?? 0);
            }
        }

        [NotMapped]
        public string DefensiveIntervalRatioLbl
        {
            get
            {
                if (ConDirCostAndGenraAdm != 0)
                {
                    return (cashInvRec / (ConDirCostAndGenraAdm / 365)).ToString("0.##");
                }
                else
                {
                    return "N/A";
                }
            }
        }
        [NotMapped]
        public decimal DefensiveIntervalRatio
        {
            get
            {
                if (ConDirCostAndGenraAdm != 0)
                {
                    return (cashInvRec / (ConDirCostAndGenraAdm / 365));
                }
                else
                {
                    return 0;
                }
            }
        }

        [NotMapped]
        public decimal NetOverBillings
        {
            get
            {
                return (BillingsInExcessOfCostsAndEstimatedEarningsOnUncompletedContracts ?? 0) -
                (CostsAndEstimatedEarningsInExcessOfBillingsOnUncompletedContracts ?? 0);
            }
        }


        [NotMapped]
        public string NetOverBillingsPercentRevenueLbl
        {
            get
            {
                if (ConstructionRevenues != 0 && ConstructionRevenues != null)
                {
                    return string.Format("{0:0.00}", (NetOverBillings / ConstructionRevenues) * 100);
                }
                else
                {
                    return "N/A";
                }
            }
        }
        [NotMapped]
        public decimal NetOverBillingsPercentRevenue
        {
            get
            {
                if (ConstructionRevenues != 0 && ConstructionRevenues != null)
                {
                    return (NetOverBillings / ConstructionRevenues) ?? 0;
                }
                else
                {
                    return 0;
                }
            }
        }

        [NotMapped]
        public string DaysRevenueReceivableLbl
        {
            get
            {
                if (ConstructionRevenues != 0 && ConstructionRevenues != null)
                {
                    return string.Format("{0:0.00}", ReceivablesCurrent / (ConstructionRevenues / 360));
                }
                else
                {
                    return "N/A";
                }
            }
        }
        [NotMapped]
        public decimal DaysRevenueReceivable
        {
            get
            {
                if (ConstructionRevenues != 0 && ConstructionRevenues != null)
                {
                    return (ReceivablesCurrent ?? 0) / ((ConstructionRevenues ?? 0) / 360);
                }
                else
                {
                    return 0;
                }
            }
        }

        [NotMapped]
        public string DaysCashLbl
        {
            get
            {
                if (ConstructionRevenues != 0 && ConstructionRevenues != null)
                {
                    return string.Format("{0:0.00}", ((CashAndCashEquivalents ?? 0) + (Investments ?? 0)) * 360 / ConstructionRevenues);
                }
                else
                {
                    return "N/A";
                }
            }
        }
        [NotMapped]
        public decimal DaysCash
        {
            get
            {
                if (ConstructionRevenues != 0 && ConstructionRevenues != null)
                {
                    return ((CashAndCashEquivalents ?? 0) + (Investments ?? 0)) * 360 / ConstructionRevenues ?? 0;
                }
                else
                {
                    return 0;
                }
            }
        }
        [NotMapped]
        public string CashOverBillingsLbl
        {
            get
            {
                if (BillingsInExcessOfCostsAndEstimatedEarningsOnUncompletedContracts != 0 && BillingsInExcessOfCostsAndEstimatedEarningsOnUncompletedContracts != null)
                {
                    return string.Format("{0:0.00}", ((CashAndCashEquivalents ?? 0) + (Investments ?? 0)) / BillingsInExcessOfCostsAndEstimatedEarningsOnUncompletedContracts).ToString();
                }
                else
                {
                    return "N/A";
                }
            }
        }

        [NotMapped]
        public decimal CashOverBillings
        {
            get
            {
                if (BillingsInExcessOfCostsAndEstimatedEarningsOnUncompletedContracts != 0 && BillingsInExcessOfCostsAndEstimatedEarningsOnUncompletedContracts != null)
                {
                    return (((CashAndCashEquivalents ?? 0) + (Investments ?? 0)) / (BillingsInExcessOfCostsAndEstimatedEarningsOnUncompletedContracts ?? 0));
                }
                else
                {
                    return 0;
                }
            }
        }

        [NotMapped]
        public string CashEquityLbl
        {
            get
            {
                if (TotalStockholdersEquity != 0 && TotalStockholdersEquity != null)
                {
                    return string.Format("{0:0.00}", (((CashAndCashEquivalents ?? 0) + (Investments ?? 0)) / TotalStockholdersEquity) * 100);
                }
                else
                {
                    return "N/A";
                }
            }
        }
        [NotMapped]
        public decimal CashEquity
        {
            get
            {
                if (TotalStockholdersEquity != 0 && TotalStockholdersEquity != null)
                {
                    return (((CashAndCashEquivalents ?? 0) + (Investments ?? 0)) / (TotalStockholdersEquity ?? 0));
                }
                else
                {
                    return 0;
                }
            }
        }

        [NotMapped]
        public string CashRevenueLbl
        {
            get
            {
                if (ConstructionRevenues != 0 && ConstructionRevenues != null)
                {
                    return string.Format("{0:0.00}", (((CashAndCashEquivalents ?? 0) + (Investments ?? 0)) / ConstructionRevenues) * 100);
                }
                else
                {
                    return "N/A";
                }
            }
        }
        [NotMapped]
        public decimal CashRevenue
        {
            get
            {
                if (ConstructionRevenues != 0 && ConstructionRevenues != null)
                {
                    return ((CashAndCashEquivalents ?? 0) + (Investments ?? 0)) / (ConstructionRevenues ?? 0);
                }
                else
                {
                    return 0;
                }
            }
        }

        [NotMapped]
        public decimal LinesCreditPlusInstalNotesPaybledue
        {
            get
            {
                return (LinesOfCreditShortTermBorrowings ?? 0) + (InstallmentNotesPayableDueWithinOneYear ?? 0);
            }
        }


        [NotMapped]
        public string DebtCoverageRatioLbl
        {
            get
            {
                if (LinesCreditPlusInstalNotesPaybledue > 0)
                {
                    return ((EBIDTA + (DepreciationAndAmortization ?? 0)) / LinesCreditPlusInstalNotesPaybledue).ToString("0.00");
                }
                else
                {
                    return "N/A";
                }
            }
        }
        [NotMapped]
        public decimal DebtCoverageRatio
        {
            get
            {
                if (LinesCreditPlusInstalNotesPaybledue > 0)
                {
                    return ((EBIDTA + (DepreciationAndAmortization ?? 0)) / LinesCreditPlusInstalNotesPaybledue);
                }
                else
                {
                    return 0;
                }
            }
        }

        [NotMapped]
        public decimal BondingCapacity
        {
            get
            {
                return Math.Min(WorkingCapital, (TotalStockholdersEquity ?? 0)) * 15;
            }
        }

        [NotMapped]
        public string BacklogPercentageBondingCapacityLbl
        {
            get
            {
                if (BondingCapacity != 0)
                {
                    return (((TotalBacklog ?? 0) / BondingCapacity) * 100).ToString("0.##");
                }
                else
                {
                    return "N/A";
                }
            }
        }

        [NotMapped]
        public decimal BacklogPercentageBondingCapacity
        {
            get
            {
                if (BondingCapacity != 0)
                {
                    return (TotalBacklog ?? 0) / BondingCapacity;
                }
                else
                {
                    return 0;
                }
            }
        }

        [NotMapped]
        public string BacklogPercentageRevenueLbl
        {
            get
            {
                if (ConstructionRevenues != 0 && ConstructionRevenues != null)
                {
                    return string.Format("{0:0.00}", ((TotalBacklog ?? 0) / ConstructionRevenues) * 100);
                }
                else
                {
                    return "N/A";
                }
            }
        }
        [NotMapped]
        public decimal BacklogPercentageRevenue
        {
            get
            {
                if (ConstructionRevenues != 0 && ConstructionRevenues != null)
                {
                    return (TotalBacklog ?? 0) / (ConstructionRevenues ?? 0);
                }
                else
                {
                    return 0;
                }
            }
        }

        [NotMapped]
        public string BacklogGrossProfitGAExpenseLbl
        {
            get
            {
                if (GeneralAndAdministrativeExpenses != 0 && GeneralAndAdministrativeExpenses != null)
                {
                    return ((Convert.ToDecimal(BacklogGrossProfit)) / (GeneralAndAdministrativeExpenses ?? 0) * 100).ToString("0.##");
                }
                else
                {
                    return "N/A";
                }
            }
        }
        [NotMapped]
        public decimal BacklogGrossProfitGAExpense
        {
            get
            {
                if (GeneralAndAdministrativeExpenses != 0 && GeneralAndAdministrativeExpenses != null)
                {
                    return Convert.ToDecimal(BacklogGrossProfit) / (GeneralAndAdministrativeExpenses ?? 0) * 100;
                }
                else
                {
                    return 0;
                }
            }
        }

        [NotMapped]
        public string MonthsRevenueBacklogLbl
        {
            get
            {
                if (ConstructionRevenues != 0 && ConstructionRevenues != null)
                {
                    return string.Format("{0:0.00}", (TotalBacklog ?? 0) / (ConstructionRevenues / 12));
                }
                else
                {
                    return "N/A";
                }
            }
        }
        [NotMapped]
        public decimal MonthsRevenueBacklog
        {
            get
            {
                if (ConstructionRevenues != 0 && ConstructionRevenues != null)
                {
                    return (TotalBacklog ?? 0) / (ConstructionRevenues ?? 0);
                }
                else
                {
                    return 0;
                }
            }
        }

        [NotMapped]
        public string EquityRevenueLbl
        {
            get
            {
                if (ConstructionRevenues != 0 && ConstructionRevenues != null)
                {
                    return string.Format("{0:0.00}", ((TotalStockholdersEquity ?? 0) / ConstructionRevenues ?? 0) * 100);
                }
                else
                {
                    return "N/A";
                }
            }
        }
        [NotMapped]
        public decimal EquityRevenue
        {
            get
            {
                if (ConstructionRevenues != 0 && ConstructionRevenues != null)
                {
                    return (TotalStockholdersEquity ?? 0) / (ConstructionRevenues ?? 0);
                }
                else
                {
                    return 0;
                }
            }
        }

        [NotMapped]
        public string InterestBearingDebtEquityLbl
        {
            get
            {
                if (TotalStockholdersEquity != 0 && TotalStockholdersEquity != null)
                {
                    return string.Format("{0:0.00}", ((NotesPayableDueAfterOneYear ?? 0) + (EquipmentLinesOfCredit ?? 0) + LinesCreditPlusInstalNotesPaybledue) / TotalStockholdersEquity);
                }
                else
                {
                    return "N/A";
                }
            }
        }
        [NotMapped]
        public decimal InterestBearingDebtEquity
        {
            get
            {
                if (TotalStockholdersEquity != 0 && TotalStockholdersEquity != null)
                {
                    return ((NotesPayableDueAfterOneYear ?? 0) + (EquipmentLinesOfCredit ?? 0) + LinesCreditPlusInstalNotesPaybledue) / (TotalStockholdersEquity ?? 0);
                }
                else
                {
                    return 0;
                }
            }
        }
        [NotMapped]
        public string DebtWorkingCapitalLbl
        {
            get
            {
                if (WorkingCapital != 0)
                {
                    return string.Format("{0:0.00}", ((NotesPayableDueAfterOneYear ?? 0) + (EquipmentLinesOfCredit ?? 0) + LinesCreditPlusInstalNotesPaybledue) / WorkingCapital);
                }
                else
                {
                    return "N/A";
                }
            }
        }
        [NotMapped]
        public decimal DebtWorkingCapital
        {
            get
            {
                if (WorkingCapital != 0)
                {
                    return ((NotesPayableDueAfterOneYear ?? 0) + (EquipmentLinesOfCredit ?? 0) + LinesCreditPlusInstalNotesPaybledue) / WorkingCapital;
                }
                else
                {
                    return 0;
                }
            }
        }

        [NotMapped]
        public string TotalLiabilitiesToEquityLbl
        {
            get
            {
                if (TotalStockholdersEquity != 0 && TotalStockholdersEquity != null)
                {
                    return string.Format("{0:0.00}", ((TotalCurrentLiabilities + TotalLongTermLiabilities) / (TotalStockholdersEquity ?? 0)) * 100);
                }
                else
                {
                    return "N/A";
                }
            }
        }
        [NotMapped]
        public decimal TotalLiabilitiesToEquity
        {
            get
            {
                if (TotalStockholdersEquity != 0 && TotalStockholdersEquity != null)
                {
                    return (TotalCurrentLiabilities + TotalLongTermLiabilities) / (TotalStockholdersEquity ?? 0);
                }
                else
                {
                    return 0;
                }
            }
        }

        [NotMapped]
        public string GrossProfitPercentageLbl
        {
            get
            {
                if (ConstructionRevenues != 0 && ConstructionRevenues != null)
                {
                    return string.Format("{0:0.00}", ((ConstructionGrossProfit + (NonConstructionGrossProfitLoss ?? 0)) / (ConstructionRevenues ?? 0)) * 100);
                }
                else
                {
                    return "N/A";
                }
            }
        }
        [NotMapped]
        public decimal GrossProfitPercentage
        {
            get
            {
                if (ConstructionRevenues != 0 && ConstructionRevenues != null)
                {
                    return (ConstructionGrossProfit + (NonConstructionGrossProfitLoss ?? 0)) / (ConstructionRevenues ?? 0);
                }
                else
                {
                    return 0;
                }
            }
        }

        [NotMapped]
        public string GAPercentageLbl
        {
            get
            {
                if (ConstructionRevenues != 0 && ConstructionRevenues != null)
                {
                    return string.Format("{0:0.00}", ((GeneralAndAdministrativeExpenses ?? 0) / (ConstructionRevenues ?? 0)) * 100);
                }
                else
                {
                    return "N/A";
                }
            }
        }
        [NotMapped]
        public decimal GAPercentage
        {
            get
            {
                if (ConstructionRevenues != 0 && ConstructionRevenues != null)
                {
                    return (GeneralAndAdministrativeExpenses ?? 0) / (ConstructionRevenues ?? 0);
                }
                else
                {
                    return 0;
                }
            }
        }

        [NotMapped]
        public string PreTaxEarningsPercentageLbl
        {
            get
            {
                if (ConstructionRevenues != 0 && ConstructionRevenues != null)
                {
                    return string.Format("{0:0.00}", (EarningBeforeTaxes / (ConstructionRevenues ?? 0)) * 100);
                }
                else
                {
                    return "N/A";
                }
            }
        }
        [NotMapped]
        public decimal PreTaxEarningsPercentage
        {
            get
            {
                if (ConstructionRevenues != 0 && ConstructionRevenues != null)
                {
                    return EarningBeforeTaxes / (ConstructionRevenues ?? 0);
                }
                else
                {
                    return 0;
                }
            }
        }

        [NotMapped]
        public string PreTaxReturnCurrentEquityLbl
        {
            get
            {
                if (TotalStockholdersEquity != 0 && TotalStockholdersEquity != null)
                {
                    return string.Format("{0:0.00}", ((EarningBeforeTaxes) / TotalStockholdersEquity) * 100);
                }
                else
                {
                    return "N/A";
                }
            }
        }
        [NotMapped]
        public decimal PreTaxReturnCurrentEquity
        {
            get
            {
                if (TotalStockholdersEquity != 0 && TotalStockholdersEquity != null)
                {
                    return (EarningBeforeTaxes) / (TotalStockholdersEquity ?? 0);
                }
                else
                {
                    return 0;
                }
            }
        }

        [NotMapped]
        public string PreTaxReturnCurrentAssetsLbl
        {
            get
            {
                if (TotalAssets != 0)
                {
                    return string.Format("{0:0.00}", ((EarningBeforeTaxes) / TotalAssets) * 100);
                }
                else
                {
                    return "N/A";
                }
            }
        }
        [NotMapped]
        public decimal PreTaxReturnCurrentAssets
        {
            get
            {
                if (TotalAssets != 0)
                {
                    return (EarningBeforeTaxes) / TotalAssets;
                }
                else
                {
                    return 0;
                }
            }
        }


        [NotMapped]
        public string AfterTaxReturnCurrentEquityLbl
        {
            get
            {
                if (TotalStockholdersEquity != 0 && TotalStockholdersEquity != null)
                {
                    return string.Format("{0:0.00}", (NetIncome / TotalStockholdersEquity) * 100);
                }
                else
                {
                    return "N/A";
                }
            }
        }
        [NotMapped]
        public decimal AfterTaxReturnCurrentEquity
        {
            get
            {
                if (TotalStockholdersEquity != 0 && TotalStockholdersEquity != null)
                {
                    return NetIncome / TotalStockholdersEquity ?? 0;
                }
                else
                {
                    return 0;
                }
            }
        }

        [NotMapped]
        public string AfterTaxReturnCurrentAssetsLbl
        {
            get
            {
                if (TotalAssets != 0)
                {
                    return string.Format("{0:0.00}", NetIncome / TotalAssets);
                }
                else
                {
                    return "N/A";
                }
            }
        }
        [NotMapped]
        public decimal AfterTaxReturnCurrentAssets
        {
            get
            {
                if (TotalAssets != 0)
                {
                    return NetIncome / TotalAssets;
                }
                else
                {
                    return 0;
                }
            }
        }

        [NotMapped]
        public decimal CEBITDA
        {
            get
            {
                return EarningBeforeTaxes + (DepreciationAndAmortization ?? 0) + (InterestExpense ?? 0);
            }
        }

        [NotMapped]
        public string AsPercentageRevenueLbl
        {
            get
            {
                if (ConstructionRevenues != 0 && ConstructionRevenues != null)
                {
                    return string.Format("{0:0.00}", (CEBITDA / ConstructionRevenues) * 100);
                }
                else
                {
                    return "N/A";
                }
            }
        }
        public decimal AsPercentageRevenue
        {
            get
            {
                if (ConstructionRevenues != 0 && ConstructionRevenues != null)
                {
                    return CEBITDA / (ConstructionRevenues ?? 0);
                }
                else
                {
                    return 0;
                }
            }
        }

        [NotMapped]
        public decimal NetWorthAvailableLoc
        {
            get
            {
                return (TotalStockholdersEquity ?? 0) + (LineOfCreditAvailability ?? 0);
            }
        }

        [NotMapped]
        public decimal FiveTimesWorkingCapital
        {
            get
            {
                return WorkingCapital * 5;
            }
        }

        [NotMapped]
        public decimal TwentyFivePrcntPriorYrRevenue
        {
            get
            {
                return (ConstructionRevenues ?? 0) * 25 / 100;
            }
        }

        [NotMapped]
        public decimal SeventyPercentOfThreeLargestCompletedContracts
        {
            get
            {
                return (TotalThreeLargestCompletedProjects ?? 0) * 7 / 10;
            }
        }

        [NotMapped]
        public decimal AverageMeasuresForSingleJob
        {
            get
            {
                return (NetWorthAvailableLoc + FiveTimesWorkingCapital +
    TwentyFivePrcntPriorYrRevenue + SeventyPercentOfThreeLargestCompletedContracts) / 4;
            }
        }

        [NotMapped]
        public decimal NetWorthTotalLineCredit
        {
            get
            {
                return (TotalStockholdersEquity ?? 0) + (TotalLineOfCredit ?? 0);
            }
        }

        [NotMapped]
        public decimal TenTimesWorkingCapital
        {
            get
            {
                return Convert.ToDecimal(string.Format("{0:0.00}", WorkingCapital * 10));
            }
        }

        [NotMapped]
        public decimal FiftyPrcntPriorYrRevenue
        {
            get
            {
                return Convert.ToDecimal(string.Format("{0:0.00}", (ConstructionRevenues ?? 0) * 50 / 100));
            }
        }

        [NotMapped]
        public decimal TenTimesNetWorth
        {
            get
            {
                return (TotalStockholdersEquity ?? 0) * 10;
            }
        }

        [NotMapped]
        public decimal AverageMeasuresForAggrigate
        {
            get
            {
                return (NetWorthTotalLineCredit + TenTimesWorkingCapital +
    FiftyPrcntPriorYrRevenue + TenTimesNetWorth) / 4;
            }
        }

        [NotMapped]
        public decimal CalculationForReturnonTotalAssets
        {
            get
            {
                if (TotalAssets != 0)
                {

                    return Convert.ToDecimal(string.Format("{0:0.0000}", (OperatingIncome) / TotalAssets));
                }
                else
                {
                    return 0;
                }
            }
        }

        [NotMapped]
        public decimal CalculationForSalestoTotalAssets
        {
            get
            {
                if (TotalAssets != 0)
                {
                    return Convert.ToDecimal(string.Format("{0:0.0000}", (ConstructionRevenues ?? 0) / TotalAssets));
                }
                else
                {
                    return 0;
                }
            }
        }
        [NotMapped]
        public decimal TotalLiabilities
        {
            get
            {
                return TotalCurrentLiabilities + TotalLongTermLiabilities;
            }
        }

        [NotMapped]
        public decimal CalculationForEquityToDebt
        {
            get
            {
                if (TotalLiabilities != 0)
                {
                    return Convert.ToDecimal(string.Format("{0:0.0000}", (TotalStockholdersEquity ?? 0) / TotalLiabilities));
                }
                else
                {
                    return 0;
                }
            }
        }

        [NotMapped]
        public decimal CalculationForWorkingCapitalToTotalAssets
        {
            get
            {
                if (TotalAssets != 0)
                {
                    return Convert.ToDecimal(string.Format("{0:0.0000}", WorkingCapital / TotalAssets));
                }
                else
                {
                    return 0;
                }
            }
        }

        [NotMapped]
        public decimal CalculationForRetainedEarningstoTotalAssets
        {
            get
            {
                if (TotalAssets != 0)
                {
                    return Convert.ToDecimal(string.Format("{0:0.0000}", (TotalStockholdersEquity ?? 0) / TotalAssets));
                }
                else
                {
                    return 0;
                }
            }
        }

        [NotMapped]
        public decimal ReturnonTotalAssets
        {
            get
            {
                return Convert.ToDecimal(string.Format("{0:0.0000}", CalculationForReturnonTotalAssets * (decimal)3.3000));
            }
        }

        [NotMapped]
        public decimal SalestoTotalAssets
        {
            get
            {
                return Convert.ToDecimal(string.Format("{0:0.0000}", CalculationForSalestoTotalAssets * (decimal)0.9990));
            }
        }

        [NotMapped]
        public decimal EquityToDebt
        {
            get
            {
                return Convert.ToDecimal(string.Format("{0:0.0000}", CalculationForEquityToDebt * (decimal)0.6000));
            }
        }

        [NotMapped]
        public decimal WorkingCapitalToTotalAssets
        {
            get
            {
                return Convert.ToDecimal(string.Format("{0:0.0000}", CalculationForWorkingCapitalToTotalAssets * (decimal)1.2000));
            }
        }

        [NotMapped]
        public decimal RetainedEarningstoTotalAssets
        {
            get
            {
                return Convert.ToDecimal(string.Format("{0:0.0000}", CalculationForRetainedEarningstoTotalAssets * (decimal)1.4000));
            }
        }

        [NotMapped]
        public decimal ZScore
        {
            get
            {
                return (ReturnonTotalAssets + SalestoTotalAssets +
    EquityToDebt + WorkingCapitalToTotalAssets + RetainedEarningstoTotalAssets);
            }
        }

        [NotMapped]
        public string NetProfitMarginLbl
        {
            get
            {
                if (ConstructionRevenues != 0 && ConstructionRevenues != null)
                {
                    return string.Format("{0:0.00}", (NetIncome / ConstructionRevenues) * 100).ToString();
                }
                else
                {
                    return "N/A";
                }
            }
        }
        [NotMapped]
        public decimal NetProfitMargin
        {
            get
            {
                if (ConstructionRevenues != 0 && ConstructionRevenues != null)
                {
                    return NetIncome / (ConstructionRevenues ?? 0);
                }
                else
                {
                    return 0;
                }
            }
        }
        [NotMapped]
        public bool IsDisabled { get; set; }
    }
}



