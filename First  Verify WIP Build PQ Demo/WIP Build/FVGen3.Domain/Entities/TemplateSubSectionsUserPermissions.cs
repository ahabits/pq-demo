﻿/*
Page Created Date:  01/20/2014
Created By: Rajesh
Purpose:
Version: 1.0
****************************************************
*/

using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace FVGen3.Domain.Entities
{
    public class TemplateSubSectionsUserPermissions
    {
        
        //>>DV:SB,KT
        [Key]
        public long SubSectionUserPermissionId { get; set; }

        [ForeignKey("TemplateSubSectionsPermissions")]
        public long SubSectionPermissionId { get; set; }
        public virtual TemplateSubSectionsPermissions TemplateSubSectionsPermissions { get; set; }
        [ForeignKey("SystemUsers")]
        public Guid UserId { get; set; }
        public virtual SystemUsers SystemUsers { get; set; }

        public bool ReadOnlyAccess { get; set; }
        public bool EditAccess { get; set; }
    }
}