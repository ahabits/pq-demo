﻿/*
Page Created Date:  10/02/2013
Created By: Kiran Talluri
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FVGen3.Domain.Entities
{
    public class ClientTemplateReportingData
    {
        
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long ClientTempReportingId { get; set; }

        [ForeignKey("ClientTemplates")]
        public Guid ClientTemplateId { get; set; }
        public virtual ClientTemplates ClientTemplates { get; set; }

        public int ReportingType { get; set; }
        public long ReportingTypeId { get; set; }
        public bool? DocumentExpires { get; set; }
        public bool? DocIsMandatory { get; set; } // Kiran on 8/5//2014
        public bool? ReportingStatus { get; set; } // Kiran on 10/7/2014

        public long? SectionId { get; set; }

        //[ForeignKey("ReportingTypeId")]
        //public virtual BiddingInterests BiddingInterests { get; set; }

        //[ForeignKey("ReportingTypeId")]
        //public virtual DocumentType DocumentType { get; set; }
    }
}
