﻿/*
Page Created Date:  06/11/2013
Created By: Siva Bommisetty
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace FVGen3.Domain.Entities
{
    public class LocalAddClientBusinessUnitSite
    {
        public long ClientId { get; set; }
        public long ClientBusinessUnitId { get; set; }
        public long ClientBusinessUnitSiteId { get; set; }
        public long OldBusSiteId { get; set; }
        public string ClientSelectedBusinessUnit { get; set; } // Added on 8/22/2013

        [Required(ErrorMessage = "*")]
        public string BusinessUnitSiteName { get; set; }
        public bool isSiteEdit { get; set; }

        // Kiran on 10/29/2014
        [Required(ErrorMessage = "*")]
        public string BusinessUnitLocation { get; set; }

        //[Required(ErrorMessage = "*")]
        //public string BusinessUnitRegion { get; set; }
        //[Required(ErrorMessage = "*")]
        //public string BusinessUnitRegionTxt { get; set; }
        //public bool BusinessUnitType { get; set; }
        // Ends<<<

        public List<SelectListItem> SafetyRepresentataives { get; set; }

        [Required(ErrorMessage = "*")]
        public List<string> BusinessUnitSiteSafetyRepresentative { get; set; }
    }
}
