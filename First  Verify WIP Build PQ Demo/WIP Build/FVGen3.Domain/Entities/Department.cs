﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.Entities
{
 public  class Department
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }   
       
        public string DepartmentName { get; set; }
        [ForeignKey("ClientID")]
        public virtual Organizations Organizations { get; set; }
        public long ClientID { get; set; }
        public virtual List<OnsiteOffsite> OnsiteOffsite { get; set; }
    }
}
