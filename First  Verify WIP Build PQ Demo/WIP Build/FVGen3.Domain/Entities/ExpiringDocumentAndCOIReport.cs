﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.Entities
{
    public class ExpiringDocumentAndCOIReport
    {
        public string VendorName { get; set; }
        public string ClientName { get; set; }
        public string ExpirationDate { get; set; }
        public string AutoEmailLastSent { get; set; }
        public string Status { get; set; }
        public List<DocumentType> DocumentType { get; set; }


        public List<Boolean> DoctypeList { get; set; }
        public long[] doctypeList { get; set; }



        public List<DocumentType> DocumentTypeList { get; set; }



    }
}
