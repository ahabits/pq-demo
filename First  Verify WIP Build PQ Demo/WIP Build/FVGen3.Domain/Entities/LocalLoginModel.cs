﻿/****************************************************
Page Created Date:  05/03/2013
Created By: Sumalata
Purpose:LoginPage
Version: 1.0
****************************************************/


using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace FVGen3.Domain.Entities
{
     public class LocalLoginModel
    {
        //[Required(ErrorMessage = "Please Enter Email Address")]
        [DisplayName("User Name")]
        // Kiran on 01/20/2015
        //[RegularExpression(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$", ErrorMessage = "Invalid Email ID")]
        //[RegularExpression(LocalConstants.EMAIL_FORMAT, ErrorMessage = "Invalid Email ID")]
        // Ends<<<
        public string UserName { get; set; }

        //[Required(ErrorMessage = "Please Enter Password")]
        [DataType(DataType.Password)]
        [DisplayName("Password")]
        public string Password { get; set; }

        [DisplayName("Remember Me")]
        public bool RememberMe { get; set; }

        public DateTime LoginTime { get; set; }
        public DateTime LogoutTime { get; set; }
        public String SystemAction {get; set;}
        public String lastLoginURl { get; set; }
    }
}
