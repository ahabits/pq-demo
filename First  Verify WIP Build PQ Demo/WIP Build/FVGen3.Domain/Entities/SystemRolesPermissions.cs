﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace FVGen3.Domain.Entities
{
    public class SystemRolesPermissions
    {
        [Key]
        public long SysRolePermissionId { get; set; }
        
        public string PermissionName { get; set; }
        public bool? ReadPermission { get; set; }
        public bool? InsertPermission { get; set; }
        public bool? ModifyPermission { get; set; }
        public bool? DeletePermission { get; set; }

        [ForeignKey("SystemRoles")]
        public string SysRoleId { get; set; }
        public virtual SystemRoles SystemRoles { get; set; }

        [ForeignKey("SystemViews")]
        public long? SysViewsId { get; set; }
        public virtual SystemViews SystemViews { get; set; }

    }
}
