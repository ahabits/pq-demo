﻿/*
Page Created Date:  07/31/2013
Created By: Kiran Talluri
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace FVGen3.Domain.Entities
{
    public class LocalVendorEmployeeRegistrationXXX
    {
        [Required(ErrorMessage = "Enter Email")]
        [RegularExpression(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$", ErrorMessageResourceName = "Common_Email_Invalid_ErrorMessage", ErrorMessageResourceType = typeof(Resources.Resources))]
        public string Email { get; set; }

        [Required(ErrorMessage = "Enter Password")]
        [RegularExpression(@"(?=^.{10,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$", ErrorMessageResourceName = "AddNewUserModel_InvalidPassword", ErrorMessageResourceType = typeof(Resources.Resources))]
        public string Password { get; set; }

        [Required(ErrorMessage = "Re-Enter Password")]
        [System.Web.Mvc.Compare("Password", ErrorMessageResourceName = "LocalNewUserContactDetails_ConfirmPassword", ErrorMessageResourceType = typeof(Resources.Resources))]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessage = "*")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "*")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "*")]
        public string ContactTitle { get; set; }
    }
}
