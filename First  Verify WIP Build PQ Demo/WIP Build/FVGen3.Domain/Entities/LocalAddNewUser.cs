﻿/*
Page Created Date:  05/18/2013
Created By: Kiran Talluri
Purpose:
Version: 1.0
****************************************************
*/

using System.ComponentModel.DataAnnotations;
using Resources;

namespace FVGen3.Domain.Entities
{
    public class LocalAddNewUser
    {
        [Required(ErrorMessageResourceName = "Common_ErrorMessage_UserName", ErrorMessageResourceType = typeof(Resources.Resources))]
        [StringLength(256, ErrorMessageResourceName = "ErrorMessage_Exceed256", ErrorMessageResourceType = typeof(Resources.Resources))]
        // Kiran on 01/20/2015
        [RegularExpression(LocalConstants.EMAIL_FORMAT, ErrorMessageResourceName = "Common_Email_Invalid_ErrorMessage", ErrorMessageResourceType = typeof(Resources.Resources))]
        //[RegularExpression(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$", ErrorMessageResourceName = "AddNewUserModel_InvalidEmail", ErrorMessageResourceType = typeof(Resources.Resources))]
        public string Email { get; set; }
        // Ends<<<

        [Required(ErrorMessageResourceName = "Common_ErrorMessage_Password", ErrorMessageResourceType = typeof(Resources.Resources))]
        [StringLength(128, ErrorMessageResourceName = "ErrorMessage_Exceed128", ErrorMessageResourceType = typeof(Resources.Resources))]
        [RegularExpression(@"(?=^.{10,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$", ErrorMessageResourceName = "AddNewUserModel_InvalidPassword", ErrorMessageResourceType = typeof(Resources.Resources))]
        public string Password { get; set; }

        [Required(ErrorMessageResourceName = "ErrorMessage_MatchPassword", ErrorMessageResourceType = typeof(Resources.Resources))]
        [StringLength(128, ErrorMessageResourceName = "ErrorMessage_Exceed128", ErrorMessageResourceType = typeof(Resources.Resources))]
        [System.Web.Mvc.Compare("Password", ErrorMessageResourceName = "AddNewUserModel_ConfirmPassword", ErrorMessageResourceType = typeof(Resources.Resources))]
        public string ConfirmPassword { get; set; }

        public int UserType { get; set; }

        [Required(ErrorMessageResourceName = "ErrorMessage_Organization", ErrorMessageResourceType = typeof(Resources.Resources))]
        public long OrganizationId { get; set; }
    }
}
