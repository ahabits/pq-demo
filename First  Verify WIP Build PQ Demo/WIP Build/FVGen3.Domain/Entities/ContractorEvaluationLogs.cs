﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.Entities
{
    public class ContractorEvaluationLogs
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        [ForeignKey("VendorReviewInput")]
        public long ReviewId { get; set; }
        public virtual VendorReviewInput VendorReviewInput { get; set; }
        public string EmailSentTo { get; set; }
        public DateTime EmailSentDateTime { get; set; }
        public int NotificationNo { get; set; } 
    }
}
