﻿/*
Page Created Date:  05/18/2013
Created By: Suma
Purpose:
Version: 1.0
****************************************************
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace FVGen3.Domain.Entities
{
    public class LocalPrequalification
    {
       
        public long PrequalificationId { get; set; }

        
        public DateTime PrequalificationStart { get; set; }
        public DateTime PrequalificationFinish { get; set; }
        public DateTime PrequalificationCreate {get; set;}
        public DateTime? PrequalificationSubmit { get; set; }
        public string Initials { get; set; }
        public string VendorName { get; set; }
        public long VendorID { get; set; } // sumanth on 27/08/2015 for FV-162
        public string ClientName { get; set; }// Kiran on 6/24/2014
        public string IndicationTextClient { get; set; }
        public bool HasPrevious { get; set; }
        public bool HasNext { get; set; }
        public int PageNumber { get; set; }
        public bool isClientApproval { get; set; } // Kiran on 4/15/2014
        public bool isReSubmitted { get; set; }
        public long ClientId { get; set; }
    }
}
