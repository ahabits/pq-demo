﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace FVGen3.Domain.Entities
{
   public class Regions
    {[Key]
        public int RegionId { get; set; }
        public string RegionName { get; set; }
        //public virtual List<ClientRegions> ClientRegions { get; set; }
        public virtual List<RegionStates> RegionStates { get; set; }
    }
}
