﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace FVGen3.Domain.Entities
{
    public class LocalAddDocumentType
    {
        [Required(ErrorMessage="*")]
        public string DocumentTypeName { get; set; }
        public bool DocumentExpires { get; set; }
        public bool isEdit { get; set; }
        public bool RestrictAccess { get; set; }
        public long DocumentTypeId { get; set; }
    }
}
