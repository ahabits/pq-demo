﻿
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace FVGen3.Domain.Entities
{
   public class VendorReviewInputSection3Ans
    {
        [Key]
        public long Id { get; set; }

        [ForeignKey("VendorReviewInput")]
        public long VendorReviewInputId { get; set; }
        public virtual VendorReviewInput VendorReviewInput { get; set; }

        [ForeignKey("VendorReviewQuestionsForSection3")]
        public long Section3QuestionId { get; set; }
        public virtual VendorReviewQuestionsForSection3 VendorReviewQuestionsForSection3 { get; set; }

        public string Answer { get; set; }
        public Guid? FilledBy { get; set; }

    }
}
