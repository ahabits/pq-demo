﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.Entities
{
    public class LocalUsersList
    {
        public Guid UserId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string OrgName { get; set; }
        public string OrganizationType { get; set; }
        public bool? UserStatus { get; set; }
        public bool IsSubClientUser { get; set; }
        //public List<Contact> contacts { get; set; }
        public string UserRoles { get; set; }
        public long OrgID { get; set; }

    }
}
