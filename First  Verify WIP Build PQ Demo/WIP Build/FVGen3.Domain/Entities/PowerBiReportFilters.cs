﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.Entities
{
  public class PowerBiReportFilters
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key] 
        public long Id { get; set;}
        [ForeignKey("ReportId")]
        public virtual PowerBiReports PowerBiReports { get; set; }
        public long ReportId { get; set; }
        public string DatasetName { get; set; }
        public string FilterName { get; set; } 
        public string Condition { get; set; } 
        public string FilterTo { get; set; }
    }
}
