﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FVGen3.Domain.Entities
{
    public class SystemViews
    {
        [Key]
        public long SystemViewsId { get; set; }

        public string SystemViewName { get; set; }
        public string SystemController { get; set; }
        public string SystemViewCSHTML { get; set; }
        public string OrganizationType { get; set; }

        [ForeignKey("SystemSideMenus")]
        public long? SystemSideMenuId { get; set; }
        public virtual SystemSideMenus SystemSideMenus { get; set; }
        
        public virtual List<SystemRolesPermissions> SystemRolesPermissions { get; set; }
        
    }
}
