﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FVGen3.Domain.Entities
{
    public class SubClients
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [ForeignKey("SuperClientId")]
        public virtual Organizations SuperClientOrganization { get; set; } 
        public long SuperClientId { get; set; }

        [ForeignKey("ClientId")]
        public virtual Organizations SubClientOrganization { get; set; }
        public long  ClientId { get; set; }

        public bool? IsActive { get; set; }
    }
}
