﻿/*
Page Created Date:  03/24/2015
Created By: Mani
Purpose:Cargil Requirement
Version: 1.0
****************************************************
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace FVGen3.Domain.Entities
{
    public class VendorReviewQuestions
    {
        [Key]
        public long VendorReviewQuestionId{get;set;}
        public long ClientID { get; set; }
        public string QuestionString { get; set; }
        public bool QisMandatory { get; set; }
    }
}
