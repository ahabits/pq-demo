﻿/*
Page Created Date:  10/09/2014
Created By: Kiran Talluri
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace FVGen3.Domain.Entities
{
    public class LocalAddTrainingVideo
    {
        public long SectionId { get; set; }
        public long ClientId { get; set; }
        public long SubSectionId { get; set; }
        [Required(ErrorMessage = "*")]
        public string Video1 { get; set; }
        public string Video2 { get; set; }
        public string SubSectionName { get; set; }
        public string SubSectionTypeCondition { get; set; }

        public bool mode { get; set; }
        public string FileNameOld { get; set; }
        public Guid? DocumentId { get; set; }

        public List<Document> VideoOrAlternateDocuments { get; set; }
    }
}
