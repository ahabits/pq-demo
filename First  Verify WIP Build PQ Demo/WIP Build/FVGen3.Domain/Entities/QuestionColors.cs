﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.Entities
{
   public  class QuestionColors
    { [Key]
        public int Id { get; set; }
        public string Color { get; set; }
        public string Answer { get; set; }

        [ForeignKey("Questions")]
        public long QuestionID { get; set; }
        public virtual Questions Questions { get; set; }

    }
}
