﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FVGen3.Domain.Entities
{
    public class EmployeeQuizUserInput
    {
        [Key]
        public long EmpQuizRecId { get; set; }
        
        public string UserInput { get; set; }


        // Kiran on 12/18/2014

        //[ForeignKey("EmployeeQuizzes")]
        //public long EmpQuizzesId { get; set; }
        //public virtual EmployeeQuizzes EmployeeQuizzes { get; set; }

        [ForeignKey("QuestionColumnDetails")]
        public long QuestionColumnId { get; set; }
        public virtual QuestionColumnDetails QuestionColumnDetails { get; set; }

        [ForeignKey("SystemUsersOrganizationsQuizzes")]
        public long SysUserOrgQuizId { get; set; }
        public virtual SystemUsersOrganizationsQuizzes SystemUsersOrganizationsQuizzes { get; set; }
        // Ends<<<
    }
}
