﻿/*
Page Created Date:  11/06/2013
Created By: Siva Bommisetty
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FVGen3.Domain.Entities
{
    public class ClientBusinessUnitSiteRepresentatives
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long ClientBUSiteRepID { get; set; }

        [ForeignKey("SafetyRepresentative")]
        public virtual SystemUsers SystemUsers { get; set; }
        public Guid SafetyRepresentative { get; set; }
        

        [ForeignKey("ClientBUSiteID")]
        public virtual ClientBusinessUnitSites ClientBusinessUnitSites { get; set; }
        public long ClientBUSiteID { get; set; }

        public System.Nullable<bool> ReceiveNotifications { get; set; }

    }
}
