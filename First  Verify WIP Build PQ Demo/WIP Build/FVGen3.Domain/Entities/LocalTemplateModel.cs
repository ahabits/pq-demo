﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.Web.WebPages.Html;

namespace FVGen3.Domain.Entities
{
    public class LocalTemplateModel
    {
        public LocalTemplateModel()
        {
            isSectionSelected = true;
        }
        [Required(ErrorMessage = "*")]
        public string TemplateName { get; set; }
        public Guid? Templateid { get; set; }
        [Required(ErrorMessage = "*")]
        public long ClientId { get; set; }
        public string CopyTemplateId { get; set; }
        public bool isCopyTemplateSelected { get; set; }
        public bool isSectionSelected { get; set; }
        public bool isSubSectionSelected { get; set; }
        public bool isQuestionsSelected { get; set; }
      
        public int? TemplateSequence { get; set; }// Mani on 7/31/2015 for FV-169
        public string RiskLevel { get; set; }
        public bool LockedByDefault { get; set; }        
        public bool RenewalLockedByDefault { get; set; }
        public List<string> unlockNotificationAdminEmails { get; set; }        
        public int EmaiTemplateId { get; set; }
        public List< System.Web.Mvc.SelectListItem> ListOfAdminUsersForNotificationsTosent { get; set; }
        //public string ClientOfTemplate { get; set; }
        public string TemplateType { get; set; }
        [Required(ErrorMessage = "*")]
        public string TemplateNote { get; set; }

        [StringLength(5, ErrorMessageResourceName = "ErrorMessage_TemplateCode", ErrorMessageResourceType = typeof(Resources.Resources))]
        public string TemplateCode { get; set; }
        [AllowHtml]
        public string LockComments { get; set; }

        public bool IsERI { get; set; }
        public bool ExcludePQNotifications { get; set; }
        public long? LanguageId { get; set; }
        [Required(ErrorMessage = "*")]
        public List<long> ERIClients { get; set; }
        public bool HideClients { get; set; }
        public bool HasLocationNotification { get; set; }
        public bool HasVendorDetailLocation { get; set;}
        public bool HideCommentsSec { get; set; }
        public int TemplateStatus { get; set; }
        public string ClientName { get; set; }
        public List<System.Web.Mvc.SelectListItem> SubClients { get; set; }
    }
}
