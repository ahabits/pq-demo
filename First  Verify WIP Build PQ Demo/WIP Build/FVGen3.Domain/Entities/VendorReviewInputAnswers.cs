﻿/*
Page Created Date:  03/24/2015
Created By: Mani
Purpose:Cargil Requirement
Version: 1.0
****************************************************
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FVGen3.Domain.Entities
{
   public class VendorReviewInputAnswers
    {
       [Key]
       public long VendorRevInputAnsId { get; set; }

      
       public long? VendorReviewInputId { get; set; }

       public long? VendorReviewQuestionId { get; set; }
       public string InputValue { get; set; }
      
    }
}
