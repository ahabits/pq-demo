﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.Entities
{
   public class LocalModelForBusites
    {
        public LocalModelForBusites()
        {
            ClientTemplateId = new List<Guid>();
        }
        public List<LocalBUDetails> BUDetails { get; set; }
        public List<String> Price { get; set; }
        public List<String> Prices { get; set; }
        public List<Guid> ClientTemplateId { get; set; }
        public Guid TemplateId { get; set; }
        public List<long?> PrequalificationTrainingChek { get; set; }// Mani on 8/18/2015
    }

   public class LocalBUDetails
   {
       public long BUId { get; set; }
        public long ClientId { get; set; }
        public string BUName { get; set; }
       public string OrgName { get; set; }
       public int? GroupNumber { get; set; }
        public bool IsDeletable { get; set; }
        public List<LocalBUSiteDetails> BUSiteDetails { get; set; }
       public IEnumerable<LocalBUSiteDetails> TempBUSiteDetails { get; set; }
       public List<PrequalificationTrainingAnnualFees> PrequalificationTrainingAnnualFees { get; set; }// Mani on 8/18/2015
       public List<ClientTemplatesBUGroupPrice> ClientTemplatesBUGroupPrice { get; set; }// Mani on 8/18/2015
        public LocalBUDetails()
        {
            IsDeletable = true;
        }
    }

   public class LocalBUSiteDetails
   {
       public long BUsiteId { get; set; }
       public string BUsiteName { get; set; }
       public bool Ischecked { get; set; }
       public bool? IsPaymentDone { get; set; }// Mani on 8/18/2015
   }
}
