﻿/*
 *****************************************************
Page Created Date:  05/11/2013
Created By: Siva Bommisetty
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FVGen3.Domain.Entities
{
    public class SystemRoles
    {
        public Guid ApplicationId { get; set; }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string RoleId { get; set; }
        public string RoleName { get; set; }
        public string LoweredRoleName { get; set; }
        public string Description { get; set; }
        public string OrganizationType { get; set; }

        public virtual List<SystemRolesPermissions> SystemRolesPermissions { get; set; }
        public virtual List<OrganizationRoles> OrganizationRoles { get; set; }
    }
}
