﻿/*
 *****************************************************
Page Created Date:  05/01/2013
Created By: Siva Bommisetty
Purpose:
Version: 1.0
****************************************************
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FVGen3.Domain.Entities
{
    public class SystemUsers
    {
        public Guid ApplicationId { get; set; }
        
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid UserId { get; set; }
        //public long OrganizationID { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public System.Nullable<int> PasswordFormat { get; set; }
        public string PasswordSalt { get; set; }
        public string MobilePIN { get; set; }
        public int? TagNumber { get; set; }
        public int? Badgenumber { get; set; }
        public string Email { get; set; }
        public string LoweredEmail { get; set; }
        public string PasswordQuestion { get; set; }
        public string PasswordAnswer { get; set; }
        public System.Nullable<bool> IsApproved { get; set; }
        public System.Nullable<bool> IsLockedOut { get; set; }
        public System.Nullable<DateTime> CreateDate { get; set; }
        public System.Nullable<DateTime> LastLoginDate { get; set; }
        public System.Nullable<DateTime> LastPasswordChangedDate { get; set; }
        public System.Nullable<DateTime> LastLockoutDate { get; set; }
        public System.Nullable<int> FailedPasswordAttemptCount { get; set; }
        public System.Nullable<DateTime> FailedPasswordAttemptWindowStart { get; set; }
        public System.Nullable<int> FailedPasswordAnswerAttemptCount { get; set; }
        public System.Nullable<DateTime> FailedPasswordAnswerAttemptWindowStart { get; set; }
        public string Comment { get; set; }
        public bool? UserStatus { get; set; }
        public long LanguageId { get; set; }
        //Foreign key to Organization
        //public virtual List<SystemUsersOrganizations> SystemUsersOrganizations { get; set; }

        public virtual List<Contact> Contacts {get; set;}
        public virtual List<UserDepartments> UserDepartments { get; set; }
        public virtual List<SystemUsersOrganizations> SystemUsersOrganizations { get; set; }

        public virtual List<ClientBusinessUnitSiteRepresentatives> ClientBusinessUnitSiteRepresentatives { get; set; }

        // Kiran on 03/13/2015
        public virtual List<SystemUsersOrganizationsCategories> SystemUsersOrganizationsCategories { get; set; }
        public virtual List<SystemUsersOrganizationsQuizzesCategories> SystemUsersOrganizationsQuizzesCategories { get; set; }

        public virtual List<SystemUsersBUSites> SystemUsersBUSites { get; set; }
        public virtual List<TemplateSubSectionsNotifications> TemplateSubSectionsNotifications { get; set; }

        // Kiran on 03/13/2015 Ends<<<
    }
}
