﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.Entities
{
    public class UserAcknowledgement
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public bool Acknowledgement { get; set; }
        public DateTime SubmittedOn { get; set; }
        [ForeignKey("SystemUsers")]
        public Guid SubmittedBy { get; set; }
        public virtual SystemUsers SystemUsers { get; set; } 
    }
}
