﻿/*
Page Created Date:  10/02/2013
Created By: Kiran Talluri
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace FVGen3.Domain.Entities
{
    public class LocalAddProficiencyCapability
    {
        public long ProficiencyId { get; set; }
        public string ProficiencyCategory { get; set; }
        public string ProficiencySubCategory { get; set; }
        [Required(ErrorMessage = "*")]
        public string ProficiencyCategoryName { get; set; }
        [Required(ErrorMessage = "*")]
        public string ProficiencySubCategoryName { get; set; }

        [Required(ErrorMessage="*")]
        [MaxLength(200, ErrorMessageResourceName = "ErrorMessage_Exceed200", ErrorMessageResourceType = typeof(Resources.Resources))]//sumanth on 10/31/2015 for FVOS-105 to limit the text box value.
        public string ProficiencyName { get; set; }

        // Added on 11/09/2013
        public bool Status { get; set; }
        // Ends<<

        public bool isEdit { get; set; }
    }
}
