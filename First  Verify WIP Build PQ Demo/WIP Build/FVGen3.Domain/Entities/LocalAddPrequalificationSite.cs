﻿/*
Page Created Date:  06/11/2013
Created By: Siva Bommisetty
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace FVGen3.Domain.Entities
{
    public class LocalAddPrequalificationSite
    {
        public long siteId { get; set; }
        [Required(ErrorMessage = "*")]
        public long? BusinessUnit { get; set; }
        //[Required(ErrorMessage = "*")]
        //public string BusinessUnitLocation { get; set; }
        [Required(ErrorMessage = "*")]
        public long ClientBusinessUnitSite { get; set; }
        public string Comments { get; set; }
        public long subSectionId { get; set; }
        public long preQualificationId { get; set; }

        public long? ClientId { get; set; }
        public List<SelectListItem> BusinessUnitSites { get; set; }
        
        // Sandhya on 04/08/2015
        public List<BusinessUnitSitesList> ClientBusinessUnitSites { get; set; }

        //public int BusinessUnitType { get; set; }

        public class BusinessUnitSitesList
        {
            public long BusinessUnitSiteId { get; set; }
            public string SiteName { get; set; }
            public bool isChecked { get; set; }
        }
        // Ends<<<
    }
}
