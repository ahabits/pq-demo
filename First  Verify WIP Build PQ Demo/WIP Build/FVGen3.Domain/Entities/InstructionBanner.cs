﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace FVGen3.Domain.Entities
{
    public class InstructionBanner
    {
        [Required(ErrorMessage = "*")]
        [AllowHtml]
        public string Instruction { get; set; }
    }
}
