﻿/*
 *****************************************************
Page Created Date:  08/25/2013
Created By: Siva Bommisetty
Purpose:
Version: 1.0
****************************************************
*/


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace FVGen3.Domain.Entities
{
    public class PrequalificationCompletedSections
    {
        [Key]
        public long PrequalCompletedId { get; set; }
        
        [ForeignKey("TemplateSections")]
        public long TemplateSectionId { get; set; }
        public virtual TemplateSections TemplateSections { get; set; }

        [ForeignKey("Prequalification")]
        public long PrequalificationId { get; set; }
        public virtual Prequalification Prequalification { get; set; }

        public DateTime? SubmittedDate { get; set; }
        public Guid? SubmittedBy { get; set; }
    }
}
