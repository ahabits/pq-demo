﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.Entities
{
    public class MappedQuestions
    {

        [Key]
        public long Id { get; set; }
 
        public long DestinationQuestionId { get; set; }
        [ForeignKey("Questions"), Column(Order = 1)]
        public long SourceQuestionId { get; set; }
        public long? SourceSubSectionId { get; set; }
        public long? DestinationSubSectionId { get; set; }
        public bool IsMapped { get; set; } 
        public virtual Questions Questions { get; set; }
    }

}
