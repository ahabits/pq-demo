﻿/*
Page Created Date:  10/22/2014
Created By: Kiran Talluri
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace FVGen3.Domain.Entities
{
    public class LocalQuizTemplateBuilderIFrame
    {
        public long SectionId { get; set; }
        [MaxLength(5, ErrorMessageResourceName = "ErrorMessage_SectionHeader", ErrorMessageResourceType = typeof(Resources.Resources))]
        public String SectionHeader { get; set; }
        [MaxLength(5, ErrorMessageResourceName = "ErrorMessage_SectionFooter", ErrorMessageResourceType = typeof(Resources.Resources))]
        public String SectionFooter { get; set; }
        
        [Required(ErrorMessage="*")]
        public String QuizIFrameURL { get; set; }
        [Required(ErrorMessage = "*")]
        public String QuizIFrameHeight { get; set; }
        [Required(ErrorMessage = "*")]
        public String QuizIFrameWidth { get; set; }


        public long? SectionHeaderId { get; set; }
        public long? SectionFooterId { get; set; }

        
    }
    
}
