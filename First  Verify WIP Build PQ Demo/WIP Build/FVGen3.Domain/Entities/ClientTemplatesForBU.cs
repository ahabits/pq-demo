﻿/*
Page Created Date:  08/01/2013
Created By: Siva Bommisetty
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FVGen3.Domain.Entities
{
    public class ClientTemplatesForBU
    {
        [Key]
        public long ClientTemplateForBUId { get; set; }

        [ForeignKey("ClientTemplateId")]
        public virtual ClientTemplates ClientTemplates { get; set; }
        public Guid? ClientTemplateId { get; set; }

        [ForeignKey("ClientBusinessUnits")]
        public long? ClientBusinessUnitId { get; set; }
        public virtual ClientBusinessUnits ClientBusinessUnits { get; set; }

        [ForeignKey("ClientTemplatesBUGroupPrice")]
        public long? ClientTemplateBUGroupId { get; set; }
        public virtual ClientTemplatesBUGroupPrice ClientTemplatesBUGroupPrice { get; set; }

        public virtual List<ClientTemplatesForBUSites> ClientTemplatesForBUSites { get; set; }
        public virtual List<QuizPaymentsItems> QuizPaymentsItems { get; set; }
        public virtual List<PrequalificationTrainingAnnualFees> PrequalificationTrainingAnnualFees { get; set; } //sandhya on 06/08/2015
    }
}
