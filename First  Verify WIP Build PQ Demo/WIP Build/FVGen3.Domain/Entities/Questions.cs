﻿/*
 *****************************************************
Page Created Date:  06/06/2013
Created By: Siva Bommisetty
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FVGen3.Domain.Entities
{
    public class Questions
    {
        [Key]
        public long QuestionID { get; set; }

        public string QuestionText { get; set; }
        public int DisplayOrder { get; set; }
        public bool Visible { get; set; }
        public int NumberOfColumns { get; set; }
        public bool? IsMandatory { get; set; }
        public bool? IsBold { get; set; }
        public bool ResponseRequired { get; set; }
        public bool DisplayResponse { get; set; }
        public bool? HasDependantQuestions { get; set; }

        [ForeignKey("QuestionsBank")]
        public long? QuestionBankId { get; set; }
        public virtual QuestionsBank QuestionsBank { get; set; }

      

        [ForeignKey("TemplateSubSections")]
        public long SubSectionId { get; set; }
        public virtual TemplateSubSections TemplateSubSections { get; set; }
        public string CorrectAnswer { get; set; } // Kiran on 11/3/2014

        public virtual List<QuestionColumnDetails> QuestionColumnDetails { get; set; }
        public virtual List<VendorDetailQuestions> VendorDetailQuestions { get; set; }
        public virtual List<QuestionsDependants> QuestionsDependants { get; set; }
        //public virtual List<VendorDetailQuestions> VendorDetailQuestions { get; set; }
        public virtual List<EmployeeQuizAnswers> EmployeeQuizAnswers { get; set; }
        public virtual List<MappedQuestions> MappedQuestions { get; set; }
        public virtual List<QuestionColors> QuestionColors { get; set; }
        //sandhya on 04-11-2015
        public bool HasHighlights { get; set; }
        public string ReportForKeyword { get; set; }
        public int? GroupAsOneRecord { get; set; }
        // Ends<<<
    }
}
