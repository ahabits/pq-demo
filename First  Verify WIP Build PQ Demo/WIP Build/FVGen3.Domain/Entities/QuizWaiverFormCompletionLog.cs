﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.Entities
{
    public class QuizWaiverFormCompletionLog
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        [ForeignKey("SystemUsersOrganizationsQuizzes")]
        public long QuizId { get; set; }
        public DateTime SubmittedDate { get; set; }
        public Guid SubmittedBy { get; set; }
        public string Signature { get; set; }
        public virtual SystemUsersOrganizationsQuizzes SystemUsersOrganizationsQuizzes { get; set; } 
    }
}
