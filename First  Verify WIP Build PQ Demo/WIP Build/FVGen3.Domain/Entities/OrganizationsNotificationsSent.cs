﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FVGen3.Domain.Entities
{
    public class OrganizationsNotificationsSent
    {
        [Key]
        public long NotificationId { get; set; }

        public int SetupType { get; set; }
        public long? SetupTypeId { get; set; }
        //public DateTime ExpiringDateTime { get; set; }
        //public bool? FirstNotificationSent { get; set; }
        //public bool? SecondNotificationSent { get; set; }
        //public bool? ThirdNotificationSent { get; set; }

        [ForeignKey("ClientId"), InverseProperty("OrganizationsNotificationsSentClientList")]
        public virtual Organizations Client { get; set; }
        public long? ClientId { get; set; }


        public virtual List<OrganizationsNotificationsEmailLogs> OrganizationsNotificationsEmailLogs { get; set; }

    }
}
