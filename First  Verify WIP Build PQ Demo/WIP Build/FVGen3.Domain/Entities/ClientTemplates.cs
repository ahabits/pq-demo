﻿/*
Page Created Date:  06/11/2013
Created By: Siva Bommisetty
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FVGen3.Domain.Entities
{
    public class ClientTemplates
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ClientTemplateID { get; set; }
     
        public int DisplayOrder { get; set; }
        public bool Visible { get; set; }
        public bool DefaultTemplate { get; set; }
        //public int? TemplateType { get; set; }

        [ForeignKey("ClientID")]
        public virtual Organizations Organizations { get; set; }
        public long ClientID { get; set; }

        [ForeignKey("Templates")]
        public Guid TemplateID { get; set; }
        public virtual Templates Templates { get; set; }
        //public string QuizVideoURL { get; set; }
        //public decimal? QuizPassRate { get; set; }

        public virtual List<ClientTemplateReportingData> ClientTemplateReportingData { get; set; }
        public virtual List<Prequalification> Prequalification { get; set; }
        public virtual List<SystemUsersOrganizationsQuizzes> SystemUsersOrganizationsQuizzes { get; set; }
        public virtual List<ClientTemplatesForBU> ClientTemplatesForBU { get; set; }
        //public virtual List<EmployeeQuizzes> EmployeeQuizzes { get; set; }
        public virtual List<ClientTemplatesQuizDetails> ClientTemplatesQuizDetails { get; set; } // Kiran on 10/28/2014
        public virtual List<ClientTemplatesBUGroupPrice> ClientTemplatesBUGroupPrice { get; set; } // Sandhya on 05/08/2015
        
    }
}
