﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.Entities
{
    public class EmpRegistrationLocalModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }  
        public string MobileNumber { get; set; }
        public string Password { get; set; }
        public long VendorId { get; set; } 
        public long? LanguageId { get; set; }
    }
}
 