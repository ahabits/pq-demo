﻿/*
Page Created Date:  10/24/2013
Created By: Kiran Talluri
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace FVGen3.Domain.Entities
{
    public class LocalAddQuestionInQuestionBank
    {        
        public long QuestionBankId { get; set; }
        [Required(ErrorMessage = "*")]
        public string QuestionString { get; set; }
        public bool isEdit { get; set; }

        public bool QuestionStatus { get; set; } // Kiran on 11/23/2013
        public int QuestionFor { get; set; } // Kiran on 11/29/2013


    }
}
