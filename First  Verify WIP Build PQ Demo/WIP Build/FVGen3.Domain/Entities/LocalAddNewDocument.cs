﻿/*
Page Created Date:  06/11/2013
Created By: Siva Bommisetty
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace FVGen3.Domain.Entities
{
    public class LocalAddNewDocument
    {
        public long OrganizationID { get; set; }

        [Required(ErrorMessage = "*")]
        public string TemplateID { get; set; }

        //Nov 22nd
        [Required(ErrorMessage = "*")]
        public string IntroOrUnit { get; set; }

        [Required(ErrorMessage = "*")]
        public string TemplateSectionID { get; set; }

        [Required(ErrorMessage = "*")]
        public string ClientBusinessUnitId { get; set; }

        [Required(ErrorMessage = "*")]
        public string HeadOrFootOrSubSec { get; set; }

        [Required(ErrorMessage = "*")]
        public string HeadFootSubSecId { get; set; }

        [StringLength(100, ErrorMessageResourceName = "ErrorMessage_Exceed100", ErrorMessageResourceType = typeof(Resources.Resources))]
        public string headerOrFooterTitle { get; set; }

        [Required(ErrorMessage = "*")]
        public string file { get; set; }

        [Required(ErrorMessage = "*")]
        [StringLength(100, ErrorMessageResourceName = "ErrorMessage_Exceed100", ErrorMessageResourceType = typeof(Resources.Resources))]
        public string fileTitle { get; set; }

        [Required(ErrorMessage = "*")]
        public int displayOrder
        {
            get;
            set;
        }
        [Required(ErrorMessage = "*")]
        public long ClientBusinessUnitsiteId { get; set; }
    }
}
