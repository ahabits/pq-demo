﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace FVGen3.Domain.Entities
{
    public class LocalSystemRoles
    {
        
        [Required(ErrorMessageResourceName = "ErrorMessage_Role", ErrorMessageResourceType = typeof(Resources.Resources))]
        public string RoleName { get; set; }

        [Required(ErrorMessage = "*")]
        public string OrgType { get; set; }

    }
}
