﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FVGen3.Domain.Entities
{
    public class EmployeeQuizAnswers
    {
        [Key]
        public long QuizQuestAnsId { get; set; }
        
        public string CorrectAnswer { get; set; }

        [ForeignKey("Questions")]
        public long QuestionId { get; set; }
        public virtual Questions Questions { get; set; }

    }
}
