﻿/*
Page Created Date:  01/20/2014
Created By: Rajesh
Purpose:
Version: 1.0
****************************************************
*/

using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;

namespace FVGen3.Domain.Entities
{
    public class TemplateSubSectionsPermissions
    {
        
        //>>DV:SB,KT
        [Key]
        public long SubSectionPermissionId { get; set; }

        [ForeignKey("TemplateSectionsPermission")]
        public long SectionsPermissionId { get; set; }
        public virtual TemplateSectionsPermission TemplateSectionsPermission { get; set; }
        [ForeignKey("TemplateSubSections")]
        public long SubSectionId { get; set; }
        public virtual TemplateSubSections TemplateSubSections { get; set; }
        public string PermissionFor { get; set; }

        public int PermissionType { get; set; }

        public virtual List<TemplateSubSectionsUserPermissions> TemplateSubSectionsUserPermissions { get; set; } 
    }
}