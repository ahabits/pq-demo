﻿/*
Page Created Date:  10/02/2013
Created By: Kiran Talluri
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FVGen3.Domain.Entities
{
    public class ProficiencyCapabilities
    {   
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long ProficiencyId { get; set; }

        public string ProficiencyCategory { get; set; }
        public string ProficiencySubCategroy { get; set; }
        public string ProficiencyName { get; set; }
        // Added code on 11/09/2013
        public int? Status { get; set; }
        // Ends<<<
    }
}
