﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.ViewModels
{
    public class PrequalificationCommentsViewModel
    {
        public DateTime EmailSentDateTime { get; set; } 
        public string Name { get; set; }
        public string Comments { get; set; }
        public int SetupType { get; set; }
        public int NotificationType { get; set; }
        public long? PrequalificationId { get; set; }
        public long EmailRecordId { get; set; }
        public string UserEmail { get; set; }
        public string UserContact { get; set; }
    }
}
