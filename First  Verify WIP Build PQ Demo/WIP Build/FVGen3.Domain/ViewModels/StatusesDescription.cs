﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.ViewModels
{
    public class StatusesDescription
    {
        public string StatusName { get; set; }
        public string Description { get; set; }
        public int DisplayOrder { get; set; }
    }
}
