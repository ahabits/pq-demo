﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.ViewModels
{
    public class PqPaymentsViewModel
    {
        public decimal? PqPrice { get; set; }
        public bool? PaymentReceived { get; set; }
        public List<ClientBusinessUnitsViewModel> PqBus { get; set; }

        public List<IGrouping<int?, ClientTemplatesBuGroupPriceViewModel>>
            BuGroupPrice { get; set; }
        public List<ClientTemplatesForBuViewModel> UnPaidBus { get; set; }
        public List<IGrouping<long?, PqSitesViewModel>> PaidAdditionalBu { get; set; }
        public List<PrequalificationPaymentsViewModel> UnusedPaymentIds { get; set; }
        public int MonthDiff { get; set; }
        public List<long?> PaidBus { get; set; } 
        public List<long?> UnPaidGroups { get; set; }
        public decimal TotalAdditionalFee { get; set; }
        public List<IGrouping<long?, ClientTemplatesForBuViewModel>> AnnualFeeBus { get; set; }
        public List<IGrouping<long, PrequalificationTrainingAnnualFeesViewModel>> AnnualPaidBus
        {
            get;
            set;
        }
        public decimal TotalAnnualFee { get; set; }
        public decimal TemplatePrice { get; set; }
        public bool IsEri { get; set; } 
    }
    

    public class ClientBusinessUnitsViewModel
    {
        public string BusinessUnitName { get; set; }
    }

    public class ClientTemplatesBuGroupPriceViewModel
    {
        public decimal? GroupPrice { get; set; }
        public int? GroupPriceType { get; set; }
        public List<ClientTemplatesForBuViewModel> ClientTemplatesForBu { get; set; }
    }

    public class ClientTemplatesForBuViewModel
    {
        public string BusinessUnitName { get; set; }
        public decimal? GroupPrice { get; set; }
        public long? ClientTemplateBuGroupId { get; set; }
        public string ClientName { get; set; }
        public long? ClientBusinessUnitId { get; set; }
    }

    public class PqSitesViewModel
    {
        public decimal? PaymentReceivedAmount { get; set; }
        public int? PaymentCategory { get; set; }
        public long? ClientBusinessUnitId { get; set; }
        public string ClientName { get; set; }
        public string BusinessUnitName { get; set; }
        public long? PrequalificationPaymentsId { get; set; }
    }

    public class PrequalificationPaymentsViewModel
    {
        public decimal? PaymentReceivedAmount { get; set; }
    }

    public class PrequalificationTrainingAnnualFeesViewModel
    {
        public string BusinessUnitName { get; set; }
        public decimal? AnnualFeeReceivedAmount { get; set; }
        public long ClientTemplateBuGroupId { get; set; }
    }
}
