﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.ViewModels
{
    public class VendorDetailsViewModel
    {
        public long ClientId { get; set; }
        public long VendorId { get; set; }
        public Guid TemplateID { get; set; }
        public Guid? ClientTemplateID { get; set; }
        public string ClientOrganizationType { get; set; }
        public bool IsERI { get; set; }
        public bool? PaymentReceived { get; set; }
        public string ClientName { get; set; }
        public string VendorName { get; set; }
        public DateTime PrequalificationStart { get; set; }
        public DateTime PrequalificationFinish { get; set; }
        public DateTime PrequalificationCreate { get; set; }
        public string PrequalificationStatusName { get; set; }
        public long PrequalificationStatusId { get; set; }
        public bool? HasFlag { get; set; }
        public string ClientVendorRefId { get; set; }
        public bool ShowScorecard { get; set; }
        public long RecommendedCount { get; set; }
        public long NotRecommendedCount { get; set; }
        public long DefaultTemplateSectionId { get; set; }
        public long PaymentSectionId { get; set; }
        public bool TemplateHasPaymentSection { get; set; }
        public string TemplateCode { get; set; }
        public bool HasVendorDetailLocation { get; set; }
        public List<VendorContact> vendorContacts { get; set; }
        public List<string> subSectionNames { get; set; }

        public VendorOrgDetails vendorOrgDetails { get; set; }

        public List<LocalVendorDetails> pqSubmittedDetails { get; set; }

        public LocalVendorDetails pqUserIdLoggedInDetails { get; set; }
        public bool? Locked { get; set; }
        public bool? RequestForMoreInfo { get; set; }
        public Guid? UserIdAsCompleter { get; set; }
    }

    public class VendorContact
    {
        public string FullName { get; set; }
        public string Title { get; set; }
        public string EmailAddress { get; set; }
        public string TelephoneNumber { get; set; }
        public long? GroupNumber { get; set; }
    }

    public class VendorOrgDetails
    {
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string PhoneNumber { get; set; }
        public string FaxNumber { get; set; }
        public string WebsiteURL { get; set; }
        public string TaxID { get; set; }
    }

    public class LocalVendorDetails
    {
        public LocalVendorDetails()
        {
            LoggedInUserForPreQualification = false;
        }
        public string FullName { get; set; }
        public string ContactTile { get; set; }
        public DateTime? PrequalificationSubmit { get; set; }
        public DateTime? StatusChangeDate { get; set; }
        public bool LoggedInUserForPreQualification { get; set; }
        public string Email { get; set; }
    }

}
