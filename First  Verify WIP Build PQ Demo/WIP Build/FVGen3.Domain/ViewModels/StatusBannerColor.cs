﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.ViewModels
{
    public class StatusBannerColor
    {
        public string StatusName { get; set; } 
        public string BannerColor { get; set; }
    }
}
