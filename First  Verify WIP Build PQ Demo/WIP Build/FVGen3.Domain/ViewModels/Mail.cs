﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.ViewModels
{
   public class Mail
    { public string toAddress { get; set; } 
        public string body { get; set; }
        public string subject { get; set; }
        public string bcc { get; set; }
        public string cc { get; set; }
    }
}
