﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.ViewModels
{
    public class PqReferenceDocumentsViewModel
    {
        public Guid DocumentId { get; set; }
        public string DocumentName { get; set; }
        public long OrganizationId { get; set; }
    }
}
