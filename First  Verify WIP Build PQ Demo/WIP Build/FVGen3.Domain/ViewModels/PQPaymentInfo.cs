﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.ViewModels
{
    public class PQPaymentInfo
    {
        public bool HasPaymentSection { get; set; } 
        public long PaymentSectionId { get; set; }
    }
}
