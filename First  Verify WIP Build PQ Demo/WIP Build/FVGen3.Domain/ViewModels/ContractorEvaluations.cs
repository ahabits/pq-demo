﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.ViewModels
{
    public class ContractorEvaluations
    {
        public List<VendorReviewInputViewModel> VendorReviews { get; set; }
        public long PqStatusId { get; set; }
        public string VendorName { get; set; }
        public string ClientName { get; set; }
        public DateTime PrequalificationCreate { get; set; }
        public string PqStatusName { get; set; }
        public long PqId { get; set; }
        public long? VendorId { get; set; }
        public long? BuId { get; set; }
    }
}
