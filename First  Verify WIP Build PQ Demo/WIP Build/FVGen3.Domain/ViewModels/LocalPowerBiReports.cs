﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.ViewModels
{
  public class LocalPowerBiReports
    {
        public LocalPowerBiReports() 
        {
            IsSelected = false;
        }
        public long ReportId { get; set; }
        public string ReportName { get; set; }
        public string DisplayName { get; set; }
        public int Order { get; set; }
        public string Url { get; set; }
        public bool IsSelected { get; set; }
    }
}
