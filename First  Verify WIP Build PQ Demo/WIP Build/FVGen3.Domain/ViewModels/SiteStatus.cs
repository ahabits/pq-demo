﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.ViewModels
{
    public class SiteStatus
    {
        public long SiteId { get; set; }
        public long Status { get; set; }
    }
}
