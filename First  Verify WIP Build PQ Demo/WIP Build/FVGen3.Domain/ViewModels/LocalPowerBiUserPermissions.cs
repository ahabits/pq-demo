﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.ViewModels
{
   public class LocalPowerBiUserPermissions
    {public long ID { get; set; }
        public string OrgName { get; set; } 
        public long? OrgId { get; set; }
        public long ReportId { get; set; }
        public string PermissionTo { get; set; }
        public int PermissionType { get; set; }
        public string ReportName { get; set; }
        public string Users { get; set; }
        public string UserId { get; set; }
    }
}
