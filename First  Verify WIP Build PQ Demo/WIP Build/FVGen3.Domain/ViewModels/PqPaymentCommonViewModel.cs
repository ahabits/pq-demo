﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.ViewModels
{
    public class PqPaymentCommonViewModel
    {
        public decimal TotalAdditionalFee { get; set; }
        public List<IGrouping<long?, ClientTemplatesForBuViewModel>> AnnualFeeBus { get; set; }
        public decimal TemplatePrice { get; set; }
        public decimal TotalAnnualFee { get; set; }
        public List<long?> PqSitesBuUnits { get; set; }
        public int MonthDiff { get; set; }
    }
}
