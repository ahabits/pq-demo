﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FVGen3.Domain.Entities;

namespace FVGen3.Domain.ViewModels
{
    public class PqViewModel
    {
        public List<long?> PqSiteBus { get; set; }
        public List<Guid> ClientTemplateIds { get; set; }
        public List<long> AdditionalFeeBus { get; set; }
        public List<ClientTemplatesForBU> ClientTemplatesForBus { get; set; }

    }

    public class QuestionsViewModel
    {
        public long QuestionId { get; set; }
        public long? QuestionBankId { get; set; }
        public int NumberOfColumns { get; set; }
        public bool? IsMandatory { get; set; }
        public bool? IsBold { get; set; }
        public string QuestionText { get; set; }
        public bool? HasDependantQuestions { get; set; }
        public long SubSectionId { get; set; }
        public bool Visible { get; set; }
        public int DisplayOrder { get; set; }
        public long? QuestionReferenceId { get; set; }
        public bool HasHighlights { get; set; }
        public List<QuestionColumnDetailsViewModel> QuestionColumnDetails { get; set; }
        public List<QuestionsDependantsViewModel> QuestionsDependants { get; set; }
        public List<QuestionColorsViewModel> QuestionColors { get; set; }
    }

    public class QuestionColumnDetailsViewModel
    {
        public long QuestionColumnId { get; set; }
        public int ColumnNo { get; set; }
        public string ColumnValues { get; set; }
        public int DisplayType { get; set; }
        public int? ButtonTypeReferenceValue { get; set; }
        public long QuestionTypeId { get; set; }
        public string TableReference { get; set; }
        public string TableReferenceField { get; set; }
        public string TableRefFieldValueDisplay { get; set; }
        public string SortReferenceField { get; set; }
        public long QuestionId { get; set; }
        public string QFormula { get; set; }
        public string QuestionColumnUserInput { get; set; }
        public bool MpaSectionSignedQuestionHasUserInput { get; set; }
    }

    public class QuestionsDependantsViewModel
    {
        public int DependantType { get; set; }
        public long DependantId { get; set; }
        public string DependantValue { get; set; }
    }

    public class QuestionColorsViewModel
    {
        public string Color { get; set; }
        public string Answer { get; set; }
    }
}
