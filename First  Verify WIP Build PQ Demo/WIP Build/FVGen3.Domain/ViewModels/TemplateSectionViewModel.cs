﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.ViewModels
{
    public class TemplateSectionViewModel
    {
        public long PqId { get; set; }
        public int? TemplateSectionType { get; set; }
        public DateTime PqSectionSubmittedDate { get; set; } 
        public bool? PqSectionCompleted { get; set; }
        public bool BlockClientDocsSelection { get; set; }
        public long PrequalificationStatusId { get; set; }
        public bool IsEri { get; set; }
        public List<TemplateSectionsPermissionViewModel> TemplateSectionsPermission { get; set; }
        public List<TemplateSectionSubSectionsViewModel> TemplateSubSections { get; set; }
        public virtual List<ClientTemplateProfilesViewModel> ClientTemplateProfiles { get; set; }
        public virtual List<TemplateSectionsHeaderFooterViewModel> TemplateSectionsHeaderFooter { get; set; }
        public List<SupportingDocumentsViewModel> OtherSectionDocs { get; set; }
        public decimal TotalAdditionalFee { get; set; }
        public List<IGrouping<long?, ClientTemplatesForBuViewModel>> AnnualFeeBus { get; set; }
        public decimal TemplatePrice { get; set; }
        public decimal TotalAnnualFee { get; set; }
        public List<long?> PqSitesBuUnits { get; set; }
        public bool InterimSubSectionExists { get; set; }
        public bool HasMultipleLocations { get; set; }
        public bool RestrictedSectionEdit { get; set; }
        public Guid VendorSuperUserId { get; set; }
        public long VendorSuperUserContactId { get; set; }
        public int VisibleTo { get; set; }
        public int? Mode { get; set; }
        public Guid TemplateId { get; set; }
        public long VendorId { get; set; }
        public long ClientId { get; set; }
        public string ClientName { get; set; }
        public string VendorName { get; set; }
        public string VendorSuperUserContactName { get; set; }
        public string VendorAddress1 { get; set; }
        public string VendorAddress2 { get; set; }
        public string VendorCity { get; set; }
        public string VendorState { get; set; }
        public string VendorZip { get; set; }
        public string LogoPath { get; set; }
        public string FinalizedUserEmail { get; set; }
        public DateTime? FinalizedDateTime { get; set; }
        public string FinalizedUserName { get; set; }
        public string ClientVendorRefId { get; set; }
    }

    public class ClientTemplateProfilesViewModel
    {
        public int PositionType { get; set; }
        public long ClientId { get; set; }
        public int? ContentType { get; set; }
        public string PositionTitle { get; set; }
        public string PositionContent { get; set; }
        public bool AttachmentExist { get; set; }
        public long ClientTemplateProfileId { get; set; }
    }

    public class TemplateSectionsHeaderFooterViewModel
    {
        public int? PositionType { get; set; }
        public string PositionTitle { get; set; }
        public string PostionContent { get; set; }
        public bool? AttachmentExist { get; set; }
        public long HeaderFooterId { get; set; }
    }
    public class TemplateSectionsPermissionViewModel
    {
        public int VisibleTo { get; set; }
    }
    public class TemplateSectionSubSectionsViewModel
    {
        public long SubSectionId { get; set; }
        public bool? Visible { get; set; }
        public long? ClientId { get; set; }
        public int DisplayOrder { get; set; }
        public bool? SubHeader { get; set; }
        public string SectionNotes { get; set; }
        public int? NoOfColumns { get; set; }
        public bool? AttachmentExist { get; set; }
        public int? SubSectionType { get; set; }
        public string SubSectionName { get; set; }
        public string SubSectionTypeCondition { get; set; }

        public List<TemplateSubSectionsPermissionsViewModel> TemplateSubSectionsPermissions { get; set; }
        public List<QuestionsViewModel> Questions { get; set; }
    }

    public class TemplateSubSectionsPermissionsViewModel
    {
        public string PermissionFor { get; set; }
        public int PermissionType { get; set; }
        public List<TemplateSubSectionsUserPermissionsViewModel> TemplateSubSectionsUserPermissions { get; set; }
    }

    public class TemplateSubSectionsUserPermissionsViewModel
    {
        public Guid UserId { get; set; }
        public bool EditAccess { get; set; }
    }
}
