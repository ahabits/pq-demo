﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.ViewModels
{
    public class ArchiveOrganizationDetailsViewModel
    {
        public int VersionNo { get; set; }
        public string ChangedBy { get; set; }
        public DateTime ChangedDateTime { get; set; }
        public string LegalNameOfBusiness { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Country { get; set; }
        public string PhoneNumber { get; set; }
        public string FaxNumber { get; set; }
        public string WebsiteUrl { get; set; }
        public string FederalIdNumber { get; set; }
        public string PrincipalCompanyOfficerName { get; set; }
        public string TaxId { get; set; }
        public string OrgRepresentativeName { get; set; }
        public string OrgRepresentativeEmail { get; set; }
    }

}
