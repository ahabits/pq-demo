﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.ViewModels
{
   public class CurrentlyOnsite
    { 
        public string VendorName { get; set; }
        public string VisitorName { get; set; }
        public string Checkin { get; set; }
        public string CompanyName { get; set; } 
       public string SiteName { get; set; }
        public string CheckinUser { get; set; }
        public string SiteId { get; set; }
    }
}
