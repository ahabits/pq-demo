﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FVGen3.Domain.Entities;
namespace FVGen3.Domain.ViewModels
{
    public class OrganizationUsers
    {
        public bool? UserStatus { get; set; } 
        public long EmployeeCount { get; set; }
        public string EmployeeStatus { get; set; }

        public Guid UserId { get; set; }
        public string EmployeeFullName { get; set; }
        public string EmployeeTitle { get; set; }
        public string DocumentType { get; set; }
        public string DocumentTypeValue { get; set; }
        public long SysUserOrganizationId { get; set; }

        public List<EmployeesCategories> employeesCategories { get; set; }        
    }
    public class EmployeesCategories
    {
        public string CategoryName { get; set; }
        public string UserName { get; set; }
        public string SubCategoryInputValue { get; set; }
        public string SubCategoryEnteredDate { get; set; }
    }
}
