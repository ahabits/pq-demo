﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.ViewModels
{
    public class SupportingDocumentsViewModel
    {
        public string OrganizationType { get; set; }
        public long? ClientId { get; set; }
        public string DocumentStatus { get; set; }
        public string DocumentTypeName { get; set; }
        public long DocumentTypeId { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public string DocumentName { get; set; }
        public bool RestrictAccess { get; set; }
        public Guid DocumentId { get; set; }
        public long OrganizationId { get; set; }
        public DateTime? Expires { get; set; }
        public DateTime Uploaded { get; set; }
        public string UploaderFullName { get; set; }
        public string UpdatedByUserFullName { get; set; }
        public DateTime? StatusChangeDate { get; set; }
        public string StatusChangedUserFullName { get; set; }
        public System.Nullable<Guid> StatusChangedBy { get; set; }
        public string ClientName { get; set; }
    }
}
