﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.ViewModels
{
    public class VendorReviewInputViewModel
    {
        public long VendorReviewInputId { get; set; }
        public Guid InputUserId { get; set; }
        public string Comments { get; set; }
        public DateTime? ReviewDate { get; set; }
        public decimal UserRating { get; set; }

        public string InputUserEmail { get; set; }
        public string InputUserName { get; set; }
    }
}
