﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.Domain.ViewModels
{
    public class VendorPQsSpecificToClient
    {
        public long PrequalificationId { get; set; } 
        public string ClientName { get; set; }
        public DateTime PrequalificationStart { get; set; }
        public DateTime PrequalificationFinish { get; set; }
        public long PrequalificationStatusId { get; set; }
    }
}
