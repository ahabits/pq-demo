﻿/*
Page Created Date:  02/16/2015
Created By: Kiran Talluri
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FVGen3.Domain.Entities;

namespace FVGen3.Domain.Abstract
{
    public interface ISystemUsersOrganizationsQuizzesCategoriesRepository
    {
        IEnumerable<SystemUsersOrganizationsQuizzesCategories> GetSysUserOrgQuizzesCategories();
        SystemUsersOrganizationsQuizzesCategories GetSysUserOrgQuizzesCategoriesByID(long sysUserOrgCatId);
        void InsertSysUserOrgQuizCategory(SystemUsersOrganizationsQuizzesCategories empQuizCategory);
        void UpdateSysUserOrgQuizCategory(SystemUsersOrganizationsQuizzesCategories sysUserOrgQuizCatId);
        void DeleteSysUserOrgQuizCategory(long sysUserOrgQuizCatId);
        void Save();

        IQueryable<SystemUsersOrganizationsQuizzesCategories> SystemUsersOrganizationsQuizzesCategories { get; }
    }
}
