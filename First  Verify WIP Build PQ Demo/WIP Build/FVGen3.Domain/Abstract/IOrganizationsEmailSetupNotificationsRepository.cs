﻿/*
Page Created Date:  13/08/2013
Created By: Kiran Talluri
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FVGen3.Domain.Entities;

namespace FVGen3.Domain.Abstract
{
    public interface IOrganizationsEmailSetupNotificationsRepository
    {
        IQueryable<OrganizationsEmailSetupNotifications> OrganizationsEmailSetupNotifications { get; }
    }
}
