﻿/*
Page Created Date:  02/16/2015
Created By: Kiran Talluri
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FVGen3.Domain.Entities;


namespace FVGen3.Domain.Abstract
{
    public interface IEmployeeQuizCategoriesRepository
    {
        IEnumerable<EmployeeQuizCategories> GetCategories();
        List<EmployeeQuizCategories> GetTrainingCategories();
        List<EmployeeQuizCategories> GetEmployeeCategories();
        EmployeeQuizCategories GetCategoryByID(long empQuizCategoryId);
        
        IQueryable<EmployeeQuizCategories> EmployeeQuizCategories { get; }
    }
}
