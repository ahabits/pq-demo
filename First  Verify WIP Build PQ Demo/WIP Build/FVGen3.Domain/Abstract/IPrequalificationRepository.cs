﻿/*
Page Created Date:  05/18/2013
Created By: Suma
Purpose:
Version: 1.0
****************************************************
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FVGen3.Domain.Entities;


namespace FVGen3.Domain.Abstract
{

    public interface IPrequalificationRepository
    {
        IQueryable<Prequalification> Prequalification { get; }
    }
}
