﻿/*
Page Created Date:  11/22/2013
Created By: Kiran Talluri
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FVGen3.Domain.Entities;

namespace FVGen3.Domain.Abstract
{    
    public interface IPrequalificationStatusChangeLogRepository
    {
        IQueryable<PrequalificationStatusChangeLog> PrequalificationStatusChangeLog { get; }
    }
}
