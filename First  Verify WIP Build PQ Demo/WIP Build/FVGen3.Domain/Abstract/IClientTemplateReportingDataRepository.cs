﻿/*
Page Created Date:  10/02/2013
Created By: Kiran Talluri
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FVGen3.Domain.Entities;

namespace FVGen3.Domain.Abstract
{
    public interface IClientTemplateReportingDataRepository
    {
        IQueryable<ClientTemplateReportingData> ClientTemplateReportingData { get; }
    }
}
