﻿/*
Page Created Date:  08/06/2013
Created By: Suma
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FVGen3.Domain.Entities;

namespace FVGen3.Domain.Abstract
{
    public interface ITemplateDiscountPricesNoInUseRepository
    {
        IQueryable<TemplateDiscountPricesNotInUse> TemplateDiscountPrices { get; }
    }
}

