﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FVGen3.Domain.Entities;

namespace FVGen3.Domain.Abstract
{
    public interface ISystemRolesPermissionsRepository
    {
        IQueryable<SystemRolesPermissions> SystemRolesPermissions { get; }
    }
}
