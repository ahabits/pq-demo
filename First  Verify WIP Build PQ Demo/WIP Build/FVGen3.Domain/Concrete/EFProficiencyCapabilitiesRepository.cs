﻿/*
Page Created Date:  10/02/2013
Created By: Kiran Talluri
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FVGen3.Domain.Entities;
using FVGen3.Domain.Abstract;

namespace FVGen3.Domain.Concrete
{
    public class EFProficiencyCapabilitiesRepository : IProficiencyCapabilitiesRepository, IDisposable
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<ProficiencyCapabilities> ProficiencyCapabilities
        {
            get { return context.ProficiencyCapabilities; }
        }
        public void Dispose()
        {
            context.Dispose();
        }
    }
}
