﻿/*
Page Created Date:  11/22/2013
Created By: Kiran Talluri
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FVGen3.Domain.Entities;
using FVGen3.Domain.Abstract;

namespace FVGen3.Domain.Concrete
{
    public class EFPrequalificationStatusChangeLogRepository : IPrequalificationStatusChangeLogRepository, IDisposable
    {
        private EFDbContext context = new EFDbContext();
        public IQueryable<PrequalificationStatusChangeLog> PrequalificationStatusChangeLog
        {
            get { return context.PrequalificationStatusChangeLog; }

        }
        public void Dispose()
        {
            context.Dispose();
        }

    }
}
