﻿/*
Page Created Date:  02/16/2015
Created By: Kiran Talluri
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FVGen3.Domain.Abstract;
using FVGen3.Domain.Entities;

namespace FVGen3.Domain.Concrete
{
    public class EFSystemUsersOrganizationsCategoriesRepository : ISystemUsersOrganizationsCategoriesRepository, IDisposable
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<SystemUsersOrganizationsCategories> SystemUsersOrganizationsCategories
        {
            get { return context.SystemUsersOrganizationsCategories; }
        }
        public void Dispose()
        {
            context.Dispose();
        }
    }
}
