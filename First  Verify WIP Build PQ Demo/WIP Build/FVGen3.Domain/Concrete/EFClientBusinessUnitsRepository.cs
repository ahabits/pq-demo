﻿/*
Page Created Date:  06/07/2013
Created By: Siva Bommisetty
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FVGen3.Domain.Abstract;
using FVGen3.Domain.Entities;

namespace FVGen3.Domain.Concrete
{
    public class EFClientBusinessUnitsRepository : IClientBusinessUnitsRepository ,IDisposable
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<ClientBusinessUnits> ClientBusinessUnits
        {
            get { return context.ClientBusinessUnits; }
        }

        public void Dispose()
        {
            context.Dispose();
        }
    }
}
