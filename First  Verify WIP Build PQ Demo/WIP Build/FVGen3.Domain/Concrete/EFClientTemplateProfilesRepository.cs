﻿/*
Page Created Date:  08/28/2013
Created By: Siva Bommisetty
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FVGen3.Domain.Abstract;
using FVGen3.Domain.Entities;

namespace FVGen3.Domain.Concrete
{
    public class EFClientTemplateProfilesRepository : IClientTemplateProfilesRepository,IDisposable
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<ClientTemplateProfiles> ClientTemplateProfiles
        {
            get { return context.ClientTemplateProfiles; }
        }

        public void Dispose()
        {
            context.Dispose();
        }
    }
}
