﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FVGen3.Domain.Abstract;
using FVGen3.Domain.Entities;

namespace FVGen3.Domain.Concrete
{
    public class EFSystemViewsRepository : ISystemViewsRepository, IDisposable
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<SystemViews> SystemViews
        {
            get { return context.SystemViews; }
        }
        public void Dispose()
        {
            context.Dispose();
        }
    }
}
