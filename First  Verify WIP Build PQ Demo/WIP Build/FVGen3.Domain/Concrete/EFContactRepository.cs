﻿/*
Page Created Date:  05/01/2013
Created By: Siva Bommisetty
Purpose:
Version: 1.0
****************************************************
*/

using FVGen3.Domain.Abstract;
using FVGen3.Domain.Entities;
using System.Linq;
using System;

namespace FVGen3.Domain.Concrete
{
    public class EFContactRepository : IContactsRepository, IDisposable

    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<Contact> Contact
        {
            get { return context.Contact; }
        }
        public void Dispose()
        {
            context.Dispose();
        }
    }
}