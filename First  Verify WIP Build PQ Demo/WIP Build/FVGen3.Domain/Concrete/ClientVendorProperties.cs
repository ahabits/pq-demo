﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace FVGen3.Domain.Entities
{
    public class ClientVendorProperties
    {
        [Key]
        public int ClientVendorPropertiesId { get; set;}
        public long ClientID { get; set; }
        public long VendorID { get; set; }
        public bool TrainingPaymentExempt { get; set; }	
    }
}
