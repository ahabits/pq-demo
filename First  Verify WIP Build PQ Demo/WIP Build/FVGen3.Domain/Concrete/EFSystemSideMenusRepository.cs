﻿/*
Page Created Date:  08/01/2013
Created By: Siva Bommisetty
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FVGen3.Domain.Abstract;
using FVGen3.Domain.Entities;

namespace FVGen3.Domain.Concrete
{
    public class EFSystemSideMenusRepository : ISystemSideMenusRepository, IDisposable
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<SystemSideMenus> SystemSideMenus
        {
            get { return context.SystemSideMenus; }
        }
        public void Dispose()
        {
            context.Dispose();
        }
    }
}
