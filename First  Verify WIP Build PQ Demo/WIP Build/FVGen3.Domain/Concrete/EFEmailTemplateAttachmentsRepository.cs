﻿/*
Page Created Date:  05/11/2013
Created By: Siva Bommisetty
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FVGen3.Domain.Entities;
using FVGen3.Domain.Abstract;

namespace FVGen3.Domain.Concrete
{
    public class EFEmailTemplateAttachmentsRepository : IEmailTemplateAttachmentsRepository,IDisposable
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<EmailTemplateAttachments> EmailTemplateAttachments
        {
            get { return context.EmailTemplateAttachments; }
        }
        public void Dispose()
        {
            context.Dispose();
        }
    }
}
