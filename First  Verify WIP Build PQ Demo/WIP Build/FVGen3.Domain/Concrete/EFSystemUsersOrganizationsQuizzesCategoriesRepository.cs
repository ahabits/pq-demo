﻿/*
Page Created Date:  02/16/2015
Created By: Kiran Talluri
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FVGen3.Domain.Entities;
using FVGen3.Domain.Abstract;
using System.Data;

namespace FVGen3.Domain.Concrete
{
    public class EFSystemUsersOrganizationsQuizzesCategoriesRepository : ISystemUsersOrganizationsQuizzesCategoriesRepository, IDisposable
    {
        private EFDbContext context = new EFDbContext();

        public IEnumerable<SystemUsersOrganizationsQuizzesCategories> GetSysUserOrgQuizzesCategories()
        {
            return context.SystemUsersOrganizationsQuizzesCategories.ToList();
        }

        public SystemUsersOrganizationsQuizzesCategories GetSysUserOrgQuizzesCategoriesByID(long id)
        {
            return context.SystemUsersOrganizationsQuizzesCategories.Find(id);
        }

        public void InsertSysUserOrgQuizCategory(SystemUsersOrganizationsQuizzesCategories empQuizCategory)
        {
            context.SystemUsersOrganizationsQuizzesCategories.Add(empQuizCategory);
        }

        public void DeleteSysUserOrgQuizCategory(long sysUserOrgQuizCatId)
        {
            SystemUsersOrganizationsQuizzesCategories empQuizCategory = context.SystemUsersOrganizationsQuizzesCategories.Find(sysUserOrgQuizCatId);
            context.SystemUsersOrganizationsQuizzesCategories.Remove(empQuizCategory);
        }

        public void UpdateSysUserOrgQuizCategory(SystemUsersOrganizationsQuizzesCategories empQuizCategory)
        {
            context.Entry(empQuizCategory).State = EntityState.Modified;
        }

        public void Save()
        {
            context.SaveChanges();
        }

        public IQueryable<SystemUsersOrganizationsQuizzesCategories> SystemUsersOrganizationsQuizzesCategories
        {
            get { return context.SystemUsersOrganizationsQuizzesCategories; }
        }
        public void Dispose()
        {
            context.Dispose();
        }
    }
}
