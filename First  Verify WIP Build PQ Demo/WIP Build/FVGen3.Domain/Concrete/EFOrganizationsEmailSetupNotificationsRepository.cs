﻿/*
Page Created Date:  13/08/2013
Created By: Kiran Talluri
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FVGen3.Domain.Entities;
using FVGen3.Domain.Abstract;

namespace FVGen3.Domain.Concrete
{
    public class EFOrganizationsEmailSetupNotificationsRepository : IOrganizationsEmailSetupNotificationsRepository,IDisposable
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<OrganizationsEmailSetupNotifications> OrganizationsEmailSetupNotifications
        {
            get { return context.OrganizationsEmailSetupNotifications; }
        }
        public void Dispose()
        {
            context.Dispose();
        }
    }
}
