﻿
/*
Page Created Date:  05/18/2013
Created By: Suma
Purpose:
Version: 1.0
****************************************************
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FVGen3.Domain.Entities;
using FVGen3.Domain.Abstract;

namespace FVGen3.Domain.Concrete
{
    public class EFPrequalificationRepository: IPrequalificationRepository, IDisposable
    {
        private EFDbContext context = new EFDbContext();
        public IQueryable<Prequalification> Prequalification
        {
            get { return context.Prequalification; }
        
        }
        public void Dispose()
        {
            context.Dispose();
        }

    }
}
