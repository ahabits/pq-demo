﻿
/*
Page Created Date:  09/04/2013
Created By: Suma
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FVGen3.Domain.Entities;
using FVGen3.Domain.Abstract;

namespace FVGen3.Domain.Concrete
{
    public class EFOrganizationsNotificationsEmailLogsRepository: IOrganizationsNotificationsEmailLogsRepository,IDisposable
    {
        private EFDbContext context = new EFDbContext();
        public IQueryable<OrganizationsNotificationsEmailLogs> OrganizationsNotificationsEmailLogs
        {
            get { return context.OrganizationsNotificationsEmailLogs; }

        }
        public void Dispose()
        {
            context.Dispose();
        }

    }
}
