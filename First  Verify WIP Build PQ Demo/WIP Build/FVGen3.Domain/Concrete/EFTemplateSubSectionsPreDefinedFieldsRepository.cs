﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FVGen3.Domain.Abstract;
using FVGen3.Domain.Entities;

namespace FVGen3.Domain.Concrete
{
    public class EFTemplateSubSectionsPreDefinedFieldsRepository : ITemplateSubSectionsPreDefinedFieldsRepository, IDisposable
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<TemplateSubSectionsPreDefinedFields> TemplateSubSectionsPreDefinedFields
        {
            get { return context.TemplateSubSectionsPreDefinedFields; }
        }
        public void Dispose()
        {
            context.Dispose();
        }
    }
}
