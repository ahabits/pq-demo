﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FVGen3.Domain.Abstract;
using FVGen3.Domain.Entities;

namespace FVGen3.Domain.Concrete
{
    public class EFPrequalificationSitesRepository : IPrequalificationSitesRepository, IDisposable
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PrequalificationSites> PrequalificationSites
        {
            get { return context.PrequalificationSites; }
        }
        public void Dispose()
        {
            context.Dispose();
        }
    }
}
