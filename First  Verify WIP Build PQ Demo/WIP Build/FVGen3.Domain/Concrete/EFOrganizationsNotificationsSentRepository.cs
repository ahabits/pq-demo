﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FVGen3.Domain.Abstract;
using FVGen3.Domain.Entities;
using System.Data;

namespace FVGen3.Domain.Concrete
{
    public class EFOrganizationsNotificationsSentRepository : IOrganizationsNotificationsSentRepository, IDisposable
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<OrganizationsNotificationsSent> OrganizationsNotificationsSent
        {
            get { return context.OrganizationsNotificationsSent; }
        }
        public void Dispose()
        {
            context.Dispose();
        }
    }
}
