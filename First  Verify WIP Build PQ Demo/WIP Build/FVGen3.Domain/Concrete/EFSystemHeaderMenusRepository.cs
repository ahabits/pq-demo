﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FVGen3.Domain.Abstract;
using FVGen3.Domain.Entities;

namespace FVGen3.Domain.Concrete
{
    public class EFSystemHeaderMenusRepository : ISystemHeaderMenusRepository, IDisposable
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<SystemHeaderMenus> SystemHeaderMenus
        {
            get { return context.SystemHeaderMenus; }
        }
        public void Dispose()
        {
            context.Dispose();
        }
    }
}
