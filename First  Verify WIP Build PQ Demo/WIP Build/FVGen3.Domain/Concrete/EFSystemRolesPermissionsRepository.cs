﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FVGen3.Domain.Abstract;
using FVGen3.Domain.Entities;

namespace FVGen3.Domain.Concrete
{
    public class EFSystemRolesPermissionsRepository : ISystemRolesPermissionsRepository, IDisposable
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<SystemRolesPermissions> SystemRolesPermissions
        {
            get { return context.SystemRolesPermissions; }
        }
        public void Dispose()
        {
            context.Dispose();
        }
    }
}
