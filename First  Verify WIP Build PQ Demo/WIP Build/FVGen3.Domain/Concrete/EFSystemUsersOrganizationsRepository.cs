﻿/*
Page Created Date:  06/11/2013
Created By: Siva Bommisetty
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Text;
using FVGen3.Domain.Abstract;
using FVGen3.Domain.Entities;
using System.Linq;

namespace FVGen3.Domain.Concrete
{
    class EFSystemUsersOrganizationsRepository : ISystemUsersOrganizationsRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<SystemUsersOrganizations> SystemUsersOrganizations
        {
            get { return context.SystemUsersOrganizations; }
        }
    }
}
