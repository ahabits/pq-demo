﻿/*
Page Created Date:  06/11/2013
Created By: Siva Bommisetty
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FVGen3.Domain.Abstract;
using FVGen3.Domain.Entities;

namespace FVGen3.Domain.Concrete
{
    public class EFQuizzesCompletedSectionsRepository : IQuizzesCompletedSectionsRepository, IDisposable
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<QuizzesCompletedSections> QuizzesCompletedSections
        {
            get { return context.QuizzesCompletedSections; }
        }
        public void Dispose()
        {
            context.Dispose();
        }
    }
}
