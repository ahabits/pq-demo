﻿/*
Page Created Date:  08/11/2015
Created By: Sumanth
Purpose:
Version: 1.0
****************************************************
*/
using FVGen3.Domain.Abstract;
using FVGen3.Domain.Entities;
using System.Linq;
using System;
namespace FVGen3.Domain.Concrete
{
    class EFClientVendorReferenceIdsRepository:IDisposable
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<ClientVendorReferenceIds> ClientVendorReferenceIds
        {
            get { return context.ClientVendorReferenceIds; }
        
        
        }
        public void Dispose()
        {
            context.Dispose();
        }
    }
}
