﻿/*
Page Created Date:  08/06/2013
Created By: Suma
Purpose:
Version: 1.0
****************************************************
*/


using System;
using System.Collections.Generic;
using System.Text;
using FVGen3.Domain.Abstract;
using FVGen3.Domain.Entities;
using System.Linq;

namespace FVGen3.Domain.Concrete
{
    class EFTemplateDiscountPricesNotInUseRepository : ITemplateDiscountPricesNoInUseRepository, IDisposable
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<TemplateDiscountPricesNotInUse> TemplateDiscountPrices
        {
            get { return context.TemplateDiscountPricesNotInUse; }
        }
        public void Dispose()
        {
            context.Dispose();
        }
    }
}
