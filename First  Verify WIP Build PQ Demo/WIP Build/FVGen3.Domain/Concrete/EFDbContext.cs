﻿/*
Page Created Date:  05/01/2013
Created By: Siva Bommisetty
Purpose:
Version: 1.0
****************************************************
*/
using FVGen3.Domain.Entities;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;

namespace FVGen3.Domain.Concrete
{
    public class EFDbContext : DbContext
    { public DbSet<ApplicationError> ApplicationError { get; set; }
        public DbSet<VendorDetailQuestions> VendorDetailQuestions { get; set; }
        public DbSet<Contact> Contact { get; set; }

        //public DbSet<VendorDetailQuestions> VendorDetailQuestions { get; set; }
        public DbSet<QuestionsForMigration> QuestionsForMigration { get; set; }
        public DbSet<RegionStates> RegionStates { get; set; }
        public DbSet<MultipleInvitationCode> MultipleInvitationCode { get; set; }
        public DbSet<QuestionColors> QuestionColors { get; set; }
        public DbSet<FinancialAnalyticsLog> FinancialAnalyticsLog { get; set; }
        public DbSet<FinancialAnalyticsModel> FinancialAnalyticsModel { get; set; }
        public DbSet<VendorEmailLog> VendorEmailLog { get; set; }
        public DbSet<Organizations> Organizations { get; set; }
        public DbSet<OrganizationRoles> OrganizationRoles { get; set; }
        public DbSet<SystemUsers> SystemUsers { get; set; }
        public DbSet<SystemUserLogs> SystemUserLogs { get; set; }
        public DbSet<SystemRoles> SystemRoles { get; set; }
        public DbSet<SystemApplicationId> SystemApplicationId { get; set; }
        public DbSet<SystemLogs> SystemLogs { get; set; }

        public DbSet<EmailTemplates> EmailTemplates { get; set; }
        public DbSet<EmailTemplateAttachments> EmailTemplateAttachments { get; set; }
        public DbSet<SystemUsersOrganizations> SystemUsersOrganizations { get; set; }
        //public DbSet<ClientRegions> ClientRegions { get; set; }
        public DbSet<Regions> Regions { get; set; }
        public DbSet<ClientBusinessUnits> ClientBusinessUnits { get; set; }
        public DbSet<SineProCheckinCheckout> SignProCheckinCheckout { get; set; }
        public DbSet<PrequalificationSites> PrequalificationSites { get; set; }  // Rajesh on 07/24/2013
        public DbSet<ClientBusinessUnitSites> ClientBusinessUnitSites { get; set; }
        public DbSet<ClientBusinessUnitSiteRepresentatives> ClientBusinessUnitSiteRepresentatives { get; set; }

        public DbSet<Prequalification> Prequalification { get; set; }
        public DbSet<LatestPrequalification> LatestPrequalification { get; set; }
        public DbSet<FeeCapLog> FeeCapLog { get; set; }
        public DbSet<PrequalificationStatus> PrequalificationStatus { get; set; }
        public DbSet<PrequalificationUserInput> PrequalificationUserInput { get; set; }
        
        //>>DV:Siva
        public DbSet<Templates> Templates { get; set; }
        public DbSet<TemplateSections> TemplateSections { get; set; }

        public DbSet<TemplateSubSections> TemplateSubSections { get; set; }
        public DbSet<TemplateSectionsHeaderFooter> TemplateSectionsHeaderFooter { get; set; }
        public DbSet<Questions> Questions { get; set; }
        public DbSet<QuestionsDependants> QuestionsDependants { get; set; }   // Rajesh on 08/05/2013

        public DbSet<QuestionsBank> QuestionsBank { get; set; }

        public DbSet<QuestionControlType> QuestionControlType { get; set; }
        public DbSet<QuestionColumnDetails> QuestionColumnDetails { get; set; }
        public DbSet<TemplateSubSectionsPreDefinedFields> TemplateSubSectionsPreDefinedFields { get; set; }
        //<<DV:Siva

        //>>DV:Kiran on 24-05-2013
        public DbSet<SystemUsersOrganizationsRoles> SystemUsersOrganizationsRoles { get; set; }
        //<<DV:Kiran

        //>>DV: Kiran on 22-06-2013
        public DbSet<OrganizationsEmailSetupNotifications> OrganizationsEmailSetupNotifications { get; set; }
        public DbSet<DocumentType> DocumentType { get; set; }
        public DbSet<Document> Document { get; set; }
        public DbSet<DocumentLogs> DocumentLogs { get; set; }
        public DbSet<CSIWebResponseDetails> CSIWebResponseDetails { get; set; } // Kiran on 2/18/2014
        public DbSet<ArchiveOrganizationDetails> ArchiveOrganizationDetails { get; set; } // Kiran on 8/8/2014
        //<<DV:kiran

        //>>DV: Kiran on 08/01/2013
        public DbSet<ClientTemplatesForBU> ClientTemplatesForBU { get; set; }
        public DbSet<SystemUsersOrganizationsQuizzes> SystemUsersOrganizationsQuizzes { get; set; }
        //<<DV: Kiran

        public DbSet<VendorInviteDetails> VendorInviteDetails { get; set; }
        public DbSet<VendorInviteLocations> VendorInviteLocations { get; set; } 
        public DbSet<ClientTemplates> ClientTemplates { get; set; }

        public DbSet<BiddingInterests> BiddingInterests { get; set; } // Kiran 08/08/2013
        public DbSet<ClaycoBiddingInterests> ClaycoBiddingInterests { get; set; } // Kiran 08/08/2013
        public DbSet<PrequalificationReportingData> PrequalificationReportingData { get; set; } // Kiran 08/08/2013
        public DbSet<PrequalificationCompletedSections> PrequalificationCompletedSections { get; set; }  // Siva on 08/25/2013

        public DbSet<OnsiteOffsite> OnsiteOffsites { get; set; } // Kiran 08/08/2013
        public DbSet<OnsiteOffsiteVisitor> OnsiteOffsiteVisitor { get; set; } // Kiran 08/08/2013
        public DbSet<Visitors> Visitors { get; set; } // Kiran 08/08/2013

        public DbSet<PrequalificationStatusChangeLog> PrequalificationStatusChangeLog { get; set; } // Kiran on 11/22/2013

        public DbSet<BiddingInterestType> BiddingInterestType { get; set; }

        // Siva on 07/31/2013 >>>
        public DbSet<SystemViews> SystemViews { get; set; }
        public DbSet<SystemRolesPermissions> SystemRolesPermissions { get; set; }
        public DbSet<OrganizationsNotificationsSent> OrganizationsNotificationsSent { get; set; }
        // Ends <<<
        
        // Rajesh on 08/05/2013 >>>
        public DbSet<SystemHeaderMenus> SystemHeaderMenus { get; set; }
        public DbSet<SystemSideMenus> SystemSideMenus { get; set; }
        // Ends <<<

        //>>:DV Suma on 08-06-2013
        public DbSet<TemplatePrices> TemplatePrices { get; set; }
        public DbSet<TemplateDiscountPricesNotInUse> TemplateDiscountPricesNotInUse { get; set; }

        public DbSet<OrganizationsNotificationsEmailLogs> OrganizationsNotificationsEmailLogs { get; set; }
        //<<:DV Suma

        // Rajesh on 08/25/2013
        public DbSet<EmployeeQuizAnswers> EmployeeQuizAnswers { get; set; }
        public DbSet<EmployeeQuizUserInput> EmployeeQuizUserInput { get; set; }
        public DbSet<QuizzesCompletedSections> QuizzesCompletedSections { get; set; }
        public DbSet<ClientTemplateReportingData> ClientTemplateReportingData { get; set; } // on 10/02/2013
        public DbSet<ProficiencyCapabilities> ProficiencyCapabilities { get; set; } // Kiran on 10/02/2013
        // Ends <<<
        public DbSet<PrequalificationComments> PrequalificationComments { get; set; }//Rajesh on 1/7/2014
        public DbSet<ClientTemplateProfiles> ClientTemplateProfiles { get; set; }
        public DbSet<TemplateSectionsPermission> TemplateSectionsPermission { get; set; }//Rajesh on 1/7/2014
        public DbSet<TemplateSubSectionsNotifications> TemplateSubSectionsNotifications { get; set; }//Rajesh on 2/7/2014

        public DbSet<InviteTemplateConfiguration> InviteTemplateConfiguration { get; set; }
        public DbSet<TemplateSubSectionsPermissions> TemplateSubSectionsPermissions { get; set; }//Rajesh on 2/11/2014
        public DbSet<TemplateSubSectionsUserPermissions> TemplateSubSectionsUserPermissions { get; set; }//Rajesh on 2/11/2014
        public DbSet<PrequalificationNotifications> PrequalificationNotifications { get; set; }//Rajesh on 2/11/2014
        public DbSet<OrganizationsPrequalificationOpenPeriod> OrganizationsPrequalificationOpenPeriod { get; set; }//Rajesh on 3/17/2014

        public DbSet<PrequalificationEMRStatsValues> PrequalificationEMRStatsValues { get; set; }//Rajesh on 3/28/2014
        public DbSet<PrequalificationEMRStatsYears> PrequalificationEMRStatsYears { get; set; }//Rajesh on 3/28/2014

        public DbSet<ClientUserSignature> ClientUserSignature { get; set; }//Rajesh on 6/26/2014
        public DbSet<ClientTemplatesQuizDetails> ClientTemplatesQuizDetails { get; set; }//Kiran on 10/28/2014
        
        // Kiran on 11/4/2014
        //public DbSet<CheckListStatus> CheckListStatus { get; set; }
        //12/18/2014
        //public DbSet<SystemUsersOrganizationsEmpQuizChecklist> SystemUsersOrganizationsEmpQuizChecklist { get; set; }
        //public DbSet<SystemUsersOrganizationsEmpChecklist> SystemUsersOrganizationsEmpChecklist { get; set; }
        // Ends<<<

        // Kiran on 02/16/2015
        public DbSet<EmployeeQuizCategories> EmployeeQuizCategories { get; set; }
        public DbSet<SystemUsersOrganizationsCategories> SystemUsersOrganizationsCategories { get; set; }
        public DbSet<SystemUsersOrganizationsQuizzesCategories> SystemUsersOrganizationsQuizzesCategories { get; set; }

        public DbSet<VendorReviewQuestions> VendorReviewQuestions { get; set; }
        public DbSet<VendorReviewInputAnswers> VendorReviewInputAnswers { get; set; }
        public DbSet<VendorReviewInput> VendorReviewInput { get; set; }
        public DbSet<Invitationlogs> Invitationlogs { get; set; }
        public DbSet<VendorReviewQuestionsForSection3> VendorReviewQuestionsForSection3 { get; set; }
        public DbSet<VendorReviewInputSection3Ans> VendorReviewInputSection3Ans { get; set; }


        // Sumanth on 08/11/2015
        public DbSet<ClientVendorReferenceIds> ClientVendorReferenceIds { get; set; }

        public DbSet<VendorDetails> VendorDetails { get; set; } // Sandhya on 04/11/2015
        // Kiran on 04/28/2015
        public DbSet<ClientTemplatesBUGroupPrice> ClientTemplatesBUGroupPrice { get; set; } 
        public DbSet<ClientTemplatesForBUSites> ClientTemplatesForBUSites { get; set; }
        public DbSet<QuizPayments> QuizPayments { get; set; }
        public DbSet<QuizPaymentsItems> QuizPaymentsItems { get; set; }
        public DbSet<PrequalificationPayments> PrequalificationPayments { get; set; }
        public DbSet<PrequalificationTrainingAnnualFees> PrequalificationTrainingAnnualFees { get; set; } 
        // Ends<<<
        public DbSet<TempAssignBulkTrainingDetails> TempAssignBulkTrainingDetails { get; set; } // Kiran on 12/3/2015
        public DbSet<ClientVendorProperties> ClientVendorProperties { get; set; } //rb 06/02/2016
        public DbSet<TemplateLog> TemplateLog { get; set; }
        // Remove pluralization of table names
        public DbSet<SysUserOrgQuizHistory> SysUserOrgQuizHistory { get; set; }
        public DbSet<AgreementSubSectionConfiguration> AgreementSubSectionConfiguration { get; set; }
        public DbSet<AgreementSubSectionLog> AgreementSubSectionLog { get; set; }
        public DbSet<PrequalificationSubsection> PrequalificationSubsection { get; set; }
        public DbSet<PrequalificationClient> PrequalificationClient { get; set; }
        public DbSet<PrequalificationQuestion> PrequalificationQuestion { get; set; }

        public DbSet<ClientContractTypes> ClientContractTypes { get; set; }

        public DbSet<PrequalificationSectionsToDisplay> PrequalificationSectionsToDisplay { get; set; }
        public DbSet<ProductCategories> ProductCategories { get; set; }
        public DbSet<Language> Language { get; set; }
        public DbSet<SystemUsersBUSites> SystemUsersBUSites { get; set; }
        
        public DbSet<LocationStatusLog> LocationStatusLog { get; set; }
        public DbSet<EmailTemplateContent> EmailTemplateContent { get; set; }
        public DbSet<SubClients> SubClients { get; set; }
        public DbSet<Countries> Countries { get; set; }
        public DbSet<States> States { get; set; }
        public DbSet<Department> Department { get; set; }
        public DbSet<UserDepartments> UserDepartments { get; set; }  
        public DbSet<MappedQuestions> MappedQuestions { get; set; }
        public DbSet<CountryLanguages> CountryLanguages { get; set; }
        public DbSet<UserAcknowledgement> UserAcknowledgement { get; set; }
        public DbSet<QuizWaiverFormCompletionLog> QuizWaiverFormCompletionLog { get; set; }
        public DbSet<ClientNotifications> ClientNotifications { get; set; }
        public DbSet<PqBannerComments> PqComments { get; set; }
        public DbSet<PowerBiReports> PowerBiReports { get; set; }
        public DbSet<PowerBiPermissions> PowerBiPermissions { get; set; }
        public DbSet<PowerBiUserPermissions> PowerBiUserPermissions { get; set; }
        public DbSet<PowerBiReportUrls> PowerBiReportUrls { get; set; }
        public DbSet<PowerBiReportFilters> PowerBiReportFilters { get; set; }

        //public DbSet<UserAcknowledgement> UserAcknowledgement { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            base.OnModelCreating(modelBuilder);
        }
    }
}
