﻿/*
Page Created Date:  02/16/2015
Created By: Siva Bommisetty
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FVGen3.Domain.Abstract;
using FVGen3.Domain.Entities;

namespace FVGen3.Domain.Concrete
{
    public class EFEmployeeQuizCategoriesRepository : IEmployeeQuizCategoriesRepository,IDisposable
    {
        private EFDbContext context = new EFDbContext();

        public IEnumerable<EmployeeQuizCategories> GetCategories()
        {
            return context.EmployeeQuizCategories.ToList();
        }

        public List<EmployeeQuizCategories> GetTrainingCategories()
        {
            return context.EmployeeQuizCategories.Where(rec => rec.CategoryType == 2).ToList();
        }

        public List<EmployeeQuizCategories> GetEmployeeCategories()
        {
            return context.EmployeeQuizCategories.Where(rec => rec.CategoryType == 1).ToList();
        }

        public EmployeeQuizCategories GetCategoryByID(long empQuizCategoryId)
        {
            return context.EmployeeQuizCategories.Find(empQuizCategoryId);
        }

        public IQueryable<EmployeeQuizCategories> EmployeeQuizCategories
        {
            get { return context.EmployeeQuizCategories; }
        }
        public void Dispose()
        {
            context.Dispose();
        }
    }
}
