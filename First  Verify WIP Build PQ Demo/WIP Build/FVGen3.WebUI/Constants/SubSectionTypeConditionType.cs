﻿namespace FVGen3.WebUI.Constants
{
    public class SubSectionTypeConditionType
    {
        public const string AgreementQueries = "AgreementQueries";
        public const string AgreementAddendum = "AgreementAddendum";
    }
}