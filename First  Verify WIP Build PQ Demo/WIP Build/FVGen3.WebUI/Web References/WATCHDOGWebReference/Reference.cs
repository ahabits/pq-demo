﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// 
// This source code was auto-generated by Microsoft.VSDesigner, Version 4.0.30319.42000.
// 
#pragma warning disable 1591

namespace FVGen3.WebUI.WATCHDOGWebReference {
    using System;
    using System.Web.Services;
    using System.Diagnostics;
    using System.Web.Services.Protocols;
    using System.Xml.Serialization;
    using System.ComponentModel;
    
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.3056.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Web.Services.WebServiceBindingAttribute(Name="SearchServiceSoap", Namespace="http://www.attuswebsolutions.com/AttusTechSystemNameLookup/SearchService")]
    public partial class SearchService : System.Web.Services.Protocols.SoapHttpClientProtocol {
        
        private System.Threading.SendOrPostCallback ProcessNameListMessageStringOperationCompleted;
        
        private System.Threading.SendOrPostCallback ProcessNameListMessageOperationCompleted;
        
        private System.Threading.SendOrPostCallback ProcessNameListMultiOperationCompleted;
        
        private System.Threading.SendOrPostCallback ProcessNameListMessage_AnyOperationCompleted;
        
        private bool useDefaultCredentialsSetExplicitly;
        
        /// <remarks/>
        public SearchService() {
            this.Url = global::FVGen3.WebUI.Properties.Settings.Default.FVGen3_WebUI_WATCHDOGWebReference_SearchService;
            if ((this.IsLocalFileSystemWebService(this.Url) == true)) {
                this.UseDefaultCredentials = true;
                this.useDefaultCredentialsSetExplicitly = false;
            }
            else {
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }
        
        public new string Url {
            get {
                return base.Url;
            }
            set {
                if ((((this.IsLocalFileSystemWebService(base.Url) == true) 
                            && (this.useDefaultCredentialsSetExplicitly == false)) 
                            && (this.IsLocalFileSystemWebService(value) == false))) {
                    base.UseDefaultCredentials = false;
                }
                base.Url = value;
            }
        }
        
        public new bool UseDefaultCredentials {
            get {
                return base.UseDefaultCredentials;
            }
            set {
                base.UseDefaultCredentials = value;
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }
        
        /// <remarks/>
        public event ProcessNameListMessageStringCompletedEventHandler ProcessNameListMessageStringCompleted;
        
        /// <remarks/>
        public event ProcessNameListMessageCompletedEventHandler ProcessNameListMessageCompleted;
        
        /// <remarks/>
        public event ProcessNameListMultiCompletedEventHandler ProcessNameListMultiCompleted;
        
        /// <remarks/>
        public event ProcessNameListMessage_AnyCompletedEventHandler ProcessNameListMessage_AnyCompleted;
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("ProcessNameListMessageString", RequestNamespace="http://www.attuswebsolutions.com/AttusTechSystemNameLookup/SearchService", ResponseNamespace="http://www.attuswebsolutions.com/AttusTechSystemNameLookup/SearchService", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public string ProcessNameListMessageString(string xmlStream) {
            object[] results = this.Invoke("ProcessNameListMessageString", new object[] {
                        xmlStream});
            return ((string)(results[0]));
        }
        
        /// <remarks/>
        public void ProcessNameListMessageStringAsync(string xmlStream) {
            this.ProcessNameListMessageStringAsync(xmlStream, null);
        }
        
        /// <remarks/>
        public void ProcessNameListMessageStringAsync(string xmlStream, object userState) {
            if ((this.ProcessNameListMessageStringOperationCompleted == null)) {
                this.ProcessNameListMessageStringOperationCompleted = new System.Threading.SendOrPostCallback(this.OnProcessNameListMessageStringOperationCompleted);
            }
            this.InvokeAsync("ProcessNameListMessageString", new object[] {
                        xmlStream}, this.ProcessNameListMessageStringOperationCompleted, userState);
        }
        
        private void OnProcessNameListMessageStringOperationCompleted(object arg) {
            if ((this.ProcessNameListMessageStringCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.ProcessNameListMessageStringCompleted(this, new ProcessNameListMessageStringCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("ProcessNameListMessage", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Bare)]
        [return: System.Xml.Serialization.XmlElementAttribute(Namespace="http://www.attuswebsolutions.com/AttusTechSystemNameLookup/SearchService")]
        public System.Xml.XmlNode ProcessNameListMessage([System.Xml.Serialization.XmlElementAttribute(Namespace="http://www.attuswebsolutions.com/AttusTechSystemNameLookup/SearchService")] System.Xml.XmlNode attusRequest) {
            object[] results = this.Invoke("ProcessNameListMessage", new object[] {
                        attusRequest});
            return ((System.Xml.XmlNode)(results[0]));
        }
        
        /// <remarks/>
        public void ProcessNameListMessageAsync(System.Xml.XmlNode attusRequest) {
            this.ProcessNameListMessageAsync(attusRequest, null);
        }
        
        /// <remarks/>
        public void ProcessNameListMessageAsync(System.Xml.XmlNode attusRequest, object userState) {
            if ((this.ProcessNameListMessageOperationCompleted == null)) {
                this.ProcessNameListMessageOperationCompleted = new System.Threading.SendOrPostCallback(this.OnProcessNameListMessageOperationCompleted);
            }
            this.InvokeAsync("ProcessNameListMessage", new object[] {
                        attusRequest}, this.ProcessNameListMessageOperationCompleted, userState);
        }
        
        private void OnProcessNameListMessageOperationCompleted(object arg) {
            if ((this.ProcessNameListMessageCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.ProcessNameListMessageCompleted(this, new ProcessNameListMessageCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("ProcessNameListMulti", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Bare)]
        [return: System.Xml.Serialization.XmlElementAttribute(Namespace="http://www.attuswebsolutions.com/AttusTechSystemNameLookup/SearchService")]
        public System.Xml.XmlNode ProcessNameListMulti([System.Xml.Serialization.XmlElementAttribute(Namespace="http://www.attuswebsolutions.com/AttusTechSystemNameLookup/SearchService")] System.Xml.XmlNode NsaRequest) {
            object[] results = this.Invoke("ProcessNameListMulti", new object[] {
                        NsaRequest});
            return ((System.Xml.XmlNode)(results[0]));
        }
        
        /// <remarks/>
        public void ProcessNameListMultiAsync(System.Xml.XmlNode NsaRequest) {
            this.ProcessNameListMultiAsync(NsaRequest, null);
        }
        
        /// <remarks/>
        public void ProcessNameListMultiAsync(System.Xml.XmlNode NsaRequest, object userState) {
            if ((this.ProcessNameListMultiOperationCompleted == null)) {
                this.ProcessNameListMultiOperationCompleted = new System.Threading.SendOrPostCallback(this.OnProcessNameListMultiOperationCompleted);
            }
            this.InvokeAsync("ProcessNameListMulti", new object[] {
                        NsaRequest}, this.ProcessNameListMultiOperationCompleted, userState);
        }
        
        private void OnProcessNameListMultiOperationCompleted(object arg) {
            if ((this.ProcessNameListMultiCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.ProcessNameListMultiCompleted(this, new ProcessNameListMultiCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("ProcessNameListMessage_Any", RequestNamespace="http://www.attuswebsolutions.com/AttusTechSystemNameLookup/SearchService", ResponseNamespace="http://www.attuswebsolutions.com/AttusTechSystemNameLookup/SearchService", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Bare)]
        [return: System.Xml.Serialization.XmlTextAttribute()]
        [return: System.Xml.Serialization.XmlAnyElementAttribute()]
        public System.Xml.XmlNode[] ProcessNameListMessage_Any([System.Xml.Serialization.XmlTextAttribute()] [System.Xml.Serialization.XmlAnyElementAttribute()] System.Xml.XmlNode[] attusRequest) {
            object[] results = this.Invoke("ProcessNameListMessage_Any", new object[] {
                        attusRequest});
            return ((System.Xml.XmlNode[])(results[0]));
        }
        
        /// <remarks/>
        public void ProcessNameListMessage_AnyAsync(System.Xml.XmlNode[] attusRequest) {
            this.ProcessNameListMessage_AnyAsync(attusRequest, null);
        }
        
        /// <remarks/>
        public void ProcessNameListMessage_AnyAsync(System.Xml.XmlNode[] attusRequest, object userState) {
            if ((this.ProcessNameListMessage_AnyOperationCompleted == null)) {
                this.ProcessNameListMessage_AnyOperationCompleted = new System.Threading.SendOrPostCallback(this.OnProcessNameListMessage_AnyOperationCompleted);
            }
            this.InvokeAsync("ProcessNameListMessage_Any", new object[] {
                        attusRequest}, this.ProcessNameListMessage_AnyOperationCompleted, userState);
        }
        
        private void OnProcessNameListMessage_AnyOperationCompleted(object arg) {
            if ((this.ProcessNameListMessage_AnyCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.ProcessNameListMessage_AnyCompleted(this, new ProcessNameListMessage_AnyCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        public new void CancelAsync(object userState) {
            base.CancelAsync(userState);
        }
        
        private bool IsLocalFileSystemWebService(string url) {
            if (((url == null) 
                        || (url == string.Empty))) {
                return false;
            }
            System.Uri wsUri = new System.Uri(url);
            if (((wsUri.Port >= 1024) 
                        && (string.Compare(wsUri.Host, "localHost", System.StringComparison.OrdinalIgnoreCase) == 0))) {
                return true;
            }
            return false;
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.3056.0")]
    public delegate void ProcessNameListMessageStringCompletedEventHandler(object sender, ProcessNameListMessageStringCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.3056.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class ProcessNameListMessageStringCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal ProcessNameListMessageStringCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public string Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((string)(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.3056.0")]
    public delegate void ProcessNameListMessageCompletedEventHandler(object sender, ProcessNameListMessageCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.3056.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class ProcessNameListMessageCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal ProcessNameListMessageCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public System.Xml.XmlNode Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((System.Xml.XmlNode)(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.3056.0")]
    public delegate void ProcessNameListMultiCompletedEventHandler(object sender, ProcessNameListMultiCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.3056.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class ProcessNameListMultiCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal ProcessNameListMultiCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public System.Xml.XmlNode Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((System.Xml.XmlNode)(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.3056.0")]
    public delegate void ProcessNameListMessage_AnyCompletedEventHandler(object sender, ProcessNameListMessage_AnyCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.3056.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class ProcessNameListMessage_AnyCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal ProcessNameListMessage_AnyCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public System.Xml.XmlNode[] Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((System.Xml.XmlNode[])(this.results[0]));
            }
        }
    }
}

#pragma warning restore 1591