﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FVGen3.WebUI.Utilities
{
    public static class NullableConvertions
    {
        public static int? ToNullableInt(this string s)
        {
            int i;
            if (int.TryParse(s, out i)) return i;
            return null;
        }
        public static long? ToNullableLong(this string s)
        {
            long i;
            if (long.TryParse(s, out i)) return i;
            return null;
        }
        public static Guid? ToNullableGuid(this string s)
        {
            Guid i;
            if (Guid.TryParse(s, out i)) return i;
            return null;
        }
        public static IEnumerable<TSource> DistinctBy<TSource, TKey>
            (this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
        {
            var known = new HashSet<TKey>();
            return source.Where(element => known.Add(keySelector(element)));
        }
    }
}