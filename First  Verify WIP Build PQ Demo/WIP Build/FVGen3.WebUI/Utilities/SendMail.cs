﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;
using System.Text;
using System.Configuration;

namespace FVGen3.WebUI.Utilities
{
    public class SendMail
    {

        public static string sendMail(String toAddress, string body, string subject)
        {
            return sendMail(toAddress, body, subject, null);
        }
        public static string sendMail(String toAddress, string body, string subject, string bcc)
        {
            return sendMail(toAddress, body, subject, bcc, null);
        }
        public static string sendMail(String toAddress, string body, string subject, string bcc, string cc)
        {
            return sendMail(toAddress, body, subject, bcc, cc, null);
        }


        public static string sendMail(String toAddress, string body, string subject, string bcc, string cc, List<Attachment> attachments)
        {
            String fromAddress = ConfigurationSettings.AppSettings["fromMail"];
            String fromPassword = ConfigurationSettings.AppSettings["fromPassword"];
            String smtpHost = ConfigurationSettings.AppSettings["smtpHost"];
            String smtpPort = ConfigurationSettings.AppSettings["smtpPort"];
            bool useSSL = ConfigurationSettings.AppSettings["useSSL"] == "True";
            bool isTest = ConfigurationSettings.AppSettings["IsTest"] == "true";
            //String subject = ConfigurationSettings.AppSettings["subject"];
            //string url = "http://localhost:2439/Vendor/registrationVerificationUrl?id=" + uid;
            //String body = "<html><body><p>Body with Guid </p> <a href=" + url + "> Verify Email </a>  <p>";
            //body = body + uid + "</p> </body></html>";

            if (isTest)
            {
                toAddress = ConfigurationSettings.AppSettings["testMail"];
                bcc = "";
                cc = "";
            }
            SmtpClient client = new SmtpClient();
            client.Port = Convert.ToInt32(smtpPort);
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.Host = smtpHost;
            // Kiran on 10/14/2015
            if (useSSL)
            {
                client.EnableSsl = true;
            }
            // Ends<<<
            client.Credentials = new System.Net.NetworkCredential(fromAddress, fromPassword);

            //MailMessage mail = new MailMessage(fromAddress, toAddress);
            using (var mail = new MailMessage(fromAddress, toAddress))
            {
                mail.Subject = subject;
                mail.Body = body;
                mail.BodyEncoding = Encoding.UTF8;
                mail.IsBodyHtml = true;

                if (attachments != null && attachments.Any())
                {
                    foreach (var attach in attachments)
                    {
                        mail.Attachments.Add(attach);
                    }
                }

                if (!string.IsNullOrEmpty(bcc))
                    mail.Bcc.Add(bcc);
                if (!string.IsNullOrEmpty(cc))
                    mail.CC.Add(cc);
                //mail.CC.Add("jillianr@firstverify.com");// Mani on 8/13/2015 for fv-183
                try
                {
                    //if (!isTest)
                    client.Send(mail);
                }
                catch (SmtpException exception)
                {
                    // Console.WriteLine("Mail Sending Failed");
                    return "Mail Sending Failed" + exception.Message;
                }
            }
            return "Success";
        }


    }
}