﻿using System.Collections.Generic;
using PayPal.Api;
using PayPal;
using System.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PayPal.Sample.Utilities;
using System;
using FVGen3.Domain.Entities;

namespace PayPal.Sample
{
    public static class Common
    {
        public static string FormatJsonString(string json)
        {
            if (string.IsNullOrEmpty(json))
            {
                return string.Empty;
            }

            if (json.StartsWith("["))
            {
                // Hack to get around issue with the older Newtonsoft library
                // not handling a JSON array that contains no outer element.
                json = "{\"list\":" + json + "}";
                var formattedText = JObject.Parse(json).ToString(Formatting.Indented);
                formattedText = formattedText.Substring(13, formattedText.Length - 14).Replace("\n  ", "\n");
                return formattedText;
            }
            return JObject.Parse(json).ToString(Formatting.Indented);
        }

        /// <summary>
        /// Gets a random invoice number to be used with a sample request that requires an invoice number.
        /// </summary>
        /// <returns>A random invoice number in the range of 0 to 999999</returns>
        public static string GetRandomInvoiceNumber()
        {
            return new Random().Next(999999).ToString();
        }

        public static void ExecutePayment(string payerid, string paymentid)
        {

            var apiContext = PayPal.Sample.Configuration.GetAPIContext();

            //var paymentId = Session[guid] as string;
            var paymentExecution = new PaymentExecution() { payer_id = payerid };
            var payment = new Payment() { id = paymentid };


            var executedPayment = payment.Execute(apiContext, paymentExecution);

        }
        
    }
}