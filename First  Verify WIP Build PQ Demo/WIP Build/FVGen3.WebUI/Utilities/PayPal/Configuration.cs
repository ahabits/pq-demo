﻿using System.Collections.Generic;
using PayPal.Api;
using System.Configuration;
using System;

namespace PayPal.Sample
{ 
    public static class Configuration
    {
        public readonly static string ClientId;
        public readonly static string ClientSecret;

        // Static constructor for setting the readonly static members.
        static Configuration()
        {
            var config = GetConfig();
            ClientId = config["clientId"];
            ClientSecret = config["clientSecret"];
        }

        // Create the configuration map that contains mode and other optional configuration details.
        public static Dictionary<string, string> GetConfig()
        {
            var data = new Dictionary<string, string>();
            bool useSandbox = Convert.ToBoolean(ConfigurationManager.AppSettings["UseSandbox"]);
            data.Add("mode", useSandbox?"sandbox": "live");
            data.Add("connectionTimeout", "360000");
            data.Add("requestRetries", "1");
            data.Add("clientId", useSandbox? "AezGasAQkdyrBvbTm7dcYH6eB6JzRwSCqQ1wXJFwE9mPWutI5oaE3viEJxJgu2Xuq9lLY0ZxK03fmVlm":"AUIg-Z1puCywVfEu94QTOPBeregS4CkNJ_GwYUXzg1qPA_TRaPicpUqzv5gjdHbvFjZfx_WYiXl8rflh" );
            data.Add("clientSecret", useSandbox ? "EPyZqtNtTnsEPAdxKRlicAjtb7BMCbxWX-M5AvWCOqcv30_L0X0ltZO1qo4t3eb7Zb1fpiwvNiJznPmv":"ELnx5G-fCNPC_XX03nfacFcCRsLy-h7n57LLFDItL7TRDlkM20s8wQwngBu_boYxYfFPhWtzxxglt1NW");
            
            return data;
        }

        // Create accessToken
        private static string GetAccessToken()
        {
            // ###AccessToken
            // Retrieve the access token from
            // OAuthTokenCredential by passing in
            // ClientID and ClientSecret
            // It is not mandatory to generate Access Token on a per call basis.
            // Typically the access token can be generated once and
            // reused within the expiry window                
            string accessToken = new OAuthTokenCredential(ClientId, ClientSecret, GetConfig()).GetAccessToken();
            return accessToken;
        }

        // Returns APIContext object
        public static APIContext GetAPIContext(string accessToken = "")
        {
            // ### Api Context
            // Pass in a `APIContext` object to authenticate 
            // the call and to send a unique request id 
            // (that ensures idempotency). The SDK generates
            // a request id if you do not pass one explicitly. 
            var apiContext = new APIContext(string.IsNullOrEmpty(accessToken) ? GetAccessToken() : accessToken);
            apiContext.Config = GetConfig();

            // Use this variant if you want to pass in a request id  
            // that is meaningful in your application, ideally 
            // a order id.
            // String requestId = Long.toString(System.nanoTime();
            // APIContext apiContext = new APIContext(GetAccessToken(), requestId ));

            return apiContext;
        }

    }
}
 