﻿/*
Page Created Date:  08/08/2014
Created By: Kiran Talluri
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Xml;
using FVGen3.Domain.Abstract;
using FVGen3.Domain.Entities;
using FVGen3.Domain.Concrete;
using System.IO;
using System.Web.Mvc;

namespace FVGen3.WebUI.Utilities
{
    public class CSIWebServiceInitiateRequest
    {
        public static string CSIWebService(long vendorOrganizationId, string orgName, string orgPrincipalOfficerName, string directoryPath)
        {
            EFDbContext entityDB = new EFDbContext();
            WATCHDOGWebReference.SearchService myService = new WATCHDOGWebReference.SearchService();
            myService.Url = ConfigurationSettings.AppSettings["ATTUSWebServiceURL"].ToString();
            myService.Timeout = 30000;

            XmlDocument myXmlRequest = new XmlDocument();
            XmlNode docNode = myXmlRequest.CreateXmlDeclaration("1.0", "UTF-8", null);
            myXmlRequest.AppendChild(docNode);

            XmlNode ATTUSWATCHDOGREQUEST = myXmlRequest.CreateElement("", "ATTUSWATCHDOGREQUEST", "");
            myXmlRequest.AppendChild(ATTUSWATCHDOGREQUEST);

            XmlNode BILLINGCODE = myXmlRequest.CreateElement("", "BILLINGCODE", "");

            XmlAttribute partnerIdAttribute = myXmlRequest.CreateAttribute("partnerid");
            partnerIdAttribute.Value = "91b53925-dddf-44dd-a84f-baf6ce3c6aee";
            BILLINGCODE.Attributes.Append(partnerIdAttribute);

            XmlAttribute billingIdAttribute = myXmlRequest.CreateAttribute("billingid");
            billingIdAttribute.Value = "91b53925-dddf-44dd-a84f-baf6ce3c6aee";
            BILLINGCODE.Attributes.Append(billingIdAttribute);

            ATTUSWATCHDOGREQUEST.AppendChild(BILLINGCODE);

            XmlNode LISTS = myXmlRequest.CreateElement("", "LISTS", "");

            XmlNode LIST = myXmlRequest.CreateElement("", "LIST", "");

            XmlAttribute codeAttribute = myXmlRequest.CreateAttribute("code");
            codeAttribute.Value = "OFAC";
            LIST.Attributes.Append(codeAttribute);

            LISTS.AppendChild(LIST);
            ATTUSWATCHDOGREQUEST.AppendChild(LISTS);

            XmlNode QUERY = myXmlRequest.CreateElement("", "QUERY", "");

            XmlNode ITEM = myXmlRequest.CreateElement("", "ITEM", "");

            XmlAttribute allentitiesAttribute = myXmlRequest.CreateAttribute("allentities");
            allentitiesAttribute.Value = orgPrincipalOfficerName;
            ITEM.Attributes.Append(allentitiesAttribute);

            XmlAttribute corpnameAttribute = myXmlRequest.CreateAttribute("corpname");
            corpnameAttribute.Value = orgName;
            ITEM.Attributes.Append(corpnameAttribute);

            QUERY.AppendChild(ITEM);
            ATTUSWATCHDOGREQUEST.AppendChild(QUERY);

            XmlNode VERSION = myXmlRequest.CreateElement("", "VERSION", "");
            VERSION.InnerText = "2.0";

            ATTUSWATCHDOGREQUEST.AppendChild(VERSION);

            XmlNode myXmlResponse = myService.ProcessNameListMessage(myXmlRequest);

            // Kiran on 3/5/2014
            XmlDocument responseDoc = new XmlDocument();
            responseDoc.LoadXml(myXmlResponse.OuterXml.ToString());

            string fileName = vendorOrganizationId.ToString();


            Directory.CreateDirectory(Path.Combine(directoryPath));
            var path = Path.Combine(directoryPath, fileName + ".xml");
            // Kiran on 7/31/2014
            if (path != null)
            {
                System.IO.File.Delete(path);
            }
            responseDoc.Save(path);
            // Ends<<<

            XmlNodeList parentNodes = myXmlResponse.ChildNodes;

            // Kiran on 7/31/2014
            var existingCSIRecordsForCurrentPrequalification = entityDB.CSIWebResponseDetails.Where(rec => rec.VendorOrgId == vendorOrganizationId);
            if (existingCSIRecordsForCurrentPrequalification != null)
            {
                foreach (var existingCSIRecord in existingCSIRecordsForCurrentPrequalification.ToList())
                {
                    entityDB.CSIWebResponseDetails.Remove(existingCSIRecord);
                    entityDB.SaveChanges();
                }
            }
            // Ends<<<

            foreach (XmlNode parentNode in parentNodes)
            {
                if (parentNode.Name == "SEARCHCRITERIA")
                {
                    XmlNodeList searchCriteriaNodes = parentNode.ChildNodes;
                    foreach (XmlNode searchCriteriaNode in searchCriteriaNodes)
                    {
                        if (searchCriteriaNode.Name == "ENTITIES")
                        {
                            XmlNodeList entitiesNodes = searchCriteriaNode.ChildNodes;
                            if (entitiesNodes != null)
                            {
                                foreach (XmlNode entitiesNode in entitiesNodes)
                                {
                                    if (entitiesNode.Name == "ENTITY")
                                    {
                                        XmlNodeList entityElementNodes = entitiesNode.ChildNodes;
                                        var entityNodeAttributes = entitiesNode.Attributes;

                                        foreach (XmlNode node in entityElementNodes)
                                        {
                                            CSIWebResponseDetails csiRecord = new CSIWebResponseDetails();
                                            csiRecord.VendorOrgId = vendorOrganizationId;
                                            csiRecord.RequestInvokedFrom = 0;
                                            csiRecord.EntityID = Convert.ToInt64(entityNodeAttributes[0].Value);
                                            csiRecord.EntityNumber = Convert.ToInt64(entityNodeAttributes[1].Value);
                                            if (node.ChildNodes.Count != 0)
                                            {
                                                XmlNodeList childNodesOfEntityInnerNodes = node.ChildNodes;
                                                if (childNodesOfEntityInnerNodes.Count > 1)
                                                {
                                                    foreach (XmlNode childNode in childNodesOfEntityInnerNodes)
                                                    {
                                                        if (childNode.Name == "ALIAS")
                                                        {
                                                            csiRecord.ElementNodeName = childNode.Name;
                                                            csiRecord.ElementNodeValue = childNode.Attributes[0].Value;
                                                        }
                                                        else
                                                        {
                                                            csiRecord.ElementNodeName = childNode.Name;
                                                            var addressAttributes = childNode.Attributes;
                                                            csiRecord.ElementNodeValue = addressAttributes[0].Value + "," + addressAttributes[1].Value + "," + addressAttributes[2].Value + "," + addressAttributes[4].Value + "," + addressAttributes[5].Value;
                                                        }
                                                        entityDB.CSIWebResponseDetails.Add(csiRecord);
                                                        entityDB.SaveChanges();
                                                    }
                                                }
                                                else
                                                {
                                                    foreach (XmlNode childNode in childNodesOfEntityInnerNodes)
                                                    {
                                                        if (childNode.Name == "ALIAS")
                                                        {
                                                            csiRecord.ElementNodeName = childNode.Name;
                                                            csiRecord.ElementNodeValue = childNode.Attributes[0].Value;
                                                        }
                                                        else
                                                        {
                                                            csiRecord.ElementNodeName = childNode.ParentNode.Name;
                                                            csiRecord.ElementNodeValue = childNode.Value;
                                                        }
                                                        entityDB.CSIWebResponseDetails.Add(csiRecord);
                                                        entityDB.SaveChanges();
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                csiRecord.ElementNodeName = node.Name;
                                                csiRecord.ElementNodeValue = node.Value;
                                                entityDB.CSIWebResponseDetails.Add(csiRecord);
                                                entityDB.SaveChanges();
                                            }

                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return "Success";
        }
    }
}