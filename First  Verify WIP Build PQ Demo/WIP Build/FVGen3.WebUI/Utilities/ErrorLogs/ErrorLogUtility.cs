﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FVGen3.Domain.LocalModels;
using FVGen3.WebUI.Controllers;
using FVGen3.Domain.Entities;
using FVGen3.DataLayer.DTO;
namespace FVGen3.WebUI.Utilities.ErrorLogs
{
    public class ErrorLogUtility
    {
        public readonly HttpRequestBase RequestBase;

        public ErrorLogUtility(HttpRequestBase requestBase)
        {
            if (requestBase == null)
            {
                throw new NullReferenceException("requestBase is null at ErrorLogUtility.");
            }
            RequestBase = requestBase;
        }

        public ErrorLogUtility(HttpRequest request)
            : this(new HttpRequestWrapper(request))
        {
        }

        public ErrorModel GetErrorModel(Exception exception)
        {
            var errorModel = new ErrorModel();
            SetRouteDetail(ref errorModel);
            SetRequestDetail(ref errorModel);
            SetExceptionDetail(ref errorModel, exception);
            return errorModel;
        }

        public ErrorModel GetErrorModel(Exception exception, object session)
        {
            var errorModel = GetErrorModel(exception);
            SetSessionDetail(ref errorModel, session);
            return errorModel;
        }

        private void SetRequestDetail(ref ErrorModel errorModel)
        {
            errorModel.RequestHeader = RequestBase.ToRaw();
            errorModel.RequestData = RequestBase.ParamsToString();
        }

        private void SetRouteDetail(ref ErrorModel errorModel)
        {
            Func<string, string> routeDataValue = delegate (string indexName)
            {
                bool hasValue = RequestBase.RequestContext.RouteData.Values[indexName] != null;
                return hasValue ? RequestBase.RequestContext.RouteData.Values[indexName].ToString() : "";
            };
            errorModel.ControllerName = routeDataValue("controller");
            errorModel.ActionName = routeDataValue("action");
        }

        private void SetExceptionDetail(ref ErrorModel errorModel, Exception exception)
        {
            errorModel.Exception = exception;
            errorModel.CreatedDateTime = DateTime.Now;
        }

        private void SetSessionDetail(ref ErrorModel errorModel, object session)
        {
            errorModel.HasSession = session != null;
            errorModel.Session = (session != null) ? session.ToString() : "";
        }
    }
}
