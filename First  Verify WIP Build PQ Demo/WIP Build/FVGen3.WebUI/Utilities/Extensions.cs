﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text.RegularExpressions;
using Resources;

namespace FVGen3.WebUI.Utilities
{
    static class Extensions
    {
        public static IList<T> Clone<T>(this IList<T> listToClone) where T : ICloneable
        {
            return listToClone.Select(item => (T)item.Clone()).ToList();
        }

        public static bool IsEmail(this string s)
        {
            try
            {
                Regex regex = new Regex(LocalConstants.EMAIL_FORMAT);
                Match match = regex.Match(s);
                return match.Success;
            }
            catch (FormatException)
            {
                return false;
            }
        }
    }
}