﻿var FVHelper = {
    
    PopulateStateDropdown: function (ddlSelector,vendorId, emptyResultString, includeCanada) {        
        $.ajax({
            type: 'Get',
            url: '../api/DDHelper/StateCodes',
            data: { vendorID: vendorId, emptyResultString: emptyResultString, includeCanada: includeCanada },
            success: function (data) {
                ddlSelector.empty();
                for (var i = 0; i < data.length; i++) {
                    var datasite = data[i].Key;
                    newOption = $('<option>' + datasite + '</option>');
                    ddlSelector.append(newOption);
                }
            },
            error: function () {
                alert("Unable to process state codes please try again ");
            }
        });
    }
}