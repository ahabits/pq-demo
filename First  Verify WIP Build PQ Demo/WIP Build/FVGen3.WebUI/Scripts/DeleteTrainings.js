﻿

    function DeleteTrainings(SysUserOrgQuizId) {
        

        if (SysUserOrgQuizId != -1) {
            var c = confirm("Are you sure you want to delete?");
            if (c == true) {
                $('.spinnerOverlay').show();
                $('.importSubmitSpinner').show();
                $.ajax({
                    type: 'GET',
                    url: '../AdminVendors/DeleteTraining',
                    data:{ 'SysUserOrgQuizId': + SysUserOrgQuizId,
                    },

                    success: function (data) {
                        if (data.indexOf("Session expired") >= 0) {
                            window.location = "../SystemUsers/Login";
                        }
                        else if (data == "EmployeeDeleted") {
                            $('.spinnerOverlay').hide();
                            $('.importSubmitSpinner').hide();
                            location.reload(true);
                        }

                    },
                    error: function () {
                        console.trace();
                        alert("Fail ");
                    }
                });
            }
            else {
                return false;
            }
        }
    }

