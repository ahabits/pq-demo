﻿function showLogs(templateName,templateId) {
    PopupModel.setData("Loading..");
    PopupModel.setHeader("");
    PopupModel.show();
    Templates.getTemplateLog(templateId).then(function (response) {
        

        var template = '<table><tr><th width="50%" align="left">Changed By</th><th width="30%" align="left">Changed Date</th><th width="20%" align="left">Changed To</th></tr>';
        $.each(response, function (index, data) {
            template += '<tr><td>' + data.FullName + '</td>';
            template += '<td>' + moment(data.ChangedDate).format('MM-DD-YYYY') + '</td>';
            template += '<td>' + (Boolean(data.LockedByDefaultVal)?'Locked':'Unlocked') + '</td></tr>';
        });
        if (response.length == 0)
            template += '<tr><td colspan=3 class="commentsItalic"><center>No records found</center></td></tr>'            
        template += '</template>'
        PopupModel.setHeader(templateName);
        PopupModel.setData(template);
    });
}

var Templates = {
    getTemplateLog: function (templateId) {
        return $.get("/TemplateTool/GetTemplateLog?templateId=" + templateId)
        .then(function (response) {
            return response;
        });
    }
}