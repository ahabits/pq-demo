﻿var _gaq = _gaq || [];

_gaq.push(['_setAccount', 'UA-68881761-1']);
_gaq.push(['_trackPageview']);
(function () {
    var ga = document.createElement('script');
    ga.type = 'text/javascript';
    ga.async = true;
    ga.src = ('https:' === document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(ga, s);
})();
if (window.addEventListener) {
    window.addEventListener('error',
        function (err) {
            if (UserInfo.HasInfo === false) {
                Post("../SystemUsers/GetUserInfo",
                    {},
                    function (data) {
                        pushErrAnalytics(err);
                        UserInfo = data;
                    });
            } else {
                pushErrAnalytics(err);
            }
        });
} else {
    window.attachEvent('error',
        function (err) {
            if (UserInfo.HasInfo === false) {
                Post("../SystemUsers/GetUserInfo",
                    {},
                    function (data) {
                        pushErrAnalytics(err);
                        UserInfo = data;
                    });
            } else {
                pushErrAnalytics(err);
            }
        });
}
$(document).ajaxError(function (event, jqxhr, settings, thrownError) {
    if (UserInfo.HasInfo === false) {
        Post("../SystemUsers/GetUserInfo",
            {},
            function (data) {
                pushAjaxAnalytics(settings.url, jqxhr.responseText);
                UserInfo = data;
            });
    } else {
        pushAjaxAnalytics(settings.ur, thrownError + ' -> ' + settings.data);
    }
});
function pushAjaxAnalytics(url, msg) {
    Post("../SystemUsers/isTest", {}, function (data) {
        //alert(data);
        if (data) {
            _gaq.push([
        '_trackEvent',
        url,
        'Ajax Error' + ' -> ' + UserInfo.UserId,
        msg,
        0,
        true]);
        }
    });

}
function pushErrAnalytics(err) {
    //alert('error');
    Post("../SystemUsers/isTest", {}, function (data) {

        if (data) {
            var lineAndColumnInfo = err.colno ? ' line:' + err.lineno + ', column:' + err.colno : ' line:' + err.lineno;
            _gaq.push([
                '_trackEvent',
                window.location.pathname,
                err.error.stack + ' -> ' + UserInfo.UserId,
                err.filename + lineAndColumnInfo,
                0,
                true
            ]);
        }
    });
}