﻿var Vendor= {
    GetVendor: function (vendorName, city, state, biddings, proficiency, type, callback) {
        //console.log(JSON.stringify({
        //    vendorName: vendorName,
        //    city: city,
        //    state: state,
        //    type: type,
        //    biddings: biddings,
        //    proficiency: proficiency
        //}));
        Post("../Client/GetVendorsForInvite",
        {
            vendorName: vendorName,
            city: city,
            state: state,
            type: type,
            biddings: biddings,
            proficiency: proficiency
        },function(result) {
            callback(result);
        });
    },
    GetAllVendors: function (callback) {
        Post("../Vendor/GetAllVendors",
        {
           
        }, function (result) {
            callback(result);
        });
    }
}