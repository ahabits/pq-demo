﻿var CustomReports = {
    populateClientContractType: function () {


        var clientId = $("#Name").val();
        Post("../AdminVendors/GetClientContractTypes?ClientId=" + clientId,
           {},
           function (data) {
               var controls = $(Controls.Dropdown(data));
               controls.attr("id", "contractType");
               controls.addClass("contractType");
               //controls.attr("style","width:98%");
               $("#contractTypeRow").html(controls);
           });

    }
}