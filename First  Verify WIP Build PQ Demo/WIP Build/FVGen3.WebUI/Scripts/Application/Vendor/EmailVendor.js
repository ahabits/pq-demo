﻿var EmailVendor =
{
    GetPQStatuses: function () {
        Prequalification.GetPQStatues(function (result) {
            //$("#Statuses").html(Controls.MultiCheckBoxes(result));
            $("#statuses").html($(Controls.MultiCheckBoxes(result, "")));
        });
    },

    GetVendorOrganizations: function () {
        if ($("#currentOrgId").val() == null) {
            var controls = $(Controls.Dropdown([{ Key: "-1", Value: "Select" }]));
            controls.attr("id", "vendors");
            $("#vendorsRow").html(controls);
            return;
        }
        Post("../Vendor/GetVendors",
           { clientId: $("#currentOrgId").val() },
           function (data) {
               var controls = $(Controls.Dropdown(data));
               controls.attr("id", "vendors");
               $("#vendorsRow").html(controls);
           });
    },
    GetClientOrganizations: function () {
        Post("../Client/GetClients",
            {},
            function (data) {
                var controls = $(Controls.Dropdown(data));
                controls.attr("id", "currentOrgId");
                controls.attr("onchange", "EmailVendor.GetVendorOrganizations()");
                $("#clientsRow").html(controls);
                EmailVendor.GetVendorOrganizations();
            });
    },

    PopulateUserEmailDropdown: function (ddlSelector) {
        
        var clientId = $("#currentOrgId").val();
        var PqId = 0;
        $("#searchVendorResults_block_id input:checked").each(function () {
            //alert($(this).data("pqid"));
            if ($(this).attr("class") === 'mailes') {
              
                PqId=$(this).data("pqid");
            }
        });
       
        $.ajax({
            type: 'Get',
            url: '../api/DDHelper/GetUserEmails',
            data: { orgID: clientId, PqId: PqId },
            success: function (data) {
                ddlSelector.attr("style", "height:200px;width:300px;");
                ddlSelector.empty();

                for (var i = 0; i < data.length; i++) {
                    var datasite = data[i].Key;
                    newOption = $('<option value=' + data[i].Value + '>' + datasite + '</option>');
                    ddlSelector.append(newOption);
                }
            },
            error: function () {
                alert("Unable to fetch email(s).Please try again. ");
            }
        });
    },
    GetPQVendorDetailsByVendorId: function (pqIds, selector) {
        Post("../AdminVendors/GetPQVendorDetailsList",
            { PqIds: pqIds, clientId: $("#currentOrgId").val() },
            function (data) {
                $(selector).val(data);
            });
    },
    SendMail: function (vendorId, pqId) {
        $(".spinnerOverlay").show();
        $('.importSubmitSpinner').show();
        Post("../Vendor/EmailVendor",
        {
            ToAddress: PopupModel.getBody().find("#toEmails").val(),
            CCAddress: PopupModel.getBody().find("#ccEmails").val(),
            BCCAddress: PopupModel.getBody().find("#bccEmails").val(),
            Body: PopupModel.getBody().find("#templateDiv").find('.nicEdit-main').html(),
            VendorIds: vendorId,
            ClientId: $("#currentOrgId").val(),
            PQIds: pqId,
            BCCSendEmailToVendorContacts: PopupModel.getBody().find("#BCCSendEmailToVendorContacts").is(':checked')
    },
            function (data) {
                $('.spinnerOverlay').hide();
                $('.importSubmitSpinner').hide();
                if (data === false) {
                    alert("Unable to process the request.Please contact system admin.");
                    return;
                }
                alert("Email has been sent successfully");
                PopupModel.closeModel();
            });
    },
    ProcessEmailSend: function () {
        
        var PqIds = [];
        var venId = [];
        $("#searchVendorResults_block_id input:checked").each(function () {
            //alert($(this).data("pqid"));
            if ($(this).attr("class") === 'mailes') {
                PqIds.push($(this).data("pqid"));
                venId.push($(this).data("vendorid"));
            }
        });
        if (PqIds.length === 0) {
            alert("Please select the vendors");
            return;
        }
        EmailVendor.EmailVendor(venId.join(",") + '', PqIds.join(",") + '', "", 1);
    },
    EmailVendor: function (vendorId, pqId, name, step) {
        
        if (step === 2) {


            $("#nextButton").hide();
            PopupModel.getBody().find("#step1").hide();
            PopupModel.getBody().find("#step2").show();

           

            sendCC = PopupModel.getBody().find("#searchUsersSendCC").val();
            sendBCC = PopupModel.getBody().find("#searchUsersSendBCC").val();

            Post("../Organization/GetOrganizationSuperUser",
                { orgId: $("#currentOrgId").val() },
                function (data) {
                    
                    if (data != '') {
                        if (sendCC == null)
                            sendCC = data.Email;
                        else
                            sendCC += "," + data.Email;
                        PopupModel.getBody().find("#ccEmails").val(sendCC);
                    }
                });

            Post("../AdminVendors/GetPQVendorDetailsList",
           { PqIds: pqId },
           function (data) {
               sendVendorContactsBCC = data;
               if (sendVendorContactsBCC != '') {
                   if (sendBCC == null)
                       sendBCC = sendVendorContactsBCC;
                   else
                       sendBCC += "," + sendVendorContactsBCC;
               }
               PopupModel.getBody().find("#bccEmails").val(sendBCC);
              
           });

            //PopupModel.getBody().find("#ccEmails").val(sendCC);
            //PopupModel.getBody().find("#bccEmails").val(sendBCC);
            //PopupModel.getBody().find("#step1").hide();
            //PopupModel.getBody().find("#step2").show();
            //$("#nextButton").hide();
            PopupModel
                .appendHeaderHtml("<span class=\"PageFooter_Button headRight\" id=\"saveButton\" onclick=\"EmailVendor.SendMail('" +
                    vendorId +
                    "','" + pqId + "')\">Send</span>",
                    1);
        } else {
            PopupModel.reset();
            var popupContent = $("#extraInfo").html();
            PopupModel.setData(popupContent);
            PopupModel.setHeader(name);
            PopupModel.show();
            //EmailVendor.GetPQVendorDetailsByVendorId(pqId, PopupModel.getBody().find("#toEmails"));
            PopupModel.getBody().find("#toEmails").val("info@firstverify.com");
            EmailVendor.PopulateUserEmailDropdown(PopupModel.getBody().find("#searchUsersSendCC"));
            EmailVendor.PopulateUserEmailDropdown(PopupModel.getBody().find("#searchUsersSendBCC"));
            PopupModel.getBody().find("#step1").show();
            PopupModel.getBody().find("#step2").hide();
            PopupModel
                .appendHeaderHtml("<span class=\"PageFooter_Button headRight\" id=\"nextButton\" onclick=\"EmailVendor.EmailVendor('" +
                    vendorId +
                    "','" + pqId + "','" + name + "',2)\">Next</span>",
                    1);

            var a = new nicEditor({
                buttonList: [
                    'fontSize', 'bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', 'html',
                    'ul', 'ol', 'indent', 'outdent', 'forecolor', 'bgcolor'
                ]
            }).panelInstance('RichTextArea');

        }



        //$("#SendCCDiv").css("display", "block");
    }

}

$(function () {
    //EmailVendor.GetPQStatuses();
    //EmailVendor.GetVendorOrganizations();

    $("#buttonTabOne").click(function () {
        $("#vendorName").show();
        $("#status").hide();
        $("#SearchType").attr('value', "Name");
        $("#searchVendorResults_block_id").html("");
        Tabs.click(this);
    });

    $("#buttonTabTwo").click(function () {
        $("#vendorName").hide();
        $("#status").show();
        $("#SearchType").attr('value', "status");
        $("#searchVendorResults_block_id").html("");
        Tabs.click(this);
    });

});
function SelectAllPrequalificationStatus() {

    if ($("#selectAllPrequalStatus").is(":checked")) {

        $("#searchVendorResults_block_id").find(":checkbox").each(function () {
            $(this).prop("checked", true);
        });
    }
    else {
        $("#searchVendorResults_block_id").find(":checkbox").each(function () {
            $(this).prop("checked", false);
        });
    }
}

function SelectAllPrequalificationStatuses() {
    if ($("#selectAllPrequalificationStatus").is(":checked")) {

        $("#statuses").find(":checkbox").each(function () {
            $(this).prop("checked", true);
        });
    }
    else {
        $("#statuses").find(":checkbox").each(function () {
            $(this).prop("checked", false);
        });
    }
}

function searchVendor() {
    var search = $("#SearchType").val();
    var vendorId = $("#vendors").val();
    var prequalStatus = new Array();

    $("#statuses input:checked").each(function () {
        //alert("Hai in status");
        prequalStatus.push($(this).attr('id'));

    });
    
    if (search === "status" && prequalStatus.length === 0) {
        alert("Please select at least one status type");
        return;
    }
    $.ajax({
        type: 'POST',
        url: 'VendorSearchResultsForEmailing',
        data: 'searchType=' + search + '&clientId=' + $("#currentOrgId").val() + '&vendorId=' + vendorId + '&prequalStatus=' + prequalStatus + '&sortBy=' + $("#orderBy").val(),
        success: function (data) {
            if (data.indexOf("Session expired") >= 0) {
                window.location = "../SystemUsers/Login?lastLoginPage=" + lastpageurl;
            }
            else {

                $("#searchVendorResults_block_id").html(data);

                //if (search == "Name") {
                //    $("#searchVendorResults_block_id").find($("#SelectAllCheckBoxDiv")).hide();
                //    $("#searchVendorResults_block_id").find($("#statusMessage")).hide();
                //} else {
                //    $("#SelectAllCheckBoxDiv").show();
                //}
                //$('.spinnerOverlay').hide();
                //$('.importSubmitSpinner').hide();
            }
        },
        error: function () {

            //$('.spinnerOverlay').hide();
            //$('.importSubmitSpinner').hide();
        }
    });
}