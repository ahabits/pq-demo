﻿var VendorSearch= {
    populateClientContractType: function() {


        var clientId = $("#Name").val();

        Post("../AdminVendors/GetClientContractTypes?ClientId=" + clientId,
           {  },
           function (data) {
               var controls = $(Controls.Dropdown(data));
               controls.attr("id", "contractType");
               controls.addClass("contractType");
               //controls.attr("style","width:98%");
               $("#contractTypeRow").html(controls);
           });

    }
}

var kgrid = $("#searchVendorResults_block_id").kendoGrid({
    dataSource: new kendo.data.DataSource({
        type: "get",

        transport: {
            type: 'post',
            read: {
                type: 'post',
                url: "../AdminVendors/VendorSearchResultsPartialView",
                data:function(options){
                    return Object.assign(getParams(), options.data);
                }
            },
            //read:"../AdminVendors/VendorSearchResultsPartialView",
            //data:{clientId:-1}

            //parameterMap: function (data, operation) {
            //    if (operation == "read") {
            //        return kendo.stringify(data, getParams())
            //    }
            //}
        //    read: function (options) {
        //        url: "../AdminVendors/VendorSearchResultsPartialView",


        //   additional = Object.assign(getParams(), options.data);
        //        additional.pageSize = 10;
        //        $.ajax({
        //            url: "../AdminVendors/VendorSearchResultsPartialView",
        //            type:'post',
        //            data: additional, // the "data" field contains paging, sorting, filtering and grouping data 
        //            success: function (result) {
        //                // notify the DataSource that the operation is complete 

        //                options.success(result);
        //            }
        //        });
        //    }

        },
        batch: true,
        schema: {

            data: "Data",
            total: "Total",
            model: {
                fields: {
                    PrequalificationStart: { type: "date" },
                    PrequalificationId: { type: "number" },
                    VendorName: { type: "string" },
                    PrequalificationFinish: { type: "date" },
                    VendorId: { type: "number" },
                    ClientId: { type: "number" },
                    TemplateCode: { type: "string" },
                    PrequalificationStatusId: { type: "number" }
                }
            }
        },
        pageSize: 50,
        serverPaging: true,
        serverFiltering: true,
        serverSorting: true
    }),
    autoBind: false,
    height: 700,

    pageable: true,
    columns: [
        {
            title: "Vendor Name:",
            template: '#if (PrequalificationStatusId< 3){#<a href="\\#!" onclick="vendorDetails("#=VendorId#",  "#=PrequalificationId#", "#=-1#", "#=0#","#=PrequalificationStatusId#")">#=VendorName#</a>#}else{ #<a href="../AdminVendors/VendorDetails?SelectedVendorId=#=VendorId#&PreQualificationId=#=PrequalificationId#&TemplateSectionID=#=-1#&mode=#=0#">#=VendorName# &nbsp<b>#=TemplateCode#</b></a>#}#'
        },
        {
            field: "Status",

            title: "Status:",
            filterable: false,
        },
        {
            title: "Status Period:",
            format: "{0:MM/dd/yyyy}",
            template: "#=kendo.toString(kendo.parseDate(new Date(PrequalificationStart), 'yyyy-MM-dd'), 'MM/dd/yyyy')#-#=kendo.toString(kendo.parseDate(new Date(PrequalificationFinish), 'yyyy-MM-dd'), 'MM/dd/yyyy')#"
        },
        {
            field: "PrequalificationId",
            title: "PrequalificationId",
            filterable: false,
            hidden: true
        },
         { template: "#if(HasPermission){#<a href='../Prequalification/FinancialAnalyticsWithPQ?PQId=#=PrequalificationId#'>Financial Analytics</a>#}#" },
    ]
});
function searchVendor(lastpageurl) {
    
    kgrid.data('kendoGrid').dataSource.page(1);
    //kgrid.data('kendoGrid').dataSource.refresh();
}
function getParams() {
    
    $("#searchVendorResults_block_id").show();

    var venname = $("#VendorName").val();



    var period = "searchParticular";

    if ($("#VenSearch").is(":checked")) {
        period = "searchAll";

    }

    var sort = $("#sortType").val();

    var scope = $("#Name").val();

    var search = $("#searchType").val();

    var firstLetter = $("#firstLetterVal").val();

   
    var statusId = new Array();

    var bidInterest = new Array();

    var claycobidInterest = new Array();

    var proficiencyCapabilities = new Array();

    var productCategories = new Array();


    var Subsite = $("#Subsite").val();

    if (Subsite == null)

        Subsite = "";
    else Subsite = Subsite.toString();


    var Site = $("#site").val();

    if (Site == null)

        Site = "";
    else Site = Site.toString();

    var clientvendorrefid = $("#ClientVendorRefId").val();

    var clientContractType = $("#contractType").val();

    var vendorState = $("#vendorState").val();

    var vendorCity = $("#vendorCity").val();
    var Country = $("#Country").val();
   
    //alert(Country);
    var clientTemplateId = $("#questionnaireType").val();

    if (clientTemplateId == "Any")

        clientTemplateId = "";

    $("#PrequalStatus input:checked").each(function () {


        statusId.push($(this).attr('PreID'));

    });


    $("#BiddingInterests input:checked").each(function () {



        bidInterest.push($(this).attr('BiddingID'));

    });

    $("#ClaycoBiddingInterests input:checked").each(function () {
        claycobidInterest.push($(this).attr('BiddingID'));
    });

    $("#ProductCategories input:checked").each(function () {
        productCategories.push($(this).attr('ProductCategoryID'));
    });

    $("#ProficiencyCapabilities input:checked").each(function () {
        proficiencyCapabilities.push($(this).attr('ProficiencyId'));
    });
    var GeographicArea = $("#GeographicArea").val();
    if (GeographicArea!=null) {
         GeographicArea = $("#GeographicArea").val().toString();
    }
   
    var viewModel = kendo.observable({
        param: {
            VendorName: encodeURIComponent(venname),
            periodVal: period,
            sortType: sort,
            
            vendorCity: vendorCity,
            Country: Country,
            vendorState: vendorState,
            Subsite: Subsite,
            clientId: scope,
            searchType: search,
            firstLetterVal: firstLetter,
            statusIdList: statusId.join(","),

            biddingIdList: bidInterest.join(","),
          
            claycoBiddingIdList: claycobidInterest.join(","),
            proficiencyIdList: proficiencyCapabilities,
            clientTemplateid: clientTemplateId,
            clientvendorrefid: clientvendorrefid,
            Site: Site,
            clientContractId: clientContractType,
            productCategoryList: productCategories,
            GeographicArea: GeographicArea
        }

    });
    return JSON.parse(JSON.stringify(viewModel.param));
}


