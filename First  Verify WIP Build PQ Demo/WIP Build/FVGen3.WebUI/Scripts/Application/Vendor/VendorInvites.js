﻿currentTab = 1;
var LocationIds = [];
var Comments = "";
var searchConfig = {
    processing: true,
    serverSide: true,
    bLengthChange: false,
    searching: false,
    "deferLoading": 0,
    "ajax": {
        "url": "../Client/GetVendorsForInvite",
        error: function (xhr, error, thrown) {
            alert("Unable to process the request please contact the system admin");
            console.log(xhr.responseText);
        },
        "data": function (d) {

            var vendorName = $('#vendorNameExpired').val();
            var vendorCity = "";
            var vendorState = "";
            var Country = "";
            if (currentTab !== 2) {
                Country = $('#CountryName option:selected').val();
                vendorName = $("#VendorName").val();
                vendorCity = $("#vendorCity").val();
                vendorState = $("#vendorState").val();
                if (vendorState === "N/A")
                    vendorState = "";
            }
            var biddings = [];
            var claycoBiddings = [];
            if (currentTab !== 2) {
                $("#BiddingInterests input:checked").each(function () {
                    biddings.push(Number($(this).attr("BiddingID")));
                    //alert(biddings);
                });
                $("#ClaycoBiddingInterests input:checked").each(function () {
                    claycoBiddings.push(Number($(this).attr("BiddingID")));
                    //alert(claycoBiddings);
                });
            }
            var proficiency = [];
            if (currentTab !== 2) {
                //$("#ProficiencyList input:checked").each(function () {
                //    proficiency.push(Number($(this).attr("id").replace("prof_", "")));
                //});
                $("#ProficiencyCapabilities input:checked").each(function () {
                    proficiency.push(Number($(this).attr("ProficiencyId")));
                });
            }
            biddings = biddings.join(',');
            proficiency = proficiency.join(',');
            claycoBiddings = claycoBiddings.join(',');
            d.vendorName = vendorName;
            d.status = 0;
            d.city = vendorCity;
            d.state = vendorState;
            d.type = window.currentTab === 2 ? 0 : 1;
            d.biddings = biddings;
            d.ERIOnly = $("#isERI").is(":checked");
            d.proficiency = proficiency;
            d.claycoBiddings = claycoBiddings;
            d.client = $('#client').val();
            d.Country = Country;
        },
        "type": "get"
    },
    aoColumnDefs: [
        { aTargets: ['_all'], bSortable: false }
    ],
    "lengthMenu": [[10, 20, 50], [10, 20, 50]],
    "columns": [
        { "data": "OrgId" },
        { "data": "OrgName" },
        { "data": "CurrentClientLatestStatus" }
    ],
    "createdRow": function (row, data, index) {
        //alert($(createOffsiteRow(data, index, $('#buDropdown').val(), $('#locDropdown').val())).html());
        //$(row).html($(createOffsiteRow(data, index, $('#buDropdown').val(), $('#locDropdown').val())).html());
        $(row).html(VendorInvites.GetSearchRow(data, currentTab));
    }
};

var globalVenList = $('#vendorDetailsGlobal').DataTable(searchConfig);
//var globalVenList = $('#vendorDetailsGlobalERI').DataTable(searchConfigForERIVendors);
//searchConfig.searching = true;
var locVenList = $('#vendorDetailsLocal').DataTable(searchConfig);

var richEditor;
var VendorInvites = {
    ShowVendorDetails: function (vendorId, name) {
        PopupModel.reset();
        PopupModel.setData("Loading..");
        PopupModel.setHeader("Vendor Summary");
        //PopupModel
        //    .appendHeaderHtml("<span class=\"PageFooter_Button headRight\" id=\"popupHeadRight\" onclick=\"VendorInvites.PreInvite(" +
        //        vendorId +
        //        ",null,'" + '' + "',0)\">Next</span>",
        //        1);
        PopupModel.show();
        Post("../Vendor/GetVendorSummary",
            { vendorId: vendorId },
            function (data) {
                PopupModel
           .appendHeaderHtml("<span class=\"PageFooter_Button headRight\" id=\"popupHeadRight\" onclick=\"VendorInvites.PreInvite(" +
               vendorId +
               ",null,'" + data.Name.replace(/'/g, '_') + "',0)\">Next</span>",
               1);

                PopupModel.setHeader(data.Name + " - " + data.PhoneNumber);
                PopupModel.setData(VendorInvites.VendorSummaryTemplate(data));
            });

    },
    ShowTerms: function (obj) {
        //alert($(obj));

        if ($("option:selected", obj).attr("IsLocked") === 'true') {
            TermsModel.reset();
            TermsModel.setData("Loading..");
            TermsModel.setHeader($(obj).val());
            TermsModel.show(function () {
                $(obj).val("");
            });
            Post("../AdminVendors/GetLockTemplateComments",
                { clientTemplateId: $("option:selected", obj).attr("clentTemplate") },
                function (data) {
                    data = data + "<br>" +
                        "<div style='margin:10px;'><input type='checkbox' id='lockedTearms'/> I Understand.</div><br><div><button type='button' onclick='VendorInvites.TermsAccepted()'>Done</button></div>";
                    TermsModel.setData(data);
                });

        }
    },
    TermsAccepted: function () {
        if ($("#lockedTearms").is(":checked"))
            TermsModel.forceClose();
        else
            alert("Please read the comments");
    },
    GetPQVendorDetailsByVendorId: function (vendorId, selector) {
        Post("../AdminVendors/GetPQVendorDetailsByVendorId",
            { vendorId: vendorId, clientId: $('#client').val() },
            function (data) {
                $(selector).val(data);
            });
    },

    IsERIClient: function (callback) {
        Post("../AdminVendors/CheckIsERIClient",
            { clientId: $('#client').val() },
            function (data) {
                callback(data);
            });
    },

    //PopulateERIVendors: function () {
    //    VendorInvites.IsERIClient(function (data) {
    //        if (data.toLowerCase() === "true") {
    //          //  $("#tab4").show();
    //            $('#vendorDetailsGlobalERI').DataTable().draw();
    //        }
    //        else {
    //        //    $("#tab4").hide();
    //        }
    //    });
    //},
    PreInvite: function (vendorId, pqId, name, step) {//step-0 Final other wise steps

        PopupModel.reset();
        var isSendMeCC = "";
        var isSendMeBCC = "";
        var sendCC = "";
        var sendBCC = "";
        LocationIds = [];
        Comments = "";
        if (currentTab === 2 && step === 0) {

            isSendMeCC = PopupModel.getBody().find("#searchIsSendMeCC").is(":checked");
            isSendMeBCC = PopupModel.getBody().find("#searchisSendBCC").is(":checked");
            sendCC = PopupModel.getBody().find("#searchUsersSendCC").val();
            sendBCC = PopupModel.getBody().find("#searchUsersSendBCC").val();
            $("#extraInfo").find("#searchLocation").parent().css("display", "none");
            $("#extraInfo").find("#searchRiskLevel").parent().css("display", "none");
            $("#extraInfo").find("#searchComments").parent().css("display", "none");
            $("#extraInfo").find("#SendCCDiv").css("display", "none");
            $("#extraInfo").find("#SendBCCDiv").css("display", "none");
            $("#extraInfo").find("#templateDiv").css("display", "block");
            $("#extraInfo").find("#toExpiredPQ").parent().css("display", "block");
            $("#extraInfo").find("#ccExpiredPQ").parent().css("display", "block");
            $("#extraInfo").find("#bccExpiredPQ").parent().css("display", "block");

        } else {
            $("#extraInfo").find("#searchLocation").parent().css("display", "block");
            $("#extraInfo").find("#searchComments").parent().css("display", "block");
            $("#extraInfo").find("#SendCCDiv").css("display", "block");
            if (currentTab === 2)
                $("#extraInfo").find("#searchRiskLevel").parent().css("display", "none");
            else
                $("#extraInfo").find("#searchRiskLevel").parent().css("display", "block");
            $("#extraInfo").find("#SendBCCDiv").css("display", "block");
            $("#extraInfo").find("#templateDiv").css("display", "none");
            $("#extraInfo").find("#toExpiredPQ").parent().css("display", "none");
            $("#extraInfo").find("#ccExpiredPQ").parent().css("display", "none");
            $("#extraInfo").find("#bccExpiredPQ").parent().css("display", "none");
        }
        var html = $("#extraInfo").html();// + "<input type='button' value='Send' onclick='VendorInvites.InviteVendor(" + vendorId + ")'/>";
        if (currentTab === 2 && step === 0) {
            var locations = [];
            PopupModel.getBody().find("#searchLocation :selected").each(function () {
                var $this = $(this);
                if ($this.length) {
                    locations.push($this.text());
                    LocationIds.push($this.val());
                }

            });            //locations.push("SDAFDASFa"); 
            var riskLevel = PopupModel.getBody().find("#searchRiskLevel").val();
            var comments = PopupModel.getBody().find("#searchComments").val();
            Comments = comments;
            if (locations.length === 0)
                locations = ["N/A"];
            html = html.replace("[LocationName]", locations.join(","));
            html = html.replace("[VendorComments]", comments);
            html = html.replace("[RiskLevelName]", riskLevel);
        }
        if (currentTab === 2 && step !== 0) {
            PopupModel
                .appendHeaderHtml("<span class=\"PageFooter_Button headRight\" id=\"popupHeadRight\" onclick=\"VendorInvites.PreInvite(" +
                    vendorId +
                    "," + pqId + ",'" + name.replace(/'/g, '') + "',0)\">Next</span>",
                    1);

        } else {
            PopupModel
                .appendHeaderHtml("<span class=\"PageFooter_Button headRight\" id=\"popupHeadRight\" onclick=\"VendorInvites.InviteVendor(" +
                    vendorId +
                    "," + pqId + ",'" +
                    isSendMeCC +
                    "','" +
                    isSendMeBCC +
                    "','" +
                    sendCC +
                    "','" +
                    sendBCC +
                    "')\">Send Invitation</span>",
                    1);
        }
        PopupModel.setData(html);
        if (currentTab === 2 && step === 0) {
            VendorInvites.GetPQVendorDetailsByVendorId(vendorId, PopupModel.getBody().find("#toExpiredPQ"));
            PopupModel.getBody().find("#ccExpiredPQ").val(sendCC);
            PopupModel.getBody().find("#bccExpiredPQ").val(sendBCC);
        }
        PopupModel.setHeader(name.replace('_', "'"));
        richEditor = new nicEditor({
            buttonList: [
                'fontSize', 'bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', 'html',
                'ul', 'ol', 'indent', 'outdent', 'forecolor', 'bgcolor'
            ]
        }).panelInstance('RichTextArea');

        PopupModel.show();
        VendorInvites.GetLocalInvitationTemplate(vendorId);
    },
    resetCss: function () {
        $("#searchLocation").parent().css("display", "none");
        $("#searchRiskLevel").parent().css("display", "none");
        $("#searchComments").parent().css("display", "none");
        $("#SendCCDiv").css("display", "none");
        $("#SendBCCDiv").css("display", "none");
        $("#templateDiv").css("display", "block");
    },
    InviteVendor: function (vendorId, pqId, isSendMeCC, isSendMeBCC, sendCC, sendBCC) {

        var locations = PopupModel.getBody().find("#searchLocation").val();
        //locations.push("SDAFDASFa"); 
        var riskLevel = PopupModel.getBody().find("#searchRiskLevel").val();
        var comments = PopupModel.getBody().find("#searchComments").val();
        if (currentTab === 2) {
            locations = LocationIds;
            comments=Comments;

}
        var sendTo = "";
        if (isSendMeCC === "") {
            var isSendMeCC = PopupModel.getBody().find("#searchIsSendMeCC").is(":checked");
            var isSendMeBCC = PopupModel.getBody().find("#searchisSendBCC").is(":checked");
            sendCC = PopupModel.getBody().find("#searchUsersSendCC").val();
            sendBCC = PopupModel.getBody().find("#searchUsersSendBCC").val();
        } else {
            sendCC = PopupModel.getBody().find("#ccExpiredPQ").val();
            sendBCC = PopupModel.getBody().find("#bccExpiredPQ").val();
            sendTo = PopupModel.getBody().find("#toExpiredPQ").val();
            var reg = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;
            if (sendTo.trim() === "") {
                alert("Please provide the valid To address");
                return;
            }
            var emails = sendTo.split(",");
            for (var i = 0; i < emails.length; i++) {
                if (emails[i] !== "" && !reg.test(emails[i])) {
                    alert("Invalid email has been found in To Address. Please provide the valid email address");
                    return;
                }
            }
            emails = sendCC.split(",");
            for (var i = 0; i < emails.length; i++) {
                if (emails[i] !== "" && !reg.test(emails[i])) {
                    alert("Invalid email has been found in CC Address. Please provide the valid email address");
                    return;
                }
            }
            emails = sendBCC.split(",");
            for (var i = 0; i < emails.length; i++) {
                if (emails[i] !== "" && !reg.test(emails[i])) {
                    alert("Invalid email has been found in BCC Address. Please provide the valid email address");
                    return;
                }
            }
            //var reg =
            //    /^(([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,8}|[0-9]{1,3})(\]?)(\s*,\s*|\s*$))*/;
            //if (!reg.test(sendTo) || !reg.test(sendCC) || !reg.test(sendBCC)) {
            //    alert("Invalid Emails");
            //    return;
            //}



        }
        //return;
        var htmlTemplate = PopupModel.getBody().find("#templateDiv").find('.nicEdit-main').html();
        if (currentTab !== 2)
            htmlTemplate = "";

        if (locations === null || locations === undefined)
            locations = [];
        if (sendCC === null || sendCC === undefined)
            sendCC = [];
        if (sendBCC === null || sendBCC === undefined)
            sendBCC = [];

        $(".spinnerOverlay").show();
        $(".importSubmitSpinner").show();
        Post("../AdminVendors/OutSideInvitation",
            {
                vendorId: vendorId,
                invitationType:
                    currentTab - 1,
                locations: locations.join(","),
                isSendMeCC: isSendMeCC,
                isSendMeBCC: isSendMeBCC,
                riskLevel: riskLevel,
                sendCC: currentTab === 2 ? sendCC : sendCC.join(","),
                sendBCC: currentTab === 2 ? sendBCC : sendBCC.join(","),
                comments: comments,
                toAddress: sendTo,
                htmlTemplate: htmlTemplate,
                clientId: $('#client').val(),
                PqId: pqId,
                isERI: $('#isERI').is(":checked"),
               
            },
            function (data) {
                $(".spinnerOverlay").hide();
                $(".importSubmitSpinner").hide();
                if (data === "Success") {
                    alert("Invitation has been sent successfully");
                    PopupModel.closeModel();
                } else
                    alert(data);

            });
    },
    VendorSummaryTemplate: function (data) {
        var html = "";
        html += '<div class="vendorDetailsTableLeft">' +
            '<div class="section-heading">' +
            '<h2 style="margin:0;padding:5px 2px;"> Vendor Details</h2>' +
            '</div>' +
            '<table>' +
            '<tr><td class="itemHeader">Address</td>' +
            '<td>' + data.Address1 + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td></td>' +
            '<td>' + data.Address2 + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td></td>' +
            '<td>' + data.City + ' , ' + data.State + data.Zip + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td class="itemHeader">Phone</td>' +
            '<td>' + data.PhoneNumber + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td class="itemHeader">Fax</td>' +
            '<td>' + data.Fax + '</td></tr>' +
            '<tr><td class="itemHeader">Website</td><td><a href="#!" onclick="window.open(\'' + data.Website + '\')">' + data.Website + '</a></td>' +
            //@if (@a.WebsiteURL != null)
            //{
            //    if (@a.WebsiteURL.StartsWith("http:"))
            //    {
            //        <td><a href="#!" onclick="window.open('@a.WebsiteURL')">@a.WebsiteURL</a></td>
            //        }
            //    else
            //    {
            //        <td><a href="#!" onclick="window.open( 'http://@a.WebsiteURL')">@a.WebsiteURL</a></td>
            //        }
            //}
            '</tr>' +
            '</table>' +
            '</div>';
        //if (data.Address !== "") {
        //    html += '<div class="rowGenericReports" id="popRiskLevel">' +
        //    '<label class="section-heading "><h2>Address</h2></label>' +
        //    '<div class="popData">';
        //    html += data.Address;
        //    html += '</div>' +
        //        '</div>';
        //}
        //if (data.Fax !== "") {
        //    html += '<div class="rowGenericReports" id="popRiskLevel">' +
        //        '<label class="section-heading "><h2>Fax</h2></label>' +
        //        '<div class="popData">';
        //    html += data.Fax;
        //    html += '</div>' +
        //        '</div>';
        //}
        //if (data.Website !== "") {
        //    html += '<div class="rowGenericReports" id="popRiskLevel">' +
        //        '<label class="section-heading "><h2>Website</h2></label>' +
        //        '<div class="popData">';
        //    html += data.Website;
        //    html += '</div>' +
        //        '</div>';
        //}
        if (data.Biddings.length > 0) {
            html += '<div class="rowGenericReports" id="popBiddings">' +
                '<label class="section-heading "><h2>Bidding Interests</h2></label>' +
                '<ul class="popData">';
            $.each(data.Biddings,
                function (i) {
                    html += "<li>" + data.Biddings[i] + "</li>";
                });
            html += '</ul>' +
                '</div>';
        } else if (data.Proficiancy.length > 0) {
            html += '<div class="rowGenericReports" id="popProficiency">' +
                '<label class="section-heading "><h2>Proficiency & Capability</h2></label>' +
                '<ul class="popData">';
            $.each(data.Proficiancy,
           function (i) {
               html += "<li>" + data.Proficiancy[i] + "</li>";
           });
            html += '</ul>' +
            '</div>';
        }
        html += '<div class="rowGenericReports" id="popStatus">' +
            '<label class="section-heading "><h2>Status(es)</h2></label>' +
            '<ul class="popData">';
        $.each(data.Statuses,
            function (i) {
                html += "<li> Client " + (i + 1) + ": " + data.Statuses[i] + "</li>";
            });
        html += '</ul>' +
                '</div>';
        html += '<div class="rowGenericReports" id="popRiskLevel">' +
            '<label class="section-heading "><h2>Risk Level(s)</h2></label>' +
            '<ul class="popData">';
        $.each(data.RickLevels,
            function (i) {
                html += "<li>  Client " + (i + 1) + ": " + data.RickLevels[i] + "</li>";
            });
        html += '</ul>' +
            '</div>';
        return html;
    },
    GetSearchRow: function (data, currentTab) {
        var info;
        if (currentTab === 2) {
            info = '<td width="60%">' +
                data.OrgName +
                '</td> ' +
                '<td  width="20%">' +
                data.CurrentClientLatestStatus +
                '</td>' +
                '<td width="20%"><a href="#!" onclick="VendorInvites.PreInvite(' + data.OrgId + ',' + data.PQId + ',\'' + data.OrgName.replace(/'/g, '') + '\',1)">Invite Vendor</a></td>';
        } else {
            info = '<td  width="100%"><a href="#!" onclick="VendorInvites.ShowVendorDetails(' +
                data.OrgId +
                ',\'' + data.OrgName.replace("'", "") + '\')">' +
                data.OrgName+
                '</a></td> <td style="display:none"></td><td style="display:none"></td>';
        }
        return info;
    },
    //ShowVendors: function (data) {

    //    var html = "<table>";
    //    //globalVenList.columns.remove();
    //    globalVenList.clear();
    //    globalVenList.column(1).visible(currentTab === 2);
    //    globalVenList.column(2).visible(currentTab === 2);

    //    if (data.length === 0)
    //        html += '<tr><td><i>No Data Available</i></td></tr>';
    //    $.each(data,
    //        function (i) {
    //            var info;
    //            if (currentTab === 2) {
    //                info = '<tr><td>' +
    //                    data[i].OrgName +
    //                    '</td> ' +
    //                    '<td>' +
    //                    data[i].CurrentClientLatestStatus +
    //                    '</td>' +
    //                    '<td><a href="#!" onclick="VendorInvites.PreInvite(' + data[i].OrgId + ',\'' + data[i].OrgName + '\')">Invite Vendor</a></td></tr>';
    //            } else {
    //                info = '<tr><td><a href="#!" onclick="VendorInvites.ShowVendorDetails(' +
    //                    data[i].OrgId +
    //                    ',\'' + data[i].OrgName + '\')">' +
    //                    data[i].OrgName +
    //                    '</a></td> <td></td><td></td>';
    //            }
    //            globalVenList.rows.add($(info));
    //            html += info;
    //        });
    //    html += "</table>";
    //    globalVenList.draw();
    //    //$("#vendorDetails").html(html);
    //},
    GetLocalInvitationTemplate: function (vendorId) {
        Post("../AdminVendors/GetLocalInvitation",
            { clientId: $('#client').val(), vendorId:vendorId },
            function (data) {
                $(".nicEdit-main").html(data);
                
                //richEditor.removeInstance("RichTextArea");
                //$("#RichTextArea").html(data);
                //richEditor = new nicEditor({
                //    buttonList: [
                //        'fontSize', 'bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', 'html',
                //        'ul', 'ol', 'indent', 'outdent', 'forecolor', 'bgcolor'
                //    ]
                //}).panelInstance('RichTextArea');
            });
    },
    TabClick: function (obj, tabNumber) {
        
        window.currentTab = tabNumber;
        Tabs.click(obj);
        if (tabNumber === 1) {
            $("#newVendorDiv").show();
            $("#inviteVendorWithSearch").hide();
        } else {
            VendorInvites.HideSections();
            $("#newVendorDiv").hide();
            $("#inviteVendorWithSearch").show();
            if (tabNumber === 2) {
                $("#searchVendorsForExpired").show();
                $("#vendorDetailsExpired").show();
               
                $("#expiredHint").show();
            }
            if (tabNumber === 3) {
                VendorInvites.ShowSections();                
                $("#outsideHint").show();                
                $("#vendorDetailsOutSideNW").show();
            }
            //if (tabNumber === 4) {
            //    $("#vendorDetailsERI").show();                
            //}
        }
    },
    HideSections: function () {
        //$("#StatusDiv").hide();
        $("#BiddingsDiv").hide();
        $("#vendorStateCityDiv").hide();
        $("#ProficiencyDiv").hide();
        $("#vendorDetailsExpired").hide();
        $("#vendorDetailsOutSideNW").hide();
        $("#vendorName").hide();
        $("#searchBtnDiv").hide();
        $("#expiredHint").hide();
        $("#outsideHint").hide();
        $("#vendorDetailsERI").hide();
       $("#ERIRow").hide();
       $("#searchVendorsForExpired").hide();
    },
    ShowSections: function () {
        //$("#StatusDiv").show();
        $("#BiddingsDiv").show();
        $("#vendorStateCityDiv").show();
        $("#ProficiencyDiv").show();
        $("#vendorName").show();
        $("#searchBtnDiv").show();
        $("#ERIRow").show();
    },
    GetClientBiddingInterests: function (selector) {
        Post("../AdminVendors/BiddingOrProficiencyResultsPartial",
            { clientId: $('#client').val() },
            function (data) {
                $(selector).html(data);
            });
    },

    GetCountryStates: function (clientId, includeCanada, ddlSelector) {
        
        var country = $("#Country").val();
        
        if (window.currentTab === 3) { country = $("#CountryName").val(); }
        //alert(ddlSelector);
        $.ajax({
            type: 'Get',
            url: '../Vendor/GetStateSelectListItemsJson',
            data: { clientID: clientId, includeCanada: includeCanada, country: country.replace(/'/g, "''") },
            success: function (data) {
                //ddlSelector.attr("style", "height:200px;width:300px;");
                ddlSelector.empty();

                for (var i = 0; i < data.length; i++) {
                    var datasite = data[i].Text;
                    newOption = $("<option value='" + data[i].Value + "'>" + datasite + "</option>");
                    ddlSelector.append(newOption);
                }
            },
            error: function () {
                alert("Unable to fetch state(s).Please try again. ");
            }
        });
    },

    GetCountryLanguages: function(ddlSelector)
    {
        
        //alert("hai");
        var country = $("#Country").val();

        $.ajax({
            type: 'Get',
            url: '../AdminVendors/GetGetCountryLanguages',
            data: { country: country },
            success: function (languages) {

                ddlSelector.empty();

                for (var i = 0; i < languages.length; i++) {
                    //var datasite = data[i].Text;
                    newOption = $("<option value='" + languages[i].Value + "'>" + languages[i].Key + "</option>");
                    ddlSelector.append(newOption);
                }
            },
            error: function () {
                alert("Unable to fetch state(s).Please try again. ");
            }
        });
    }
}
$(function () {

    //var config = {
    //    source: function (request, response) {
    //        jQuery.get("/Vendor/GetVendorsAutoComplete",
    //            {
    //                prefix: request.term
    //            },
    //            function (data) {
    //                // assuming data is a JavaScript array such as
    //                // ["one@abc.de", "onf@abc.de","ong@abc.de"]
    //                // and not a string
    //                response($.map(data,
    //                    function (item) {
    //                        return { label: item, value: item };
    //                    }));
    //            });
    //    },
    //    minLength: 1
    //};

    //$("#VendorName").autocomplete(config);
    //$("#VendorCompanyName").autocomplete(config);

    //Biddings.GetBiddings(function (result) {
    //    $("#biddingList").html(Controls.MultiCheckBoxes(result, "bid_"));
    //});
    VendorInvites.GetClientBiddingInterests("#biddingList");
    //Proficiency.GetProficiency(function (result) {
    //    //Object.keys(result).forEach(function (key) {
    //    //    result[key].Key = result[key].Id;
    //    //    result[key].header = result[key].Category;
    //    //    result[key].Value = result[key].SubCategory + (result[key].SubCategory === "" ? '' : ' - ') + result[key].ProficiencyName;
    //    //    delete result[key].Id;
    //    //    delete result[key].Category;
    //    //    delete result[key].SubCategory;
    //    //    delete result[key].ProficiencyName;
    //    //});
    //    //Object.keys(result).forEach(function (key) {
    //    for (var key = 0; key < result.length; key++) {
    //        result[key].Key = result[key].Id;
    //        result[key].header = result[key].Category;
    //        result[key].Value = result[key].SubCategory +
    //            (result[key].SubCategory === "" ? '' : ' - ') +
    //            result[key].ProficiencyName;
    //        delete result[key].Id;
    //        delete result[key].Category;
    //        delete result[key].SubCategory;
    //        delete result[key].ProficiencyName;
    //    }
    //    //});
    //    $("#ProficiencyList").html(Controls.MultiCheckBoxesWithHeader(result, "prof_"));
    //});
    States.GetStates(false,
        function (result) {
            $("#vendorStateRow").html($(Controls.Dropdown(result)).attr("id", "vendorState"));
        });
    Prequalification.GetPQStatues(function (result) {
        //$("#Statuses").html(Controls.MultiCheckBoxes(result));
        var data = _.filter(result, { SType: false });
        var control = $(Controls.Dropdown(data));
        control.prepend("<option value=0>All</option>");
        control.val("0");
        $("#StatusesDiv").html(control.attr("id", "statuses"));
        $('#statuses').on('change', function () {
            $('#vendorDetailsLocal').DataTable().draw();
        });
    });
    $("#searchVendors").click(function () {
        globalVenList.clear();
        globalVenList.column(1).visible(currentTab === 2);
        globalVenList.column(2).visible(currentTab === 2);
        $('#vendorDetailsGlobal').DataTable().draw();
        //$('#vendorDetailsGlobalERI').DataTable().draw();

    });
    $('#searchVendorsForExpired').click(function () {
        locVenList.draw();
    });

});