﻿var EditVendor = {
    GetPrequalificationPayments: function (vendorId, selector, callback) {
       
        $.ajax({
            type: 'post',
            url: '../Prequalification/GetPrequalificationPayments',
            data: { vendorId: vendorId },
            success: function (data) {
                var template = "<div class='partialPage'><div class = 'page-subtitle' style='width:100%;float:left;'> <h2><b>Prequalification Payments</b></h2> </div> ";
                if (data.length == 0)
                    template = template.concat("<table><tr><td><i>No Prequalification payments have been made</i></td></tr></table>");
                else {
                    var payments = _.chain(data).sortBy("PaymentReceivedDate")
                      .reverse()
                      .groupBy("FeeCapPeriod")
                      .toPairs()
                      .map(function (currentItem) {
                          return _.zipObject(["FeeCapPeriod", "info"], currentItem);
                      })
                      .value();
                  //  var payments = _.sortBy(payment, 'FeeCapPeriod').reverse();
                 
                    template = template.concat("<table>");
                   for (var i = 0; i < payments.length; i++) {

                       var initialRec = payments[i].info[0];
                       if (initialRec.FeeCapPeriod == 'Final status not assigned') continue;
                        template = template.concat("<tr class='no-bottom-border'>");
                        template = template.concat("<td colspan=5><b>" + initialRec.FeeCapPeriod +" </b></td>");
                        if (initialRec.PeriodStatus == 0 || initialRec.FeeCapPeriod == 'Open Amount')
                            template = template.concat("<td><b><img src='../images/collapse.png' style='cursor: pointer;height:20px;width:20px;float:right;margin-right:20px;' class='ExpandListButton" + i + "' onclick=SectionClick(" + payments.length + ",'PQFeeFields" + i + "',this) /> </b></td>");
                        else
                            template = template.concat("<td><b><img src='../images/expandlist.png' style='cursor: pointer;height:20px;width:20px;float:right;margin-right:20px;' class='ExpandListButton" + i + "' onclick=SectionClick(" + payments.length + ",'PQFeeFields" + i + "',this) /> </b></td>");

                            template = template.concat("</tr>");
                            
                        template = template.concat("<tr style='border-bottom: none!important;'><td></td></tr>");
                        for (var j = 0; j < payments[i].info.length; j++) {
                          
                            
                            var value = payments[i].info[j];
                            
                               
                            if (initialRec.PeriodStatus == 0 || initialRec.FeeCapPeriod == 'Open Amount') {
                                if (j == 0) {
                                    template = template.concat("<tr class=PQFeeFields" + i + "><th align='left'>Transaction Id</th><th align='left'>Received Date</th><th align='left'>Template Name</th><th align='left'>Amount</th></tr>");
                                }
                                template = template.concat("<tr class=PQFeeFields" + i + ">");
                            }
                            else {
                                if (j == 0) {
                                    template = template.concat("<tr style='display:none' class=PQFeeFields" + i + "><th align='left'>Transaction Id</th><th align='left'>Received Date</th><th align='left'>Template Name</th><th align='left'>Amount</th></tr>");
                                }
                                template = template.concat("<tr style='display:none'class=PQFeeFields" + i + ">");
                            }
                            template = template.concat("<td>" + value.PaymentTransactionId + "</td>");
                            template = template.concat("<td>" + (value.PaymentReceivedDate == null ? "" : moment(value.PaymentReceivedDate).format("MM/DD/YYYY h:mm A")) + "</td>");
                            template = template.concat("<td>" + value.TemplateName + "</td>");
                            template = template.concat("<td>$" + value.PaymentReceivedAmount + "</td>");
                            template = template.concat("</tr>");
                        }
                    }
                   for (var i = 0; i < payments.length; i++) {

                       var initialRec = payments[i].info[0];
                       if (initialRec.FeeCapPeriod != 'Final status not assigned') continue;
                       template = template.concat("<tr class='no-bottom-border'>");
                       template = template.concat("<td colspan=5><b>" + initialRec.FeeCapPeriod + " </b></td>");
                       if (initialRec.PeriodStatus == 0 || initialRec.FeeCapPeriod == 'Open Amount')
                           template = template.concat("<td><b><img src='../images/collapse.png' style='cursor: pointer;height:20px;width:20px;float:right;margin-right:20px;' class='ExpandListButton" + i + "' onclick=SectionClick(" + payments.length + ",'PQFeeFields" + i + "',this) /> </b></td>");
                       else
                           template = template.concat("<td><b><img src='../images/expandlist.png' style='cursor: pointer;height:20px;width:20px;float:right;margin-right:20px;' class='ExpandListButton" + i + "' onclick=SectionClick(" + payments.length + ",'PQFeeFields" + i + "',this) /> </b></td>");

                       template = template.concat("</tr>");

                       template = template.concat("<tr style='border-bottom: none!important;'><td></td></tr>");
                       for (var j = 0; j < payments[i].info.length; j++) {


                           var value = payments[i].info[j];


                           if (initialRec.PeriodStatus == 0 || initialRec.FeeCapPeriod == 'Open Amount') {
                               if (j == 0) {
                                   template = template.concat("<tr class=PQFeeFields" + i + "><th align='left'>Transaction Id</th><th align='left'>Received Date</th><th align='left'>Template Name</th><th align='left'>Amount</th></tr>");
                               }
                               template = template.concat("<tr class=PQFeeFields" + i + ">");
                           }
                           else {
                               if (j == 0) {
                                   template = template.concat("<tr style='display:none' class=PQFeeFields" + i + "><th align='left'>Transaction Id</th><th align='left'>Received Date</th><th align='left'>Template Name</th><th align='left'>Amount</th></tr>");
                               }
                               template = template.concat("<tr style='display:none'class=PQFeeFields" + i + ">");
                           }
                           template = template.concat("<td>" + value.PaymentTransactionId + "</td>");
                           template = template.concat("<td>" + (value.PaymentReceivedDate == null ? "" : moment(value.PaymentReceivedDate).format("MM/DD/YYYY h:mm A")) + "</td>");
                           template = template.concat("<td>" + value.TemplateName + "</td>");
                           template = template.concat("<td>$" + value.PaymentReceivedAmount + "</td>");
                           template = template.concat("</tr>");
                       }
                   }

               template = template.concat("<td></td>");
                    
                    template = template.concat("</table>");
                }
                template = template.concat("</div>");
                selector.append(template);
               
                if (callback)
                    callback();
            },
            error: function () {
                alert("Unable to process prequalification payments please try again");
            }
        });
    },
    GetQuizPayments: function (vendorId, selector, callback) {
       
        $.ajax({
            type: 'post',
            url: '../Prequalification/GetQuizPayments',
            data: { vendorId: vendorId },
            success: function (data) {

                var template = "<div class='partialPage'><div class = 'page-subtitle' style='width:100%;float:left;'> <h2><b>Quiz Payments</h2></b> </div>";
                if (data.length == 0)
                    template = template.concat("<table><tr><td><i>No Quiz payments have been made</i></td></tr></table>");
                else {
                    var payments = _.chain(data)
                      .groupBy("ClientName")
                      .toPairs()
                      .map(function (currentItem) {
                          return _.zipObject(["ClientName", "info"], currentItem);
                      })
                      .value();
                    template = template.concat(" <table>");
                    for (var i = 0; i < payments.length; i++) {
                        
                        var initialRec = payments[i].info[0];

                        template = template.concat("<tr class='no-bottom-border'>");
                        template = template.concat("<td  colspan=6><b>" + initialRec.ClientName + "</b></td>");
                      
                        template = template.concat("<td><b><img src='../images/expandlist.png' style='cursor: pointer;height:20px;width:20px;float:right;margin-right:20px;' class='QuizExpand QuizExpandListButton" + i + "'  onclick=SectionClick(" + payments.length + ",'QuizFields" + i + "',this) /> </b></td>");

                        template = template.concat("</tr>");
                        template = template.concat("<tr style='border-bottom: none!important;'><td></td></tr>");

                        var payments1 = _.chain(payments[i].info)
                        .groupBy("PaymentTransactionId")
                        .toPairs()
                        .map(function(currentItem) {
                            return _.zipObject(["PaymentTransactionId", "info"], currentItem);
                        })
                        .value();
                       
                   for (var j = 0; j < payments1.length; j++) {
                        
                       var initialRec = payments1[j].info[0];
                       if(j==0)
                        template = template.concat("<tr style='display:none' class= QuizFields" + i + "><th align='left'>Transaction Id</th><th align='left'>Received Date</th><th align='left'>Employee Name</th><th align='left'>Quiz Name</th><th align='left'>Amount</th></tr>");

                        template = template.concat("<tr style='display:none' class= QuizFields" + i + ">");
                      template = template.concat("<td>" + initialRec.PaymentTransactionId + "</td>");
                        template = template.concat("<td>" + (initialRec.PaymentTransactionId !== '' ? (initialRec.PaymentReceivedDate == null ? "" : moment(initialRec.PaymentReceivedDate).format("MM/DD/YYYY h:mm A")) : '') + "</td>");
                        template = template.concat("<td></td>");
                        template = template.concat("<td></td>");

                        template = template.concat("<td>$" + (initialRec.PaymentTransactionId !== '' ? initialRec.TransactionMetaData:'0') + "</td>");
                        template = template.concat("</tr>");
                        for (var k = 0; k < payments1[j].info.length; k++) {
                            

                            var value = payments1[j].info[k];
                            template = template.concat("<tr style='display:none' class=QuizFields" + i + ">");
                            template = template.concat("<td></td>");
                           
                            template = template.concat("<td>" + (value.PaymentTransactionId === '' ? (value.PaymentReceivedDate == null ? "" : moment(value.PaymentReceivedDate).format("MM/DD/YYYY h:mm A")):'') + "</td>");
                            template = template.concat("<td>" + value.EmployeeName + "</td>");
                            template = template.concat("<td>" + value.QuizName + "</td>");
                            template = template.concat("<td></td>");
                            template = template.concat("</tr>");
                        }
                    }
                }
                    template = template.concat("</table>");
                    //template = template.concat(" <table>");
                    //template = template.concat("<tr><th align='left'>Transaction Id</th><th align='left'>Received Date</th><th align='left'>Employee Name</th><th align='left'>Amount</th></tr>");
                    //for (var i = 0; i < data.length; i++) {
                    //    var value = data[i];
                    //    template = template.concat("<tr>");
                    //    template = template.concat("<td>" + value.PaymentTransactionId + "</td>");
                    //    template = template.concat("<td>" + (value.PaymentReceivedDate == null ? "" : moment(value.PaymentReceivedDate).format("MM/DD/YYYY h:mm A")) + "</td>");
                    //    template = template.concat("<td>" + value.EmployeeName + "</td>");
                    //    template = template.concat("<td>$" + value.PaymentReceivedAmount + "</td>");
                    //    template = template.concat("</tr>");
                    //}
                    //template = template.concat("</table>");
                }
                template.concat("</div>");
                selector.prepend(template);
              
                if (callback)
                    callback();
            },
            error: function () {
               
                alert("Unable to process Quiz payments please try again");
            }
        });
    },
    GetVendorOpenPeriods: function (vendorId, selector, callback) {
      
        $.ajax({
            type: 'post',
            url: '../Prequalification/GetVendorOpenPeriods',
            data: { vendorId: vendorId },
            success: function (data) {
                var template = "<div class='partialPage'><div class = 'page-subtitle'> <h2>Fee Cap Details</h2> </div> ";
                if (data.length === 0)
                    template = template.concat("<table><tr><td><i>No Prequalification periods</i></td></tr></table>");
                else {
                    template = template.concat("<table>");
                    template = template.concat("<tr><th align='left'>Period Start</th><th align='left'>Period End</th><th align='left'>Period Status</th><th align='left'>Total Payments Received</th></tr>");
                    for (var i = 0; i < data.length; i++) {
                        var value = data[i];
                        template = template.concat("<tr>");
                        template = template.concat("<td>" + moment(value.PeriodStart).format("MM/DD/YYYY h:mm A") + "</td>");
                        template = template.concat("<td>" + moment(value.PeriodClose).format("MM/DD/YYYY h:mm A") + "</td>");
                        template = template.concat("<td>" + (value.PeriodStatus === 0?"Open":"Closed") + "</td>");
                        template = template.concat("<td>$" + value.TotalPaymentRecieved + "</td>");
                        template = template.concat("</tr>");
                    }
                    template = template.concat("</table>");
                }
                template = template.concat("</div>");
                selector.append('<br/><br/>' + template);
           
                if (callback)
                    callback();
            },
            error: function () {
              
                alert("Unable to process prequalification payments please try again");
            }
        });
    },
    GetVendorOpenAmounts: function (vendorId, selector, callback) {
      
        $.ajax({
            type: 'post',
            url: '../Prequalification/GetVendorFeecapOpenAmount',
            data: { vendorId: vendorId }, 
            success: function (data) {
                var template = "<div class='partialPage'><div class = 'page-subtitle'> <h2>Fee Cap Open/Pending Amounts</h2>" +
                    "<i><h4 style='float:left;color:red';> Once these prequalifications are approved, This amount will be adjusted to fee cap amount </h4></i></div> ";
                if (data.length === 0)
                    template = template.concat("<table><tr><td><i>No Open Payments</i></td></tr></table>");
                else {
                    template = template.concat("<table>");
                    template = template.concat("<tr class='no-bottom-border'><th align='left' style='padding: 10px;'>Template Name</th><th align='left' style='padding: 10px;'>Amount</th><th><b><img src='../images/expandlist.png' style='cursor: pointer;height:20px;width:20px;float:right;margin-right:20px;' class='QuizExpand'  onclick=SectionClick(" + 0 + ",'FeeCap',this) /> </b></th></tr>");
                    for (var i = 0; i < data.length; i++) {
                        var value = data[i];
                        template = template.concat("<tr style='display:none;' class='FeeCap'>");
                        template = template.concat("<td>" + value.TemplateName + "</td>");
                        template = template.concat("<td>$" + value.Amount + "</td>");
                        template = template.concat("</tr>");
                    }
                    template = template.concat("</table>");
                }
                template = template.concat("</div>");
                selector.append('<br/><br/>' + template);
              
                if (callback)
                    callback();
            },
            error: function () {
               
                alert("Unable to process prequalification payments please try again");
            }
        });
    },

}
