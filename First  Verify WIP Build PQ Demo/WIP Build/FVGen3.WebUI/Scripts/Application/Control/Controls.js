﻿var Controls = {
    MultiCheckBoxes: function (list, idExtention) {
        var html = "<div class='scrollCheckboxReports'><table>";
        $.each(list, function (i) {
            html += '<tr><td><input id="' + idExtention + list[i].Key + '" type="checkbox"';
            $.each(list[i], function (key, value) {
                if (key !== "Key" && key !== "Value")
                    html += ' ' + key + '=' + value;
            });
            html += '></td><td><label for="' + idExtention + list[i].Key + '">' + list[i].Value + '</label></td></tr>';
        });
        html += '</div></table>';
        return html;
    },
    MultiCheckBoxesWithHeader: function (data, idExtention) {
        var html = "<div class='scrollCheckboxReports lineheight'><table>";

        var result = _.sortBy(_.toPairs(_.groupBy(data, "header"))
            .map(function (currentItem) {
                return _.zipObject(["header", "childData"], currentItem);
            }),
            "data");
        for (var j = 0; j < result.length; j++) {
            html += '<tr><td colspan=2><strong>' + result[j].header + '</strong></td></tr>';
            var list = result[j].childData;
            $.each(list, function (i) {
                html += '<tr><td><input id="' + idExtention + list[i].Key + '" type="checkbox"';
                $.each(list[i], function (key, value) {
                    if (key !== "Key" && key !== "Value" && key !== "header")
                        html += ' ' + key + '=' + value;
                });
                html += '></td><td><label for="' + idExtention + list[i].Key + '">' + list[i].Value + '</label></td></tr>';
            });
        }

        html += '</div></table>';
        return html;
    },
    Dropdown: function (data) {
        var html = "<select class='rowGenericReportsSelect'>";
        $.each(data, function (i) {
            html += '<option value=' + data[i].Key;
            //Add Additional Params
            $.each(data[i], function (key, value) {
                if (key !== "Key" && key !== "Value")
                    html += ' ' + key + '=' + value;
            });
            html += '>' + data[i].Value + '</option>';
        });
        html += '</select>';
        return html;
    },
    FillDropdown: function (data,dropdown) {
        var html = "";
        $.each(data, function (i) {
            html += '<option value=' + data[i].Key;
            //Add Additional Params
            $.each(data[i], function (key, value) {
                if (key !== "Key" && key !== "Value")
                    html += ' ' + key + '=' + value;
            });
            html += '>' + data[i].Value + '</option>';
        });
        html += '';
        $(dropdown).append(html);
    }
}