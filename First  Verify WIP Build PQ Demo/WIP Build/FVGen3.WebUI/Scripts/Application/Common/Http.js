﻿
function Post(url, params, callback) {
    $.post(url, params,
        function (results) {
            callback(results);
        }).fail(function () {
            console.log("error");
        }, "application/json");
    //$.ajax({
    //    url: url,
    //    type: "POST",
    //    data: params,
    //    //traditional : true,
    //    success: function(result) {
    //        callback(result);
    //    },contentType: 'application/json; charset=utf-8',
    //    dataType: 'json'
    //});
}
function PostWithArray(url, params, callback) {
    //$.post(url, params,
    //    function (results) {
    //        callback(results);
    //    }).fail(function () {
    //        console.log("error");
    //    }, "application/json");
    $.ajax({
        url: url,
        type: "POST",
        data: JSON.stringify(params),
        //traditional : true,
        success: function(result) {
            callback(result);
        },contentType: 'application/json; charset=utf-8',
        dataType: 'text'
    });
}
var States= {
    GetStates:function(includeCanada,callback) {
        Post("../Common/GetUnitedStates", { includeCanada: includeCanada },callback);
    }
}
var EmailTemplates= {
    GetEmailTemplates:function(callback) {
        Post("../EmailTemplates/GetEmailTemplates", {}, callback);
    }
}