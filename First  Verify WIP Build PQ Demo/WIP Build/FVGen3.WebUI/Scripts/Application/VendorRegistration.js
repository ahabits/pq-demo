﻿var taxHint='',
VendorRegistration = {

    onInit:function(taxHintLoc){
        taxHint = taxHintLoc;
    },
    CountryTypeChange: function (type)//0-USA,1-NonUSA
    {
        //alert(type);
        //$("#TaxID").rules("remove", { regex: "^[a-zA-Z'.\\s]{1,40}$" })
        if (type == 1) {
            if ($("#TaxID").length) {
                $("#TaxID").rules("remove", "regex");
                $("#TaxID").attr("maxlength", "30");
                $("#TaxID").closest("div").removeAttr("poptext");
            }

            $("#stateForUSA").attr("name", "xx");
            $("#stateForNonUSA").attr("name", "State");
            $("#countryForUSA").attr("name", "xxx");
            $("#countryForNonUSA").attr("name", "Country");
            $("#stateForUSA").hide();
            $("#stateForNonUSA").show();
            $("#countryForUSA").hide();
            $("#countryForNonUSA").show();

            $("#PhoneNumber").unmask('(000) 000-0000');
            $("#FaxNumber").unmask('(000) 000-0000');
            $("#PhoneNumber").attr("maxlength", "30");
            $("#FaxNumber").attr("maxlength", "30");
            $("#MobileNumber").unmask('(000) 000-0000');
            $("#MobileNumber").attr("maxlength", "30");
        }

        else {
            if ($("#TaxID").length) {
                $("#TaxID").attr("maxlength", "11");
                $("#TaxID").closest("div").attr("poptext", taxHint);
                $("#TaxID").rules("add", {
                    regex: "[0-9]{3}[-][0-9]{2}[-][0-9]{4}|[0-9]{2}[-][0-9]{7}", messages:
                                    { regex: "Invalid TaxId" }
                });
            }

            $("#stateForUSA").attr("name", "State");
            $("#stateForNonUSA").attr("name", "xx");
            $("#countryForUSA").attr("name", "Country");
            $("#countryForNonUSA").attr("name", "xxx");
            $("#stateForNonUSA").hide();
            $("#stateForUSA").show();
            $("#countryForNonUSA").hide();
            $("#countryForUSA").show();

            $("#PhoneNumber").mask('(000) 000-0000');
            $("#FaxNumber").mask('(000) 000-0000');
            $("#PhoneNumber").attr("maxlength", "14");
            $("#FaxNumber").attr("maxlength", "14");
            $("#MobileNumber").mask('(000) 000-0000');
            $("#MobileNumber").attr("maxlength", "14");
        }
    },

    GetCountryStates: function (clientId, includeCanada, ddlSelector)
    {
        var country = $("#Country").val();
        //alert(ddlSelector);
        $.ajax({
            type: 'Get',
            url: '../Vendor/GetStateSelectListItemsJson',
            data: { clientID: clientId, includeCanada: includeCanada, country: country.replace(/'/g, "''") },
            success: function (data) {
                //ddlSelector.attr("style", "height:200px;width:300px;");
                ddlSelector.empty();

                for (var i = 0; i < data.length; i++) {
                    var datasite = data[i].Text;
                    newOption = $("<option value='" + data[i].Value + "'>" + datasite + "</option>");
                    ddlSelector.append(newOption);
                }
            },
            error: function () {
                alert("Unable to fetch state(s).Please try again. ");
            }
        });
    },
    GetCountryLanguages: function (ddlSelector) {
        //alert("hai");
        var country = $("#Country").val();

        $.ajax({
            type: 'Get',
            url: '../AdminVendors/GetGetCountryLanguages',
            data: { country: country },
            success: function (languages) {

                ddlSelector.empty();

                for (var i = 0; i < languages.length; i++) {
                    //var datasite = data[i].Text;
                    newOption = $("<option value='" + languages[i].Value + "'>" + languages[i].Key + "</option>");
                    ddlSelector.append(newOption);
                }
            },
            error: function () {
                alert("Unable to fetch state(s).Please try again. ");
            }
        });
    }
}