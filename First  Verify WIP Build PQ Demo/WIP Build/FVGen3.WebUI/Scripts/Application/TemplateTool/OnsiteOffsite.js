﻿function Sineprocheckindata() {
   // Tabs.click(caller);
    HideAllComponents();
    $("#budiv").hide();
    $("#locdiv").hide();
    $("#Sineprositesdiv").show();
    $(".grid").show();
    $(".grid").kendoGrid({
        toolbar: ["excel"],
        excel: {
            allPages: true,
            fileName: "Sine Pro Checkin.xlsx",
            //avoidLinks: true
        },
        //excelExport: exportGridWithTemplatesContent,
        dataSource: {
            type: "Get",

            transport: {
                read: "../OnsiteOffsite/SineproData",

            },
            schema: {
                model: {
                    fields: {
                        CompanyName: { type: "string" },
                        VisitorName: {
                            type: "string"
                        },
                        Checkin: { type: "string" },
                        SiteName: { type: "string" },
                        CheckinUser: {
                            type: "string"
                        }
                    }
                }
            },

            pageSize: 20,


        },



        sortable: false,
        pageable: true,
        columns: [{

            field: "VisitorName",
            title: "Visitor Name",
            filterable: {
                cell: {
                    operator: "contains",
                    suggestionOperator: "contains"
                },

            }
        }, {

            // template: "#= kendo.toString(kendo.parseDate(Checkin), 'M/d/yyyy h:mm tt') #",
            title: "Last Checkin",
            filterable: false,
            field: "Checkin"

        },
        {

            // template: "#= kendo.toString(kendo.parseDate(Checkin), 'M/d/yyyy h:mm tt') #",
           
            //filterable: false,
            field: "SiteId",
            hidden: true,

        },
        {

            field: "CompanyName",
            title: "Company Name",
            sortable: { allowUnsort: false },
        }, {

            field: "SiteName",
            title: "Site Name",
            sortable: { allowUnsort: false },

        }, {

            field: "CheckinUser",
            title: "Security Name Checkin",
            sortable: { allowUnsort: false },
        }
        ]

    });



}
function Getsineprodate() { 
    debugger;
   
    var dataSource = $(".grid").data("kendoGrid").dataSource;
      var filters = dataSource.filter({ field: "SiteId", operator: "contains", value: $("#SineProSiteId option:selected").val() });
        var allData = dataSource.data();
        var query = new kendo.data.Query(allData);
        var data = query.filter(filters).data;
   
}

$('#filter').on('input', function (e) {

    var grid = $('.grid').data('kendoGrid');
    var columns = grid.columns;

    var filter = { logic: 'or', filters: [] };
    columns.forEach(function (x) {
        if (x.field) {
            var source = grid.dataSource.options.schema.model;
            var type = grid.dataSource.options.schema.model.fields[x.field].type;
            if (type == 'string') {
                filter.filters.push({
                    field: x.field,
                    operator: 'contains',
                    value: e.target.value
                })
            }
            else if (type == 'number') {
                if (isNumeric(e.target.value)) {
                    filter.filters.push({
                        field: x.field,
                        operator: 'eq',
                        value: e.target.value
                    });
                }

            } else if (type == 'date') {
                var data = grid.dataSource.data();
                for (var i = 0; i < data.length ; i++) {
                    var dateStr = kendo.format(x.format, data[i][x.field]);
                    // change to includes() if you wish to filter that way https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/includes
                    if (dateStr.startsWith(e.target.value)) {
                        filter.filters.push({
                            field: x.field,
                            operator: 'eq',
                            value: data[i][x.field]
                        })
                    }
                }
            } else if (type == 'boolean' && getBoolean(e.target.value) !== null) {
                var bool = getBoolean(e.target.value);
                filter.filters.push({
                    field: x.field,
                    operator: 'eq',
                    value: bool
                });
            }
        }

    });
    grid.dataSource.filter(filter);
});