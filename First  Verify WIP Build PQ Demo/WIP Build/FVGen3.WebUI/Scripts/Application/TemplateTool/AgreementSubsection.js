﻿
var AgreementSubSection = {
    ConfigureAgreementMails: function (SubsecId) {
        PopupModel.reset();
        PopupModel.setHeader("Email Configuration");
        PopupModel.setData("Loading..");
        EmailTemplates.GetEmailTemplates(function (data) {
            var htmlTemplate = $(AgreementSubSection.AgreementConfigTemplate(SubsecId));
            htmlTemplate.find("#EmailTemplateRow").html($(Controls.Dropdown(data)).attr("id", "EmailTemplateId"));
            PopupModel.setData(htmlTemplate);

            AgreementSubSection.GetEmailConfiguration(SubsecId, function (data) {
                if (data.IsAdmin)
                    PopupModel.getBody().find("#SendNotificationToFirstVerify").attr("checked", true);
                if (data.IsClient)
                    PopupModel.getBody().find("#SendNotificationToClient").attr("checked", true);
                PopupModel.getBody().find("#EmailTemplateId").val(data.EmailTemplateId);
            });

        });
        
        PopupModel.show();
    },
    ClientSignatureConfig: function (subSectionId) {
        PopupModel.reset();
        PopupModel.setHeader("Edit Signature");
        PopupModel.setData("Loading..");
        AgreementSubSection.GetClientSign(subSectionId,
            function(data) {
                PopupModel.setData($(AgreementSubSection.ClientConfigurationTemplate(data, subSectionId)));
            });
        PopupModel.show();
    },
    ClientConfigurationTemplate: function (data,subSectionId) {

        var html =
            '<div class="rowPopUp" style="color:red;">If signature and title was not filled out then this agreement section will act as a counter signature otherwise it will treat as a Auto Signature.</div><div class="rowPopUp" id="defaultClientSign">' +
                '<table><tbody><tr><td><label class="enableStarPopUp">Client Signed:</label>' +
                '</td><td><input id="Signed" name="Signed" type="text" value="' + data.Sign + '">' +
                '<span class="field-validation-valid" data-valmsg-for="Signed" data-valmsg-replace="true">' +
                '</span></td></tr><tr><td><label class="enableStarPopUp">Client Title:</label></td><td>' +
                '<input id="Title" name="Title" type="text" value="' + data.Title + '">' +
                '<span class="field-validation-valid" data-valmsg-for="Title" data-valmsg-replace="true"></span></td></tr>' +
                '</tbody></table></div><div class="pageFooterContent"> <div class="pageFooterButtons"><table><tbody><tr><td>' +
                '<button type="button" btntype="Update" class="PageFooter_Button" onclick="AgreementSubSection.UpdateSign(' + subSectionId + ')">Save</button></td><td btntype="Update">or</td><td><div class="pageFooterCancelButton"><a href="#!" onclick="PopupModel.closeModel();">Cancel</a></div></td></tr>	</tbody></table> </div></div>';
        //var html = '<div class="rowPopUp"><input type="text" value="' + data.Sign + '"><input type="text" value="' + data.Title + '"></div>';
        return html;
    },
    UpdateSign:function(subSectionId) {
        
        var sign = $("#Signed").val();
        var title = $("#Title").val();

        Post("../TemplateTool/UpdateClientSign", { Title: title, Sign: sign, SubSectionId: subSectionId }, function (data) {
            if (data === "Success")
                PopupModel.closeModel();
        });
    },
    AgreementConfigTemplate: function (SubsecId) {
        var html =
            ' <div class="rowPopUp" id="AdditionalClients"> ' +
                '<div class="rowCheckBox">' +
                    '<table><tr><td  valign="top"rowspan="2"><label class="">Send Notifications To</label></td>' +
                        '<td><input type="checkbox" name="SendNotificationToClient" id="SendNotificationToClient"/></td>' +
                        '<td><label for="SendNotificationToClient">Client</label></td>' +
                        '<td><input type="checkbox" name="SendNotificationToVendor" id="SendNotificationToVendor" value="true" disabled="disabled" checked="checked"/></td>' +
                        '<td><label for="SendNotificationToVendor">Vendor</label></td></tr>' +
                        '<tr><td><input type="checkbox" name="SendNotificationToFirstVerify" id="SendNotificationToFirstVerify"/></td>' +
                        '<td><label for="SendNotificationToFirstVerify" >First, Verify</label></td><td></td>' +
                    '</tr></table>' +
                '</div><div class="rowIST" id="EmailTemplatesList"> <label>Email Template</label><div id="EmailTemplateRow"></div></div> ' +
                '<div ><div class="pageFooterContent">' +
                '<div class="PageFooter_Buttons">' +
                '<table><tr><td><button type="button" class="PageFooter_Button" btnType="Insert" onclick="AgreementSubSection.EmailConfiguration(' + SubsecId + ')">Save</button></td></tr></table></div> </div></div>' +
            '</div>';
        return html;
    },
    EmailConfiguration: function (SubsecId) {
        var model = PopupModel.getBody();
        Post("../TemplateTool/AgreementEmailConfiguration",
        {
            SubSectionId: SubsecId,
            IsVendor: model.find("#SendNotificationToVendor").is(":checked"),
            IsClient: model.find("#SendNotificationToClient").is(":checked"),
            IsAdmin: model.find("#SendNotificationToFirstVerify").is(":checked"),
            EmailTemplateId: model.find("#EmailTemplateId").val()
        }, function (data) {
            alert(data);
            if (data === "Configuration created successfully")
                PopupModel.closeModel();
        });
    },
    GetEmailConfiguration:function(SubSectionId,callback) {
        Post("../TemplateTool/GetAgreementConfiguration",{subSectionId:SubSectionId},callback);
    },
    GetClientSign:function(subSectionId,callback) {
        Post("../TemplateTool/GetClientSignData", { subSectionId: subSectionId }, callback);
    }
}


