﻿//$(function () {
//    
//    if ($("#IsERI").is(":checked")) {
//        $("#dropDownOne").hide();
//    } else {
//        $("#ERIClients").hide();
//    }
//});
var Template = {
    //ERIChange:function(obj) {
    //    if ($(obj).is(":checked")) {
    //        $("#dropDownOne").hide();
    //        $("#ERIClients").show();
    //    } else {
    //        $("#ERIClients").hide();
    //        $("#dropDownOne").show();
    //    }
    //},

    getSubClients:function()
    {
        var clientId = $("#ClientId").val();

        $.ajax({
            type: 'Get',
            url: '../TemplateTool/GetSubClients',
            data: { orgID: clientId },
            success: function (data) {
                
                $("#ERIClients").empty();
                for (var i = 0; i < data.length; i++) {
                    //var subClient = data[i].Key;
                    newOption = $('<option value=' + data[i].Key + '>' + data[i].Value + '</option>');
                    $("#ERIClients").append(newOption);
                }
                if(data.length > 0)
                {
                    $("#SubClients").show();
                    $("#HideClientsFromUsers").show();
                }
                else {
                    $("#SubClients").hide();
                    $("#HideClientsFromUsers").hide();
                }
            },
            error: function () {
                alert("Unable to fetch sub client(s).Please try again. ");
            }
        });
    }
}