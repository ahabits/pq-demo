﻿var Prequalification = {
    GetPQStatues: function (callback) {
        Post("../Prequalification/getPQStatuses",
            {}, function (result) {
                callback(result);
            });
    },
    CreateFlag: function (qid, color) {
        if (color === "Multiple") {
            //$("#id_" + qid).find('.ctrlDiv label').append('<i class="fa fa-align-justify"  style="color:#626862;"></i>');
            $("#id_" + qid).find('.ctrlDiv label').append('<img src="../images/mixed.jpg">');
        }
        else if (color != null && color !== "") {
            $("#id_" + qid).find('.ctrlDiv label').append('<i class="fa fa-exclamation-circle"  style=" color:' + color + '""></i>');
        }

    },
    CreateDependentFlag: function (pqId, questionId) {
        PostWithArray("../Prequalification/GetQuestionColor",
            { PqId: pqId, QuestionId: JSON.parse(questionId) },
            function (result) {
                debugger;
                var data = JSON.parse(result);
                $.each(data,
                    function(index, data) {
                        Prequalification.CreateFlag(data.Key, data.Value);
                    });
            });
    },
    GetParentColor: function (PqId, QuestionId) {
        PostWithArray("../Prequalification/GetParentColor",
            { PqId: PqId, QuestionId: JSON.parse(QuestionId) },
            function (result) {

                var data = JSON.parse(result);
                $.each(data,
                    function (index, data) {
                        Prequalification.CreateFlag(data.Key, data.Value);
                    });
            });
    }
}