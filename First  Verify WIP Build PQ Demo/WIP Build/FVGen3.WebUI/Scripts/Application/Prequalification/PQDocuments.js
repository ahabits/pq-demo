﻿
var PQDocuments = {
    
    DownloadDoc : function(DocumentId, DocumentName)   // Added Parameters - Kiran 11/19/2013  //Suma  on 4/12/2013 Removed parameter templateId
    {
        window.open('../Companies/DownloadPQDoc?idAsFileName='+DocumentId +'&userEnteredFilename='+ DocumentName+')');
    },

    DeletePQDocument: function (DocumentId) {
        
        Post("../Prequalification/DeletePQDocument?DocumentId=" + DocumentId,
           {  },
           function (data) {
               $('#grid').data('kendoGrid').dataSource.read(); 
           });
    },
    
    kgrid : function(pqId){
        $("#grid").kendoGrid({

            dataSource:{
                
                transport: {

                    read: {
                        dataType: "json",
                        type: 'Post',
                        url: "../Prequalification/PQDocumentsPartialView",
                        data: { pqid: pqId }
                    },


                },
                batch: true,
                schema: {



                    model: {
                        fields: {
                            UploadedDate: { type: "date" },
                            DocumentId: { type: "string" },
                            DocumentName: { type: "string" },


                            UploadedBy: { type: "string" },

                        }
                    }
                },
                pageSize: 50,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true
            },

            height: 700,

            pageable: true,
            columns: [
                {
                    field: "DocumentName",
                    title: "Document Name",
                    template: '<a href="javascript:void(0)" onclick=\'PQDocuments.DownloadDoc("#=DocumentId#","#=DocumentName#")\'>#=DocumentName#</a>'
                },
                {
                    field: "UploadedBy",
                    title: "Uploaded By",
                },

                {
                    field: "UploadedDate",
                    title: "Uploaded On",
                    format: "{0:MM/dd/yyyy}",
                },
                {
                    field: "",
                    title: " ",
                    template: '<img src="/images/DelButton.png" style="cursor: pointer;" onclick=\'PQDocuments.DeletePQDocument(\"#=DocumentId#")\'/>'
                }
            ]        
        });
    }
}

