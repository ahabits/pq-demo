﻿$(function () {
   //var Invite= $("#languageSetup").kendoWindow({
   //     width: "800px",
   //     height: "350px",
   //     top: "300px",
   //     visible: false,
   //     actions: [
                    
   //               "Close"
   //     ],
   // }).data("kendoWindow");
});
function timeOutHandler(min) {
    setInterval(function () {
        Model.show();
    }, 60000 * min);
}
var TemplateSectionsList = {

    ShowLogs: function (DocumentId, DocumentName) {
        PopupModel.setData("Loading..");
        PopupModel.setHeader(DocumentName);
        PopupModel.show();
        TemplateSectionsList.GetLogs(DocumentId).then(function (response) {
            var dialog = $("#popModelDiv").kendoWindow({
                width: "1200px",
                height:"500px",
                title: DocumentName,
                visible: false,
                actions: [
                    
                    "Close"
                ],
                close: function (e) {
                    $(this.element).empty();
                }
            }).data("kendoWindow");
            dialog.center().open();
           
            var data = response;
            
            var template = '<table style="width:100%"><tr><th width="25%" align="left">Document Name</th><th width="20%" align="left">Document Type</th><th width="8%" align="left">Status</th><th width="15%" align="left">Expiration Date</th><th width="15%" align="left">Changed By</th><th width="10%" align="left">Changed Date</th><th width="10%" align="left">Activity</th></tr>';
         
            if (response.length == 0)
                template += '<tr><td colspan=3 class="commentsItalic"><center>No records found</center></td></tr>'
            else {
                $.each(response, function (index, data) {
                    template += '<tr><td>' + data.DocumentName + '</td>';
                    template += '<td>' + data.DocumentType + '</td>';
                    template += '<td>' + data.Status + '</td>';
                    template += '<td>' + data.ExpirationDate + '</td>';
                    template += '<td>' + data.ModifiedBy + '</td>';
                    template += '<td>' + moment(data.ModifiedOn).format('MM-DD-YYYY hh:mm a') + '</td>'
                    template += '<td>' + data.ModificationType + '</td>'
                     '</tr>';
                });
            }
            template += '</table>'
            dialog.content(template);
            //alert(response);

            //PopupModel.setHeader(templateName);
            //PopupModel.setData(template);
        });
    },
    GetLogs: function (DocumentId) {
        //Post("../Prequalification/DeletePQDocument?DocumentId=" + DocumentId,
        // {},
        // function (data) {
        //     $('#grid').data('kendoGrid').dataSource.read();
        // });
        return $.get("/Prequalification/GetDocumentLog?DocumentId=" + DocumentId)
        .then(function (response) {
            return response;
        });
    },
    UnlockClientDocs: function (pqId) {
        
        Post("../Prequalification/UnlockClientDocuments",
          { pqId: pqId },
          function (result) {
              if (result === "Success")
                  window.location.reload(true);
              else alert("Unable to process the request.Please try again.");
          });
    },
    StartAutoSave: function () {
        Model.add('<button type="button" class="PageFooter_Button" onclick="submitForm(1);Model.hide()"> Save </button>');
        timeOutHandler(10);
    },
    AgreementClick: function (pqid, subsectionid) {
        window.open("../Prequalification/AgreementEmailCompose?pqId=" + pqid + "&subSectionId=" + subsectionid,
            "popupWindow",
            "width=600, height=400, scrollbars=yes");

    },
    AddOrEditAddendum: function (pqSubSectionId, pqid, subsectionid) {
        window.open("../Prequalification/AddOrEditAddendum?pqSubSectionId=" +
            pqSubSectionId +
            "&pqId=" +
            pqid +
            "&subSectionId=" +
            subsectionid,
            "popupWindow",
            "width=600, height=400, scrollbars=yes");
        //alert("ASDFADSFAS");
    },

    ToggleAddendum: function (pqSubSectionId) {
        Post("../Prequalification/ToggleAddendumVendorDisply",
            { pqSubSectionId: pqSubSectionId },
            function (result) {
                if (result === "Ok")
                    window.location.reload(true);
                else alert("Unable to process the request.Please try again.");
            });

        //alert("ASDFADSFAS");
    },

    SubSectionInterimSave: function (subSecId, prequalificationId) {

        if (TemplateSectionsList.ValidateSubSection(subSecId)) {

            var form = [];

            $("#subid_" + subSecId).find('input,select,textarea').each(function () {
                var val = TemplateSectionsList.GetInputVal($(this));

                if (val != null) {
                    var a = {};
                    var QuestionColumnId = $(this).attr('questioncolumnid');
                    //var val = $(this).val();
                    var InsertionType = $(this).attr('InsertionType');
                    var QuestionReferenceId = $(this).attr('questionreferenceid');
                    var controlType = $(this).attr('type');
                    //alert(controlType);

                    a.QuestionColumnId = QuestionColumnId;
                    a.UserInput = val;
                    a.InsertionType = InsertionType;
                    a.QuestionReferenceId = QuestionReferenceId;
                    //alert(a);
                    if (controlType != "button")
                        form.push(a);
                }
            });

            
            $(".spinnerOverlay").show();
            $('.importSubmitSpinner').show();
            PostWithArray("../Prequalification/SubSectionInterimsave",
            { userInputs: form, prequalId: prequalificationId, subSectionId: subSecId },
            function (result) {

                
                $('.spinnerOverlay').hide();
                $('.importSubmitSpinner').hide();
                if (result === "Ok") {
                    alert("Notification sent to FV");
                    window.location.reload(true);
                }
                else alert("Unable to process the request.Please try again.");
            });
        }
    },
    GetInputVal: function (obj) {
        
        if ($(obj).attr('type') != "button" && $(obj).attr('type') != "radio" && $(obj).attr('type') != "checkbox") //For All Required Fields except radio button
        {
            return $(obj).val();
        }
        else if ($(obj).attr('type') == "radio" && $(obj).is(":checked")) {
            return $(obj).val();
        }
        else if ($(obj).attr('type') == "checkbox") {
            return $(obj).is(':checked');
        }
        return null;
    },

    ValidateSubSection: function (subSecId) {
        var isValid = true;
        $("#subid_" + subSecId).find('input,select,textarea').each(function () {
            if ($(this).is(":visible") && !$(this).valid()) {
                isValid = false;
            }
        });
        if (!isValid) {
            var inp = $("#subid_" + subSecId).find('.input-validation-error:first').get(0);

            if (inp) {
                inp.focus();
            }
        }
        return isValid;
    },
    ERIRegionsChange: function (pqid) {
        var regions = $("#ERIRegion").val().join();
        
        Post("../Prequalification/GetClientsByRegion",
           { pqId: pqid, region: regions },
           function (result) {
               var options = "";
               $.each(result, function (key, item) {
                   options = options + "<option value='" + item.Key + "'>" + item.Value + "</option>";
               });
               $("#ERIClient").html(options);
           });
    }
} 