﻿var currentObjtoPaste;
jQuery(document).ready(function ($) {
    
    $('input').keydown(function (event) {
        
        if (event.keyCode == 13) {
           
            event.preventDefault();
            return false;
        }
    });
    $('input').keyup(function (e) {
        if (event.keyCode == 187) {
            if ($(this).hasClass("calcField")) {
                Model.show();
                $("#calc").focus();
                currentObjtoPaste = $(this);
            }
        }
        if (event.keyCode == 13) {
            if ($(this).attr("id") == "calc") {
                doneToPaste();
                return true;
            }
        }
        if (e.which == 39) { // right arrow
            $(this).closest('td').next().find('input').focus();

        } else if (e.which == 37) { // left arrow
            $(this).closest('td').prev().find('input').focus();

        } else if (e.which == 40 || e.which == 13) { // down arrow
            var currentRow = $(this).closest('tr').next().find('td:eq(' + $(this).closest('td').index() + ')').find('input');
            if (currentRow.is(":disabled"))
                currentRow.closest('tr').next().find('td:eq(' + $(this).closest('td').index() + ')').find('input').focus();
            else
                currentRow.focus();
            e.preventDefault();
            return false;

        } else if (e.which == 38) { // up arrow
            var currentRow = $(this).closest('tr').prev().find('td:eq(' + $(this).closest('td').index() + ')').find('input');

            if (currentRow.is(":disabled"))
                currentRow.closest('tr').prev().find('td:eq(' + $(this).closest('td').index() + ')').find('input').focus();
            else
                currentRow.focus();
        }
    });
    var maxRowCount = 0;
    $(window).load(function () {
        // this code will run after all other $(document).ready() scripts
        // have completely finished, AND all page elements are fully loaded
        for (var i = 0; i < 4; i++) {
            TotalsForAssets(i);
            TotalsForIncomeStatement(i);
            TotalsForLiabilities(i);
        }
        changeOrLoadRed();
    });
    $('#container').change(function () {
        changeOrLoadRed();
    });

    $('table tr').each(function () {
        maxRowCount = Math.max(maxRowCount, $(this).children('td').length);
    });

    var cellCounter = 1;

    for (var columnIndex = 0; columnIndex < maxRowCount; ++columnIndex) {
        $('table tr').each(function () {
            var cellForCurrentColumn = $(this)
            .children('td')
            .eq(columnIndex)
            .find('input');

            if (cellForCurrentColumn != null) {
                cellForCurrentColumn.attr('tabindex', cellCounter++);

            }
        });
    }


    $(".noDecDlrFrmt").autoNumeric('init', { maximumValue: '999999999999', minimumValue: '-999999999999', decimalPlacesOverride: 0, currencySymbol: '$', unformatOnSubmit: true, formatOnPageLoad: true });
    $(".oneDecNumFrmt").autoNumeric('init', { maximumValue: '999999999999.9', minimumValue: '-999999999999.9', decimalPlacesOverride: 1, unformatOnSubmit: true, formatOnPageLoad: true });
    $(".twoDecNumFrmt").autoNumeric('init', { maximumValue: '999999999999.99', minimumValue: '-999999999999.99', decimalPlacesOverride: 2, unformatOnSubmit: true, formatOnPageLoad: true });
    $(".noDecNoDlrFrmt").autoNumeric('init', { maximumValue: '999999999999', minimumValue: '-999999999999', decimalPlacesOverride: 0, unformatOnSubmit: true, formatOnPageLoad: true });
    $(".oneDecPerFrmt").autoNumeric('init', { maximumValue: '999999999999.9', minimumValue: '-999999999999.9', decimalPlacesOverride: 1, currencySymbolPlacement: 's', currencySymbol: '%', unformatOnSubmit: true, formatOnPageLoad: true });
});




//Function for Assets
function TotalsForAssets(i) {
    

    var assetsArray = [$('#CashAndCashEquivalents_' + i).val(), $('#Investments_' + i).val(),
            $('#ReceivablesCurrent_' + i).val(), $('#ReceivableRetainage_' + i).val(),
            $('#CostsAndEstimatedEarningsInExcessOfBillingsOnUncompletedContracts_' + i).val(),
            $('#RefundableIncomeTaxes_' + i).val(), $('#NoteReceivable_' + i).val(),
            $('#OtherCurrentAssets_' + i).val(), $('#NetPropertyAndEquipment_' + i).val(),
            $('#OtherAssets_' + i).val()];

    for (k = 0; k < assetsArray.length; k++) {
        if (assetsArray[k] == null)
            assetsArray[k] = "0";
        else
            assetsArray[k] = assetsArray[k].replace(/,/g, '').replace("$", "");

        if (assetsArray[k] == "")
            assetsArray[k] = "0";
    }

    //Calculate Total current assets 
    var totalCurAssets = 0;
    var totalAssets = 0;

    for (var j = 0; j < assetsArray.length; j++) {
        if (j < assetsArray.length - 2) {

            totalCurAssets += Number(assetsArray[j]);

            totalAssets = totalCurAssets;
        }
        else {
            totalAssets += Number(assetsArray[j]);
        }
    }
    totalCurAssets = totalCurAssets.toString();
    totalAssets = totalAssets.toString();
    $('#TotalCurrentAssets_' + i).val(totalCurAssets.replace(/\B(?=(\d{3})+(?!\d))/g, ","));
    $('#TotalAssets_' + i).val(totalAssets.replace(/\B(?=(\d{3})+(?!\d))/g, ","));

}
//function for Liabilities and Equity
function TotalsForLiabilities(i) {
    

    var arrayList1 = [$('#LinesOfCreditShortTermBorrowings_' + i).val(),
          $('#InstallmentNotesPayableDueWithinOneYear_' + i).val(), $('#AccountsPayable_' + i).val(),
          $('#RetainagePayable_' + i).val(), $('#AccruedExpenses_' + i).val(),
          $('#BillingsInExcessOfCostsAndEstimatedEarningsOnUncompletedContracts_' + i).val(),
          $('#CurrentIncomeTaxesPayable_' + i).val(), $('#DeferredIncomeTaxesPayableCurrent_' + i).val(),
          $('#NotesPayableDueAfterOneYear_' + i).val(), $('#EquipmentLinesOfCredit_' + i).val(),
          $('#OtherLongTermLiabilities_' + i).val(), $('#DeferredIncomeTaxesPayableLongTerm_' + i).val()];
    for (k = 0; k < arrayList1.length; k++) {
        if (arrayList1[k] == null)
            arrayList1[k] = "0";
        else
            arrayList1[k] = arrayList1[k].replace(/,/g, '');
        if (arrayList1[k] == "")
            arrayList1[k] = "0";
    }

    //Calculate Total Current Liabilities
    var totalCurLia = 0;
    for (j = 0; j < 8; j++) {
        //var eachTCL = parseInt(arrayList1[j]);
        totalCurLia += Number(arrayList1[j]);
    }
    $('#TotalCurrentLiabilities_' + i).val(totalCurLia.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));

    //Calculate Long Term Liabilities
    var totalLngTrmLia = 0;
    for (j = 8; j < 12; j++) {
        var eachTCL = parseInt(arrayList1[j]);
        totalLngTrmLia = totalLngTrmLia + eachTCL;
    }
    $('#TotalLongTermLiabilities_' + i).val(totalLngTrmLia);

    //Calculate Total Liabilities and Stockholders Equity
    var totStkHldEqui = $('#TotalStockholdersEquity_' + i).val();
    if (totStkHldEqui == null || totStkHldEqui == "") {
        totStkHldEqui = "0";
    }
    else {
        totStkHldEqui = parseInt(totStkHldEqui.replace(/,/g, ''));
    }
   
    var totLiaStcEqu = totalCurLia + totalLngTrmLia + parseInt(totStkHldEqui);
    $('#TotalLiabilitiesAndStockholdersEquity_' + i).val(totLiaStcEqu.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));

}

//function for Income Statement
function TotalsForIncomeStatement(i) {
    var conRev = $('#ConstructionRevenues_' + i).val();

    var conDirCst = $('#ConstructionDirectCosts_' + i).val();
    if (conRev == null || conRev == "") {
        conRev = 0;
    } else
        conRev = parseInt(conRev.replace(/[^a-z0-9\s]/gi, ''));


   
    if (conDirCst == null || conDirCst == "") {
        conDirCst = "0";
    } else
        conDirCst = parseInt(conDirCst.replace(/,/g, ''));


    var conGrsPrf = parseInt(conRev) - parseInt(conDirCst);
    
    //Math.abs()    use it to remove sign in the view
    $('#ConstructionGrossProfit_' + i).val(conGrsPrf.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
    
    if (conGrsPrf < 0) {
        $('#ConstructionGrossProfit_' + i).addClass("tdred");
    }
    else {
        $('#ConstructionGrossProfit_' + i).removeClass("tdred");
    }
    //To Calculate Operating Income
    var nonConGrsPrf = $('#NonConstructionGrossProfitLoss_' + i).val();
    var genAdmnExp = $('#GeneralAndAdministrativeExpenses_' + i).val();
    if (nonConGrsPrf == null || nonConGrsPrf == "") {
        nonConGrsPrf = 0;
    } else
        nonConGrsPrf = parseInt(nonConGrsPrf.replace(/,/g, ''));

    if (genAdmnExp == null || genAdmnExp == "") {
        genAdmnExp = 0;
    } else
        genAdmnExp = parseInt(genAdmnExp.replace(/,/g, ''));

    
    var opInc = parseInt(nonConGrsPrf) - parseInt(genAdmnExp) + parseInt(conGrsPrf);
    
    $('#OperatingIncome_' + i).val(opInc.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
    if (opInc < 0) {
        $('#OperatingIncome_' + i).addClass("tdred");
    }
    else {
        $('#OperatingIncome_' + i).removeClass("tdred");
    }


    //To Calculate Earnings Before Income Tax
    var othrInc = $('#OtherIncomeDeductions_' + i).val();
    if (othrInc == null || othrInc == "") {
        othrInc = 0;
    } else
        //othrInc = parseInt(othrInc.replace(/[^a-z0-9\s]/gi, ''));
        othrInc = parseInt(othrInc.replace(/,/g, ''));

   
    var ernBefIncTax = opInc + parseInt(othrInc);

    $('#EarningsBeforeIncomeTaxes_' + i).val(ernBefIncTax.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));

    if (ernBefIncTax < 0) {
        $('#EarningsBeforeIncomeTaxes_' + i).addClass("tdred");
    }
    else {
        $('#EarningsBeforeIncomeTaxes_' + i).removeClass("tdred");
    }


    //Calculate Net Income
    var incTaxExpBen = $('#IncomeTaxExpenseBenifit_' + i).val();
    if (incTaxExpBen == null || incTaxExpBen == "") {
        incTaxExpBen = 0;
    } else
        incTaxExpBen = parseInt(incTaxExpBen.replace(/,/g, ''));
    
    var netInc = ernBefIncTax - parseInt(incTaxExpBen);
    $('#NetIncome_' + i).val(netInc.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
    if (netInc < 0) {
        $('#NetIncome_' + i).addClass("tdred");
    }
    else {
        $('#NetIncome_' + i).removeClass("tdred");
    }


    //Calculate EBITDA
    var depAmr = $('#DepreciationAndAmortization_' + i).val();
    var intExp = $('#InterestExpense_' + i).val();
    if (depAmr == null || depAmr=="") {
        depAmr = 0;
    } else
        depAmr = parseInt(depAmr.replace(/,/g, ''));

   
    if (intExp == null || intExp == "") {
        intExp = 0;
    } else
        intExp = parseInt(intExp.replace(/,/g, ''));
   

    var ebidt = ernBefIncTax + parseInt(depAmr) + parseInt(intExp);


    $('#EBITDA_' + i).val(ebidt.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));

    if (ebidt < 0) {
        $('#EBITDA_' + i).addClass("tdred");
    }
    else {
        $('#EBITDA_' + i).removeClass("tdred");
    }

    //Calculate Backlog Gross Profit
    var totBcklg = $('#TotalBacklog_' + i).val();
    var bckLgGrsPrf = 0;
    if (totBcklg == null || totBcklg == "") {
        totBcklg = 0;
    } else
        totBcklg = parseInt(totBcklg.replace(/,/g, ''));
    

    if (conRev != 0) {
        bckLgGrsPrf = (conGrsPrf * parseInt(totBcklg) / conRev).toFixed(2);
    }
    $('#BacklogGrossProfit_' + i).val(bckLgGrsPrf.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
    if (bckLgGrsPrf < 0) {
        $('#BacklogGrossProfit_' + i).addClass("tdred");
    }
    else {
        $('#BacklogGrossProfit_' + i).removeClass("tdred");
    }

}
function changeOrLoadRed() {
    
    var IDs = [];
    $(".tdred").each(function () { IDs.push(this.id); });
    for (i = 0; i < IDs.length; i++) {
        var domVal = $('#' + IDs[i]).val();
        if (domVal.indexOf('-') > -1) {
            domVal = domVal.replace('-', '(');
            domVal = domVal.concat(')');
        }
        //else {
        //    domVal = domVal.replace('(', '');
        //    domVal = domVal.replace(')', '');
        //    domVal = '(' + domVal + ')';
        //}
        $('#' + IDs[i]).val(domVal);
    }
}