﻿var vendorInviteCurrentTab = 0;
var Dashboard = {
    TabClick: function (obj, tabNumber) {
        window.vendorInviteCurrentTab = tabNumber;
        Tabs.click(obj);
        if (tabNumber === 0) {
            Dashboard.HideInviteVendorTabs();
            $("#InviteVendorNewVendor").show();
        } else if (tabNumber === 1) {
            Dashboard.HideInviteVendorTabs();
            $("#InviteVendorExpiredVendor").show();
            if ($("#InviteVendorExpiredVendor").html() === "")
                getInvitedVendors(0, "");
        } else {
            Dashboard.HideInviteVendorTabs();
            $("#InviteVendorOutsideVendor").show();
            if ($("#InviteVendorOutsideVendor").html() === "")
                getInvitedVendors(0, "");
        }
    },
    HideInviteVendorTabs: function () {
        $("#InviteVendorNewVendor").hide(); 
        $("#InviteVendorExpiredVendor").hide();
        $("#InviteVendorOutsideVendor").hide();
    }
}
 