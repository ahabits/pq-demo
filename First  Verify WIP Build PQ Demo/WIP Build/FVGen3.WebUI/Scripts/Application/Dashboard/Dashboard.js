﻿var vendorInviteCurrentTab = 0;
var Dashboard = {
    DownloadData:function(type) {
        window.open('/AdminVendors/DownloadPrequalifications?type='+type, '_blank');
    },
    TabClick: function (obj, tabNumber) {
        window.vendorInviteCurrentTab = tabNumber;
        Tabs.click(obj);
        if (tabNumber === 0) {
            Dashboard.HideInviteVendorTabs();
            $("#InviteVendorNewVendor").show();
        }else if (tabNumber === 1) {
            Dashboard.HideInviteVendorTabs();
            $("#InviteVendorExpiredVendor").show();
           
            if($("#InviteVendorExpiredVendor").html()==="")
                getInvitedVendors(0, "");
        } else {
            Dashboard.HideInviteVendorTabs();
            $("#InviteVendorOutsideVendor").show();
            if ($("#InviteVendorOutsideVendor").html() === "")
                getInvitedVendors(0, "");
        }
    },
    HideInviteVendorTabs:function() {
        $("#InviteVendorNewVendor").hide();
        $("#InviteVendorExpiredVendor").hide();
        $("#InviteVendorOutsideVendor").hide();
    }
}
function showMoreRecentDocs() {
    $("#recentDocsMoreLink_Div_Id").hide();
    $(".moreDocuments").show();
    $("#MoreDocsDiv_Id").show();
}

function showOnlyThreeRecentDocs() {
    $("#MoreDocsDiv_Id").hide();
    $("#LessDocsDiv_Id").show();
    $("#recentDocsMoreLink_Div_Id").show();
    $(".moreDocuments").hide();
}

function showMoreStatusDescriptions(moreButton) {

    if ($(moreButton).text().indexOf("More") != -1) {
        $(".statusDescClass").show();
        $(moreButton).text("Less..");
    }
    else {
        $(moreButton).text("More..");
        $(".statusDescClass").hide();
    }
}
function showMoreNotifications(moreButton) {

    if ($(moreButton).text().indexOf("More") != -1) {
        $(".NotificationsClass").show();
        $(moreButton).text("Less..");
    }
    else {
        $(moreButton).text("More..");
        $(".NotificationsClass").hide();
    }
}

function showMorePrequals(moreButton) {

    if ($(moreButton).text().indexOf("More") != -1) {
        $(".prequalClass").show();
        $(moreButton).text("Less..");
    }
    else {
        $(moreButton).text("More..");
        $(".prequalClass").hide();
    }
}

function showLeftPanel(type, currentElement)//0-Doc 1-Notifications 2-Prequalifications Status 3-InviteVendor
{
    //To Hide All


    $("#PrequalificationsMainDiv").hide();
    $("#IncompleteMainDiv").hide();//Rajesh on 12/27/2013
    $("#ClientReviewMainDiv").hide(); // Kiran on 9/26/2014
    $("#RecentDocsMainDiv").hide();
    $("#PrequalificationStatusMainDiv").hide();
    $("#NotificationsMainDiv").hide();
    $("#VendorsInvitedMainDiv").hide(); // Kiran on 9/13/2014
    $("#RecentDocsFilter").hide();
    $("#SearchRecentDocs").hide();
    $("#PowerBiReport").hide();
    $("#report").hide();
    switch (type) {
        case 0: $("#RecentDocsMainDiv").show(); $("#SearchRecentDocs").show(); $("#RecentDocsFilter").show(); break;
        case 1:
            {
                $("#NotificationsMainDiv").show();
                //markAllNotificationsAsRead(currentElement);
                break;
            }
        case 2: $("#PrequalificationStatusMainDiv").show(); break;
        case 3: $("#VendorsInvitedMainDiv").show();//Rajesh on 08/09/2014
        case 4: {
            $("#report").show();
            $("#PowerBiReport").show();
        }
    }
}

function SearchVendorsInvites(obj) {
    var email = $(obj).closest(".inviteVendorPage").find("#searchinvitedvendorByEmail").val();
    getInvitedVendors(0, email);

}


//Rajesh on 08/09/2014
function getInvitedVendors(pageNumber, SearchEmail) {
    kendo.ui.progress($("#VendorsInvitedMainDiv"), true);
    //$(".spinnerOverlay").show();
    //$('.importSubmitSpinner').show();
    var currentTab = $("input[name='inviteTabs']:checked").val();
    if (!currentTab)
        currentTab = vendorInviteCurrentTab;
    currentTab = Number(currentTab);
    $.ajax({
        type: 'Post',
        url: 'InviteVendorsPartialView',
        data: '&pageNumber=' + pageNumber + '&SearchEmail=' + SearchEmail + '&invitationType=' + currentTab,
        success: function (data) {
            kendo.ui.progress($("#VendorsInvitedMainDiv"), false);

            if (currentTab === 0)
                $("#InviteVendorNewVendor").html(data);
            else if (currentTab === 1)
                $("#InviteVendorExpiredVendor").html(data);
            else
                $("#InviteVendorOutsideVendor").html(data);
            $(".spinnerOverlay").hide();
            $('.importSubmitSpinner').hide();
        },
        error: function () {
            kendo.ui.progress($("#VendorsInvitedMainDiv"), false);

            console.trace();
            $(".spinnerOverlay").hide();
            $('.importSubmitSpinner').hide();
        }
    });

}

function markAllNotificationsAsRead(currentElement) {
    $.ajax(
    {
        type: 'POST',
        url: 'markAllNotificationsAsRead',
        data: '',
        success: function (data) {

            if (data.indexOf("Session expired") >= 0) {
                window.location = "../SystemUsers/Login?lastLoginPage=" + "~/AdminVendors/AdminDashboard";
            }
            else if (data == "updateSuccess") {
                $(currentElement).closest("li").find("sup").remove();
            }
            else {
                alert(Resources.Common_unableToProcess);
            }
        },
        error: function () {
        }
    });
}
$(document).ready(function () {

    $(".AdminBG tr:odd").css("background", "#f6f6f6");
    $("#RecentDocsFilter").hide();
    $("#SearchRecentDocs").hide();
    getPrequalification(0, 0);
    getPrequalification(0, 1);
    getPrequalification(0, 2);
    $("#SearchRecentDocs").on("click", function (e) {
        GetRecentDocs();
    });
    $("#report").hide();
    $("#PowerBiReport").hide();
   // $("#Comments").kendoEditor();


});
var Invite=$("#languageSetup").kendoWindow({
    width: "800px",
    height:"350px",
    top: "300px",
    visible: false,
    actions: [
        "Minimize",
        "Maximize",
        "Close"
    ],
  //  open: onOpen
    //,
    //onchange:GetEmailData
})
function getNotifications(pageNumber) {
    $.ajax({
        type: 'Post',
        url: 'NotificationsPartialView',
        data: '&pageNumber=' + pageNumber,
        success: function (data) {
            //alert(data);
            $("#NotificationsMainDiv").html(data);
        },
        error: function () {
            console.trace();
        }
    });

}

function getparams() {
    var viewModel = kendo.observable({
        param: {
            filters: $('#RecentDocsFilter').val()
        }
    });
    return JSON.parse(JSON.stringify(viewModel.param));
}
 
function GetRecentDocs() {
    $("#RecentDocsMainDiv").kendoGrid({
        toolbar: ["excel"],
        excel: {
            allPages:true,
            fileName: "Recent Documents.xlsx",
            avoidLinks: true
        },
        dataSource: new kendo.data.DataSource({
            type: "get",

            transport: {
                type: 'post',
                read: {
                    type: 'post',
                    url: "../AdminVendors/GetRecentDocs",

                    data: function (options) {
                        return Object.assign(getparams(), options.data);
                    }

                },
            },

            schema: {
                data: "Data",
                total: "Total",
                model: {
                    fields: {
                        DocumentTypeName: { type: "string" },
                        ClientName: { type: "string" },
                        VendorName: { type: "string" },
                        // Expires:{type:"date"},
                        DateUploaded: { type: "date" },
                        VendorStatus: { type: "string" },
                        Email: { type: "email" },
                        UserName: { type: "string" },
                        TemplateID: { type: "Guid" },
                        TemplateSectionId: { type: "number" },
                        PreQualificationId: { type: "number" },
                        DocumentId: { type: "string" },
                        Initials: { type: "string" }
                    }
                }
            },
            pageSize: 30,
            serverPaging: true,
            serverFiltering: true,
            serverSorting: true
        }),

        sortable: true,
        resizable: true,
        pageable: true,
   
        columns: [

            {

                title: "Vendor Name",
                field:"VendorName",
                //width: "16%",
                template: '<a href="../Prequalification/TemplateSectionsList?TemplateID=#=TemplateID#&TemplateSectionID=#=TemplatesectionId#&PreQualificationId=#=PrequalificationId#&mode=#=0#">#=VendorName#</a>',
            },

             {
                 title: "Document Type Name",
                 field: "DocumentTypeName",
                 template: "#=DocumentTypeName#</br><span class=indicationText>#=VendorStatus#</span>",

             },
              {
                  title:"Status",
                  field: "VendorStatus",
                  hidden: true,

              },
             {
                 field: "ExpirationDate",
                 title: "Expiration Date",
                 //width: "18%",exportFormat: "MM/dd/yyyy h:mm:ss AM/PM",
                 //template: "#if(ExpirationDate === null){##}else{# #= kendo.toString(new Date(parseInt(ExpirationDate.substr(6))),'MM/dd/yyyy')#  #}#"
             },

            {
                field: "ClientName",
                title: "Client Name",


            },
            {
                title: "Uploaded Date",
                width: "18%", exportFormat: "MM/dd/yyyy h:mm:ss AM/PM",
                template: "#= kendo.toString(DateUploaded,'MM/dd/yyyy HH:MM tt')# by '<a href='mailto:#=Email#'>#=UserName#</a>",
                   
                filterable: false
            },
                
                
            {
                width: "10%",
                template: "#if(HasDelete){#<img src='../images/DelButton.png' onclick=\"deleteDocument('#= DocumentId #')\" '></img>#}#<span>#=Initials!= null ? Initials : '' #</span>"
            }
        ],
            

    });
    var exportFlag = false;
    $("#RecentDocsMainDiv").data("kendoGrid").bind("excelExport", function (e) {
        kendo.ui.progress(this.element, true);
           
        if (!exportFlag) {
            debugger;
               
            e.sender.showColumn(2);
            e.preventDefault();
            exportFlag = true;
            setTimeout(function () {
                e.sender.saveAsExcel();
            });
        } else {
            kendo.ui.progress(this.element, false);
               
            e.sender.hideColumn(2);
            exportFlag = false;
        }
    });
}

function getPrequalification(pageNumber, type) // type 0 - Submitted, 1 - Incomplete, 2 - Client Review
{
    $.ajax({
        type: 'Post',
        url: 'PrequalificationPartialView',
        data: '&pageNumber=' + pageNumber + '&type=' + type,
        success: function (data) {
            if (type == 1) {
                $("#IncompleteMainDiv").html(data);
            }
            else if (type == 0) {
                $("#PrequalificationsMainDiv").html(data);
            }
            else
                $("#ClientReviewMainDiv").html(data);
            // Ends<<<
        },
        error: function () {
            console.trace();
        }
    });

}

function ShowPreQualifications(type) 
{
    if (type == 1) {
        $("#PrequalificationsMainDiv").show();
        $("#IncompleteMainDiv").hide();
        $("#ClientReviewMainDiv").hide();
    }
    else if (type == 0) {
        $("#PrequalificationsMainDiv").hide();
        $("#IncompleteMainDiv").show();
        $("#ClientReviewMainDiv").hide();
    }
    else 
    {
        $("#PrequalificationsMainDiv").hide();
        $("#IncompleteMainDiv").hide();
        $("#ClientReviewMainDiv").show();
    }
}
    
function showPrequalification(pageNumber, obj) {
    $(".Prequalifications1").hide();
    $(".Prequalifications2").hide();
    $(".Prequalifications3").hide();

    $(".Prequalifications" + pageNumber).show();
    $(".prequalificationPages").css("background", "#dedede");
    $(obj).css("background", "#ffffff");
}

function showIncompletePrequalification(pageNumber, obj) {
    $(".IPrequalifications1").hide();
    $(".IPrequalifications2").hide();
    $(".IPrequalifications3").hide();

    $(".IPrequalifications" + pageNumber).show();
    $(".IPrequalificationsPages").css("background", "#dedede");
    $(obj).css("background", "#ffffff");
}

function showClientReviewPrequalification(pageNumber, obj) {
    $(".CRPrequalifications1").hide();
    $(".CRPrequalifications2").hide();
    $(".CRPrequalifications3").hide();

    $(".CRPrequalifications" + pageNumber).show();
    $(".CRPrequalificationsPages").css("background", "#dedede");
    $(obj).css("background", "#ffffff");
}


function deleteDocument(documentId) {
    var confirmDelete = confirm(Resources.Common_ConfirmBox_delete_message);
    if (confirmDelete) {
        $.ajax({
            type: 'Post',
            url: 'RemoveDocument',
            data: '&documentId=' + documentId,
            success: function (data) {
                   
                GetRecentDocs();
                // Ends<<<
            },
            error: function () {
                console.trace();
            }
        });
    }
}
   
function deleteVendorInvited(invitationid) {
    var confirmDelete = confirm(Resources.Common_ConfirmBox_delete_message);
    if (confirmDelete) {
        $.ajax({
            type: 'Post',
            url: 'RemoveVendorfromVendorsInvited',
            data: '&invitationId=' + invitationid,
            success: function (data) {
                getInvitedVendors('0','');
            },
            error: function () {
                console.trace();

            }
        });
    }
}
    
function showInvitedVendors(pageNumber, obj) {
    $(obj).closest(".inviteVendorPage").find(".InvitedVendors1").hide();
    $(obj).closest(".inviteVendorPage").find(".InvitedVendors2").hide();
    $(obj).closest(".inviteVendorPage").find(".InvitedVendors3").hide();
    $("#SearchRecentDocs").hide();
    $(obj).closest(".inviteVendorPage").find(".InvitedVendors" + pageNumber).show();
    $(obj).closest(".inviteVendorPage").find(".InvitedVendorsPages").css("background", "#dedede");
    $(obj).css("background", "#fff");
}
function onOpen() {
   resetData();
    GetInvitationData();
}
function Onclose() {

    Invite.data("kendoWindow").close();
}
var CurrentvendorInvitationId = '';
function OpeninvitationWindow(id, title) {
    debugger;
    CurrentvendorInvitationId = id;
    Invite.data("kendoWindow").title(title);
    Invite.data("kendoWindow").center().open();
    $("#Comments").val('');
    $('#InvitationId').val(id);
    $.post("/AdminVendors/GetInvitationData", { invitationid: id }, function (data) {
        debugger;
     
       
        $("#Comments").val(data);
   
    });
   
}

//function GetInvitationData() {
//    debugger;
//    var CurrentInviteId = CurrentvendorInvitationId;
//    //var LanguageID = $('#LanguageId').val();
//    $('#InvitationId').val(CurrentInviteId);
//    $.post("/AdminVendors/GetInvitationData", { invitationid: CurrentInviteId }, function (data) {
//        debugger;
        
//        $('#InvitationId').val(CurrentInviteId);
//        $("#Comments").data("kendoEditor").value(decodeEntities(data));
//    });

//}

function SaveEmailData() {
    debugger;
    var windowWidget = Invite.data("kendoWindow");
    kendo.ui.progress(windowWidget.element, true);
    var Form = $('#Form').serialize();
    var CurrentInvitationId = CurrentvendorInvitationId;
    var InvitationId = $('#InvitationId').val(CurrentInvitationId);
    
    var Comments = $("#Comments").val();
    if (Comments.length == 0) {
        kendo.ui.progress(windowWidget.element, false);
        alert("Please add comments");
        return false;
       
    }
        //  $("#Comments").val();
        //For Resend Invitation
         $.post("/AdminVendors/SendInvitation", Form, function (data) {
             debugger;
             if (data == "Success") {
                 kendo.ui.progress(windowWidget.element, false);
            alert("Invitation has been sent successfully");
                 Onclose();
         }

             kendo.ui.progress(windowWidget.element, false);

    });

        //  kendo.ui.progress(windowWidget.element, false);

}