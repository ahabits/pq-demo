﻿Emp = {
    GetVendors: function () {
        Vendor.GetAllVendors(function (result) {
            $.each(result, function (key, value) {
                $("#VendorId").append('<option value=' + value.Key + '>' + value.Value + '</option>');
            });
        });
    },
    AddEmployee: function () {
        debugger;
        var isValid = true;
        $("#target input").each(function () {
            debugger;
            var element = $(this);
            if (element.val() == "") {
                isValid = false;

                alert("Please fill all mandatory fields");
                return false;
            }
           
        });
        if ($('#LanguageId').val() == 0) {
            isValid = false;
            alert("Please fill all mandatory fields");
            return false;
        }
        if ($("#VndReg").prop('checked') == false) {
            isValid = false;
            alert("Please fill all mandatory fields");
            return false;
        }
        if (isValid) {

            Post("../SystemUsers/EmpRegistration", $('form').serialize()
          , function (result) {
             
              if (result == "Registration has been completed successfully") {
                  old_alert("Your registration is successful!" + "\n" + "Please login to start using your account.");
                   window.location = "../SystemUsers/Login";
              }
              else {
                  old_alert(result);
              }

          });
        }
    }
    
}
$(document).ready(function () {
    $("#VendorId_drop").kendoAutoComplete({
        dataTextField: "Name",
        dataValueField: 'id',
        filter: "startswith",
        minLength: 2,
        select: function (e) {
            var dataItem = this.dataItem(e.item.index());
            //vendorId = dataItem.ID;
            //alert(dataItem.ID);
            //alert(vendorId);
            $("#VendorId").val(dataItem.ID);
        },
        dataSource: {
            type: "json",
            serverFiltering: true,
            transport: {
                read: { url: "../Vendor/GetOrgVendors" },
                parameterMap: function (data, action) {
                    if (action === "read") {
                        return {

                            //term: data.filter.filters[0].value
                            name: $("#VendorId_drop").data("kendoAutoComplete").value()
                        };
                    } else {
                        return data;
                    }
                }
            }
        },
        //select: onSelect
    });
    Language.GetAll(function (result) {
        Controls.FillDropdown(result, $("#LanguageId"));
    })
});