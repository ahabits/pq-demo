﻿var UserEmails = [];
var UserIDs = [];
var multiSelectWidget;
var windowWidget;
var type;
$(document).ready(function () {

    multiSelectWidget = $("#Users").kendoMultiSelect({
        dataTextField: "Name",
        dataValueField: 'UserId',
        filter: "contains",
        minLength: 3,
        separator: ", ",
        select: function (e) {

            var dataItem = this.dataItem(e.item.index());
            UserEmails.push(dataItem.Email);
            UserIDs.push(dataItem.UserId);
        },

        dataSource: []


    }).data("kendoMultiSelect");

});


$(document).ready(function () {
    type = $('#Type').val();
    Organization.GetClientOrgs(function (result) {
        Controls.FillDropdown(result, $("#ClientId"));
    })


    $("#SearchRecentDocs").on("click", function (e) {

        $("#listView").data("kendoGrid").dataSource.read();

    });
    windowWidget = $("#AddTemplate").kendoWindow({


        title: "Add",
        visible: false,
        modal: true,
        width: "600px"
    }).data("kendoWindow");
    $("#listView").kendoGrid({
        dataSource: new kendo.data.DataSource({
            type: "get",

            transport: {
                type: 'post',
                read: {
                    type: 'post',
                    url: "../SystemUsers/GetClientUsers",

                    data: function (options) {
                        return Object.assign(getparams(), options.data);
                    }

                },
            },
            schema: {
                data: "Data",
                total: "Total",
            },
            pageSize: 20,
            serverPaging: true,
            serverFiltering: true,
            serverSorting: true

        }),
        sortable: false,
        pageable: true,

        pageable: {
            refresh: true,

        },
        columns: [{

            field: "ClientName",
            title: "Client Name",
        },
        {
            hidden: true,
            field: "Id",

        },
        {

            field: "UsersEmails",
            title: "User Email's",

        }, {

            field: "UserNames",
            title: "Users",

        },
        {
            hidden: true,

            field: "ClientID",


        }, {
            hidden: true,

            field: "UserIds",


        },
        {
            width: "100px",
            template: "<i class='fa fa-edit'style='font-size: x-large;padding-right: 10px;' onclick=\"EditDetails(#=Id#,'#=UserIds#',#=ClientID#)\"></i><i class='fa fa-trash' aria-hidden='true'style='font-size: x-large;padding-right: 10px;color:red;' onclick=\"deleteDetails(#=Id#,#=ClientID#)\"></i>"

        }
        ], editable: false


    });
    $('#filter').on('input', function (e) {
        var length = $('#filter').val().length;
        //  if (length>=3 ||length ==0)
        $("#listView").data("kendoGrid").dataSource.read();

    });

});

function Onclose() {
    Resetdata();
    $("#AddTemplate").data("kendoWindow").close();
}
function getparams() {
    var viewModel = kendo.observable({
        param: {
            filters: $('#filter').val(),
            Type:type
        }

    });
    return JSON.parse(JSON.stringify(viewModel.param));
}

function UpdateDetails(id) {
    var dataItem = multiSelectWidget.dataItems();
    var userIDsArray = [];
    var useremails = [];
    for (i = 0; i < dataItem.length; i++) {
        userIDsArray.push(dataItem[i].UserId);
        useremails.push(dataItem[i].Email);
    }
    var userEmails = useremails.toString();
    var userIDs = userIDsArray.toString();

    Post("../SystemUsers/SaveClientInvitaionEmails", { ClientUserEmails: userEmails, UserIds: userIDs, ClientID: $('#ClientId').val(), Id: id, Type: type }, function (result) {
        if (result == "Success") {
            Resetdata();
            var grid = $("#listView").data("kendoGrid").dataSource.read();
            $("#AddTemplate").data("kendoWindow").close();
        }
        else { Resetdata(); alert(result); }

    });

}
function deleteDetails(id, clientid) {
    var windowWidget = $("#AddTemplate").data("kendoWindow");

    var c = confirm("Do you want to delete this Invitation?")
    if (c) {
        kendo.ui.progress(windowWidget.element, true);
        $.ajax({
            type: 'GET',
            url: 'DeleteClient',
            data: 'Id=' + id,
            success: function (data) {

                $("#listView").data("kendoGrid").dataSource.read();
                kendo.ui.progress(windowWidget.element, false);
            },
            error: function (req, status, result) {
                kendo.ui.progress(windowWidget.element, false);
                alert("Unable to delete, Please contact administrator");

            }
        });
    }
}
function FillUserEmails(users) {
    debugger;
    $("#Users").data().kendoMultiSelect.value([]);
    Post("../SystemUsers/GetClientUsersData?ClientId=" + $('#ClientId').val(), {}, function (result) {
        var info = [];
        var selectedItems = [];
        for (i = 0; i < result.length; i++) {
            //$('#Users').append('<option value="' + data[i].UserId + '">' + data[i].Name + '</option>');
            if (users && $.inArray(result[i].UserId, users) > -1)
                selectedItems.push({ Name: result[i].Name, UserId: result[i].UserId });
            //$("#Users_taglist").append('<li class="k-button" deselectable="on"><span deselectable="on">' + data[i].Name + '</span><span unselectable="on" aria-label="delete" class="k-select"><span class="k-icon k-i-close Delete"></span></span></li>');
            info.push({ Name: result[i].Name, UserId: result[i].UserId, Email: result[i].Email });
        }
        var dataSource = new kendo.data.DataSource({
            data: info
        });

        multiSelectWidget.setDataSource(dataSource);
        if (users)
            multiSelectWidget.value(users);  
    });
    //$.ajax(
    //  {
    //      type: 'get',
    //      url: "../SystemUsers/GetClientUsers?ClientId= " +  $('#ClientId').val(),
    //      success: function (data) {
    //          var info = [];
    //          for (i = 0; i < data.length; i++) {
    //              //$('#Users').append('<option value="' + data[i].UserId + '">' + data[i].Name + '</option>');
    //              //if ($.inArray(data[i].UserId, users) > -1)

    //              //$("#Users_taglist").append('<li class="k-button" deselectable="on"><span deselectable="on">' + data[i].Name + '</span><span unselectable="on" aria-label="delete" class="k-select"><span class="k-icon k-i-close Delete"></span></span></li>');
    //              info.push({ Name: data[i].Name, UserId: data[i].UserId });
    //          }
    //          var dataSource = new kendo.data.DataSource({
    //              data: info
    //          });

    //          multiSelectWidget.setDataSource(dataSource);
    //          multiSelectWidget.trigger("change");
    //          //  $("#Users").data("kendoMultiSelect").value(Users);

    //      }

    //  })


}
function EditDetails(Id, UserIds, ClientId) {
    var dataItem = multiSelectWidget.dataItems();
    windowWidget.title('Edit');
    var users = UserIds.split(',');

    $('#ClientId').val(ClientId);
    FillUserEmails(users);

    $('#ClientId').prop("disabled", true);
    // $("#Submitbtn").text('Done');
    $("#Submitbtn").attr('onclick', 'UpdateDetails(' + Id + ')');
    $("#AddTemplate").data("kendoWindow").open().center();




}
function AddDetails() {

    FillUserEmails();
    var userEmails = UserEmails.toString();
    var userIDs = UserIDs.toString();
    var clientid = $('#ClientId').val();
    Post("../SystemUsers/SaveClientInvitaionEmails", { ClientUserEmails: userEmails, UserIds: userIDs, ClientID: clientid, Type: type }, function (result) {
        if (result == "Success") {
            Resetdata();
            var grid = $("#listView").data("kendoGrid").dataSource.read();
            $("#AddTemplate").data("kendoWindow").close();
            // $("#ClientId option[value="+clientid+"]").hide();
        }
        else { Resetdata(); alert(result); }

    });


}
function Resetdata() {

    UserIDs.splice(0);
    UserEmails.splice(0);
}
function OpenKendoWindow() {

    FillUserEmails();
    windowWidget.title('Add');

    $('#ClientId').prop("disabled", false);
    $('#ClientId').val('');

    // $("#Submitbtn").text('Done');
    $("#Submitbtn").attr('onclick', 'AddDetails()');
    Resetdata();

    $("#AddTemplate").data("kendoWindow").open().center();
};