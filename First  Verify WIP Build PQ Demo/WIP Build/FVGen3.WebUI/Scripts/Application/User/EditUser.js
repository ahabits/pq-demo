﻿var WindowWidget;

var EditUser = {
    GetClientBUSites: function (userOrgId) {
        var clientBUs = $("#ClientBU").val().join();
        
        Post("../SystemUsers/getClientBUSitesbyBU",
           { userOrgId: userOrgId, buIds: clientBUs },
           function (result) {
               var options = "";
               $.each(result, function (key, item) {
                   options = options + "<option value='" + item.Key + "'>" + item.Value + "</option>";
               });
               $("#ClientBUSite").html(options);
           });
    }
}
function Onclose() {

    $("#Users").data("kendoWindow").close();
}

   
function onOpen(Isdelete, Hasusers) {
    if (Hasusers == "False" && Isdelete == false) return false;
    else if (Hasusers == "False" && Isdelete == true) {
        $.ajax({
            type: 'GET',
            url: 'DeleteUser',
            data: 'UserId=' + UserId,
            success: function (data) {
                if (data == "Error") {
                    alert("User can't be deleted as his/her data is tied with prequalification. Hence the user has been deactivated.");
                    // Rajesh on 02/12/2015
                    var loc = window.location + "";
                    if (loc.slice(-2) == "#!")
                        window.location = loc.substring(0, loc.length - 2);
                    else
                        window.location = loc;
                    // Ends<<< 
                }
                else
                    window.location = data;
                //$("#searchBlock_id").load(link);
            },
            error: function () {
                console.trace();
                //alert("Processing your previous request, please wait.. ");
            }
        });
        window.location = "../SystemUsers/DeleteUser?UserId=" + UserId;
    }
    else {
        var CurrentUserID = $('#userId').val();
        WindowWidget = $("#Users").kendoWindow({
            width: "500px",
            height: "350px",
            top: "300px",
            title: $("#LastName").val() + " " + $("#FirstName").val(),
            visible: false,
            actions: [
                
                "Close"
            ],

        }).data("kendoWindow").center().open();

   
        $(".UsersContent").empty();
        kendo.ui.progress(WindowWidget.element, true);
        $.post("/SystemUsers/GetOrgUsers", { UserId: CurrentUserID }, function (data) {
            debugger;
            var template = "<div class='Hinttext'>Assign "+$("#LastName").val() + " " + $("#FirstName").val()+" Notifications to:</div>";
            template += "<table>";
           
            for (i = 0; i < data.length; i++) {
                template += "<tr>";
                template += "<td style='padding: 2px 20px;'> <input type='radio' id='UserId_" + data[i].UserId + "' name='UpdatedUser' value='" + data[i].UserId + "'>";
                template += "<td> <label for='" + data[i].UserId + "' style='font-size: medium;font-weight: 600;'>" + data[i].Name + "</label>";
                template += "</tr>";
            } template += "</table>";
          
            $(".UsersContent").append(template);
            kendo.ui.progress(WindowWidget.element, false);
            $('#Savebtn').attr('onClick', 'UpdateUser(' + Isdelete + ');');
        });
      //  kendo.ui.progress(WindowWidget.element, false);
    }
}

function UpdateUser(Isdelete)
{
    kendo.ui.progress(WindowWidget.element, true);
    if ($('input[name="UpdatedUser"]:checked').length <= 0) {
        alert("Please Selected User");
        kendo.ui.progress(WindowWidget.element, false);
        return false;
    }               
    var NewUserId = $('input[name="UpdatedUser"]:checked').val();
    var CurrentUserId = $('#userId').val();
    $.post("/SystemUsers/UpdateUser", { NewUser: NewUserId, CurrentUser: CurrentUserId, Isdelete: Isdelete }, function (data) {
      
            debugger;
            if (data == "Error") {
                kendo.ui.progress(WindowWidget.element, false);
                alert("User can't be deleted . Hence the user has been deactivated.");
                // Rajesh on 02/12/2015
                
                var loc = window.location + "";
                if (loc.slice(-2) == "#!")
                    window.location = loc.substring(0, loc.length - 2);
                else
                    window.location = loc;
                // Ends<<< 
            }
            else if (data == "Updated Successfully") {
                kendo.ui.progress(WindowWidget.element, false);
                Onclose();
            }
            else {
                window.location = data;
                kendo.ui.progress(WindowWidget.element, false);
            }
       
    });
  
}


function ChangePassword(UserID)
{
    $("#Users").kendoWindow({
        width: "500px",
        height: "350px",
        top: "300px",
        title: $("#LastName").val() + " " + $("#FirstName").val(),
        visible: false,
        actions: [
           
            "Close"
        ],

    }).data("kendoWindow").center().open();
    $(".UsersContent").empty();
    var template = "<div>";
    
    template += "<div id='userpassword'>";
    template += "<div style='color:red;font-size: large;font-style: oblique;'>You can reset the password here.</div>";
    template += "<div class='tip'>";
    template += "<label class='enableStar' style='font-size: large;font-weight:600;'>Password</label>";
    template += "<input type='password'name=Password1 id='Password1' maxlength='20'style='padding: 5px;border: solid 2px #ddd;font-size: x-large;border-radius: 5px;margin:20px;'/><span>Your password must meet the following requirements:<br />At least 10 characters long<br />Contain at least 1 uppercase letter<br />Contain at least 1 lowercase letter<br />Contain at least 1 number</span>";
    template += "</div>";
    template += "<div class='tip'>";
    template += "<label class='enableStar'style='font-size: large;font-weight:600;width: 20%;display: inline-block;'>Confirm Password</label>";
    template += "<input type='password'name=ConfirmPassword1 id='ConfirmPassword1' maxlength='20'style='padding: 5px;border: solid 2px #ddd;font-size: x-large;border-radius: 5px;margin:20px;'/><span>Please re-type the password you entered in the Password box above.</span>";
    template += "<div id='hinttext' style='color:red;margin-left: 40%;'></div>";
    template += "</div>";

    template += "</div>";
    template += "</div>";
    $(".UsersContent").append(template);
    $('#Savebtn').attr('onClick', 'UpdateUserpassword("'+UserID+'");');

}
function UpdateUserpassword(UserID) {
    $("#hinttext").text('');
    //var CurrentUserID = $('#userId').val();
    var NewPassword = $('#Password1').val();
    var Confirmpassword = $('#ConfirmPassword1').val();
    var regex =new RegExp( "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{10,}$");
 
    var IsPasswordMatch = NewPassword.localeCompare(Confirmpassword);
    if (NewPassword.length == 0 && Confirmpassword == 0) {
        alert("Please enter passwords");
        return false;
    }
    if (!regex.test(NewPassword)) {
        alert("Please enter valid Password");
        return false;
    }
  if(IsPasswordMatch!=0)
    {
        tableBody = $("#hinttext").text('Passwords do not match');
        return false;
  }
  //$('#Password').val(NewPassword);
  //$('#ConfirmPassword').val(Confirmpassword);
  $.post("/SystemUsers/Updatepassword", { UserId: UserID, Password: NewPassword }, function (data) {
      if(data=="Success")
      {
          alert("Password has been changed successfully");
          Onclose();
      }
  });
    
}