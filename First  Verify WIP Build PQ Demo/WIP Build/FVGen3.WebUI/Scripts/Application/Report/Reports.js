﻿var Reports = {
   
    GetClientTemplates: function (callback) {
        var clientId = $("#ClientOrganizationId_SubSec").val();
        Post("../Client/GetClientTemplates",
            { ClientId: clientId }, function (data) {
                var controls=$(Controls.Dropdown(data));
                controls.attr("id", "template");
                controls.css("width", "220px");
                controls.removeClass("rowGenericReportsSelect");
                $("#clientTemplates").html(controls);
            });
    },
    InjuryStatistics: function () {
        var form = document.createElement("form");
        form.setAttribute("method", "post");
        form.setAttribute("action", "../Reports/InjuryStatisticsReport.aspx");
        form.setAttribute("target", "_blank");
        
        form.appendChild(createField("TemplateId", $("#Template_Injury").val()));
        form.appendChild(createField("ClientId", $("#ClientOrgId_Injury").val()));
        document.body.appendChild(form);
        form.submit();
    },
   
   
    ContractVendors: function () {
        
        var form = document.createElement("form");
        form.setAttribute("method", "post");
        form.setAttribute("action", "../Reports/VendorContractInfo.aspx");
        form.setAttribute("target", "_blank");
        
        form.appendChild(createField("TemplateId", $("#Template_ContractRow").val()));
        form.appendChild(createField("ClientId", $("#ClientOrgId_Contract").val()));
        document.body.appendChild(form);
        form.submit();
    },


    StatusHistory: function () {
        var form = document.createElement("form");
        form.setAttribute("method", "post");
        form.setAttribute("action", "../Reports/StatusStatistics.aspx");
        form.setAttribute("target", "_blank");
        
        form.appendChild(createField("TemplateId", $("#Template_Status").val()));
        form.appendChild(createField("ClientId", $("#ClientOrgId_Status").val()));
        document.body.appendChild(form);
        form.submit();
    },
    InsuranceInformation: function () {
        var form = document.createElement("form");
        form.setAttribute("method", "post");
        form.setAttribute("action", "../Reports/InsuranceInformation.aspx");
        form.setAttribute("target", "_blank");
        
        form.appendChild(createField("TemplateId", $("#Template_Insurance").val()));
        form.appendChild(createField("ClientId", $("#ClientOrgId_Insurance").val()));
        document.body.appendChild(form);
        form.submit();
    },
    Financial: function () {
        
        var form = document.createElement("form");
        form.setAttribute("method", "post");
        form.setAttribute("action", "../Reports/FinancialAnalytics.aspx");
        form.setAttribute("target", "_blank");
        

        form.appendChild(createField("ClientId", $("#ClientOrgId_Analytics").val()));
        document.body.appendChild(form);
        form.submit();
    },
    GetClientTemplates_Report: function (obj,targetDiv,targetId) {
        var clientId = $(obj).val();
        Post("../Client/GetClientTemplates",
            { ClientId: clientId }, function (data) {
                var controls=$(Controls.Dropdown(data));
                controls.attr("id", targetId);
                controls.removeClass("rowGenericReportsSelect");
                $("#" + targetDiv).html(controls);
            });
    },
    GetFeeTemplates_Report: function (obj, targetDiv, targetId) {
        
        var clientId = $(obj).val();
        Post("../Client/GetFeeTemplates",
            { ClientId: clientId }, function (data) {
                var controls = $(Controls.Dropdown(data));
                controls.attr("id", targetId);
                controls.removeClass("rowGenericReportsSelect");
                $("#" + targetDiv).html(controls);
            });
    },
    OpenSubSectionReport: function () {
        var form = document.createElement("form");
        form.setAttribute("method", "post");
        form.setAttribute("action", "../Reports/SubSectionReport.aspx");
        form.setAttribute("target", "_blank");

        form.appendChild(createField("templateid", $("#template").val()));
        document.body.appendChild(form);
        form.submit();
    },
    GetOnsiteOffsiteVisitors: function () {
        var OOVisitorfromdate = $("#OOVisitorfromdate").val();
        var OOVisitortodate = $("#OOVisitortodate").val();
        var OOVisitorlocation = $('#OOVisitorlocation').val();
        var OOvisitor = $('#OOVisitors').val();
        var offset = new Date().getTimezoneOffset()

        if (new Date(OOVisitorfromdate) > new Date(OOVisitortodate)) {
            alert("From date should be less than To date");
            return false;
        }
        if (OOVisitorfromdate == "") {
            alert("Please choose From date");
            return false;
        }

        if (OOVisitortodate == "") {
            alert("Please choose To date");
            return false;
        }

        var form = document.createElement("form");
        form.setAttribute("method", "post");
        form.setAttribute("action", "../Reports/GetOnsiteOffsiteVisitorsReport.aspx");
        form.setAttribute("target", "_blank");
        
        form.appendChild(createField("OOVisitorfromdate", OOVisitorfromdate));
        form.appendChild(createField("OOVisitortodate", OOVisitortodate));
        form.appendChild(createField("OOVisitorlocation", OOVisitorlocation));
        form.appendChild(createField("OOvisitor", OOvisitor));
        form.appendChild(createField("timeZoneOffset", offset));

        document.body.appendChild(form);
        form.submit();
    },
}