﻿
var UserIDs = [];
var multiSelectWidget;
var windowWidget;

$(document).ready(function () {

    multiSelectWidget = $("#Users").kendoMultiSelect({
        dataTextField: "Name",
        dataValueField: 'UserId',
        filter: "contains",
        minLength: 3,
        separator: ", ",
        select: function (e) {

            var dataItem = this.dataItem(e.item.index());
           
            UserIDs.push(dataItem.UserId);
        },

        dataSource: []


    }).data("kendoMultiSelect");
    windowWidget = $("#AddTemplate").kendoWindow({


        title: "Add",
        visible: false,
        modal: true,
        width: "600px"
    }).data("kendoWindow");
    $("#listView").kendoGrid({
        dataSource: new kendo.data.DataSource({
            type: "get",

            transport: {
                type: 'post',
                read: {
                    type: 'post',
                    url: "../SystemUsers/GetPowerBiUsers",

                    data: function (options) {
                        return Object.assign(getparams(), options.data);
                    }

                },
            },
            schema: {
                data: "Data",
                total: "Total",
            },
            pageSize: 20,
            serverPaging: true,
            serverFiltering: true,
            serverSorting: true

        }),
        sortable: false,
        pageable: true,

        pageable: {
            refresh: true,

        },
        columns: [{

            field: "ReportName",
            title: "ReportName",
        },
        {
            hidden: true,
            field: "ReportId",

        },
        {

            field: "PermissionType",
            hidden: true
        },

        {

            field: "OrgId",
            hidden: true
        }
        ,
        {

            field: "PermissionTo",
            title: "Organization Type"
        }, {

            field: "OrgName",
            title: "Organization",
        },
        {
            hidden: true,
            field: "ID",

        }, {

            field: "Users",
            title: "Users",

        },
        {
            hidden: true,

            field: "OrgId",


        }, {
            hidden: true,

            field: "UserId",


        },
        {
            width: "100px",
            template: "<i class='fa fa-edit'style='font-size: x-large;padding-right: 10px;' onclick=\"EditDetails(#=ID#,'#=UserId#',#=OrgId#,'#=PermissionType#',#=ReportId#)\"></i><i class='fa fa-trash' aria-hidden='true'style='font-size: x-large;padding-right: 10px;color:red;' onclick=\"deleteDetails(#=ID#)\"></i>"

        }
        ], editable: false


    });
    //$('#filter').on('input', function (e) {
    //    var length = $('#filter').val().length;
    //    //  if (length>=3 ||length ==0)
    //    $("#listView").data("kendoGrid").dataSource.read();

    //});

});

function Onclose() {
    Resetdata();
    $("#AddTemplate").data("kendoWindow").close();
}
function getparams() {
    var viewModel = kendo.observable({
        param: {
            filters: $('#filter').val(),
            
        }

    });
    return JSON.parse(JSON.stringify(viewModel.param));
}

function UpdateDetails(id) {
    var dataItem = multiSelectWidget.dataItems();
    var userIDsArray = [];
    for (i = 0; i < dataItem.length; i++) {
        userIDsArray.push(dataItem[i].UserId);
    }
    var userIDs = userIDsArray.toString();
    Post("../SystemUsers/PostPowerBiUsersData", { UserIds: userIDs, OrgId: $('#OrganizationId').val(), Id: id, Type: $('#PermissionType').val(), ReportId: $('#ReportId').val() }, function (result) {
        if (result == "Success") {
            Resetdata();
            var grid = $("#listView").data("kendoGrid").dataSource.read();
            $("#AddTemplate").data("kendoWindow").close();
        }
        else { Resetdata(); alert(result); }

    });



}
function deleteDetails(id) {
    var windowWidget = $("#AddTemplate").data("kendoWindow");

    var c = confirm("Do you want to delete this Permission?")
    if (c) {
        kendo.ui.progress(windowWidget.element, true);
        $.ajax({
            type: 'GET',
            url: 'DeletePowerBiUserPermission',
            data: 'Id=' + id,
            success: function (data) {

                $("#listView").data("kendoGrid").dataSource.read();
                kendo.ui.progress(windowWidget.element, false);
            },
            error: function (req, status, result) {
                kendo.ui.progress(windowWidget.element, false);
                alert("Unable to delete, Please contact administrator");

            }
        });
    }
}

function FillUserEmails(users) {
    debugger;

    $("#Users").data().kendoMultiSelect.value([]);
    UserIDs.splice(0);
    if ($('#OrganizationId').val() == "") { $('#UsersDiv').hide(); return; }
    else {
        $('#UsersDiv').show();
        Post("../SystemUsers/GetOrganizationUsers?OrgId=" + $('#OrganizationId').val(), {}, function (result) {
            var info = [];
            var selectedItems = [];
            for (i = 0; i < result.length; i++) {
                if (users && $.inArray(result[i].UserId, users) > -1)
                    selectedItems.push({ Name: result[i].Name, UserId: result[i].UserId });
                info.push({ Name: result[i].Name, UserId: result[i].UserId });
            }
            var dataSource = new kendo.data.DataSource({
                data: info
            });

            multiSelectWidget.setDataSource(dataSource);
            if (users)
                multiSelectWidget.value(users);
        });
    }
}
function EditDetails(Id, UserIds, orgId, Type, ReportId) {
    debugger;
    var dataItem = multiSelectWidget.dataItems();
    $('#ReportId').prop("disabled", true);
    windowWidget.title('Edit');
    $('#OrganizationId').empty();
    var users = UserIds.split(',');
    $('#PermissionType').val(Type);
    if (Type != 0) {
        Organization.GetOrgs(Type, function (result) {
            Controls.FillDropdown(result, $("#OrganizationId"));
            $('#OrganizationId').val(orgId);
            if (orgId != null) {
                FillUserEmails(users);
                $('#UsersDiv').show();
            }
            else {
                $('#UsersDiv').hide();
            }
        })
    }
    else {
        $('#OrgDiv').hide();
        $('#UsersDiv').hide();
    }
    $('#ReportId').val(ReportId);
    $("#Submitbtn").attr('onclick', 'UpdateDetails(' + Id + ')');
    $("#AddTemplate").data("kendoWindow").open().center();
}
function AddDetails() {
  
    var userIDs = UserIDs.toString();
    var userIDs = UserIDs.toString();
    var OrgId = $('#OrganizationId').val();
    var ReportId = $('#ReportId').val();
    Post("../SystemUsers/PostPowerBiUsersData", { UserIds: userIDs, OrgId: $('#OrganizationId').val(), Type: $('#PermissionType').val(), ReportId: ReportId }, function (result) {
        if (result == "Success") {
            Resetdata();
            var grid = $("#listView").data("kendoGrid").dataSource.read();
            $("#AddTemplate").data("kendoWindow").close();
        }
        else { Resetdata(); alert(result); }

    });
}
function Resetdata() {
    $('#PermissionType').val('');
    $('#OrganizationId').empty();
    UserIDs.splice(0);

}
function OpenKendoWindow() {
    
    $('#ReportId').prop("disabled", false);
    $('#OrgDiv').hide();
    $('#UsersDiv').hide();
    $('#ReportId').val('');
    windowWidget.title('Add');

    $("#Submitbtn").attr('onclick', 'AddDetails()');
    Resetdata();

    $("#AddTemplate").data("kendoWindow").open().center();
};
function GetOrgs(Type) {
    $('#OrganizationId').empty();
    $("#Users").data().kendoMultiSelect.value([]);
    UserIDs.splice(0);
    if (Type != "0") {
        $('#OrgDiv').show();

        Organization.GetOrgs(Type, function (result) {
            Controls.FillDropdown(result, $("#OrganizationId"));
           
        })

    }
    else {
        $('#OrgDiv').hide();
        $('#UsersDiv').hide();
    }
}

