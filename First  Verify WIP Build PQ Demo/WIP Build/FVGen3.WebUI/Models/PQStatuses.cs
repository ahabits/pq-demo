﻿
namespace FVGen3.WebUI.Models
{
    // ReSharper disable once InconsistentNaming
    public class PQStatuses
    {
        public bool? SType { get; set; }
        public long Key { get; set; }
        public string Value { get; set; }
    }
}