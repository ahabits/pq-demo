﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FVGen3.WebUI.Models
{
    public class DropdownGenericModel
    {
        public long ID { get; set; }  
        public string Name { get; set; }
    }
}