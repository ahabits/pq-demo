﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FVGen3.WebUI.Models
{
    public class LocalOrganizations
    {
        public long OrganizationID { get; set; }
        public string Name { get; set; }
        public string Address1 { get; set; }
        public string PhoneNumber { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public bool IsERI { get; set; }
        public int? CopyAnswers { get; set; }
        public string Country { get; set; }
        public string TemplateName { get; set; }
        public Guid TemplateID { get; set; }
        public bool IsLocked { get; set; }
        public IEnumerable<string> SubClients { get; set; }
    }
}