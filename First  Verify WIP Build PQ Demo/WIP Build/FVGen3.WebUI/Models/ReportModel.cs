﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FVGen3.WebUI.Models
{
    public class ReportModel
    {
        public string Sql { get; set; }
        public List<KeyValuePair<string,string>> Data { get; set; }
        public ReportModel()
        {
            Data = new List<KeyValuePair<string, string>>();
        }
    }
}