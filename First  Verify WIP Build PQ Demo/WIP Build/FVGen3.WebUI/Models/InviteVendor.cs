﻿using System.Collections.Generic;

namespace FVGen3.WebUI.Models
{
    public class InviteVendor
    {
        public string vendorName;
        public string city;
        public string state;
        public List<long> biddings;
        public int type;
    }
}