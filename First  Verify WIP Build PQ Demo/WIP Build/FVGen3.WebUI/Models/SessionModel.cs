﻿

using FVGen3.Domain.Entities;
using System;
using System.Collections.Generic;

namespace FVGen3.WebUI.Models
{
    public class SessionModel
    {
        public Guid UserId { get; set; }
        public string Role { get; set; }
        public long OrganizationId { get; set; }
        public List<LocalHeaderMenuModel> HeaderMenus { get; set; }
    }
}