﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FVGen3.WebUI.Models
{
    public class ExtractGenerator
    {
        public class VendorsWithNotPassedRequest
        {
            public string clientId { get; set; }
            public bool isRequiredMoodleData { get; set; }
        }
    }
}