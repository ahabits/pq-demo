﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FVGen3.WebUI.Models
{
    public class SignProCheckin
    {
        [JsonProperty("event")]
        public string event_ { get; set; }
        public DateTime date { get; set; }
        public Data data { get; set; }
    }
    public class FormResponses
    {
        public string id { get; set; }
        public List<object> responses { get; set; }
        public DateTime prefillDate { get; set; }

    }

    public class Host
    { 
        public string id { get; set; }
        public string email { get; set; }
        public string mobile { get; set; }
        public string company { get; set; }
        public string lastName { get; set; }
        public string photoURL { get; set; }
        public string firstName { get; set; }
        public string hostGroup { get; set; }
        public string hostGroupName { get; set; }

    }

    public class Site
    {
        public string id { get; set; }
        public string name { get; set; }

    }

    public class VisitorType
    {
        public string id { get; set; }
        public string name { get; set; }

    }

    public class Data
    {
        public string id { get; set; }
        public DateTime startDate { get; set; }
        public DateTime? expiresDate { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string email { get; set; }
        public string mobile { get; set; }
        public bool mobileVerified { get; set; }
        public bool emailVerified { get; set; }
        public string photoURL { get; set; }
        public string status { get; set; }
        public bool autoExpired { get; set; }
        public FormResponses formResponses { get; set; }
        public string formResponsesSignOut { get; set; }
        public Host host { get; set; }
        public string company { get; set; }
        public Site site { get; set; }
        public VisitorType visitorType { get; set; }

    }
}