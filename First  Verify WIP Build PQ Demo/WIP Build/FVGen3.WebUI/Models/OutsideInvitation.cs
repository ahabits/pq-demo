﻿
using System.Web.Mvc;

namespace FVGen3.WebUI.Models
{
    public class OutsideInvitation
    {
        public long vendorId { get; set; }
        public int invitationType{ get; set; }
        public string locations{ get; set; }
        public string riskLevel{ get; set; }
        public string comments{ get; set; }
        public bool isSendMeCC{ get; set; }
        public bool isSendMeBCC{ get; set; }
        public string sendCC{ get; set; }
        public string sendBCC{ get; set; }
        public string toAddress { get; set; }
        [AllowHtml]
        public string htmlTemplate{ get; set; }

        public long clientId { get; set; }
        public long? PqId { get; set; }

        public bool isERI { get; set; }
    }
}