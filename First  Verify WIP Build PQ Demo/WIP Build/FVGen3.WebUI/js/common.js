

// Added function for permissions on 08/07/2013 by Kiran Talluri
function setPermissions(permissionValue) {
    //var permissionValue = '@ViewBag.ViewPermissions' + "";
    //alert(permissionValue);
    if (permissionValue == "NoViewName") {
        alert("You have not defined ViewName in your controller");
        permissionValue = "";
    }
    if (permissionValue == "UnhandledException") {
        alert("You do not have permission to view the requested page");
        permissionValue = "";
    }
    if (permissionValue == "" || permissionValue.indexOf("R") == -1) {
        //window.history.go(-1); // user does not have permissions to view this page...
        if (window.opener != null) {
            //PopUp
            window.close();
        }
        else {
            //Not a Popup
            alert("Either you do not have permission to view the requested page or your session is expired, please contact system administrator");
            window.history.go(-1);
        }
    }

    if (permissionValue.indexOf("I") == -1) {
        $('[btnType = Insert]').each(function () {
            $(this).hide();

            if ($(this).attr('type') != undefined && $(this).attr('type').toString().toLowerCase() == "submit") {
                $('form').submit(function (e) {
                    e.preventDefault();
                });
            }
        });
    }
    if (permissionValue.indexOf("M") == -1) {
        $('[btnType = Update]').each(function () {

            $(this).hide();

            if ($(this).attr('type') != undefined && $(this).attr('type').toString().toLowerCase() == "submit") {
                $('form').submit(function (e) {
                    e.preventDefault();
                });
            }
        });
    }
    if (permissionValue.indexOf("D") == -1) {
        $('[btnType = Delete]').each(function () {

            $(this).hide();
        });
    }
}
// Ends <<<
var popTextbgcolor = 'inherit';
$(document).ready(function () {
    try {

        $(".kDatePicker").each(function () {
            if (!$(this).hasClass("k-input"))
                $(this).kendoDatePicker();
        });
    } catch (err) { }
    $("div").focusin(function () {

        var attr = $(this).attr('popText');
        if (typeof attr !== 'undefined' && attr !== false) {
            $('div#pop-up').find('p').html($(this).attr('popText'));
            var top = ($(this).position().top);
            var left = $(this).position().left;
            $("div#pop-up").css('top', top).css('left', left - $("div#pop-up").width() - 50);
            //popTextbgcolor = $(this).css("background-color");
            $(this).css("background-color", "#b9d7f6");
            if ($(this).attr('popText') != "")
                $('div#pop-up').show();
        }

    });
    $("div").focusout(function () {

        var attr = $(this).attr('popText');
        if (typeof attr !== 'undefined' && attr !== false) {
            //$(this).css("background-color", "#FFFFFF");
            $(this).css("background-color", popTextbgcolor);
            $('div#pop-up').hide();
        }
    });
    if ($(document).width() < 768) {
        var mheight = $('#s5_menu_wrap').height();
        $('#nav-trigger').click(function () {
            $('#s5_menu_wrap').fadeToggle();
        });
        $('#s5_menu_wrap').css('bottom', -mheight);
    }
});

$(window).on("load", function () {
    $("select#Name:disabled").addClass('select-off');

    var $vdcolor = $('#vdcolor').css('background-color');
    if ($vdcolor == "rgb(204, 0, 0)" || ($vdcolor !== undefined && $vdcolor.toUpperCase() === "#F40000")) {
        $('#vdcolor').addClass('negative');
    }
        //else if ($vdcolor == "rgb(161, 159, 159)") {
        //    $('#vdcolor').addClass('neutral');
        //}
    else if ($vdcolor == "rgb(255, 148, 34)" || ($vdcolor !== undefined && $vdcolor.toUpperCase() === "#FF9422")) {
        $('#vdcolor').addClass('warning');
    }
    else if ($vdcolor == "rgb(27, 148, 71)" || ($vdcolor !== undefined && $vdcolor.toUpperCase() === "#1B9447")) {
        $('#vdcolor').addClass('approved');
    }
    else {
        $('#vdcolor').addClass('neutral');
    }


});
if (typeof Object.assign != 'function') {
    Object.assign = function (target) {
        'use strict';
        if (target == null) {
            throw new TypeError('Cannot convert undefined or null to object');
        }

        target = Object(target);
        for (var index = 1; index < arguments.length; index++) {
            var source = arguments[index];
            if (source != null) {
                for (var key in source) {
                    if (Object.prototype.hasOwnProperty.call(source, key)) {
                        target[key] = source[key];
                    }
                }
            }
        }
        return target;
    };
}
if (!Array.prototype.indexOf) { /*For IE8 there is no such function. So we are implementing this one*/
    Array.prototype.indexOf = function (searchElement /*, fromIndex */) {
        'use strict';
        if (this == null) {
            throw new TypeError();
        }
        var n, k, t = Object(this),
            len = t.length >>> 0;

        if (len === 0) {
            return -1;
        }
        n = 0;
        if (arguments.length > 1) {
            n = Number(arguments[1]);
            if (n != n) { // shortcut for verifying if it's NaN
                n = 0;
            } else if (n != 0 && n != Infinity && n != -Infinity) {
                n = (n > 0 || -1) * Math.floor(Math.abs(n));
            }
        }
        if (n >= len) {
            return -1;
        }
        for (k = n >= 0 ? n : Math.max(len - Math.abs(n), 0) ; k < len; k++) {
            if (k in t && t[k] === searchElement) {
                return k;
            }
        }
        return -1;
    };
}
if (typeof String.prototype.trim !== 'function') {
    String.prototype.trim = function () {
        return this.replace(/^\s+|\s+$/g, '');
    }
}
if (!Object.keys) {
    Object.keys = function (obj) {
        var keys = [],
            key;

        for (key in obj) {
            if (obj.hasOwnProperty(key)) {
                keys.push(key);
            }
        }

        return keys;
    };
}

var Model = {
    show: function () {
        $("#myModal").show("slide", { direction: "right" }, 1000);
    },
    hide: function () {
        $("#myModal").hide("slide", { direction: "right" }, 1000);
        if (Model.onClose) {
            Model.onClose();
        }
    },
    add: function (html) {
        $("#modelHtml").html(html);
    },
    addHeader: function (html) {
        $("#modelContent").html(html);
    },
    isVisible: function () {
        
        return ($("#myModal").is(':visible'));
    }
}
var ExpandList = {
    click: function (obj) {
        $(obj).closest(".totalSubsection").find(".clientdocuments").toggle();
        if ($(obj).attr("src").indexOf("expandlist.png") != -1) {
            $(obj).attr("src", "../images/collapse.png");
        }
        else {
            $(obj).attr("src", "../images/expandlist.png");
        }

    },
    subClick: function (obj) {
        $(obj).closest(".clientdocuments").find(".subSection").toggle();
        if ($(obj).attr("src").indexOf("expandlist.png") != -1) {
            $(obj).attr("src", "../images/collapse.png");
        }
        else {
            $(obj).attr("src", "../images/expandlist.png");
        }

    }
}
function insertAtCaret(areaId, text) {
    var txtarea = document.getElementById(areaId);
    if (!txtarea) {
        return;
    }

    var scrollPos = txtarea.scrollTop;
    var strPos = 0;
    var br = ((txtarea.selectionStart || txtarea.selectionStart == '0') ?
      "ff" : (document.selection ? "ie" : false));
    if (br == "ie") {
        txtarea.focus();
        var range = document.selection.createRange();
        range.moveStart('character', -txtarea.value.length);
        strPos = range.text.length;
    } else if (br == "ff") {
        strPos = txtarea.selectionStart;
    }

    var front = (txtarea.value).substring(0, strPos);
    var back = (txtarea.value).substring(strPos, txtarea.value.length);
    txtarea.value = front + text + back;
    strPos = strPos + text.length;
    if (br == "ie") {
        txtarea.focus();
        var ieRange = document.selection.createRange();
        ieRange.moveStart('character', -txtarea.value.length);
        ieRange.moveStart('character', strPos);
        ieRange.moveEnd('character', 0);
        ieRange.select();
    } else if (br == "ff") {
        txtarea.selectionStart = strPos;
        txtarea.selectionEnd = strPos;
        txtarea.focus();
    }

    txtarea.scrollTop = scrollPos;
}

var PreogressBar = {
    show: function (selector) {
        if (!selector)
            selector = $("#body");
        kendo.ui.progress(selector, true);
    },
    hide: function (selector) {
        if (!selector)
            selector = $("#body");
        kendo.ui.progress(selector, false);
    }
}
if (!window.old_alert) {
    
    window.old_alert = window.alert;
    window.alert = function (message, title, fallback) {
        
        if (fallback) {
            old_alert(message);
            return;
        } if (!title)
            title = '';
        try {
            $("<div></div>").kendoAlert({
title:title, 
                content: message,
                minWidth: 300
            }).data("kendoAlert").open();

        } catch (e) {
            window.old_alert(message);
        }
    };
}
var Notification = {
    
  
    warning: function (Message) {
        var notificationElement = $("#notification");
        notificationElement.kendoNotification({
            position: {
                top: 50,
                right: 20
            }, autoHideAfter: 3000
        });
        
        var notificationWidget = notificationElement.data("kendoNotification");
   
        notificationWidget.warning(Message, "warning");
    },
    error: function (Message) {
    var notificationElement = $("#notification");
    notificationElement.kendoNotification({
        position: {
           top: 50,
           right: 20
        }, autoHideAfter: 3000
    });
    

    var notificationWidget = notificationElement.data("kendoNotification");
   
    notificationWidget.error(Message);
    },
    success: function (Message) {
        var notificationElement = $("#notification");
        notificationElement.kendoNotification({
            position: {
                top: 50,
                right: 20
            }, autoHideAfter: 3000
        });
        var notificationWidget = notificationElement.data("kendoNotification");
   
        notificationWidget.show(Message, "success");
    },
    
    success: function (Message) {
        var notificationElement = $("#notification");
        notificationElement.kendoNotification({
            position : {
            top: 50,
            right: 20
    }, autoHideAfter: 3000
        });
        var notificationWidget = notificationElement.data("kendoNotification");
   
        notificationWidget.info(Message);
    }
    
  

    
   

     
}
