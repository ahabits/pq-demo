﻿
// Get the modal
var terms = document.getElementById('termsModel');//$('#popupModel');

//model.style.display = "block";

// Get the <span> element that closes the modal
var span = document.getElementById("termsClose");



// When the user clicks on <span> (x), close the modal
span.onclick = function () {
    //model.style.display = "none";
    TermsModel.closeModel();
}

// When the user clicks anywhere outside of the modal, close it


var TermsModel = {

   
    closeEvent:function (){},
    closeModel:function() {
        $(terms).hide();
        this.closeEvent();
    },
    forceClose:function() {
        $(terms).hide();
    },
    show: function (closeEvent) {
        $(terms).show();
        if (closeEvent !== undefined)
            this.closeEvent = closeEvent;
    },
    setHeader: function (name) {
        $("#termsHeader").html(name);
    },
    setData: function (data) {
        
        $("#termsBody").html(data);
    },
    setHeight: function (val) {
        $("#termsModel").find(".modal-content").css("max-height", val);
    },
    getBody:function() {
        return $("#termsBody");
    },
    appendHeaderHtml: function (val,index) {
        if (index === 0) {
            $("#termsHeaderDiv").prepend(val);
            return;
        }
        if (index === -1) {
            $("#termsHeaderDiv").append(val);
            return;
        }
        $("#termsHeaderDiv > span:nth-child(" + (index) + ")").after(val);
    },
    reset: function () {
        
        $("#termsHeaderDiv").html('<span class="close" onclick="TermsModel.closeModel();">&times;</span>' +
            '<span><h2 id="termsHeader"> Header</h2></span>');
    }
}