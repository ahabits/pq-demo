﻿
// Get the modal
var model = document.getElementById('popupModel');//$('#popupModel');

//model.style.display = "block";

// Get the <span> element that closes the modal
var span = document.getElementById("popupClose");

if (span != null) {

    // When the user clicks on <span> (x), close the modal
    span.onclick = function () {
        //model.style.display = "none";
        PopupModel.closeModel();
    }

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function (event) {
        if (event.target == popupModel) {
            PopupModel.closeModel();
        }
    }
}
var PopupModel = {
    closeEvent:function (){},
    closeModel:function() {
        $(model).hide();
        this.closeEvent();
    },
    forceClose:function() {
        $(model).hide();
    },
    show: function (closeEvent) {
        $(model).show();
        if (closeEvent !== undefined)
            this.closeEvent = closeEvent;
    },
    setHeader: function (name) {
        $("#modelHeader").html(name);
    },
    setData: function (data) {
        $("#modelBody").html(data);
    },
    setHeight: function (val) {
        $("#popupModel").find(".modal-content").css("max-height", val);
    },
    getBody:function() {
        return $("#modelBody");
    },
    appendHeaderHtml: function (val,index) {
        if (index === 0) {
            $("#modelHeaderDiv").prepend(val);
            return;
        }
        if (index === -1) {
            $("#modelHeaderDiv").append(val);
            return;
        }
        $("#modelHeaderDiv > span:nth-child(" + (index) + ")").after(val);
    },
    reset:function() {
        $("#modelHeaderDiv").html('<span class="close" onclick="PopupModel.closeModel();">&times;</span>' +
            '<span><h2 id="modelHeader">Modal Header</h2></span>');
    }
}