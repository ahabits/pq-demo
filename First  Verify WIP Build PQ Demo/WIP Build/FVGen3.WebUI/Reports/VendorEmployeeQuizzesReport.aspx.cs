﻿using FVGen3.WebUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FVGen3.WebUI.Reports
{
    public partial class VendorEmployeeQuizzesReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public ReportModel EmployeeQuizReport(string clientOrgId, string ids)
        {
            var data = "";

            data = @"select distinct sites.SiteName + '-'+ sites.BusinessUnitLocation as SiteName,Templates.TemplateName, Contact.LastName +' , '+ Contact.FirstName AS EmpName, s.QuizResult, Convert(varchar,s.QuizDateSubmitted,101) as QuizDateStart, org.Name as Vendor, org1.Name AS Client from SystemUsersOrganizationsQuizzes s INNER JOIN [dbo].[Organizations] org on s.VendorId= org.OrganizationId INNER JOIN [dbo].[Organizations] org1 on s.clientId= org1.OrganizationId INNER JOIN [dbo].[ClientTemplatesBUGroupPrice] cg ON cg.ClientTemplateID= s.ClientTemplateId INNER JOIN [dbo].[ClientTemplatesForBU] cb ON cb.ClientTemplateBUGroupId = cg.ClientTemplateBUGroupId INNER JOIN [dbo].[ClientTemplatesForBUSites] cs on cb.ClientTemplateForBUId = cs.ClientTemplateForBUId INNER JOIN SystemUsersOrganizations ON s.SysUserOrgId = SystemUsersOrganizations.SysUserOrganizationId  INNER JOIN SystemUsers On SystemUsersOrganizations.UserId=SystemUsers.UserId and SystemUsers.UserStatus=1 INNER JOIN Contact ON Contact.UserId = SystemUsersOrganizations.UserId INNER JOIN [dbo].[ClientBusinessUnitSites] sites ON sites.ClientBusinessUnitId = cb.ClientBusinessUnitId INNER JOIN [dbo].[ClientTemplates] ON ClientTemplates.ClientTemplateId= s.ClientTemplateId INNER JOIN Templates ON ClientTemplates.TemplateID = Templates.TemplateID where cg.GroupingFor=2 and cg.GroupPriceType!=-1 and s.ClientId=@clientOrgId and cs.ClientBusinessUnitSiteId  
            in (select * from ufn_ParseIntList(@ids)) and sites.ClientBusinessUnitSiteId in(select * from ufn_ParseIntList(@ids)) and (s.QuizRemoved is null or s.QuizRemoved=0) and s.QuizResult is not null order by org.Name";
            if (Session["RoleName"] == "Client")
            {
                data = "select distinct sites.SiteName + '-'+ sites.BusinessUnitLocation  as SiteName,Templates.TemplateName, Contact.LastName +' , '+ Contact.FirstName AS EmpName, s.QuizResult, Convert(varchar,s.QuizDateSubmitted,101) as QuizDateStart, org.Name as Vendor, org1.Name AS Client from SystemUsersOrganizationsQuizzes s INNER JOIN [dbo].[Organizations] org on s.VendorId= org.OrganizationId INNER JOIN [dbo].[Organizations] org1 on s.clientId= org1.OrganizationId INNER JOIN [dbo].[ClientTemplatesBUGroupPrice] cg ON cg.ClientTemplateID= s.ClientTemplateId INNER JOIN [dbo].[ClientTemplatesForBU] cb ON cb.ClientTemplateBUGroupId = cg.ClientTemplateBUGroupId INNER JOIN [dbo].[ClientTemplatesForBUSites] cs on cb.ClientTemplateForBUId = cs.ClientTemplateForBUId INNER JOIN SystemUsersOrganizations ON s.SysUserOrgId = SystemUsersOrganizations.SysUserOrganizationId  INNER JOIN SystemUsers On SystemUsersOrganizations.UserId=SystemUsers.UserId and SystemUsers.UserStatus=1 INNER JOIN Contact ON Contact.UserId = SystemUsersOrganizations.UserId INNER JOIN [dbo].[ClientBusinessUnitSites] sites ON sites.ClientBusinessUnitId = cb.ClientBusinessUnitId INNER JOIN [dbo].[ClientTemplates] ON ClientTemplates.ClientTemplateId= s.ClientTemplateId INNER JOIN Templates ON ClientTemplates.TemplateID = Templates.TemplateID where cg.GroupingFor=2 and cg.GroupPriceType!=-1 and s.ClientId=" + @Session["currentOrgId"] + " and cs.ClientBusinessUnitSiteId  in (select * from ufn_ParseIntList(@ids)) and sites.ClientBusinessUnitSiteId in(select * from ufn_ParseIntList(@ids)) and (s.QuizRemoved is null or s.QuizRemoved=0) and s.QuizResult is not null order by org.Name";
            }

            var reportData = new ReportModel();
            reportData.Data = new List<KeyValuePair<string, string>>();
            reportData.Sql = data; 
            reportData.Data.Add(new KeyValuePair<string, string>("@clientOrgId", clientOrgId));
            reportData.Data.Add(new KeyValuePair<string, string>("@ids", ids));

            return reportData;

        }
    }
}