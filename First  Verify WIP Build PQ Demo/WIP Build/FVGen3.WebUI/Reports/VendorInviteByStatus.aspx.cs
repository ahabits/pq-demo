﻿using FVGen3.Domain.Concrete;
using FVGen3.Domain.Entities;
using FVGen3.WebUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AhaApps.Libraries.Extensions;
using FVGen3.WebUI.Utilities;
using Microsoft.Reporting.WebForms;
using System.Data.Entity;
using FVGen3.BusinessLogic.Interfaces;
using FVGen3.BusinessLogic;
using System.Data;

namespace FVGen3.WebUI.Reports
{
    public partial class VendorInviteByStatus : System.Web.UI.Page
    {
        IReportsBusiness reportBusiness;
        public VendorInviteByStatus()
        {
            this.reportBusiness = new ReportsBusiness();
        }
        private void Page_Load(object sender, System.EventArgs e)
        {
            if (!this.IsPostBack)
            {
                try
                {

                    if (!this.IsPostBack)
                    {
                        //var result = CustomReportGeneration(Request.Form["Subsite"].ToNullableGuid());


                        //ReportViewer1.LocalReport.DataSources.Clear();
                        //ReportViewer1.ProcessingMode = ProcessingMode.Local;

                        ReportViewer2.LocalReport.ReportPath = Server.MapPath("VendorInviteByStatusReport.rdlc");

                        DataSet dataSet = new DataSet();

                        System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["EFDbContext"].ConnectionString);

                        var query = reportBusiness.GetVendorInvitesByStatusQuery(Request.Form["TemplateID"].ToNullableGuid() ?? new Guid(), long.Parse(Request.Form["ClientId"]), Request.Form["State"]);
                        connection.Open();
                        System.Data.SqlClient.SqlDataAdapter adapter = new System.Data.SqlClient.SqlDataAdapter(query, connection);

                       
                        adapter.Fill(dataSet);

                        ReportDataSource dataSource = new ReportDataSource("VendorInviteByStatusDataSet", dataSet.Tables[0]);
                        ReportViewer2.LocalReport.DataSources.Add(dataSource);
                        ReportViewer2.DocumentMapCollapsed = true;
                        ReportViewer2.LocalReport.Refresh();

                        ReportViewer2.Visible = true;
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }


            }
        }

//        public ReportModel GetInvitationSent(string ClientId, string State,string TemplateId)
//        {
           
//            var data = "";
//            var HasState = "";
//            if (@State != "")
//            { HasState = "org.State = @State AND vid.ClientId = @ClientId"; } else { HasState = "vid.ClientId = @ClientId"; }
         
//                data = @"select VendorOrgs.Name VendorName, org.City VendorCity,org.State VendorState, org.Name as InvitedBy, ui.DBE as DBE,ui.Financials as Financials,ui.IsMinority as IsMinority,ui.MBE as MBE,ui.Safety as Safety,ui.SBE as SBE,ui.WBE as WBE
//FROM VendorInviteDetails  vid join prequalification as p on p.PrequalificationId = vid.PQId 
// JOIN Organizations org ON vid.ClientId = org.OrganizationID 
// JOIN Organizations as VendorOrgs ON vid.Vendorid = VendorOrgs.OrganizationID 
//join (
// SELECT PreQualificationId,[Minority Business] IsMinority,[Business qualifies as a Disadvantaged Business Enterprise (DBE):] DBE,[Business qualifies as a Minority Business Enterprise (MBE):] MBE 
//,[Business qualifies as a Small Business Enterprise (SBE): ] SBE
//,[Business qualifies as a Women's Business Enterprise (WBE):] WBE
//,[Financials:] Financials
//,[Safety:] Safety
//      FROM
//         (SELECT PreQualificationId, UserInput, QuestionText FROM PrequalificationUserInput ui 
//		 join QuestionColumnDetails qcol on qcol.QuestionColumnId =ui.QuestionColumnId
//		 join Questions q on q.QuestionID=qcol.QuestionId where qcol.QuestionColumnId in( )) I
//         PIVOT (max(UserInput) FOR QuestionText IN ([Minority Business],[Business qualifies as a Disadvantaged Business Enterprise (DBE):],[Business qualifies as a Minority Business Enterprise (MBE):]
//,[Business qualifies as a Small Business Enterprise (SBE): ],[Business qualifies as a Women's Business Enterprise (WBE):]
//,[Financials:]
//,[Safety:]
//)) P
//)ui on p.PrequalificationId=ui.PreQualificationId
//where "+HasState;
           
//            //if (Session["RoleName"].ToString() == "Client")
//            //{
//            //    data = "select org.City,org.State, vid.Address1, vid.Country, vid.InvitationDate, org.Name, vid.City FROM VendorInviteDetails vid join prequalification as p on p.PrequalificationId = vid.PQId INNER JOIN Organizations org ON vid.ClientId = org.OrganizationID INNER JOIN Organizations as VendorOrgs ON vid.Vendorid = VendorOrgs.OrganizationID where vid.ClientId ='" + Session["currentOrgId"] + "' And org.State=@State";
//            //}


//            var reportData = new ReportModel();
//            reportData.Data = new List<KeyValuePair<string, string>>();
//            reportData.Sql = data;
//            reportData.Data.Add(new KeyValuePair<string, string>("@State", State.ToStringNullSafe()));
//            reportData.Data.Add(new KeyValuePair<string, string>("@TemplateId", TemplateId.ToStringNullSafe()));
//            reportData.Data.Add(new KeyValuePair<string, string>("@ClientId", ClientId.ToStringNullSafe()));
//            return reportData;
//        }
    }
}