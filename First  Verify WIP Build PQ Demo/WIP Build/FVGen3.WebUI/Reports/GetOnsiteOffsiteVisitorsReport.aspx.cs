﻿using FVGen3.WebUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FVGen3.WebUI.Reports
{
    public partial class GetOnsiteOffsiteVisitorsReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public ReportModel GetVisitors(DateTime fromDate, DateTime toDate, string OOVisitorlocation, string OOvisitor, int workingOffset)
        {
            string workingOffsetString = (workingOffset * -1).ToString();

            var data = "";
            data = "select sites.SiteName, (v.LastName + ', ' + v.FirstName )as VisitorName, " +
                    " (c1.LastName + ', ' + c1.FirstName )as SecurityNameCheckin, (c2.LastName + ', ' + c2.FirstName )as SecurityNameCheckout, " +
                    " dateadd(HOUR, " + workingOffsetString + " ,oov.Checkin) as Checkin, dateadd(HOUR, " + workingOffsetString + ",oov.Checkout) as Checkout, oov.VisitorID,oov.ClientBusinessUnitSiteID,bus.BusinessUnitName " +
                    " from 	dbo.OnsiteOffsiteVisitor oov " +
                    " join Visitors v on v.VisitorID  = oov.VisitorID " +
                    " left outer join Contact c1 on c1.UserId  = oov.CheckinClientUser " +
                    " left outer join Contact c2 on c2.UserId  = oov.CheckoutClientUser  " +
                    " join ClientBusinessUnits bus on bus.ClientBusinessUnitId = oov.ClientBusinessUnitID " +
                    " join ClientBusinessUnitSites sites on sites.ClientBusinessUnitSiteId = oov.ClientBusinessUnitSiteID " +
                    " where (oov.Checkin between @fromDate and @toDate " +
                    " or oov.Checkout between @fromDate and @toDate) ";
            if (OOVisitorlocation != "-1")
            {
                data = data + " and (@OOVisitorlocation is null  or @OOVisitorlocation = oov.ClientBusinessUnitSiteId) ";
            }
            
            if (@OOvisitor != "-1")
            {
                data = data + " and (@OOvisitor is null or @OOvisitor = oov.VisitorID) ";
            }
            var reportData = new ReportModel();
            reportData.Data = new List<KeyValuePair<string, string>>();
            reportData.Sql = data;
            reportData.Data.Add(new KeyValuePair<string, string>("@fromDate", fromDate.ToString()));
            reportData.Data.Add(new KeyValuePair<string, string>("@toDate", toDate.ToString()));
            reportData.Data.Add(new KeyValuePair<string, string>("@OOVisitorlocation", OOVisitorlocation));
            reportData.Data.Add(new KeyValuePair<string, string>("@OOvisitor", OOvisitor));
            
            return reportData;

        }
    }
}