﻿using FVGen3.BusinessLogic;
using FVGen3.BusinessLogic.Interfaces;
using FVGen3.WebUI.Utilities;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FVGen3.WebUI.Reports
{
    public partial class InsuranceInformation : System.Web.UI.Page
    {
        IReportsBusiness reportBusiness;
        public InsuranceInformation()
        {
            this.reportBusiness = new ReportsBusiness();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                if (!this.IsPostBack)
                {
                    ReportViewer1.LocalReport.ReportPath = Server.MapPath("InsuranceInformationReport.rdlc");

                    DataSet dataSet = new DataSet();

                    System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["EFDbContext"].ConnectionString);

                    var query = reportBusiness.GetInsuranceInformationQuery(Request.Form["TemplateId"].ToNullableGuid() ?? new Guid(), long.Parse(Request.Form["ClientId"]));
                    connection.Open();
                    System.Data.SqlClient.SqlDataAdapter adapter = new System.Data.SqlClient.SqlDataAdapter(query, connection);

                    adapter.Fill(dataSet);

                    ReportDataSource dataSource = new ReportDataSource("InsuranceInformationDataSet", dataSet.Tables[0]);

                    ReportViewer1.LocalReport.DataSources.Add(dataSource);
                    ReportViewer1.DocumentMapCollapsed = true;
                    ReportViewer1.LocalReport.Refresh();

                    ReportViewer1.Visible = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}