﻿using FVGen3.WebUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AhaApps.Libraries.Extensions;
using FVGen3.BusinessLogic;
using FVGen3.BusinessLogic.Interfaces;
namespace FVGen3.WebUI.ReportsForAdmin
{
    public partial class ExpiringDocumentWebForm : System.Web.UI.Page
    {
        IClientBusiness ClientBusiness;
        public string sql = "";

        public ExpiringDocumentWebForm()
        {
            this.ClientBusiness = new ClientBusiness();

        }
        public ExpiringDocumentWebForm(string sql)
        {
            this.sql = sql;

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            sql = Request.Form["sql"].ToString();
        }


        public ReportModel ExpiredDocument(string doctypeList)
        {
            List<long> DoctypeList = new List<long>();
           List<string> ClientIdsList = new List<string>();
            var RoleName = Session["RoleName"];

            var ClientId = (long)Session["currentOrgId"];
            ClientIdsList.Add(ClientId.ToString());
            var PqClient = "";
            if (RoleName.Equals("Super Client"))
            {
            var subclients = ClientBusiness.GetSubClients(ClientId).Select(r => r.Key);
                ClientIdsList.AddRange(subclients);
                PqClient = "and p.ClientId=@ClientId";
           }
            var ClientIds = string.Join(",", ClientIdsList.ToArray());

            var currentDate = DateTime.Now.ToString("MM/dd/yyyy");
            var reportValues = "";
            string finalStatuses = "8,9,13,24,25,26";

            if (RoleName.Equals("Admin"))
            {
                reportValues = "select Distinct* from (Select DocType.DocumentTypeName,client.Name AS Client,vendor.Name AS Vendor, " +
                                "pstatus.PrequalificationStatusName AS Status,convert(varchar,orgLogs.EmailSentDateTime,110) AS EmailSentDate, " +
                                "case when latestLog.NotificationNo = 1 then 'First' when latestLog.NotificationNo = 2 then 'Second' when latestLog.NotificationNo = 3 then 'Third' end as NotificationNo," +
                                " convert(varchar,Document.Expires,110) AS Expires  from Document " +
                                " INNER JOIN DocumentType AS DocType ON Document.DocumentTypeId = DocType.DocumentTypeId " +
                                " INNER JOIN Organizations AS org ON Document.OrganizationId = org.OrganizationID " +
                                " INNER JOIN OrganizationsNotificationsEmailLogs AS orgLogs " +
                                "inner join (select DocumentId,max(NotificationNo) NotificationNo from OrganizationsNotificationsEmailLogs group by DocumentId) as latestLog on latestLog.DocumentId=orgLogs.DocumentId and latestLog.NotificationNo=orgLogs.NotificationNo"+
                                " ON orgLogs.DocumentId = Document.DocumentId " +
                                //" INNER JOIN Organizations AS client ON Document.OrganizationId = client.OrganizationID " +
                                " INNER JOIN Organizations AS vendor ON orgLogs.VendorId = vendor.OrganizationID " +
                                " INNER JOIN Prequalification AS p ON orgLogs.PrequalificationId = p.PrequalificationId " +
                                " INNER JOIN PrequalificationStatus AS pstatus  " +
                                " ON p.PrequalificationStatusId = pstatus.PrequalificationStatusId  " +
                                " INNER JOIN OrganizationsNotificationsSent AS s " +
                                " ON s.NotificationId =orgLogs.NotificationId  " +
                                " INNER JOIN Organizations AS client ON s.ClientId = client.OrganizationID " +
                                " where(  " +
                                " Document.ReferenceType = 'Prequalification'  " +
                                " and Document.DocumentTypeId in(select * from dbo.ufn_ParseIntList(@doctypeList)) and p.PrequalificationStatusId in (select * from dbo.ufn_ParseIntList(@finalStatuses)) and Document.DocumentStatus != 'Deleted' and (orgLogs.RecordStatus != 'Archived' or orgLogs.RecordStatus is null) )) as t order by t.Client,t.Vendor,t.DocumentTypeName, t.Expires desc"; // Kiran on 1/6/2015 for displaying LIFO(Last In First Out) notifications // Kiran on 04/06/2015 to display notifications only for final statuses
            }

            else if (RoleName.Equals("Client") || RoleName.Equals("Super Client"))
            {
                reportValues = "select Distinct* from (Select DocType.DocumentTypeName,client.Name AS Client,vendor.Name AS Vendor, " +
                                "pstatus.PrequalificationStatusName AS Status,convert(varchar,orgLogs.EmailSentDateTime,110) AS EmailSentDate, " +
                                "case when latestLog.NotificationNo = 1 then 'First' when latestLog.NotificationNo = 2 then 'Second' when latestLog.NotificationNo = 3 then 'Third' end as NotificationNo," +
                                " convert(varchar,Document.Expires,110) AS Expires from Document " +
                                " INNER JOIN DocumentType AS DocType ON Document.DocumentTypeId = DocType.DocumentTypeId " +
                                " INNER JOIN Organizations AS org ON Document.OrganizationId = org.OrganizationID " +
                                " INNER JOIN OrganizationsNotificationsEmailLogs AS orgLogs " +
                                "inner join (select DocumentId,max(NotificationNo) NotificationNo from OrganizationsNotificationsEmailLogs group by DocumentId) as latestLog on latestLog.DocumentId=orgLogs.DocumentId and latestLog.NotificationNo=orgLogs.NotificationNo" +
                                " ON orgLogs.DocumentId = Document.DocumentId " +
                                // " INNER JOIN Organizations AS client ON Document.OrganizationId = client.OrganizationID " +
                                " INNER JOIN Organizations AS vendor ON orgLogs.VendorId = vendor.OrganizationID " +
                                " INNER JOIN Prequalification AS p ON orgLogs.PrequalificationId = p.PrequalificationId " +
                                " INNER JOIN PrequalificationStatus AS pstatus  " +
                                " ON p.PrequalificationStatusId = pstatus.PrequalificationStatusId  " +
                                " INNER JOIN OrganizationsNotificationsSent AS s " +
                                " ON s.NotificationId =orgLogs.NotificationId  " +
                                " INNER JOIN Organizations AS client ON s.ClientId = client.OrganizationID " +
                                " where(  " +
                                " Document.ReferenceType = 'Prequalification'  " +
                                " and s.ClientId in("+ClientIds+") " + PqClient+
                                " and Document.DocumentTypeId in(select * from dbo.ufn_ParseIntList(@doctypeList)) and p.PrequalificationStatusId in (select * from dbo.ufn_ParseIntList(@finalStatuses)) and Document.DocumentStatus != 'Deleted' and (orgLogs.RecordStatus != 'Archived' or orgLogs.RecordStatus is null) ) ) as t order by t.Client,t.Vendor,t.DocumentTypeName, t.Expires desc"; // Kiran on 1/6/2015 for displaying LIFO(Last In First Out) notifications // Kiran on 04/06/2015 to display notifications only for final statuses
            }

            var reportData = new ReportModel();
            reportData.Data = new List<KeyValuePair<string, string>>();
            reportData.Sql = reportValues;
            reportData.Data.Add(new KeyValuePair<string, string>("@doctypeList", doctypeList));
            reportData.Data.Add(new KeyValuePair<string, string>("@finalStatuses", finalStatuses));
            reportData.Data.Add(new KeyValuePair<string, string>("@ClientId", ClientId.ToStringNullSafe()));
            reportData.Data.Add(new KeyValuePair<string, string>("@ClientIds", ClientIds.ToStringNullSafe()));

            return reportData;

            //return Json(reportData, JsonRequestBehavior.AllowGet);

        }
    }
}