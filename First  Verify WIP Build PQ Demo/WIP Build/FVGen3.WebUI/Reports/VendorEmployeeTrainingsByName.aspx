﻿<%--Kiran on 12/02/2015--%>

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="VendorEmployeeTrainingsByName.aspx.cs" Inherits="FVGen3.WebUI.Reports.VendorEmployeeTrainingsByName" %>
<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>
<%@ Import Namespace="FVGen3.Domain.Entities"%>
<%@ Import Namespace="System.Data"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
        <style>
            #s5_logo_wrap {
                float: left;
                padding-top:4px;  
            }
            #s5_logo {
                cursor: pointer;
                margin-bottom: 21px;
                max-width: 200px;
            } 
        </style>
</head>

<script runat="server">
            private void Page_Load(object sender, System.EventArgs e)
            {
                try
                {
                    if (!this.IsPostBack)
                    {
                        //var sql = "";
                        //try
                        //{

                        //    sql = Request.Form["sql"].ToString();
                        //}
                        //catch
                        //{
                        //    sql = "";

                        //}
                        //if (sql != "")
                        //{
                        DateTime toDate = DateTime.Parse(Request.Form["trainingtoDate"]);
                        DateTime fromDate = DateTime.Parse(Request.Form["trainingfromDate"]);
                        var result = GetTraining(long.Parse(Request.Form["clientOrgId"]), Request.Form["ids"], Request.Form["baseids"], fromDate, toDate);
                            ReportViewer1.LocalReport.DataSources.Clear();
                            ReportViewer1.ProcessingMode = ProcessingMode.Local;

                            ReportViewer1.LocalReport.ReportPath = Server.MapPath("VendorEmployeeTrainingsByName.rdlc");

                            DataSet dataSet = new DataSet();

                            System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["EFDbContext"].ConnectionString);
                            connection.Open();
                            System.Data.SqlClient.SqlDataAdapter adapter = new System.Data.SqlClient.SqlDataAdapter(result.Sql, connection);
                            var data = result.Data;
                            foreach (var d in data)
                            {                                
                                adapter.SelectCommand.Parameters.AddWithValue(d.Key, d.Value);
                            }
                            adapter.SelectCommand.CommandTimeout = 300;
                        
                            adapter.Fill(dataSet);
                            
                            ReportDataSource dataSource = new ReportDataSource("DataSet1", dataSet.Tables[0]);

                            ReportViewer1.LocalReport.DataSources.Add(dataSource);
                            ReportViewer1.DocumentMapCollapsed = true;
                            ReportViewer1.LocalReport.Refresh();

                            ReportViewer1.Visible = true;
                        //}
                        //else
                        //{
                        //    Response.Redirect("~/AdminVendors/CustomReports");
                        //}
                    }
                
                }
                catch(Exception ex)
                {
                    throw ex;
                }
            }

            protected void SqlDataSource1_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
            {

            }

            protected void PrequalificationDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
            {

            }
</script>    

<body>
    <form id="form1" runat="server">
        <table width="100%" border="0">
            <tr>
                <td>
                    <div id ="s5_logo_wrap">
                        <img alt="logo" src="../images/logoForReports.png" id="s5_logo" />
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                   <asp:ScriptManager ID="ScriptManager1" runat="server">
                    </asp:ScriptManager>
                    <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Arial Narrow" 
                        Font-Size="8pt" InteractiveDeviceInfos="(Collection)" 
                        WaitMessageFont-Names="Arial Narrow" WaitMessageFont-Size="14pt" Height="674px" 
                        Width="100%">
                        <LocalReport ReportPath="Reports\VendorEmployeeTrainingsByName.rdlc">
                            <DataSources>
                                <rsweb:ReportDataSource DataSourceId="VendorEmployeeQuizzesDataSource" 
                                    Name="VendorEmployeeQuizzesDataSet" />
                            </DataSources>
                        </LocalReport>
                    </rsweb:ReportViewer>
    
                    <asp:ObjectDataSource ID="VendorEmployeeQuizzesDataSource" runat="server" 
                        OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
                        TypeName="FVGen3.WebUI.Reports.VendorEmployeeQuizzesDataSetTableAdapters.VendorEmployeeeQuizzesDataTable1TableAdapter">
                    </asp:ObjectDataSource>
                </td>
            
            </tr>
        </table>

    </form>
</body>
</html>
