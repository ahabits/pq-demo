﻿using FVGen3.WebUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AhaApps.Libraries.Extensions;
using Microsoft.Reporting.WebForms;
using System.Data.Entity;
using System.Data;

namespace FVGen3.WebUI.Reports
{
    public partial class InvitationsSent : System.Web.UI.Page
    {
        private void Page_Load(object sender, System.EventArgs e)
        {
            if (!this.IsPostBack)
            {
                // sumanth on 19/08/2015
                // added try & catch block to handle exceptions and if the address bar is changed then it is redirected to previous page.Refer FVOS-1 

                try
                {

                    //var sql = "";
                    //try
                    //{

                    //    sql = Request.Form["sql"].ToString();
                    //}
                    //catch
                    //{
                    //    sql = "";

                    //}
                    //if (sql != "")
                    //{
                    //var clientid =0l;
                    //if (!string.IsNullOrEmpty(Request.Form["ClientId"])) {clientid = long.Parse(Request.Form["ClientId"]); } else { clientid = clientid; }
                    var result = GetInvitationSent(Request.Form["ClientId"], Request.Form["fromDate"], Request.Form["toDate"]);
                    ReportViewer1.LocalReport.DataSources.Clear();
                    ReportViewer1.ProcessingMode = ProcessingMode.Local;

                    ReportViewer1.LocalReport.ReportPath = Server.MapPath("InvitationsSentReport.rdlc");

                    DataSet dataSet = new DataSet();

                    System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["EFDbContext"].ConnectionString);
                    connection.Open();
                    System.Data.SqlClient.SqlDataAdapter adapter = new System.Data.SqlClient.SqlDataAdapter(result.Sql, connection);

                    var data = result.Data;
                    foreach (var d in data)
                    {
                        adapter.SelectCommand.Parameters.AddWithValue(d.Key, d.Value);
                        //var param=new System.Data.SqlClient.SqlParameter();
                        //param.DbType=DbType.DateTime;
                        //param.ParameterName=d.Key;
                        //param.Value=d.Value;
                        //adapter.SelectCommand.Parameters.Add(param);
                    }
                    adapter.SelectCommand.CommandTimeout = 300;

                    adapter.Fill(dataSet);

                    ReportDataSource dataSource = new ReportDataSource("InvitationsSentDataSet", dataSet.Tables[0]);

                    ReportViewer1.LocalReport.DataSources.Add(dataSource);
                    ReportViewer1.DocumentMapCollapsed = true;
                    ReportViewer1.LocalReport.Refresh();

                    ReportViewer1.Visible = true;
                    //}
                    //else
                    //{
                    //    Response.Redirect("~/AdminVendors/CustomReports");                        
                    //}
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        public ReportModel GetInvitationSent(string ClientId,string fromDate, string toDate)
        {
            var data = "";
            if (@ClientId != ""&& Session["RoleName"].ToString() == "Admin") {
                data = "select vid.VendorCompanyName, vid.VendorFirstName, vid.VendorLastName, vid.Address1, vid.Address2, vid.City, vid.State, vid.Zip, vid.Country, vid.VendorEmail, vid.InvitationDate, org.Name,CASE WHEN vid.VendorId IS NULL then 'No' else 'Yes' end as IsAccepted  from VendorInviteDetails as vid INNER JOIN Organizations as org on vid.ClientId = org.OrganizationID where InvitationDate >=@fromDate AND InvitationDate <=@toDate AND vid.ClientId=@ClientId";
            }
            else if(@ClientId == "" && Session["RoleName"].ToString() == "Admin")
            {
                data = "select vid.VendorCompanyName, vid.VendorFirstName, vid.VendorLastName, vid.Address1, vid.Address2, vid.City, vid.State, vid.Zip, vid.Country, vid.VendorEmail, vid.InvitationDate, org.Name,CASE WHEN vid.VendorId IS NULL then 'No' else 'Yes' end as IsAccepted  from VendorInviteDetails as vid INNER JOIN Organizations as org on vid.ClientId = org.OrganizationID where InvitationDate >=@fromDate AND InvitationDate <=@toDate";
            }
            if (Session["RoleName"].ToString() == "Client"|| Session["RoleName"].ToString() == "Super Client")
            {
                data = "select vid.VendorCompanyName, vid.VendorFirstName, vid.VendorLastName, vid.Address1, vid.Address2, vid.City,vid.State, vid.Zip, vid.Country, vid.VendorEmail, vid.InvitationDate,CASE WHEN vid.VendorId IS NULL then 'No' else 'Yes' end as IsAccepted from VendorInviteDetails as vid INNER JOIN Organizations as org on vid.ClientId = org.OrganizationID where vid.ClientId ='" + Session["currentOrgId"] + "' AND InvitationDate >= @fromDate AND InvitationDate <=@toDate";
            }


            var reportData = new ReportModel();
            reportData.Data = new List<KeyValuePair<string, string>>();
            reportData.Sql = data;
            reportData.Data.Add(new KeyValuePair<string, string>("@fromDate", fromDate.ToStringNullSafe()));
            reportData.Data.Add(new KeyValuePair<string, string>("@toDate", toDate.ToStringNullSafe()));
            reportData.Data.Add(new KeyValuePair<string, string>("@ClientId", ClientId.ToStringNullSafe()));
            return reportData;
        }
    }
}