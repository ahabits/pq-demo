﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="VendorEmployeeQuizzesReport.aspx.cs" Inherits="FVGen3.WebUI.Reports.VendorEmployeeQuizzesReport" %>
<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>
<%@ Import Namespace="FVGen3.Domain.Entities"%>
<%@ Import Namespace="System.Data"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
        <style>
            #s5_logo_wrap {
                float: left;
                padding-top:4px;  
            }
            #s5_logo {
                cursor: pointer;
                margin-bottom: 21px;
                max-width: 200px;
            } 
        </style>
</head>

<script runat="server">
            private void Page_Load(object sender, System.EventArgs e)
            {
                try
                {

                    if (!this.IsPostBack)
                    {
                        // sumanth on 19/08/2015
                        // added try & catch block to handle exceptions and if the address bar is changed then it is redirected to previous page.Refer FVOS-1 
                        //var sql = "";
                        //try
                        //{

                        //    sql = Request.Form["sql"].ToString();
                        //}
                        //catch
                        //{
                        //    sql = "";

                        //}
                        //if (sql != "")
                        //{
                            var result = EmployeeQuizReport(Request.Form["clientOrgId"], Request.Form["ids"]);
                            
                            ReportViewer1.LocalReport.DataSources.Clear();
                            ReportViewer1.ProcessingMode = ProcessingMode.Local;

                            ReportViewer1.LocalReport.ReportPath = Server.MapPath("VendorEmployeeQuizzesReport.rdlc");

                            DataSet dataSet = new DataSet();

                            System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["EFDbContext"].ConnectionString);
                            connection.Open();
                            //string sql = "SELECT Templates.TemplateName, Contact.FirstName + Contact.LastName AS EmpName, EmployeeQuizzes.QuizResult, EmployeeQuizzes.QuizDateStart, vendor.Name as Vendor, client.Name AS Client FROM     EmployeeQuizzes INNER JOIN Organizations AS vendor ON EmployeeQuizzes.VendorId = vendor.OrganizationID INNER JOIN Organizations AS client ON EmployeeQuizzes.ClientId = client.OrganizationID INNER JOIN ClientTemplates ON EmployeeQuizzes.QuizClientTemplateId = ClientTemplates.ClientTemplateID INNER JOIN Templates ON ClientTemplates.TemplateID = Templates.TemplateID INNER JOIN Contact ON EmployeeQuizzes.EmpId = Contact.UserId GROUP BY vendor.Name, client.Name, Templates.TemplateName, Contact.FirstName + Contact.LastName, EmployeeQuizzes.QuizResult, EmployeeQuizzes.QuizDateStart";
                            System.Data.SqlClient.SqlDataAdapter adapter = new System.Data.SqlClient.SqlDataAdapter(result.Sql, connection);
                            var data = result.Data; 
                            foreach (var d in data)
                            {
                                dynamic val = d.Value;                                
                                adapter.SelectCommand.Parameters.AddWithValue(d.Key, val);
                            }
                            adapter.SelectCommand.CommandTimeout = 300;
                            
                            
                            adapter.Fill(dataSet);
                            //dataSet.ReadXml(MapPath("ClientDataSet.xsd"));

                            ReportDataSource dataSource = new ReportDataSource("DataSet1", dataSet.Tables[0]);

                            ReportViewer1.LocalReport.DataSources.Add(dataSource);
                            ReportViewer1.DocumentMapCollapsed = true;
                            ReportViewer1.LocalReport.Refresh();

                            ReportViewer1.Visible = true;
                        //}
                        //else
                        //{
                        //    Response.Redirect("~/AdminVendors/CustomReports");
                        //}
                    }
                
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            protected void SqlDataSource1_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
            {

            }

            protected void PrequalificationDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
            {

            }
</script>    

<body>
    <form id="form1" runat="server">
        <table width="100%" border="0">
            <tr>
                <td>
                    <div id ="s5_logo_wrap">
                        <img alt="logo" src="../images/logoForReports.png" id="s5_logo" />
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                   <asp:ScriptManager ID="ScriptManager1" runat="server">
                    </asp:ScriptManager>
                    <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Arial Narrow" 
                        Font-Size="8pt" InteractiveDeviceInfos="(Collection)" 
                        WaitMessageFont-Names="Arial Narrow" WaitMessageFont-Size="14pt" Height="674px" 
                        Width="100%">
                        <LocalReport ReportPath="Reports\VendorEmployeeQuizzesReport.rdlc">
                            <DataSources>
                                <rsweb:ReportDataSource DataSourceId="VendorEmployeeQuizzesDataSource" 
                                    Name="VendorEmployeeQuizzesDataSet" />
                            </DataSources>
                        </LocalReport>
                    </rsweb:ReportViewer>
    
                    <asp:ObjectDataSource ID="VendorEmployeeQuizzesDataSource" runat="server" 
                        OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
                        TypeName="FVGen3.WebUI.Reports.VendorEmployeeQuizzesDataSetTableAdapters.VendorEmployeeeQuizzesDataTable1TableAdapter">
                    </asp:ObjectDataSource>
                </td>
            
            </tr>
        </table>

    </form>
</body>
</html>
