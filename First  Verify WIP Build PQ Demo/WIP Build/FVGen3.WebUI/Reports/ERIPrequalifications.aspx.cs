﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FVGen3.WebUI.Models;
using System.Data.Entity;
using FVGen3.BusinessLogic.Interfaces;
using Microsoft.Reporting.WebForms;
using FVGen3.Domain.Concrete;
using FVGen3.Domain.Entities;
using FVGen3.DataLayer.DTO;
using System.Data.SqlClient;
using System.Data;

namespace FVGen3.WebUI.Reports
{
    public partial class ERIPrequalifications : System.Web.UI.Page
    {
        private IPrequalificationBusiness _prequalification;
        EFDbContext entityDB = new EFDbContext();
        public ERIPrequalifications(IPrequalificationBusiness _prequalification)
        {
            this._prequalification = _prequalification;
        }
        public ERIPrequalifications() : base()
        {
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {

                Guid? templateId = null;
                    
                if(Request.Form["TemplateId"].ToString() != "All")
                templateId = Guid.Parse(Request.Form["TemplateId"].ToString());
                DataSet eriPrequalifications = getERIPrequalifications(templateId);
                
                ReportDataSource dataSource = new ReportDataSource("ERIPrequalificationsDataSet", eriPrequalifications.Tables[0]);
                ReportViewer1.LocalReport.DataSources.Clear();
                ReportViewer1.ProcessingMode = ProcessingMode.Local;

                ReportViewer1.LocalReport.ReportPath = Server.MapPath("ERIPrequalifications.rdlc");
                ReportViewer1.LocalReport.DataSources.Add(dataSource);
                ReportViewer1.DocumentMapCollapsed = true;
                ReportViewer1.LocalReport.Refresh();

                ReportViewer1.Visible = true;
            }
        }
        private DataSet getERIPrequalifications(Guid? templateId)
        {
            string superUserRoleId = entityDB.SystemRoles.FirstOrDefault(row => row.RoleName == "Super User" && row.OrganizationType == "Vendor").RoleId;
            DataSet eriPrequalifications = new DataSet();
            eriPrequalifications.Tables.Add(new DataTable());
            eriPrequalifications.Tables[0].Columns.Add("Name");
            eriPrequalifications.Tables[0].Columns.Add("Address1");
            eriPrequalifications.Tables[0].Columns.Add("Address2");
            eriPrequalifications.Tables[0].Columns.Add("City");
            eriPrequalifications.Tables[0].Columns.Add("State");
            eriPrequalifications.Tables[0].Columns.Add("Country");
            eriPrequalifications.Tables[0].Columns.Add("Zip");
            eriPrequalifications.Tables[0].Columns.Add("FirstName");
            eriPrequalifications.Tables[0].Columns.Add("LastName");
            eriPrequalifications.Tables[0].Columns.Add("MobileNumber");
            eriPrequalifications.Tables[0].Columns.Add("Email");
            eriPrequalifications.Tables[0].Columns.Add("TemplateCode");
            eriPrequalifications.Tables[0].Columns.Add("PrequalificationStatusName");
            eriPrequalifications.Tables[0].Columns.Add("PrequalificationStart");
            eriPrequalifications.Tables[0].Columns.Add("PrequalificationFinish");
            eriPrequalifications.Tables[0].Columns.Add("VendorId");
            eriPrequalifications.Tables[0].Columns.Add("Clients");
            var prequalifications = new List<Prequalification>();
            if (templateId == null)
                prequalifications = entityDB.Prequalification.SqlQuery("select * from LatestPrequalification where ClientId = -1").ToList();

            else
                prequalifications = entityDB.Prequalification.SqlQuery("select lp.* from LatestPrequalification lp join clienttemplates ct on lp.clienttemplateid=ct.clienttemplateid where ct.TemplateID = @templateId", new SqlParameter("@templateId", templateId)).ToList();

            foreach (var prequalification in prequalifications)
            {
                var superUserDetails = getSuperUser(prequalification.VendorId);
                var pqClients = prequalification.PrequalificationClient.ClientIdsList;

                var clientNames = string.Join(",", entityDB.Organizations.Where(rec => pqClients.Contains(rec.OrganizationID)).Select(row => row.Name));

                DataRow dr = eriPrequalifications.Tables[0].NewRow();
                dr["Name"] = prequalification.Vendor.Name;
                dr["Address1"] = prequalification.Vendor.Address1;
                dr["Address2"] = prequalification.Vendor.Address2;
                dr["City"] = prequalification.Vendor.City;
                dr["State"] = prequalification.Vendor.State;
                dr["Country"] = prequalification.Vendor.Country;
                dr["Zip"] = prequalification.Vendor.Zip;
                dr["FirstName"] = superUserDetails.FirstName;
                dr["LastName"] = superUserDetails.LastName;
                dr["MobileNumber"] = superUserDetails.MobileNumber;
                dr["Email"] = superUserDetails.Email;
                dr["TemplateCode"] = prequalification.ClientTemplates.Templates.TemplateCode;
                dr["PrequalificationStatusName"] = prequalification.PrequalificationStatus.PrequalificationStatusName;
                dr["PrequalificationStart"] = prequalification.PrequalificationStart.ToString("MM/dd/yyyy");
                dr["PrequalificationFinish"] = prequalification.PrequalificationFinish.ToString("MM/dd/yyyy");
                dr["VendorId"] = prequalification.VendorId;
                dr["Clients"] = clientNames;

                eriPrequalifications.Tables[0].Rows.Add(dr);
            }
            return eriPrequalifications;
        }
        private UserInfo getSuperUser(long vendorId)
        {
            var user =
                    entityDB.SystemUsersOrganizations.FirstOrDefault(
                        r => r.OrganizationId == vendorId && r.PrimaryUser == true).SystemUsers;
            var result = new UserInfo();
            if (user != null)
            {
                result.Email = user.Email;
                result.FirstName = user.Contacts[0].FirstName;
                result.LastName = user.Contacts[0].LastName;
                result.MobileNumber = user.Contacts[0].MobileNumber;
            }
            return result;
        }
    }
}