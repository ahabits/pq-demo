﻿using System;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using FVGen3.WebUI.Models;
using FVGen3.WebUI.Utilities;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FVGen3.Domain.Concrete;
using System.Data;

namespace FVGen3.WebUI.Reports
{
    public partial class FinancialAnalytics1 : System.Web.UI.Page
    {
        EFDbContext entityDB;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                var result = CustomReportGeneration(long.Parse(Request.Form["ClientId"]));
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("FinancialAnalyticsReport.rdlc");

                DataSet dataSet = new DataSet();

                System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["EFDbContext"].ConnectionString);



                connection.Open();
                System.Data.SqlClient.SqlDataAdapter adapter = new System.Data.SqlClient.SqlDataAdapter(result.Sql, connection);

                var data = result.Data; //Newtonsoft.Json.JsonConvert.DeserializeObject<List<KeyValuePair<string,string>>>(Request.Form["data"]);
                foreach (var d in data)
                {
                    adapter.SelectCommand.Parameters.AddWithValue(d.Key, d.Value);
                }
                adapter.SelectCommand.CommandTimeout = 300;
                adapter.Fill(dataSet);

                ReportDataSource dataSource = new ReportDataSource("FinancialAnalyticsDataSet", dataSet.Tables[0]);

                ReportViewer1.LocalReport.DataSources.Add(dataSource);
                ReportViewer1.DocumentMapCollapsed = true;
                ReportViewer1.LocalReport.Refresh();

                ReportViewer1.Visible = true;
            }

        }
        public ReportModel CustomReportGeneration(long clientId)
        {
            var result = new ReportModel();
            result.Sql = @"select  o.Name,lp.PrequalificationId,year(Fiscalyearenddate) as YEAR,f.* from LatestPrequalification as lp
join Organizations as o
on o.OrganizationID=lp.VendorId
join(SELECT clientid,  VendorId,SourceOfFinancialStatements as Typeoffinancialstatements,
DATEADD(year, 1, DateOfFinancialStatements)  as Fiscalyearenddate,
Dateofinterimfinancialstatement as Dateofinterimfinancialstatement,
 (CashAndCashEquivalents +Investments+ReceivablesCurrent +ReceivableRetainage +
                        CostsAndEstimatedEarningsInExcessOfBillingsOnUncompletedContracts+RefundableIncomeTaxes +
                      NoteReceivable+OtherCurrentAssets) - (LinesOfCreditShortTermBorrowings+InstallmentNotesPayableDueWithinOneYear+AccountsPayable +RetainagePayable +AccruedExpenses+
                        BillingsInExcessOfCostsAndEstimatedEarningsOnUncompletedContracts+CurrentIncomeTaxesPayable +
                      DeferredIncomeTaxesPayableCurrent)
					    as Workingcapital,
				iif(GeneralAndAdministrativeExpenses<>0 ,((CashAndCashEquivalents +Investments +ReceivablesCurrent +ReceivableRetainage +
                        CostsAndEstimatedEarningsInExcessOfBillingsOnUncompletedContracts+RefundableIncomeTaxes +
                       NoteReceivable+OtherCurrentAssets) - (LinesOfCreditShortTermBorrowings+InstallmentNotesPayableDueWithinOneYear+AccountsPayable +RetainagePayable +AccruedExpenses +
                        BillingsInExcessOfCostsAndEstimatedEarningsOnUncompletedContracts+CurrentIncomeTaxesPayable +
                       DeferredIncomeTaxesPayableCurrent))/(GeneralAndAdministrativeExpenses/365),0)
					   as Daysoverheadcoveredinworkingcapital,
					   iif(ConstructionRevenues<>0 ,((CashAndCashEquivalents+Investments +ReceivablesCurrent+ReceivableRetainage +
                       CostsAndEstimatedEarningsInExcessOfBillingsOnUncompletedContracts+RefundableIncomeTaxes +
                      NoteReceivable+OtherCurrentAssets) - (LinesOfCreditShortTermBorrowings+InstallmentNotesPayableDueWithinOneYear+AccountsPayable+RetainagePayable +AccruedExpenses +
                        BillingsInExcessOfCostsAndEstimatedEarningsOnUncompletedContracts+CurrentIncomeTaxesPayable +
                     DeferredIncomeTaxesPayableCurrent)) /ConstructionRevenues* 100,0)
					   as Workingcapitalpercenttorevenue ,
					  iif((CashAndCashEquivalents +Investments+ReceivablesCurrent +ReceivableRetainage +
                        CostsAndEstimatedEarningsInExcessOfBillingsOnUncompletedContracts+RefundableIncomeTaxes +
                       NoteReceivable+OtherCurrentAssets) - (LinesOfCreditShortTermBorrowings+InstallmentNotesPayableDueWithinOneYear+AccountsPayable +RetainagePayable +AccruedExpenses +
                      BillingsInExcessOfCostsAndEstimatedEarningsOnUncompletedContracts+CurrentIncomeTaxesPayable +
                    DeferredIncomeTaxesPayableCurrent)<>0 ,ConstructionRevenues/ ((CashAndCashEquivalents +Investments +ReceivablesCurrent+ReceivableRetainage +
                        CostsAndEstimatedEarningsInExcessOfBillingsOnUncompletedContracts+RefundableIncomeTaxes +
                       NoteReceivable+OtherCurrentAssets)-(LinesOfCreditShortTermBorrowings+InstallmentNotesPayableDueWithinOneYear+AccountsPayable+RetainagePayable +AccruedExpenses +
                        BillingsInExcessOfCostsAndEstimatedEarningsOnUncompletedContracts+CurrentIncomeTaxesPayable +
                      DeferredIncomeTaxesPayableCurrent)),0)
					    as Workingcapitalturnover,
					IIF((LinesOfCreditShortTermBorrowings+
    InstallmentNotesPayableDueWithinOneYear + AccountsPayable +RetainagePayable +
   AccruedExpenses +BillingsInExcessOfCostsAndEstimatedEarningsOnUncompletedContracts+
   CurrentIncomeTaxesPayable+DeferredIncomeTaxesPayableCurrent)<>0,(CashAndCashEquivalents+Investments +ReceivablesCurrent +ReceivableRetainage +
                        CostsAndEstimatedEarningsInExcessOfBillingsOnUncompletedContracts+RefundableIncomeTaxes +
                      NoteReceivable+OtherCurrentAssets)/ (LinesOfCreditShortTermBorrowings+
    InstallmentNotesPayableDueWithinOneYear +AccountsPayable+ RetainagePayable +
    AccruedExpenses+BillingsInExcessOfCostsAndEstimatedEarningsOnUncompletedContracts +
  CurrentIncomeTaxesPayable+ DeferredIncomeTaxesPayableCurrent),0)
    as CurrentRatio,
		IIF((LinesOfCreditShortTermBorrowings +
    InstallmentNotesPayableDueWithinOneYear + AccountsPayable+ RetainagePayable +
    AccruedExpenses  +BillingsInExcessOfCostsAndEstimatedEarningsOnUncompletedContracts+
  CurrentIncomeTaxesPayable + DeferredIncomeTaxesPayableCurrent)<>0,(CashAndCashEquivalents+Investments +ReceivablesCurrent +ReceivableRetainage)/ (LinesOfCreditShortTermBorrowings+
    InstallmentNotesPayableDueWithinOneYear + AccountsPayable + RetainagePayable +
    AccruedExpenses  +BillingsInExcessOfCostsAndEstimatedEarningsOnUncompletedContracts +
   CurrentIncomeTaxesPayable + DeferredIncomeTaxesPayableCurrent),0)
    as Quickratio,
  IIF((ConstructionDirectCosts + GeneralAndAdministrativeExpenses)<>0,(CashAndCashEquivalents +Investments +ReceivablesCurrent +ReceivableRetainage)/ ((ConstructionDirectCosts +GeneralAndAdministrativeExpenses)/365),0) 
  as Defensiveintervalratio,
  BillingsInExcessOfCostsAndEstimatedEarningsOnUncompletedContracts-CostsAndEstimatedEarningsInExcessOfBillingsOnUncompletedContracts 
   as NetOverBillings,
      iif(ConstructionRevenues<>0,(BillingsInExcessOfCostsAndEstimatedEarningsOnUncompletedContracts-CostsAndEstimatedEarningsInExcessOfBillingsOnUncompletedContracts)/ConstructionRevenues* 100,0) as NetOverBillingsPercentRevenue,
	  iif(ConstructionRevenues<>0 ,ReceivablesCurrent/(ConstructionRevenues / 360),0)
	 as DaysRevenueReceivable,
	   iif(ConstructionRevenues<>0 ,(CashAndCashEquivalents + Investments) * 360 /ConstructionRevenues,0)
	  as DaysCash,
	   IIF(BillingsInExcessOfCostsAndEstimatedEarningsOnUncompletedContracts<>0 ,(CashAndCashEquivalents+ Investments) / BillingsInExcessOfCostsAndEstimatedEarningsOnUncompletedContracts,0)
	    as CashOverBillings,
		IIF(TotalStockholdersEquity<>0 ,((CashAndCashEquivalents +Investments) /TotalStockholdersEquity) * 100,0)
		as CashEquity,
		iif(ConstructionRevenues<>0 ,((CashAndCashEquivalents + Investments) /ConstructionRevenues) * 100,0)
		as CashRevenue,
		LinesOfCreditShortTermBorrowings+ InstallmentNotesPayableDueWithinOneYear
		 as LinesCreditPlusInstalNotesPaybledue,
		IIF((LinesOfCreditShortTermBorrowings + InstallmentNotesPayableDueWithinOneYear)<>0,(ConstructionRevenues -ConstructionDirectCosts + NonConstructionGrossProfitLoss -
    GeneralAndAdministrativeExpenses + OtherIncomeDeductions + DepreciationAndAmortization  + InterestExpense+DepreciationAndAmortization ) / (LinesOfCreditShortTermBorrowings + InstallmentNotesPayableDueWithinOneYear),0)
		as DebtCoverageRatio,
	 case when ((CashAndCashEquivalents +Investments +ReceivablesCurrent +ReceivableRetainage +CostsAndEstimatedEarningsInExcessOfBillingsOnUncompletedContracts+RefundableIncomeTaxes+
                      NoteReceivable+OtherCurrentAssets) - (InstallmentNotesPayableDueWithinOneYear+AccountsPayable +RetainagePayable +AccruedExpenses +
                        BillingsInExcessOfCostsAndEstimatedEarningsOnUncompletedContracts+CurrentIncomeTaxesPayable +
                       DeferredIncomeTaxesPayableCurrent))< TotalStockholdersEquity then ((CashAndCashEquivalents+Investments+ReceivablesCurrent +ReceivableRetainage +CostsAndEstimatedEarningsInExcessOfBillingsOnUncompletedContracts+RefundableIncomeTaxes +
                    NoteReceivable+OtherCurrentAssets) - (InstallmentNotesPayableDueWithinOneYear+AccountsPayable +RetainagePayable +AccruedExpenses +
                     BillingsInExcessOfCostsAndEstimatedEarningsOnUncompletedContracts+CurrentIncomeTaxesPayable +
                   DeferredIncomeTaxesPayableCurrent))*15
					   else TotalStockholdersEquity*15 End BondingCapacity,

					 iif(( case when ((CashAndCashEquivalents +Investments +ReceivablesCurrent+ReceivableRetainage+CostsAndEstimatedEarningsInExcessOfBillingsOnUncompletedContracts+RefundableIncomeTaxes +
                      NoteReceivable+OtherCurrentAssets) - (InstallmentNotesPayableDueWithinOneYear +AccountsPayable +RetainagePayable +AccruedExpenses+
                        BillingsInExcessOfCostsAndEstimatedEarningsOnUncompletedContracts+CurrentIncomeTaxesPayable +
                      DeferredIncomeTaxesPayableCurrent))< TotalStockholdersEquity  then ((CashAndCashEquivalents+Investments +ReceivablesCurrent +ReceivableRetainage +CostsAndEstimatedEarningsInExcessOfBillingsOnUncompletedContracts+RefundableIncomeTaxes +
                    NoteReceivable+OtherCurrentAssets) - (InstallmentNotesPayableDueWithinOneYear +AccountsPayable +RetainagePayable +AccruedExpenses +
                       BillingsInExcessOfCostsAndEstimatedEarningsOnUncompletedContracts+CurrentIncomeTaxesPayable +
                       DeferredIncomeTaxesPayableCurrent))*15
					   else TotalStockholdersEquity*15 end )<>0, ( TotalBacklog /  case when ((CashAndCashEquivalents+Investments+ReceivablesCurrent +ReceivableRetainage +CostsAndEstimatedEarningsInExcessOfBillingsOnUncompletedContracts+RefundableIncomeTaxes +
                       NoteReceivable+OtherCurrentAssets) - (InstallmentNotesPayableDueWithinOneYear+AccountsPayable +RetainagePayable +AccruedExpenses +
                       BillingsInExcessOfCostsAndEstimatedEarningsOnUncompletedContracts+CurrentIncomeTaxesPayable +
                      DeferredIncomeTaxesPayableCurrent))< TotalStockholdersEquity then ((CashAndCashEquivalents+Investments+ReceivablesCurrent +ReceivableRetainage +CostsAndEstimatedEarningsInExcessOfBillingsOnUncompletedContracts+RefundableIncomeTaxes +
                       NoteReceivable+OtherCurrentAssets) - (InstallmentNotesPayableDueWithinOneYear+AccountsPayable +RetainagePayable +AccruedExpenses+
                       BillingsInExcessOfCostsAndEstimatedEarningsOnUncompletedContracts+CurrentIncomeTaxesPayable +
                      DeferredIncomeTaxesPayableCurrent))*15
					   else TotalStockholdersEquity *15 end ) * 100,0)
					     as BacklogPercentageBondingCapacity,
					IIF(ConstructionRevenues<>0 ,(TotalBacklog /ConstructionRevenues) * 100,0)
					 as BacklogPercentageRevenue,
					IIF(ConstructionRevenues<>0  and GeneralAndAdministrativeExpenses<>0 , ((ConstructionRevenues - ConstructionDirectCosts) * TotalBacklog/ ConstructionRevenues) / (GeneralAndAdministrativeExpenses* 100),0)
					 as BacklogGrossProfitGAExpense,
                       IIF(ConstructionRevenues<>0 ,TotalBacklog  / (ConstructionRevenues / 12),0)
					    as MonthsRevenueBacklog,
					     IIF(ConstructionRevenues<>0 ,(TotalStockholdersEquity / ConstructionRevenues) * 100,0)
					     as EquityRevenue,
						 IIF(TotalStockholdersEquity<>0 ,(NotesPayableDueAfterOneYear+ EquipmentLinesOfCredit + LinesOfCreditShortTermBorrowings +InstallmentNotesPayableDueWithinOneYear ) / TotalStockholdersEquity,0)
						  as InterestBearingDebtEquity,
						  IIF( ((CashAndCashEquivalents+Investments +ReceivablesCurrent +ReceivableRetainage +
                       CostsAndEstimatedEarningsInExcessOfBillingsOnUncompletedContracts+RefundableIncomeTaxes +
                       NoteReceivable+OtherCurrentAssets) - (InstallmentNotesPayableDueWithinOneYear +AccountsPayable +RetainagePayable +AccruedExpenses +
                       BillingsInExcessOfCostsAndEstimatedEarningsOnUncompletedContracts+CurrentIncomeTaxesPayable +
                       DeferredIncomeTaxesPayableCurrent)) <>0,(NotesPayableDueAfterOneYear + EquipmentLinesOfCredit +LinesOfCreditShortTermBorrowings ) / ((CashAndCashEquivalents+Investments +ReceivablesCurrent +ReceivableRetainage +
                       CostsAndEstimatedEarningsInExcessOfBillingsOnUncompletedContracts+RefundableIncomeTaxes +
                       NoteReceivable+OtherCurrentAssets) - (InstallmentNotesPayableDueWithinOneYear+AccountsPayable +RetainagePayable +AccruedExpenses +
                        BillingsInExcessOfCostsAndEstimatedEarningsOnUncompletedContracts+CurrentIncomeTaxesPayable +
                     DeferredIncomeTaxesPayableCurrent)),0) 
					   as DebtWorkingCapital,
					    IIF(TotalStockholdersEquity<>0 ,((LinesOfCreditShortTermBorrowings+
     InstallmentNotesPayableDueWithinOneYear +AccountsPayable +RetainagePayable +
     AccruedExpenses + BillingsInExcessOfCostsAndEstimatedEarningsOnUncompletedContracts +
    CurrentIncomeTaxesPayable + DeferredIncomeTaxesPayableCurrent + NotesPayableDueAfterOneYear +EquipmentLinesOfCredit +
    OtherLongTermLiabilities + DeferredIncomeTaxesPayableLongTerm) /TotalStockholdersEquity ) * 100,0) 
	as TotalLiabilitiesToEquity,
	IIF(ConstructionRevenues<>0 ,((ConstructionRevenues - ConstructionDirectCosts  +NonConstructionGrossProfitLoss) /ConstructionRevenues) * 100,0)
			as GrossProfitPercentage,
			IIF(ConstructionRevenues<>0 ,(GeneralAndAdministrativeExpenses  / ConstructionRevenues) * 100,0)
			as GAPercentage,
			IIF(ConstructionRevenues<>0 ,((ConstructionRevenues -ConstructionDirectCosts+ NonConstructionGrossProfitLoss -
    GeneralAndAdministrativeExpenses+ OtherIncomeDeductions) /ConstructionRevenues) * 100,0) 
			as PreTaxEarningsPercentage,
			IIF(TotalStockholdersEquity<>0 ,((ConstructionRevenues - ConstructionDirectCosts +NonConstructionGrossProfitLoss-
   GeneralAndAdministrativeExpenses+OtherIncomeDeductions)/ TotalStockholdersEquity) * 100,0)	
			as PreTaxReturnCurrentEquity,
			 IIF((CashAndCashEquivalents + Investments+ ReceivablesCurrent + ReceivableRetainage +
                      CostsAndEstimatedEarningsInExcessOfBillingsOnUncompletedContracts + RefundableIncomeTaxes +
                       NoteReceivable +OtherCurrentAssets + NetPropertyAndEquipment + OtherAssets)<>0,(ConstructionRevenues - ConstructionDirectCosts + NonConstructionGrossProfitLoss -
   GeneralAndAdministrativeExpenses+OtherIncomeDeductions)/ (CashAndCashEquivalents + Investments +ReceivablesCurrent + ReceivableRetainage +
                        CostsAndEstimatedEarningsInExcessOfBillingsOnUncompletedContracts + RefundableIncomeTaxes +
                     NoteReceivable + OtherCurrentAssets + NetPropertyAndEquipment + OtherAssets) * 100,0)
			as PreTaxReturnCurrentAssets,
					 IIF(TotalStockholdersEquity<>0 ,((ConstructionRevenues - ConstructionDirectCosts + NonConstructionGrossProfitLoss -
    GeneralAndAdministrativeExpenses+ OtherIncomeDeductions -IncomeTaxExpenseBenifit) / TotalStockholdersEquity) * 100,0)
					  as AfterTaxReturnCurrentEquity,
					IIF((CashAndCashEquivalents +Investments + ReceivablesCurrent + ReceivableRetainage +
                        CostsAndEstimatedEarningsInExcessOfBillingsOnUncompletedContracts + RefundableIncomeTaxes +
                       NoteReceivable +OtherCurrentAssets + NetPropertyAndEquipment+ OtherAssets)<>0,(ConstructionRevenues - ConstructionDirectCosts +NonConstructionGrossProfitLoss  -
  GeneralAndAdministrativeExpenses+ OtherIncomeDeductions - IncomeTaxExpenseBenifit)/ (CashAndCashEquivalents+ Investments+ReceivablesCurrent + ReceivableRetainage +
                        CostsAndEstimatedEarningsInExcessOfBillingsOnUncompletedContracts + RefundableIncomeTaxes +
                      NoteReceivable + OtherCurrentAssets +NetPropertyAndEquipment  + OtherAssets),0)
					   as AfterTaxReturnCurrentAssets,
					   ConstructionRevenues - ConstructionDirectCosts +NonConstructionGrossProfitLoss -
   GeneralAndAdministrativeExpenses +OtherIncomeDeductions + DepreciationAndAmortization + InterestExpense
					   as EBITDA,
					 IIF(ConstructionRevenues<>0,((ConstructionRevenues -ConstructionDirectCosts + NonConstructionGrossProfitLoss -
   GeneralAndAdministrativeExpenses + OtherIncomeDeductions + DepreciationAndAmortization + InterestExpense)/ ConstructionRevenues) * 100,0) 
					 as  AsPercentageRevenue,
					 ConstructionRevenues
   		from FInancialAnalyticsView where clientid=@clientId)f
		on f.VendorId=lp.VendorId
		
		where  lp.ClientId=@clientid and year(Fiscalyearenddate)>year(GETDATE())-4

		order by o.Name,year(Fiscalyearenddate) desc";
            result.Data.Add(new KeyValuePair<string, string>("@clientId", clientId.ToString()));
            return result;
        }
    }
}