﻿using FVGen3.WebUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AhaApps.Libraries.Extensions;

namespace FVGen3.WebUI.Reports
{
    public partial class VendorEmployeeTrainingsByName : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public ReportModel GetTraining(long clientOrgId, string ids, string baseids, DateTime trainingfromDate, DateTime trainingtoDate)
        {
            var data = "";
            data = "select distinct Contact.LastName +' , '+ Contact.FirstName AS EmpName, Templates.TemplateName, s.QuizResult, Convert(varchar,s.QuizDateSubmitted,101) as QuizDateStart, " +
                  "org.Name as Vendor, org1.Name AS Client from SystemUsersOrganizationsQuizzes s " +
                  "INNER JOIN [dbo].[Organizations] org on s.VendorId= org.OrganizationId " +
                  "INNER JOIN [dbo].[Organizations] org1 on s.clientId= org1.OrganizationId " +
                  "INNER JOIN SystemUsersOrganizations ON s.SysUserOrgId = SystemUsersOrganizations.SysUserOrganizationId  " +
                  " INNER JOIN SystemUsersOrganizationsQuizzes soq2 on SystemUsersOrganizations.SysUserOrganizationId  =soq2.SysUserOrgId " +
                  "INNER JOIN SystemUsers On SystemUsersOrganizations.UserId=SystemUsers.UserId and SystemUsers.UserStatus=1 " +
                  "INNER JOIN Contact ON Contact.UserId = SystemUsersOrganizations.UserId " +
                  "INNER JOIN [dbo].[ClientTemplates] ct ON ct.ClientTemplateId= s.ClientTemplateId " +
                  "INNER JOIN Templates ON ct.TemplateID = Templates.TemplateID where " +
                  "s.ClientId=@clientOrgId and (s.QuizRemoved is null or s.QuizRemoved=0) and s.ClientTemplateId in(select * from dbo.ufn_ParseGuidList(@ids))" +
                //"and org.OrganizationID in (select vendorid from SystemUsersOrganizationsQuizzes where ClientTemplateID in (" + baseids + ")" +
                  "and soq2.ClientTemplateID in (select * from dbo.ufn_ParseGuidList(@baseids))" +
                  " and not (s.ClientTemplateId  = '4189031e-b706-41e3-a21d-9302c52de65f' and s.QuizResult is null)" +
                  " and s.QuizDateSubmitted >='" + trainingfromDate + "' and s.QuizDateSubmitted <='" + trainingtoDate.AddDays(1) +"' order by org.Name";
            //alert(data);
            if (Session["RoleName"] == "Client")
            {
                data = "select distinct Contact.LastName +' , '+ Contact.FirstName AS EmpName, Templates.TemplateName, s.QuizResult, Convert(varchar,s.QuizDateSubmitted,101) as QuizDateStart, " +
                           "org.Name as Vendor, org1.Name AS Client from SystemUsersOrganizationsQuizzes s " +
                           "INNER JOIN [dbo].[Organizations] org on s.VendorId= org.OrganizationId " +
                           "INNER JOIN [dbo].[Organizations] org1 on s.clientId= org1.OrganizationId " +
                           "INNER JOIN SystemUsersOrganizations ON s.SysUserOrgId = SystemUsersOrganizations.SysUserOrganizationId  " +
                           " INNER JOIN SystemUsersOrganizationsQuizzes soq2 on org.OrganizationID = soq2.VendorId " +
                           "INNER JOIN SystemUsers On SystemUsersOrganizations.UserId=SystemUsers.UserId and SystemUsers.UserStatus=1 " +
                           "INNER JOIN Contact ON Contact.UserId = SystemUsersOrganizations.UserId " +
                           "INNER JOIN [dbo].[ClientTemplates] ct ON ct.ClientTemplateId= s.ClientTemplateId " +
                           "INNER JOIN Templates ON ct.TemplateID = Templates.TemplateID where " +
                           "s.ClientId=" + Session["currentOrgId"] + "and (s.QuizRemoved is null or s.QuizRemoved=0) and s.ClientTemplateId in(select * from dbo.ufn_ParseGuidList(@ids))" +
                            " and soq2.ClientTemplateID in (select * from dbo.ufn_ParseGuidList(@baseids))" +
                           "and s.QuizResult is not null" +
                           " and s.QuizDateSubmitted >='" + trainingfromDate + "' and s.QuizDateSubmitted <='" + trainingtoDate.AddDays(1) + "' order by org.Name";
            }
            var reportData = new ReportModel();
            reportData.Data = new List<KeyValuePair<string, string>>();
            reportData.Sql = data;
            reportData.Data.Add(new KeyValuePair<string, string>("@clientOrgId", clientOrgId.ToStringNullSafe()));
            reportData.Data.Add(new KeyValuePair<string, string>("@ids", ids));
            reportData.Data.Add(new KeyValuePair<string, string>("@baseids", baseids));
            reportData.Data.Add(new KeyValuePair<string, string>("@trainingtoDate", trainingtoDate.ToString()));
            reportData.Data.Add(new KeyValuePair<string, string>("@trainingfromDate", trainingfromDate.ToString()));

            return reportData;

        }
    }
}