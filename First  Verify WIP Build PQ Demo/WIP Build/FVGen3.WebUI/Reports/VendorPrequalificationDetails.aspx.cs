﻿using FVGen3.Domain.Concrete;
using FVGen3.Domain.Entities;
using FVGen3.WebUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AhaApps.Libraries.Extensions;
using FVGen3.WebUI.Utilities;
using System.Data.Entity;
using Microsoft.Reporting.WebForms;
using System.Data;

namespace FVGen3.WebUI.Reports
{
    public partial class VendorPrequalificationDetails1 : System.Web.UI.Page
    {
        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {

                if (!this.IsPostBack)
                {
                    var result = CustomReportGeneration(Request.Form["Subsite"],
                 Request.Form["vendorCity"],
                 Request.Form["vendorState"], Request.Form["GeographicArea"],
                 Request.Form["biddingIdList"], Request.Form["claycoBiddingInterests"],
                 Request.Form["statusIdList"], Request.Form["proficiencyIdList"], Request.Form["VendorName"], Request.Form["Country"],
                 Request.Form["periodVal"], Request.Form["sortType"], long.Parse(Request.Form["clientId"]), Request.Form["clientTemplateid"].ToNullableGuid(),
                 Request.Form["clientVendorRefId"], Request.Form["minorityBusiness"], Request.Form["unionShop"], Request.Form["mostCurrentEMR"], Request.Form["citedByOsha"],
                 Request.Form["safetyDirector"], Request.Form["Site"], Request.Form["clientContractId"], Request.Form["productCategoryList"]);


                    ReportViewer1.LocalReport.DataSources.Clear();
                    ReportViewer1.ProcessingMode = ProcessingMode.Local;

                    ReportViewer1.LocalReport.ReportPath = Server.MapPath("VendorPrequalificationDetails.rdlc");

                    DataSet dataSet = new DataSet();

                    System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["EFDbContext"].ConnectionString);


                    connection.Open();
                    System.Data.SqlClient.SqlDataAdapter adapter = new System.Data.SqlClient.SqlDataAdapter(result.Sql, connection);
                    var data = result.Data; //Newtonsoft.Json.JsonConvert.DeserializeObject<List<KeyValuePair<string,string>>>(Request.Form["data"]);
                    foreach (var d in data)
                    {
                        adapter.SelectCommand.Parameters.AddWithValue(d.Key, d.Value);
                    }
                    adapter.SelectCommand.CommandTimeout = 300;

                    adapter.Fill(dataSet);

                    ReportDataSource dataSource = new ReportDataSource("VendorPrequalificationDetails", dataSet.Tables[0]);

                    ReportViewer1.LocalReport.DataSources.Add(dataSource);
                    ReportViewer1.DocumentMapCollapsed = true;
                    ReportViewer1.LocalReport.Refresh();

                    ReportViewer1.Visible = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        public ReportModel CustomReportGeneration(string Subsite, string vendorCity, string vendorState,string GeographicArea, string biddingIdList, string claycoBiddingInterests,
                                string statusIdList, string proficiencyIdList, string VendorName, string Country, string periodVal, string sortType, long clientId, Guid? clientTemplateid, string clientVendorRefId,
                                string minorityBusiness, string unionShop, string mostCurrentEMR, string citedByOsha, string safetyDirector, string Site, string clientContractId, string productCategoryList)
        {
            EFDbContext entityDB = new EFDbContext();
            LocalSearchVendors searchVendors = new LocalSearchVendors();
            var venname = VendorName;
            var period = periodVal;
            var sort = sortType;
            var varclientId = clientId;
            var clientrefid = clientVendorRefId;
            Session["SelectedClientID"] = varclientId;



            //For Vendor city State and Name 
            if (vendorState == "Select") vendorState = "";

            var strStateCityName = "";
            if ((String.IsNullOrEmpty(venname)) && (String.IsNullOrEmpty(vendorState)) && (String.IsNullOrEmpty(vendorCity)) && (String.IsNullOrEmpty(Country)))
                strStateCityName = "";

            else if ((!String.IsNullOrEmpty(venname)) && (!String.IsNullOrEmpty(vendorState)) && (!String.IsNullOrEmpty(vendorCity)))
                strStateCityName = "org.State  like @vendorState and  org.City like @vendorCity and org.Name like '%" + venname + "%'";

            else if (String.IsNullOrEmpty(venname) && (!String.IsNullOrEmpty(vendorState)) && (!String.IsNullOrEmpty(vendorCity)))
                strStateCityName = "org.State  like @vendorState and  org.City like '%" + vendorCity.Trim() + "%'";

            else if (String.IsNullOrEmpty(vendorState) && (!String.IsNullOrEmpty(venname)) && (!String.IsNullOrEmpty(vendorCity)))
                strStateCityName = " org.City like '%" + vendorCity.Trim() + "%' and org.Name like '%" + venname + "%'";

            else if (String.IsNullOrEmpty(vendorCity) && (!String.IsNullOrEmpty(vendorState)) && (!String.IsNullOrEmpty(venname)))
                strStateCityName = " org.State  like @vendorState and org.Name like '%" + venname + "%'";

            else if (String.IsNullOrEmpty(venname) && String.IsNullOrEmpty(vendorState) && (!String.IsNullOrEmpty(vendorCity)))
                strStateCityName = " org.City like '%" + vendorCity.Trim() + "%'";

            else if (String.IsNullOrEmpty(venname) && String.IsNullOrEmpty(vendorCity) && (!String.IsNullOrEmpty(vendorState)))
                strStateCityName = "org.State  like @vendorState";

            else if (String.IsNullOrEmpty(vendorCity) && String.IsNullOrEmpty(vendorState) && (!String.IsNullOrEmpty(venname)))
                strStateCityName = "org.Name like '%" + venname + "%'";

            if ((!String.IsNullOrEmpty(Country)) && String.IsNullOrEmpty(strStateCityName))
            {
            strStateCityName += "org.Country like '%" + Country + "%'";
            }
            else if ((!String.IsNullOrEmpty(Country)) && (!String.IsNullOrEmpty(strStateCityName)))
            {
            strStateCityName += " and org.Country like '%" + Country + "%'";
            }
            if (!String.IsNullOrEmpty(strStateCityName))
            {
            strStateCityName = "and(" + strStateCityName + ")";

            }
            // For vendor operating Region
            var GeoArea = "";
            if (!string.IsNullOrEmpty(GeographicArea) && GeographicArea != "null")
            {
                var GeoList = GeographicArea.Split(',');
                var regions = entityDB.Regions.Where(r => GeoList.Contains(r.RegionName)).Select(r => r.RegionId);
                var RegionIds = string.Join(",", regions);
                GeoArea = "and org.GeographicArea in (select RegionName from Regions where RegionId in(" + RegionIds + "))";


            }
            //For StatusIdList
            var strStatusSql = "p.PrequalificationStatusId in(" + statusIdList + ") and";
            if (String.IsNullOrEmpty(statusIdList)) strStatusSql = " ";
            var questionnaireSearch = "";

            // Kiran on 04/10/2015
            if (clientTemplateid != null)
                questionnaireSearch = "and p.ClientTemplateId='" + clientTemplateid + "'";
            // Ends<<<

           
            // sumanth on 08/12/2015
            var strClientRefidSql = "";
            if (!String.IsNullOrEmpty(clientrefid))
            {
                long clientid = 0;
                long vendorid = 0;
                var clientrefdetails = entityDB.ClientVendorReferenceIds.FirstOrDefault(rec => rec.ClientVendorReferenceId == clientrefid);
                if (clientrefdetails != null)
                {
                    clientid = clientrefdetails.ClientId;
                    vendorid = clientrefdetails.VendorId;
                }
                strClientRefidSql = " and p.VendorId in (select distinct VendorId from ClientVendorReferenceIds where ClientId='" + varclientId + "' and ClientVendorReferenceId='" + clientrefid + "')";

            }

            var strClientContractSql = "";
            if (!string.IsNullOrEmpty(clientContractId) && clientContractId.ToLower() != "none")
            {
                strClientContractSql =
                    " and p.VendorId in (select VendorId from ClientVendorReferenceIds where ClientId=@varclientId  and ClientContractId='" +
                    clientContractId + "')";

            }
            //For Select statement
            //var strMainsql = "select  org.Name, " +
            //                  "org.Address1,org.Address2, " +
            //                  "org.City, org.State, org.Country,org.Zip,  " +
            //                  "Contact.FirstName, Contact.LastName, " +
            //                  "Contact.MobileNumber, SystemUsers.Email, " +
            //                  "ps.PrequalificationStatusName,p.PrequalificationStart," +
            //                  "p.PrequalificationFinish,p.VendorId from Prequalification p " +
            //                  "INNER JOIN PrequalificationStatus AS ps " +
            //                  "ON ps.PrequalificationStatusId = p.PrequalificationStatusId " +
            //                  "INNER JOIN  Organizations AS org ON org.OrganizationID = p.VendorId " +
            //                  "INNER JOIN SystemUsers ON p.UserIdAsSigner = SystemUsers.UserId " +
            //                  "INNER JOIN Contact ON SystemUsers.UserId = Contact.UserId " +
            //                  "INNER JOIN SystemUsersOrganizations  " +
            //                  "ON org.OrganizationID = SystemUsersOrganizations.OrganizationId " +
            //                  "AND SystemUsers.UserId = SystemUsersOrganizations.UserId " +
            //                  "INNER JOIN SystemUsersOrganizationsRoles " +
            //                  "ON SystemUsersOrganizations.SysUserOrganizationId = SystemUsersOrganizationsRoles.SysUserOrgId " +
            //                  "INNER JOIN SystemRoles ON SystemUsersOrganizationsRoles.SysRoleId = SystemRoles.RoleId ";

            // Kiran on 3/17/2014
            var strMainsql = "select distinct org.Name,"+
                @"case when (p.InvitationFrom=2) then (select lastname+','+firstname from contact where userid =InvitationSentByUser)
when p.InvitationFrom=1 then 'First Verify'

else 'Without Invitation' end 


as invitationfrom ,  org.Address1,org.Address2, org.City, org.State, org.Country,org.Zip, " +
                                 "users.FirstName, users.LastName, users.MobileNumber as MobileNumber, users.Email, Templates.TemplateCode, " +  // sumanth on 10/21/2015 for FVOS-78 to display phone number instead of mobile number
                                 @"case 

    when psiteStatuses.cou is null or psiteStatuses.cou = 0

        then ps.PrequalificationStatusName
	else dbo.ufnGetPQStatus(p.PrequalificationId, @clientIdForStatus, @userid, ps.PrequalificationStatusName) end  PrequalificationStatusName, " +
                                 "p.PrequalificationStart,p.PrequalificationFinish,p.VendorId from LatestPrequalification AS p " +//Suma on 2/12/2013  //Suma on 4/12/2013,c.TemplateID
                                   "left join (select count(*) cou,PrequalificationId from PrequalificationSites where PQSiteStatus is not null group by PrequalificationId) psiteStatuses on psiteStatuses.PrequalificationId = p.PrequalificationId " +
                                   "INNER JOIN PrequalificationStatus AS ps " +
                                   " ON ps.PrequalificationStatusId = p.PrequalificationStatusId " +
                                   " inner join ClientTemplates  on p.ClientTemplateId = ClientTemplates.ClientTemplateID" +
                                   " inner join Templates  on Templates.TemplateID=ClientTemplates.TemplateID " +
                                   " INNER JOIN  Organizations AS org ON org.OrganizationID = p.VendorId "+
             //LatestPrequalification AS p  " +
             //"left join (select count(*) cou,PrequalificationId from PrequalificationSites where PQSiteStatus is not null group by PrequalificationId) psiteStatuses on psiteStatuses.PrequalificationId = p.PrequalificationId" +
             //" INNER JOIN PrequalificationStatus AS ps ON ps.PrequalificationStatusId = p.PrequalificationStatusId  " +
             //"INNER JOIN  Organizations AS org ON org.OrganizationID = p.VendorId " +

             //"INNER JOIN (select prequalification.VendorId, MAX(PrequalificationStart) as prequalStart from prequalification where (ClientId=@clientId or PrequalificationId in(select PQId from PrequalificationClient where ','+ClientIds+',' like @p0))" + questionnaireSearch +

             // //" INNER JOIN (select Prequalification.VendorId, MAX(PrequalificationStart) as prequalStart from Prequalification where ClientId=" + clientId + questionnaireSearch +
             // " group by(Prequalification.VendorId)) latest ON (p.PrequalificationStart = latest.prequalStart and p.VendorId = latest.VendorId)" + // Kiran on 11/27/2014
             //         

             //"LEFT JOIN (select pre.VendorId, MAX(pre.PrequalificationStart)as latestPrequalificationStart from Prequalification pre group by pre.VendorId) latest ON p.PrequalificationStart = latest.latestPrequalificationStart and p.VendorId = latest.VendorId " + // Kiran on 04/30/2014
         //Added By Vasavi
        
" left join(select Contact.FirstName, Contact.LastName, Contact.PhoneNumber as MobileNumber, SystemUsers.Email, OrganizationId  from "+

      " SystemUsersOrganizations " +
       " JOIN SystemUsers ON SystemUsers.UserId = SystemUsersOrganizations.UserId " +

    " JOIN Contact ON SystemUsers.UserId = Contact.UserId " +

      " JOIN SystemUsersOrganizationsRoles ON SystemUsersOrganizations.SysUserOrganizationId = SystemUsersOrganizationsRoles.SysUserOrgId " +

    " JOIN SystemRoles ON SystemUsersOrganizationsRoles.SysRoleId = SystemRoles.RoleId  and RoleName = 'super user' and SystemRoles.OrganizationType = 'Vendor') as users on users.OrganizationId = org.OrganizationID ";
         //End
            
            //Commented By Vasavi 

            //"LEFT JOIN SystemUsersOrganizations  ON org.OrganizationID = SystemUsersOrganizations.OrganizationId " +
            //"LEFT JOIN SystemUsers ON SystemUsers.UserId = SystemUsersOrganizations.UserId " +
            //"LEFT JOIN Contact ON SystemUsers.UserId = Contact.UserId " +
            ////"LEFT JOIN ClientTemplates ON ClientTemplates.ClientTemplateID = p.ClientTemplateId   " +
            ////"LEFT JOIN Templates ON ClientTemplates.TemplateID = Templates.TemplateID " +
            //"LEFT JOIN SystemUsersOrganizationsRoles ON SystemUsersOrganizations.SysUserOrganizationId = SystemUsersOrganizationsRoles.SysUserOrgId " +
            //"LEFT JOIN SystemRoles ON SystemUsersOrganizationsRoles.SysRoleId = SystemRoles.RoleId ";
            //Commented by vasavi end
            //For Innerjoin of PrequalificationReportindData
            var strPrequalificationReportingDatasql = "INNER JOIN  PrequalificationReportingData AS prd ON (prd.PrequalificationId = p.PrequalificationId";

            var biddingsql = "prd.ReportingType = 0 and prd.ReportingDataId in (" + biddingIdList + ")";
            var proficiencysql = "prd.ReportingType = 1 and prd.ReportingDataId in (" + proficiencyIdList + ")";
            var claycobiddingsql = "prd.ReportingType = 3 and prd.ReportingDataId in (" + claycoBiddingInterests + ")";
            var productCategoriesSql = "prd.ReportingType = 4 and prd.ReportingDataId in (" + productCategoryList + ")";

            var ProfBidsql = "";
            if (String.IsNullOrEmpty(claycoBiddingInterests) && String.IsNullOrEmpty(biddingIdList) && (String.IsNullOrEmpty(proficiencyIdList)) && String.IsNullOrEmpty(productCategoryList))
            {
                //Suma on 10/15/2013 >>>
                strPrequalificationReportingDatasql = " ";
                ProfBidsql = " ";
                //<<<Ends
            }
            else if (!String.IsNullOrEmpty(biddingIdList) && !String.IsNullOrEmpty(proficiencyIdList) && !String.IsNullOrEmpty(claycoBiddingInterests))
            {
                ProfBidsql = "and(" + biddingsql + "or(" + proficiencysql + ") or (" + claycobiddingsql + ")))";
            }
            else if (!String.IsNullOrEmpty(biddingIdList) && !String.IsNullOrEmpty(proficiencyIdList) && String.IsNullOrEmpty(claycoBiddingInterests))
            {
                ProfBidsql = "and(" + biddingsql + "or(" + proficiencysql + ")))";
            }
            else if (!String.IsNullOrEmpty(biddingIdList) && String.IsNullOrEmpty(proficiencyIdList) && !String.IsNullOrEmpty(claycoBiddingInterests))
            {
                ProfBidsql = "and(" + biddingsql + "or(" + claycobiddingsql + ")))";
            }
            else if (String.IsNullOrEmpty(biddingIdList) && !String.IsNullOrEmpty(proficiencyIdList) && String.IsNullOrEmpty(claycoBiddingInterests))
            {
                ProfBidsql = "and(" + proficiencysql + "))";
            }
            else if (!String.IsNullOrEmpty(biddingIdList) && String.IsNullOrEmpty(proficiencyIdList) && String.IsNullOrEmpty(claycoBiddingInterests))
            {
                ProfBidsql = "and(" + biddingsql + "))";
            }
            else if (String.IsNullOrEmpty(biddingIdList) && String.IsNullOrEmpty(proficiencyIdList) && !String.IsNullOrEmpty(claycoBiddingInterests))
            {
                ProfBidsql = " and (" + claycobiddingsql + "))";
            }

            if (!String.IsNullOrEmpty(productCategoryList))
                ProfBidsql = " and (" + productCategoriesSql + "))";
            //if (String.IsNullOrEmpty(biddingIdList) && (String.IsNullOrEmpty(proficiencyIdList)))
            //{
            //    strPrequalificationReportingDatasql = "";
            //    ProfBidsql = "";
            //}
            //else if (!String.IsNullOrEmpty(biddingIdList) && (!String.IsNullOrEmpty(proficiencyIdList)))
            //{
            //    ProfBidsql = "and(" + biddingsql + "or(" + proficiencysql + ")))";
            //}
            //else if (String.IsNullOrEmpty(biddingIdList))
            //{
            //    ProfBidsql = "and(" + proficiencysql + "))";
            //}
            //else if (String.IsNullOrEmpty(proficiencyIdList))
            //{
            //    ProfBidsql = "and(" + biddingsql + "))";
            //}

            //For Subsite
            var strsubSitesql = "left JOIN PrequalificationSites AS psite ON (p.PrequalificationId = psite.PrequalificationId ";
            //if (Subsite != null)
            //{
            //    strsubSitesql += "and psite.ClientBusinessUnitSiteId = @Subsite ";
            //}
            //if (Site != null)
            //{
            //    strsubSitesql += "and psite.ClientBusinessUnitId = @Site ";
            //}
            strsubSitesql += ")";
            var siteCondition = "";
            var subsite = Subsite.Split(',');
            if (!subsite.Contains("Any") && !subsite.Contains(""))
            {

                siteCondition += "and psite.ClientBusinessUnitSiteId in( " + Subsite + ") ";

            }
            var site = Site.Split(',');
            if (!site.Contains("Any") && !site.Contains(""))
            {

                siteCondition += "and psite.ClientBusinessUnitId in(" + Site + ") ";

            }
            //if (!string.IsNullOrEmpty(Subsite) && Subsite != "-1")
            //{
            //    siteCondition += "and psite.ClientBusinessUnitSiteId in( " + Subsite + ")";
            //}
            //if (!string.IsNullOrEmpty(Site) && Site != "-1")
            //{
            //    siteCondition += "and psite.ClientBusinessUnitId in(" + Site + ")";
            //}

            //if (Subsite == null && Site == null) strsubSitesql = "";

            //Minority Business
            string minorityBusinessSQL = string.Empty;
            if (!String.IsNullOrEmpty(minorityBusiness) && minorityBusiness != "Any")
            {
                minorityBusinessSQL = "JOIN (select p.PrequalificationId, pqi.UserInput as minorityBusiness from  " +
                                "Prequalification p  " +
                                "join ClientTemplates ct on p.ClientTemplateID = ct.ClientTemplateID " +
                                "join Templates t on t.TemplateID = ct.TemplateID " +
                                "join TemplateSections ts on ts.TemplateID = t.TemplateID " +
                                "join TemplateSubSections tss on tss.TemplateSectionID = ts.TemplateSectionID " +
                                "join questions q on q.SubSectionId = tss.SubSectionID " +
                                "join QuestionColumnDetails qcd on q.QuestionID = qcd.QuestionId " +
                                "join PrequalificationUserInput pqi on qcd.QuestionColumnId = pqi.QuestionColumnId  and p.prequalificationid = pqi.prequalificationid " +
                                 "where q.QuestionText =  'Minority Business:' and pqi.UserInput = '" + minorityBusiness + "') as mb on p.PrequalificationId = mb.PrequalificationId";
            }
            //Union Shop
            string unionShopSQL = string.Empty;
            if (!String.IsNullOrEmpty(unionShop) && unionShop != "Any")
            {
                unionShopSQL = "JOIN (select p.PrequalificationId, pqi.UserInput as unionShop from  " +
                                "Prequalification p  " +
                                "join ClientTemplates ct on p.ClientTemplateID = ct.ClientTemplateID " +
                                "join Templates t on t.TemplateID = ct.TemplateID " +
                                "join TemplateSections ts on ts.TemplateID = t.TemplateID " +
                                "join TemplateSubSections tss on tss.TemplateSectionID = ts.TemplateSectionID " +
                                "join questions q on q.SubSectionId = tss.SubSectionID " +
                                "join QuestionColumnDetails qcd on q.QuestionID = qcd.QuestionId " +
                                "join PrequalificationUserInput pqi on qcd.QuestionColumnId = pqi.QuestionColumnId  and p.prequalificationid = pqi.prequalificationid " +
                                 "where q.QuestionText =  'Union Shop:' and pqi.UserInput = '" + unionShop + "') as us on p.PrequalificationId = us.PrequalificationId";
            }
            //
            string citedByOshaSQL = string.Empty;
            if (!String.IsNullOrEmpty(citedByOsha) && citedByOsha != "Any")
            {
                citedByOshaSQL = "JOIN (select p.PrequalificationId, pqi.UserInput as unionShop from  " +
                                "Prequalification p  " +
                                "join ClientTemplates ct on p.ClientTemplateID = ct.ClientTemplateID " +
                                "join Templates t on t.TemplateID = ct.TemplateID " +
                                "join TemplateSections ts on ts.TemplateID = t.TemplateID " +
                                "join TemplateSubSections tss on tss.TemplateSectionID = ts.TemplateSectionID " +
                                "join questions q on q.SubSectionId = tss.SubSectionID " +
                                "join QuestionColumnDetails qcd on q.QuestionID = qcd.QuestionId " +
                                "join PrequalificationUserInput pqi on qcd.QuestionColumnId = pqi.QuestionColumnId  and p.prequalificationid = pqi.prequalificationid " +
                                 "where q.QuestionText =  'Has your company been cited by OSHA or the EPA in the past three years?' and pqi.UserInput = '" + citedByOsha + "') as osha on p.PrequalificationId = osha.PrequalificationId";
            }
            string safetyDirectorSQL = string.Empty;
            if (!String.IsNullOrEmpty(safetyDirector))
            {
                safetyDirectorSQL = "JOIN (select p.PrequalificationId, pqi.UserInput as unionShop from  " +
                                "Prequalification p  " +
                                "join ClientTemplates ct on p.ClientTemplateID = ct.ClientTemplateID " +
                                "join Templates t on t.TemplateID = ct.TemplateID " +
                                "join TemplateSections ts on ts.TemplateID = t.TemplateID " +
                                "join TemplateSubSections tss on tss.TemplateSectionID = ts.TemplateSectionID " +
                                "join questions q on q.SubSectionId = tss.SubSectionID " +
                                "join QuestionColumnDetails qcd on q.QuestionID = qcd.QuestionId " +
                                "join PrequalificationUserInput pqi on qcd.QuestionColumnId = pqi.QuestionColumnId  and p.prequalificationid = pqi.prequalificationid " +
                                 "where q.QuestionText =  'Has your company been cited by OSHA or the EPA in the past three years?' and pqi.UserInput = '" + safetyDirector + "') as sd on p.PrequalificationId = sd.PrequalificationId";
            }
            string mostCurrentEMRSQL = string.Empty;
            double mostCurrentEmrdec = -1.00;
            if (!String.IsNullOrEmpty(mostCurrentEMR) && double.TryParse(mostCurrentEMR, out mostCurrentEmrdec))
            {

                mostCurrentEMRSQL = "JOIN (select p.PrequalificationId, pqi.UserInput as unionShop from  " +
                                "Prequalification p  " +
                                "join ClientTemplates ct on p.ClientTemplateID = ct.ClientTemplateID " +
                                "join Templates t on t.TemplateID = ct.TemplateID " +
                                "join TemplateSections ts on ts.TemplateID = t.TemplateID " +
                                "join TemplateSubSections tss on tss.TemplateSectionID = ts.TemplateSectionID " +
                                "join questions q on q.SubSectionId = tss.SubSectionID " +
                                "join QuestionColumnDetails qcd on q.QuestionID = qcd.QuestionId " +
                                "join QuestionsDependants qd on qd.DependantId = q.QuestionId  " +
                                "join questions q2 on q2.QuestionID = qd.QuestionId and q2.SubSectionId = tss.SubSectionID " +
                                "join PrequalificationUserInput pqi on qcd.QuestionColumnId = pqi.QuestionColumnId  and p.prequalificationid = pqi.prequalificationid " +
                                 "where q.QuestionText in( 'EMR:' ,'Most Recent Policy Year') " +
                                "and (IsNumeric(pqi.UserInput) = 1 and pqi.UserInput is not null " +
                                "and TRY_PARSE(REPLACE(pqi.UserInput,',','') as decimal(11,2)) < " + mostCurrentEMR + " ))" +
                                 "as mcemr on p.PrequalificationId = mcemr.PrequalificationId";
            }
            //For sortType
            var sortName = sortType;

            if (String.IsNullOrEmpty(sortType) || (sortType == "Name")) sortName = "org.Name";
            if (sortType == "Status") sortName = " PrequalificationStatusName";
            if (sortType == "Status Period") sortName = "p.PrequalificationStart";

            var reportSQL = "";
            var userSites = "";
            var userid = (Guid)Session["UserId"];
            var userSitesRec = entityDB.SystemUsersBUSites.FirstOrDefault(r => r.SystemUserId == userid);
            if (userSitesRec != null)
                userSites = userSitesRec.BUSiteId;
            var userLocStr = (string.IsNullOrEmpty(userSites) ? "" : " and psite.ClientBusinessUnitSiteId in (" + userSites + ")");
            //Suma on 10/15/2013 ===>>>
            if (period == "searchAll")
            {

                reportSQL = " " + strMainsql + " " +
                             //" " + strStateCityName + " " +
                             " " + strClientRefidSql + " " +
                             " " + strClientContractSql + " " +
                             " " + strPrequalificationReportingDatasql + " " +
                             " " + ProfBidsql + " " +

                             " " + minorityBusinessSQL + " " +
                             " " + unionShopSQL + " " +
                             " " + citedByOshaSQL + " " +
                             " " + safetyDirectorSQL + " " +
                             " " + strsubSitesql + " " +
                             " " + mostCurrentEMRSQL + " " +
                             " " + userLocStr + " " +
                             " where( " +
                             " Templates.IsHide=0 and" +
                             //" " + strStatusSql + " p.ClientId = " + clientId + "and ps.ShowInDashBoard = 1" + " )order by " + sortName + " "; // Kiran on 3/17/2014
                             " " + strStatusSql + " (p.ClientId = @clientId or p.PrequalificationId in(select PQId from PrequalificationClient where ','+ClientIds+',' like @p0)) and ps.ShowInDashBoard = 1 " + strStateCityName + questionnaireSearch + siteCondition + GeoArea+ " )order by " + sortName + " ";
            }
            else if (period == "searchParticular")
            {
                var baseDate = (DateTime.Now.AddDays(-25)).ToString("MM/dd/yyyy");
                var currentDate = DateTime.Now.ToString("MM/dd/yyyy");
                reportSQL = " " + strMainsql + " " +
                             //" " + strStateCityName + " " +
                             " " + strClientRefidSql + " " +
                             " " + strClientContractSql + " " +
                             " " + strPrequalificationReportingDatasql + " " +
                             " " + ProfBidsql + " " +
                             " " + strsubSitesql + " " +
                               " " + userLocStr + " " +
                             " where( " +
                            // " SystemRoles.RoleName = 'Super User' AND SystemRoles.OrganizationType = 'Vendor' and" +
                             //" " + strStatusSql + " p.ClientId = " + clientId + "and ps.ShowInDashBoard = 1" +
                             " " + strStatusSql + strStateCityName + questionnaireSearch + " (p.ClientId = @clientId or p.PrequalificationId in(select PQId from PrequalificationClient where ','+ClientIds+',' like @p0)) and ps.ShowInDashBoard = 1" +
                            " and(p.PrequalificationStart >= '" + baseDate + "' ) " + siteCondition+ GeoArea + // sumanth on 10/23/2015 for FVOS-77 added inverted commas to the base date.
                            " )order by " + sortName + " "; // Kiran on 3/17/2014


            }
            var reportData = new ReportModel();
            reportData.Data = new List<KeyValuePair<string, string>>();
            reportData.Sql = reportSQL;
            reportData.Data.Add(new KeyValuePair<string, string>("@vendorState", vendorState));//'%" + vendorCity.Trim() + "%'
            reportData.Data.Add(new KeyValuePair<string, string>("@vendorCity", "%" + vendorCity.Trim() + "%"));//'%" + vendorCity.Trim() + "%'
            reportData.Data.Add(new KeyValuePair<string, string>("@Site", Site.ToStringNullSafe()));
            reportData.Data.Add(new KeyValuePair<string, string>("@Subsite", Subsite.ToStringNullSafe()));
            reportData.Data.Add(new KeyValuePair<string, string>("@varclientId", varclientId.ToStringNullSafe()));
            reportData.Data.Add(new KeyValuePair<string, string>("@clientId", clientId.ToStringNullSafe()));
            reportData.Data.Add(new KeyValuePair<string, string>("@p0", "%," + clientId.ToStringNullSafe() + ",%"));
            reportData.Data.Add(new KeyValuePair<string, string>("@userid",  userid.ToString() ));
            reportData.Data.Add(new KeyValuePair<string, string>("@clientIdForStatus", (Session["RoleName"].ToString().Equals("Admin")?"0": clientId.ToStringNullSafe())));
            //Suma on 10/15/2013 Ends<<<
            //return Json(reportData, JsonRequestBehavior.AllowGet);
            return reportData;
        }
    }
}
