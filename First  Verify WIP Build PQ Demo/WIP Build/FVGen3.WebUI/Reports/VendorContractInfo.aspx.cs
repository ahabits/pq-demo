﻿using FVGen3.Domain.Concrete;
using FVGen3.Domain.Entities;
using FVGen3.WebUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AhaApps.Libraries.Extensions;
using FVGen3.WebUI.Utilities;
using Microsoft.Reporting.WebForms;
using System.Data.Entity;
using FVGen3.BusinessLogic.Interfaces;
using FVGen3.BusinessLogic;
using System.Data;

namespace FVGen3.WebUI.Reports
{
    public partial class VendorContractInfo : System.Web.UI.Page
    {
        IReportsBusiness reportBusiness;
        public VendorContractInfo()
        {
            this.reportBusiness = new ReportsBusiness();
        }
        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {

                if (!this.IsPostBack)
                {
                    //var result = CustomReportGeneration(Request.Form["Subsite"].ToNullableGuid());


                    //ReportViewer1.LocalReport.DataSources.Clear();
                    //ReportViewer1.ProcessingMode = ProcessingMode.Local;

                    ReportViewer1.LocalReport.ReportPath = Server.MapPath("VendorContractInfoReport.rdlc");

                    DataSet dataSet = new DataSet();

                    System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["EFDbContext"].ConnectionString);

                    var query = reportBusiness.GetContractVendorsQuery(Request.Form["TemplateId"].ToNullableGuid() ?? new Guid(), long.Parse(Request.Form["ClientId"]));
                    connection.Open();
                    System.Data.SqlClient.SqlDataAdapter adapter = new System.Data.SqlClient.SqlDataAdapter(query, connection);

                    //adapter.SelectCommand.Parameters.AddWithValue("@questionColId", param);
                    //var data = result.Data; //Newtonsoft.Json.JsonConvert.DeserializeObject<List<KeyValuePair<string,string>>>(Request.Form["data"]);
                    //foreach (var d in data)
                    //{
                    //    adapter.SelectCommand.Parameters.AddWithValue(d.Key, d.Value);
                    //}
                    //adapter.SelectCommand.CommandTimeout = 300;

                    adapter.Fill(dataSet);

                    ReportDataSource dataSource = new ReportDataSource("VendorContractInfoDataSet", dataSet.Tables[0]);

                    ReportViewer1.LocalReport.DataSources.Add(dataSource);
                    ReportViewer1.DocumentMapCollapsed = true;
                    ReportViewer1.LocalReport.Refresh();

                    ReportViewer1.Visible = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        //public ReportModel CustomReportGeneration(Guid? clientTemplateid)
        //{
        //    EFDbContext entityDB = new EFDbContext();
        //    LocalSearchVendors searchVendors = new LocalSearchVendors();

        //    var reportData = new ReportModel();
        //    reportData.Data = new List<KeyValuePair<string, string>>();
        //    reportData.Sql = "";
        //    reportData.Data.Add(new KeyValuePair<string, string>("@clientTemplateId", clientTemplateid.ToString()));//'%" + vendorCity.Trim() + "%'

        //    //Suma on 10/15/2013 Ends<<<
        //    //return Json(reportData, JsonRequestBehavior.AllowGet);
        //    return reportData;
        //}
    }
}
