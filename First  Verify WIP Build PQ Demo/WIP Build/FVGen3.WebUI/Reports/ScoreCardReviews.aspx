﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Import Namespace="FVGen3.Domain.Entities"%>
<%@ Import Namespace="System.Data"%>
<%@ Import Namespace="AhaApps.Libraries.Extensions"%>
<%@ Import Namespace="FVGen3.WebUI.Utilities"%>
<!DOCTYPE html>

<html>
<head runat="server">
    <meta name="viewport" content="width=device-width" />
    <title></title>
    <style>
            #s5_logo_wrap {
                float: left;
                padding-top:4px;  
            }
            #s5_logo {
                cursor: pointer;
                margin-bottom: 21px;
                max-width: 200px;
            } 
        </style>
</head>
<script runat="server">
            private void Page_Load(object sender, System.EventArgs e)
            { 
                if (!this.IsPostBack)
                {
                     // sumanth on 19/08/2015
                    // added try & catch block to handle exceptions and if the address bar is changed then it is redirected to previous page. 
                    try
                    {

                        //var sql = "";
                        //try
                        //{

                        //    sql = Request.Form["sql"].ToString();
                        //}
                        //catch
                        //{
                        //    sql = "";

                        //}
                        //if (sql != "")
                        //{

                        var result = GetInvitationSent(Request.Form["clientOrgId"], (Request.Form["score"].ToNullableInt()), Request.Form["reviewType"], Request.Form["userIds"]);
                            ReportViewer1.LocalReport.DataSources.Clear();
                            ReportViewer1.ProcessingMode = ProcessingMode.Local;

                            ReportViewer1.LocalReport.ReportPath = Server.MapPath("ScoreCardReviews.rdlc");

                            DataSet dataSet = new DataSet();

                            System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["EFDbContext"].ConnectionString);
                            connection.Open();

                            System.Data.SqlClient.SqlDataAdapter adapter = new System.Data.SqlClient.SqlDataAdapter(result.Sql, connection);

                            var data = result.Data;
                            foreach (var d in data)
                            {                                
                                adapter.SelectCommand.Parameters.AddWithValue(d.Key, d.Value);
                            }
                            adapter.SelectCommand.CommandTimeout = 300;    
                        
                        adapter.Fill(dataSet);

                            ReportDataSource dataSource = new ReportDataSource("DataSet1", dataSet.Tables[0]);

                            ReportViewer1.LocalReport.DataSources.Add(dataSource);
                            ReportViewer1.DocumentMapCollapsed = true;
                            ReportViewer1.LocalReport.Refresh();

                            ReportViewer1.Visible = true;
                        //}
                        //else
                        //{
                        //    Response.Redirect("~/AdminVendors/CustomReports");
                        //}
                    }
                    catch (Exception ex)
                    {
                        Response.Write(ex.Message);                    
                    }
                }
            }

            protected void SqlDataSource1_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
            {

            }

            protected void PrequalificationDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
            {

            }

            public FVGen3.WebUI.Models.ReportModel GetInvitationSent(string clientOrgId, int? score, string reviewType, string userIds)
            {
                var data = "select vndrReview.VendorId, vendor.Name as Vendor,client.Name as Client, users.username as ClientUserEmail,contact.LastName + ', '+ contact.FirstName as ClientUserName,case when vndrReview.ReviewType=0 then 'Annual' when vndrReview.ReviewType=1 then 'project' end as ReviewType,vndrReview.InputUserId,vndrReview.UserRating from VendorReviewInput vndrReview inner join Organizations as vendor on vendor.OrganizationID=vndrReview.VendorId inner join Organizations as client ON client.organizationid=vndrReview.ClientId inner join SystemUsers as users on users.UserId =vndrReview.InputUserId left join Contact as contact on contact.UserId =users.UserId  where vndrReview.clientid=@clientOrgId";

                if (userIds.Length != 0)
                    data = data + " and vndrReview.InputUserId in(select * from dbo.ufn_ParseGuidList(@userIds))";

                if ( score != null)
                {
                    var less = score - 1;
                    data = data + " and vndrReview.UserRating <=@score and vndrReview.UserRating>=" + less + "";
                }
                if (!string.IsNullOrEmpty(reviewType))
                    data = data + " and vndrReview.reviewtype=@reviewType";

                var reportData = new FVGen3.WebUI.Models.ReportModel();
                reportData.Data = new List<KeyValuePair<string, string>>();
                reportData.Sql = data;
                reportData.Data.Add(new KeyValuePair<string, string>("@clientOrgId", clientOrgId));
                reportData.Data.Add(new KeyValuePair<string, string>("@score", score.ToStringNullSafe()));
                reportData.Data.Add(new KeyValuePair<string, string>("@reviewType", reviewType.ToStringNullSafe()));
                reportData.Data.Add(new KeyValuePair<string, string>("@userIds", userIds));
                return reportData;

            }
</script>  

<body>
    <form id="form1" runat="server">
        <table width="100%" border="0">
            <tr>
                <td>
                    <div id ="s5_logo_wrap">
                        <img alt="logo" src="../images/logoForReports.png" id="s5_logo" />
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                   <asp:ScriptManager ID="ScriptManager1" runat="server">
                    </asp:ScriptManager>
                    <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Arial Narrow" 
                        Font-Size="8pt" InteractiveDeviceInfos="(Collection)" 
                        WaitMessageFont-Names="Arial Narrow" WaitMessageFont-Size="14pt" Height="674px" 
                        Width="100%">
                        <LocalReport ReportPath="Reports\ScoreCardReviews.rdlc">
                            <DataSources>
                                <rsweb:ReportDataSource DataSourceId="ScoreCardReviews" 
                                    Name="ScoreCardReviewsDataSet" />
                            </DataSources>
                        </LocalReport>
                    </rsweb:ReportViewer>
    
                    <asp:ObjectDataSource ID="ScoreCardReviewsDataSource" runat="server" 
                        OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
                        TypeName="FVGen3.WebUI.Reports.ScoreCardReviewsDataSetTableAdapters.ScoreCardReviewsDataTable1TableAdapter">
                    </asp:ObjectDataSource>
                </td>
            
            </tr>
        </table>

    </form>
</body>
</html>
