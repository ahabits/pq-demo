﻿using FVGen3.WebUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FVGen3.WebUI.Reports
{
    public partial class Notifications : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public ReportModel GetNotification()
        {
            var data = "select orgn.ClientId, orgn.SetupTypeId, orgn.SetupType as NotificationsSentSetupType, convert(varchar,one.EmailSentDateTime,110) AS EmailSentDate , d.DocumentName, one.EmailSentTo, one.VendorId, one.PrequalificationId, one.NotificationId, one.EmailRecordId, case when one.NotificationNo = 1 then 'First' when one.NotificationNo = 2 then 'Second' when one.NotificationNo = 3 then 'Third' end as NotificationNo, dt.DocumentTypeName, ps.PrequalificationStatusName, case when orgn.SetupType=1 and oesn.SetupType=0 then 'Document Expiration - ' + dt.DocumentTypeName when orgn.SetupType=1 and oesn.SetupType=1 then 'Prequalification Expiration - ' + ps.PrequalificationStatusName end as Purpose, oesn.SetupType as EmailSetupSetupType, c.Name as Client, v.Name as Vendor from OrganizationsNotificationsSent orgn LEFT JOIN OrganizationsNotificationsEmailLogs one on one.NotificationId = orgn.NotificationId LEFT JOIN OrganizationsEmailSetupNotifications oesn on oesn.OrgEmailSetupId = orgn.SetupTypeId LEFT JOIN DocumentType dt on dt.DocumentTypeId =  oesn.SetupTypeId and oesn.SetupType = 0 LEFT JOIN PrequalificationStatus ps on ps.PrequalificationStatusId = oesn.SetupTypeId and oesn.SetupType = 1 LEFT JOIN Organizations c on c.OrganizationID = orgn.ClientId LEFT JOIN Organizations v on v.OrganizationID = one.VendorId LEFT JOIN Document d on d.DocumentId = one.DocumentId where orgn.SetupType = 1 and d.DocumentStatus != 'Deleted'";

            var reportData = new FVGen3.WebUI.Models.ReportModel();
            reportData.Sql = data;
            return reportData;

        }
    }
}