﻿using FVGen3.WebUI.Models;
using FVGen3.WebUI.Utilities;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FVGen3.WebUI.Reports
{
    public partial class SubSectionReport1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                var result = CustomReportGeneration(Request.Form["templateid"].ToNullableGuid());


                ReportViewer1.LocalReport.DataSources.Clear();
                ReportViewer1.ProcessingMode = ProcessingMode.Local;

                ReportViewer1.LocalReport.ReportPath = Server.MapPath("SubSectionReport.rdlc");

                DataSet dataSet = new DataSet();

                System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["EFDbContext"].ConnectionString);


                connection.Open();
                System.Data.SqlClient.SqlDataAdapter adapter = new System.Data.SqlClient.SqlDataAdapter(result.Sql, connection);
                var data = result.Data; //Newtonsoft.Json.JsonConvert.DeserializeObject<List<KeyValuePair<string,string>>>(Request.Form["data"]);
                foreach (var d in data)
                {
                    adapter.SelectCommand.Parameters.AddWithValue(d.Key, d.Value);
                }
                adapter.SelectCommand.CommandTimeout = 300;

                adapter.Fill(dataSet);

                ReportDataSource dataSource = new ReportDataSource("SubSectionDataSet", dataSet.Tables[0]);

                ReportViewer1.LocalReport.DataSources.Add(dataSource);
                ReportViewer1.DocumentMapCollapsed = true;
                ReportViewer1.LocalReport.Refresh();

                ReportViewer1.Visible = true;
            }
        }
        public ReportModel CustomReportGeneration(Guid? templateId)
        {
            var result = new ReportModel();
            result.Sql = @"select vendor.Name, q.QuestionText,ui.UserInput from TemplateSubSections tss
                        join Questions q on q.SubSectionId=tss.SubSectionID
                        join QuestionColumnDetails qcd on qcd.QuestionId=q.QuestionID
                        join TemplateSections ts on ts.TemplateSectionID=tss.TemplateSectionID
                        join Templates t on t.TemplateID=ts.TemplateID
                        join ClientTemplates ct on ct.TemplateID=t.TemplateID
                        join LatestPrequalification lp on lp.ClientTemplateId=ct.ClientTemplateID
                        join Organizations vendor on vendor.OrganizationID=lp.VendorId
                        left join PrequalificationUserInput ui on ui.PreQualificationId=lp.PrequalificationId and qcd.QuestionColumnId=ui.QuestionColumnId
                        where ts.TemplateID=@templateId 
                        and q.Visible=1 and tss.SubSectionTypeCondition='SubSectionReport'
                        order by Name,Q.DisplayOrder";
            result.Data.Add(new KeyValuePair<string, string>("@templateId", templateId.ToString()));
            return result;
        }
    }
}