﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="VendorInviteByStatus.aspx.cs" Inherits="FVGen3.WebUI.Reports.VendorInviteByStatus" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Import Namespace="FVGen3.Domain.Entities"%>
<%@ Import Namespace="System.Data"%>
<%@ Import Namespace="FVGen3.WebUI.Utilities"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head2" runat="server">
    <title></title>
    <style>
        #s5_logo_wrap {
            float: left;
            padding-top:4px;  
        }
        #s5_logo {
            cursor: pointer;
            margin-bottom: 21px;
            max-width: 200px;
        } 
    </style> 
</head>

<script runat="server">
       

        protected void SqlDataSource1_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
        {

        }

        protected void PrequalificationDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {

        }
</script>
<body>
    <form id="form2" runat="server">
        <table width="100%" border="0">
            <tr>
                <td>
                    <div id ="s5_logo_wrap">
                        <img alt="logo" src="../images/logoForReports.png" id="s5_logo" />
                    </div>
                </td>
            </tr>
        
            <tr>
                <td>
                    <asp:ScriptManager ID="ScriptManager2" runat="server">
                    </asp:ScriptManager>
                    <rsweb:ReportViewer ID="ReportViewer2" runat="server" Font-Names="Verdana" 
                        Font-Size="8pt" InteractiveDeviceInfos="(Collection)" 
                        WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Height="674px"                     
                        Width="100%">
                        <LocalReport ReportPath="Reports\VendorInviteByStatusReport.rdlc">
                        </LocalReport>
                    </rsweb:ReportViewer>
                </td>
            </tr>
        </table>

    </form>
</body>
</html>

