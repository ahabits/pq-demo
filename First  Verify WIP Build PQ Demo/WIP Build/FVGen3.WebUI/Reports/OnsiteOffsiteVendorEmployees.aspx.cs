﻿using FVGen3.WebUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AhaApps.Libraries.Extensions;

namespace FVGen3.WebUI.Reports
{
    public partial class OnsiteOffsiteVendorEmployees : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        //var result = GetTraining(long.Parse(Request.Form["fromDate"]), Request.Form["toDate"], Request.Form["location"], Request.Form["vendor"], Request.Form["employee"]);
        public ReportModel GetTraining(DateTime fromDate, DateTime toDate, string location, string vendor, string employee, int workingOffset)
        {


            string workingOffsetString = (workingOffset * -1).ToString();

            var data = "";
            data = "select sites.SiteName, o.Name, (c.LastName + ', ' + c.FirstName )as EmployeeName, " +
                    " (c1.LastName + ', ' + c1.FirstName )as SecurityNameCheckin, (c2.LastName + ', ' + c2.FirstName )as SecurityNameCheckout, " +
                    " dateadd(HOUR, " + workingOffsetString + " ,oo.Checkin) as checkin, dateadd(HOUR, " + workingOffsetString + ",oo.Checkout) as checkout, oo.VendorEmployeeID, oo.VendorID,oo.ClientBusinessUnitSiteId,bus.BusinessUnitName " +
                    " from 	dbo.OnsiteOffsite oo " +
                    " join Organizations o on o.OrganizationID = oo.VendorID " +
                    " join Contact c on c.UserID  = oo.VendorEmployeeID " +
                    " left outer join Contact c1 on c1.UserID  = oo.CheckinClientUser " +
                    " left outer join Contact c2 on c2.UserID  = oo.CheckoutClientUser  " +
                    " join ClientBusinessUnits bus on bus.ClientBusinessUnitId = oo.ClientBusinessUnitId " +
                    " join ClientBusinessUnitSites sites on sites.ClientBusinessUnitSiteId = oo.ClientBusinessUnitSiteId " +
                    " where ((oo.Checkin >= @fromDate and oo.Checkin  <= @toDate)  or (oo.Checkout >= @fromDate and oo.Checkout  <= @toDate))  and oo.UserType=1 ";
            if (location != "-1")
            {
                data = data + " and (@location is null  or @location = oo.ClientBusinessUnitSiteId) ";
            }
            if (@vendor != "-1")
            {
                data = data + " and (@vendor is null  or @vendor = oo.VendorID) ";
            }
            if (@employee != "-1")
            {
                data = data + " and (@employee is null or @employee = oo.VendorEmployeeID) ";
            }
            var reportData = new ReportModel();
            reportData.Data = new List<KeyValuePair<string, string>>();
            reportData.Sql = data;
            reportData.Data.Add(new KeyValuePair<string, string>("@fromDate", fromDate.ToString()));
            reportData.Data.Add(new KeyValuePair<string, string>("@toDate", toDate.ToString()));
            reportData.Data.Add(new KeyValuePair<string, string>("@location", location));
            reportData.Data.Add(new KeyValuePair<string, string>("@vendor", vendor));
            reportData.Data.Add(new KeyValuePair<string, string>("@employee", employee));
            //reportData.Data.Add(new KeyValuePair<string, string>("@offset", workingOffset.ToString()));
            return reportData;

        }
    }
}


