﻿using FVGen3.WebUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AhaApps.Libraries.Extensions;
using FVGen3.BusinessLogic;
using Resources;
using System.ServiceModel.Channels;

namespace FVGen3.WebUI.Reports
{
    public partial class CurrentlyOnsite : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        public ReportModel GetTraining(int workingOffset, string location)
        {
//            string workingOffsetString = (workingOffset * -1).ToString();
//            var data = "";
//            data = @"
//select sites.SiteName, o.Name as VendorName, (c.LastName + ', ' + c.FirstName )as VisitorName, (c1.LastName + ', ' + c1.FirstName )as SecurityNameCheckin, 
//                      dateadd(HOUR, " + workingOffsetString + @" ,oo.Checkin) as checkin ,bus.BusinessUnitName as [BUName],null as CompanyName,null as SiteContact 
//                      from
//                      dbo.OnsiteOffsite oo
//                      join Organizations o on o.OrganizationID = oo.VendorID
//                      join Contact c on c.UserID  = oo.VendorEmployeeID
//                      left outer join Contact c1 on c1.UserID  = oo.CheckinClientUser                      
//                      join ClientBusinessUnits bus on bus.ClientBusinessUnitId = oo.ClientBusinessUnitId
//                      join ClientBusinessUnitSites sites on sites.ClientBusinessUnitSiteId = oo.ClientBusinessUnitSiteId
//					  join 
//					  (select Max(Checkin) as checkinMax, max(Checkout) as checkoutMax ,DATEDIFF(hour, Max(Checkin), Max(GETDATE())) as timediff,
//                    VendorEmployeeID, VendorID,ClientID,ClientBusinessUnitSiteID,ClientBusinessUnitId from OnsiteOffsite 
//					Group By VendorEmployeeID, VendorID,ClientID,ClientBusinessUnitSiteID,ClientBusinessUnitId
//                     having DATEDIFF(hour, Max(Checkin), Max(GETDATE())) < 13
//                     and (max(Checkout) is null or max(Checkout) < max(Checkin))) ag on 
//	                    oo.VendorEmployeeID = ag.VendorEmployeeID and
//	                    oo.VendorID = ag.VendorID and
//	                    oo.ClientID = ag.ClientID and
//	                    oo.ClientBusinessUnitID = ag.ClientBusinessUnitID and
//	                    oo.ClientBusinessUnitSiteID = ag.ClientBusinessUnitSiteID and 
//	                    oo.Checkin = ag.checkinMax ";
//            if (location != "-1")
//            {
//                data = data + " and (@location is null  or @location = oo.ClientBusinessUnitSiteId) ";
//            }

//            data = data + @" UNION
//                        select sites.SiteName, null as VendorName, (v.LastName + ', ' + v.FirstName )as VisitorName, (c1.LastName + ', ' + c1.FirstName )as SecurityNameCheckin, 
//                      dateadd(HOUR, " + workingOffsetString + @",oo.Checkin) as checkin,bus.BusinessUnitName as [BUName],v.Company as CompanyName,v.SiteContact as SiteContact
//                       from
//                      dbo.[OnsiteOffsiteVisitor] oo                      
//                      join Visitors v on v.VisitorId  = oo.VisitorId
//                      left outer join Contact c1 on c1.UserID  = oo.CheckinClientUser                      
//                      join ClientBusinessUnits bus on bus.ClientBusinessUnitId = oo.ClientBusinessUnitId
//                      join ClientBusinessUnitSites sites on sites.ClientBusinessUnitSiteId = oo.ClientBusinessUnitSiteId
//					  		join 
//					  (select Max(Checkin) as checkinMax, max(Checkout) as checkoutMax ,DATEDIFF(hour, Max(Checkin), Max(GETDATE())) as timediff,
//                    VisitorID, ClientID,ClientBusinessUnitSiteID,ClientBusinessUnitId from OnsiteOffsiteVisitor 
//					Group By VisitorID, ClientID,ClientBusinessUnitSiteID,ClientBusinessUnitId
//                     having DATEDIFF(hour, Max(Checkin), Max(GETDATE())) < 13
//                     and (max(Checkout) is null or max(Checkout) < max(Checkin))) ag on 
//	                    oo.VisitorID = ag.VisitorID and	                    
//	                    oo.ClientID = ag.ClientID and
//	                    oo.ClientBusinessUnitID = ag.ClientBusinessUnitID and
//	                    oo.ClientBusinessUnitSiteID = ag.ClientBusinessUnitSiteID and 
//	                    oo.Checkin = ag.checkinMax
//
//            ";
//            if (location != "-1")
//            {
//                data = data + " and (@location is null  or @location = oo.ClientBusinessUnitSiteId) ";
//            }
            var OnSiteOffSiteHelper = new OnSiteOffSiteHelper();
            var Systemuser = new SystemUserBusiness();
            var Checkouttime = LocalConstants.Checkout;
            var userid = (Guid)Session["UserId"];
           bool IsOrmatUser = Systemuser.CheckIsOrmatUser(userid);
            if (IsOrmatUser)
            {
                Checkouttime = LocalConstants.OrmatCheckout;
            }
            var data = OnSiteOffSiteHelper.GetVendorCheckinsQuery(workingOffset, location, Checkouttime);

            var reportData = new ReportModel();
            reportData.Data = new List<KeyValuePair<string, string>>();
            reportData.Sql = data;
            reportData.Data.Add(new KeyValuePair<string, string>("@location", location));
            return reportData;
        }
    }
}


