﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FVGen3.WebUI.Reports
{
    public partial class SafetyhistoryReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("SafetyHistoryReport.rdlc");

                DataSet dataSet = new DataSet();

                System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["EFDbContext"].ConnectionString);

                var query = @"select Vendor,
EMRStatsYear,emrVal EMR,
[Number of days away from work: (total from Column K on your OSHA Form)] DaysAwayFromWork,
[Number of fatalities: (total from Column G on your OSHA Form)] Fatalities,
[Number of job transfer or restricted work day cases: (total from Column I on your OSHA Form)] jobTransfer,
[Number of lost work day cases: (total from Column H on your OSHA Form)] LostWorkDay,
[Number of other recordable cases: (total from Column J on your OSHA Form)] OtherRecordableCases,
[RIR: # recordable cases (total from columns G, H, I, J) x 200,000 / Total employee hours worked last year] RIRCases,
[DART: # of DART incidents (total from columns H and I) x 200,000 / Total employee hours worked last year] DARTCases,
[Lost Work Day Case Rate: # lost work day cases (total from column H) x 200,000 / Total employee hours worked last year] LostWorkDayCaseRate
 from(
 select o.Name as Vendor, pesv.QuestionColumnIdValue,pesy.EMRStatsYear, q.QuestionText ,emr.emrVal
 from Prequalification p join Organizations o 
 on o.OrganizationID = p.VendorId join PrequalificationEMRStatsYears pesy 
 on pesy.PrequalificationId = p.PrequalificationId join PrequalificationEMRStatsValues pesv 
 
 on pesy.PrequalEMRStatsYearId = pesv.PrequalEMRStatsYearId 
 join (select PreQualificationId, max(UserInput) emrVal from PrequalificationUserInput ui
 join QuestionColumnDetails qcol on qcol.QuestionColumnId =ui.QuestionColumnId
		 join Questions q on q.QuestionID=qcol.QuestionId 
		 where q.QuestionText='EMR:'
		 group by PreQualificationId,QuestionText
 ) emr on emr.PreQualificationId=p.PrequalificationId
 join QuestionColumnDetails qd 
 on qd.QuestionColumnId = pesv.QuestionColumnId 
 join Questions q on q.QuestionID = qd.QuestionId 
 where ISNUMERIC(pesy.EMRStatsYear)=1 and p.PrequalificationStatusId in (8,9,13,24,25,26) group by o.Name,  pesy.EMRStatsYear,q.QuestionText,pesv.QuestionColumnIdValue,emr.emrVal) as p 
 pivot(  max(QuestionColumnIdValue)  for QuestionText in
  ([Number of days away from work: (total from Column K on your OSHA Form)],
[Number of fatalities: (total from Column G on your OSHA Form)],[Number of job transfer or restricted work day cases: (total from Column I on your OSHA Form)],
[Number of lost work day cases: (total from Column H on your OSHA Form)],
[Number of other recordable cases: (total from Column J on your OSHA Form)],
[RIR: # recordable cases (total from columns G, H, I, J) x 200,000 / Total employee hours worked last year],
[Lost Work Day Case Rate: # lost work day cases (total from column H) x 200,000 / Total employee hours worked last year],
[DART: # of DART incidents (total from columns H and I) x 200,000 / Total employee hours worked last year])
) piv where cast(EMRStatsYear as decimal(18,2))>2012 
order by vendor,EMRStatsYear";
                connection.Open();
                System.Data.SqlClient.SqlDataAdapter adapter = new System.Data.SqlClient.SqlDataAdapter(query, connection);



                adapter.Fill(dataSet);

                ReportDataSource dataSource = new ReportDataSource("SafetyHistoryDataset", dataSet.Tables[0]);

                ReportViewer1.LocalReport.DataSources.Add(dataSource);
                ReportViewer1.DocumentMapCollapsed = true;
                ReportViewer1.LocalReport.Refresh();

                ReportViewer1.Visible = true;
            }
        }
    }
}