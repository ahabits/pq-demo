﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SubSectionReport.aspx.cs" Inherits="FVGen3.WebUI.Reports.SubSectionReport1" %>
<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
    
<form id="form1" runat="server">
        <table width="100%" border="0">
            <tr>
                <td>
                    <div id ="s5_logo_wrap">
                        <img alt="logo" src="../images/logoForReports.png" id="s5_logo" />
                    </div>
                </td>
            </tr>
        
            <tr>
                <td>
                      <asp:ScriptManager ID="ScriptManager1" runat="server">
                    </asp:ScriptManager>
                    <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" 
                        Font-Size="8pt" InteractiveDeviceInfos="(Collection)" 
                        WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Height="674px" Width="100%">
                        <LocalReport ReportPath="Reports\SubSectionReport.rdlc">
                            <DataSources>
                                <rsweb:ReportDataSource DataSourceId="SubSectionReportDataSource" 
                                    Name="SubSectionReport" />
                            </DataSources>
                        </LocalReport>
                    </rsweb:ReportViewer>
                    <asp:ObjectDataSource ID="SubSectionReportDataSource" runat="server" 
                        OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
                        TypeName="FVGen3.WebUI.Reports.SubSectionReportTableAdapters.SubSectionDataTableAdapter">
                    </asp:ObjectDataSource>
                </td>
            </tr>
        </table>

    </form>
</html>
