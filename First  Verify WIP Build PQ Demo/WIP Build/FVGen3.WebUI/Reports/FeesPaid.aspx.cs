﻿using System;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using FVGen3.WebUI.Models;
using FVGen3.WebUI.Utilities;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FVGen3.Domain.Concrete;
using FVGen3.BusinessLogic.Interfaces;
using FVGen3.BusinessLogic;

namespace FVGen3.WebUI.Reports
{
    public partial class FeesPaid1 : System.Web.UI.Page
    {
        IReportsBusiness reportBusiness;
        public FeesPaid1()
        {
        this.reportBusiness = new ReportsBusiness();
        }
        EFDbContext entityDB;

        protected void Page_Load(object sender, EventArgs e)
        {
        if (!this.IsPostBack)
        {
        //ReportViewer1.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource()
        //    {
        //    Name = "FeesPaidDataSet",
        //    Value = reportBusiness.GetFee(Request.Form["feefromdate"], Request.Form["feetodate"])

        //    });
        // var result = reportBusiness.GetFee(Request.Form["feefromdate"], Request.Form["feetodate"]);
       // var result = "select PaymentReceivedAmount as VendorName,PaymentTransactionId as Transactionid,InvoiceAmount as Fee,PaymentCategory as ClientName  from PrequalificationPayments";
        ReportViewer1.LocalReport.ReportPath = Server.MapPath("FeesPaidReport.rdlc");

        //DataSet dataSet = new DataSet();

        //System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["EFDbContext"].ConnectionString);



        //connection.Open();
        //System.Data.SqlClient.SqlDataAdapter adapter = new System.Data.SqlClient.SqlDataAdapter(result, connection);

        ////var data = result.Data; //Newtonsoft.Json.JsonConvert.DeserializeObject<List<KeyValuePair<string,string>>>(Request.Form["data"]);
        ////foreach (var d in data)
        ////{
        ////adapter.SelectCommand.Parameters.AddWithValue(d.Key, d.Value);
        ////}
        //adapter.SelectCommand.CommandTimeout = 300;
        //adapter.Fill(dataSet);

        ReportDataSource dataSource = new ReportDataSource("FeesPaidDataSet");
        dataSource.Value = reportBusiness.GetFee(Request.Form["feefromdate"], Request.Form["feetodate"], Request.Form["TemplateId"].ToNullableGuid() ?? new Guid(), long.Parse(Request.Form["ClientId"]));
        ReportViewer1.LocalReport.DataSources.Add(dataSource);
        ReportViewer1.DocumentMapCollapsed = true;
        ReportViewer1.LocalReport.Refresh();

        ReportViewer1.Visible = true;
        }

        }

        //void LocalReport_SubreportProcessing(object sender, Microsoft.Reporting.WebForms.SubreportProcessingEventArgs e)
        //{
        //    e.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource()
        //    {
        //    Name = "FeesPaidDataSet",
        //    Value = reportBusiness.GetFee(Request.Form["feefromdate"], Request.Form["feetodate"])

        //    });
        //}
        //        public ReportModel CustomReportGeneration(string fromDate, string toDate)
        //        {
        //        var result = new ReportModel();
        //        result.Sql = @"with CTE as
        //(
        //select 
        //org.Name as OrgName,pqp.InvoiceAmount,
        //pf.ClientId,
        //
        //case when pftf.AnnualFeeReceivedAmount is null then qp.PaymentReceivedAmount
        //else pftf.AnnualFeeReceivedAmount end as trainingfee,
        //pqp.PaymentReceivedAmount,pqp.PaymentTransactionId as pqTransactionId,qp.PaymentTransactionId as triningTransactionId,
        //qp.PaymentReceivedAmount as QuizPaymentReceivedAmount
        //
        //
        //from [dbo].[Organizations] org
        // join [dbo].[LatestPrequalification] pf
        //on org.OrganizationID=pf.VendorId
        //LEFT join [dbo].[PrequalificationTrainingAnnualFees] pftf
        //on pf.PrequalificationId=pftf.PrequalificationId
        //LEFT join [dbo].[PrequalificationPayments] pqp
        //on  pf.PrequalificationId=pqp.PrequalificationId
        //LEFT join [dbo].[SystemUsersOrganizations] so
        //on org.OrganizationID=so.OrganizationId
        //LEFT join [dbo].[SystemUsersOrganizationsQuizzes] suoq
        //on so.SysUserOrganizationId=suoq.SysUserOrgId
        //LEFT join [dbo].[QuizPayments] qp
        //on suoq.SysUserOrgQuizId=qp.SysUserOrgQuizId
        //
        //where  pqp.PaymentReceivedDate between @fromdate and @todate or qp.PaymentReceivedDate between @fromDate and @toDate
        //)
        //
        //
        //SELECT OrgName as ClientName,PaymentReceivedAmount as Fee ,InvoiceAmount as VendorName,pqTransactionId as Transactionid
        //		
        //FROM CTE a
        //
        //
        //
        //order by OrgName";

        //        result.Data.Add(new KeyValuePair<string, string>("@fromDate", fromDate.ToStringNullSafe()));
        //        result.Data.Add(new KeyValuePair<string, string>("@toDate", toDate.ToStringNullSafe()));
        //        return result;

        //        }
    }
}