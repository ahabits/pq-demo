﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ExpiringDocumentWebForm.aspx.cs" Inherits="FVGen3.WebUI.ReportsForAdmin.ExpiringDocumentWebForm" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Import Namespace="FVGen3.Domain.Entities"%>
<%@ Import Namespace="System.Data"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
        <style>
            #s5_logo_wrap {
                float: left;
                padding-top:4px;  
            }
            #s5_logo {
                cursor: pointer;
                margin-bottom: 21px;
                max-width: 200px;
            } 
        </style>
</head>

<script runat="server">
    private void Page_Load(object sender, System.EventArgs e)
    {
        try
        {
            if (!this.IsPostBack)
            {
                // sumanth on 19/08/2015
                // added try & catch block to handle exceptions and if the address bar is changed then it is redirected to previous page.Refer FVOS-1 

                //try
                //{

                //var sql = "";
                //try
                //{

                //    sql = Request.Form["sql"].ToString();
                //}
                //catch
                //{
                //    sql = "";

                //}
                //if (sql != "")
                //{
                var result = ExpiredDocument(Request.Form["doctypeList"]);

                ReportViewer1.LocalReport.DataSources.Clear();
                ReportViewer1.ProcessingMode = ProcessingMode.Local;

                ReportViewer1.LocalReport.ReportPath = Server.MapPath("ExpiredReport.rdlc");

                DataSet dataSet = new DataSet();

                System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["EFDbContext"].ConnectionString);
                connection.Open();
                System.Data.SqlClient.SqlDataAdapter adapter = new System.Data.SqlClient.SqlDataAdapter(result.Sql, connection);

                var data = result.Data; //Newtonsoft.Json.JsonConvert.DeserializeObject<List<KeyValuePair<string, string>>>(Request.Form["data"]);
                foreach (var d in data)
                {
                    
                    adapter.SelectCommand.Parameters.AddWithValue(d.Key, d.Value);
                }
                adapter.SelectCommand.CommandTimeout = 300;

                adapter.Fill(dataSet);

                ReportDataSource dataSource = new ReportDataSource("DataSet1", dataSet.Tables[0]);

                ReportViewer1.LocalReport.DataSources.Add(dataSource);
                ReportViewer1.DocumentMapCollapsed = true;
                ReportViewer1.LocalReport.Refresh();

                ReportViewer1.Visible = true;
                //}
                //else
                //{
                //    Response.Redirect("~/AdminVendors/CustomReports");

                //}
                //}
                //catch (Exception ex)
                //{
                //    Response.Write(ex.Message);

                //}
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void SqlDataSource1_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {

    }

    protected void PrequalificationDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {

    }
</script>

<body>
    <form id="form1" runat="server">
        <table width="100%" border="0">
            <tr>
                <td>
                    <div id ="s5_logo_wrap">
                        <img alt="logo" src="../images/logoForReports.png" id="s5_logo" />
                    </div>
                </td>
            </tr> 
            <tr>
                <td>
                    <asp:ScriptManager ID="ScriptManager1" runat="server">
                    </asp:ScriptManager>
                    <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" 
                        Font-Size="8pt" InteractiveDeviceInfos="(Collection)" 
                        WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Height="500px" 
                        Width="100%">
                        <LocalReport ReportPath="Reports\ExpiredReport.rdlc">
                        </LocalReport>
                    </rsweb:ReportViewer>
                </td>
            </tr>
        </table>

    </form>
</body>
</html>
