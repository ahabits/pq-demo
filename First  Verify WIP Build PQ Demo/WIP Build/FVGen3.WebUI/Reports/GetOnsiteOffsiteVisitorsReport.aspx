﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GetOnsiteOffsiteVisitorsReport.aspx.cs" Inherits="FVGen3.WebUI.Reports.GetOnsiteOffsiteVisitorsReport" %>

<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>
<%@ Import Namespace="FVGen3.Domain.Entities"%>
<%@ Import Namespace="System.Data"%> 


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
        <style>
            #s5_logo_wrap {
                float: left;
                padding-top:4px;  
            }
            #s5_logo {
                cursor: pointer;
                margin-bottom: 21px;
                max-width: 200px;
            } 
        </style>
</head>

<script runat="server">
            private void Page_Load(object sender, System.EventArgs e)
            {
                try
                {
                    if (!this.IsPostBack)
                    {
                        DateTime toDate = DateTime.Parse(Request.Form["OOVisitortodate"]);
                        DateTime fromDate = DateTime.Parse(Request.Form["OOVisitorfromdate"]);
                        Int32 timeZoneOffset = int.Parse(Request.Form["timeZoneOffset"]);

                        var serverOffset = TimeZoneInfo.Local.GetUtcOffset(DateTime.UtcNow);
                        var workingOffset = (timeZoneOffset / 60) + serverOffset.Hours;

                        
                        
                        bool singleDate = (DateTime.Compare(toDate, fromDate) == 0);
                        toDate = toDate.AddDays(1).AddSeconds(-1);

                        var result = GetVisitors(fromDate, toDate, Request.Form["OOVisitorlocation"], Request.Form["OOvisitor"], workingOffset);
                                                    ReportViewer1.LocalReport.DataSources.Clear();
                            ReportViewer1.ProcessingMode = ProcessingMode.Local;

                            ReportViewer1.LocalReport.ReportPath = Server.MapPath("OnsiteOffsiteVisitors.rdlc");

                            DataSet dataSet = new DataSet();

                            System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["EFDbContext"].ConnectionString);
                            connection.Open();
                            System.Data.SqlClient.SqlDataAdapter adapter = new System.Data.SqlClient.SqlDataAdapter(result.Sql, connection);
                            var data = result.Data;
                            foreach (var d in data)
                            {                                
                                adapter.SelectCommand.Parameters.AddWithValue(d.Key, d.Value);
                            }
                            adapter.SelectCommand.CommandTimeout = 300;
                        
                            adapter.Fill(dataSet);
                            
                            ReportDataSource dataSource = new ReportDataSource("OnsiteOffsiteVisitorsDataSet", dataSet.Tables[0]);
                            
                            ReportParameter[] parameters = new ReportParameter[4];
                            if (singleDate)
                            {
                                parameters[0] = new ReportParameter("column_visible", "True");
                            }
                            else
                            {
                                parameters[0] = new ReportParameter("column_visible", "False");
                            }
                            parameters[1] = new ReportParameter("offset", workingOffset.ToString());
                            parameters[2] = new ReportParameter("start_date", toDate.ToString());
                            parameters[3] = new ReportParameter("end_date", fromDate.ToString());
                            
                        
                            ReportViewer1.LocalReport.SetParameters(parameters);

                            ReportViewer1.LocalReport.DataSources.Add(dataSource);
                            ReportViewer1.DocumentMapCollapsed = true;
                            ReportViewer1.LocalReport.Refresh();

                            ReportViewer1.Visible = true;
                        
                    }
                
                }
                catch(Exception ex)
                {
                    throw ex;
                }
            }

            protected void SqlDataSource1_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
            {

            }

            protected void PrequalificationDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
            {

            }
</script>    

<body>
    <form id="form1" runat="server">
        <table width="100%" border="0">
            <tr>
                <td>
                    <div id ="s5_logo_wrap">
                        <img alt="logo" src="../images/logoForReports.png" id="s5_logo" />
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                   <asp:ScriptManager ID="ScriptManager1" runat="server">
                    </asp:ScriptManager>
                    <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Arial Narrow" 
                        Font-Size="8pt" InteractiveDeviceInfos="(Collection)" 
                        WaitMessageFont-Names="Arial Narrow" WaitMessageFont-Size="14pt" Height="674px" 
                        Width="100%">
                        <LocalReport ReportPath="Reports\OnsiteOffsiteVisitors.rdlc">
                            <DataSources>
                                <rsweb:ReportDataSource DataSourceId="OnsiteOffsiteVisitorsDataSource" 
                                    Name="OnsiteOffsiteVisitorsDataSet" />
                            </DataSources>
                        </LocalReport>
                    </rsweb:ReportViewer>
    
                    <asp:ObjectDataSource ID="OnsiteOffsiteVisitorsDataSource" runat="server" 
                        OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
                        TypeName="FVGen3.WebUI.Reports.OnsiteOffsiteVisitorsDataSetTableAdapters.OnsiteOffsiteVisitorsDataTable1TableAdapter">
                    </asp:ObjectDataSource>
                </td>
            
            </tr>
        </table>

    </form>
</body>
</html>

