﻿using System;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using FVGen3.WebUI.Models;
using FVGen3.WebUI.Utilities;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FVGen3.Domain.Concrete;
using System.Data;

namespace FVGen3.WebUI.Reports
{
    public partial class StatusStatistics1 : System.Web.UI.Page
    {
        EFDbContext entityDB;

      

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                var result = CustomReportGeneration(Request.Form["templateid"].ToNullableGuid() ?? new Guid(), long.Parse(Request.Form["ClientId"]));
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("StatusStatisticsReport.rdlc");

                DataSet dataSet = new DataSet();

                System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["EFDbContext"].ConnectionString);

                var query = result.Sql;

                connection.Open();
                System.Data.SqlClient.SqlDataAdapter adapter = new System.Data.SqlClient.SqlDataAdapter(query, connection);



                adapter.Fill(dataSet);

                ReportDataSource dataSource = new ReportDataSource("StatusStatisticsDataSet", dataSet.Tables[0]);

                ReportViewer1.LocalReport.DataSources.Add(dataSource);
                ReportViewer1.DocumentMapCollapsed = true;
                ReportViewer1.LocalReport.Refresh();

                ReportViewer1.Visible = true;
            }

        }
        public ReportModel CustomReportGeneration(Guid templateId, long clientId)
        {
            //  var clientTemplate = entityDB.ClientTemplates.FirstOrDefault(r => r.ClientID == clientId && r.TemplateID == templateId);
            var result = new ReportModel();
            result.Sql = @"WITH CTE as ( SELECT     RN = rank() OVER (partition by PrequalificationId ORDER BY statuschangedate), 
    PrequalificationId,PrequalificationStatusId,StatusChangeDate,PrequalificationStart FROM PrequalificationStatusChangeLog
	 where PrequalificationStatusId in(3,5,7,16,15))
	 
	 select Vendor,[Pending-Renewal] as PendingRenewal ,[Pending] as Pending,[Client Review] as ClientReview,[Incomplete] as Incomplete,[Processing] as Processing from(
	 
	 select O.Name as Vendor,ps.PrequalificationStatusName,case  when cur.StatusChangeDate IS NOT NULL then ABS(datediff(day,cur.StatusChangeDate,nex.StatusChangeDate))
	 else ABS(datediff(day,lp.prequalificationstart,nex.StatusChangeDate)) end as timeperiod from CTE cur 
	 full join CTE nex 
	 on cur.PrequalificationId=nex.PrequalificationId and nex.RN=cur.RN+1
	
	 join LatestPrequalification as lp
	 on lp.PrequalificationId=nex.PrequalificationId
	 join Organizations as o
	 on o.OrganizationID=lp.vendorid
	join PrequalificationStatus as ps
	on ps.PrequalificationStatusId=nex.PrequalificationStatusId
join ClientTemplates as ct
	on ct.ClientTemplateID=lp.ClientTemplateId
	
	where lp.clientid= " + clientId + @" and ct.TemplateId ='"+ templateId +"'" +@"
	) as p
	PIVOT  
(  
Max(timeperiod)  
FOR PrequalificationStatusName IN ([Pending], [Processing], [Incomplete], [Client Review], [Pending-Renewal])  
) pp";

            return result;
        }
    }
}