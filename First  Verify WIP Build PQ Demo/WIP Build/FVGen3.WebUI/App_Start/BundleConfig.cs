﻿using System.Web;
using System.Web.Optimization;

namespace FVGen3.WebUI
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                        "~/Scripts/jquery-ui-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.unobtrusive*",
                        "~/Scripts/jquery.unobtrusive*",                        
                        "~/Scripts/jquery.validate*"));
            bundles.Add(new ScriptBundle("~/bundles/prequalification").Include(
                        "~/Scripts/Application/Prequalification/TemplateSectionsList.js",
                        "~/Scripts/jquery-migrate-1.2.1.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/common").Include(
                       "~/js/jquery-1.9.1.min.js", "~/js/common.js", "~/Scripts/Application/Common/Http.js", "~/Scripts/Application/Common/Tabs.js"));
            bundles.Add(new ScriptBundle("~/bundles/kendo").Include(
                "~/Scripts/kendo/kendo.all.min.js", "~/Scripts/kendo/jszip.min.js"));


            bundles.Add(new ScriptBundle("~/bundles/FinancialAnalytics").Include(
                     "~/Scripts/Application/Prequalification/FinancialDataView.js"));
            bundles.Add(new ScriptBundle("~/bundles/InviteVendor")
                .Include("~/js/termsModelInvite.js", "~/Scripts/Application/Common/Tabs.js",
                         "~/Scripts/Application/Vendor/Vendor.js",
                         "~/Scripts/Application/Biddings/Biddings.js",
                         "~/Scripts/Application/Prequalification/Prequalifications.js", 
                         "~/Scripts/Application/Vendor/VendorInvites.js",
                         "~/Scripts/Application/Proficiency/Proficiency.js"
                         ));
            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new StyleBundle("~/Content/css").Include("~/Content/site.css"));
       
            bundles.Add(new StyleBundle("~/Content/themes/base/css").Include(
                        "~/Content/themes/base/jquery.ui.core.css",
                        "~/Content/themes/base/jquery.ui.resizable.css",
                        "~/Content/themes/base/jquery.ui.selectable.css",
                        "~/Content/themes/base/jquery.ui.accordion.css",
                        "~/Content/themes/base/jquery.ui.autocomplete.css",
                        "~/Content/themes/base/jquery.ui.button.css",
                        "~/Content/themes/base/jquery.ui.dialog.css",
                        "~/Content/themes/base/jquery.ui.slider.css",
                        "~/Content/themes/base/jquery.ui.tabs.css",
                        "~/Content/themes/base/jquery.ui.datepicker.css",
                        "~/Content/themes/base/jquery.ui.progressbar.css"
                        //"~/Content/themes/base/jquery.ui.theme.css"
                        ));
            //bundles.Add(new StyleBundle("~/bundles/kendo/css").Include(
            //          "~/css/kendo/kendo.default.min.css", "~/css/kendo/kendo.common.min.css", "~/css/kendo/kendo.default.mobile.min.css", "~/css/kendo/images/*.*"));
            bundles.Add(new StyleBundle("~/bundles/admindashboard/css").Include(
                                  "~/css/common.css", "~/css/appDashboard.css", "~/css/index.css"));

            bundles.Add(new StyleBundle("~/css/kendo/bundle").Include(
                "~/css/kendo/kendo.default.min.css", "~/css/kendo/kendo.common.min.css", "~/css/kendo/kendo.default.mobile.min.css"));

        }
    }
}