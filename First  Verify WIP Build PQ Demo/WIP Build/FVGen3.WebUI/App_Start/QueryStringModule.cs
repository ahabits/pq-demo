/***************************
Page Created Date:  09/19/2013
Created By: Rajesh Pendela
Purpose:
Version: 1.0
***************************/


#region Using

using System;
using System.IO;
using System.Web;
using System.Text;
using System.Security.Cryptography;

#endregion


/// <summary>
/// Summary description for QueryStringModule
/// </summary>
public class QueryStringModule : IHttpModule
{

  #region IHttpModule Members

  public void Dispose()
  {
    // Nothing to dispose
  }

  public void Init(HttpApplication context)
  {
    //context.BeginRequest += new EventHandler(context_BeginRequest);
  }

  #endregion

  public const string PARAMETER_NAME = "fvaskey=";
  public const string ENCRYPTION_KEY = "RG1985";

  //void context_BeginRequest(object sender, EventArgs e)
  //{
  //  HttpContext context = HttpContext.Current;
  //  if (context.Request.Url.OriginalString.Contains("aspx") && context.Request.RawUrl.Contains("?"))
  //  {
  //    string query = ExtractQuery(context.Request.RawUrl);
  //    string path = GetVirtualPath();

  //    if (query.StartsWith(PARAMETER_NAME, StringComparison.OrdinalIgnoreCase))
  //    {
  //      // Decrypts the query string and rewrites the path.
  //      string rawQuery = query.Replace(PARAMETER_NAME, string.Empty);
  //      string decryptedQuery = Decrypt(rawQuery);
  //      context.RewritePath(path, string.Empty, decryptedQuery);
  //    }
  //    else if (context.Request.HttpMethod == "GET")
  //    {
  //      // Encrypt the query string and redirects to the encrypted URL.
  //      // Remove if you don't want all query strings to be encrypted automatically.
  //      string encryptedQuery = Encrypt(query);
  //      context.Response.Redirect(path + encryptedQuery);
  //    }
  //  }
  //}

  /// <summary>
  /// Parses the current URL and extracts the virtual path without query string.
  /// </summary>
  /// <returns>The virtual path of the current URL.</returns>
  public static string GetVirtualPath()
  {
    string path = HttpContext.Current.Request.RawUrl;
    path = path.Substring(0, path.IndexOf("?"));
    path = path.Substring(path.LastIndexOf("/") + 1);
    return path;
  }

  /// <summary>
  /// Parses a URL and returns the query string.
  /// </summary>
  /// <param name="url">The URL to parse.</param>
  /// <returns>The query string without the question mark.</returns>
  public static string ExtractQuery(string url)
  {
    int index = url.IndexOf("?") + 1;
    return url.Substring(index);
  }

  #region Encryption/decryption

  /// <summary>
  /// The salt value used to strengthen the encryption.
  /// </summary>
  private readonly static byte[] SALT = Encoding.ASCII.GetBytes(ENCRYPTION_KEY.Length.ToString());

  /// <summary>
  /// Encrypts any string using the Rijndael algorithm.
  /// </summary>
  /// <param name="inputText">The string to encrypt.</param>
  /// <returns>A Base64 encrypted string.</returns>
  public static string Encrypt(string inputText)
  {
    RijndaelManaged rijndaelCipher = new RijndaelManaged();
    byte[] plainText = Encoding.Unicode.GetBytes(inputText);
    PasswordDeriveBytes SecretKey = new PasswordDeriveBytes(ENCRYPTION_KEY, SALT);

    using (ICryptoTransform encryptor = rijndaelCipher.CreateEncryptor(SecretKey.GetBytes(32), SecretKey.GetBytes(16)))
    {
      using (MemoryStream memoryStream = new MemoryStream())
      {
        using (CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
        {
          cryptoStream.Write(plainText, 0, plainText.Length);
          cryptoStream.FlushFinalBlock();
          return "?" + PARAMETER_NAME + HttpUtility.UrlEncode(Convert.ToBase64String(memoryStream.ToArray()));
        }
      }
    }
  }

  /// <summary>
  /// Decrypts a previously encrypted string.
  /// </summary>
  /// <param name="inputText">The encrypted string to decrypt.</param>
  /// <returns>A decrypted string.</returns>
  public static string Decrypt(string inputText)
  {
    var extraParams = "";
    if (inputText.IndexOf("&") != -1)
    {
        extraParams = inputText.Substring(inputText.IndexOf("&"));
        inputText = inputText.Substring(0, inputText.IndexOf("&"));
    }
        RijndaelManaged rijndaelCipher = new RijndaelManaged();
    string urlDecodedText = HttpUtility.UrlDecode(inputText).Replace(" ", "+");
    byte[] encryptedData = Convert.FromBase64String(urlDecodedText);
    PasswordDeriveBytes secretKey = new PasswordDeriveBytes(ENCRYPTION_KEY, SALT);

    using (ICryptoTransform decryptor = rijndaelCipher.CreateDecryptor(secretKey.GetBytes(32), secretKey.GetBytes(16)))
    {
      using (MemoryStream memoryStream = new MemoryStream(encryptedData))
      {
        using (CryptoStream cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read))
        {
          byte[] plainText = new byte[encryptedData.Length];
          int decryptedCount = cryptoStream.Read(plainText, 0, plainText.Length);
          return Encoding.Unicode.GetString(plainText, 0, decryptedCount) + extraParams;
        }
      }
    }
  }

  #endregion

}
