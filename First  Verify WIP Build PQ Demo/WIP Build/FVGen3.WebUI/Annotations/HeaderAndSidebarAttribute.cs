﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FVGen3.Domain.Entities;
using FVGen3.WebUI.Controllers;

namespace FVGen3.WebUI.Annotations
{
    public class HeaderAndSidebarAttribute : ActionFilterAttribute
    {
        public string headerName { get; set; }
        public string sideMenuName { get; set; }
        public string ViewName { get; set; }
        public bool isPermissionRequired { get; set; }
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {

            List<LocalHeaderMenuModel> headerMenu=(List<LocalHeaderMenuModel>)filterContext.Controller.ControllerContext.HttpContext.Session["headerMenus"];
            try
            {
                if (headerMenu != null && !headerName.Equals(""))
                {
                    headerMenu.ForEach(record => record.isChecked = false);
                    headerMenu.FirstOrDefault(record => record.menuName == headerName).isChecked = true;
                    //if (!sideMenuName.Equals(""))
                    {
                        headerMenu.FirstOrDefault(record => record.menuName == headerName)?.sideMenus.ForEach(record => record.isChecked = false);
                        headerMenu.FirstOrDefault(record => record.menuName == headerName).sideMenus.FirstOrDefault(record => record.menuName == sideMenuName).isChecked = true;
                        try
                        {
                            filterContext.Controller.ViewBag.ViewPermissions=headerMenu.FirstOrDefault(record => record.menuName == headerName).sideMenus.FirstOrDefault(record => record.menuName == sideMenuName).ViewsAndPermissions.FirstOrDefault(rec => rec.ViewName == ViewName).ViewPermission;
                        }
                        catch 
                        {
                            filterContext.Controller.ViewBag.ViewPermissions = ViewName == null ? "NoViewName" : "UnhandledException";
                        }
                        //filterContext.Controller.ViewBag.ViewPermissions = headerMenu.FirstOrDefault(record => record.menuName == headerName).sideMenus.FirstOrDefault(record => record.menuName == sideMenuName).ViewPermission;
                    }
                }
            }
            catch
            {
                // ignored
            }
            filterContext.Controller.ViewBag.headerMenus = headerMenu;
            if (isPermissionRequired &&
                           !(filterContext.Controller.ViewBag.ViewPermissions+"").Contains("R"))
            {
                var controller = (BaseController)filterContext.Controller;
                filterContext.Result = controller.RedirectToAction("../SystemUsers/UnauthorizedAccess");
            }
            //filterContext.Controller.ViewBag.ViewPermissions = "IR";
        }
    }
}