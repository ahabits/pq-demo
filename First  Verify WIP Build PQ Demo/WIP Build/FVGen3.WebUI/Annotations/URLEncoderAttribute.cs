﻿/*
Page Created Date:  09/19/2013
Created By: Rajesh Pendela
Purpose: URL Encryption and decryption
Version: 1.0
****************************************************
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FVGen3.WebUI.Annotations
{
    public class URLEncoderAttribute : System.Web.Mvc.AuthorizeAttribute 
    {
        //public string lastPageUrl { get; set; }
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            HttpContext context = HttpContext.Current;
            if (context.Request.RawUrl.Contains("?"))
            {
                string query = QueryStringModule.ExtractQuery(context.Request.RawUrl);
                string path = QueryStringModule.GetVirtualPath();

                //if (context.Request.Url.OriginalString.Contains(QueryStringModule.PARAMETER_NAME) && context.Request.HttpMethod == "GET")
                //{
                //    //do nothing
                //    //programmer already sended encrypted key
                //}
                if (query.StartsWith(QueryStringModule.PARAMETER_NAME, StringComparison.OrdinalIgnoreCase))
                {
                    // Decrypts the query string and rewrites the path.
                    string rawQuery = query.Replace(QueryStringModule.PARAMETER_NAME, string.Empty);
                    string decryptedQuery = QueryStringModule.Decrypt(rawQuery);
                    context.RewritePath(path, string.Empty, decryptedQuery);
                }
                else if (context.Request.HttpMethod == "GET")
                {
                    // Encrypt the query string and redirects to the encrypted URL.
                    // Remove if you don't want all query strings to be encrypted automatically.
                    string encryptedQuery = QueryStringModule.Encrypt(query);
                    context.Response.Redirect(path + encryptedQuery,true);
                    return false; //To Stop controller calling twice..
                }
            }
            return true;
        }
        
    }
}