﻿/*
Page Created Date:  07/16/2013
Created By: Rajesh Pendela
Purpose:
Version: 1.0
****************************************************
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FVGen3.WebUI.Annotations
{
    public class SessionExpireAttribute : System.Web.Mvc.AuthorizeAttribute 
    {
        public string lastPageUrl { get; set; }
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {

            HttpContext ctx = HttpContext.Current;
            //if (string.IsNullOrEmpty(lastPageUrl))
            {
                lastPageUrl = HttpContext.Current.Request.Url.PathAndQuery;
            }

            // check if session is supported
            if (ctx.Session["UserId"] == null)
            {
                ctx.Session.Clear();
                ctx.Session.Abandon();
                ctx.Response.Redirect("~/SystemUsers/Login?lastLoginPage=" + HttpUtility.UrlEncode(lastPageUrl) + "&messageType=3",true); //RedirectToAction("Login");
                //lastPageUrl = "";
                return false;
            }
            //lastPageUrl = "";
            return true;
        }
        
    }
}