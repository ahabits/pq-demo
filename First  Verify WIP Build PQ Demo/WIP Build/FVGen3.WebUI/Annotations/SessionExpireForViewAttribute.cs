﻿/*
Page Created Date:  07/16/2013
Created By: Rajesh Pendala, Kiran Talluri
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FVGen3.WebUI.Annotations
{
    public class SessionExpireForViewAttribute : System.Web.Mvc.AuthorizeAttribute 
    {
        public string pageType { get; set; }
        protected override bool AuthorizeCore( HttpContextBase httpContext )
        {
            HttpContext ctx = HttpContext.Current;
            
            // check if session is supported
            if ( ctx.Session["UserId"] == null )
            {
                ctx.Session.Clear();
                ctx.Session.Abandon();
                
                //ctx.Response.Redirect("~/SystemUsers/Login?lastLoginPage="+lastPageUrl+""); //RedirectToAction("Login");
                //if(pageType==null||pageType=="partial")
                //    ctx.Response.Write("Session expired");
                //else//popupview
                    ctx.Response.Redirect("~/Utilities/SessionExpirePage",true);
                return false;
            }
            return true;
            
        }
    }
}