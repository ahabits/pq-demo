﻿/*
Page Created Date:  28/05/2013
Created By: Rajesh Pendela
Purpose:All operations of prequalification will be controlled here
Version: 1.0
****************************************************
Version: 2.0
Date: 07/25/2013
Changes by : Rajesh Pendela
Changes: Pop up view of Dynamic subsection for Prequalification sites
*****************************************************
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FVGen3.WebUI.Annotations;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using FVGen3.WebUI.Utilities;
using System.Net.Mail;
using iTextSharp.text.pdf;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using FVGen3.BusinessLogic;
using AhaApps.Libraries.Extensions;
using FVGen3.BusinessLogic.Interfaces;
using FVGen3.WebUI.Constants;
using FVGen3.WebUI.Models;
using Resources;
using PayPal.Sample;
using PayPal.Sample.Utilities;
using FVGen3.BusinessLogic.Extentions;
using FVGen3.DataLayer.DTO;
using System.Web.Script.Serialization;
using FVGen3.Domain.Concrete;
using FVGen3.Domain.Entities;
using FVGen3.Domain.ViewModels;
using FVGen3.Domain.LocalModels;

namespace FVGen3.WebUI.Controllers
{
    public class PrequalificationController : BaseController
    {

        //
        // GET: /Prequalification/
        EFDbContext entityDB;
        private IPrequalificationBusiness _prequalification;
        private ITemplateToolBusiness _templateToolBusiness;
        private IOrganizationBusiness _organization;
        private IClientBusiness _clientBusiness;
        private IPrequalificationSitesBusiness _pqSites;
        private IDocumentBusiness _documents;
        private ISystemUserBusiness _sysUser;
        private IEmailTemplateBusiness _email;
        private IEmployeeBusiness _employee;
        private RequestFlow flow = new RequestFlow();
        private PrequalificationHelper helper = new PrequalificationHelper();
        public PrequalificationController(IDocumentBusiness _documents, IPrequalificationSitesBusiness _pqSites, ISystemUserBusiness _sysUser, IClientBusiness clientBusiness, ITemplateToolBusiness templateToolBusiness, IPrequalificationBusiness _prequalification, IOrganizationBusiness organization, IEmailTemplateBusiness email, IEmployeeBusiness employee)
        {
            entityDB = new EFDbContext();
            this._prequalification = _prequalification;
            _templateToolBusiness = templateToolBusiness;
            _organization = organization;
            _clientBusiness = clientBusiness;
            this._pqSites = _pqSites;
            this._documents = _documents;
            this._sysUser = _sysUser;
            _email = email; var a = new List<long>();
            _employee = employee;
        }

        [SessionExpire(lastPageUrl = "../SystemUsers/Dashboard")]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "PREQUALIFICATIONREQUIREMENTS", sideMenuName = "", ViewName = "TemplatesList")]
        public ActionResult TemplatesList(long PreQualificationId, long? oldPreQualificationId, Guid? templateId)//Rajesh on 9/21/2013
        {

            if (PreQualificationId == -1)//Means He clicked Renewal
            {
                //PreQualificationId = AddNewPrequalification(PreQualificationId);
                //Rajesh on 9/21/2013
                long tempPreQualificationId = AddNewPrequalification(oldPreQualificationId);
                if (tempPreQualificationId != -1)
                    PreQualificationId = tempPreQualificationId;
                //Ends<<<
                else
                {
                    var oldPrequalification = entityDB.Prequalification.Find(oldPreQualificationId);
                    var latestPrequalification = entityDB.LatestPrequalification.FirstOrDefault(rec => rec.VendorId == oldPrequalification.VendorId && rec.ClientId == oldPrequalification.ClientId);

                    return
                 Redirect("../Prequalification/TemplateSectionsList?templateId=" +
                          latestPrequalification.ClientTemplates.TemplateID +
                          "&TemplateSectionID=-1&PreQualificationId=" + latestPrequalification.PrequalificationId);
                }

                //To add Prequalification Logs
                Prequalification preQualificationDetails_loc = entityDB.Prequalification.Find(PreQualificationId);
                PrequalificationStatusChangeLog preQualificationLog = new PrequalificationStatusChangeLog();
                preQualificationLog.PrequalificationId = PreQualificationId;
                preQualificationLog.PrequalificationStatusId = 15;
                preQualificationLog.StatusChangeDate = DateTime.Now;
                preQualificationLog.StatusChangedByUser = new Guid(Session["UserId"].ToString());
                preQualificationLog.PrequalificationStart = preQualificationDetails_loc.PrequalificationStart;
                preQualificationLog.PrequalificationFinish = preQualificationDetails_loc.PrequalificationFinish;
                preQualificationLog.CreatedDate = DateTime.Now;
                entityDB.PrequalificationStatusChangeLog.Add(preQualificationLog);
                entityDB.SaveChanges();
                //>>>Ends

                entityDB = new EFDbContext();
            }
            //This session value was created for Template sections list

            Prequalification preQualificationDetails = entityDB.Prequalification.Find(PreQualificationId);

            ViewBag.CopyAnswers = (preQualificationDetails.Vendor.CopyAnswers == null) ? 0 : (int)preQualificationDetails.Vendor.CopyAnswers;

            //////////////////

            long[] prequalificationStatus = { 1, 2 };
            int status = Array.IndexOf(prequalificationStatus, preQualificationDetails.PrequalificationStatusId);

            if (status == -1 && preQualificationDetails.ClientTemplates != null)
            {
                if (preQualificationDetails.locked != null && preQualificationDetails.locked == true)
                {
                    SendUnlockRequestNotification(preQualificationDetails.PrequalificationId, preQualificationDetails);
                    return Redirect("../SystemUsers/Dashboard");
                }
                return Redirect("../Prequalification/TemplateSectionsList?templateId=" + preQualificationDetails.ClientTemplates.TemplateID + "&TemplateSectionID=-1&PreQualificationId=" + PreQualificationId);
            }



            if (preQualificationDetails.PrequalificationStatusId != 2)
            {
                //Rajesh DT:-8/7/2013>>>>
                preQualificationDetails.PrequalificationStart = DateTime.Now;
                DateTime FinishDateTime = new DateTime();
                var preStatus = entityDB.PrequalificationStatus.FirstOrDefault(rec => rec.PrequalificationStatusId == 2);
                if (preStatus != null)
                {
                    if (preStatus.PeriodLengthUnit.ToLower().Equals("d"))
                        FinishDateTime = DateTime.Now.AddDays(preStatus.PeriodLength);
                    else if (preStatus.PeriodLengthUnit.ToLower().Equals("m"))
                        FinishDateTime = DateTime.Now.AddMonths(preStatus.PeriodLength);
                    else if (preStatus.PeriodLengthUnit.ToLower().Equals("y"))
                        FinishDateTime = DateTime.Now.AddYears(preStatus.PeriodLength);
                }
                preQualificationDetails.PrequalificationFinish = FinishDateTime;
                // Ends <<<<<



                //To add Prequalification Logs
                PrequalificationStatusChangeLog preQualificationLog = new PrequalificationStatusChangeLog();
                preQualificationLog.PrequalificationId = preQualificationDetails.PrequalificationId;
                preQualificationLog.PrequalificationStatusId = 2;
                preQualificationLog.StatusChangeDate = DateTime.Now;
                preQualificationLog.StatusChangedByUser = new Guid(Session["UserId"].ToString());
                preQualificationLog.PrequalificationStart = preQualificationDetails.PrequalificationStart;
                preQualificationLog.PrequalificationFinish = preQualificationDetails.PrequalificationFinish;
                preQualificationLog.CreatedDate = DateTime.Now;
                entityDB.PrequalificationStatusChangeLog.Add(preQualificationLog);
                //>>>Ends
            }

            preQualificationDetails.PrequalificationStatusId = 2;
            ViewBag.PreQualificationId = PreQualificationId;
            entityDB.Entry(preQualificationDetails).State = EntityState.Modified;
            entityDB.SaveChanges();
            if (templateId != null)
            {

                Guid clientTemplateId = preQualificationDetails.ClientTemplateId.Value;
                var ISEriClient = entityDB.ClientTemplates.FirstOrDefault(rec => rec.TemplateID == templateId && rec.Organizations.OrganizationType == OrganizationType.SuperClient);
                if (ISEriClient != null) { clientTemplateId = ISEriClient.ClientTemplateID; }

                return Redirect("StartTemplate?ClientTemplateId=" + clientTemplateId + "&PrequalificationId=" + PreQualificationId + "");
            }
            var templatesList = entityDB.ClientTemplates.Where(records => records.ClientID == preQualificationDetails.ClientId && records.Templates.TemplateType == 0 && records.Templates.TemplateStatus == 1 && records.Templates.IsHide != true).OrderBy(rec => rec.Templates.TemplateSequence).ToList();


            if (preQualificationDetails.Client.OrganizationType != OrganizationType.SuperClient)
            {
                templatesList = templatesList.Where(r => r.Templates.IsERI != true).ToList();

            }// Kiran on 03/13/2015 to sort the templates by template name
            ViewBag.templatesList = templatesList;
            return View();
        }

        private long AddNewPrequalification(long? oldPreQualificationId)//Rajesh on 9/21/2013
        {

            if (oldPreQualificationId == null || oldPreQualificationId == -1)//Rajesh on 9/21/2013
                return -1;//Rajesh on 9/21/2013

            var oldPrequalification = entityDB.Prequalification.Find(oldPreQualificationId);
            var latestPrequalificationId = entityDB.LatestPrequalification.FirstOrDefault(rec => rec.VendorId == oldPrequalification.VendorId && rec.ClientId == oldPrequalification.ClientId).PrequalificationId;

            if (oldPrequalification.PrequalificationId != latestPrequalificationId)
                return -1;
            Prequalification p = new Prequalification();
            var InvitationDetails = _prequalification.GetPQInviteDetails(oldPrequalification.PrequalificationId);

            p.InvitationFrom = InvitationDetails.InvitationFrom;
            if (InvitationDetails.InvitationFrom == 0 && !string.IsNullOrEmpty(InvitationDetails.InvitationSentByUser))
                p.InvitationFrom = LocalConstants.PQInvitationId;
            else p.InvitationFrom = InvitationDetails.InvitationFrom;
            p.InvitationSentByUser = InvitationDetails.InvitationSentByUserId;


            p.ClientId = oldPrequalification.ClientId;
            p.ClientTemplateId = oldPrequalification.ClientTemplateId;
            p.PrequalificationCreate = DateTime.Now;
            if (oldPrequalification.ClientTemplates.Templates.LockedByDefault == true && oldPrequalification.ClientTemplates.Templates.RenewalLock == true)
                p.locked = oldPrequalification.ClientTemplates.Templates.RenewalLock;

            DateTime StartDateTime = new DateTime();
            var preStatus = entityDB.PrequalificationStatus.FirstOrDefault(rec => rec.PrequalificationStatusId == 15);
            if (preStatus != null)
            {
                if (preStatus.PeriodLengthUnit.ToLower().Equals("d"))
                    StartDateTime = oldPrequalification.PrequalificationFinish.AddDays(-preStatus.PeriodLength);
                else if (preStatus.PeriodLengthUnit.ToLower().Equals("m"))
                    StartDateTime = oldPrequalification.PrequalificationFinish.AddMonths(-preStatus.PeriodLength);
                else if (preStatus.PeriodLengthUnit.ToLower().Equals("y"))
                    StartDateTime = oldPrequalification.PrequalificationFinish.AddYears(-preStatus.PeriodLength);
            }
            p.PrequalificationFinish = oldPrequalification.PrequalificationFinish;
            p.PrequalificationStart = StartDateTime; p.PrequalificationStatusId = 15;
            p.PrequalificationStatusId = 15;

            //Ends<<<
            p.PrequalificationUserInputs = new List<PrequalificationUserInput>();
            p.UserIdAsCompleter = oldPrequalification.UserIdAsCompleter;
            p.VendorId = oldPrequalification.VendorId;
            entityDB.Prequalification.Add(p);
            logs.Info("Pq Added Successfully " + p.PrequalificationId, "Oldpqid=" + oldPreQualificationId, "Prequalificationcontroller/Addnewprequalification");
            //entityDB.SaveChanges();

            if (oldPrequalification.Vendor.CopyAnswers != null && oldPrequalification.Vendor.CopyAnswers == 2)
            {

                //To Get oldprequalification EMRYears
                //var EMRYearsIDs = (from EMRYears in oldPrequalification.PrequalificationEMRStatsYears.ToList()
                //                   select EMRYears.PrequalEMRStatsYearId).ToList();
                var EMRYearsIDs = oldPrequalification.PrequalificationEMRStatsYears.Select(rec => rec.PrequalEMRStatsYearId).ToList();
                //var ColumnIds = (from EMRValues in entityDB.PrequalificationEMRStatsValues.Where(rec => EMRYearsIDs.Contains(rec.PrequalEMRStatsYearId)).ToList()
                //                 select EMRValues.QuestionColumnId).ToList();
                var ColumnIds = entityDB.PrequalificationEMRStatsValues.Where(rec => EMRYearsIDs.Contains(rec.PrequalEMRStatsYearId)).Select(rec => rec.QuestionColumnId).ToList();
                var columnIdsForClientData = entityDB.TemplateSections.Where(rec => rec.TemplateSectionsPermission.Any(r => r.VisibleTo == 2) || rec.TemplateSectionType == 34).SelectMany(r => r.TemplateSubSections).SelectMany(r => r.Questions).SelectMany(r => r.QuestionColumnDetails).Select(r => r.QuestionColumnId).ToList();
                ColumnIds.AddRange(columnIdsForClientData);
                foreach (var UserInput in oldPrequalification.PrequalificationUserInputs.Where(rec => !ColumnIds.Contains(rec.QuestionColumnId)).ToList())
                {
                    PrequalificationUserInput inputVal = new PrequalificationUserInput();
                    inputVal.PreQualificationId = p.PrequalificationId;
                    inputVal.QuestionColumnId = UserInput.QuestionColumnId;
                    inputVal.UserInput = UserInput.UserInput;
                    entityDB.PrequalificationUserInput.Add(inputVal);
                }
                foreach (var PQClients in oldPrequalification.PrequalificationClientList.ToList())
                {
                    var PQClient_loc = new PrequalificationClient();
                    PQClient_loc.ClientIds = PQClients.ClientIds;
                    PQClients.PQId = p.PrequalificationId;
                    PQClients.Region = PQClients.Region;
                    entityDB.PrequalificationClient.Add(PQClients);
                }
                foreach (var reportingData in oldPrequalification.PrequalificationReportingData.ToList())
                {
                    var ReportingData_Loc = new PrequalificationReportingData();
                    ReportingData_Loc.PrequalificationId = p.PrequalificationId;
                    ReportingData_Loc.ClientId = p.ClientId;
                    ReportingData_Loc.VendorId = p.VendorId;
                    ReportingData_Loc.ReportingType = reportingData.ReportingType;
                    ReportingData_Loc.ReportingDataId = reportingData.ReportingDataId;
                    entityDB.PrequalificationReportingData.Add(ReportingData_Loc);
                }
                foreach (var sites in oldPrequalification.PrequalificationSites.ToList())
                {
                    PrequalificationSites site = new PrequalificationSites();
                    site.PrequalificationId = p.PrequalificationId;
                    site.VendorId = sites.VendorId;
                    site.ClientBULocation = sites.ClientBULocation;
                    site.ClientBusinessUnitId = sites.ClientBusinessUnitId;
                    site.ClientBusinessUnitSiteId = sites.ClientBusinessUnitSiteId;
                    site.ClientId = sites.ClientId;
                    site.Comment = sites.Comment;
                    entityDB.PrequalificationSites.Add(site);
                }
                //Adding Documents


            }

            entityDB.SaveChanges();
            var QuestionWith1000 = new Questions();
            //Update EMRYears using questionId 1000
            var templateSections = oldPrequalification.ClientTemplates.Templates.TemplateSections.Where(rec => !rec.TemplateSectionsPermission.Any(r => r.VisibleTo == 2) && rec.TemplateSectionType != 34 && !rec.SectionName.EndsWith("approval", StringComparison.CurrentCultureIgnoreCase)).ToList();
            foreach (var TemplateSection in templateSections)
            {
                var YearWiseEMR = TemplateSection.TemplateSubSections.FirstOrDefault(rec => rec.SubSectionTypeCondition == "YearWiseStats");
                if (YearWiseEMR != null)
                {
                    var Question = YearWiseEMR.Questions.FirstOrDefault(rec => rec.QuestionBankId == 1000);
                    QuestionWith1000 = Question;
                    var CurrentYear = DateTime.Now.Year;
                    foreach (var QColumnDetails in Question.QuestionColumnDetails.OrderBy(rec => rec.ColumnNo).ToList())
                    {
                        PrequalificationUserInput PUserInput = entityDB.PrequalificationUserInput.FirstOrDefault(rec => rec.PreQualificationId == p.PrequalificationId && rec.QuestionColumnId == QColumnDetails.QuestionColumnId);
                        if (PUserInput != null)
                        {
                            PUserInput.UserInput = --CurrentYear + "";
                            entityDB.Entry(PUserInput).State = EntityState.Modified;
                        }
                        else
                        {
                            PUserInput = new PrequalificationUserInput();
                            PUserInput.PreQualificationId = p.PrequalificationId;
                            PUserInput.QuestionColumnId = QColumnDetails.QuestionColumnId;
                            PUserInput.UserInput = --CurrentYear + "";
                            entityDB.PrequalificationUserInput.Add(PUserInput);
                        }


                    }
                }
            }

            entityDB.SaveChanges();
            if (QuestionWith1000.QuestionColumnDetails != null)
                foreach (var QcolumnDetails in QuestionWith1000.QuestionColumnDetails.ToList())
                {
                    var QCValue = entityDB.PrequalificationUserInput.FirstOrDefault(rec => rec.PreQualificationId == p.PrequalificationId && rec.QuestionColumnId == QcolumnDetails.QuestionColumnId).UserInput;
                    if (QCValue != null)
                    {
                        var EMRYear = entityDB.PrequalificationEMRStatsYears.FirstOrDefault(rec => rec.PrequalificationId == oldPrequalification.PrequalificationId && rec.EMRStatsYear == QCValue);
                        if (EMRYear != null)
                        {

                            foreach (var EMRValues in EMRYear.PrequalificationEMRStatsValues.ToList())
                            {
                                var QuestionColumnDetails = EMRValues.Questions.QuestionColumnDetails.FirstOrDefault(rec => rec.ColumnNo == QcolumnDetails.ColumnNo);
                                PrequalificationUserInput PUserInput = entityDB.PrequalificationUserInput.FirstOrDefault(rec => rec.PreQualificationId == p.PrequalificationId && rec.QuestionColumnId == QuestionColumnDetails.QuestionColumnId);
                                if (PUserInput != null)
                                {
                                    PUserInput.UserInput = EMRValues.QuestionColumnIdValue + "";
                                    entityDB.Entry(PUserInput).State = EntityState.Modified;
                                }
                                else
                                {
                                    PUserInput = new PrequalificationUserInput();
                                    PUserInput.PreQualificationId = p.PrequalificationId;
                                    PUserInput.QuestionColumnId = QuestionColumnDetails.QuestionColumnId;
                                    PUserInput.UserInput = EMRValues.QuestionColumnIdValue + "";
                                    entityDB.PrequalificationUserInput.Add(PUserInput);
                                }
                            }
                        }
                    }
                }
            entityDB.SaveChanges();
            var copyanalytics = _prequalification.CopyAnalytics(oldPreQualificationId, entityDB);
            if (copyanalytics != null)
            {
                foreach (var items in copyanalytics)
                {
                    entityDB.FinancialAnalyticsLog.Add(items);

                }
                entityDB.SaveChanges();

            }
            return p.PrequalificationId;
        }

        [SessionExpire(lastPageUrl = "../SystemUsers/Dashboard")]
        public ActionResult StartTemplate(Guid ClientTemplateId, long PrequalificationId, bool? copyAnswer, long? client, string selectedDocs, string comments, bool isAjax = false)
        {

            var CTemplate = entityDB.ClientTemplates.Find(ClientTemplateId);
            Prequalification preQualificationDetails = entityDB.Prequalification.Find(PrequalificationId);
            preQualificationDetails.ClientTemplateId = ClientTemplateId;
            preQualificationDetails.locked = CTemplate.Templates.LockedByDefault;
            preQualificationDetails.Comments = comments;
            //Mani on 8/19/2015
            decimal? clientTemplateBUGroupPrice = 0.0M;
            try
            {
                clientTemplateBUGroupPrice = entityDB.ClientTemplatesBUGroupPrice.FirstOrDefault(rec => rec.ClientTemplateID == ClientTemplateId && rec.GroupPriceType == 0 && rec.GroupingFor == 0).GroupPrice;
            }
            catch { }
            // Ends<<<
            preQualificationDetails.TotalInvoiceAmount = clientTemplateBUGroupPrice; // Kiran on 02/24/2015



            if (preQualificationDetails.PrequalificationStatusId != 3)
            {
                //Rajesh DT:-8/7/2013>>>>
                preQualificationDetails.PrequalificationStart = DateTime.Now;
                DateTime FinishDateTime = new DateTime();
                var preStatus = entityDB.PrequalificationStatus.FirstOrDefault(rec => rec.PrequalificationStatusId == 3);
                if (preStatus != null)
                {
                    if (preStatus.PeriodLengthUnit.ToLower().Equals("d"))
                        FinishDateTime = DateTime.Now.AddDays(preStatus.PeriodLength);
                    else if (preStatus.PeriodLengthUnit.ToLower().Equals("m"))
                        FinishDateTime = DateTime.Now.AddMonths(preStatus.PeriodLength);
                    else if (preStatus.PeriodLengthUnit.ToLower().Equals("y"))
                        FinishDateTime = DateTime.Now.AddYears(preStatus.PeriodLength);
                }
                preQualificationDetails.PrequalificationFinish = FinishDateTime;
                //<<<<<

                //To add Prequalification Logs
                PrequalificationStatusChangeLog preQualificationLog = new PrequalificationStatusChangeLog();
                preQualificationLog.PrequalificationId = preQualificationDetails.PrequalificationId;
                preQualificationLog.PrequalificationStatusId = 3;
                preQualificationLog.StatusChangeDate = DateTime.Now;
                preQualificationLog.StatusChangedByUser = new Guid(Session["UserId"].ToString());
                preQualificationLog.PrequalificationStart = preQualificationDetails.PrequalificationStart;
                preQualificationLog.PrequalificationFinish = preQualificationDetails.PrequalificationFinish;
                preQualificationLog.CreatedDate = DateTime.Now;
                entityDB.PrequalificationStatusChangeLog.Add(preQualificationLog);
                //>>>Ends


                foreach (var TSections in CTemplate.Templates.TemplateSections.ToList())
                {
                    var TSubSection = TSections.TemplateSubSections.FirstOrDefault(rec => rec.SubSectionTypeCondition == "YearWiseStats");
                    if (TSubSection != null)
                    {
                        var YearQuestion = TSubSection.Questions.FirstOrDefault(rec => rec.QuestionBankId == 1000);
                        if (YearQuestion != null)
                        {
                            var CurrentYear = DateTime.Now.Year;
                            foreach (var QColumnDetails in YearQuestion.QuestionColumnDetails.OrderBy(rec => rec.ColumnNo).ToList())
                            {
                                PrequalificationUserInput PUserInput = new PrequalificationUserInput();
                                PUserInput.PreQualificationId = preQualificationDetails.PrequalificationId;
                                PUserInput.QuestionColumnId = QColumnDetails.QuestionColumnId;
                                PUserInput.UserInput = --CurrentYear + "";
                                entityDB.PrequalificationUserInput.Add(PUserInput);
                            }
                        }
                    }
                }

            }
            preQualificationDetails.PrequalificationStatusId = 3;

            entityDB.Entry(preQualificationDetails).State = EntityState.Modified;



            if (copyAnswer != null && copyAnswer == true)
            {
                var clientId = Convert.ToInt64(client);
                var selectedPreQualificationId = entityDB.Prequalification.FirstOrDefault(rec => rec.ClientId == clientId && rec.VendorId == preQualificationDetails.VendorId).PrequalificationId;

                //var templateSupportedBiddings = (from reporting in CTemplate.ClientTemplateReportingData.Where(rec => rec.ReportingType == 0)
                //                                 select reporting.ReportingTypeId).ToList();
                //var templateSupportedProficiency = (from reporting in CTemplate.ClientTemplateReportingData.Where(rec => rec.ReportingType == 1)
                //                                    select reporting.ReportingTypeId).ToList();
                var templateSupportedBiddings = CTemplate.ClientTemplateReportingData.Where(rec => rec.ReportingType == 0).Select(rec => rec.ReportingTypeId);

                var templateSupportedProficiency = CTemplate.ClientTemplateReportingData.Where(rec => rec.ReportingType == 1).Select(rec => rec.ReportingTypeId).ToList();

                //Copy prequalification user input
                foreach (var UserInput in entityDB.PrequalificationUserInput.Where(rec => rec.PreQualificationId == selectedPreQualificationId).ToList())
                {//rec.TemplateSectionType != 34 && !rec.SectionName.EndsWith("approval",StringComparison.CurrentCultureIgnoreCase
                    var templateSection = UserInput.QuestionColumnDetails.Questions.TemplateSubSections.TemplateSections;
                    if (templateSection.TemplateSectionType == 34 || templateSection.TemplateSectionsPermission.Any(r => r.VisibleTo == 2) ||
                        templateSection.SectionName.EndsWith("approval", StringComparison.CurrentCultureIgnoreCase))
                        continue;
                    if (templateSection.TemplateSectionType == 6 || templateSection.TemplateSectionType == 8)//Bidding
                    {
                        if (UserInput.QuestionColumnDetails.Questions.QuestionsBank.QuestionReference == 0 && templateSupportedBiddings.Contains((long)UserInput.QuestionColumnDetails.Questions.QuestionsBank.QuestionReferenceId))
                        {
                            PrequalificationUserInput inputValue = new PrequalificationUserInput();
                            inputValue.PreQualificationId = preQualificationDetails.PrequalificationId;
                            inputValue.QuestionColumnId = UserInput.QuestionColumnId;
                            inputValue.UserInput = UserInput.UserInput;
                            entityDB.PrequalificationUserInput.Add(inputValue);


                            //Adding Reportin data
                            var newReport = new PrequalificationReportingData();
                            newReport.PrequalificationId = preQualificationDetails.PrequalificationId;
                            newReport.ClientId = clientId;
                            newReport.VendorId = preQualificationDetails.VendorId;
                            newReport.ReportingType = 0;
                            newReport.ReportingDataId = UserInput.QuestionColumnDetails.Questions.QuestionsBank.QuestionReferenceId;
                            entityDB.PrequalificationReportingData.Add(newReport);
                        }
                        else
                            continue;
                    }
                    else if (templateSection.TemplateSectionType == 5)//Proficiency
                    {
                        if (UserInput.QuestionColumnDetails.Questions.QuestionsBank.QuestionReference == 1 && templateSupportedProficiency.Contains((long)UserInput.QuestionColumnDetails.Questions.QuestionsBank.QuestionReferenceId))
                        {
                            PrequalificationUserInput inputValue = new PrequalificationUserInput();
                            inputValue.PreQualificationId = preQualificationDetails.PrequalificationId;
                            inputValue.QuestionColumnId = UserInput.QuestionColumnId;
                            inputValue.UserInput = UserInput.UserInput;
                            entityDB.PrequalificationUserInput.Add(inputValue);


                            //Adding Reportin data
                            var newReport = new PrequalificationReportingData();
                            newReport.PrequalificationId = preQualificationDetails.PrequalificationId;
                            newReport.ClientId = clientId;
                            newReport.VendorId = preQualificationDetails.VendorId;
                            newReport.ReportingType = 1;
                            newReport.ReportingDataId = UserInput.QuestionColumnDetails.Questions.QuestionsBank.QuestionReferenceId;
                            entityDB.PrequalificationReportingData.Add(newReport);
                        }
                        else
                            continue;

                    }
                    else
                    {
                        PrequalificationUserInput inputVal = new PrequalificationUserInput();
                        inputVal.PreQualificationId = preQualificationDetails.PrequalificationId;
                        inputVal.QuestionColumnId = UserInput.QuestionColumnId;
                        inputVal.UserInput = UserInput.UserInput;
                        entityDB.PrequalificationUserInput.Add(inputVal);
                    }
                }

                //Adding Documents
                if (selectedDocs != null)
                {
                    foreach (var doc in selectedDocs.Split(','))
                    {
                        try
                        {
                            Guid id = new Guid();
                            var docId = new Guid(doc.Replace("Doc_", ""));
                            var sourceDoc = entityDB.Document.Find(docId);
                            var destinationDoc = new Document();
                            destinationDoc.DocumentTypeId = sourceDoc.DocumentTypeId;
                            destinationDoc.OrganizationId = sourceDoc.OrganizationId;
                            destinationDoc.SystemUserAsUploader = sourceDoc.SystemUserAsUploader;
                            destinationDoc.ReferenceType = sourceDoc.ReferenceType;
                            destinationDoc.ReferenceRecordID = preQualificationDetails.PrequalificationId;//This Only Changed
                            destinationDoc.DocumentName = sourceDoc.DocumentName;
                            destinationDoc.Uploaded = sourceDoc.Uploaded;
                            destinationDoc.Expires = sourceDoc.Expires;
                            destinationDoc.DocumentId = id;
                            destinationDoc.ClientId = sourceDoc.ClientId;
                            entityDB.Document.Add(destinationDoc);


                            var dotIndex = sourceDoc.DocumentName.LastIndexOf('.');
                            var extension = sourceDoc.DocumentName.Substring(dotIndex);
                            var path = Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["ClientUploadsPath"]), sourceDoc.DocumentId + extension);
                            var dest = Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["ClientUploadsPath"]), id + extension);

                            System.IO.File.Copy(path, dest, true);
                        }
                        catch { }
                    }

                }

                //Adding Sites

                //foreach (var site in entityDB.PrequalificationSites.Where(rec => rec.PrequalificationId == selectedPreQualificationId).ToList())
                //{
                //    var newSite = new PrequalificationSites();
                //    newSite.PrequalificationId = preQualificationDetails.PrequalificationId;
                //    newSite.ClientId = clientId;
                //    newSite.VendorId = preQualificationDetails.VendorId;
                //    newSite.ClientBULocation = site.ClientBULocation;
                //    newSite.ClientBusinessUnitId = site.ClientBusinessUnitId;
                //    newSite.ClientBusinessUnitSiteId = site.ClientBusinessUnitSiteId;
                //    newSite.Comment = site.Comment;
                //    entityDB.PrequalificationSites.Add(newSite);
                //}



            }
            entityDB.SaveChanges();

            _prequalification.ZeroPayments(PrequalificationId);
            //var predetails = entityDB.Prequalification.Find(PrequalificationId);
            //var TemplateID = entityDB.ClientTemplates.FirstOrDefault(r => r.ClientTemplateID == ClientTemplateId && r.ClientID == predetails.ClientId).TemplateID;
            //var templatesection = entityDB.TemplateSections.FirstOrDefault(r => r.TemplateID == TemplateID && r.TemplateSectionType == 3 && r.Visible == false);

            //Check to see if this  is locked. If so display message and redirect to dashboard.
            if (preQualificationDetails.locked != null && preQualificationDetails.locked == true)
            {
                SendUnlockRequestNotification(preQualificationDetails.PrequalificationId, preQualificationDetails);
                if (isAjax)
                    return Json("../SystemUsers/Dashboard");
                return Redirect("../SystemUsers/Dashboard");
            }
            if (isAjax)
                return Json("../Prequalification/TemplateSectionsList?templateId=" + preQualificationDetails.ClientTemplates.TemplateID + "&TemplateSectionID=-1&PreQualificationId=" + PrequalificationId);
            return Redirect("../Prequalification/TemplateSectionsList?templateId=" + preQualificationDetails.ClientTemplates.TemplateID + "&TemplateSectionID=-1&PreQualificationId=" + PrequalificationId);
        }



        private void SendUnlockRequestNotification(long PreQualificationId, Prequalification currentPreQualification)
        {
            //To Send Notifications for particular sections
            //Rajesh on 2/17/2014

            //if (Session["RoleName"].ToString().Equals("Vendor") || Session["RoleName"].ToString().Equals("Client"))
            {
                string adminEmails = currentPreQualification.ClientTemplates.Templates.unlockNotificationAdminEmails.ToStringNullSafe();

                if (!String.IsNullOrWhiteSpace(adminEmails))
                {
                    var template = entityDB.EmailTemplates.Find(currentPreQualification.ClientTemplates.Templates.EmaiTemplateId);
                    if (template == null) return;


                    var subject = template.EmailSubject;
                    var body = template.EmailBody;
                    var Comment = template.CommentText;
                    var clientEmail = "";
                    var strMessage = "";
                    var emailsToSent = ""; // Kiran on 9/23/2014
                    var clientSysUserOrgs = currentPreQualification.Client.SystemUsersOrganizations;
                    foreach (var clientUserOrg in clientSysUserOrgs)
                    {
                        var clientOrgRoles = clientUserOrg.SystemUsersOrganizationsRoles;
                        foreach (var clientOrgRole in clientOrgRoles)
                        {
                            if (clientOrgRole.SystemRoles.RoleName.Equals("Super User"))
                            {
                                clientEmail = clientUserOrg.SystemUsers.Email;
                            }
                        }
                    }
                    subject = subject.Replace("[Date]", DateTime.Now + "");
                    subject = subject.Replace("[VendorName]", currentPreQualification.Vendor.Name);
                    subject = subject.Replace("[ClientName]", currentPreQualification.Client.Name);
                    subject = subject.Replace("[CustomerServicePhone]", currentPreQualification.Client.PhoneNumber);
                    subject = subject.Replace("[CustomerServiceEmail]", clientEmail);

                    body = body.Replace("[Date]", DateTime.Now + "");
                    body = body.Replace("[VendorName]", currentPreQualification.Vendor.Name);
                    body = body.Replace("[ClientName]", currentPreQualification.Client.Name);
                    body = body.Replace("[CustomerServicePhone]", currentPreQualification.Client.PhoneNumber);
                    body = body.Replace("[CustomerServiceEmail]", clientEmail);

                    strMessage = SendMail.sendMail(adminEmails, body, subject);


                }
            }
            //Ends<<<
        }
        public ActionResult GetERIClients(long PQId)
        {
            if (string.IsNullOrEmpty(Request.Params["filter[filters][0][value]"]))
            {
                return Json(new List<KeyValuePair<string, string>>(), JsonRequestBehavior.AllowGet);
            }
            var lockedClients = _clientBusiness.GetPQClientsInSites(PQId);
            var lockedClientIds = lockedClients.Select(r => r.Key);
            var pqClient = _prequalification.GetPrequalificationClient(PQId) ?? new PrequalificationClient();
            var clients = _clientBusiness.GetERIClientsByRegion(PQId, pqClient.Region) ?? new List<KeyValuePair<string, string>>();
            return Json(clients.Where(r => r.Value.ToLower().Contains(Request.Params["filter[filters][0][value]"].ToLower()) && !lockedClientIds.Contains(long.Parse(r.Key))).ToList(), JsonRequestBehavior.AllowGet);
        }
        public ActionResult AddERIPQClients(long PQId)
        {

            //var res = _prequalification.GetTemplateOrganizations(PQId);
            //var pq = _prequalification.GetPrequalification(PQId);
            //var dData = new DropdDownDataSupplier();
            var locations = _clientBusiness.GetERIsRegion(PQId) ?? new List<KeyValuePair<string, string>>();
            var pqClient = _prequalification.GetPrequalificationClient(PQId) ?? new PrequalificationClient();
            var lockedClients = _clientBusiness.GetPQClientsInSites(PQId);
            var lockedClientIds = lockedClients.Select(r => r.Key);
            var clients = _clientBusiness.GetERIClientsByRegion(PQId, pqClient.Region) ?? new List<KeyValuePair<string, string>>();
            ViewBag.clients = clients.Where(r => !lockedClientIds.Contains(long.Parse(r.Key))).ToList();
            ViewBag.lockedClients = lockedClients;
            ViewBag.locations = locations;
            ViewBag.pqId = PQId;
            ViewBag.pqClient = pqClient;
            ViewBag.HideClients = _prequalification.GetPQTemplate(PQId).HideClients;
            return PartialView();
        }
        public ActionResult GetClientLocations(long pqId, long SubsectionId)
        {
            var currentPQ = entityDB.Prequalification.Find(pqId);
            var clientId = 0L;
            var clientUserAssociatedBUSites = new List<long>();
            var userBUSites = "";
            var clientLocations = _clientBusiness.GetPQClientLocations(pqId, clientId);
            if (SSession.Role.Equals(OrganizationType.Client))
            {
                clientId = SSession.OrganizationId;
                Guid userId = SSession.UserId;
                var buSites = entityDB.SystemUsersBUSites.FirstOrDefault(rec => rec.SystemUserId == userId);
                if (buSites != null)
                    userBUSites = buSites.BUSiteId;
                if (!string.IsNullOrEmpty(userBUSites.Trim()))
                    clientUserAssociatedBUSites = userBUSites.Split(',').Select(long.Parse).ToList();
                if (clientUserAssociatedBUSites.Any())
                    clientLocations = clientLocations.Where(rec => clientUserAssociatedBUSites.Contains((long)rec.BUSiteId)).ToList();
            }
            ViewBag.pqClientLocations = clientLocations;
            ViewBag.SubsectionId = SubsectionId;
            ViewBag.PQId = pqId;
            ViewBag.PQStatus = currentPQ.PrequalificationStatusId;
            return PartialView();
        }
        public ActionResult GetClientRegions(long pqId, long sectionId)
        {
            var clientId = 0L;
            if (Session["RoleName"].ToString().Equals("Client"))
                clientId = Convert.ToInt64(Session["currentOrgId"]);

            var clientLocations = _clientBusiness.GetPQClientLocations(pqId, clientId);
            ViewBag.pqClientLocations = clientLocations;
            ViewBag.sectionId = sectionId;
            ViewBag.PQId = pqId;
            return PartialView();
        }
        public ActionResult GetClientsByRegion(string region, long pqId)
        {
            var res = _clientBusiness.GetERIClientsByRegion(pqId, region) ?? new List<KeyValuePair<string, string>>();
            var lockedClients = _clientBusiness.GetPQClientsInSites(pqId);
            var lockedClientIds = lockedClients.Select(r => r.Key);
            return Json(res.Where(r => !lockedClientIds.Contains(long.Parse(r.Key))));
        }
        [HttpGet]
        public ActionResult AssignLocationStatus(long SubsectionId, long pqSiteId, long pqSiteClientId, long pqId)
        {
            var acceptedStatuses = new List<string>() { "8", "13", "26" };
            ViewBag.pqStatuses = _clientBusiness.GetPQStatuses().Where(r => acceptedStatuses.Contains(r.Key)).Select(r => new SelectListItem() { Text = r.Value, Value = r.Key });
            AssignLocationStatus assignStatus = new AssignLocationStatus();
            assignStatus.PrequalificationSiteId = pqSiteId;
            assignStatus.SubsectionId = SubsectionId;
            UserInfo clientSuperUser = _organization.GetSuperuser(pqSiteClientId);
            assignStatus.SendTo = "info@firstverify.com";
            assignStatus.SendCC = clientSuperUser.Email;
            if (entityDB.TemplateSubSectionsNotifications.FirstOrDefault(rec => rec.TemplateSubSectionID == SubsectionId) != null)
            {
                var emailTemplateId = entityDB.TemplateSubSectionsNotifications.FirstOrDefault(rec => rec.TemplateSubSectionID == SubsectionId).EmaiTemplateId;
                var emailTemplate = entityDB.EmailTemplates.FirstOrDefault(rec => rec.EmailTemplateID == emailTemplateId);
                assignStatus.Subject = emailTemplate.EmailSubject;
                assignStatus.Body = emailTemplate.EmailBody;

                var prequalification = entityDB.Prequalification.Find(pqId);
                var clientInfo = _organization.GetOrganization(pqSiteClientId);
                var clientOrgSuperUser = _sysUser.GetSuperUserInfo(pqSiteClientId);

                assignStatus.Subject = assignStatus.Subject.Replace("[Date]", DateTime.Now + "");
                assignStatus.Subject = assignStatus.Subject.Replace("[VendorName]", prequalification.Vendor.Name);
                assignStatus.Subject = assignStatus.Subject.Replace("[ClientName]", clientInfo.Name);
                assignStatus.Subject = assignStatus.Subject.Replace("[CustomerServicePhone]", clientInfo.PhoneNumber);
                assignStatus.Subject = assignStatus.Subject.Replace("[CustomerServiceEmail]", clientSuperUser.Email);
                assignStatus.Subject = assignStatus.Subject.Replace("[ClientUserName]", clientOrgSuperUser.LastName + ", " + clientOrgSuperUser.FirstName);


                assignStatus.Body = assignStatus.Body.Replace("[Date]", DateTime.Now + "");
                assignStatus.Body = assignStatus.Body.Replace("[VendorName]", prequalification.Vendor.Name);
                assignStatus.Body = assignStatus.Body.Replace("[ClientName]", clientInfo.Name);
                assignStatus.Body = assignStatus.Body.Replace("[CustomerServicePhone]", clientInfo.PhoneNumber);
                assignStatus.Body = assignStatus.Body.Replace("[CustomerServiceEmail]", clientSuperUser.Email);
                assignStatus.Body = assignStatus.Body.Replace("[ClientUserName]", clientOrgSuperUser.LastName + ", " + clientOrgSuperUser.FirstName);
            }

            return View(assignStatus);
        }
        [HttpPost]
        public ActionResult AssignLocationStatus(AssignLocationStatus assignStatus)
        {
            bool updated = _clientBusiness.UpdateLocationStatus(assignStatus.PrequalificationSiteId, assignStatus.PrequalificationStatusId);
            var mailStatus = SendNotification(assignStatus);
            assignStatus.StatuschangedBy = SSession.UserId;
            if (mailStatus.Equals("Success"))
            {
                _clientBusiness.LogSiteStatusChange(assignStatus);
            }
            return Redirect("PopUpCloseView?subSectionId=" + assignStatus.SubsectionId);
        }

        private string SendNotification(AssignLocationStatus assignStatus)
        {
            string ccMail = "";
            string toAddress = "";

            if (!string.IsNullOrEmpty(assignStatus.SendTo))
            {
                if (!assignStatus.SendTo.Trim().Equals("")) //observe the !
                {
                    toAddress = assignStatus.SendTo;
                    string toAddressEmails = "";
                    foreach (var email in toAddress.Split(','))
                    {
                        if (email == "")
                        {
                            toAddressEmails = toAddressEmails + email.Replace(",", "");
                        }
                        else
                        {
                            toAddressEmails = toAddressEmails + email.Replace(",", "") + ",";
                        }
                    }
                    toAddress = toAddressEmails.Substring(0, toAddressEmails.Length - 1);
                }
            }

            if (!string.IsNullOrEmpty(assignStatus.SendCC))
            {
                if (!assignStatus.SendCC.Trim().Equals("")) //observe the !
                {
                    ccMail = assignStatus.SendCC;
                    string ccEmails = "";
                    foreach (var email in ccMail.Split(','))
                    {
                        if (email == "")
                        {
                            ccEmails = ccEmails + email.Replace(",", "");
                        }
                        else
                        {
                            ccEmails = ccEmails + email.Replace(",", "") + ",";
                        }
                    }
                    ccMail = ccEmails.Substring(0, ccEmails.Length - 1);
                }
            }
            var bodyString = HttpUtility.HtmlDecode(assignStatus.Body);
            var formattedBody = (bodyString ?? "").Replace("\r", "");

            string emailBody = System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Views/Prequalification/PrequalEmailFormat.htm"));
            emailBody = emailBody.Replace("ReplaceString", formattedBody);

            return SendMail.sendMail(toAddress, emailBody, assignStatus.Subject, "", ccMail);

        }
        public ActionResult GetQuestionColor(long PqId, List<long> QuestionId)
        {
            return Json(_prequalification.GetQuestionColor(PqId, QuestionId));
        }
        public ActionResult GetParentColor(long PqId, List<long> QuestionId)
        {
            return Json(_prequalification.GetParentColor(PqId, QuestionId));
        }
        //Sub Section Id is for present selected Id
        [SessionExpire]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "PREQUALIFICATIONREQUIREMENTS", sideMenuName = "", ViewName = "TemplateSectionsList")]
        public ActionResult TemplateSectionsList(Guid templateId, long TemplateSectionID, long PreQualificationId, int? mode)//null or 0 :-edit mode 1-View Mode
        {
            long templateLanguageId = (long)_prequalification.GetTemplateLanguage(templateId);
            var languageCode = _organization.GetLanguageCode(templateLanguageId);
            _prequalification.SetCulture(languageCode);
            var RestrictedCommentsSec = _sysUser.GetPQcomments(PreQualificationId);
            ViewBag.preComments = RestrictedCommentsSec?.Comments;
            ViewBag.PqCommentsHasFlag = RestrictedCommentsSec?.IsChecked;
            var Templates = _prequalification.GetPQTemplate(PreQualificationId);
            ViewBag.HasCommentsSecPermisstion = Templates.HideCommentsSec;
            ViewBag.Showlocations = Templates.HasVendorDetailLocation;
            ViewBag.HaspredefineCommentsEditPermission = _sysUser.IsUserHasPermission(SSession.UserId, 1012);
            VendorDetailsViewModel pqDetails = _prequalification.GetPQCommonData(PreQualificationId);
            var userId = SSession.UserId;

            if (pqDetails.Locked == true && SSession.Role.ToString() == LocalConstants.Vendor)
                return Redirect("../SystemUsers/DashBoard");

            //If any other user try to enter any other url we check that user belogs to this vendor or not

            long loginUserOrgId = SSession.OrganizationId;

            if (pqDetails.VendorId != loginUserOrgId && SSession.Role.ToString() == LocalConstants.Vendor)
            {
                return Redirect("../SystemUsers/DashBoard");
            }
            if ((pqDetails.ClientId != loginUserOrgId && pqDetails.ClientOrganizationType != OrganizationType.SuperClient) && SSession.Role.ToString() == LocalConstants.Client)
            {
                return Redirect("../SystemUsers/DashBoard");
            }
            TemplateSectionViewModel selectedTemplateSection = new TemplateSectionViewModel();


            Templates selectedTemplate = _prequalification.GetPQTemplate(PreQualificationId);
            ViewBag.HasCommentsSecPermisstion = selectedTemplate.HideCommentsSec;
            TemplateSectionID = GetTemplateSectionId(PreQualificationId, TemplateSectionID);

            selectedTemplateSection.InterimSubSectionExists = false;
            if (SSession.Role.Equals(OrganizationType.Client) && TemplateSectionID != -1)
            {
                selectedTemplateSection.InterimSubSectionExists = _prequalification.checkSectionHasInterimSubSections(TemplateSectionID);
            }
            if (TemplateSectionID != -1)
            {
                selectedTemplateSection =
                    _prequalification.GetTemplateSection(PreQualificationId, TemplateSectionID, SSession.Role, SSession.UserId);
            }
            UserInfo vendorSuperUserInfo = _organization.GetSuperuser(pqDetails.VendorId);

            try
            {
                selectedTemplateSection.VendorSuperUserId = vendorSuperUserInfo.UserId;
            }
            catch { }
            try
            {
                selectedTemplateSection.VendorSuperUserContactId = vendorSuperUserInfo.ContactId;
            }
            catch { }
            selectedTemplateSection.RestrictedSectionEdit = RolesHelper.IsUserInRole(userId, 1001, "read", pqDetails.ClientId, pqDetails.VendorId);

            if (TemplateSectionID == -1)
            {
                PqPaymentCommonViewModel pqPaymentCommonData =
                    _prequalification.GetPqPaymentCommonData(PreQualificationId, pqDetails.VendorId,
                        pqDetails.ClientTemplateID);
                selectedTemplateSection.AnnualFeeBus = pqPaymentCommonData.AnnualFeeBus;
                selectedTemplateSection.PqSitesBuUnits = pqPaymentCommonData.PqSitesBuUnits;
                selectedTemplateSection.TemplatePrice = _prequalification.GetPqPriceToPay(pqDetails.VendorId,
                    pqDetails.ClientTemplateID, PreQualificationId);
                selectedTemplateSection.TotalAdditionalFee = pqPaymentCommonData.TotalAdditionalFee;
                selectedTemplateSection.TotalAnnualFee = pqPaymentCommonData.TotalAnnualFee;
            }

            if (SSession.Role.ToString().Equals(OrganizationType.Vendor))
            {
                if (pqDetails.PaymentReceived == null || pqDetails.PaymentReceived == false)
                {
                    var templateSecType = _prequalification.GetTemplateSectionType(TemplateSectionID);

                    if (templateSecType == 0 || templateSecType == 3 || templateSecType == 40 || templateSecType == 31 || templateSecType == 32 || templateSecType == 7)//if user try to open isprofile or payment
                    {
                        //Do nothing
                    }
                    else if (pqDetails.TemplateHasPaymentSection)
                    {
                        TemplateSectionID = entityDB.Templates.Find(templateId).TemplateSections.FirstOrDefault(rec => rec.TemplateSectionType == 3).TemplateSectionID;
                        ViewBag.PaymentError = true;
                    }
                }
                if (pqDetails.PrequalificationStatusId >= 5 && pqDetails.PrequalificationStatusId != 7 && pqDetails.PrequalificationStatusId != 15)//means he submited prequalification//Rajesh on 3/24/2014
                {
                    mode = 1;//He can view sections
                }
                if (pqDetails.PrequalificationStatusId == 7)
                {
                    bool sectionCompleted = _prequalification.PqHasCompletedSection(PreQualificationId, TemplateSectionID);
                    if (sectionCompleted)
                        mode = 1;
                }

                if (pqDetails.PrequalificationStatusId == 4)
                    mode = 1;

                int? sectionType = _prequalification.GetTemplateSectionType(TemplateSectionID);
                if (SSession.Role.ToString().Equals(OrganizationType.Vendor) && sectionType == SectionTypes.ExceptionProbation)
                {
                    bool sectionCompleted = _prequalification.PqHasCompletedSection(PreQualificationId, TemplateSectionID);
                    if (!sectionCompleted)
                    {
                        mode = 0; // Vendor can edit Exception/Probation Section forever.    
                    }
                }
                // Ends<<<
            }
            else if (SSession.Role.ToString().Equals(OrganizationType.Client) || SSession.Role.Equals(OrganizationType.SuperClient))
            {
                mode = 1;//He can view sections

                var permissiontype = selectedTemplateSection.VisibleTo;
                var userhasRestrictedSectionAccess = FVGen3.BusinessLogic.RolesHelper.IsUserInRole(userId, 10001, "Restricted Section Access", pqDetails.ClientId, pqDetails.VendorId);
                if (permissiontype == 4 && userhasRestrictedSectionAccess)
                {
                    mode = 0;
                }
                //Check against TemplateSectionID to find Viewtype if 4 and user has permission make mode = 0

            }

            //To Get list of PreQualification with respet to present prequalification client and vendor

            selectedTemplateSection.Mode = mode;
            selectedTemplateSection.TemplateId = templateId;
            ViewBag.templateId = templateId;
            ViewBag.PreQualificationId = PreQualificationId;
            ViewBag.VendorId = pqDetails.VendorId;
            ViewBag.mode = mode;
            ViewBag.eritemplate = selectedTemplateSection.IsEri;
            var invitationDetails = _prequalification.GetPQInviteDetails(PreQualificationId);

            ViewBag.InvitationSentBy = GetInvitationSentBy(invitationDetails);
            ViewBag.InvitationSentByUser = invitationDetails.InvitationSentByUser;

            var invitationFromOptions = _prequalification.GetInvitationFromOptions(pqDetails.ClientId);
            List<SelectListItem> selectLists = new List<SelectListItem>();
            invitationFromOptions.ForEach(s => selectLists.Add(new SelectListItem() { Text = s.Key, Value = s.Value }));

            ViewBag.InvitationFromOptions = selectLists;
            //Ends<<<


            ViewBag.TemplateCode = selectedTemplate.TemplateCode;
            selectedTemplateSection.VendorId = pqDetails.VendorId;
            selectedTemplateSection.ClientId = pqDetails.ClientId;
            selectedTemplateSection.ClientName = pqDetails.ClientName;
            selectedTemplateSection.VendorName = pqDetails.VendorName;
            ViewBag.VendorName = pqDetails.VendorName;
            ViewBag.ClientName = pqDetails.ClientName;
            if (vendorSuperUserInfo != null)
            {
                selectedTemplateSection.VendorSuperUserContactName = vendorSuperUserInfo.FirstName + " " +
                                         vendorSuperUserInfo.LastName;
            }

            OrganizationData vendorInfo = _organization.GetOrganization(pqDetails.VendorId);
            selectedTemplateSection.VendorAddress1 = vendorInfo.Address1;
            selectedTemplateSection.VendorAddress2 = vendorInfo.Address2;
            selectedTemplateSection.VendorCity = vendorInfo.City;
            selectedTemplateSection.VendorState = vendorInfo.State;
            selectedTemplateSection.VendorZip = vendorInfo.Zip;

            ViewBag.Period = pqDetails.PrequalificationStart.ToString("MM/dd/yy") + "-" + pqDetails.PrequalificationFinish.ToString("MM/dd/yy");
            ViewBag.LogoPath = "";
            ViewBag.isEdit = pqDetails.PrequalificationStatusId == 10;
            selectedTemplateSection.LogoPath = ConfigurationSettings.AppSettings["LogoUploadPath"].ToString() + "/";

            ViewBag.StatusName = GetPQStatusForBanner(PreQualificationId, pqDetails.PrequalificationStatusName);
            ViewBag.PrequalStartDate = pqDetails.PrequalificationCreate.ToString("MM/dd/yyyy");

            ViewBag.StatusId = pqDetails.PrequalificationStatusId;

            if (@ViewBag.StatusId.ToString() == "10")
            {
                StatusBannerColor previousStatusBannerColor = _prequalification.GetPQPreviousStatusAndBannerColor(PreQualificationId, pqDetails.PrequalificationStatusId);
                ViewBag.VBPreviousStatusColour = previousStatusBannerColor.BannerColor;
                ViewBag.VBPreviousStatusName = previousStatusBannerColor.StatusName;
            }

            string statusName = ViewBag.StatusName;
            bool siteStatus = statusName == "Multiple";
            ViewBag.VBIsSiteHasMultiStatus = siteStatus;

            ViewBag.VBStatusColor = _prequalification.GetPQStatusBannerColor(pqDetails.PrequalificationStatusId, SSession.Role, mode, statusName, LocalConstants.Employees);
            try
            {
                ViewBag.FinalizedUserEmail = selectedTemplateSection.FinalizedUserEmail;
                ViewBag.FinalizedDateTime = selectedTemplateSection.FinalizedDateTime;
                ViewBag.FinalizedUserName = selectedTemplateSection.FinalizedUserName;

            }
            catch (Exception ee) { }

            List<SelectListItem> dropDown_Prequalification = GetVendorPQsSpecificToClient(pqDetails.ClientId, pqDetails.VendorId, PreQualificationId);
            ViewBag.VBPreQualifications = dropDown_Prequalification;

            var sideMenus = GetSideMenusForQuestionnaire(templateId, ref TemplateSectionID, PreQualificationId, mode, selectedTemplate, userId);
            ViewBag.sideMenu = sideMenus;
            ViewBag.TemplateSectionID = TemplateSectionID;

            selectedTemplateSection.IsEri = pqDetails.IsERI;

            getCommand();

            selectedTemplateSection.PqId = PreQualificationId;
            var vendorUsers = _organization.GetOrgUsers(pqDetails.VendorId);
            ViewBag.Vendors = (from vendors in vendorUsers
                               select new SelectListItem
                               {
                                   Text = vendors.Key.ToString(),
                                   Value = vendors.Value + "",
                                   Selected = (pqDetails.UserIdAsCompleter.ToString() == vendors.Value.ToString())
                               }).ToList();

            var showSaveAndContinue = true;
            if (Session["RoleName"].ToString().Equals("Client"))
            {
                var section = entityDB.TemplateSections.Find(TemplateSectionID);
                showSaveAndContinue = false;
                if (section != null && section.TemplateSectionsPermission != null)
                    if ((section.TemplateSectionsPermission.FirstOrDefault().VisibleTo == 2)
                        //|| section.TemplateSectionsPermission.FirstOrDefault().VisibleTo == 4)//If it is client data entry//Rajesh on 3/21/2014
                        ||
                        (section.TemplateSectionsPermission.FirstOrDefault().VisibleTo == 4 &&
                         selectedTemplateSection.RestrictedSectionEdit == true))
                    {
                        showSaveAndContinue = true;
                    }
                    else
                        foreach (var subSec in section.TemplateSubSections.ToList())
                        {
                            //If It is limited access permission then check this user has edit permission
                            var SubSecPermission =
                                subSec.TemplateSubSectionsPermissions.FirstOrDefault(
                                    rec => rec.PermissionFor == "Client");
                            if (SubSecPermission != null && SubSecPermission.PermissionType == 1)
                            {
                                var loginUserId = new Guid(Session["UserId"].ToString());
                                try
                                {
                                    if (
                                        subSec.TemplateSubSectionsPermissions.FirstOrDefault(
                                                rec => rec.PermissionFor == "Client")
                                            .TemplateSubSectionsUserPermissions.FirstOrDefault(
                                                rec => rec.UserId == loginUserId)
                                            .EditAccess)
                                    {
                                        showSaveAndContinue = true;

                                        //break;//If atleast 1 sub section is editable we can send notification
                                    }
                                }
                                catch
                                {
                                }
                            }
                        }
            }
            ViewBag.showSaveAndContinue = showSaveAndContinue;

            var PQSites = entityDB.PrequalificationSites.Where(rec => rec.PrequalificationId == PreQualificationId);
            if (SSession.Role == OrganizationType.Client)
            {
                PQSites = PQSites.Where(r => r.ClientId == SSession.OrganizationId);
            }
            var sitesInfo = PQSites.GroupBy(rec => rec.ClientId).ToList();
            ViewBag.siteInfo = sitesInfo;
            return View(selectedTemplateSection);
        }

        private List<LocalTemplatesSideMenuModel> GetSideMenusForQuestionnaire(Guid templateId, ref long TemplateSectionID, long PreQualificationId,
            int? mode, Templates selectedTemplate, Guid userId)
        {
            return _prequalification.GetPqSideMenusForQuestionnaire(PreQualificationId, mode, templateId, SSession.Role,
                SSession.UserId, ref TemplateSectionID);
        }

        private long GetTemplateSectionId(long pqId, long templateSectionId)
        {
            return _prequalification.GetPqTemplateSectionId(pqId, templateSectionId, SSession.Role);
        }

        public string GetPQStatusForBanner(long pqId, string defaultStatus)
        {
            var clientId = 0L;
            if (SSession.Role.Equals(OrganizationType.Client))
                clientId = SSession.OrganizationId;


            return _prequalification.GetPQStatusForBanner(pqId, defaultStatus, clientId, SSession.UserId).Value;

        }
        public string GetPQStatusVendorIndicationText(long pqId, string defaultStatus)
        {
            var clientId = 0L;
            if (SSession.Role.Equals(OrganizationType.Client))
                clientId = SSession.OrganizationId;


            return _prequalification.GetPQStatusVendorIndicationText(pqId, defaultStatus, clientId, SSession.UserId).Value;

        }
        public long GetLocationSectionId(long pqId)
        {
            var clientId = 0L;
            if (SSession.Role.Equals(OrganizationType.Client))
                clientId = SSession.OrganizationId;


            return _prequalification.GetLocationSectionId(pqId);

        }

        //Rajesh on DT:8-29-2013
        [SessionExpire]
        [HeaderAndSidebar(headerName = "PREQUALIFICATIONREQUIREMENTS", sideMenuName = "", ViewName = "TemplateSectionsList")]//Rajesh on 11/3/2014 Done DB Changes Also
        public ActionResult ChangeStatus(Guid templateId, long PreQualificationId)
        {
            SetupQuestionnaire(templateId, PreQualificationId, "ChangeStatus");
            var RestrictedCommentsSec = _sysUser.GetPQcomments(PreQualificationId);
            ViewBag.preComments = RestrictedCommentsSec?.Comments;
            ViewBag.PqCommentsHasFlag = RestrictedCommentsSec?.IsChecked;
            var Templates = _prequalification.GetPQTemplate(PreQualificationId);
            ViewBag.HasCommentsSecPermisstion = Templates.HideCommentsSec;
            ViewBag.Showlocations = Templates.HasVendorDetailLocation;
            ViewBag.HaspredefineCommentsEditPermission = _sysUser.IsUserHasPermission(SSession.UserId, 1012);
            LocalChangeStatus pqStatusInfo = _prequalification.GetPQStatusInfoForChangeStatus(PreQualificationId, templateId);
            ViewBag.StatusId = pqStatusInfo.pqStatusId;

            pqStatusInfo.NotCompletedSection_Status = new List<bool>();
            for (int i = 0; i < pqStatusInfo.NotCompletedSection.Count; i++)
            {
                pqStatusInfo.NotCompletedSection_Status.Add(false);
            }
            GetPqStatuses();


            var adminUsers = (from systemUsers in pqStatusInfo.adminUsers.ToList()
                              select new SelectListItem
                              {
                                  Text = systemUsers.Key,
                                  Value = systemUsers.Value.ToString(),
                                  Selected = pqStatusInfo.userIdAsSigner.ToString() == systemUsers.Value
                              }).ToList();


            SelectListItem defaultItem = new SelectListItem();
            defaultItem = new SelectListItem();
            defaultItem.Text = "";
            defaultItem.Value = "";
            var adminUsersWithDefaultValue = adminUsers.OrderBy(r => r.Text).ToList();
            adminUsersWithDefaultValue.Insert(0, defaultItem);

            ViewBag.VBAdminUsers = adminUsersWithDefaultValue;
            return View(pqStatusInfo);
        }

        private void GetPqStatuses()
        {
            var PreQualificationStatuses = new List<SelectListItem>();
            SelectListItem item = new SelectListItem();
            item.Value = "-1";
            item.Text = "";
            PreQualificationStatuses.Add(item);

            //Rajesh on 9/25/2014
            //PreQualificationStatuses.AddRange((from a in entityDB.PrequalificationStatus.Where(rec => rec.PrequalificationStatusId > 2).ToList().Where(rec => rec.ShowInDashBoard == true).OrderBy(rec => rec.PrequalificationStatusName) // Kiran on 01/13/2015
            //                                   select new SelectListItem()
            //                                   {
            //                                       Text = a.PrequalificationStatusName,
            //                                       Value = a.PrequalificationStatusId + ""
            //                                   }).ToList());
            PreQualificationStatuses.AddRange((from a in _prequalification.GetPQStatuses().ToList().OrderBy(rec => rec.Value) // Kiran on 01/13/2015
                                               select new SelectListItem()
                                               {
                                                   Text = a.Value,
                                                   Value = a.Key + ""
                                               }).ToList());
            ViewBag.PreQualificationStatuses = PreQualificationStatuses;
        }

        public ActionResult GetReferenceDocs(string referenceType, long refRecId, long? docTypeId, long? clientId, bool? OrderBy)
        {
            List<PqReferenceDocumentsViewModel> referenceDocuments = _prequalification.GetReferenceDocs(referenceType, refRecId, docTypeId, clientId, OrderBy);
            return PartialView(referenceDocuments);
        }
        //Rajesh on 11/3/2014
        private void SetupQuestionnaire(Guid templateId, long PreQualificationId, string ScreenName)
        {
            @ViewBag.mode = 0;
            @ViewBag.templateId = templateId;
            VendorDetailsViewModel pqDetails = _prequalification.GetPQCommonData(PreQualificationId);
            ViewBag.TemplateCode = pqDetails.TemplateCode;
            ViewBag.VendorId = pqDetails.VendorId;
            ViewBag.ClientId = pqDetails.ClientId;
            ViewBag.eritemplate = pqDetails.IsERI;
            if (pqDetails.ClientOrganizationType == OrganizationType.SuperClient)
            {
                ViewBag.OrgType = OrganizationType.SuperClient;
                try { ViewBag.ClientIdsList = _prequalification.GetPrequalificationClient(PreQualificationId).ClientIdsList.ToList(); } catch { }
            }
            ViewBag.ClientName = pqDetails.ClientName;
            ViewBag.VendorName = pqDetails.VendorName;
            ViewBag.Period = pqDetails.PrequalificationStart.ToString("MM/dd/yy") + "-" + pqDetails.PrequalificationFinish.ToString("MM/dd/yy");
            ViewBag.LogoPath = "";
            ViewBag.isEdit = pqDetails.PrequalificationStatusId == 10;
            ViewBag.LogoPath = ConfigurationSettings.AppSettings["LogoUploadPath"].ToString() + "/";//Rajesh on 08/09/2013
                                                                                                    //ViewBag.StatusName = preQualificationDetails.PrequalificationStatus.PrequalificationStatusName; // Kiran on 3/28/2014
            ViewBag.StatusName = GetPQStatusForBanner(PreQualificationId, pqDetails.PrequalificationStatusName);
            ViewBag.PrequalificationStartDate = pqDetails.PrequalificationStart.ToString("MM/dd/yyyy");
            ViewBag.currentDate = DateTime.Now.ToString("MM/dd/yyyy");
            ViewBag.StatusId = pqDetails.PrequalificationStatusId;

            if (@ViewBag.StatusId.ToString() == "10")
            {
                StatusBannerColor previousStatusBannerColor = _prequalification.GetPQPreviousStatusAndBannerColor(PreQualificationId, pqDetails.PrequalificationStatusId);
                ViewBag.VBPreviousStatusColour = previousStatusBannerColor.BannerColor;
                ViewBag.VBPreviousStatusName = previousStatusBannerColor.StatusName;
            }

            string statusName = ViewBag.StatusName;
            bool siteStatus = statusName == "Multiple";
            ViewBag.VBIsSiteHasMultiStatus = siteStatus;
            int? mode = @ViewBag.mode;
            ViewBag.VBStatusColor = _prequalification.GetPQStatusBannerColor(pqDetails.PrequalificationStatusId, SSession.Role, mode, statusName, LocalConstants.Employees);

            var invitationDetails = _prequalification.GetPQInviteDetails(PreQualificationId);

            ViewBag.InvitationSentBy = GetInvitationSentBy(invitationDetails);
            ViewBag.InvitationSentByUser = invitationDetails.InvitationSentByUser;

            var invitationFromOptions = _prequalification.GetInvitationFromOptions(pqDetails.ClientId);
            List<SelectListItem> selectLists = new List<SelectListItem>();
            invitationFromOptions.ForEach(s => selectLists.Add(new SelectListItem() { Text = s.Key, Value = s.Value }));

            ViewBag.InvitationFromOptions = selectLists;

            List<SelectListItem> dropDown_Prequalification = GetVendorPQsSpecificToClient(pqDetails.ClientId, pqDetails.VendorId, PreQualificationId);
            ViewBag.VBPreQualifications = dropDown_Prequalification;

            List<LocalTemplatesSideMenuModel> sideMenus = GetSideMenus(PreQualificationId, mode, templateId, ScreenName);
            ViewBag.sideMenu = sideMenus;

            getCommand();
            //ViewBag.TemplatePrice = getTemplatePrice(pqDetails.VendorId, pqDetails.ClientTemplateID, PreQualificationId);
            ViewBag.PreQualificationId = PreQualificationId;
        }

        private List<LocalTemplatesSideMenuModel> GetSideMenus(long PreQualificationId, int? mode, Guid templateId, string screenName)
        {
            return _prequalification.GetPQSideMenus(PreQualificationId, mode, templateId, SSession.Role, screenName);
        }

        private List<SelectListItem> GetVendorPQsSpecificToClient(long clientId, long vendorId, long pqId)
        {
            List<VendorPQsSpecificToClient> vendorPqsSpecificToClient = _prequalification.GetVendorPqsSpecificToClient(clientId, vendorId);

            var dropDown_Prequalification = new List<SelectListItem>();
            SelectListItem latestPreQualification = new SelectListItem();
            latestPreQualification.Text = vendorPqsSpecificToClient.FirstOrDefault().ClientName + " " + vendorPqsSpecificToClient.FirstOrDefault().PrequalificationStart.ToString("MM/dd/yyyy");
            latestPreQualification.Value = vendorPqsSpecificToClient.FirstOrDefault().PrequalificationId + "";
            dropDown_Prequalification.Add(latestPreQualification);


            if (!SSession.Role.ToString().Equals(LocalConstants.Vendor))
            {
                dropDown_Prequalification = (from preQualifications in vendorPqsSpecificToClient
                                             select new SelectListItem
                                             {
                                                 Text = preQualifications.ClientName + " " + preQualifications.PrequalificationStart.ToString("MM/dd/yyyy"),
                                                 Value = preQualifications.PrequalificationId + "",
                                                 Selected = (pqId == preQualifications.PrequalificationId)
                                             }).ToList();
            }

            if (SSession.Role.ToString().Equals(LocalConstants.Client) || SSession.Role.ToString().Equals(LocalConstants.SuperClient)) // Kiran on 4/30/2014            
            {
                List<VendorPQsSpecificToClient> clientAccessiblePqs = _clientBusiness.GetClientUserAccessiblePqs(clientId, vendorId, SSession.UserId);
                if (clientAccessiblePqs != null && clientAccessiblePqs.Count() != 0)
                {
                    dropDown_Prequalification = (from preQualifications in clientAccessiblePqs
                                                 select new SelectListItem
                                                 {
                                                     Text = preQualifications.ClientName + " " + preQualifications.PrequalificationStart.ToString("MM/dd/yyyy"),
                                                     Value = preQualifications.PrequalificationId + "",
                                                     Selected = (pqId == preQualifications.PrequalificationId)
                                                 }).ToList();
                }

            }
            if (SSession.Role.ToString().Equals(LocalConstants.Admin)) // Kiran on 4/30/2014            
            {
                if (vendorPqsSpecificToClient != null && (vendorPqsSpecificToClient.FirstOrDefault().PrequalificationFinish.Date.Ticks >= DateTime.Now.Date.Ticks && vendorPqsSpecificToClient.FirstOrDefault().PrequalificationFinish.AddDays(-30).Date.Ticks <= DateTime.Now.Date.Ticks) && (vendorPqsSpecificToClient.FirstOrDefault().PrequalificationStatusId == 4 || vendorPqsSpecificToClient.FirstOrDefault().PrequalificationStatusId == 8 || vendorPqsSpecificToClient.FirstOrDefault().PrequalificationStatusId == 9 || vendorPqsSpecificToClient.FirstOrDefault().PrequalificationStatusId == 13 || vendorPqsSpecificToClient.FirstOrDefault().PrequalificationStatusId == 23 || vendorPqsSpecificToClient.FirstOrDefault().PrequalificationStatusId == 24 || vendorPqsSpecificToClient.FirstOrDefault().PrequalificationStatusId == 25 || vendorPqsSpecificToClient.FirstOrDefault().PrequalificationStatusId == 26))
                {
                    SelectListItem pre = new SelectListItem();
                    pre.Text = "Renewal";
                    pre.Value = "-1";
                    dropDown_Prequalification.Add(pre);
                }
            }

            return dropDown_Prequalification;
        }

        private string GetInvitationSentBy(LocalVendorInviteDetails invitationDetails)
        {
            if (invitationDetails.InvitationSentBy != null)
            {
                if (invitationDetails.InvitationSentBy.Trim().Equals(Resources.Resources.Common_label_None))
                    invitationDetails.InvitationSentBy = Resources.Resources.PQBanner_Label_WithoutInvitation;
                else if (invitationDetails.InvitationSentBy.Trim().Equals(Resources.Resources.Common_label_FirstVerify))
                    invitationDetails.InvitationSentBy = Resources.Resources.PQBanner_Label_InvitationFromFirstVerify;
                else if (invitationDetails.InvitationSentBy.Trim().Equals(Resources.Resources.Common_label_ClientUser))
                    invitationDetails.InvitationSentBy = Resources.Resources.PQBanner_Label_InvitationFrom;
            }
            return invitationDetails.InvitationSentBy;
        }
        [SessionExpire]
        [HeaderAndSidebar(headerName = "PREQUALIFICATIONREQUIREMENTS", sideMenuName = "", ViewName = "Employees")]
        public ActionResult LoadEmployees(Guid templateId, long PreQualificationId, Guid EmployeeID)
        {
            TempData["AutoLoadEmployeeID"] = EmployeeID;
            return RedirectToAction("Employees", "Prequalification", new { templateId = templateId, PreQualificationId = PreQualificationId });

        }
        [HttpPost]
        public ActionResult ChangePQTemplate(long pqId, Guid templateId)
        {
            var SourceTemplate = entityDB.Prequalification.Find(pqId).ClientTemplates.TemplateID;
            var ChangeTemplate = _prequalification.ChangePQTemplate(pqId, templateId);
            var DestinationTemplate = entityDB.ClientTemplates.Find(templateId).TemplateID;
            _prequalification.MoveAnswers(pqId, SourceTemplate, DestinationTemplate);
            _prequalification.MoveEmRValues(pqId);
            _prequalification.ClearPayment(pqId);
            return Json(ChangeTemplate, JsonRequestBehavior.AllowGet);
        }

        [SessionExpire]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "PREQUALIFICATIONREQUIREMENTS", sideMenuName = "", ViewName = "Employees")]
        public ActionResult Employees(Guid templateId, long PreQualificationId)
        {
            ViewBag.AutoLoadEmployeeID = TempData["AutoLoadEmployeeID"];
            var RestrictedCommentsSec = _sysUser.GetPQcomments(PreQualificationId);
            ViewBag.preComments = RestrictedCommentsSec?.Comments;
            ViewBag.PqCommentsHasFlag = RestrictedCommentsSec?.IsChecked;
            var Templates = _prequalification.GetPQTemplate(PreQualificationId);
            ViewBag.HasCommentsSecPermisstion = Templates.HideCommentsSec;
            ViewBag.HaspredefineCommentsEditPermission = _sysUser.IsUserHasPermission(SSession.UserId, 1012);

            SetupQuestionnaire(templateId, PreQualificationId, "Employees");

            //var Prequalification = entityDB.Prequalification.Find(PreQualificationId);

            //To get Employess
            //var employee_RoleId = entityDB.SystemRoles.FirstOrDefault(record => record.RoleName == "Employee Training").RoleId;

            //ViewBag.VendorId = Prequalification.VendorId;//Rajesh on 12/2/2014
            List<long> clientIds = null;
            ViewBag.PQId = PreQualificationId;
            try
            {
                clientIds = _prequalification.GetPrequalificationClient(PreQualificationId).ClientIdsList.ToList();
            }
            catch { }
            //ViewBag.VBEmployeeSysUserOrgs = employee_sysUserOrgs.OrderBy(rec => rec.SystemUsers.Contacts[0].LastName).ToList(); // kiran on 9/11/2014 // Kiran on 02/03/2015 for sorting the employees by lastname.
            //ViewBag.VBEmployeeSysUserOrgs = entityDB.SystemUsersOrganizationsQuizzes.Where(rec => rec.SystemUsersOrganizations.OrganizationId == Prequalification.VendorId).Select(rec => rec.SystemUsersOrganizations).Distinct().OrderBy(rec => rec.SystemUsers.Contacts.FirstOrDefault().LastName).ToList();
            List<OrganizationUsers> vendorUsers = _employee.GetEmployees(PreQualificationId, clientIds);

            return View(vendorUsers);
        }

        public ActionResult EmployeeDetails(Guid userId, long? PQId)
        {
            LocalVendorEmployeeQuizzes employee = new LocalVendorEmployeeQuizzes();
            //var employeeContactDetails = entityDB.Contact.FirstOrDefault(record => record.UserId == userId);
            UserInfo userInfo = _sysUser.GetUserInfo(userId);
            employee.EmployeeName = userInfo.LastName + ", " + userInfo.FirstName; // Kiran on 02/30/2015
            employee.UserId = userId;
            // Kiran on 1/31/2014
            //var currentEmpSystemUsersRecord = entityDB.SystemUsers.Find(userId);
            //ViewBag.VBEmpPassword = Utilities.EncryptionDecryption.Decryptdata(currentEmpSystemUsersRecord.Password);
            //ViewBag.VBEmpUserName = currentEmpSystemUsersRecord.UserName;
            employee.EmpEmail = userInfo.EmpUserName;
            employee.EmpPassword = Utilities.EncryptionDecryption.Decryptdata(userInfo.Password);
            ViewBag.PQId = PQId;
            //Ends<<<
            return View(employee);
        }

        /* Kiran on 11/5/2014
           To assign the On Site Training Status to employee(Fo client user only) */
        //[HttpPost]
        //public string AssignOnSiteTrainingToEmployee(string SystemUserOrgQuizIds)
        //{
        //    if (SystemUserOrgQuizIds != "")
        //    {
        //        foreach (var employee in SystemUserOrgQuizIds.Split(','))
        //        {
        //            if (employee != "")
        //            {
        //                try
        //                {
        //                    string[] empSysUserOrgQuiz = employee.Split('|');
        //                    var empSysUserOrgQuizId = Convert.ToInt64(empSysUserOrgQuiz[0].ToString().Replace("id_", ""));
        //                    var empQuizOnSiteTrainingStatus = empSysUserOrgQuiz[1].ToString().ToLower();
        //                    var empQuizOnSitetrainingCheckListrecord = entityDB.SystemUsersOrganizationsEmpQuizChecklist.FirstOrDefault(rec => rec.CheckListStatusID == 2 && rec.SysUserOrgQuizId == empSysUserOrgQuizId);
        //                    if (empQuizOnSiteTrainingStatus == "true")
        //                    {
        //                        if (empQuizOnSitetrainingCheckListrecord == null)
        //                        {
        //                            SystemUsersOrganizationsEmpQuizChecklist assignOnSiteTraining = new SystemUsersOrganizationsEmpQuizChecklist();
        //                            assignOnSiteTraining.SysUserOrgQuizId = empSysUserOrgQuizId;
        //                            assignOnSiteTraining.CheckListStatusID = 2;
        //                            entityDB.SystemUsersOrganizationsEmpQuizChecklist.Add(assignOnSiteTraining);
        //                        }
        //                    }
        //                    else
        //                    {
        //                        if (empQuizOnSitetrainingCheckListrecord != null)
        //                        {
        //                            entityDB.SystemUsersOrganizationsEmpQuizChecklist.Remove(empQuizOnSitetrainingCheckListrecord);
        //                        }
        //                    }
        //                }
        //                catch { }
        //            }
        //        }
        //        entityDB.SaveChanges();
        //        return "SaveSuccess";
        //    }
        //    return "";
        //}
        // Ends<<<




        // Kiran on 11/20/2013
        public JsonResult GetPeriodLengthAndUnit(int newStatus)
        {
            var currentStatusPeriodUnit = from periodUnit in entityDB.PrequalificationStatus.Where(record => record.PrequalificationStatusId == newStatus).ToList()
                                          select new SelectListItem
                                          {
                                              Text = periodUnit.PeriodLengthUnit,
                                              Value = periodUnit.PeriodLength + ""
                                          };

            return Json(currentStatusPeriodUnit);
        }
        // Ends<<<


        [HttpPost]
        [SessionExpire]
        [HeaderAndSidebar(headerName = "PREQUALIFICATIONREQUIREMENTS", sideMenuName = "", ViewName = "TemplateSectionsList")]
        public ActionResult ChangeStatus(LocalChangeStatus form)
        {
            var RestrictedCommentsSec = _sysUser.GetPQcomments(form.PrequalificationId);
            ViewBag.preComments = RestrictedCommentsSec?.Comments;
            ViewBag.PqCommentsHasFlag = RestrictedCommentsSec?.IsChecked;
            var Templates = _prequalification.GetPQTemplate(form.PrequalificationId);
            ViewBag.HasCommentsSecPermisstion = Templates.HideCommentsSec;
            ViewBag.Showlocations = Templates.HasVendorDetailLocation;
            ViewBag.HaspredefineCommentsEditPermission = _sysUser.IsUserHasPermission(SSession.UserId, 1012);
            @ViewBag.mode = 0;
            @ViewBag.templateId = form.templateId;

            var adminUsers = (from systemUsers in _prequalification.GetAdminUsers()
                              select new SelectListItem
                              {
                                  Text = systemUsers.Key,
                                  Value = systemUsers.Value,

                              }).ToList();

            SelectListItem defaultItem = new SelectListItem();
            defaultItem = new SelectListItem();
            defaultItem.Text = "";
            defaultItem.Value = "";
            var adminUsersWithDefaultValue = adminUsers.OrderBy(r => r.Text).ToList();
            adminUsersWithDefaultValue.Insert(0, defaultItem);
            var finalstatus = _prequalification.Finalstatus();
            ViewBag.finalstatus = finalstatus;
            ViewBag.VBAdminUsers = adminUsersWithDefaultValue;

            VendorDetailsViewModel pqData = _prequalification.GetPQCommonData(form.PrequalificationId);
            ViewBag.StatusName = GetPQStatusForBanner(form.PrequalificationId, pqData.PrequalificationStatusName);
            ViewBag.PrequalStartDate = pqData.PrequalificationCreate.ToString("MM/dd/yyyy"); // Kiran on 3/28/2014

            string statusName = ViewBag.StatusName;
            bool siteStatus = statusName == "Multiple";
            ViewBag.VBIsSiteHasMultiStatus = siteStatus;
            int? mode = @ViewBag.mode;
            ViewBag.VBStatusColor = _prequalification.GetPQStatusBannerColor(pqData.PrequalificationStatusId, SSession.Role, mode, statusName, LocalConstants.Employees);

            // Kiran on 3/28/2014
            ViewBag.StatusId = pqData.PrequalificationStatusId;
            if (@ViewBag.StatusId.ToString() == "10")
            {
                StatusBannerColor previousStatusBannerColor = _prequalification.GetPQPreviousStatusAndBannerColor(form.PrequalificationId, pqData.PrequalificationStatusId);
                ViewBag.VBPreviousStatusColour = previousStatusBannerColor.BannerColor;
                ViewBag.VBPreviousStatusName = previousStatusBannerColor.StatusName;
            }

            if (form.isStatusChanged == false)
            {
                ModelState.AddModelError("", "Please confirm status change");
            }
            if (form.SupportingDocumentsReviewStatus == false && form.NewStatus != "-1")
            {
                ModelState.AddModelError("", "Please confirm that supporting documents are reviewed");
            }
            else if (form.NewStatus != "16" && form.NewStatus != "10") // Kiran on 02/04/2015
            {
                if (form.adminUserId == "-1" || form.adminUserId == null)
                {
                    ModelState.AddModelError("adminUserId", "Please select, to whom the vendor is assigned");
                }
                else
                {
                    _prequalification.PostPqAssignerAndExceptionStatus(form);
                }
            }
            // Ends<<<

            if (form.NewStatus != "-1" && form.NewStatus != "16" && form.NewStatus != "10")//Rajesh on 10/1/2014 // Kiran on 02/04/2015
            {
                if (form.NewPeriodStart == null || form.NewPeriodStart == "")
                {
                    ModelState.AddModelError("NewPeriodStart", "Please select New Period Start Date");
                }
                if (form.NewStatusChanged == null || form.NewStatusChanged == "")
                {
                    ModelState.AddModelError("NewStatusChanged", "Please select New Status Change Date");
                }
            }

            Prequalification pq = _prequalification.GetPrequalification(form.PrequalificationId);
            if (form.displayTemplateSections != null)
            {
                string result = _prequalification.HideOrUnhidePrequalificationSections(form,
                    pq);
            }

            if (ModelState.IsValid)
            {
                if (!form.NewStatus.Equals("-1"))
                {
                    _prequalification.PostPqStatusChange(form, SSession.UserId.ToString());

                    var feecapApplicableStatuses = _prequalification.Finalstatus();
                    long PQstatus = Convert.ToInt64(form.NewStatus);
                    if (feecapApplicableStatuses.Contains(PQstatus))
                    {
                        helper.AccumulateFeeCap(form.PrequalificationId, entityDB);
                        entityDB.SaveChanges();
                    }

                    return
                        Redirect("../Prequalification/TemplateSectionsList?templateId=" +
                                 pqData.TemplateID +
                                 "&TemplateSectionID=-1&PreQualificationId=" + form.PrequalificationId);

                }
            }
            VendorDetailsViewModel pqDetails = _prequalification.GetPQCommonData(form.PrequalificationId);
            ViewBag.TemplateCode = pqDetails.TemplateCode;
            ViewBag.VendorId = pqDetails.VendorId;
            ViewBag.ClientId = pqDetails.ClientId;
            ViewBag.ClientName = pqDetails.ClientName;
            ViewBag.VendorName = pqDetails.VendorName;
            ViewBag.Period = pqDetails.PrequalificationStart.ToString("MM/dd/yy") + "-" +
                             pqDetails.PrequalificationFinish.ToString("MM/dd/yy");
            ViewBag.LogoPath = "";
            ViewBag.isEdit = pqDetails.PrequalificationStatusId == 10;
            ViewBag.LogoPath = ConfigurationSettings.AppSettings["LogoUploadPath"].ToString() + "/";

            ViewBag.PrequalificationStartDate = pqDetails.PrequalificationStart.ToString("MM/dd/yyyy");
            ViewBag.currentDate = DateTime.Now.ToString("MM/dd/yyyy");
            List<SelectListItem> dropDown_Prequalification = GetVendorPQsSpecificToClient(pqDetails.ClientId, pqDetails.VendorId, form.PrequalificationId);
            ViewBag.VBPreQualifications = dropDown_Prequalification;
            List<LocalTemplatesSideMenuModel> sideMenus = GetSideMenus(form.PrequalificationId, mode, pqDetails.TemplateID, "ChangeStatus");
            ViewBag.sideMenu = sideMenus;


            getCommand();
            //ViewBag.TemplatePrice = getTemplatePrice(pqDetails.VendorId, pqDetails.ClientTemplateID, form.PrequalificationId);
            ViewBag.PreQualificationId = form.PrequalificationId;

            //For Change Status
            ViewBag.CurrentStatus = pqDetails.PrequalificationStatusName;
            ViewBag.currentStatusPeriod = pqDetails.PrequalificationStart.ToString("MM/dd/yyyy");
            ViewBag.CurrentPQId = form.PrequalificationId;

            ViewBag.eritemplate = pqDetails.IsERI;
            LocalChangeStatus status = new LocalChangeStatus();
            status.templateId = form.templateId;
            status.PrequalificationId = form.PrequalificationId;

            var selectedStatus = form.NewStatus;
            List<long> hideSectionsToDisplay = _prequalification.GetPQSectionsToDisplay(form.PrequalificationId);
            List<TemplateSections> templateSections = _prequalification.GetTemplateSections(pqDetails.TemplateID);
            status.NotCompletedSection = templateSections.Where(rec => (rec.HideFromPrequalification != true || hideSectionsToDisplay.Contains(rec.TemplateSectionID))).OrderBy(x => x.DisplayOrder).ToList();

            status.NotCompletedSection_Status = new List<bool>();
            for (int i = 0; i < status.NotCompletedSection.Count; i++)
            {
                status.NotCompletedSection_Status.Add(false);
            }

            status.displayTemplateSections =
            (from pqTemplateSections in
                 templateSections.Where(rec => rec.HideFromPrequalification == true)
             select new TemplateSectionsStatus
             {
                 TemplateSectionId = pqTemplateSections.TemplateSectionID,
                 SectionName = pqTemplateSections.SectionName,
                 Visible = (entityDB.PrequalificationSectionsToDisplay.FirstOrDefault(rec => rec.PrequalificationId == form.PrequalificationId && rec.TemplateSectionId == pqTemplateSections.TemplateSectionID && rec.Visible == true) != null)
             }).ToList();


            var PreQualificationStatuses = new List<SelectListItem>();
            SelectListItem item = new SelectListItem();
            item.Value = "-1";
            item.Text = "";
            PreQualificationStatuses.Add(item);

            PreQualificationStatuses.AddRange((from a in entityDB.PrequalificationStatus.Where(rec => rec.PrequalificationStatusId > 2).ToList().Where(rec => rec.ShowInDashBoard == true).OrderBy(rec => rec.PrequalificationStatusName)
                                               select new SelectListItem()
                                               {
                                                   Text = a.PrequalificationStatusName,
                                                   Value = a.PrequalificationStatusId + "",
                                               }).ToList());

            ViewBag.PreQualificationStatuses = PreQualificationStatuses;

            var invitationDetails = _prequalification.GetPQInviteDetails(form.PrequalificationId);

            ViewBag.InvitationSentBy = GetInvitationSentBy(invitationDetails);
            ViewBag.InvitationSentByUser = invitationDetails.InvitationSentByUser;

            var invitationFromOptions = _prequalification.GetInvitationFromOptions(pqDetails.ClientId);
            List<SelectListItem> selectLists = new List<SelectListItem>();
            invitationFromOptions.ForEach(s => selectLists.Add(new SelectListItem() { Text = s.Key, Value = s.Value }));

            ViewBag.InvitationFromOptions = selectLists;
            return View(status);
        }

        //DV:RP DT:6/21/2013

        //Purpose:  To Open Dynamic Popup View
        [SessionExpireForView]
        public ActionResult DynamicPopUpView(long subSectionId, long PrequalificationId, bool eriDocsOnly, bool PQDocs)
        {
            ViewBag.PrequalificationId = PrequalificationId;
            TemplateSubSections subsection = entityDB.TemplateSubSections.Find(subSectionId);
            if (subsection.SubSectionTypeCondition.Equals("SitePrequalification"))
            {
                return Redirect("PrequalificationSites" + QueryStringModule.Encrypt("preQualificationSiteId=-1&subSectionId=" + subSectionId + "&preQualificationId=" + PrequalificationId));
            }
            else if (subsection.SubSectionTypeCondition.Equals("SupportingDocument"))
            {
                return Redirect("SupportingDocument" + QueryStringModule.Encrypt("subSectionId=" + subSectionId + "&PrequalificationId=" + PrequalificationId + "&eriDocsOnly=" + eriDocsOnly));
            }
            else if (subsection.SubSectionTypeCondition.Equals("PQDocuments2"))
            {
                return Redirect("PQDocument" + QueryStringModule.Encrypt("subSectionId=" + subSectionId + "&PrequalificationId=" + PrequalificationId + "&PQDocs=" + PQDocs));
            }
            else if (subsection.SubSectionTypeCondition.Equals("PrequalComments"))
            {
                return Redirect("AddComments" + QueryStringModule.Encrypt("subSectionId=" + subSectionId + "&PrequalificationId=" + PrequalificationId));
            }
            return Redirect("PrequalificationSites" + QueryStringModule.Encrypt("preQualificationSiteId=-1&subSectionId=" + subSectionId + "&preQualificationId=" + PrequalificationId));
        }
        public ActionResult AddComments(long subSectionId, long PrequalificationId)
        {
            LocalPrequalificationComments form = new LocalPrequalificationComments();
            form.PrequalificationId = PrequalificationId;
            form.SubSectionID = subSectionId;
            form.TemplateSectionID = entityDB.TemplateSubSections.Find(subSectionId).TemplateSectionID;
            return View(form);
        }
        [HttpPost]
        public ActionResult AddComments(LocalPrequalificationComments form)//Comments can only insertion
        {
            PrequalificationComments comments = new PrequalificationComments();
            comments.Comments = form.Comments;
            comments.CommentsBy = (Guid)Session["UserId"];
            comments.CommentsDate = DateTime.Now;
            comments.CommentsTime = DateTime.Now.TimeOfDay;
            comments.PrequalificationId = form.PrequalificationId;
            comments.SubSectionID = form.SubSectionID;
            comments.TemplateSectionID = form.TemplateSectionID;
            entityDB.PrequalificationComments.Add(comments);
            entityDB.SaveChanges();
            return Redirect("PopUpCloseView?subSectionId=" + form.SubSectionID);
        }
        public ActionResult UnlockClientDocuments(long pqId)
        {
            return Json(_prequalification.UnlockPQDocs(pqId));
        }
        [SessionExpireForView]
        public ActionResult SupportingDocument(Guid? DocumentId, long subSectionId, int? mode, long PrequalificationId, bool eriDocsOnly)
        {
            var currentPreQualification = _prequalification.GetPrequalification(PrequalificationId);

            getClientsAndTemplateType(PrequalificationId, currentPreQualification, eriDocsOnly, false);
            LocalSupportedDocument sDoc = _documents.GetSupportingDocument(subSectionId, DocumentId);
            LocalSupportedDocument form = new LocalSupportedDocument();

            form.subSectionId = subSectionId;
            form.SectionId = sDoc.SectionId;
            form.PrequalificationId = PrequalificationId;
            form.Mode = mode;//1-means edit Mode
            var client = _organization.GetOrganization(currentPreQualification.ClientId);
            if (DocumentId != null)
            {
                form.DocumentTypeId = sDoc.DocumentTypeId + "";
                form.ClientId = sDoc.ClientId.ToLongNullSafe();
                form.DocumentId = sDoc.DocumentId;
                eriDocsOnly = (sDoc.ClientOrganizations == null ? client.OrganizationType : sDoc.ClientOrganizations.OrganizationType) == OrganizationType.SuperClient;
                try
                {
                    form.Expires = sDoc.Expires.ToString();
                }
                catch { }
            }
            if (!_clientBusiness.CheckTemplateIsERI(PrequalificationId))
            {
                form.ClientId = currentPreQualification.ClientId;
            }

            return View(form);
        }

        private void getClientsAndTemplateType(long PrequalificationId, Prequalification currentPreQualification, bool eriDocsOnly, bool PQDocs)
        {
            PrequalificationClient pqClient = null;
            bool isERITemplate = _clientBusiness.CheckTemplateIsERI(PrequalificationId);
            var clients = new List<long>();
            if ((isERITemplate && !eriDocsOnly) || (isERITemplate && PQDocs))
            {
                pqClient = _prequalification.GetPrequalificationClient(PrequalificationId);
                if (pqClient != null)
                    clients = pqClient.ClientIdsList.ToList();
            }

            else
                clients.Add(currentPreQualification.ClientId);//It will also add the ERI...
            if (SSession.Role.Equals(OrganizationType.Client))
            {
                clients = clients.Where(r => r == currentPreQualification.ClientId || r == SSession.OrganizationId).ToList();
            }
            var pqSelectedClients = _clientBusiness.GetClients(clients);
            ViewBag.clients = pqSelectedClients;
            ViewBag.isERITemplate = isERITemplate;
        }



        public bool? isDocumentExpire(long preQualification, long docTypeId, long clientId)
        {
            return _clientBusiness.isDocumentExpire(preQualification, docTypeId, clientId);
        }
        [HttpPost]
        [SessionExpireForView]
        public ActionResult SupportingDocument(HttpPostedFileBase file, LocalSupportedDocument form)
        {
            if (form.Mode != null && form.Mode == 1)
                ModelState["file"].Errors.Clear();
            var currentPreQualification = _prequalification.GetPrequalification(form.PrequalificationId);
            var clientOrg = _organization.GetOrganization(form.ClientId);
            getClientsAndTemplateType(form.PrequalificationId, currentPreQualification, clientOrg.OrganizationType == OrganizationType.SuperClient, false);
            if (_clientBusiness.isDocumentExpire(form.PrequalificationId, Convert.ToInt64(form.DocumentTypeId), form.ClientId) == true && form.Expires == null)
            {
                ModelState.AddModelError("", "Please enter Document expiration date.");
                return View(form);
            }

            //if (ModelState.IsValid)
            {
                if (form.Mode != null && form.Mode == 1)//If it is edit mode
                {
                }
                else if (!Path.GetExtension(file.FileName).ToLower().Equals(".pdf"))
                {
                    ModelState.AddModelError("", "Invalid File format,Please select PDF ");
                    return View(form);
                }
                //sumanth on 10/26/2015 for FVOS-86 the exipiring date must be greater than current date.
                if (form.Expires != null)
                {
                    DateTime docExpires = Convert.ToDateTime(form.Expires);
                    if (docExpires < DateTime.Now)
                    {
                        ModelState.AddModelError("", "Document expiration date must not be less than current date.");
                        return View(form);
                    }
                    //sumanth ends on 10/26/2015.
                }
                Document doc = new Document();
                _documents.PostDocumentBusiness(form, SSession.Role, SSession.UserId, file, doc);
                if (form.Mode != null && form.Mode == 1) ;
                else
                {
                    if (file != null && file.ContentLength > 0)
                    {
                        //SAVE file
                        var fileName = Path.GetFileName(file.FileName);
                        var extension = Path.GetExtension(file.FileName);

                        Directory.CreateDirectory(Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["ClientUploadsPath"])));
                        var path = Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["ClientUploadsPath"]), fileName + extension);
                        file.SaveAs(path);
                        string uniqueId;
                        if (form.DocumentId != null)
                            uniqueId = form.DocumentId.ToString();
                        else
                        {
                            uniqueId = doc.DocumentId.ToString();
                        }
                        string oldFilePath = Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["ClientUploadsPath"]), fileName + extension);
                        string newFilePath = Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["ClientUploadsPath"]), uniqueId + extension);
                        System.IO.File.Delete(newFilePath);
                        System.IO.File.Move(oldFilePath, newFilePath);
                    }
                    else
                    {
                        ModelState.AddModelError("file", "*");
                    }
                }
                if (form.DocumentId != null)
                {
                    var modificationType = (form.Mode != null && form.Mode == 1) ?
                        DocumentModificationType.Modified : DocumentModificationType.Replaced;
                    _documents.RecordLog(doc.DocumentId, SSession.UserId, modificationType.ToString());
                }
                else
                {
                    _documents.RecordLog(doc.DocumentId, SSession.UserId, DocumentModificationType.Added.ToString());
                }
                return Redirect("PopUpCloseView?subSectionId=" + form.subSectionId);
            }
            //return View(form);
        }
        public ActionResult GetDocumentLog(Guid DocumentId)
        {
            return Json(_documents.GetLogs(DocumentId).OrderByDescending(r => r.ModifiedOn), JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetClientSupportingdocs(long clientId, long prequalificationId, long SectionId)
        {
            return Json(_clientBusiness.GetClientSupportingDocs(clientId, prequalificationId, SectionId));
        }

        //Dev:RP DT:7/24/2013
        //Desc:-To Delete Document
        [HttpPost]
        [SessionExpire]
        public ActionResult RemoveDocument(Guid documentId)
        {

            Document document = entityDB.Document.Find(documentId);
            // Kiran on 01/28/2015
            //try
            //{
            //    if (document != null)
            //    {
            //        var dotIndex = document.DocumentName.LastIndexOf('.');
            //        var extension = document.DocumentName.Substring(dotIndex);
            //        var path = Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["ClientUploadsPath"]), document.DocumentId + extension);
            //        if (path != null)
            //        {
            //            System.IO.File.Delete(path);
            //            entityDB.Document.Remove(document);
            //            entityDB.SaveChanges();
            //        }
            //    }
            //}
            //catch
            //{
            document.DocumentStatus = "Deleted";
            document.ShowOnDashboard = false;
            entityDB.Entry(document).State = EntityState.Modified;
            entityDB.SaveChanges();
            //}
            _documents.RecordLog(documentId, SSession.UserId, DocumentModificationType.Deleted.ToString());
            // Ends<<<
            return Json("Ok");
        }
        //<<< RemoveDocument Ends

        //[HttpGet]
        //[SessionExpireForView]
        //public ActionResult PrequalificationSites(long preQualificationSiteId, long subSectionId, long preQualificationId)
        //{
        //    LocalAddPrequalificationSite locPreQualificationSite = new LocalAddPrequalificationSite();
        //    PrequalificationSites preSite;

        //    long clientId = entityDB.Prequalification.Find(preQualificationId).ClientId;

        //    ViewBag.BussinessUnits = from busUnits in entityDB.ClientBusinessUnits.Where(rec => rec.ClientId == clientId).ToList()
        //                             select new SelectListItem
        //                             {
        //                                 Text = busUnits.BusinessUnitName,
        //                                 Value = busUnits.ClientBusinessUnitId.ToString(),
        //                             };

        //    if (preQualificationSiteId == -1)
        //    {
        //        preSite = new PrequalificationSites();
        //        locPreQualificationSite.siteId = -1;
        //    }
        //    else
        //    {
        //        preSite = entityDB.PrequalificationSites.Find(preQualificationSiteId);
        //        //locPreQualificationSite.BusinessUnitLocation = preSite.ClientBULocation;
        //        locPreQualificationSite.BusinessUnit = (long)preSite.ClientBusinessUnitId;
        //        locPreQualificationSite.ClientBusinessUnitSite = (long)preSite.ClientBusinessUnitSiteId;
        //        locPreQualificationSite.siteId = preSite.PrequalificationSitesId;
        //        locPreQualificationSite.Comments = preSite.Comment;


        //        var busUnit = entityDB.ClientBusinessUnits.FirstOrDefault(record => record.ClientBusinessUnitId == preSite.ClientBusinessUnitId);

        //        locPreQualificationSite.BusinessUnitSites = (from busUnitSites in busUnit.ClientBusinessUnitSites.ToList()
        //                                                     select new SelectListItem
        //                                                     {
        //                                                         Text = busUnitSites.SiteName,
        //                                                         Value = busUnitSites.ClientBusinessUnitSiteId.ToString(),
        //                                                         Selected = (locPreQualificationSite.ClientBusinessUnitSite == busUnitSites.ClientBusinessUnitSiteId),
        //                                                     }).ToList();

        //    }
        //    locPreQualificationSite.subSectionId = subSectionId;
        //    locPreQualificationSite.preQualificationId = preQualificationId;
        //    ViewBag.preQualificationId = preQualificationId;
        //    return View(locPreQualificationSite);
        //}


        //[HttpPost]
        //[SessionExpireForView]
        //public ActionResult PrequalificationSites(LocalAddPrequalificationSite locQualificationSite)
        //{
        //    //To Check prequalificationsite exist with locQualificationSite Details
        //    long busUnitId = Convert.ToInt64(locQualificationSite.BusinessUnit);
        //    long busUnitSiteId = Convert.ToInt64(locQualificationSite.ClientBusinessUnitSite);

        //    long clientId = entityDB.Prequalification.Find(locQualificationSite.preQualificationId).ClientId;
        //    // Kiran on 3/19/2014

        //    ViewBag.preQualificationId = locQualificationSite.preQualificationId;
        //    // Ver 2.0 <---
        //    var selectedPrequalunitOrSite = entityDB.PrequalificationSites.FirstOrDefault(rec => rec.ClientBusinessUnitId == locQualificationSite.BusinessUnit && rec.ClientBusinessUnitSiteId == locQualificationSite.ClientBusinessUnitSite && rec.PrequalificationSitesId != locQualificationSite.siteId && rec.PrequalificationId == locQualificationSite.preQualificationId);
        //    long selectedBusUnitId = Convert.ToInt64(locQualificationSite.BusinessUnit);
        //    long selectedBusUnitSitedId = Convert.ToInt64(locQualificationSite.ClientBusinessUnitSite);

        //    ViewBag.BussinessUnits = from busUnits in entityDB.ClientBusinessUnits.Where(rec => rec.ClientId == clientId).ToList()
        //                             select new SelectListItem
        //                             {
        //                                 Text = busUnits.BusinessUnitName,
        //                                 Value = busUnits.ClientBusinessUnitId.ToString(),
        //                             };

        //    if (selectedPrequalunitOrSite != null)
        //    {
        //        var busUnit = entityDB.ClientBusinessUnits.FirstOrDefault(record => record.ClientBusinessUnitId == busUnitId);

        //        var curretUserOrgBusUnitSites = from busUnitSites in busUnit.ClientBusinessUnitSites.ToList()
        //                                        select new SelectListItem
        //                                        {
        //                                            Text = busUnitSites.SiteName,
        //                                            Value = busUnitSites.ClientBusinessUnitSiteId.ToString(),
        //                                        };

        //        ViewBag.BussinessUnitSites = curretUserOrgBusUnitSites;
        //        //ModelState.AddModelError("", @Resources.Resources.PreQualificationSite_Error_siteExist);
        //        ViewBag.VBBuexists = true;
        //        return View(locQualificationSite);
        //    }
        //    // Ends<<<

        //    //if (ModelState.IsValid) // Kiran on 3/13/2014
        //    //{
        //    PrequalificationSites preSite;
        //    if (locQualificationSite.siteId == -1)//New Site
        //        preSite = new PrequalificationSites();
        //    else//Existing Site Need to Update
        //        preSite = entityDB.PrequalificationSites.Find(locQualificationSite.siteId);
        //    //preSite.ClientBULocation = locQualificationSite.BusinessUnitLocation;
        //    preSite.ClientBusinessUnitId = Convert.ToInt64(locQualificationSite.BusinessUnit);
        //    preSite.ClientBusinessUnitSiteId = Convert.ToInt64(locQualificationSite.ClientBusinessUnitSite);
        //    preSite.PrequalificationId = locQualificationSite.preQualificationId;
        //    preSite.Comment = locQualificationSite.Comments;

        //    //Rajesh on 09/09/2013

        //    var preQualification = entityDB.Prequalification.Find(locQualificationSite.preQualificationId);
        //    preSite.ClientId = preQualification.ClientId;
        //    preSite.VendorId = preQualification.VendorId;
        //    //Ends<<<

        //    if (locQualificationSite.siteId == -1)
        //        entityDB.PrequalificationSites.Add(preSite);
        //    else
        //        entityDB.Entry(preSite).State = EntityState.Modified;
        //    entityDB.SaveChanges();
        //    return Redirect("PopUpCloseView?subSectionId=" + locQualificationSite.subSectionId);

        //    //}           

        //    //return View(locQualificationSite);
        //}

        // Developed by Sandhya on 04/08/2015
        [HttpGet]
        [SessionExpireForView]
        public ActionResult PrequalificationSites(long preQualificationSiteId, long subSectionId, long preQualificationId)
        {
            LocalAddPrequalificationSite locPreQualificationSite = new LocalAddPrequalificationSite();
            PrequalificationSites preSite;

            @ViewBag.PrequalificationsiteId = preQualificationSiteId;
            var prequalification = _prequalification.GetPrequalification(preQualificationId);
            long clientId = prequalification.ClientId;
            ViewBag.Client = clientId;

            getClientsAndTemplateTypeForLocations(preQualificationId, clientId);
            if (preQualificationSiteId == -1)
            {
                preSite = new PrequalificationSites();
                locPreQualificationSite.siteId = -1;
            }
            else
            {
                preSite = _pqSites.GetPqSite(preQualificationSiteId, null, null, null);
                locPreQualificationSite.BusinessUnit = (long)preSite.ClientBusinessUnitId;
                locPreQualificationSite.ClientBusinessUnitSite = (long)preSite.ClientBusinessUnitSiteId;
                locPreQualificationSite.siteId = preSite.PrequalificationSitesId;
                locPreQualificationSite.Comments = preSite.Comment;
                locPreQualificationSite.ClientId = preSite.ClientId.ToLongNullSafe();

                var buSites = _pqSites.GetBusinessUnitSites(preSite.ClientBusinessUnitId);
                locPreQualificationSite.BusinessUnitSites = (from busUnitSites in buSites.ToList()
                                                             select new SelectListItem
                                                             {
                                                                 Text = busUnitSites.Key,
                                                                 Value = busUnitSites.Value.ToString(),
                                                                 Selected = (locPreQualificationSite.ClientBusinessUnitSite.ToString() == busUnitSites.Value.ToString()),
                                                             }).ToList();

            }
            locPreQualificationSite.subSectionId = subSectionId;
            locPreQualificationSite.preQualificationId = preQualificationId;
            ViewBag.preQualificationId = preQualificationId;
            return View(locPreQualificationSite);
        }

        private void getClientsAndTemplateTypeForLocations(long preQualificationId, long clientId)
        {
            var org = _organization.GetOrganization(clientId);
            PrequalificationClient pqClient = null;
            bool isERITemplate = _clientBusiness.CheckTemplateIsERI(preQualificationId);
            var clients = new List<long>();

            if (isERITemplate)
            {
                pqClient = _prequalification.GetPrequalificationClient(preQualificationId);

                if (SSession.Role == OrganizationType.Client)
                {
                    clients = pqClient.ClientIdsList.Where(r => r == SSession.OrganizationId).ToList();
                }
                else if (pqClient != null)
                {
                    clients = pqClient.ClientIdsList.ToList();
                }
            }
            if (org.OrganizationType != OrganizationType.SuperClient)
                clients.Add(clientId);
            var pqSelectedClients = _clientBusiness.GetClients(clients);
            ViewBag.clients = pqSelectedClients;
            ViewBag.isERITemplate = isERITemplate;
        }

        [HttpPost]
        [SessionExpireForView]
        public ActionResult PrequalificationSites(LocalAddPrequalificationSite locQualificationSite)
        {
            ViewBag.Vslist = locQualificationSite.ClientBusinessUnitSites.Count();
            //To Check prequalificationsite exist with locQualificationSite Details
            long busUnitId = Convert.ToInt64(locQualificationSite.BusinessUnit);
            long busUnitSiteId = Convert.ToInt64(locQualificationSite.ClientBusinessUnitSite);

            long clientId = _prequalification.GetPrequalification(locQualificationSite.preQualificationId).ClientId;

            ViewBag.preQualificationId = locQualificationSite.preQualificationId;

            //var selectedPrequalunitOrSite = entityDB.PrequalificationSites.FirstOrDefault(rec => rec.ClientBusinessUnitId == locQualificationSite.BusinessUnit && rec.ClientBusinessUnitSiteId == locQualificationSite.ClientBusinessUnitSite && rec.PrequalificationSitesId != locQualificationSite.siteId && rec.PrequalificationId == locQualificationSite.preQualificationId);
            var selectedPrequalunitOrSite = _pqSites.GetPqSite(locQualificationSite.siteId,
                locQualificationSite.BusinessUnit, locQualificationSite.ClientBusinessUnitSite,
                locQualificationSite.preQualificationId);
            long selectedBusUnitId = Convert.ToInt64(locQualificationSite.BusinessUnit);
            long selectedBusUnitSitedId = Convert.ToInt64(locQualificationSite.ClientBusinessUnitSite);
            getClientsAndTemplateTypeForLocations(locQualificationSite.preQualificationId, clientId);

            if (selectedPrequalunitOrSite != null)
            {
                var buSites = _pqSites.GetBusinessUnitSites(locQualificationSite.BusinessUnit);
                var curretUserOrgBusUnitSites = (from busUnitSites in buSites.ToList()
                                                 select new SelectListItem
                                                 {
                                                     Text = busUnitSites.Key,
                                                     Value = busUnitSites.Value.ToString(),
                                                 }).ToList();

                //var busUnit = entityDB.ClientBusinessUnits.FirstOrDefault(record => record.ClientBusinessUnitId == busUnitId);

                //var curretUserOrgBusUnitSites = from busUnitSites in busUnit.ClientBusinessUnitSites.ToList()
                //                                select new SelectListItem
                //                                {
                //                                    Text = busUnitSites.SiteName,
                //                                    Value = busUnitSites.ClientBusinessUnitSiteId.ToString(),
                //                                };

                ViewBag.BussinessUnitSites = curretUserOrgBusUnitSites;
                ViewBag.VBBuexists = true;
                return View(locQualificationSite);
            }

            //foreach (var prequalificationSite in locQualificationSite.ClientBusinessUnitSites)
            //{
            //    var prequalSite = entityDB.PrequalificationSites.FirstOrDefault(rec => rec.PrequalificationId == locQualificationSite.preQualificationId && rec.ClientBusinessUnitSiteId == prequalificationSite.BusinessUnitSiteId && rec.ClientBusinessUnitId == locQualificationSite.BusinessUnit);
            //    if (prequalSite == null)
            //    {
            //        PrequalificationSites preSite = new PrequalificationSites();
            //        if (prequalificationSite.isChecked && prequalificationSite.BusinessUnitSiteId != 999)
            //        {
            //            preSite.ClientBusinessUnitSiteId = Convert.ToInt64(prequalificationSite.BusinessUnitSiteId);

            //            preSite.ClientBusinessUnitId = Convert.ToInt64(locQualificationSite.BusinessUnit);
            //            preSite.PrequalificationId = locQualificationSite.preQualificationId;
            //            preSite.Comment = locQualificationSite.Comments;

            //            var preQualification = entityDB.Prequalification.Find(locQualificationSite.preQualificationId);
            //            preSite.ClientId = locQualificationSite.ClientId;
            //            preSite.VendorId = preQualification.VendorId;
            //            //To implement group wise payment insted of BU wise Payment.
            //            if (preQualification.ClientTemplates.Templates.IsERI)
            //            {
            //                var paymentId = _prequalification.GetBUPaymentId((long)preSite.ClientBusinessUnitId, locQualificationSite.preQualificationId);
            //                if (paymentId != null)
            //                    preSite.PrequalificationPaymentsId = paymentId;
            //            }
            //            //preSite.PrequalificationStatusId = preQualification.PrequalificationStatusId;

            //            entityDB.PrequalificationSites.Add(preSite);

            //            entityDB.SaveChanges();
            //        }
            //    }
            //}
            //// Siva 20th Feb
            //var locationsSec = entityDB.PrequalificationCompletedSections.FirstOrDefault(rec => rec.PrequalificationId == locQualificationSite.preQualificationId && rec.TemplateSections.TemplateSectionType == 7);
            //if (locationsSec != null)

            //    entityDB.PrequalificationCompletedSections.Remove(locationsSec);
            //// Siva 20th Feb

            //entityDB.SaveChanges();
            _pqSites.PostBuSites(locQualificationSite);
            return Redirect("PopUpCloseView?subSectionId=" + locQualificationSite.subSectionId);

            //}           

            //return View(locQualificationSite);
        }

        // Ends<<<

        public ActionResult GetClientBusinessUnits(long clientId)
        {
            return Json(_clientBusiness.GetClientBusinessUnits(clientId));
        }
        public ActionResult GetPqDocuments(long pqid)
        {
            var Docs = _clientBusiness.GetPQBusinessUnitDocuments(pqid, SSession.Role, SSession.UserId);
            ViewBag.DocCount = Docs.BuSites.Count();
            ViewBag.clients = entityDB.PrequalificationSites.Where(r => r.PrequalificationId == pqid).OrderBy(r => r.Client.Name).Select(r => r.Client.Name).Distinct().ToList();

            return PartialView(Docs);
        }
        [HttpGet]
        [SessionExpireForView]
        public ActionResult EditPrequalificationSite(long preQualificationSiteId, long subSectionId, long preQualificationId)
        {
            LocalAddPrequalificationSite locPreQualificationSite = new LocalAddPrequalificationSite();
            PrequalificationSites preSite;

            long clientId = entityDB.Prequalification.Find(preQualificationId).ClientId;

            //ViewBag.BussinessUnits = from busUnits in entityDB.ClientBusinessUnits.Where(rec => rec.ClientId == clientId).ToList()
            //                         select new SelectListItem
            //                         {
            //                             Text = busUnits.BusinessUnitName,
            //                             Value = busUnits.ClientBusinessUnitId.ToString(),
            //                         };

            getClientsAndTemplateTypeForLocations(preQualificationId, clientId);

            if (preQualificationSiteId == -1)
            {
                preSite = new PrequalificationSites();
                locPreQualificationSite.siteId = -1;
            }
            else
            {
                preSite = entityDB.PrequalificationSites.Find(preQualificationSiteId);
                //locPreQualificationSite.BusinessUnitLocation = preSite.ClientBULocation;
                locPreQualificationSite.BusinessUnit = (long)preSite.ClientBusinessUnitId;
                locPreQualificationSite.ClientBusinessUnitSite = (long)preSite.ClientBusinessUnitSiteId;
                locPreQualificationSite.siteId = preSite.PrequalificationSitesId;
                locPreQualificationSite.Comments = preSite.Comment;
                locPreQualificationSite.ClientId = preSite.ClientId.ToLongNullSafe();


                var busUnit = entityDB.ClientBusinessUnits.FirstOrDefault(record => record.ClientBusinessUnitId == preSite.ClientBusinessUnitId);

                locPreQualificationSite.BusinessUnitSites = (from busUnitSites in busUnit.ClientBusinessUnitSites.ToList()
                                                             select new SelectListItem
                                                             {
                                                                 Text = busUnitSites.SiteName,
                                                                 Value = busUnitSites.ClientBusinessUnitSiteId.ToString(),
                                                                 Selected = (locPreQualificationSite.ClientBusinessUnitSite == busUnitSites.ClientBusinessUnitSiteId),
                                                             }).ToList();

            }
            locPreQualificationSite.subSectionId = subSectionId;
            locPreQualificationSite.preQualificationId = preQualificationId;
            ViewBag.preQualificationId = preQualificationId;
            return View(locPreQualificationSite);
        }

        [HttpPost]
        [SessionExpireForView]
        public ActionResult EditPrequalificationSite(LocalAddPrequalificationSite locQualificationSite)
        {
            long clientId = entityDB.Prequalification.Find(locQualificationSite.preQualificationId).ClientId;
            getClientsAndTemplateTypeForLocations(locQualificationSite.preQualificationId, clientId);

            //To Check prequalificationsite exist with locQualificationSite Details
            long busUnitId = Convert.ToInt64(locQualificationSite.BusinessUnit);
            long busUnitSiteId = Convert.ToInt64(locQualificationSite.ClientBusinessUnitSite);


            // Kiran on 3/19/2014

            ViewBag.preQualificationId = locQualificationSite.preQualificationId;
            // Ver 2.0 <---
            var selectedPrequalunitOrSite = entityDB.PrequalificationSites.FirstOrDefault(rec => rec.ClientBusinessUnitId == locQualificationSite.BusinessUnit && rec.ClientBusinessUnitSiteId == locQualificationSite.ClientBusinessUnitSite && rec.PrequalificationSitesId != locQualificationSite.siteId && rec.PrequalificationId == locQualificationSite.preQualificationId);
            long selectedBusUnitId = Convert.ToInt64(locQualificationSite.BusinessUnit);
            long selectedBusUnitSitedId = Convert.ToInt64(locQualificationSite.ClientBusinessUnitSite);

            //ViewBag.BussinessUnits = from busUnits in entityDB.ClientBusinessUnits.Where(rec => rec.ClientId == clientId).ToList()
            //                         select new SelectListItem
            //                         {
            //                             Text = busUnits.BusinessUnitName,
            //                             Value = busUnits.ClientBusinessUnitId.ToString(),
            //                         };

            if (selectedPrequalunitOrSite != null)
            {
                var busUnit = entityDB.ClientBusinessUnits.FirstOrDefault(record => record.ClientBusinessUnitId == busUnitId);

                var curretUserOrgBusUnitSites = from busUnitSites in busUnit.ClientBusinessUnitSites.ToList()
                                                select new SelectListItem
                                                {
                                                    Text = busUnitSites.SiteName,
                                                    Value = busUnitSites.ClientBusinessUnitSiteId.ToString(),
                                                };

                ViewBag.BussinessUnitSites = curretUserOrgBusUnitSites;
                //ModelState.AddModelError("", @Resources.Resources.PreQualificationSite_Error_siteExist);
                ViewBag.VBBuexists = true;
                return View(locQualificationSite);
            }
            // Ends<<<

            //if (ModelState.IsValid) // Kiran on 3/13/2014
            //{
            PrequalificationSites preSite;
            if (locQualificationSite.siteId == -1)//New Site
                preSite = new PrequalificationSites();
            else//Existing Site Need to Update
                preSite = entityDB.PrequalificationSites.Find(locQualificationSite.siteId);
            //preSite.ClientBULocation = locQualificationSite.BusinessUnitLocation;
            preSite.ClientBusinessUnitId = Convert.ToInt64(locQualificationSite.BusinessUnit);
            preSite.ClientBusinessUnitSiteId = Convert.ToInt64(locQualificationSite.ClientBusinessUnitSite);
            preSite.PrequalificationId = locQualificationSite.preQualificationId;
            preSite.Comment = locQualificationSite.Comments;

            //Rajesh on 09/09/2013

            var preQualification = entityDB.Prequalification.Find(locQualificationSite.preQualificationId);
            preSite.ClientId = locQualificationSite.ClientId;
            preSite.VendorId = preQualification.VendorId;
            //Ends<<<

            if (locQualificationSite.siteId == -1)
                entityDB.PrequalificationSites.Add(preSite);
            else
                entityDB.Entry(preSite).State = EntityState.Modified;
            entityDB.SaveChanges();
            return Redirect("PopUpCloseView?subSectionId=" + locQualificationSite.subSectionId);
        }

        //Dev:RP DT:7/24/2013
        //Desc:-To get dynamic content of a sub section
        [OutputCache(Duration = 0)]
        public ActionResult GetDynamicSubSectionContent(long subSectionId, long preQualificationID, long? mode)
        {

            ViewBag.mode = mode;
            ViewBag.subSectionId = subSectionId;
            ViewBag.preQualificationID = preQualificationID;
            TemplateSubSections subsection = _prequalification.GetTemplateSubSection(subSectionId);
            var templateSectionId = subsection.TemplateSectionID;
            var currentPrequalification = _prequalification.GetPrequalification(preQualificationID); // Kiran on 01/31/2015
            if (subsection.SubSectionTypeCondition.Equals(SubSectionTypeCondition.SitePrequalification))
            {
                ConfigurePQSites(preQualificationID);
                ViewBag.IsVendorDetails = false;
                return PartialView("PrequalificationSitesPartialView");
            }

            if (subsection.SubSectionTypeCondition.Equals(SubSectionTypeCondition.SupportingDocument))
            {
                Guid userId = new Guid(Session["UserId"].ToString());
                ViewBag.RestrictedAccess = FVGen3.BusinessLogic.RolesHelper.IsUserInRole(userId, 1000, "read", currentPrequalification.ClientId, currentPrequalification.VendorId) || Session["RoleName"].Equals(LocalConstants.SuperAdmin);

                //var clientTemplateIds = entityDB.ClientTemplates.FirstOrDefault(row => row.ClientTemplateID == currentPrequalification.ClientTemplateId).Templates.ClientTemplates.Select(rec => rec.ClientTemplateID).ToList();
                //var documentTypeIds = entityDB.ClientTemplateReportingData.Where(rec => rec.ReportingType == 2 && clientTemplateIds.Contains(rec.ClientTemplateId)).Select(rec => rec.ReportingTypeId).ToList();
                //var sysUserBusiness = new FVGen3.BusinessLogic.SystemUserBusiness();
                //if (Session["RoleName"].Equals("Admin") || Session["RoleName"].Equals("Vendor"))
                //{
                //    if (SSession.Role.Equals(OrganizationType.Admin))
                //        ViewBag.Documents = entityDB.Document.Where(rec => rec.ReferenceType == "PreQualification" && rec.SectionId == templateSectionId && rec.ReferenceRecordID == preQualificationID && documentTypeIds.Contains(rec.DocumentTypeId)).GroupBy(rec => rec.ClientId).ToList();
                //    else
                //        ViewBag.Documents = entityDB.Document.Where(rec => rec.ReferenceType == "PreQualification" && rec.SectionId == templateSectionId && rec.ReferenceRecordID == preQualificationID && (rec.DocumentStatus != "Deleted" || rec.DocumentStatus == null) && documentTypeIds.Contains(rec.DocumentTypeId)).GroupBy(rec => rec.ClientId).ToList(); // Kiran on 01/28/2015 for displaying only approved/rejected documents // Kiran on 01/31/2015 to display the documents that are applicable for this client template.
                //}

                //else
                //{
                //    ViewBag.Documents = entityDB.Document.Where(rec => (rec.ClientId == null || rec.ClientId == SSession.OrganizationId || rec.ClientOrganizations.OrganizationType == OrganizationType.SuperClient) && rec.ReferenceType == "PreQualification"
                //    && rec.ReferenceRecordID == preQualificationID
                //    && rec.SectionId == templateSectionId
                //    && (rec.DocumentStatus == "Approved" || rec.Systemusers.SystemUsersOrganizations.FirstOrDefault().Organizations.OrganizationType == OrganizationType.Client)
                //    && documentTypeIds.Contains(rec.DocumentTypeId)).GroupBy(rec => rec.ClientId).ToList(); // Kiran on 01/31/2015 to display the documents that are applicable for this client template.
                //}
                ViewBag.Documents = _prequalification.GetPqSupportingDocs(templateSectionId, preQualificationID,
                    SSession.Role, SSession.OrganizationId);
                return PartialView("SupportedDocumentsPartialView");
            }
            //Rajesh on 1/7/2014
            if (subsection.SubSectionTypeCondition.Equals("PrequalComments"))
            {
                ViewBag.Comments = entityDB.PrequalificationComments.Where(rec => rec.PrequalificationId == preQualificationID && rec.SubSectionID == subSectionId).ToList();
                return PartialView("CommentsPartialView");
            }
            if (subsection.SubSectionTypeCondition.Equals(LocalConstants.SubSectionTypeCondition.PQDocuments2))
            {
                ViewBag.pqid = preQualificationID;
                //ViewBag.PQDocuments = _prequalification.getPQDocuments(preQualificationID);
                return PartialView("PQDocumentsPartialView");
            }
            //<<<Ends
            return Json("");
        }

        public void ConfigurePQSites(long PQId)
        {
            IEnumerable<PQBusinessUnitSites> PQSites = _pqSites.GetPQSites(PQId, SSession.UserId);

            var sitesInfo = PQSites;
            ViewBag.siteInfo = sitesInfo;
            ViewBag.PrequalificationStatus = _prequalification.GetPQStatusId(PQId);
        }
        public ActionResult GetPQSites(long PQId)
        {
            ViewBag.isUsedForStatus = true;
            ConfigurePQSites(PQId);
            GetPqStatuses();
            var finalstatus = _prequalification.Finalstatus();
            ViewBag.finalstatus = finalstatus;
            ViewBag.IsVendorDetails = false;
            return PartialView("PrequalificationSitesPartialView");
        }
        public ActionResult GetSiteStatuses(long PQId, bool IsVendorDetails)
        {
            ConfigurePQSites(PQId);
            ViewBag.HideLinks = true;
            ViewBag.IsVendorDetails = IsVendorDetails;
            return PartialView("PrequalificationSitesPartialView");
        }
        public ActionResult GetBUDocuments(long BuId)
        {
            var buDocs = _prequalification.GetBUDocs(BuId);
            return Json(buDocs);
        }
        public ActionResult ChanngeLocationStatus(long siteid, long Status)
        {
            List<SiteStatus> form = new List<SiteStatus>();

            SiteStatus data = new SiteStatus();

            data.SiteId = siteid;
            data.Status = Status;
            form.Add(data);
            var status = _pqSites.UpdateSiteStatuses(form, SSession.UserId);

            return Json("Success", JsonRequestBehavior.AllowGet);
        }
        [OutputCache(Duration = 0)]
        public ActionResult PrequalificationSitesPartialView()
        {
            return PartialView();
        }

        [OutputCache(Duration = 0)]
        public ActionResult SupportedDocumentsPartialView()
        {
            return PartialView();
        }

        [OutputCache(Duration = 0)]
        public ActionResult CommentsPartialView()
        {
            return PartialView();
        }

        //Dev:RP DT:7/24/2013
        //Desc:-To Close Dynamic popup it may be PrequalificationSites or any..
        public ActionResult PopUpCloseView(long subSectionId)
        {
            ViewBag.subSectionId = subSectionId;
            return View();
        }
        public ActionResult GetPQSiteStatus(long PQSiteId)
        {
            return Json(_pqSites.GetSiteStatuses(PQSiteId), JsonRequestBehavior.AllowGet);
        }
        public string ChangeDocumentName(Guid DocumentId, string DocName)
        {
            try
            {
                var res = _prequalification.ChangeDocumentName(DocumentId, DocName);
                _documents.RecordLog(DocumentId, SSession.UserId, DocumentModificationType.Modified.ToString());
                return res;
            }
            catch (Exception ee)
            {
                return Resources.Resources.Common_unableToProcess;
            }
        }
        public string ValidateForm(string fieldwithvalues)
        {


            foreach (var values in fieldwithvalues.Split('|'))
            {
                if (values == "")
                    continue;
                string[] questionID_Val = values.Split('^');
                if (!string.IsNullOrEmpty(questionID_Val[1])) continue;
                if (questionID_Val[0] != "")
                {
                    var questioncolid = Convert.ToInt64(questionID_Val[0]);
                    var isMandatory = entityDB.QuestionColumnDetails.Find(questioncolid).Questions.IsMandatory;

                    if (isMandatory == true)
                    {
                        return "Please fill all required information";
                    }
                }
            }
            return "";
        }
        //DV:RP DT:6/21/2013   10-02-2013
        //Purpose:  To submit form(TemplateSection)
        [HttpPost]
        //Type >>> null or 0= Save And Continue 1-Only Save
        // Code added on 7/1/2014 for Client Signature, it works only when the section is created using the template tool
        // Does not work in copy template, it has to be recreated.
        public String SubmitFormData(String fieldsWithValues, long SectionId, long PreQualificationId, int? type,
                decimal? paymentAmt)

        //This filedsWithValues contains QuestionColumnId ^ value ^ Insertion type(Insertion table)|QuestionColumnId ^ value Insertion type(Insertion table) etc..
        {
            var hideSectionsToDisplay = entityDB.PrequalificationSectionsToDisplay.Where(r => r.PrequalificationId == PreQualificationId && r.Visible == true).Select(r => r.TemplateSectionId);
            //if (type == 0)
            //{
            //    var formvalidate = ValidateForm(fieldsWithValues);
            //    if (!string.IsNullOrEmpty(formvalidate))
            //        return formvalidate;
            //}
            var UserId = SSession.UserId;

            if (type == 0 && SSession.Role.Equals(OrganizationType.Vendor) && _prequalification.HasFinancialAnalytics(SectionId, UserId) == true)
            {
                var dataexist = _prequalification.HasFinancialData(PreQualificationId);
                if (!dataexist)
                {
                    return "please fill FinancialData";
                }
            }
            cntr = 0;
            if (Session["UserId"] == null)
            {
                return "Session Has Expired";
            }
            String result = "Success";

            bool isBiddingDeleted = false;
            var currentPreQualification = entityDB.Prequalification.Find(PreQualificationId);

            // Kiran on 8/5/2014
            if (Session["RoleName"].ToString().Equals("Vendor") || Session["RoleName"].ToString().Equals("Admin"))//If Vendor Login
            {
                if (entityDB.TemplateSections.Find(SectionId).TemplateSectionType == 4)
                {
                    var template = entityDB.TemplateSections.Find(SectionId).Templates;
                    var selectedClients = new List<long>() { currentPreQualification.ClientId }; try
                    {
                        if (template.IsERI)
                            selectedClients.AddRange(_prequalification.GetPrequalificationClient(PreQualificationId).ClientIdsList);
                    }
                    catch { }
                    var mandatoryDocumentsOfCurrentTemplate = entityDB.ClientTemplates.Where(record => record.TemplateID == template.TemplateID && selectedClients.Contains(record.ClientID)).SelectMany(r => r.ClientTemplateReportingData).Where(record => record.DocIsMandatory == true && record.ReportingType == 2).ToList();
                    var vendorUploadedDocumentIds = entityDB.Document.Where(documents => documents.ReferenceType == "PreQualification" && documents.ReferenceRecordID == currentPreQualification.PrequalificationId && (documents.DocumentStatus != "Deleted" || documents.DocumentStatus == null)).Select(rec => new { rec.DocumentTypeId, rec.ClientId }).ToList();

                    var errorString = "";

                    foreach (var mandatoryDocument in mandatoryDocumentsOfCurrentTemplate)
                    {
                        if (vendorUploadedDocumentIds.Any(r => r.DocumentTypeId == mandatoryDocument.ReportingTypeId && (r.ClientId == null || mandatoryDocument.ClientTemplates.ClientID == r.ClientId)))
                        {
                            continue;
                        }
                        else
                        {
                            var mandatoryDocName = entityDB.DocumentType.FirstOrDefault(record => record.DocumentTypeId == mandatoryDocument.ReportingTypeId).DocumentTypeName;
                            errorString = errorString + "\n " + mandatoryDocName + (template.IsERI ? " - " + mandatoryDocument.ClientTemplates.Organizations.Name : "");
                        }
                    }
                    if (errorString != "")
                    {
                        return "Following documents have to be uploaded to proceed further:" + "\n" + errorString;
                    }
                }

                // Kiran on 12/6/2014

                var prequalificationSites = entityDB.PrequalificationSites.Where(rec => rec.PrequalificationId == PreQualificationId).ToList();
                var pqSiteClients = prequalificationSites.Select(r => r.ClientId).Distinct().ToList();
                if (entityDB.TemplateSections.Find(SectionId).TemplateSectionType == 7)
                {
                    var clientids = new List<long>() { currentPreQualification.ClientId };
                    if (currentPreQualification.Client.OrganizationType == OrganizationType.SuperClient)
                    {
                        var pqClients = _prequalification.GetPrequalificationClient(PreQualificationId);
                        clientids = pqClients == null ? new List<long>() : pqClients.ClientIdsList.ToList();
                    }

                    if (pqSiteClients == null || pqSiteClients.Count == 0 || pqSiteClients.Count < clientids.Count)
                    {
                        if (type == 0)
                            return "Please add at least one location for each client you selected in the General Information section";
                    }
                    // Mani on 8/19/2015
                    else
                    {
                        //update paymentid for after added sites
                        var paidBuIds = entityDB.PrequalificationSites.Where(rec => rec.PrequalificationId == PreQualificationId && rec.PrequalificationPaymentsId != null).Select(rec => rec.ClientBusinessUnitId).Distinct().ToList();
                        var addedSites = entityDB.PrequalificationSites.Where(rec => rec.PrequalificationId == PreQualificationId && (rec.PrequalificationPaymentsId == null || rec.PrequalificationPayments.PaymentCategory == 0)).ToList();
                        foreach (var site in addedSites)
                        {
                            if (paidBuIds.Contains(site.ClientBusinessUnitId))
                            {
                                var paymentid = entityDB.PrequalificationSites.FirstOrDefault(rec => rec.ClientBusinessUnitId == site.ClientBusinessUnitId && rec.PrequalificationId == PreQualificationId && rec.PrequalificationPaymentsId != null).PrequalificationPaymentsId;
                                site.PrequalificationPaymentsId = paymentid;
                                entityDB.Entry(site).State = EntityState.Modified;
                                entityDB.SaveChanges();
                            }
                        }
                        //Ends>>>
                        var sitesToPay = entityDB.PrequalificationSites.Where(rec => rec.PrequalificationId == PreQualificationId && (rec.PrequalificationPaymentsId == null || rec.PrequalificationPayments.PaymentCategory == 0)).ToList();
                        List<ClientTemplatesBUGroupPrice> pricesToBePaid = new List<ClientTemplatesBUGroupPrice>();
                        //Kiran on 24th Feb 2015
                        var bunitIds = prequalificationSites.Select(rec => rec.ClientBusinessUnitId).Distinct().ToList();

                        //var templatesForBU = entityDB.ClientTemplatesForBU.Where(rec => bunitIds.Contains(rec.ClientBusinessUnitId) && rec.ClientTemplateId == currentPreQualification.ClientTemplateId && rec.ClientTemplatesBUGroupPrice.GroupingFor == 0).Select(rec => rec.ClientTemplateBUGroupId).ToList(); // Kiran on 06/16/2015
                        if (sitesToPay.Count() != 0)
                        {
                            var clienttemplates = currentPreQualification.ClientTemplates.Templates.ClientTemplates.Select(r => r.ClientTemplateID).ToList();
                            var buIds = sitesToPay.Select(rec => rec.ClientBusinessUnitId).Distinct().ToList();
                            var templatesForBuToPay = entityDB.ClientTemplatesForBU.Where(rec => buIds.Contains(rec.ClientBusinessUnitId) && rec.ClientTemplateId != null && clienttemplates.Contains((Guid)rec.ClientTemplateId) && rec.ClientTemplatesBUGroupPrice.GroupPrice != 0).Select(rec => rec.ClientTemplateBUGroupId).ToList(); // Kiran on 06/16/2015 // sumanth on 10/19/2015 for FVOS-92
                            pricesToBePaid = entityDB.ClientTemplatesBUGroupPrice.Where(rec => templatesForBuToPay.Contains(rec.ClientTemplateBUGroupId) && rec.GroupingFor == 0 && rec.GroupPriceType > 0).ToList();
                        }
                        // for annual payment
                        //var preqsitesBus = entityDB.PrequalificationSites.Where(rec => rec.PrequalificationId == PreQualificationId).Select(rec => rec.ClientBusinessUnitId).ToList();
                        var annualFeeGroups = entityDB.PrequalificationTrainingAnnualFees.Where(rec => rec.PrequalificationId == PreQualificationId).Select(rec => rec.ClientTemplateBUGroupId).ToList();
                        var clientTemplatesForBu = entityDB.ClientTemplatesForBU.Where(rec => rec.ClientTemplateId == currentPreQualification.ClientTemplateId && bunitIds.Contains(rec.ClientBusinessUnitId) && rec.ClientTemplatesBUGroupPrice.GroupingFor == 1).ToList();
                        var groupIds = clientTemplatesForBu.Select(rec => rec.ClientTemplateBUGroupId).Distinct().ToList();
                        var templateBuId = clientTemplatesForBu.Select(rec => rec.ClientTemplateForBUId).Distinct().ToList();
                        var annualBuPricingGroups = entityDB.ClientTemplatesBUGroupPrice.Where(rec => rec.ClientTemplateID == currentPreQualification.ClientTemplateId && rec.GroupingFor == 1 && rec.GroupPrice != 0 && groupIds.Contains(rec.ClientTemplateBUGroupId)).ToList();
                        var toPayForAnuual = entityDB.ClientTemplatesBUGroupPrice.Where(rec => rec.ClientTemplateID == currentPreQualification.ClientTemplateId && rec.GroupingFor == 1 && rec.GroupPrice != 0 && groupIds.Contains(rec.ClientTemplateBUGroupId) && !annualFeeGroups.Contains(rec.ClientTemplateBUGroupId)).ToList();

                        // Making payments section as complete or open the section
                        var paymentSection = entityDB.PrequalificationCompletedSections.FirstOrDefault(rec => rec.PrequalificationId == PreQualificationId && rec.TemplateSections.TemplateSectionType == 3);
                        if (pricesToBePaid != null && pricesToBePaid.Count() > 0 || (toPayForAnuual != null && toPayForAnuual.Count() > 0))
                        {
                            if (paymentSection != null)
                            {
                                entityDB.PrequalificationCompletedSections.Remove(paymentSection);
                            }
                        }
                        else if (paymentSection == null && currentPreQualification.PaymentReceived == true)
                        {
                            PrequalificationCompletedSections newPaymentSec = new PrequalificationCompletedSections
                            {
                                TemplateSectionId =
                                    currentPreQualification.ClientTemplates.Templates.TemplateSections.FirstOrDefault(
                                        rec => rec.TemplateSectionType == 3).TemplateSectionID,
                                PrequalificationId = PreQualificationId,
                                SubmittedDate = DateTime.Now,
                                SubmittedBy = SSession.UserId
                            };
                            entityDB.PrequalificationCompletedSections.Add(newPaymentSec);
                            currentPreQualification.PaymentReceived = true;
                        }
                        entityDB.SaveChanges();

                        // after annual payent if he add bu in the paid group insert records as payment completed.
                        foreach (var templateBu in clientTemplatesForBu)
                        {
                            if (entityDB.PrequalificationTrainingAnnualFees.FirstOrDefault(rec => rec.ClientTemplateForBUId == templateBu.ClientTemplateForBUId && rec.PrequalificationId == PreQualificationId) == null)
                            {
                                var preqAnnualFee = entityDB.PrequalificationTrainingAnnualFees.FirstOrDefault(rec => rec.ClientTemplateBUGroupId == templateBu.ClientTemplateBUGroupId && rec.PrequalificationId == PreQualificationId);
                                if (preqAnnualFee != null)
                                {
                                    PrequalificationTrainingAnnualFees annualFeeRec = new PrequalificationTrainingAnnualFees();
                                    annualFeeRec.AnnualFeeAmount = 0;
                                    annualFeeRec.AnnualFeeReceivedAmount = 0;
                                    annualFeeRec.ClientTemplateBUGroupId = preqAnnualFee.ClientTemplateBUGroupId;
                                    annualFeeRec.ClientTemplateForBUId = templateBu.ClientTemplateForBUId;
                                    annualFeeRec.FeeReceivedDate = DateTime.Now;
                                    annualFeeRec.PrequalificationId = preqAnnualFee.PrequalificationId;
                                    annualFeeRec.FeeTransactionId = preqAnnualFee.FeeTransactionId;
                                    annualFeeRec.FeeTransactionMetaData = preqAnnualFee.FeeTransactionMetaData;
                                    annualFeeRec.SkipPayment = preqAnnualFee.SkipPayment;
                                    entityDB.PrequalificationTrainingAnnualFees.Add(annualFeeRec);
                                    entityDB.SaveChanges();
                                }
                            }
                        }
                        // Ends >>>

                        // Kiran on 02/24/2015
                        decimal? totalAmountToPay = 0.0M;
                        //var totalInvoiceAmountToPayForPrequalSites = entityDB.ClientTemplatesBUGroupPrice.Where(rec => templatesForBU.Contains(rec.ClientTemplateBUGroupId) && rec.GroupingFor == 0 && rec.GroupPriceType != 0).ToList();
                        var defaultPriceForSelectedTemplate = 0.0M;
                        try
                        {
                            defaultPriceForSelectedTemplate = (decimal)entityDB.ClientTemplatesBUGroupPrice.FirstOrDefault(rec => rec.ClientTemplateID == currentPreQualification.ClientTemplateId && rec.GroupPriceType == 0 && rec.GroupingFor == 0).GroupPrice;

                        }
                        catch
                        {

                        }
                        var prequalSitesBusinessUnitsClientTemplatesForBU = entityDB.ClientTemplatesForBU.Where(rec => bunitIds.Contains(rec.ClientBusinessUnitId) && rec.ClientTemplateId == currentPreQualification.ClientTemplateId && rec.ClientTemplatesBUGroupPrice.GroupingFor == 0 && rec.ClientTemplatesBUGroupPrice.GroupPriceType != 0).ToList();
                        if (prequalSitesBusinessUnitsClientTemplatesForBU != null && prequalSitesBusinessUnitsClientTemplatesForBU.Count() > 0)
                        {

                            foreach (var businessUnitTemplateBU in prequalSitesBusinessUnitsClientTemplatesForBU)
                            {
                                if (businessUnitTemplateBU.ClientTemplatesBUGroupPrice.GroupPriceType > 0)
                                    totalAmountToPay += businessUnitTemplateBU.ClientTemplatesBUGroupPrice.GroupPrice;
                            }
                        }
                        totalAmountToPay += defaultPriceForSelectedTemplate;



                        if (totalAmountToPay != currentPreQualification.TotalInvoiceAmount || annualBuPricingGroups.Count() > 0)
                        {
                            currentPreQualification.TotalInvoiceAmount = totalAmountToPay;
                            currentPreQualification.TotalAnnualFeeAmount = (decimal)annualBuPricingGroups.Sum(rec => rec.GroupPrice);
                            entityDB.Entry(currentPreQualification).State = EntityState.Modified;
                            //var paymentSection = entityDB.PrequalificationCompletedSections.FirstOrDefault(rec => rec.PrequalificationId == PreQualificationId && rec.TemplateSections.TemplateSectionType == 3);
                            //entityDB.PrequalificationCompletedSections.Remove(paymentSection);
                            entityDB.SaveChanges();

                        }
                        // Ends<<<
                    }
                }
                // Ends<<<
            }
            // Ends<<<

            var currentSection = entityDB.TemplateSections.Find(SectionId); // Rajesh on 7/1/2014
            //Rajesh on 12/30/2013
            if (entityDB.TemplateSections.Find(SectionId).TemplateSectionType == 99 && type != 3)
            {
                currentPreQualification.UserIdAsCompleter = new Guid(fieldsWithValues);
                currentPreQualification.UserIdLoggedIn = (Guid)Session["UserId"];
                entityDB.Entry(currentPreQualification).State = EntityState.Modified;

                entityDB.SaveChanges();
            }
            else
            {//Ends<<<
                try
                {
                    List<string> keywordList = new List<string>(); // Sandhya on 04/11/2015
                    bool isFirstInsertion_ForMPA = true; // Rajesh on 7/1/2014

                    //var preUserInputs = entityDB.PrequalificationUserInput.Where(rec => rec.PreQualificationId == PreQualificationId).ToList();
                    foreach (var record in fieldsWithValues.Split('|'))
                    {
                        // Insertion into PrequalicationUserInput table >>>
                        string[] questionID_Val = record.Split('^');//0-QuestionColumn Id,1-question Val,2-Insertion Type(Inserted table name),3-QuestionReferenceId
                        if (questionID_Val.Length == 4)
                        {
                            if (questionID_Val[2].StartsWith("ERIClient"))
                            {
                                var PQClient = entityDB.PrequalificationClient.FirstOrDefault(r => r.PQId == PreQualificationId);
                                if (PQClient == null)
                                    PQClient = new PrequalificationClient()
                                    {
                                        PQId = PreQualificationId
                                    };
                                if (questionID_Val[2].Equals("ERIClientRegion"))
                                {
                                    PQClient.Region = questionID_Val[1];
                                }
                                else
                                {
                                    var lockedClients = _clientBusiness.GetPQClientsInSites(PreQualificationId) ?? new List<KeyValuePair<long, string>>();
                                    var ids = string.Join(",", lockedClients.Select(r => r.Key));
                                    PQClient.ClientIds = questionID_Val[1];
                                    if (!string.IsNullOrEmpty(ids))
                                        PQClient.ClientIds += "," + ids;
                                    PQClient.ClientIds = string.Join(",", PQClient.ClientIds.Split(',').Where(r => r != "null" && r != ""));
                                }
                                entityDB.Entry(PQClient).State = PQClient.Id == 0 ? EntityState.Added : EntityState.Modified;
                                entityDB.SaveChanges();
                                continue;
                            }
                            if (questionID_Val[2] == "PQQuestions")
                            //These questions are belongs to the perticular prequalification not a template questions.These questions will change Pq to PQ(Addendum)
                            {
                                var PqQues = entityDB.PrequalificationQuestion.Find(Int64.Parse(questionID_Val[0]));
                                PqQues.QuestionInputValue = questionID_Val[1];
                                if (SSession.Role.Equals(OrganizationType.Vendor))
                                {
                                    var pqSubSec =
                                        entityDB.PrequalificationSubsection.FirstOrDefault(
                                            r =>
                                                r.SubSectionId == PqQues.PrequalificationSubsection.SubSectionId &&
                                                r.GroupNumber == PqQues.PrequalificationSubsection.GroupNumber &&
                                                r.SubSectionType == "ClientSign");
                                    if (PqQues.QuestionText.Equals("Date:"))
                                    {

                                        if (pqSubSec != null && pqSubSec.PrequalificationQuestion.Any())
                                        {
                                            var clientQues =
                                                pqSubSec.PrequalificationQuestion.FirstOrDefault(
                                                    r => r.QuestionText == PqQues.QuestionText);
                                            clientQues.QuestionInputValue = PqQues.QuestionInputValue;
                                        }
                                    }
                                    else
                                    {
                                        if (pqSubSec != null && pqSubSec.PrequalificationQuestion.Any())
                                        {
                                            var clientQues =
                                                pqSubSec.PrequalificationQuestion.FirstOrDefault(
                                                    r => r.QuestionText == PqQues.QuestionText);
                                            var currentSecQuestioncolumns =
                                                pqSubSec.TemplateSubSections.TemplateSections.TemplateSubSections
                                                    .SelectMany(r => r.Questions).SelectMany(
                                                        r => r.QuestionColumnDetails).Select(r => r.QuestionColumnId);

                                            var clientUserSign =
                                                entityDB.ClientUserSignature.FirstOrDefault(
                                                    r =>
                                                        r.QuestionText == PqQues.QuestionText &&
                                                        currentSecQuestioncolumns.Contains(r.QuestionColumnId));
                                            if (clientUserSign != null)
                                                clientQues.QuestionInputValue = clientUserSign.QuestionColumnIdValue;
                                        }

                                    }
                                }
                                continue;
                            }




                            bool isInsert = false;

                            long QuestionColumnId = Convert.ToInt64(questionID_Val[0]);
                            var QuestionColumn = entityDB.QuestionColumnDetails.Find(QuestionColumnId);

                            //sandhya 04/11/2015
                            var reportForKeyword = entityDB.Questions.Find(QuestionColumn.QuestionId).ReportForKeyword;
                            if (!string.IsNullOrEmpty(reportForKeyword) && !keywordList.Contains(reportForKeyword))
                            {
                                keywordList.Add(reportForKeyword);
                            }
                            // Ends<<<

                            PrequalificationUserInput preUserInput_NewRecord;
                            //To get existing record
                            //RAB trying to make this more efficient by reducitn calls
                            //preUserInput_NewRecord = entityDB.PrequalificationUserInput.FirstOrDefault(rec => rec.PreQualificationId == PreQualificationId && rec.QuestionColumnId == QuestionColumnId);
                            preUserInput_NewRecord = entityDB.PrequalificationUserInput.FirstOrDefault(rec => rec.QuestionColumnId == QuestionColumnId && rec.PreQualificationId == PreQualificationId);

                            //if no record found
                            if (preUserInput_NewRecord == null)
                            {
                                isInsert = true;
                                preUserInput_NewRecord = new PrequalificationUserInput();
                            }

                            preUserInput_NewRecord.PreQualificationId = PreQualificationId;
                            preUserInput_NewRecord.QuestionColumnId = QuestionColumnId;
                            preUserInput_NewRecord.UserInput = questionID_Val[1].Trim();
                            // Rajesh on 7/1/2014





                            if (isInsert)
                            {

                                if (String.IsNullOrWhiteSpace(questionID_Val[1]))
                                    continue;
                                entityDB.PrequalificationUserInput.Add(preUserInput_NewRecord);
                                // Ends<<<
                            }
                            else
                                entityDB.Entry(preUserInput_NewRecord).State = EntityState.Modified;
                            // Insertion Ends <<<
                            //================================

                            //In addition to user input we are inserting data into related tables viz. YearWiseStats, PrequalificationReportingData etc.
                            if (questionID_Val[2].Equals("YearWiseStatsInsertion"))
                            {
                                if (QuestionColumn.Questions.QuestionBankId == 1000)//If It is EMR Years Related Question
                                {
                                    PrequalificationEMRStatsYears EMRYears = entityDB.PrequalificationEMRStatsYears.FirstOrDefault(rec => rec.PrequalificationId == PreQualificationId && rec.ColumnNo == QuestionColumn.ColumnNo);
                                    if (EMRYears == null)
                                    {
                                        EMRYears = new PrequalificationEMRStatsYears();
                                        EMRYears.PrequalificationId = PreQualificationId;
                                        EMRYears.ColumnNo = QuestionColumn.ColumnNo;
                                        EMRYears.EMRStatsYear = questionID_Val[1].Trim();
                                        entityDB.PrequalificationEMRStatsYears.Add(EMRYears);
                                    }
                                    else
                                    {
                                        EMRYears.EMRStatsYear = questionID_Val[1].Trim();
                                        entityDB.Entry(EMRYears).State = EntityState.Modified;
                                    }
                                    entityDB.SaveChanges();//We are doing this partial save bcz of childs exist
                                }
                                else
                                {
                                    PrequalificationEMRStatsYears EMRYears = entityDB.PrequalificationEMRStatsYears.FirstOrDefault(rec => rec.PrequalificationId == PreQualificationId && rec.ColumnNo == QuestionColumn.ColumnNo);
                                    if (EMRYears == null || QuestionColumn.Questions.NumberOfColumns != 3)
                                    {
                                        //Do Nothing
                                    }
                                    else
                                    {
                                        PrequalificationEMRStatsValues EMRValues = entityDB.PrequalificationEMRStatsValues.FirstOrDefault(rec => rec.QuestionColumnId == QuestionColumn.QuestionColumnId && rec.PrequalEMRStatsYearId == EMRYears.PrequalEMRStatsYearId);
                                        if (EMRValues == null)
                                        {
                                            EMRValues = new PrequalificationEMRStatsValues();
                                            EMRValues.PrequalEMRStatsYearId = EMRYears.PrequalEMRStatsYearId;
                                            EMRValues.QuestionId = QuestionColumn.Questions.QuestionID;
                                            EMRValues.QuestionColumnId = QuestionColumn.QuestionColumnId;
                                            EMRValues.QuestionColumnIdValue = questionID_Val[1].Trim();
                                            entityDB.PrequalificationEMRStatsValues.Add(EMRValues);
                                        }
                                        else
                                        {
                                            EMRValues.QuestionColumnIdValue = questionID_Val[1].Trim();
                                            entityDB.Entry(EMRValues).State = EntityState.Modified;
                                        }
                                    }
                                }
                            }
                            else if (questionID_Val[2].Equals("BiddingManualInsertion") || questionID_Val[2].Equals("BiddingInsertion") || questionID_Val[2].Equals("ProficiencyInsertion") || questionID_Val[2].Equals("ClaycoBiddingInsertion"))
                            {
                                if (!isBiddingDeleted)
                                {
                                    var reportingType = 0;
                                    if (questionID_Val[2].Equals("ProficiencyInsertion"))
                                        reportingType = 1;
                                    if (questionID_Val[2].Equals("ClaycoBiddingInsertion"))
                                        reportingType = 3;
                                    if (questionID_Val[2].Equals("ProductCategoriesInsertion"))
                                        reportingType = 4;
                                    var ReportingDatalist =
                                        entityDB.PrequalificationReportingData.Where(
                                            rec =>
                                                rec.PrequalificationId == PreQualificationId &&
                                                rec.ReportingType == reportingType).ToList();
                                    foreach (var reportData in ReportingDatalist)
                                    {
                                        entityDB.PrequalificationReportingData.Remove(reportData);
                                    }
                                    entityDB.SaveChanges();
                                    isBiddingDeleted = true;
                                }
                                if (String.IsNullOrWhiteSpace(questionID_Val[1]))
                                    continue;
                                else
                                {
                                    foreach (var ids in questionID_Val[1].Split(','))
                                    {
                                        PrequalificationReportingData data = new PrequalificationReportingData();
                                        data.PrequalificationId = PreQualificationId;
                                        data.ClientId = currentPreQualification.ClientId;
                                        data.VendorId = currentPreQualification.VendorId;
                                        if (questionID_Val[2].Equals("ProficiencyInsertion"))
                                            data.ReportingType = 1;
                                        else if (questionID_Val[2].Equals("ClaycoBiddingInsertion"))
                                            data.ReportingType = 3;
                                        else if (questionID_Val[2].Equals("ProductCategoriesInsertion"))
                                            data.ReportingType = 4;
                                        else
                                            data.ReportingType = 0;//Bidding intrest

                                        //ids is user input---incase of BiddingManualInsertion we get dropdown value
                                        //in case of BiddingInsertion/Proficiency insertion we get Yes,no,Not applicable
                                        if (questionID_Val[2].Equals("BiddingManualInsertion") || questionID_Val[2].Equals("ClaycoBiddingInsertion") ||
                                            questionID_Val[2].Equals("ProductCategoriesInsertion"))//It contains only dropdowns
                                        {
                                            long RId = -1;
                                            if (!long.TryParse(ids, out RId)) continue; //Convert.ToInt64(ids);
                                            if (entityDB.PrequalificationReportingData.FirstOrDefault(rec => rec.PrequalificationId == PreQualificationId && rec.ReportingType == 0 && rec.ReportingDataId == RId) != null)
                                                continue;
                                            data.ReportingDataId = RId;
                                        }
                                        else//It contains only Radio Buttons
                                        {
                                            long a;
                                            if (!ids.ToLower().Trim().Equals("yes") || !long.TryParse(questionID_Val[3], out a))
                                                continue;
                                            data.ReportingDataId = a; //Convert.ToInt64(questionID_Val[3]);
                                        }

                                        entityDB.PrequalificationReportingData.Add(data);
                                    }
                                }
                            }

                            entityDB.SaveChanges();
                        }

                    }
                    var clientTemplateID = entityDB.Prequalification.Where(rec => rec.PrequalificationId == PreQualificationId).FirstOrDefault().ClientTemplateId;
                    var templateID = entityDB.ClientTemplates.Where(rec => rec.ClientTemplateID == clientTemplateID.Value).FirstOrDefault().TemplateID;
                    //Sandhya 04/11/2015---Start
                    foreach (var key in keywordList) // Sandhya on 04/21/2015 Modified while updating section
                    {
                        if (key.ToLower().Equals("vendordetails"))
                        {
                            var GetVendordetails = entityDB.VendorDetails.Where(rec => rec.PrequalificationId == PreQualificationId).ToList();
                            if (GetVendordetails != null)
                            {
                                foreach (var Onerecord in GetVendordetails)
                                {
                                    entityDB.VendorDetails.Remove(Onerecord);
                                    entityDB.SaveChanges();
                                }
                            }
                        }

                    }

                    foreach (var key in keywordList)
                    {
                        //var questionclo = entityDB.QuestionColumnDetails.Where(rec => rec.Questions.ReportForKeyword == key).GroupBy(rec => rec.Questions.GroupAsOneRecord).ToList();
                        //var questionclo = entityDB.QuestionColumnDetails
                        //    .Where(rec => rec.Questions.ReportForKeyword == key 
                        //        && rec.Questions.TemplateSubSections.TemplateSections.TemplateID == templateID
                        //        && rec.PrequalificationUserInputs.Any(pqi => pqi.PreQualificationId == PreQualificationId )).GroupBy(rec => rec.Questions.GroupAsOneRecord).ToList();

                        var questionclo = helper.GetQuestionColumnData(PreQualificationId, key);

                        //var questions = entityDB.Questions.Where(rec => rec.ReportForKeyword == key).ToList();
                        List<string> myCollection = new List<string>();
                        foreach (var questionsrecord in questionclo)
                        {
                            //sumanth on 08/12/2015
                            if (key.ToLower().Equals("vendordetails"))
                            {
                                VendorDetails vendor = new VendorDetails();

                                foreach (var questions in questionsrecord.Questions)
                                {
                                    long prequalificationId = 0;
                                    //var questionColumID = questions.QuestionColumnId;
                                    //var questionText = questions.Questions.QuestionText.Replace(":", "").Replace(" ", "").Trim();
                                    //var userInput = "";
                                    //if (questions.PrequalificationUserInputs != null && questions.PrequalificationUserInputs.Count() > 0)
                                    //{
                                    //    userInput = questions.PrequalificationUserInputs[0].UserInput;
                                    //    prequalificationId = questions.PrequalificationUserInputs[0].PreQualificationId;
                                    //}
                                    var questionText = questions.QuestionText.Replace(":", "").Replace(" ", "").Trim();
                                    var userInput = questions.PrequalificationUserInput;

                                    //if (prequalificationId == PreQualificationId)
                                    //{
                                    if (questionText.Equals("FullName") || questionText.Equals("Nombrecompleto"))
                                        vendor.FullName = userInput;
                                    else if (questionText.Equals("Title") || questionText.Equals("Título"))
                                        vendor.Title = userInput;
                                    else if (questionText.Equals("EmailAddress") || questionText.Equals("Correoelectrónico"))
                                        vendor.EmailAddress = userInput;
                                    else if (questionText.Equals("TelephoneNumber") || questionText.Equals("Númerodeteléfono"))
                                        vendor.TelephoneNumber = userInput;
                                    //}
                                    vendor.GroupNumber = Convert.ToInt64(questionsrecord.Key); // Sandhya on 05/12/2015
                                }
                                vendor.PrequalificationId = PreQualificationId;

                                if (!string.IsNullOrEmpty(vendor.FullName) || !string.IsNullOrEmpty(vendor.Title) || !string.IsNullOrEmpty(vendor.EmailAddress) || !string.IsNullOrEmpty(vendor.TelephoneNumber))
                                    entityDB.VendorDetails.Add(vendor);
                                entityDB.SaveChanges();
                            }
                            else if (key.ToLower().Equals("clientvendorid"))
                            {
                                // sumanth on 08/12/0215

                                foreach (var questions in questionsrecord.Questions)
                                {
                                    ClientVendorReferenceIds ClientVendorRefId = new ClientVendorReferenceIds();

                                    long prequalificationId = 0;
                                    //var questionColumID = questions.QuestionColumnId;
                                    //var questionText = questions.Questions.QuestionText.Replace(":", "").Replace(" ", "").Trim();
                                    //var userInput = "";
                                    //if (questions.PrequalificationUserInputs != null && questions.PrequalificationUserInputs.Count() > 0)
                                    //{
                                    //    userInput = questions.PrequalificationUserInputs[0].UserInput;
                                    //    prequalificationId = questions.PrequalificationUserInputs[0].PreQualificationId;
                                    //}
                                    var questionText = questions.QuestionText.Replace(":", "").Replace(" ", "").Trim();
                                    var userInput = questions.PrequalificationUserInput;

                                    ClientVendorRefId = entityDB.ClientVendorReferenceIds.FirstOrDefault(rec => rec.ClientId == currentPreQualification.ClientId && rec.VendorId == currentPreQualification.VendorId);


                                    if (ClientVendorRefId == null)
                                    {
                                        ClientVendorReferenceIds ClientVendorRefId1 = new ClientVendorReferenceIds();
                                        //if (prequalificationId == PreQualificationId)
                                        //{

                                        ClientVendorRefId1.ClientId = currentPreQualification.ClientId;
                                        ClientVendorRefId1.VendorId = currentPreQualification.VendorId;
                                        if (questionText.ToLower().Equals("vendornumberassigned") || questionText.ToLower().Equals("vendorid") || questionText.ToLower().Equals("suppliernumber"))
                                            ClientVendorRefId1.ClientVendorReferenceId = userInput;
                                        else if (questionText.ToLower().Equals("contractortype") || questionText.ToLower().Equals("contractor/suppliertype"))
                                            ClientVendorRefId1.ClientContractId = Convert.ToInt64(userInput);

                                        //}
                                        //if (!string.IsNullOrEmpty(ClientVendorRefId1.ClientVendorReferenceId))
                                        if (!string.IsNullOrEmpty(userInput))
                                            entityDB.ClientVendorReferenceIds.Add(ClientVendorRefId1);
                                        entityDB.SaveChanges();

                                    }
                                    else
                                    {
                                        if (questionText.ToLower().Equals("vendornumberassigned") || questionText.ToLower().Equals("vendorid") || questionText.ToLower().Equals("suppliernumber"))
                                            ClientVendorRefId.ClientVendorReferenceId = userInput;
                                        else if ((questionText.ToLower().Equals("contractortype") || questionText.ToLower().Equals("contractor/suppliertype")) && !string.IsNullOrEmpty(userInput))
                                            ClientVendorRefId.ClientContractId = Convert.ToInt64(userInput);
                                        entityDB.Entry(ClientVendorRefId).State = EntityState.Modified;
                                        entityDB.SaveChanges();
                                    }




                                }
                            }
                        }
                    }
                    //Sandhya 04/11/2015---End

                    //To Insert MPA Client Sign

                    MPASectionInsertion(isFirstInsertion_ForMPA, currentSection, currentPreQualification);

                    //Ends<<<

                    if (type != 1)//If it is not an intrem(Only )Save
                        SendSubSectionNotification(SectionId, PreQualificationId, currentPreQualification);

                    var Tsection = entityDB.TemplateSections.Find(SectionId);
                    //var SubSection = Tsection.TemplateSubSections.FirstOrDefault(rec => rec.SubSectionTypeCondition == "CSIWebServiceResponse");
                    //if ( SubSection!= null)
                    //{
                    //    // Removed if condition on 07/31/2014
                    //    //if (entityDB.CSIWebResponseDetails.FirstOrDefault(rec => rec.PrequalificationId == PreQualificationId && SubSection.SubSectionID==rec.ResponseNeededForID) == null)
                    //        CSIWebServiceInitiateRequest(currentPreQualification,SubSection.SubSectionID);
                    //}


                }
                catch (Exception ee)
                {
                    result = "Error : " + ee.Message;
                }
            }
            //Rajesh on 11/23/2013
            if (type != null && type == 1)
                return "Saved Successfully";
            //Ends
            //Rajesh on 08/26/2013
            PrequalificationCompletedSections sectionCompleted = entityDB.PrequalificationCompletedSections.FirstOrDefault(rec => rec.PrequalificationId == PreQualificationId && rec.TemplateSectionId == SectionId);
            //var templateSection = entityDB.TemplateSections.Find(SectionId);
            //if (templateSection.TemplateSectionType == 3)
            //{
            //    var prequalification = entityDB.Prequalification.Find(PreQualificationId);
            //    // kiran on 3/17/2014
            //    if (prequalification.PaymentReceived != true)
            //    {
            //        //prequalification.PaymentReceivedAmount = paymentAmt;
            //        //prequalification.PaymentReceivedDate = DateTime.Now;
            //        prequalification.PaymentReceived = true;//In Here we are doing skip payment
            //        entityDB.Entry(prequalification).State = EntityState.Modified;
            //        entityDB.SaveChanges();
            //    }
            //    // Ends<<<
            //    prequalification.PaymentReceived = true;
            //    entityDB.Entry(prequalification).State = EntityState.Modified;
            //    entityDB.SaveChanges();
            //}

            //Ends <<<<<


            // Rajesh on 07/27/2013
            string strNextSectionURL = "";

            Prequalification preQualificationDetails = entityDB.Prequalification.Find(PreQualificationId);
            List<TemplateSections> tempSections = preQualificationDetails.ClientTemplates.Templates.TemplateSections.Where(rec => rec.Visible == true && (rec.HideFromPrequalification != true || hideSectionsToDisplay.Contains(rec.TemplateSectionID))).OrderBy(rec => rec.DisplayOrder).ToList();

            //Rajesh on 1/21/2014
            if (Session["RoleName"].ToString().Equals("Client") || SSession.Role.Equals(OrganizationType.SuperClient))
            {
                long[] prequalificationStatus = { };//Requested on FV-1012
                int status = Array.IndexOf(prequalificationStatus, preQualificationDetails.PrequalificationStatusId);
                if (status != -1)
                    tempSections = preQualificationDetails.ClientTemplates.Templates.TemplateSections.Where(rec => rec.Visible == true && (rec.HideFromPrequalification != true || hideSectionsToDisplay.Contains(rec.TemplateSectionID)) && rec.VisibleToClient == true).OrderBy(rec => rec.DisplayOrder).ToList();
                var allVisibleTemplateSectionsForClient = entityDB.TemplateSectionsPermission.Where(rec => rec.VisibleTo == 3 || rec.VisibleTo == 2).Select(rec => rec.TemplateSectionID).ToList();

                //Guid userId = new Guid(Session["UserId"].ToString());
                //var userhasRestrictedSectionAccess = FVGen3.BusinessLogic.RolesHelper.IsUserInRole(userId, "Restricted Section Access", preQualificationDetails.ClientId, preQualificationDetails.VendorId);

                //if (userhasRestrictedSectionAccess)
                //{
                allVisibleTemplateSectionsForClient.AddRange(entityDB.TemplateSectionsPermission.Where(rec => rec.VisibleTo == 4).Select(rec => rec.TemplateSectionID).ToList());
                //}

                //Rajesh on 12-08-2016 for restricted access 2>>
                var hasAccess2 = FVGen3.BusinessLogic.RolesHelper.IsUserInRole(SSession.UserId, LocalConstants.RESTRICTED_ACCESS_2_VIEW_ID, "read", preQualificationDetails.ClientId, preQualificationDetails.VendorId) || Session["RoleName"].Equals("Super Admin");
                if (hasAccess2)
                    allVisibleTemplateSectionsForClient.AddRange(entityDB.TemplateSectionsPermission.Where(rec => rec.VisibleTo == LocalConstants.SectionPermissionType.RESTRICTED_ACCESS_2).Select(rec => rec.TemplateSectionID).ToList());

                //<<
                tempSections = tempSections.Where(rec => allVisibleTemplateSectionsForClient.Contains(rec.TemplateSectionID)).ToList();
                // Ends<<<
            }
            //Ends<<<
            //Rajesh on 2/13/2014
            if (Session["RoleName"].ToString().Equals("Vendor"))//If Vendor Login
            {

                //To get all templates sections permission with visible to vendor client admin
                //var allVisibleTemplateSections = (from templateSecPermission in entityDB.TemplateSectionsPermission.Where(rec => rec.VisibleTo == 3 || rec.VisibleTo == 1).ToList()
                //                                  select templateSecPermission.TemplateSectionID).ToList();



                var allVisibleTemplateSections = entityDB.TemplateSectionsPermission.Where(rec => rec.VisibleTo == 3 || rec.VisibleTo == 1 || rec.VisibleTo == 5).Select(rec => rec.TemplateSectionID).ToList();
                Guid userId = new Guid(Session["UserId"].ToString());
                //  var userhasRestrictedSectionAccess = FVGen3.BusinessLogic.RolesHelper.IsUserInRole(userId, 1001, "read", preQualificationDetails.ClientId, preQualificationDetails.VendorId);
                ViewBag.RestrictedSectionEdit = FVGen3.BusinessLogic.RolesHelper.IsUserInRole(userId, 1001, "read", preQualificationDetails.ClientId, preQualificationDetails.VendorId);
                //if (userhasRestrictedSectionAccess)
                // {
                //     allVisibleTemplateSections.AddRange(entityDB.TemplateSectionsPermission.Where(rec => rec.VisibleTo == 4).Select(rec => rec.TemplateSectionID).ToList());
                //}

                tempSections = preQualificationDetails.ClientTemplates.Templates.TemplateSections.Where(rec => rec.Visible == true && (rec.HideFromPrequalification != true || hideSectionsToDisplay.Contains(rec.TemplateSectionID)) && allVisibleTemplateSections.Contains(rec.TemplateSectionID)).OrderBy(rec => rec.DisplayOrder).ToList();
            }
            //Ends<<<

            long longSecid = SectionId;
            try
            {
                //Rajesh on 1/21/2014
                if (tempSections.FirstOrDefault(rec => rec.TemplateSectionID == SectionId).TemplateSectionType == 99)//If It is Finalize and Submit
                {
                    NextURL(PreQualificationId, ref result, ref strNextSectionURL, preQualificationDetails, paymentAmt, SectionId);
                    sectionCompleted = MarkSectionAsCompleted(SectionId, PreQualificationId, result, sectionCompleted, tempSections, type);
                    return result;
                }
                //Ends<<
                if (tempSections.FirstOrDefault(rec => rec.TemplateSectionID == SectionId).TemplateSectionType == 34)
                {
                    MarkSectionAsClientReview(PreQualificationId);
                }
                longSecid = tempSections[tempSections.IndexOf(tempSections.FirstOrDefault(rec => rec.TemplateSectionID == SectionId)) + 1].TemplateSectionID;
                if (tempSections.FirstOrDefault(rec => rec.TemplateSectionID == longSecid).TemplateSectionType == 34)
                {
                    if (preQualificationDetails.RequestForMoreInfo == true)
                    {
                        //Do Nothing
                    }
                    else//To get next Section
                        longSecid = tempSections[tempSections.IndexOf(tempSections.FirstOrDefault(rec => rec.TemplateSectionID == longSecid)) + 1].TemplateSectionID;

                }
            }
            catch (Exception ee)
            { //Means it is last page submission
                //Rajesh on 1/21/2014
                NextURL(PreQualificationId, ref result, ref strNextSectionURL, preQualificationDetails, paymentAmt, SectionId);
                sectionCompleted = MarkSectionAsCompleted(SectionId, PreQualificationId, result, sectionCompleted, tempSections, type);
                // Ends <<<<

                return result;
            }
            strNextSectionURL = "../Prequalification/TemplateSectionsList?templateId=" + preQualificationDetails.ClientTemplates.TemplateID + "&TemplateSectionID=" + longSecid + "&PreQualificationId=" + PreQualificationId;
            if (result.Equals("Success"))
                result = strNextSectionURL;
            // Ends <<<<

            sectionCompleted = MarkSectionAsCompleted(SectionId, PreQualificationId, result, sectionCompleted, tempSections, type);
            return result;
        }

        private void MarkSectionAsClientReview(long pqId)
        {
            var currentPreQualification = entityDB.Prequalification.Find(pqId);
            if (currentPreQualification != null)
            {
                currentPreQualification.PrequalificationStatusId = 16;

                entityDB.Entry(currentPreQualification).State = EntityState.Modified;
                if (currentPreQualification.PrequalificationStatusId != 5 || currentPreQualification.PrequalificationStatusId != 16)//16 means client review
                {
                    //To add Prequalification Logs
                    PrequalificationStatusChangeLog preQualificationLog = new PrequalificationStatusChangeLog();
                    preQualificationLog.PrequalificationId = currentPreQualification.PrequalificationId;
                    //preQualificationLog.PrequalificationStatusId = 5;//prequalification_loc.PrequalificationStatusId;
                    preQualificationLog.PrequalificationStatusId = currentPreQualification.PrequalificationStatusId;//rajesh on 9/25/2014
                    preQualificationLog.StatusChangeDate = DateTime.Now;
                    preQualificationLog.StatusChangedByUser = new Guid(Session["UserId"].ToString());
                    preQualificationLog.PrequalificationStart = currentPreQualification.PrequalificationStart;
                    preQualificationLog.PrequalificationFinish = currentPreQualification.PrequalificationFinish;
                    preQualificationLog.CreatedDate = DateTime.Now;
                    entityDB.PrequalificationStatusChangeLog.Add(preQualificationLog);
                    //>>>Ends
                }


                entityDB.SaveChanges();
            }
        }
        private void MPASectionInsertion(bool isFirstInsertion_ForMPA, TemplateSections currentSection,
            Prequalification currentPreQualification)
        {
            if (isFirstInsertion_ForMPA)
            {
                isFirstInsertion_ForMPA = false;
                if (currentSection.TemplateSectionType == 33) //If it is MPA
                {
                    List<long> QuestionColumnIds = new List<long>();
                    foreach (var subSec in currentSection.TemplateSubSections.ToList())
                    {
                        foreach (var questions in subSec.Questions.ToList())
                        {
                            foreach (var qColumns in questions.QuestionColumnDetails.ToList())
                                QuestionColumnIds.Add(qColumns.QuestionColumnId);
                        }
                    }

                    var clientSigns = entityDB.ClientUserSignature.Where(rec => QuestionColumnIds.Contains(rec.QuestionColumnId));
                    foreach (var clientUserSign in clientSigns.ToList())
                    {
                        var controlType = clientUserSign.QuestionColumnDetails.QuestionControlTypeId;
                        var isInsert = true;
                        var preUserInput =
                            entityDB.PrequalificationUserInput.FirstOrDefault(
                                rec =>
                                    rec.QuestionColumnId == clientUserSign.QuestionColumnId &&
                                    rec.PreQualificationId == currentPreQualification.PrequalificationId);

                        if (preUserInput == null)
                            preUserInput = new PrequalificationUserInput();
                        else
                        {
                            isInsert = false;
                            if (!string.IsNullOrEmpty(preUserInput.UserInput))
                                continue;
                        }
                        String getRefVal = "";
                        if (clientUserSign.GetValueQuestionColumId != null)
                        {
                            try
                            {
                                getRefVal =
                                    entityDB.PrequalificationUserInput.FirstOrDefault(
                                        rec =>
                                            rec.PreQualificationId == currentPreQualification.PrequalificationId &&
                                            rec.QuestionColumnId == clientUserSign.GetValueQuestionColumId).UserInput;
                            }
                            catch
                            {
                            }
                        }
                        preUserInput.PreQualificationId = currentPreQualification.PrequalificationId;
                        preUserInput.QuestionColumnId = clientUserSign.QuestionColumnId;
                        preUserInput.UserInput = clientUserSign.QuestionColumnIdValue;
                        if (!getRefVal.Equals(""))
                            preUserInput.UserInput = getRefVal;
                        if (isInsert)
                            entityDB.PrequalificationUserInput.Add(preUserInput);
                        else
                            entityDB.Entry(preUserInput).State = EntityState.Modified;
                    }
                    entityDB.SaveChanges();
                }
            }
        }

        public ActionResult GetPrequalificationPayments(long vendorId)
        {
            var payments = helper.GetPrequalificationPayment(vendorId);
            return Json(payments);
        }
        public ActionResult GetQuizPayments(long vendorId)
        {
            var payments = helper.GetQuizPayments(vendorId);
            return Json(payments);
        }
        private void SendSubSectionNotification(long SectionId, long PreQualificationId, Prequalification currentPreQualification)
        {
            //To Send Notifications for particular sections
            //Rajesh on 2/17/2014

            //if (Session["RoleName"].ToString().Equals("Vendor") || Session["RoleName"].ToString().Equals("Client"))
            {
                var EditableSubSectionIds = new List<long>();
                var flag_SendNotification = true;
                var section = entityDB.TemplateSections.Find(SectionId);
                if (Session["RoleName"].ToString().Equals("Client"))
                {
                    flag_SendNotification = false;
                    if (section.TemplateSectionsPermission.FirstOrDefault().VisibleTo == 2)//If it is client data entry//Rajesh on 3/21/2014
                    {
                        flag_SendNotification = true;
                    }
                    else
                        foreach (var subSec in section.TemplateSubSections.ToList())
                        {
                            //If It is limited access permission then check this user has edit permission
                            try
                            {
                                if (subSec.TemplateSubSectionsPermissions.FirstOrDefault(rec => rec.PermissionFor == "Client").PermissionType == 1)
                                {
                                    var loginUserId = new Guid(Session["UserId"].ToString());
                                    if (subSec.TemplateSubSectionsPermissions.FirstOrDefault(rec => rec.PermissionFor == "Client").TemplateSubSectionsUserPermissions.FirstOrDefault(rec => rec.UserId == loginUserId).EditAccess)
                                    {
                                        flag_SendNotification = true;
                                        EditableSubSectionIds.Add(subSec.SubSectionID);
                                        //break;//If atleast 1 sub section is editable we can send notification
                                    }
                                }
                            }
                            catch { }
                        }
                }

                var SubSections = section.TemplateSubSections.ToList();
                if (Session["RoleName"].ToString().Equals("Client") && section.TemplateSectionsPermission.FirstOrDefault().VisibleTo != 2)//Rajesh on 3/21/2014
                    SubSections = section.TemplateSubSections.Where(rec => EditableSubSectionIds.Contains(rec.SubSectionID)).ToList();

                if (flag_SendNotification)
                    foreach (var subSec in SubSections)
                    {
                        var sendNotification = true;
                        if (subSec.NotificationExist == true)
                        {

                            //foreach (var Question in subSec.Questions.Where(rec => rec.IsMandatory == true).ToList())
                            //{
                            //    if (sendNotification)
                            //        foreach (var QColumn in Question.QuestionColumnDetails.ToList())
                            //        {
                            //            if (entityDB.PrequalificationUserInput.FirstOrDefault(rec => rec.PreQualificationId == PreQualificationId && rec.QuestionColumnId == QColumn.QuestionColumnId) == null)
                            //            {
                            //                sendNotification = false;
                            //                break;
                            //            }
                            //        }
                            //}
                            if (sendNotification == false)
                                break;



                            var notification = subSec.TemplateSubSectionsNotifications.FirstOrDefault();


                            if (notification != null)
                                if (entityDB.PrequalificationNotifications.FirstOrDefault(rec => rec.PrequalificationId == PreQualificationId && rec.TemplateSubSectionNotificationId == notification.TemplateSubSectionNotificationId) == null)
                                {

                                    var template = entityDB.EmailTemplates.Find(notification.EmaiTemplateId);

                                    var subject = template.EmailSubject;
                                    var body = template.EmailBody;
                                    var Comment = template.CommentText;
                                    var clientEmail = "";
                                    var strMessage = "";
                                    var emailsToSent = ""; // Kiran on 9/23/2014
                                    var clientSysUserOrgs = currentPreQualification.Client.SystemUsersOrganizations;
                                    foreach (var clientUserOrg in clientSysUserOrgs)
                                    {
                                        var clientOrgRoles = clientUserOrg.SystemUsersOrganizationsRoles;
                                        foreach (var clientOrgRole in clientOrgRoles)
                                        {
                                            if (clientOrgRole.SystemRoles.RoleName.Equals("Super User"))
                                            {
                                                clientEmail = clientUserOrg.SystemUsers.Email;
                                            }
                                        }
                                    }
                                    if (currentPreQualification.ClientTemplates.Templates.IsERI)
                                    {
                                        try
                                        {
                                            var subSecPermission = subSec.TemplateSubSectionsPermissions.FirstOrDefault(rec => rec.PermissionFor == "Client").
                                                TemplateSubSectionsUserPermissions;
                                            var clientOrg = subSecPermission.FirstOrDefault().SystemUsers.SystemUsersOrganizations.FirstOrDefault().
                                                Organizations.OrganizationID;
                                            var clientOrgSuperUser = _organization.GetSuperuser(clientOrg);
                                            var clientInfo = _organization.GetOrganization(clientOrg);

                                            subject = subject.Replace("[ClientName]", clientInfo.Name);
                                            subject = subject.Replace("[CustomerServicePhone]", clientInfo.PhoneNumber);
                                            subject = subject.Replace("[CustomerServiceEmail]", clientOrgSuperUser.Email);

                                            body = body.Replace("[ClientName]", clientInfo.Name);
                                            body = body.Replace("[CustomerServicePhone]", clientInfo.PhoneNumber);
                                            body = body.Replace("[CustomerServiceEmail]", clientOrgSuperUser.Email);
                                        }
                                        catch
                                        {
                                            subject = subject.Replace("[ClientName]", Resources.Resources.ERI);
                                            subject = subject.Replace("[CustomerServicePhone]", Resources.Resources.ERI);
                                            subject = subject.Replace("[CustomerServiceEmail]", Resources.Resources.ERI);

                                            body = body.Replace("[ClientName]", Resources.Resources.ERI);
                                            body = body.Replace("[CustomerServicePhone]", Resources.Resources.ERI);
                                            body = body.Replace("[CustomerServiceEmail]", Resources.Resources.ERI);
                                        }
                                    }
                                    else
                                    {
                                        subject = subject.Replace("[ClientName]", currentPreQualification.Client.Name);
                                        subject = subject.Replace("[CustomerServicePhone]", currentPreQualification.Client.PhoneNumber);
                                        subject = subject.Replace("[CustomerServiceEmail]", clientEmail);

                                        body = body.Replace("[ClientName]", currentPreQualification.Client.Name);
                                        body = body.Replace("[CustomerServicePhone]", currentPreQualification.Client.PhoneNumber);
                                        body = body.Replace("[CustomerServiceEmail]", clientEmail);

                                    }
                                    subject = subject.Replace("[Date]", DateTime.Now + "");
                                    subject = subject.Replace("[VendorName]", currentPreQualification.Vendor.Name);


                                    body = body.Replace("[Date]", DateTime.Now + "");
                                    body = body.Replace("[VendorName]", currentPreQualification.Vendor.Name);

                                    // Kiran on 9/23/2014



                                    var subSectionNotificationsToSent = subSec.TemplateSubSectionsNotifications.Where(rec => rec.NotifyUserDisabled != true).ToList();
                                    if (subSectionNotificationsToSent != null)
                                    {

                                        foreach (var notificationToSent in subSectionNotificationsToSent)
                                        {
                                            emailsToSent = emailsToSent + notificationToSent.AdminOrClientUser.Email + ",";
                                        }
                                        emailsToSent = emailsToSent.Substring(0, emailsToSent.Length - 1);
                                    }
                                    //if (notification.AdminUserId != null)
                                    //strMessage = SendMail.sendMail(emailsToSent, body + "<br><br>" + Comment, subject);
                                    strMessage = SendMail.sendMail(emailsToSent, body, subject);
                                    //if (notification.ClientUserId != null)
                                    //    strMessage = SendMail.sendMail(notification.ClientUser.Email, body + "<br><br>" + Comment, subject);
                                    if (strMessage.ToString() == "Success")
                                    {
                                        //Rajesh on 1/8/2015
                                        if (!String.IsNullOrEmpty(Comment))
                                        {
                                            OrganizationsNotificationsSent NotificationSent = entityDB.OrganizationsNotificationsSent.FirstOrDefault(rec => rec.ClientId == currentPreQualification.ClientId && rec.SetupType == 2);
                                            if (NotificationSent == null)
                                            {
                                                NotificationSent = new OrganizationsNotificationsSent();
                                                NotificationSent.ClientId = currentPreQualification.ClientId;
                                                NotificationSent.SetupType = 2;
                                                entityDB.OrganizationsNotificationsSent.Add(NotificationSent);
                                            }
                                            var logs = new OrganizationsNotificationsEmailLogs();
                                            logs.Comments = Comment;
                                            logs.EmailSentDateTime = DateTime.Now;
                                            logs.MailSentBy = new Guid(Session["UserId"].ToString());
                                            logs.NotificationId = NotificationSent.NotificationId;
                                            logs.NotificationNo = 1;
                                            logs.NotificationStatus = 0;
                                            logs.NotificationType = 0;
                                            logs.PrequalificationId = currentPreQualification.PrequalificationId;
                                            logs.VendorId = currentPreQualification.VendorId;
                                            entityDB.OrganizationsNotificationsEmailLogs.Add(logs);
                                            entityDB.SaveChanges();
                                        }
                                        //Ends<<<
                                        foreach (var notificationSent in subSectionNotificationsToSent)
                                        {
                                            PrequalificationNotifications PreNotifications = new PrequalificationNotifications();
                                            PreNotifications.PrequalificationId = PreQualificationId;
                                            PreNotifications.TemplateSubSectionNotificationId = notificationSent.TemplateSubSectionNotificationId;
                                            PreNotifications.NotificationDateTime = DateTime.Now;
                                            entityDB.PrequalificationNotifications.Add(PreNotifications);
                                            entityDB.SaveChanges();//Rajesh on 3/4/2014
                                        }
                                    }
                                }
                        }
                    }
            }
            //Ends<<<
        }

        private PrequalificationCompletedSections MarkSectionAsCompleted(long SectionId, long PreQualificationId, String result, PrequalificationCompletedSections sectionCompleted, List<TemplateSections> tempSections, int? type)
        {
            var hideSectionsToDisplay = entityDB.PrequalificationSectionsToDisplay.Where(r => r.PrequalificationId == PreQualificationId && r.Visible == true).Select(r => r.TemplateSectionId);
            //Rajesh on 4/7/2014
            if (type == 3)
                return sectionCompleted;
            //Ends<<<
            if (sectionCompleted == null)
            {
                var firstOrDefault = tempSections.FirstOrDefault(rec => rec.TemplateSectionID == SectionId);
                if (firstOrDefault != null && (firstOrDefault.TemplateSectionType == 99 && result == "" || result == "preqOpenPeriodEnds"))// changes by Mani on 8/6/2015 
                {
                    //Do nothing there is some incomplete scction in this prequalification
                }
                else
                {
                    sectionCompleted = new PrequalificationCompletedSections
                    {
                        PrequalificationId = PreQualificationId,
                        TemplateSectionId = SectionId,
                        SubmittedDate = DateTime.Now,
                        SubmittedBy = SSession.UserId
                    };
                    entityDB.PrequalificationCompletedSections.Add(sectionCompleted);
                    entityDB.SaveChanges();
                }
            }
            //Rajesh on 9/25/2014
            //initially our system is like this.After prequalification completed by vendor it's directly send to admin for processing means finalize& complete satatus is 5.but now we have intrime step.
            //After finalize and submit prequalification status will be client review(16) if client completes all sections then this prequalification status will be 5(Submited to admin)
            var prequalification_loc = entityDB.Prequalification.Find(PreQualificationId);
            if (Session["RoleName"].ToString().Equals("Client") && prequalification_loc.PrequalificationStatusId == 16)
            {
                var flag_ClientSecCompleted = true;

                foreach (TemplateSections section in prequalification_loc.ClientTemplates.Templates.TemplateSections.Where(rec => rec.Visible == true && (rec.HideFromPrequalification != true || hideSectionsToDisplay.Contains(rec.TemplateSectionID))).OrderBy(x => x.DisplayOrder).ToList())
                {
                    var completedSection = entityDB.PrequalificationCompletedSections.FirstOrDefault(rec => rec.PrequalificationId == PreQualificationId && rec.TemplateSectionId == section.TemplateSectionID);//Rajesh on 2/24/2014
                    if (completedSection == null)//Rajesh on 2/24/2014
                    {
                        //Rajesh on 3/6/2014
                        if (section.TemplateSectionsPermission.FirstOrDefault().VisibleTo == 2)//Client Data Section No need to check this section for completion
                        {
                            flag_ClientSecCompleted = false;//Rajesh on 9/25/2014
                        }
                    }
                }

                if (flag_ClientSecCompleted)
                {
                    prequalification_loc.PrequalificationStatusId = 5;
                    entityDB.Entry(prequalification_loc).State = EntityState.Modified;
                    entityDB.SaveChanges();
                }
            }
            return sectionCompleted;
        }

        //private void CSIWebServiceInitiateRequest(Prequalification currentPreQualification,long SubSecId)
        //{

        //    WATCHDOGWebReference.SearchService myService = new WATCHDOGWebReference.SearchService();
        //    myService.Url = ConfigurationSettings.AppSettings["ATTUSWebServiceURL"].ToString();
        //    myService.Timeout = 30000;

        //    XmlDocument myXmlRequest = new XmlDocument();
        //    XmlNode docNode = myXmlRequest.CreateXmlDeclaration("1.0", "UTF-8", null);
        //    myXmlRequest.AppendChild(docNode);

        //    XmlNode ATTUSWATCHDOGREQUEST = myXmlRequest.CreateElement("", "ATTUSWATCHDOGREQUEST", "");
        //    myXmlRequest.AppendChild(ATTUSWATCHDOGREQUEST);

        //    XmlNode BILLINGCODE = myXmlRequest.CreateElement("", "BILLINGCODE", "");

        //    XmlAttribute partnerIdAttribute = myXmlRequest.CreateAttribute("partnerid");
        //    partnerIdAttribute.Value = "91b53925-dddf-44dd-a84f-baf6ce3c6aee";
        //    BILLINGCODE.Attributes.Append(partnerIdAttribute);

        //    XmlAttribute billingIdAttribute = myXmlRequest.CreateAttribute("billingid");
        //    billingIdAttribute.Value = "91b53925-dddf-44dd-a84f-baf6ce3c6aee";
        //    BILLINGCODE.Attributes.Append(billingIdAttribute);

        //    ATTUSWATCHDOGREQUEST.AppendChild(BILLINGCODE);

        //    XmlNode LISTS = myXmlRequest.CreateElement("", "LISTS", "");

        //    XmlNode LIST = myXmlRequest.CreateElement("", "LIST", "");

        //    XmlAttribute codeAttribute = myXmlRequest.CreateAttribute("code");
        //    codeAttribute.Value = "OFAC";
        //    LIST.Attributes.Append(codeAttribute);

        //    LISTS.AppendChild(LIST);
        //    ATTUSWATCHDOGREQUEST.AppendChild(LISTS);

        //    XmlNode QUERY = myXmlRequest.CreateElement("", "QUERY", "");

        //    XmlNode ITEM = myXmlRequest.CreateElement("", "ITEM", "");

        //    XmlAttribute allentitiesAttribute = myXmlRequest.CreateAttribute("allentities");
        //    allentitiesAttribute.Value = currentPreQualification.Vendor.PrincipalCompanyOfficerName;
        //    ITEM.Attributes.Append(allentitiesAttribute);

        //    XmlAttribute corpnameAttribute = myXmlRequest.CreateAttribute("corpname");
        //    corpnameAttribute.Value = currentPreQualification.Vendor.Name;
        //    ITEM.Attributes.Append(corpnameAttribute);

        //    QUERY.AppendChild(ITEM);
        //    ATTUSWATCHDOGREQUEST.AppendChild(QUERY);

        //    XmlNode VERSION = myXmlRequest.CreateElement("", "VERSION", "");
        //    VERSION.InnerText = "2.0";

        //    ATTUSWATCHDOGREQUEST.AppendChild(VERSION);

        //    XmlNode myXmlResponse = myService.ProcessNameListMessage(myXmlRequest);

        //    // Kiran on 3/5/2014
        //    XmlDocument responseDoc = new XmlDocument();
        //    responseDoc.LoadXml(myXmlResponse.OuterXml.ToString());

        //    string fileName = currentPreQualification.Vendor.Name + "_" + currentPreQualification.PrequalificationId;


        //    Directory.CreateDirectory(Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["WebResponseFiles"])));
        //    var path = Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["WebResponseFiles"]), fileName + ".xml");
        //    // Kiran on 7/31/2014
        //    if (path != null)
        //    {
        //        System.IO.File.Delete(path);
        //    }
        //    responseDoc.Save(path);
        //    // Ends<<<

        //    XmlNodeList parentNodes = myXmlResponse.ChildNodes;

        //    // Kiran on 7/31/2014
        //    var existingCSIRecordsForCurrentPrequalification = entityDB.CSIWebResponseDetails.Where(rec => rec.PrequalificationId == currentPreQualification.PrequalificationId);
        //    if (existingCSIRecordsForCurrentPrequalification != null)
        //    {
        //        foreach (var existingCSIRecord in existingCSIRecordsForCurrentPrequalification.ToList())
        //        {
        //            entityDB.CSIWebResponseDetails.Remove(existingCSIRecord);
        //            entityDB.SaveChanges();
        //        }
        //    }
        //    // Ends<<<

        //    foreach (XmlNode parentNode in parentNodes)
        //    {
        //        if (parentNode.Name == "SEARCHCRITERIA")
        //        {
        //            XmlNodeList searchCriteriaNodes = parentNode.ChildNodes;
        //            foreach (XmlNode searchCriteriaNode in searchCriteriaNodes)
        //            {
        //                if (searchCriteriaNode.Name == "ENTITIES")
        //                {
        //                    XmlNodeList entitiesNodes = searchCriteriaNode.ChildNodes;
        //                    if (entitiesNodes != null)
        //                    {
        //                        foreach (XmlNode entitiesNode in entitiesNodes)
        //                        {
        //                            if (entitiesNode.Name == "ENTITY")
        //                            {
        //                                XmlNodeList entityElementNodes = entitiesNode.ChildNodes;
        //                                var entityNodeAttributes = entitiesNode.Attributes;

        //                                foreach (XmlNode node in entityElementNodes)
        //                                {
        //                                    CSIWebResponseDetails csiRecord = new CSIWebResponseDetails();
        //                                    csiRecord.PrequalificationId = currentPreQualification.PrequalificationId;
        //                                    csiRecord.ResponseNeededForID = SubSecId;
        //                                    csiRecord.RequestInvokedFrom = 0;
        //                                    csiRecord.EntityID = Convert.ToInt64(entityNodeAttributes[0].Value);
        //                                    csiRecord.EntityNumber = Convert.ToInt64(entityNodeAttributes[1].Value);
        //                                    if (node.ChildNodes.Count != 0)
        //                                    {
        //                                        XmlNodeList childNodesOfEntityInnerNodes = node.ChildNodes;
        //                                        if (childNodesOfEntityInnerNodes.Count > 1)
        //                                        {
        //                                            foreach (XmlNode childNode in childNodesOfEntityInnerNodes)
        //                                            {
        //                                                if (childNode.Name == "ALIAS")
        //                                                {
        //                                                    csiRecord.ElementNodeName = childNode.Name;
        //                                                    csiRecord.ElementNodeValue = childNode.Attributes[0].Value;
        //                                                }
        //                                                else
        //                                                {
        //                                                    csiRecord.ElementNodeName = childNode.Name;
        //                                                    var addressAttributes = childNode.Attributes;
        //                                                    csiRecord.ElementNodeValue = addressAttributes[0].Value + "," + addressAttributes[1].Value + "," + addressAttributes[2].Value + "," + addressAttributes[4].Value + "," + addressAttributes[5].Value;
        //                                                }
        //                                                entityDB.CSIWebResponseDetails.Add(csiRecord);
        //                                                entityDB.SaveChanges();
        //                                            }
        //                                        }
        //                                        else
        //                                        {
        //                                            foreach (XmlNode childNode in childNodesOfEntityInnerNodes)
        //                                            {
        //                                                if (childNode.Name == "ALIAS")
        //                                                {
        //                                                    csiRecord.ElementNodeName = childNode.Name;
        //                                                    csiRecord.ElementNodeValue = childNode.Attributes[0].Value;
        //                                                }
        //                                                else
        //                                                {
        //                                                    csiRecord.ElementNodeName = childNode.ParentNode.Name;
        //                                                    csiRecord.ElementNodeValue = childNode.Value;
        //                                                }
        //                                                entityDB.CSIWebResponseDetails.Add(csiRecord);
        //                                                entityDB.SaveChanges();
        //                                            }
        //                                        }
        //                                    }
        //                                    else
        //                                    {
        //                                        csiRecord.ElementNodeName = node.Name;
        //                                        csiRecord.ElementNodeValue = node.Value;
        //                                        entityDB.CSIWebResponseDetails.Add(csiRecord);
        //                                        entityDB.SaveChanges();
        //                                    }

        //                                }
        //                            }
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //    }
        //}
        //Rajesh on 1/21/2014
        private void NextURL(long PreQualificationId, ref String result, ref string strNextSectionURL, Prequalification preQualificationDetails, decimal? paymentAmt, long SectionId)
        {
            Prequalification prequalification_loc = entityDB.Prequalification.Find(PreQualificationId);
            bool flag_Completed = true;
            bool flag_ClientSecCompleted = true;//Rajesh on 9/25/2014
            var hideSectionsToDisplay = entityDB.PrequalificationSectionsToDisplay.Where(r => r.PrequalificationId == PreQualificationId && r.Visible == true).Select(r => r.TemplateSectionId);
            foreach (TemplateSections section in prequalification_loc.ClientTemplates.Templates.TemplateSections.
                Where(rec => rec.Visible == true && (rec.HideFromPrequalification != true || hideSectionsToDisplay.Contains(rec.TemplateSectionID)))
                .OrderBy(x => x.DisplayOrder).ToList())
            {
                var completedSection = entityDB.PrequalificationCompletedSections.FirstOrDefault(rec => rec.PrequalificationId == PreQualificationId && rec.TemplateSectionId == section.TemplateSectionID);//Rajesh on 2/24/2014

                if (completedSection == null)//Rajesh on 2/24/2014
                {
                    //Rajesh on 3/6/2014
                    if (section.TemplateSectionsPermission.FirstOrDefault().VisibleTo == 2 || section.TemplateSectionsPermission.FirstOrDefault().VisibleTo == 4)//Client Data Section No need to check this section for completion
                    {
                        flag_ClientSecCompleted = false;//Rajesh on 9/25/2014
                    }
                    //Rajesh on 3/20/2014
                    else if (section.TemplateSectionID == SectionId)//Menas if we check same section at that time it well be not completed section so we need to skip checking
                    {
                    }
                    //Ends<<
                    // Ends <<<
                    else if (section.TemplateSectionType == 34)
                    {
                        if (preQualificationDetails.RequestForMoreInfo == true)
                        {
                            flag_Completed = false;
                            break;
                        }
                    }
                    else if (section.TemplateSectionType != 35)//Rajesh on 1/21/2014
                    {
                        flag_Completed = false;
                        break;
                    }

                }
                // Rajesh on 01/20/2015 to open the Exception/Probation section forever.
                //else
                //{
                //    if (section.TemplateSectionType == 34)
                //    {
                //        if (preQualificationDetails.RequestForMoreInfo == true)
                //        {
                //            flag_Completed = false;
                //            break;
                //        }
                //    }

                //}
                // Ends<<<
            }
            // Mani on 7/14/2015 for so-140
            // if (flag_Completed)//All Sections are completed
            //{
            //    var CurrentDate = DateTime.Now;
            //    var preqOpenperiod = entityDB.OrganizationsPrequalificationOpenPeriod.Where(rec => rec.VendorId == prequalification_loc.VendorId).OrderByDescending(rec => rec.PrequalificationPeriodClose).FirstOrDefault();
            //    var templatePrice = entityDB.TemplatePrices.FirstOrDefault(rec => rec.TemplateID == prequalification_loc.ClientTemplates.TemplateID).TemplatePrice;
            //    if (preqOpenperiod != null && preqOpenperiod.PrequalificationPeriodClose < CurrentDate && templatePrice !=prequalification_loc.PaymentReceivedAmount)
            //    {
            //        var paymentSection = entityDB.PrequalificationCompletedSections.FirstOrDefault(rec => rec.PrequalificationId == PreQualificationId && rec.TemplateSections.TemplateSectionType == 3);
            //        entityDB.PrequalificationCompletedSections.Remove(paymentSection);
            //        // Modify preq data
            //        prequalification_loc.PaymentReceived = false;
            //        entityDB.Entry(prequalification_loc).State = EntityState.Modified;
            //        entityDB.SaveChanges();
            //        result = "preqOpenPeriodEnds";
            //        flag_Completed = false;

            //    }
            // }
            // Ends >>
            //Rajesh on 1/21/2014
            if (Session["RoleName"].ToString().Equals("Client"))
            {
                flag_Completed = false;
            }
            //Ends<<<
            if (flag_Completed)//All Sections are completed
            {
                //if (prequalification_loc.PrequalificationStatusId != 5)
                //Rajesh on 9/25/2014
                var PresentPrequalificationstatus = 5;
                //if (!flag_ClientSecCompleted)
                //    PresentPrequalificationstatus = 16;
                //Ends<<
                if (prequalification_loc.PrequalificationStatusId != 5 || prequalification_loc.PrequalificationStatusId != 16)//16 means client review
                {
                    //To add Prequalification Logs
                    PrequalificationStatusChangeLog preQualificationLog = new PrequalificationStatusChangeLog();
                    preQualificationLog.PrequalificationId = prequalification_loc.PrequalificationId;
                    //preQualificationLog.PrequalificationStatusId = 5;//prequalification_loc.PrequalificationStatusId;
                    preQualificationLog.PrequalificationStatusId = PresentPrequalificationstatus;//rajesh on 9/25/2014
                    preQualificationLog.StatusChangeDate = DateTime.Now;
                    preQualificationLog.StatusChangedByUser = new Guid(Session["UserId"].ToString());
                    preQualificationLog.PrequalificationStart = preQualificationDetails.PrequalificationStart;
                    preQualificationLog.PrequalificationFinish = preQualificationDetails.PrequalificationFinish;
                    preQualificationLog.CreatedDate = DateTime.Now;
                    entityDB.PrequalificationStatusChangeLog.Add(preQualificationLog);
                    CopyFinancialDataToClient(PreQualificationId);
                    //>>>Ends
                }


                //ApplyFeecap(PreQualificationId, paymentAmt, prequalification_loc);
                // Kiran on 12/05/2015 for SMI-277 ends

                //prequalification_loc.PrequalificationStatusId = 5;//Submitted for Admin Review
                //Rajesh on 1/6/2015
                DateTime StartDate = DateTime.Now, EndDate = DateTime.Now;
                if (prequalification_loc.PrequalificationStatusId == 3)
                {
                    StartDate = DateTime.Now;
                }
                else if (prequalification_loc.PrequalificationStatusId == 15)
                {
                    StartDate = prequalification_loc.PrequalificationFinish.AddDays(1);
                }
                if (prequalification_loc.PrequalificationStatusId == 3 || prequalification_loc.PrequalificationStatusId == 15)//6 or 16 omit period...
                {
                    var preStatus = entityDB.PrequalificationStatus.FirstOrDefault(rec => rec.PrequalificationStatusId == 5);
                    if (preStatus != null)
                    {
                        if (preStatus.PeriodLengthUnit.ToLower().Equals("d"))
                            EndDate = StartDate.AddDays(preStatus.PeriodLength);
                        else if (preStatus.PeriodLengthUnit.ToLower().Equals("m"))
                            EndDate = StartDate.AddMonths(preStatus.PeriodLength);
                        else if (preStatus.PeriodLengthUnit.ToLower().Equals("y"))
                            EndDate = StartDate.AddYears(preStatus.PeriodLength);
                    }
                    prequalification_loc.PrequalificationStart = StartDate;
                    prequalification_loc.PrequalificationFinish = EndDate;
                }
                //Ends<<<
                prequalification_loc.PrequalificationStatusId = PresentPrequalificationstatus;//Rajesh on 9/25/2014
                prequalification_loc.PrequalificationSubmit = DateTime.Now;
                prequalification_loc.UserIdAsCompleter = (Guid)Session["UserId"];
                entityDB.Entry(prequalification_loc).State = EntityState.Modified;
                entityDB.SaveChanges();


                strNextSectionURL = "../SystemUsers/DashBoard";
            }


            if (result.Equals("Success"))
                result = strNextSectionURL;
        }
        public void CopyFinancialDataToClient(long PreQualificationId)
        {
            var latestPQ = entityDB.Prequalification.Find(PreQualificationId);
            var existingRecords = entityDB.FinancialAnalyticsModel.Where(r => r.VendorId == latestPQ.VendorId && r.ClientId == latestPQ.ClientId && r.DateOfFinancialStatements != null).Select(r => r.DateOfFinancialStatements).ToList();
            var Records = entityDB.FinancialAnalyticsModel.Where(r => r.VendorId == latestPQ.VendorId && r.ClientId == null && !existingRecords.Contains(r.DateOfFinancialStatements)).ToList();
            if (latestPQ.Vendor.FinancialDate != null && latestPQ.Vendor.FinancialMonth != null)
            {
                var vendorOrg = latestPQ.Vendor;
                DateTime todaysDate = DateTime.Now.Date;
                int Month = todaysDate.Month;
                int day = todaysDate.Day;

                //var date = new DateTime(DateTime.Now.Year, vendorOrg.FinancialMonth ?? 1, vendorOrg.FinancialDate ?? 1);
                //if (((vendorOrg.FinancialMonth ?? 0) > Month) || ((vendorOrg.FinancialMonth ?? 0) == Month && (vendorOrg.FinancialDate ?? 0) <= day))
                //{
                //    date = new DateTime(DateTime.Now.Year - 1, vendorOrg.FinancialMonth ?? 1, vendorOrg.FinancialDate ?? 1);

                //}

                foreach (var rec in Records)
                {
                    if (!rec.DateOfFinancialStatements.HasValue)
                        continue;
                    var temp = rec.DateOfFinancialStatements;
                    if (todaysDate < rec.DateOfFinancialStatements)
                    {
                        temp = rec.DateOfFinancialStatements.Value.AddYears(-1);
                    }
                    TimeSpan days = todaysDate - temp.Value.Date;
                    int Days = Math.Abs(days.Days);
                    //if (Days < 180)
                    //    continue;
                    FinancialAnalyticsModel Finance = new FinancialAnalyticsModel();
                    Finance.DateOfFinancialStatements = rec.DateOfFinancialStatements;

                    Finance.Id = rec.Id;
                    Finance.VendorId = rec.VendorId;

                    Finance.SourceOfFinancialStatements = rec.SourceOfFinancialStatements;
                    Finance.Dateofinterimfinancialstatement = rec.Dateofinterimfinancialstatement;
                    Finance.CashAndCashEquivalents = rec.CashAndCashEquivalents;
                    Finance.Investments = rec.Investments;
                    Finance.ReceivablesCurrent = rec.ReceivablesCurrent;
                    Finance.ReceivableRetainage = rec.ReceivableRetainage;
                    Finance.CostsAndEstimatedEarningsInExcessOfBillingsOnUncompletedContracts = rec.CostsAndEstimatedEarningsInExcessOfBillingsOnUncompletedContracts;
                    Finance.RefundableIncomeTaxes = rec.RefundableIncomeTaxes;
                    Finance.NoteReceivable = rec.NoteReceivable;
                    Finance.OtherCurrentAssets = rec.OtherCurrentAssets;
                    Finance.NetPropertyAndEquipment = rec.NetPropertyAndEquipment;
                    Finance.OtherAssets = rec.OtherAssets;
                    Finance.LinesOfCreditShortTermBorrowings = rec.LinesOfCreditShortTermBorrowings;
                    Finance.InstallmentNotesPayableDueWithinOneYear = rec.InstallmentNotesPayableDueWithinOneYear;
                    Finance.AccountsPayable = rec.AccountsPayable;
                    Finance.RetainagePayable = rec.RetainagePayable;
                    Finance.AccruedExpenses = rec.AccruedExpenses;
                    Finance.BillingsInExcessOfCostsAndEstimatedEarningsOnUncompletedContracts = rec.BillingsInExcessOfCostsAndEstimatedEarningsOnUncompletedContracts;
                    Finance.CurrentIncomeTaxesPayable = rec.CurrentIncomeTaxesPayable;
                    Finance.DeferredIncomeTaxesPayableCurrent = rec.DeferredIncomeTaxesPayableCurrent;
                    Finance.NotesPayableDueAfterOneYear = rec.NotesPayableDueAfterOneYear;
                    Finance.EquipmentLinesOfCredit = rec.EquipmentLinesOfCredit;
                    Finance.OtherLongTermLiabilities = rec.OtherLongTermLiabilities;
                    Finance.DeferredIncomeTaxesPayableLongTerm = rec.DeferredIncomeTaxesPayableLongTerm;
                    Finance.TotalStockholdersEquity = rec.TotalStockholdersEquity;
                    Finance.ConstructionRevenues = rec.ConstructionRevenues;
                    Finance.ConstructionDirectCosts = rec.ConstructionDirectCosts;
                    Finance.NonConstructionGrossProfitLoss = rec.NonConstructionGrossProfitLoss;
                    Finance.GeneralAndAdministrativeExpenses = rec.GeneralAndAdministrativeExpenses;
                    Finance.OtherIncomeDeductions = rec.OtherIncomeDeductions;
                    Finance.IncomeTaxExpenseBenifit = rec.IncomeTaxExpenseBenifit;
                    Finance.DepreciationAndAmortization = rec.DepreciationAndAmortization;
                    Finance.TotalBacklog = rec.TotalBacklog;
                    Finance.TotalLineOfCredit = rec.TotalLineOfCredit;
                    Finance.LineOfCreditAvailability = rec.LineOfCreditAvailability;
                    Finance.InterestExpense = rec.InterestExpense;
                    //Finance.PrequalificationId = PreQualificationId;
                    Finance.ClientId = latestPQ.ClientId;
                    Finance.TotalThreeLargestCompletedProjects = rec.TotalThreeLargestCompletedProjects;
                    entityDB.FinancialAnalyticsModel.Add(Finance);
                    logs.Info("FinancialAnalytics Added Successfully " + Finance.Id, "PreQualificationId=" + PreQualificationId, "PrequalificationController/CopyFinancialDataToClient");

                }
            }
            entityDB.SaveChanges();
        }


        //private void ApplyFeecap(long PreQualificationId, decimal? paymentAmt, Prequalification prequalification_loc)
        //{
        //    //Rajesh on 3/17/2014
        //    if (prequalification_loc.PrequalificationStatusId == 3)//Rajesh on 9/26/2014 // Kiran on 12/05/2015 for 500^ Validation(Separated the logic for Pending, Pending-Renewal Statuses)
        //    {
        //        var recievedPreqPrice = entityDB.PrequalificationPayments.FirstOrDefault(rec => rec.PrequalificationId == PreQualificationId && rec.PaymentCategory == 0).PaymentReceivedAmount;// Mani on 8/10/2015
        //        var CurrentDate = DateTime.Now;
        //        var OPeriod = entityDB.OrganizationsPrequalificationOpenPeriod.FirstOrDefault(rec => rec.VendorId == prequalification_loc.VendorId && rec.PrequalificationPeriodStart <= CurrentDate && rec.PrequalificationPeriodClose >= CurrentDate);
        //        if (OPeriod == null)
        //        {
        //            OPeriod = new OrganizationsPrequalificationOpenPeriod();
        //            OPeriod.VendorId = prequalification_loc.VendorId;
        //            OPeriod.PrequalificationPeriodStart = DateTime.Now;
        //            OPeriod.PrequalificationPeriodClose = DateTime.Now.AddYears(1);
        //            OPeriod.PeriodStatus = 0;
        //            OPeriod.TotalPaymentRecieved = paymentAmt == null ? 0 : (decimal)recievedPreqPrice;
        //            entityDB.OrganizationsPrequalificationOpenPeriod.Add(OPeriod);
        //        }
        //        else
        //        {
        //            OPeriod.TotalPaymentRecieved += (paymentAmt == null ? 0 : (decimal)recievedPreqPrice);
        //            entityDB.Entry(OPeriod).State = EntityState.Modified;
        //        }


        //        //To Close all OrganizationsPrequalificationOpenPeriod

        //        foreach (var openPeriod in entityDB.OrganizationsPrequalificationOpenPeriod.Where(rec => rec.PrequalificationPeriodClose < CurrentDate && rec.PeriodStatus == 0).ToList())
        //        {
        //            openPeriod.PeriodStatus = 1;
        //            entityDB.Entry(openPeriod).State = EntityState.Modified;
        //        }
        //        entityDB.SaveChanges();
        //    }

        //    //Ends<<<

        //    // Kiran on 12/05/2015 for 500^ validation
        //    if (prequalification_loc.PrequalificationStatusId == 15)
        //    {
        //        var CurrentDate = DateTime.Now;
        //        var templateRecievedAmount = entityDB.PrequalificationPayments.FirstOrDefault(rec => rec.PrequalificationId == PreQualificationId && rec.PaymentCategory == 0).PaymentReceivedAmount;// Mani on 8/10/2015
        //        var OPeriod = entityDB.OrganizationsPrequalificationOpenPeriod.FirstOrDefault(rec => rec.VendorId == prequalification_loc.VendorId && rec.PrequalificationPeriodStart <= CurrentDate && rec.PrequalificationPeriodClose >= CurrentDate);
        //        OrganizationsPrequalificationOpenPeriod openPeriodOfUpcomingYear = null;
        //        DateTime? upcomingYearPeriodStartDate = null;
        //        DateTime? upcomingYearPeriodEndDate = null;
        //        try
        //        {
        //            openPeriodOfUpcomingYear = entityDB.OrganizationsPrequalificationOpenPeriod.FirstOrDefault(rec => rec.VendorId == prequalification_loc.VendorId && rec.PrequalificationPeriodStart > OPeriod.PrequalificationPeriodClose);
        //        }
        //        catch (Exception e) { }
        //        // Kiran on 12/21/2015 for FV-241
        //        if (OPeriod != null)
        //        {
        //            upcomingYearPeriodStartDate = OPeriod.PrequalificationPeriodClose.AddDays(1);
        //            upcomingYearPeriodEndDate = OPeriod.PrequalificationPeriodClose.AddYears(1);
        //        }
        //        // Kiran on 12/21/2015 for FV-241 ends<<<

        //        if (OPeriod == null) // It means user making payment after the Pending-Renewal period & vendor annualopenperiod expires
        //        {
        //            OPeriod = new OrganizationsPrequalificationOpenPeriod();
        //            OPeriod.VendorId = prequalification_loc.VendorId;
        //            OPeriod.PrequalificationPeriodStart = DateTime.Now;
        //            OPeriod.PrequalificationPeriodClose = DateTime.Now.AddYears(1);
        //            OPeriod.PeriodStatus = 0;
        //            OPeriod.TotalPaymentRecieved = paymentAmt == null ? 0 : (decimal)templateRecievedAmount;
        //            entityDB.OrganizationsPrequalificationOpenPeriod.Add(OPeriod);
        //        }
        //        else if (OPeriod != null && OPeriod.PrequalificationPeriodClose > CurrentDate && openPeriodOfUpcomingYear == null) // It means user making payment during Renewal Open Period
        //        {
        //            OPeriod = new OrganizationsPrequalificationOpenPeriod();
        //            OPeriod.VendorId = prequalification_loc.VendorId;
        //            OPeriod.PrequalificationPeriodStart = Convert.ToDateTime(upcomingYearPeriodStartDate);
        //            OPeriod.PrequalificationPeriodClose = Convert.ToDateTime(upcomingYearPeriodEndDate);
        //            OPeriod.PeriodStatus = 0;
        //            OPeriod.TotalPaymentRecieved = paymentAmt == null ? 0 : (decimal)templateRecievedAmount;
        //            entityDB.OrganizationsPrequalificationOpenPeriod.Add(OPeriod);
        //        }
        //        else if (OPeriod.PrequalificationPeriodClose > CurrentDate.AddDays(60) && openPeriodOfUpcomingYear == null)
        //        {
        //            OPeriod.TotalPaymentRecieved += (paymentAmt == null ? 0 : (decimal)templateRecievedAmount);
        //            entityDB.Entry(OPeriod).State = EntityState.Modified;
        //        }
        //        else
        //        {
        //            openPeriodOfUpcomingYear.TotalPaymentRecieved += (paymentAmt == null ? 0 : (decimal)templateRecievedAmount);
        //            entityDB.Entry(OPeriod).State = EntityState.Modified;
        //        }

        //        //To Close all OrganizationsPrequalificationOpenPeriod

        //        foreach (var openPeriod in entityDB.OrganizationsPrequalificationOpenPeriod.Where(rec => rec.PrequalificationPeriodClose < CurrentDate && rec.PeriodStatus == 0 && rec.VendorId == prequalification_loc.VendorId).ToList())
        //        {
        //            openPeriod.PeriodStatus = 1;
        //            entityDB.Entry(openPeriod).State = EntityState.Modified;
        //        }
        //        entityDB.SaveChanges();
        //    }
        //}
        //Ends<<<
        public void getCommand()
        {
            SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["EFDbContext"].ConnectionString);
            connection.Open();
            ViewBag.command = connection.CreateCommand();

        }

        public async Task<ActionResult> PaymentPartialView(long pqId)
        {
            PqPaymentsViewModel paymentData = await _prequalification.GetPaymentData(pqId);
            return PartialView(paymentData);
        }

        private decimal getTemplatePrice(long vendorId, Guid? clientTemplateId, long preQualificationId)
        {
            return _prequalification.GetPqPriceToPay(vendorId, clientTemplateId, preQualificationId);
            //var currentPrequalification = entityDB.Prequalification.Find(preQualificationId);
            ////ViewBag.VBBUGroupPrice = entityDB.ClientTemplatesBUGroupPrice.Where(rec => rec.ClientTemplateID == currentPrequalification.ClientTemplateId && rec.GroupingFor == 0).GroupBy(rec => rec.GroupPriceType).ToList();
            //decimal price = 0;

            //var templatePrices = entityDB.ClientTemplatesBUGroupPrice.FirstOrDefault(rec => rec.ClientTemplateID == clientTemplateId && rec.GroupPriceType == 0 && rec.GroupingFor == 0);
            //if (templatePrices == null)
            //    return 0;
            //var CurrentDate = DateTime.Now;

            //OrganizationsPrequalificationOpenPeriod PreopenPeriod = entityDB.OrganizationsPrequalificationOpenPeriod.FirstOrDefault(rec => rec.VendorId == currentPrequalification.VendorId && rec.PrequalificationPeriodStart <= CurrentDate && rec.PrequalificationPeriodClose >= CurrentDate); ;
            //if (currentPrequalification.PrequalificationStatusId != 15)
            //    PreopenPeriod = PreopenPeriod;
            //else if (currentPrequalification.PrequalificationStatusId == 15)
            //{
            //    if (PreopenPeriod != null && PreopenPeriod.PrequalificationPeriodClose >= CurrentDate.AddDays(60))
            //        PreopenPeriod = PreopenPeriod;
            //    else
            //        try
            //        {
            //            PreopenPeriod = entityDB.OrganizationsPrequalificationOpenPeriod.FirstOrDefault(rec => rec.VendorId == vendorId && rec.PrequalificationPeriodStart > PreopenPeriod.PrequalificationPeriodClose);
            //        }
            //        catch
            //        {
            //            PreopenPeriod = null;
            //        }
            //}
            //if (PreopenPeriod == null && currentPrequalification.PaymentReceived == null)
            //    price = Convert.ToDecimal(templatePrices.GroupPrice);

            //else if (PreopenPeriod == null && currentPrequalification.PaymentReceived == false)
            //    price = Convert.ToDecimal(templatePrices.GroupPrice);

            //else if (PreopenPeriod == null && currentPrequalification.PaymentReceived != null)
            //{
            //    price = 0;
            //}
            //else
            //{
            //    long annualPeriodMaximumAmount = Convert.ToInt64(ConfigurationManager.AppSettings["PrequalificationAnnualPeriodAmount"]);
            //    if (annualPeriodMaximumAmount - PreopenPeriod.TotalPaymentRecieved >= templatePrices.GroupPrice && (currentPrequalification.PaymentReceived == null || currentPrequalification.PaymentReceived == false))
            //    {
            //        price = Convert.ToDecimal(templatePrices.GroupPrice);
            //    }
            //    else if (annualPeriodMaximumAmount - PreopenPeriod.TotalPaymentRecieved <= templatePrices.GroupPrice && (currentPrequalification.PaymentReceived == null || currentPrequalification.PaymentReceived == false))
            //    {
            //        price = annualPeriodMaximumAmount - PreopenPeriod.TotalPaymentRecieved;
            //        ViewBag.DiscountPrice = templatePrices.GroupPrice - price;
            //    }
            //    else
            //    {
            //        price = 0;
            //    }
            //}
            //if (price <= 0)
            //{
            //    ViewBag.DiscountPrice = Convert.ToDecimal(templatePrices.GroupPrice);
            //    price = 0;
            //}
            //return Math.Round(price, 2);
        }
        private decimal paymentSection(long vendorId, Guid? clientTemplateId, long preQualificationId)
        {
            var currentPrequalification = entityDB.Prequalification.FirstOrDefault(rec => rec.PrequalificationId == preQualificationId);
            ViewBag.preqBus = entityDB.PrequalificationSites.Where(rec => rec.PrequalificationPayments.PaymentCategory == 0 && rec.PrequalificationId == preQualificationId).Select(rec => rec.ClientBusinessUnits).Distinct().ToList();

            List<Guid> clientTemplateIds = getPQClientTemplateId(currentPrequalification);

            var preqsitebus = entityDB.PrequalificationSites.Where(rec => rec.PrequalificationId == preQualificationId).Select(rec => rec.ClientBusinessUnitId).Distinct().ToList();
            var additionalFeeBu = entityDB.ClientTemplatesBUGroupPrice.Where(rec => clientTemplateIds.Contains((Guid)rec.ClientTemplateID) && rec.GroupingFor == 0 && rec.GroupPrice != 0 && rec.GroupPriceType != 0).Select(rec => rec.ClientTemplateBUGroupId).ToList();

            var paidAdditonalBu = entityDB.PrequalificationPayments.Where(rec => rec.PrequalificationId == preQualificationId && rec.PaymentCategory == 1).Select(rec => rec.PrequalificationPaymentsId).ToList();
            ViewBag.paidAdditonalBu = entityDB.PrequalificationSites.Where(rec => paidAdditonalBu.Contains((long)rec.PrequalificationPaymentsId) && rec.PrequalificationId == preQualificationId).GroupBy(rec => rec.PrequalificationPaymentsId).ToList();// for paid additional bu's in view
            if (paidAdditonalBu.Count != ViewBag.paidAdditonalBu.Count)//paid in additional but move to one fee to show the payment in additional
            {
                var paidAdditionalPaymentIds = entityDB.PrequalificationSites.Where(rec => paidAdditonalBu.Contains((long)rec.PrequalificationPaymentsId) && rec.PrequalificationId == preQualificationId).Select(rec => rec.PrequalificationPaymentsId).Distinct().ToList();
                ViewBag.unusedPaymentids = entityDB.PrequalificationPayments.Where(rec => !paidAdditionalPaymentIds.Contains(rec.PrequalificationPaymentsId) && rec.PrequalificationId == preQualificationId && rec.PaymentCategory == 1).Distinct().ToList();
            }


            long preqStatusid = currentPrequalification.PrequalificationStatusId;

            var monthDiff = 0;

            var currentDate = DateTime.Now;
            var finalStatuses = _prequalification.Finalstatus();
            if (!finalStatuses.Any(r => r == preqStatusid))//if (preqStatusid == 3 || preqStatusid == 15)
            {
                monthDiff = 12;
            }
            else if (currentDate.Year > currentPrequalification.PrequalificationStart.Year)
            {
                monthDiff = currentPrequalification.PrequalificationStart.Month - currentDate.Month;//12 - (12 - currentPrequalification.PrequalificationStart.Month + currentDate.Month);
            }
            else
            {
                monthDiff = 12 - currentDate.Month + currentPrequalification.PrequalificationStart.Month;
            }

            ViewBag.monthdiff = monthDiff;
            var allBus = entityDB.ClientTemplatesForBU.Where(rec => preqsitebus.Contains(rec.ClientBusinessUnitId) && additionalFeeBu.Contains((long)rec.ClientTemplateBUGroupId)).ToList();

            ViewBag.unPaidBus = allBus;
            var additionalBus = allBus.Select(rec => rec.ClientBusinessUnitId).ToList(); //entityDB.ClientTemplatesForBU.Where(rec => preqsitebus.Contains(rec.ClientBusinessUnitId) && additionalFeeBu.Contains((long)rec.ClientTemplateBUGroupId)).Select(rec => rec.ClientBusinessUnitId).ToList();
            var unpaidBus = entityDB.PrequalificationSites.Where(rec => rec.PrequalificationId == preQualificationId && additionalBus.Contains(rec.ClientBusinessUnitId) && (rec.PrequalificationPaymentsId == null || rec.PrequalificationPayments.PaymentCategory == 0)).Select(rec => rec.ClientBusinessUnitId).Distinct().ToList();//To display one fee bu,to be paid in additional group
            var unpaidGroups = allBus.Where(r => unpaidBus.Contains(r.ClientBusinessUnitId)).Select(r => r.ClientTemplateBUGroupId);
            ViewBag.paidBus = unpaidBus;

            decimal totalAdditionalfee = 0;

            if (currentPrequalification.ClientTemplates.Templates.IsERI)
            {
                if (currentDate.Month != currentPrequalification.PrequalificationStart.Month && DateTime.Now.Day >= 16 && preqStatusid != 3 && preqStatusid != 15)
                {
                    monthDiff--;
                }
                ViewBag.unpaidGroups = unpaidGroups.ToList();
                var priceList = allBus.Select(r => new { r.ClientTemplateBUGroupId, r.ClientTemplatesBUGroupPrice.GroupPrice }).Distinct();
                foreach (var BusinessUnit in priceList)
                {
                    if (unpaidGroups.Contains(BusinessUnit.ClientTemplateBUGroupId))
                    {
                        totalAdditionalfee += (decimal)BusinessUnit.GroupPrice * monthDiff / 12;
                    }
                }

            }
            else
            {
                foreach (var BusinessUnit in allBus)
                {
                    if (unpaidBus.Contains(BusinessUnit.ClientBusinessUnitId))
                    {
                        totalAdditionalfee += (decimal)BusinessUnit.ClientTemplatesBUGroupPrice.GroupPrice;
                    }
                }
            }
            totalAdditionalfee = Math.Round(totalAdditionalfee, 2, MidpointRounding.AwayFromZero);
            totalAdditionalfee = applyFeeCap(vendorId, DateTime.Now, preQualificationId, totalAdditionalfee);
            ViewBag.AdditonalFee = totalAdditionalfee;
            var paidGroupIds = entityDB.PrequalificationTrainingAnnualFees.Where(rec => rec.PrequalificationId == preQualificationId).Select(rec => rec.ClientTemplateBUGroupId).ToList();
            var test = entityDB.ClientTemplatesForBU.Where(rec => preqsitebus.Contains(rec.ClientBusinessUnitId) && !paidGroupIds.Contains((long)rec.ClientTemplateBUGroupId) && rec.ClientTemplateId == clientTemplateId && rec.ClientTemplatesBUGroupPrice.GroupingFor == 1 && rec.ClientTemplatesBUGroupPrice.GroupPrice != 0).ToList();
            ViewBag.annualfeeBus = test.GroupBy(rec => rec.ClientTemplateBUGroupId).ToList();
            var paygroup = test.Select(rec => rec.ClientTemplatesBUGroupPrice).Distinct().ToList();
            ViewBag.annualPaidBus = entityDB.PrequalificationTrainingAnnualFees.Where(rec => rec.PrequalificationId == preQualificationId).GroupBy(rec => rec.ClientTemplateBUGroupId).ToList();
            decimal totalannualfee = 0;
            for (int i = 0; i < paygroup.Count; i++)
            {
                totalannualfee = (paygroup[i].GroupPrice.Value * ViewBag.monthdiff / 12) + totalannualfee;
            }
            ViewBag.totalAnnualFee = Math.Round(totalannualfee, 2, MidpointRounding.AwayFromZero);
            ViewBag.VBprequalAnnualFeeGroupIds = entityDB.PrequalificationTrainingAnnualFees.Where(rec => rec.PrequalificationId == preQualificationId).Select(rec => rec.ClientTemplateBUGroupId).Distinct().ToList();
            ViewBag.VBprequalAnnualFeePaidGroups = entityDB.PrequalificationTrainingAnnualFees.Where(rec => rec.PrequalificationId == preQualificationId).Distinct().ToList();// for paid bu's  // Kiran on 06/24/2015 for payment issue after making payment
            ViewBag.VBPrequalSitesBusinessUnits = preqsitebus;
            decimal price = 0;
            if (currentPrequalification.TotalPaymentReceivedAmount == null)
            {
                try
                {
                    var templatePrices = entityDB.ClientTemplatesBUGroupPrice.FirstOrDefault(rec => rec.ClientTemplateID == clientTemplateId && rec.GroupPriceType == 0 && rec.GroupingFor == 0).GroupPrice;
                    if (templatePrices != null)
                        price = (decimal)templatePrices;
                }
                catch
                {
                    price = 0;
                }
            }

            return price;
        }

        private List<Guid> getPQClientTemplateId(Prequalification currentPrequalification)
        {
            var clientIds = new List<long>();
            if (currentPrequalification.ClientTemplates.Templates.IsERI)
            {
                var clients = _prequalification.GetPrequalificationClient(currentPrequalification.PrequalificationId);
                if (clients != null)
                    clientIds = clients.ClientIdsList.ToList();
            }
            clientIds.Add(currentPrequalification.ClientId);
            var clientTemplateIds = entityDB.ClientTemplates.Where(r => clientIds.Contains(r.ClientID) && r.TemplateID == currentPrequalification.ClientTemplates.TemplateID).Select(r => r.ClientTemplateID).ToList();
            return clientTemplateIds;
        }

        //////////////////////
        //////////////////////
        // Siva on 29th Aug //
        //////////////////////

        [SessionExpire(lastPageUrl = "../SystemUsers/Dashboard")]
        public ActionResult EmailModelTemplatesList(long prequalificationId, bool isERITemplate, bool reloadContent, long? selectedSubClientId)
        {
            ViewBag.PrequalificationId = prequalificationId;
            ViewBag.eritemplate = isERITemplate;
            ViewBag.reloadContent = reloadContent;

            Prequalification pq = _prequalification.GetPrequalification(prequalificationId);
            if (isERITemplate)
            {
                var subClients = _clientBusiness.GetSubClients(pq.ClientId, false);

                List<SelectListItem> subClientsList = new List<SelectListItem>();
                subClients.ForEach(s => subClientsList.Add(new SelectListItem() { Text = s.Value, Value = s.Key }));
                ViewBag.VBSubClients = subClientsList.ToList();
            }
            List<long?> clientIds = new List<long?>();
            clientIds.Add(pq.ClientId);

            if (selectedSubClientId != null)
            {
                clientIds.Add((long)selectedSubClientId);
                ViewBag.VBSelectedSubClient = selectedSubClientId;
            }

            return View(_email.GetClientEmailTemplates(clientIds));
        }

        public ActionResult GetVendorOpenPeriods(long vendorId)
        {
            return Json(helper.GetVendorOpenPeriods(vendorId), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetVendorFeecapOpenAmount(long vendorId)
        {
            return Json(helper.GetVendorFeecapOpenAmount(vendorId), JsonRequestBehavior.AllowGet);
        }

        [SessionExpireForView]
        public ActionResult AgreementEmailCompose(long pqId, long subSectionId)
        {
            var Pq = entityDB.Prequalification.Find(pqId);
            var agreementSubsec =
                entityDB.AgreementSubSectionConfiguration.FirstOrDefault(r => r.SubSectionId == subSectionId);
            if (agreementSubsec == null)
                return Content("<b>There is no Email-Configuration. Please contact system admin</b>");
            var model = new LocalEmailComposeModel { PrequalificationId = pqId, ToAddress = "" };
            try
            {
                var email =
                entityDB.SystemUsersOrganizations.FirstOrDefault(
                        record => record.OrganizationId == Pq.VendorId && record.PrimaryUser == true)
                    .SystemUsers.Email;
                model.CCAddress = email;
            }
            catch
            {
                model.CCAddress = "";
            }


            if (agreementSubsec.SendNotificationTo.Contains("Admin"))
            {
                model.ToAddress += ",info@firstverify.com";
            }
            var clientEmail = "";
            if (agreementSubsec.SendNotificationTo.Contains("Client"))
            {
                try
                {
                    var systemUsersOrganizations = entityDB.SystemUsersOrganizations.FirstOrDefault(
                        record =>
                            record.OrganizationId == Pq.ClientId && record.PrimaryUser == true);
                    if (systemUsersOrganizations != null)
                        clientEmail =
                            systemUsersOrganizations
                                .SystemUsers.Email;
                    model.ToAddress += "," + clientEmail;
                }
                catch
                {
                    // ignored
                }
            }
            model.ToAddress = model.ToAddress.Length > 1 ? model.ToAddress.Substring(1) : "";
            var template = entityDB.EmailTemplates.Find(agreementSubsec.EmailTemplateId);
            if (template != null)
            {
                model.Subject = template.EmailSubject;
                model.Body = template.EmailBody;
            }

            model.Subject = model.Subject.Replace("[Date]", DateTime.Now + "");
            model.Subject = model.Subject.Replace("[VendorName]", Pq.Vendor.Name);
            model.Subject = model.Subject.Replace("[ClientName]", Pq.Client.Name);
            model.Subject = model.Subject.Replace("[CustomerServicePhone]", Pq.Client.PhoneNumber);
            model.Subject = model.Subject.Replace("[CustomerServiceEmail]", clientEmail);


            model.Body = model.Body.Replace("[Date]", DateTime.Now + "");
            model.Body = model.Body.Replace("[VendorName]", Pq.Vendor.Name);
            model.Body = model.Body.Replace("[ClientName]", Pq.Client.Name);
            model.Body = model.Body.Replace("[CustomerServicePhone]", Pq.Client.PhoneNumber);
            model.Body = model.Body.Replace("[CustomerServiceEmail]", clientEmail);
            return View(model);
        }

        [HttpPost]
        [SessionExpireForView]
        public ActionResult AgreementEmailCompose(LocalEmailComposeModel form)
        {
            form.LoginUser = SSession.UserId;
            if (!ModelState.IsValid)
                return View(form);
            var emailStatus = SendMail.sendMail(form.ToAddress, HttpUtility.HtmlDecode(form.Body), form.Subject, form.BCCAddress, form.CCAddress);
            if (emailStatus.Equals("Success"))
            {
                if (_templateToolBusiness.CreateAgreementSubsectionLog(form) == null)
                {
                    ModelState.AddModelError("*", "Unable to process the request");
                    return View(form);
                }
            }
            else
            {
                ViewBag.EmailStatus = false;
                return View(form);
            }
            return Redirect("PopUpCloseView?subSectionId=" + form.SubSectionId);
        }


        [SessionExpireForView]
        public ActionResult AddOrEditAddendum(long pqSubSectionId, long pqId, long subSectionId)
        {
            var pq = _prequalification.GetPrequalification(pqId);
            var vendor = _organization.GetOrganization(pq.VendorId);
            var form = _prequalification.GetAddendum(pqSubSectionId) ?? new AddendumModel()
            {
                PQId = pqId,
                SubSectionId = subSectionId,
                PQSubSectionId = pqSubSectionId,
                Header = System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/html/AddendumHeader.html")).Replace("[Date]", DateTime.Now.ToString("MM-dd-yyyy")).Replace("[VendorName]", vendor.Name),
                Footer = System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/html/AddendumFooter.html"))
            };
            return View(form);
        }
        [HttpPost]
        [SessionExpireForView]
        public ActionResult AddOrEditAddendum(AddendumModel form)
        {
            form.UserId = SSession.UserId;
            form.Header = HttpUtility.HtmlDecode(form.Header);
            form.Footer = HttpUtility.HtmlDecode(form.Footer);
            form = _prequalification.AddAddendum(form);
            if (form.PQSubSectionId == -1)
            {
                ViewBag.status = false;
                return View(form);
            }
            return Redirect("PopUpCloseView?subSectionId=" + form.SubSectionId);
        }

        [HttpPost]
        [SessionExpire]
        public string ToggleAddendumVendorDisply(long pqSubSectionId)
        {
            return _prequalification.ToggleAddendumVendorDisply(pqSubSectionId) ? "Ok" : "Fail";
        }
        [SessionExpire]
        public ActionResult LoadAddendum(long pqId, long subSectionId)
        {
            var result = _prequalification.GetAddendums(pqId, subSectionId);
            ViewBag.isEdit = SSession.Role.Equals(OrganizationType.Admin);
            var templateSubSections = entityDB.TemplateSubSections.Find(subSectionId);
            var submittedDate = DateTime.Now;
            if (templateSubSections != null)
            {
                var prequalificationCompletedSections = templateSubSections
                    .TemplateSections.PrequalificationCompletedSections.FirstOrDefault();
                if (prequalificationCompletedSections != null)
                {
                    if (prequalificationCompletedSections
                            .SubmittedDate != null)
                        submittedDate =
                            (DateTime)prequalificationCompletedSections
                                .SubmittedDate;
                }
            }
            ViewBag.SubmittedDate = submittedDate;
            if (SSession.Role.Equals(OrganizationType.Vendor))
            {
                result = result.Where(r => r.VisibleToVendor).ToList();
            }
            return PartialView(result);
        }
        [SessionExpireForView]
        public ActionResult EmailCompose(int templateId, long prequalificationId, long? selectedSubClientId)
        {
            ViewBag.errorMessage = "";
            LocalEmailComposeModel newEmailForm = _email.GetEmailTemplateContent(templateId, prequalificationId,
                selectedSubClientId, SSession.UserId);
            return View(newEmailForm);
        }

        [SessionExpireForView]
        [HttpPost]
        public ActionResult EmailCompose(LocalEmailComposeModel form, List<HttpPostedFileBase> attachments)
        {
            string bccMail = "";
            string ccMail = "";
            string toAddress = ""; // Kiran on 12/6/2014

            var bodyString = HttpUtility.HtmlDecode(form.Body);
            if (form.SendCopyToMySelf)
            {
                var userId = new Guid(Session["UserId"].ToString());
                var sysUser = entityDB.SystemUsers.Find(userId);
                //bccMail = sysUser.Email;
            }

            // Kiran on 9/15/2014
            if (!string.IsNullOrEmpty(form.CCAddress))
            {
                if (!form.CCAddress.Trim().Equals("")) //observe the !
                {
                    ccMail = form.CCAddress;
                    string ccEmails = "";
                    foreach (var email in ccMail.Split(','))
                    {
                        if (email == "")
                        {
                            ccEmails = ccEmails + email.Replace(",", "");
                        }
                        else
                        {
                            ccEmails = ccEmails + email.Replace(",", "") + ",";
                        }
                    }
                    ccMail = ccEmails.Substring(0, ccEmails.Length - 1);
                }
            }
            // Ends<<<

            // Kiran on 12/6/2014
            if (!string.IsNullOrEmpty(form.ToAddress))
            {
                if (!form.ToAddress.Trim().Equals("")) //observe the !
                {
                    toAddress = form.ToAddress;
                    string toAddressEmails = "";
                    foreach (var email in toAddress.Split(','))
                    {
                        if (email == "")
                        {
                            toAddressEmails = toAddressEmails + email.Replace(",", "");
                        }
                        else
                        {
                            toAddressEmails = toAddressEmails + email.Replace(",", "") + ",";
                        }
                    }
                    toAddress = toAddressEmails.Substring(0, toAddressEmails.Length - 1);
                }
            }
            // Ends<<<

            List<Attachment> mailAttachments = new List<Attachment>();
            if (attachments != null && attachments.Count() > 0)
            {
                foreach (var file in attachments)
                {
                    if (file != null)
                    {
                        Attachment messageAttachment = new Attachment(file.InputStream, Path.GetFileName(file.FileName));
                        mailAttachments.Add(messageAttachment);
                    }
                }
            }
            if (form.AttachmentsList != null)
                foreach (var attachment in form.AttachmentsList.Where(rec => rec.isActive == 1))
                {
                    var locAttach = entityDB.EmailTemplateAttachments.Find(attachment.AttachmentId);
                    var path = Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["EmailTemplateAttachmentsPath"]), locAttach.TemplateID + "" + locAttach.AttachmentId + locAttach.FileName);
                    var f = new FileStream(path, FileMode.Open, FileAccess.Read);
                    if (System.IO.File.Exists(path))
                    {
                        Attachment messageAttachment = new Attachment(f, locAttach.FileName);
                        messageAttachment.Name = locAttach.FileName;
                        mailAttachments.Add(messageAttachment);
                    }
                }
            else
            {
                form.AttachmentsList = new List<LocalAttachmetList>();
            }//body
            var formattedBody = bodyString.Replace("\r", "");
            //formattedBody = formattedBody.Replace("\n", "<br/>");
            string emailBody = System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Views/Prequalification/PrequalEmailFormat.htm"));
            emailBody = emailBody.Replace("ReplaceString", formattedBody);


            var emailStatus = FVGen3.Common.SendMail.sendMail(form.ToAddress, emailBody, form.Subject, form.BCCAddress, ccMail, mailAttachments);

            //Siva on Apr 7th
            if (emailStatus.Equals("Success"))
            {
                _prequalification.LogEmailNotification(form, SSession.UserId);

                return View("EmailSendingSuccesPage");
            }
            else
            {
                ViewBag.errorMessage = "Mail Sending Failed";
                return View(form);
            }
            //End of Siva on Apr 7th
        }

        [HttpPost]
        public ActionResult GetTemplatesClients(Guid ClientTemplateId, long preQualificationId)
        {
            var vendorId = entityDB.Prequalification.Find(preQualificationId).VendorId;
            var templateId = entityDB.ClientTemplates.Find(ClientTemplateId).TemplateID;
            var clientTemplates = entityDB.ClientTemplates.ToList().Where(rec => rec.TemplateID == templateId);


            List<SelectListItem> data = new List<SelectListItem>();
            foreach (var clientTemplate in clientTemplates.ToList())
            {
                foreach (var Pre in clientTemplate.Prequalification.Where(rec => rec.VendorId == vendorId && rec.PrequalificationStatusId >= 5))//To get all clients with present template and prequalification status >5
                {
                    SelectListItem item = new SelectListItem { Text = Pre.Client.Name, Value = Pre.ClientId + "" };
                    data.Add(item);
                }
            }

            return Json(data);

        }
        [HttpPost]
        public string ChangeDocStatus(Guid DocId, string status)
        {

            Document d = entityDB.Document.Find(DocId);
            // sumanth on 10/20/2015 for FV-220 starts
            d.StatusChangedBy = (Guid)Session["UserId"]; // sumanth on 10/20/2015 for FV-220 to display the user details that changed status of document
            d.StatusChangeDate = DateTime.Now; // sumanth on 10/20/2015 FV-220 to display the user details that changed status of document
            try
            {

                ViewBag.VBcontactDetails = " by " + entityDB.Contact.FirstOrDefault(rec => rec.SystemUsers.UserId == d.StatusChangedBy).LastName + " " + entityDB.Contact.FirstOrDefault(rec => rec.SystemUsers.UserId == d.StatusChangedBy).FirstName;
            }
            catch { ViewBag.VBcontactDetails = ""; }
            // sumanth ends on 10/20/2015
            d.DocumentStatus = status;
            d.ShowOnDashboard = false;
            entityDB.Entry(d).State = EntityState.Modified;
            entityDB.SaveChanges();
            _documents.RecordLog(DocId, SSession.UserId, DocumentModificationType.StatusChanged.ToString());
            return ViewBag.VBcontactDetails; // sumanth on 10/20/2015 for FV-220

        }
        [HttpPost]
        public ActionResult GetClientDocuments(long clientId, long preQualificationId, Guid ClientTemplateId)
        {
            var templateSupportedDocTypes = entityDB.ClientTemplates.Find(ClientTemplateId).ClientTemplateReportingData.Where(rec => rec.ReportingType == 2).Select(rec => rec.ReportingTypeId);

            var preQualification = entityDB.Prequalification.Find(preQualificationId);
            var vendorId = preQualification.VendorId;
            var PreId = entityDB.Prequalification.FirstOrDefault(rec => rec.ClientId == clientId && rec.VendorId == vendorId).PrequalificationId;

            List<SelectListItem> data = new List<SelectListItem>();
            foreach (var docs in entityDB.Document.Where(rec => rec.ReferenceType == "PreQualification" && rec.ReferenceRecordID == PreId && templateSupportedDocTypes.Contains(rec.DocumentTypeId)).ToList())
            {
                SelectListItem item = new SelectListItem { Text = docs.DocumentName.Replace(".pdf", ""), Value = docs.DocumentId + "" };
                data.Add(item);
            }
            return Json(data);
        }

        // Kiran on 12/14/2013
        //public ActionResult PostToPaypal(string TemplatePrice, string Client, string Vendor, long VendorId, Guid templateId, long TemplateSectionID, long PreQualificationId, int? mode)
        //{
        //    LocalPaypal paypal = new LocalPaypal();
        //    paypal.cmd = "_xclick";
        //    paypal.business = ConfigurationManager.AppSettings["BusinessAccountKey"];
        //    bool useSandbox = Convert.ToBoolean(ConfigurationManager.AppSettings["UseSandbox"]);
        //    if (useSandbox)
        //        ViewBag.actionURL = "https://www.sandbox.paypal.com/cgi-bin/webscr";
        //    else
        //        ViewBag.actionURL = "https://www.paypal.com/cgi-bin/webscr";

        //    paypal.cancel_return = ConfigurationManager.AppSettings["CancelURL"].ToString() + templateId + "&TemplateSectionID=" + TemplateSectionID + "&PreQualificationId=" + PreQualificationId + "&mode=" + mode;
        //    paypal.@return = ConfigurationManager.AppSettings["ReturnURL"].ToString() + templateId + "&TemplateSectionID=" + TemplateSectionID + "&PreQualificationId=" + PreQualificationId + "&mode=" + mode + "&paymentAmt=" + TemplatePrice; //Kiran 3/17/2014 
        //    paypal.notify_url = ConfigurationManager.AppSettings["NotifyURL"].ToString();
        //    paypal.currency_code = ConfigurationManager.AppSettings["CurrencyCode"];

        //    paypal.item_name = HttpUtility.HtmlDecode(HttpUtility.UrlDecode(Client + " Vendor Prequalification: " + Vendor + " (" + PreQualificationId + ")"));
        //    paypal.amount = TemplatePrice;
        //    paypal.preqID = PreQualificationId; // Kiran on 7/31/2014
        //    return View(paypal);
        //}
        // Ends<<<
        // Mani on 8/18/2015
        private decimal applyFeeCap(long vendorId, DateTime CurrentDate, long pqId, decimal totalAdditionalfee)
        {
            //OrganizationsPrequalificationOpenPeriod PreopenPeriod = entityDB.OrganizationsPrequalificationOpenPeriod.FirstOrDefault(rec => rec.VendorId == currentPrequalification.VendorId && rec.PrequalificationPeriodStart <= CurrentDate && rec.PrequalificationPeriodClose >= CurrentDate); ;
            //if (currentPrequalification.PrequalificationStatusId == 15)
            //{
            //    if (!(PreopenPeriod != null && PreopenPeriod.PrequalificationPeriodClose >= CurrentDate.AddDays(60)))

            //    {
            //        try
            //        {
            //            PreopenPeriod = entityDB.OrganizationsPrequalificationOpenPeriod.FirstOrDefault(rec => rec.VendorId == vendorId && rec.PrequalificationPeriodStart > PreopenPeriod.PrequalificationPeriodClose);
            //        }
            //        catch
            //        {
            //            PreopenPeriod = null;
            //        }
            //    }
            //}
            //long annualPeriodMaximumAmount = Convert.ToInt64(ConfigurationManager.AppSettings["PrequalificationAnnualPeriodAmount"]);

            //if (PreopenPeriod != null)
            //{


            //    if (annualPeriodMaximumAmount - PreopenPeriod.TotalPaymentRecieved <= totalAdditionalfee)
            //    {
            //        totalAdditionalfee = annualPeriodMaximumAmount - PreopenPeriod.TotalPaymentRecieved;
            //    }
            //}
            //if (totalAdditionalfee > annualPeriodMaximumAmount)
            //{
            //    totalAdditionalfee = annualPeriodMaximumAmount;
            //}
            //if (totalAdditionalfee <= 0)
            //{
            //    //ViewBag.DiscountPrice = Convert.ToDecimal(templatePrices.GroupPrice);
            //    totalAdditionalfee = 0;
            //}
            //return totalAdditionalfee;
            return _prequalification.ApplyFeeCap(vendorId, CurrentDate, pqId, totalAdditionalfee);
        }
        public ActionResult PostToPaypal(string TemplatePrice, string Client, string Vendor, long VendorId, Guid templateId, long TemplateSectionID, long PreQualificationId, int? mode, string skipType, string totalAmount)
        {
            cntr = 0;
            var currentPrequalification = entityDB.Prequalification.Find(PreQualificationId);
            LocalPaypal paypal = new LocalPaypal();
            paypal.cmd = "_xclick";
            paypal.business = ConfigurationManager.AppSettings["BusinessAccountKey"];
            bool useSandbox = Convert.ToBoolean(ConfigurationManager.AppSettings["UseSandbox"]);
            if (useSandbox)
                ViewBag.actionURL = "https://www.sandbox.paypal.com/cgi-bin/webscr";
            else
                ViewBag.actionURL = "https://www.paypal.com/cgi-bin/webscr";


            //int preqstartmonth = 0;
            //int todaymonth = 0;
            long preqStatusid = currentPrequalification.PrequalificationStatusId;
            var monthDiff = 0;
            //if (preqStatusid != 3 && preqStatusid != 15)
            //{
            //    preqstartmonth = currentPrequalification.PrequalificationStart.Month;
            //    todaymonth = int.Parse(DateTime.Today.Month.ToString());
            //}
            DateTime currentDate = DateTime.Now;
            var finalStatuses = _prequalification.Finalstatus();
            if (!finalStatuses.Any(r => r == preqStatusid))//if (preqStatusid == 3 || preqStatusid == 15)
            {
                monthDiff = 12;
            }
            else if (currentDate.Year > currentPrequalification.PrequalificationStart.Year)
            {
                monthDiff = currentPrequalification.PrequalificationStart.Month - currentDate.Month;
            }
            else //if (currentDate.Year == currentPrequalification.PrequalificationStart.Year)
            {
                monthDiff = 12 - currentDate.Month + currentPrequalification.PrequalificationStart.Month;
            }
            // Kiran on 08/26/2015
            //if ((todaymonth < preqstartmonth) || (todaymonth == currentPrequalification.PrequalificationFinish.Month && DateTime.Now.Year == currentPrequalification.PrequalificationFinish.Year))
            //{
            //    monthDiff = (currentPrequalification.PrequalificationFinish.Month - todaymonth) + 1;
            //}
            //else
            //{
            //    monthDiff = 12 - (todaymonth - preqstartmonth);
            //}
            ViewBag.monthdiff = monthDiff;
            if (currentDate.Month != currentPrequalification.PrequalificationStart.Month && DateTime.Now.Day >= 16 && preqStatusid != 3 && preqStatusid != 15)
            {
                monthDiff--;
            }
            // additional payment cal from db
            var clientTemplateIds = getPQClientTemplateId(currentPrequalification);
            var preqBunitIds = entityDB.PrequalificationSites.Where(rec => rec.PrequalificationId == PreQualificationId && (rec.PrequalificationPaymentsId == null || rec.PrequalificationPayments.PaymentCategory == 0)).Select(rec => rec.ClientBusinessUnitId).ToList();
            preqBunitIds = preqBunitIds.Distinct().ToList();
            var clientTemplatesforBu = entityDB.ClientTemplatesForBU
                .Where(rec => preqBunitIds.Contains(rec.ClientBusinessUnitId)
                && clientTemplateIds.Contains((Guid)rec.ClientTemplateId)
                && rec.ClientTemplatesBUGroupPrice.GroupPrice != 0
                && rec.ClientTemplatesBUGroupPrice.GroupingFor == 0
                && rec.ClientTemplatesBUGroupPrice.GroupPriceType != 0).ToList();
            decimal? additionalPayment = 0;
            additionalPayment = clientTemplatesforBu.Sum(rec => rec.ClientTemplatesBUGroupPrice.GroupPrice);
            if (currentPrequalification.ClientTemplates.Templates.IsERI)
            {
                additionalPayment = clientTemplatesforBu.Select(r => new { r.ClientTemplateBUGroupId, r.ClientTemplatesBUGroupPrice.GroupPrice }).Distinct().Sum(rec => rec.GroupPrice * monthDiff / 12);
                var selectedClients = clientTemplatesforBu.Select(r => r.ClientTemplates.Organizations.Name).Distinct();
                Client += "(" + string.Join(",", selectedClients) + ")";
            }
            additionalPayment = applyFeeCap(VendorId, DateTime.Now, PreQualificationId, additionalPayment ?? 0);
            additionalPayment = Math.Round(additionalPayment ?? 0, 2);
            var unPaidSites = entityDB.PrequalificationSites.Where(rec => rec.PrequalificationId == PreQualificationId && (rec.PrequalificationPaymentsId == null)).ToList();
            if (additionalPayment == 0 && unPaidSites != null && unPaidSites.Count != 0)
            {
                PrequalificationPayments zeroPQPayment = entityDB.PrequalificationPayments.FirstOrDefault(rec => rec.PrequalificationId == PreQualificationId && rec.PaymentCategory == 1 && rec.PaymentReceivedAmount == 0);
                if (zeroPQPayment == null)
                {
                    zeroPQPayment = new PrequalificationPayments();
                    zeroPQPayment.PaymentReceivedAmount = 0;
                    zeroPQPayment.InvoiceAmount = 0;
                    zeroPQPayment.PrequalificationId = PreQualificationId;
                    zeroPQPayment.PaymentCategory = 1;
                    zeroPQPayment.PaymentReceivedDate = DateTime.Now;
                    zeroPQPayment.SkipPayment = true;

                    entityDB.Entry(zeroPQPayment).State = EntityState.Added;
                }

                foreach (var site in unPaidSites)
                {
                    site.PrequalificationPaymentsId = zeroPQPayment.PrequalificationPaymentsId;
                    entityDB.Entry(site).State = EntityState.Modified;
                    entityDB.SaveChanges();
                }
            }
            if (additionalPayment == null)
                additionalPayment = 0;
            var additionBuIds = "";

            foreach (var templateBu in clientTemplatesforBu)
            {
                additionBuIds += templateBu.ClientBusinessUnitId.ToString() + ",";
            }
            if (additionBuIds != "")
                additionBuIds = additionBuIds.Substring(0, additionBuIds.Length - 1);
            // Annual payment cal from db //changed by sumanth on 23/06/2015 starts
            var annualFeeGroups = entityDB.ClientTemplatesBUGroupPrice.Where(rec => rec.ClientTemplateID == currentPrequalification.ClientTemplateId && rec.GroupingFor == 1 && rec.GroupPrice != 0).ToList();
            ViewBag.annualfeeBus = annualFeeGroups.Select(rec => rec.ClientTemplatesForBU).ToList();
            var allPreqSites = entityDB.PrequalificationSites.Where(rec => rec.PrequalificationId == PreQualificationId).Select(rec => rec.ClientBusinessUnitId).ToList();

            var groupIds = annualFeeGroups.Select(record => record.ClientTemplateBUGroupId).ToList();
            var annualFeeTemplateBuIds = entityDB.PrequalificationTrainingAnnualFees.Where(rec => rec.PrequalificationId == PreQualificationId).Select(rec => rec.ClientTemplateForBUId).ToList();
            var annualfeeGroupIds = entityDB.PrequalificationTrainingAnnualFees.Where(rec => rec.PrequalificationId == PreQualificationId).Select(rec => rec.ClientTemplateBUGroupId).Distinct().ToList();
            var clienttemplatesBuGroups = entityDB.ClientTemplatesForBU.Where(rec => groupIds.Contains((long)rec.ClientTemplateBUGroupId) && rec.ClientTemplateId == currentPrequalification.ClientTemplateId && allPreqSites.Contains(rec.ClientBusinessUnitId) && !annualfeeGroupIds.Contains((long)rec.ClientTemplateBUGroupId) && rec.ClientTemplatesBUGroupPrice.GroupingFor == 1).Select(rec => rec.ClientTemplatesBUGroupPrice).ToList();
            var distinctGroups = clienttemplatesBuGroups.Distinct().ToList();
            //changed by sumanth on 30/06/2015

            // Ends<<<
            decimal totalannualfee = 0;

            for (int i = 0; i < distinctGroups.Count; i++)
            {
                totalannualfee = (distinctGroups[i].GroupPrice.Value * ViewBag.monthdiff / 12) + totalannualfee;

            }
            ViewBag.totalAnnualFee = Math.Round(totalannualfee, 2, MidpointRounding.AwayFromZero);
            var AnnualFee = ViewBag.totalAnnualFee;
            // end >>
            var AnnualClientTemplatesBuIds = entityDB.ClientTemplatesForBU.Where(rec => rec.ClientTemplatesBUGroupPrice.GroupingFor == 1 && rec.ClientTemplatesBUGroupPrice.GroupPrice != 0 && rec.ClientTemplateId == currentPrequalification.ClientTemplateId && allPreqSites.Contains(rec.ClientBusinessUnitId) && !annualfeeGroupIds.Contains((long)rec.ClientTemplateBUGroupId)).Distinct().ToList();

            var annualTemplateBuIds = "";
            foreach (var templateBu in AnnualClientTemplatesBuIds)
            {
                annualTemplateBuIds += templateBu.ClientTemplateForBUId.ToString() + ",";
            }
            if (annualTemplateBuIds != "")
                annualTemplateBuIds = annualTemplateBuIds.Substring(0, annualTemplateBuIds.Length - 1);
            // Preq payment cal from db
            decimal tempPrice = 0;
            //if (currentPrequalification.TotalPaymentReceivedAmount == null )
            //{
            //    decimal? templatePrices = entityDB.ClientTemplatesBUGroupPrice.FirstOrDefault(rec => rec.ClientTemplateID == currentPrequalification.ClientTemplateId && rec.GroupPriceType == 0 && rec.GroupingFor == 0).GroupPrice;
            //    if (templatePrices != null)
            //        tempPrice = (decimal)templatePrices;
            //}
            tempPrice = getTemplatePrice(VendorId, currentPrequalification.ClientTemplates.ClientTemplateID, PreQualificationId);
            // Ends >>
            // updating the 0 fee group bu's with prequalification payment.
            var bu0GroupIds = entityDB.ClientTemplatesBUGroupPrice.Where(rec => rec.ClientTemplateID == currentPrequalification.ClientTemplateId && rec.GroupingFor == 0 && rec.GroupPrice != 0 && rec.GroupPriceType == 0).Select(rec => rec.ClientTemplateBUGroupId).ToList();
            var Group0BuIds = entityDB.ClientTemplatesForBU.Where(rec => bu0GroupIds.Contains((long)rec.ClientTemplateBUGroupId)).Select(rec => rec.ClientBusinessUnitId).ToList();
            var UnPaymentSites0group = entityDB.PrequalificationSites.Where(rec => Group0BuIds.Contains(rec.ClientBusinessUnitId) && rec.PrequalificationId == PreQualificationId && (rec.PrequalificationPaymentsId == null || rec.PrequalificationPayments.PaymentCategory == 1)).ToList();
            if (!skipType.Contains('1') && currentPrequalification.PaymentReceived == true && UnPaymentSites0group.Count != 0)
            {
                foreach (var site in UnPaymentSites0group)
                {
                    var paymentRec = entityDB.PrequalificationPayments.FirstOrDefault(rec => rec.PrequalificationId == PreQualificationId && rec.PaymentCategory == 0);
                    if (paymentRec != null)
                    {
                        site.PrequalificationPaymentsId = entityDB.PrequalificationPayments.FirstOrDefault(rec => rec.PrequalificationId == PreQualificationId && rec.PaymentCategory == 0).PrequalificationPaymentsId;
                        entityDB.Entry(site).State = EntityState.Modified;
                        entityDB.SaveChanges();
                    }
                }
            }
            //Ends >>>
            var finalstring = "";
            if (skipType.Contains('1'))
            {
                PrequalificationPayments skipPayment = entityDB.PrequalificationPayments.FirstOrDefault(rec => rec.PaymentCategory == 0 && rec.PrequalificationId == PreQualificationId);
                if (skipPayment == null)
                {
                    skipPayment = new PrequalificationPayments();
                    skipPayment.SkipPayment = true;
                    skipPayment.PrequalificationId = PreQualificationId;
                    skipPayment.PaymentReceivedDate = DateTime.Now;
                    skipPayment.PaymentCategory = 0;
                    skipPayment.PaymentReceivedAmount = tempPrice;
                    skipPayment.InvoiceAmount = entityDB.ClientTemplatesBUGroupPrice.FirstOrDefault(rec => rec.ClientTemplateID == currentPrequalification.ClientTemplateId && rec.GroupPriceType == 0 && rec.GroupingFor == 0).GroupPrice;
                    entityDB.PrequalificationPayments.Add(skipPayment);
                    helper.RecordFeecapLog(PreQualificationId, tempPrice, entityDB);
                    currentPrequalification.TotalPaymentReceivedAmount = tempPrice; // Kiran on 02/24/2015
                    //prequalification.PaymentReceivedDate = DateTime.Now;                                                                                                                                                                  

                }
                else
                {
                    currentPrequalification.TotalPaymentReceivedAmount += tempPrice;
                    entityDB.Entry(skipPayment).State = EntityState.Modified;
                }

                // for 0 group bu's update in preq.sites
                foreach (var site in UnPaymentSites0group)
                {
                    site.PrequalificationPaymentsId = skipPayment.PrequalificationPaymentsId;
                    entityDB.Entry(site).State = EntityState.Modified;
                    entityDB.SaveChanges();
                }
                //Ends>>
                currentPrequalification.PaymentReceived = true;//In Here we are doing skip payment
                entityDB.Entry(currentPrequalification).State = EntityState.Modified;
                entityDB.SaveChanges();

                tempPrice = 0;
            }
            if (skipType.Contains('2'))
            {

                PrequalificationPayments skipPayment = new PrequalificationPayments();
                skipPayment.SkipPayment = true;
                skipPayment.PrequalificationId = PreQualificationId;
                skipPayment.PaymentReceivedDate = DateTime.Now;
                skipPayment.PaymentCategory = 1;
                skipPayment.InvoiceAmount = additionalPayment;
                skipPayment.PaymentReceivedAmount = additionalPayment;
                entityDB.PrequalificationPayments.Add(skipPayment);
                helper.RecordFeecapLog(PreQualificationId, tempPrice, entityDB);
                entityDB.SaveChanges();
                var clienttemplate = currentPrequalification.ClientTemplates.Templates.ClientTemplates.Select(r => r.ClientTemplateID).ToList();
                // Adding templatebu's to sites and update payments for additional bu's
                var buGroupIds = entityDB.ClientTemplatesBUGroupPrice.Where(rec => clienttemplate.Contains((Guid)rec.ClientTemplateID) && rec.GroupingFor == 0 && rec.GroupPrice != 0 && rec.GroupPriceType != 0).Select(rec => rec.ClientTemplateBUGroupId).ToList();
                var buIds = entityDB.ClientTemplatesForBU.Where(rec => buGroupIds.Contains((long)rec.ClientTemplateBUGroupId)).Select(rec => rec.ClientBusinessUnitId).ToList();
                var UnPaymentSites = entityDB.PrequalificationSites.Where(rec => buIds.Contains(rec.ClientBusinessUnitId) && rec.PrequalificationId == PreQualificationId && (rec.PrequalificationPaymentsId == null || rec.PrequalificationPayments.PaymentCategory == 0)).ToList();

                foreach (var site in UnPaymentSites)
                {
                    site.PrequalificationPaymentsId = skipPayment.PrequalificationPaymentsId;
                    entityDB.Entry(site).State = EntityState.Modified;
                    entityDB.SaveChanges();
                }
                // Modification in prequalifcation table
                var previousAmount = currentPrequalification.TotalPaymentReceivedAmount;
                currentPrequalification.TotalPaymentReceivedAmount = previousAmount + additionalPayment; //Adding additional amount to the preq table
                currentPrequalification.PaymentReceived = true;//In Here we are doing skip payment
                entityDB.Entry(currentPrequalification).State = EntityState.Modified;
                entityDB.SaveChanges();

                helper.AccumulateFeeCap(PreQualificationId, entityDB, 1, additionalPayment ?? 0);

                additionalPayment = 0;// for skip payment make the cal fee as 0
            }
            if (skipType.Contains('3'))
            {

                foreach (var TemplateBu in AnnualClientTemplatesBuIds)
                {
                    // Sumanth on 06/24/2015
                    var clientTemplateBUGroupPrice = entityDB.ClientTemplatesBUGroupPrice.Find(TemplateBu.ClientTemplateBUGroupId).GroupPrice;
                    decimal preqannualfee = Math.Round(clientTemplateBUGroupPrice * ViewBag.monthdiff / 12, 2, MidpointRounding.AwayFromZero);
                    // Ends<<<

                    PrequalificationTrainingAnnualFees annualFee = new PrequalificationTrainingAnnualFees();
                    annualFee.PrequalificationId = PreQualificationId;
                    annualFee.SkipPayment = true;
                    annualFee.ClientTemplateBUGroupId = (long)TemplateBu.ClientTemplateBUGroupId;
                    annualFee.ClientTemplateForBUId = TemplateBu.ClientTemplateForBUId;
                    annualFee.FeeReceivedDate = DateTime.Now;

                    if (entityDB.PrequalificationTrainingAnnualFees.FirstOrDefault(rec => rec.PrequalificationId == PreQualificationId && rec.ClientTemplateBUGroupId == TemplateBu.ClientTemplateBUGroupId) == null)
                    {
                        annualFee.AnnualFeeAmount = preqannualfee;
                        annualFee.AnnualFeeReceivedAmount = preqannualfee;
                    }
                    else
                    {
                        annualFee.AnnualFeeAmount = 0;
                        annualFee.AnnualFeeReceivedAmount = 0;
                    }
                    entityDB.PrequalificationTrainingAnnualFees.Add(annualFee);
                    entityDB.SaveChanges();
                }
                //currentPrequalification.TotalAnnualFeeAmount += AnnualFee;
                if (currentPrequalification.TotalAnnualFeeReceived == null)
                    currentPrequalification.TotalAnnualFeeReceived = AnnualFee;
                else
                    currentPrequalification.TotalAnnualFeeReceived += AnnualFee;
                entityDB.Entry(currentPrequalification).State = EntityState.Modified;
                entityDB.SaveChanges();
                AnnualFee = 0;
            }
            decimal? totalPayment = tempPrice + additionalPayment + AnnualFee;
            decimal amountToPay = 0;
            var stringAmount = totalAmount.TrimStart('$');
            amountToPay = Convert.ToDecimal(stringAmount);
            var paymentSection = entityDB.PrequalificationCompletedSections.FirstOrDefault(rec => rec.PrequalificationId == PreQualificationId && rec.TemplateSectionId == TemplateSectionID && rec.TemplateSections.TemplateSectionType == 3);
            if (amountToPay == 0)
            {
                if (paymentSection == null)
                {
                    PrequalificationCompletedSections section = new PrequalificationCompletedSections();
                    section.PrequalificationId = PreQualificationId;
                    section.TemplateSectionId = TemplateSectionID;
                    section.SubmittedDate = DateTime.Now;
                    section.SubmittedBy = SSession.UserId;
                    entityDB.PrequalificationCompletedSections.Add(section);
                }
                //sandhya on 08/04/2015
                if (currentPrequalification.TotalPaymentReceivedAmount == null)
                {
                    currentPrequalification.TotalPaymentReceivedAmount = tempPrice;
                    currentPrequalification.PaymentReceived = true;
                    entityDB.Entry(currentPrequalification).State = EntityState.Modified;
                }
                //for 500^ having to pay is 0
                PrequalificationPayments paymentLog = entityDB.PrequalificationPayments.FirstOrDefault(rec => rec.PaymentCategory == 0 && rec.PrequalificationId == PreQualificationId);
                if (paymentLog == null)
                {
                    paymentLog = new PrequalificationPayments();
                    paymentLog.PrequalificationId = PreQualificationId;
                    paymentLog.PaymentReceivedDate = DateTime.Now;
                    paymentLog.PaymentCategory = 0;
                    paymentLog.PaymentReceivedAmount = tempPrice;
                    paymentLog.InvoiceAmount = entityDB.ClientTemplatesBUGroupPrice.FirstOrDefault(rec => rec.ClientTemplateID == currentPrequalification.ClientTemplateId && rec.GroupPriceType == 0 && rec.GroupingFor == 0).GroupPrice;
                    helper.RecordFeecapLog(PreQualificationId, tempPrice, entityDB);
                    entityDB.PrequalificationPayments.Add(paymentLog);
                }
                //Ends>>
                entityDB.SaveChanges();

                //var displayOrder = entityDB.TemplateSections.FirstOrDefault(rec => rec.TemplateID == templateId && rec.TemplateSectionID == TemplateSectionID).DisplayOrder;
                //var nextSection = entityDB.TemplateSections.FirstOrDefault(rec => rec.TemplateID == templateId && rec.DisplayOrder == displayOrder + 1).TemplateSectionID;
                //var nextSection = entityDB.TemplateSections.FirstOrDefault(rec => rec.TemplateID == templateId && rec.DisplayOrder > displayOrder + 1 && rec.Visible == true).TemplateSectionID;
                var nextSection = nextSectionId(PreQualificationId, TemplateSectionID);
                String strNextSectionURL = "../Prequalification/TemplateSectionsList?templateId=" + templateId + "&TemplateSectionID=" + nextSection + "&PreQualificationId=" + PreQualificationId;
                return Redirect(strNextSectionURL);
            }
            var discountPaymentRec = entityDB.PrequalificationPayments.FirstOrDefault(rec => rec.PaymentCategory == 0 && rec.PrequalificationId == PreQualificationId);
            paypal.custom = "";
            if ((!skipType.Contains('1') && tempPrice != 0) || (discountPaymentRec == null && tempPrice == 0))
                paypal.custom += "preq:" + tempPrice + "|";

            if (!skipType.Contains('2') && additionalPayment != 0 && additionBuIds != "")
                paypal.custom += "additional:" + additionBuIds + ";" + additionalPayment + "|";

            if (!skipType.Contains('3') && AnnualFee != 0 && annualTemplateBuIds != "")
                paypal.custom += "annual:" + annualTemplateBuIds + ";" + AnnualFee;
            var paymentString = paypal.custom;
            paypal.cancel_return = ConfigurationManager.AppSettings["CancelURL"].ToString() + QueryStringModule.Encrypt("templateid=" + templateId + "&TemplateSectionID=" + TemplateSectionID + "&PreQualificationId=" + PreQualificationId + "&mode=" + mode);

            //to give the access for next sections, passing only preq Template price(0 group)
            paypal.notify_url = ConfigurationManager.AppSettings["NotifyURL"].ToString();
            paypal.currency_code = ConfigurationManager.AppSettings["CurrencyCode"];

            paypal.item_name =
                HttpUtility.HtmlDecode(
                    HttpUtility.UrlDecode(Client + " Total Payment: " + Vendor + " (" + PreQualificationId + ")"));
            paypal.amount = totalPayment.ToString();

            paypal.preqID = PreQualificationId; // Kiran on 7/31/2014


            string returnURL = ConfigurationManager.AppSettings["ReturnURL"];
            string returnParams = "templateid=" + templateId + "&TemplateSectionID=" + TemplateSectionID + "&PreQualificationId=" + PreQualificationId + "&mode=" + mode + "&paymentAmt=" + tempPrice + "&payString=" + paymentString;
            string url = getPaymentURL(paypal, returnURL, returnParams);
            return Redirect(url);
        }
        //return View(paypal);



        private long nextSectionId(long prequalificationId, long currentSectionId)
        {
            var hideSectionsToDisplay = entityDB.PrequalificationSectionsToDisplay.Where(r => r.PrequalificationId == prequalificationId && r.Visible == true).Select(r => r.TemplateSectionId);
            var prequalification = entityDB.Prequalification.Find(prequalificationId);
            var tempSections =
                prequalification.ClientTemplates.Templates.TemplateSections.Where(rec => rec.Visible)
                    .OrderBy(rec => rec.DisplayOrder);

            //Rajesh on 1/21/2014
            if (Session["RoleName"].ToString().Equals("Client") || SSession.Role.Equals(OrganizationType.SuperClient))
            {
                long[] prequalificationStatus = { };//Requested on FV-1012
                int status = Array.IndexOf(prequalificationStatus, prequalification.PrequalificationStatusId);
                if (status != -1)
                    tempSections =
                        prequalification.ClientTemplates.Templates.TemplateSections.Where(
                            rec => rec.Visible && rec.VisibleToClient == true).OrderBy(rec => rec.DisplayOrder);

            }
            //Ends<<<
            //Rajesh on 2/13/2014
            if (Session["RoleName"].ToString().Equals("Vendor")) //If Vendor Login
            {

                var allVisibleTemplateSections =
                    entityDB.TemplateSectionsPermission.Where(
                            rec => rec.VisibleTo == 3 || rec.VisibleTo == 1 || rec.VisibleTo == 5)
                        .Select(rec => rec.TemplateSectionID)
                        .ToList();
                Guid userId = new Guid(Session["UserId"].ToString());
                //var userhasRestrictedSectionAccess = FVGen3.BusinessLogic.RolesHelper.IsUserInRole(userId, 1001, "read", preQualificationDetails.ClientId, preQualificationDetails.VendorId);
                ViewBag.RestrictedSectionEdit = FVGen3.BusinessLogic.RolesHelper.IsUserInRole(userId, 1001, "read",
                    prequalification.ClientId, prequalification.VendorId);
                tempSections =
                    prequalification.ClientTemplates.Templates.TemplateSections.Where(
                            rec => rec.Visible == true && (rec.HideFromPrequalification != true || hideSectionsToDisplay.Contains(rec.TemplateSectionID)) && allVisibleTemplateSections.Contains(rec.TemplateSectionID))
                        .OrderBy(rec => rec.DisplayOrder);
            }
            //var longSecid = tempSections[tempSections.IndexOf(tempSections.FirstOrDefault(rec => rec.TemplateSectionID == currentSectionId)) + 1].TemplateSectionID;
            var sections = tempSections.Select(rec => rec.TemplateSectionID).ToList();
            //Assuming this is not a last record.bcz this logic for payment next section.
            var longSecid = sections[sections.IndexOf(sections.FirstOrDefault(rec => rec == currentSectionId)) + 1];
            return longSecid;
        }

        public ActionResult Testing()
        {
            return View();
        }
        // Kiran on 07/16/2014
        [HttpPost]
        public ActionResult NotifyURLForMerchant()
        {
            var error = "";
            try
            {
                byte[] param = Request.BinaryRead(Request.ContentLength);
                string strRequest = Encoding.ASCII.GetString(param);

                // append PayPal verification code to end of string
                strRequest += "&cmd=_notify-validate";

                // create an HttpRequest channel to perform handshake with PayPal
                // Kiran on 7/30/2014
                bool useSandbox = Convert.ToBoolean(ConfigurationManager.AppSettings["UseSandbox"]);
                HttpWebRequest req;
                if (useSandbox)
                    req = (HttpWebRequest)WebRequest.Create(@"https://ipnpb.sandbox.paypal.com/cgi-bin/webscr");
                else
                    req = (HttpWebRequest)WebRequest.Create(@"https://ipnpb.paypal.com/cgi-bin/webscr");
                // Ends<<<
                req.Method = "POST";
                req.ContentType = "application/x-www-form-urlencoded";
                req.ContentLength = strRequest.Length;

                StreamWriter streamOut = new StreamWriter(req.GetRequestStream(), Encoding.ASCII);
                streamOut.Write(strRequest);
                streamOut.Close();

                // receive response from PayPal
                StreamReader streamIn = new StreamReader(req.GetResponse().GetResponseStream());
                string strResponse = streamIn.ReadToEnd();
                streamIn.Close();

                //string fileName1 = DateTime.Now.ToString("MMddyyyyHHmmssfff");

                //Directory.CreateDirectory(
                //    Path.Combine(
                //        HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["PaypalNotification"])));
                //var filePath1 =
                //    Path.Combine(
                //        HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["PaypalNotification"]),
                //        fileName1 + ".txt");

                //System.IO.File.WriteAllText(filePath1, strRequest + "\n" + strResponse);

                string txn_id = Request.Form["txn_id"];
                // if PayPal response is successful / verified
                if (strResponse.Equals("VERIFIED"))
                {
                    // paypal has verified the data, it is safe for us to perform processing now

                    // extract the form fields expected: buyer and seller email, payment status, amount
                    string payerEmail = Request.Form["payer_email"];
                    string paymentStatus = Request.Form["payment_status"];
                    string receiverEmail = Request.Form["receiver_email"];
                    string metaData = Request.Form["mc_gross"]; // Kiran on 7/30/2014
                    string transactionId = Request.Form["txn_id"]; // Kiran on 7/30/2014
                    string custom = Request.Form["custom"];
                    //long preqId = Convert.ToInt64(Request.Form["preqID"]);
                    string paymentAmount = Request.Form["mc_gross"];
                    string itemName = HttpUtility.HtmlDecode(Request.Form["item_name1"]);
                    int index = itemName.IndexOf("(");
                    string paymentDate = Request.Form["payment_date"]; // Kiran on 12/07/2015
                                                                       //int itemNameLength = itemName.Length;
                                                                       //long prequalificationId = Convert.ToInt64(itemName.Substring(index + 1, itemNameLength - 1));
                    long prequalificationId = Convert.ToInt64(itemName.Substring(index + 1).Replace(")", ""));
                    //Rajesh on 8/12/2014
                    //if (paymentStatus.Equals("Completed"))
                    //{
                    //    // Kiran on 7/30/2014
                    //    var prequalRecord = entityDB.Prequalification.FirstOrDefault(rec => rec.PrequalificationId == prequalificationId);
                    //    //prequalRecord.PaymentTransactionId = transactionId;
                    //    //prequalRecord.TransactionMetaData = strRequest;//metaData;//Rajesh on 8/12/2014
                    //    if (prequalRecord.PaymentReceived == false || prequalRecord.PaymentReceived == null)
                    //    {
                    //        prequalRecord.PaymentReceived = true;
                    //        prequalRecord.TotalPaymentReceivedAmount = Convert.ToDecimal(paymentAmount);
                    //        //prequalRecord.PaymentReceivedDate = DateTime.Now;
                    //    }
                    //    entityDB.Entry(prequalRecord).State = EntityState.Modified;
                    //    entityDB.SaveChanges();
                    //    // Ends<<<
                    //}
                    if (paymentStatus.Equals("Pending") || paymentStatus.Equals("Completed"))
                    {

                        if (!string.IsNullOrEmpty(custom))
                        {
                            var prequalRecord =
                                entityDB.Prequalification.FirstOrDefault(rec => rec.PrequalificationId == prequalificationId);

                            foreach (var sections in custom.Split('|'))
                            {
                                if (sections.Contains("preq"))
                                {
                                    var prequal = sections.Split(':');

                                    // Kiran on 02/24/2015
                                    PrequalificationPayments paymentLog =
                                        entityDB.PrequalificationPayments.FirstOrDefault(
                                            rec => rec.PrequalificationId == prequalificationId && rec.PaymentCategory == 0);
                                    var cliettemplates = prequalRecord.ClientTemplates.Templates.ClientTemplates.Select(r => r.ClientTemplateID).ToList();
                                    // Kiran on 10/16/2015 to handle the payment log issue when the user closes the browser after making payment
                                    if (paymentLog == null)
                                    {
                                        paymentLog = new PrequalificationPayments();
                                        paymentLog.InvoiceAmount =
                                            entityDB.ClientTemplatesBUGroupPrice.FirstOrDefault(
                                                rec =>
                                                    cliettemplates.Contains((Guid)rec.ClientTemplateID) &&
                                                    rec.GroupPriceType == 0 && rec.GroupingFor == 0).GroupPrice;
                                        paymentLog.PaymentReceivedAmount = Convert.ToDecimal(prequal[1]);
                                        paymentLog.PaymentReceivedDate = DateTime.Now;
                                        paymentLog.PaymentCategory = 0;
                                        paymentLog.PaymentTransactionId = transactionId;
                                        paymentLog.PrequalificationId = prequalificationId;
                                        paymentLog.TransactionMetaData = strRequest;
                                        prequalRecord.PaymentReceived = true;
                                        prequalRecord.TotalPaymentReceivedAmount = Convert.ToDecimal(prequal[1]);
                                        entityDB.Entry(paymentLog).State = EntityState.Added;
                                        helper.RecordFeecapLog(prequalificationId,
                                            prequalRecord.TotalPaymentReceivedAmount ?? 0, entityDB);
                                        // for 0 group bu's update in preq.sites
                                        var buGroupIds =
                                            entityDB.ClientTemplatesBUGroupPrice.Where(
                                                    rec =>
                                                        rec.ClientTemplateID == prequalRecord.ClientTemplateId &&
                                                        rec.GroupingFor == 0 && rec.GroupPrice != 0 && rec.GroupPriceType == 0)
                                                .Select(rec => rec.ClientTemplateBUGroupId)
                                                .ToList();
                                        var buIds =
                                            entityDB.ClientTemplatesForBU.Where(
                                                    rec => buGroupIds.Contains((long)rec.ClientTemplateBUGroupId))
                                                .Select(rec => rec.ClientBusinessUnitId)
                                                .ToList();
                                        var UnPaymentSites =
                                            entityDB.PrequalificationSites.Where(
                                                rec =>
                                                    buIds.Contains(rec.ClientBusinessUnitId) &&
                                                    rec.PrequalificationId == prequalificationId &&
                                                    rec.PrequalificationPaymentsId == null).ToList();

                                        foreach (var site in UnPaymentSites)
                                        {
                                            site.PrequalificationPaymentsId = paymentLog.PrequalificationPaymentsId;
                                            entityDB.Entry(site).State = EntityState.Modified;
                                            entityDB.SaveChanges();
                                        }
                                        entityDB.Entry(prequalRecord).State = EntityState.Modified;
                                        entityDB.SaveChanges();
                                    }
                                    else
                                    {
                                        paymentLog.PaymentTransactionId = transactionId;
                                        paymentLog.TransactionMetaData = strRequest;

                                        entityDB.Entry(paymentLog).State = EntityState.Modified;

                                        //prequalRecord.TotalPaymentReceivedAmount = Convert.ToDecimal(prequal[1]);
                                        //entityDB.Entry(prequalRecord).State = EntityState.Modified;
                                        entityDB.SaveChanges();
                                    }
                                    //  Kiran on 10/16/2015 Ends<<<
                                }
                                else if (sections.Contains("additional"))
                                {
                                    var additionalsplit = sections.Split(':');
                                    var busplit = additionalsplit[1].Split(';');

                                    var buid = busplit[0].Split(',')[0];
                                    long preqSitesBuId = Convert.ToInt64(buid);
                                    var prequalificationPaymentId =
                                        entityDB.PrequalificationSites.FirstOrDefault(
                                            rec =>
                                                rec.ClientBusinessUnitId == preqSitesBuId &&
                                                rec.PrequalificationId == prequalificationId).PrequalificationPaymentsId;

                                    PrequalificationPayments paymentLog =
                                        entityDB.PrequalificationPayments.FirstOrDefault(
                                            rec =>
                                                rec.PrequalificationId == prequalificationId &&
                                                rec.PrequalificationPaymentsId == prequalificationPaymentId &&
                                                rec.PaymentCategory == 1);

                                    // Kiran on 10/16/2015 to handle the payment log issue when the user closes the browser after making payment
                                    if (paymentLog == null)
                                    {
                                        paymentLog = new PrequalificationPayments();
                                        paymentLog.InvoiceAmount = Convert.ToDecimal(busplit[1]);
                                        paymentLog.PaymentReceivedAmount = Convert.ToDecimal(busplit[1]);
                                        paymentLog.PaymentReceivedDate = DateTime.Now;
                                        paymentLog.PaymentCategory = 1;
                                        paymentLog.PaymentTransactionId = transactionId;
                                        paymentLog.PrequalificationId = prequalificationId;
                                        paymentLog.TransactionMetaData = strRequest;

                                        entityDB.Entry(paymentLog).State = EntityState.Added;

                                        helper.RecordFeecapLog(prequalificationId, paymentLog.PaymentReceivedAmount ?? 0,
                                            entityDB);
                                        entityDB.SaveChanges();

                                        foreach (var businessUniId in busplit[0].Split(','))
                                        {
                                            if (!string.IsNullOrEmpty(businessUniId))
                                            {
                                                long preqSiteBuId = Convert.ToInt64(buid);
                                                var prequalificationSites =
                                                    entityDB.PrequalificationSites.Where(
                                                        rec =>
                                                            rec.ClientBusinessUnitId == preqSiteBuId &&
                                                            rec.PrequalificationId == prequalificationId).ToList();
                                                foreach (var prequalSite in prequalificationSites)
                                                {
                                                    prequalSite.PrequalificationPaymentsId =
                                                        paymentLog.PrequalificationPaymentsId;
                                                    entityDB.Entry(prequalSite).State = EntityState.Modified;
                                                    entityDB.SaveChanges();
                                                }
                                            }
                                        }
                                        prequalRecord.TotalPaymentReceivedAmount += Convert.ToDecimal(busplit[1]);
                                        entityDB.Entry(prequalRecord).State = EntityState.Modified;
                                        entityDB.SaveChanges();
                                    }
                                    else
                                    {
                                        paymentLog.PaymentTransactionId = transactionId;
                                        paymentLog.TransactionMetaData = strRequest;
                                        entityDB.Entry(paymentLog).State = EntityState.Modified;

                                        entityDB.SaveChanges();
                                    }
                                    helper.AccumulateFeeCap(prequalificationId, entityDB, 1, Convert.ToDecimal(Convert.ToDecimal(busplit[1])));
                                    // Kiran on 10/16/2015 Ends<<<
                                }
                                else if (sections.Contains("annual"))
                                {
                                    // kiran on 10/16/2015 to handle issue when user closes the browser 
                                    var annualsplit = sections.Split(':');
                                    var busplit = annualsplit[1].Split(';');
                                    int preqstartmonth = 0;
                                    int todaymonth = 0;

                                    var currentDate = DateTime.Now;
                                    //foreach (var templateBuid in busplit[0].Split(','))
                                    //{
                                    //    long localTemplateBuId = Convert.ToInt64(templateBuid);

                                    //    PrequalificationTrainingAnnualFees annualFee = entityDB.PrequalificationTrainingAnnualFees.FirstOrDefault(rec => rec.ClientTemplateForBUId == localTemplateBuId && rec.PrequalificationId == prequalificationId);

                                    //    annualFee.FeeTransactionId = transactionId;
                                    //    annualFee.FeeTransactionMetaData = strRequest;
                                    //    entityDB.Entry(annualFee).State = EntityState.Modified;
                                    //    entityDB.SaveChanges();
                                    //}


                                    // Sumanth on 06/24/2015

                                    long preqStatusid = prequalRecord.PrequalificationStatusId;
                                    if (preqStatusid != 3 && preqStatusid != 15 && preqStatusid != 4 && preqStatusid != 7 &&
                                        preqStatusid != 10 && preqStatusid != 23) // Kiran on 09/11/2015
                                    {
                                        preqstartmonth =
                                            entityDB.Prequalification.FirstOrDefault(
                                                    rec => rec.PrequalificationId == prequalificationId)
                                                .PrequalificationStart.Month;
                                        todaymonth = int.Parse(DateTime.Today.Month.ToString());
                                    } // Ends<<<
                                      // Kiran on 08/26/2015
                                    var finalStatuses = _prequalification.Finalstatus();
                                    if (!finalStatuses.Any(r => r == preqStatusid))//if (preqStatusid == 3 || preqStatusid == 15)
                                    {
                                        ViewBag.monthdiff = 12;
                                    }
                                    else if (currentDate.Year > prequalRecord.PrequalificationStart.Year)
                                    {
                                        ViewBag.monthdiff = prequalRecord.PrequalificationStart.Month - currentDate.Month;
                                    }
                                    else //if (currentDate.Year == currentPrequalification.PrequalificationStart.Year)
                                    {
                                        ViewBag.monthdiff = 12 - currentDate.Month + prequalRecord.PrequalificationStart.Month;
                                    }
                                    //if ((todaymonth < preqstartmonth) ||
                                    //    (todaymonth == prequalRecord.PrequalificationFinish.Month &&
                                    //     DateTime.Now.Year == prequalRecord.PrequalificationFinish.Year))
                                    //{
                                    //    ViewBag.monthdiff = (prequalRecord.PrequalificationFinish.Month - todaymonth) + 1;
                                    //}
                                    //else
                                    //{
                                    //    ViewBag.monthdiff = 12 - (todaymonth - preqstartmonth);
                                    //}
                                    // Ends<<<
                                    PrequalificationTrainingAnnualFees record = new PrequalificationTrainingAnnualFees();

                                    foreach (var templateBuid in busplit[0].Split(','))
                                    {
                                        long localTemplateBuId = Convert.ToInt64(templateBuid);
                                        var cliettemplateforBu =
                                            entityDB.ClientTemplatesForBU.FirstOrDefault(
                                                rec => rec.ClientTemplateForBUId == localTemplateBuId);
                                        // Sumanth on 06/24/2015
                                        var clientTemplateBUGroupPrice =
                                            entityDB.ClientTemplatesBUGroupPrice.Find(
                                                cliettemplateforBu.ClientTemplateBUGroupId).GroupPrice;
                                        decimal preqannualfee =
                                            Math.Round(clientTemplateBUGroupPrice * ViewBag.monthdiff / 12, 2,
                                                MidpointRounding.AwayFromZero);
                                        // Ends<<<
                                        record =
                                            entityDB.PrequalificationTrainingAnnualFees.FirstOrDefault(
                                                rec =>
                                                    rec.ClientTemplateBUGroupId ==
                                                    cliettemplateforBu.ClientTemplateBUGroupId &&
                                                    rec.PrequalificationId == prequalificationId &&
                                                    rec.ClientTemplateForBUId == localTemplateBuId);
                                        var recExist =
                                            entityDB.PrequalificationTrainingAnnualFees.FirstOrDefault(
                                                rec =>
                                                    rec.ClientTemplateBUGroupId ==
                                                    cliettemplateforBu.ClientTemplateBUGroupId &&
                                                    rec.PrequalificationId == prequalificationId);
                                        PrequalificationTrainingAnnualFees annualFee =
                                                entityDB.PrequalificationTrainingAnnualFees.FirstOrDefault(
                                                    rec =>
                                                        rec.ClientTemplateForBUId == localTemplateBuId &&
                                                        rec.PrequalificationId == prequalificationId);
                                        // kiran on 09/28/2015 to handle issue when user closes the browser
                                        if (record == null)
                                        {
                                            annualFee = new PrequalificationTrainingAnnualFees();
                                            if (recExist == null)
                                            {
                                                // Sumanth on 06/24/2015
                                                annualFee.AnnualFeeAmount = preqannualfee;
                                                annualFee.AnnualFeeReceivedAmount = preqannualfee;
                                                // Ends<<<
                                            }
                                            else
                                            {
                                                annualFee.AnnualFeeAmount = 0;
                                                annualFee.AnnualFeeReceivedAmount = 0;
                                            }
                                            annualFee.PrequalificationId = prequalificationId;
                                            annualFee.ClientTemplateForBUId = cliettemplateforBu.ClientTemplateForBUId;
                                            annualFee.ClientTemplateBUGroupId =
                                                (long)cliettemplateforBu.ClientTemplateBUGroupId;
                                            annualFee.FeeReceivedDate = DateTime.Now;
                                            // Kiran on 12/07/2015
                                            annualFee.FeeTransactionId = transactionId;
                                            annualFee.FeeTransactionMetaData = strRequest;
                                            // Kiran on 12/07/2015 ends
                                            entityDB.PrequalificationTrainingAnnualFees.Add(annualFee);
                                            entityDB.SaveChanges();
                                        }
                                        else
                                        {
                                            annualFee.FeeTransactionId = transactionId;
                                            annualFee.FeeTransactionMetaData = strRequest;
                                            entityDB.Entry(annualFee).State = EntityState.Modified;
                                            entityDB.SaveChanges();
                                        }
                                    }
                                    if (record == null)
                                    {
                                        if (prequalRecord.TotalAnnualFeeReceived != null)
                                            prequalRecord.TotalAnnualFeeReceived += Convert.ToDecimal(busplit[1]);
                                        else
                                            prequalRecord.TotalAnnualFeeReceived = Convert.ToDecimal(busplit[1]);
                                        entityDB.Entry(prequalRecord).State = EntityState.Modified;
                                        entityDB.SaveChanges();
                                    }
                                    // Kiran on 10/16/2015 Ends<<<
                                }
                            }

                            //Kiran on 12/07/2015 for FV-227
                            var VendorDetails = prequalRecord.Vendor.Name + " (" + prequalRecord.PrequalificationId + ") ";
                            string body =
                                System.IO.File.ReadAllText(
                                    System.Web.HttpContext.Current.Server.MapPath(
                                        "~/Views/Prequalification/EMailFormatOfPaymentReceived.htm"));
                            body = body.Replace("VendorDetails", VendorDetails);
                            body = body.Replace("TotalAmount", paymentAmount);
                            body = body.Replace("TxnId", transactionId);
                            body = body.Replace("PaidDate", paymentDate);
                            body = body.Replace("Client", prequalRecord.Client.Name);


                            //Kiran on 12/07/2015 for FV-227 ends

                            string fileName = txn_id;

                            Directory.CreateDirectory(
                                Path.Combine(
                                    HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["PaypalNotification"])));
                            var filePath =
                                Path.Combine(
                                    HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["PaypalNotification"]),
                                    fileName + ".txt");

                            System.IO.File.WriteAllText(filePath, strRequest + "\n" + strResponse);

                        }
                        // Ends<<<
                    }

                    // else
                    else
                    {

                        string fileName = txn_id ?? DateTime.Now.ToString("MMddyyyyHHmmss");

                        Directory.CreateDirectory(
                            Path.Combine(
                                HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["PaypalNotificationFail"])));
                        var filePath =
                            Path.Combine(
                                HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["PaypalNotificationFail"]),
                                fileName + ".txt");

                        System.IO.File.WriteAllText(filePath, strRequest + "\n" + strResponse);

                        // payment not complete yet, may be undergoing additional verification or processing

                        // do nothing - wait for paypal to send another IPN when payment is complete
                    }

                }
            }
            catch (Exception ee)
            {
                error = ee.Message;
                string fileName2 = DateTime.Now.ToString("MMddyyyyHHmmssfff");
                Directory.CreateDirectory(
                                  Path.Combine(
                                      HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["PaypalNotification"])));
                var filePath2 =
                    Path.Combine(
                        HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["PaypalNotification"]),
                        fileName2 + ".txt");

                System.IO.File.WriteAllText(filePath2, error);
                string toEmail = ConfigurationManager.AppSettings["EmailForPaymentNotice"];
                Utilities.SendMail.sendMail(toEmail, Request.Form["txn_id"],
                    "" + Request.Form["item_name1"] + " - Payment Crash");
                // To get the ipn history(last 28 days only) - https://www.paypal.com/us/cgi-bin/webscr?cmd=_display-ipns-history
            }
            return View();
        }

        // Ends<<<





        // kiran on 2/12/2014

        //public ActionResult UpdatePaymentDetails(Guid templateId, long TemplateSectionID, long PreQualificationId, int? mode, decimal paymentAmt)//3/17/2014
        //{
        //    string redirectionURL = "";
        //    var prequalificationRecord = entityDB.Prequalification.Find(PreQualificationId);
        //    prequalificationRecord.PaymentReceived = true;
        //    // Kiran on 3/17/2014 Changes by mani for fvos-39 on 8/6/2015
        //    if (prequalificationRecord.PaymentReceivedAmount == null)
        //        prequalificationRecord.PaymentReceivedAmount = paymentAmt;
        //    else
        //        prequalificationRecord.PaymentReceivedAmount += paymentAmt;
        //    prequalificationRecord.PaymentReceivedDate = DateTime.Now;
        //    // Ends<<<
        //    entityDB.Entry(prequalificationRecord).State = EntityState.Modified;
        //    entityDB.SaveChanges();
        //    redirectionURL = "TemplateSectionsList?templateId=" + templateId + "&TemplateSectionID=" + TemplateSectionID + "&PreQualificationId=" + PreQualificationId + "&mode=" + mode;
        //    return Redirect(redirectionURL);
        //}
        public static int cntr = 0; // this counter is used to handle paypal redirection to smi application payment page within and after 10 sec // sumanth 18/08/2015
        public ActionResult UpdatePaymentDetails(Guid templateId, long TemplateSectionID, long PreQualificationId, int? mode, decimal paymentAmt, string payString, string guid)//3/17/2014
        {
            PayPal.Sample.Common.ExecutePayment(Request.Params["PayerID"], Session[guid] as string);
            string redirectionURL = "";
            if (cntr == 0)
            {
                cntr = 1;
                var prequalificationRecord = entityDB.Prequalification.Find(PreQualificationId);
                prequalificationRecord.PaymentReceived = true;
                // Kiran on 3/17/2014
                //prequalificationRecord.TotalPaymentReceivedAmount += paymentAmt;

                // updating the 0 fee group bu's with prequalification payment.
                var buGroupIds = entityDB.ClientTemplatesBUGroupPrice.Where(rec => rec.ClientTemplateID == prequalificationRecord.ClientTemplateId && rec.GroupingFor == 0 && rec.GroupPrice != 0 && rec.GroupPriceType == 0).Select(rec => rec.ClientTemplateBUGroupId).ToList();
                var buIds = entityDB.ClientTemplatesForBU.Where(rec => buGroupIds.Contains((long)rec.ClientTemplateBUGroupId)).Select(rec => rec.ClientBusinessUnitId).ToList();
                var UnPaymentSites = entityDB.PrequalificationSites.Where(rec => buIds.Contains(rec.ClientBusinessUnitId) && rec.PrequalificationId == PreQualificationId && rec.PrequalificationPaymentsId == null).ToList();
                PrequalificationPayments preqPaymentLog = entityDB.PrequalificationPayments.FirstOrDefault(rec => rec.PrequalificationId == PreQualificationId && rec.PaymentCategory == 0);
                if (preqPaymentLog != null)
                {
                    foreach (var site in UnPaymentSites)
                    {
                        site.PrequalificationPaymentsId = preqPaymentLog.PrequalificationPaymentsId;
                        entityDB.Entry(site).State = EntityState.Modified;
                        entityDB.SaveChanges();
                    }
                }
                //Ends>>

                // Mani on 6/22/2015
                foreach (var sections in payString.Split('|'))
                {
                    if (sections.Contains("preq"))
                    {
                        var prequal = sections.Split(':');

                        //if (prequalificationRecord.PaymentReceived == false || prequalificationRecord.PaymentReceived == null)
                        //{
                        prequalificationRecord.PaymentReceived = true;

                        //prequalRecord.PaymentReceivedDate = DateTime.Now;
                        //}

                        PrequalificationPayments paymentLog = entityDB.PrequalificationPayments.FirstOrDefault(rec => rec.PrequalificationId == PreQualificationId && rec.PaymentCategory == 0);
                        if (paymentLog == null)
                        {
                            paymentLog = new PrequalificationPayments();

                            try
                            {
                                paymentLog.InvoiceAmount = entityDB.ClientTemplatesBUGroupPrice.FirstOrDefault(rec => rec.ClientTemplateID == prequalificationRecord.ClientTemplateId && rec.GroupPriceType == 0 && rec.GroupingFor == 0).GroupPrice;
                            }
                            catch { }
                            paymentLog.PaymentReceivedAmount = Convert.ToDecimal(prequal[1]);
                            paymentLog.PaymentReceivedDate = DateTime.Now;
                            paymentLog.PaymentCategory = 0;
                            //paymentLog.PaymentTransactionId = transactionId;
                            paymentLog.PrequalificationId = PreQualificationId;
                            //paymentLog.TransactionMetaData = strRequest;
                            prequalificationRecord.TotalPaymentReceivedAmount = Math.Round(Convert.ToDecimal(prequal[1]), 2);
                            entityDB.Entry(paymentLog).State = EntityState.Added;
                            helper.RecordFeecapLog(PreQualificationId, paymentLog.PaymentReceivedAmount ?? 0, entityDB);
                            // for 0 group bu's update in preq.sites
                            foreach (var site in UnPaymentSites)
                            {
                                site.PrequalificationPaymentsId = paymentLog.PrequalificationPaymentsId;
                                entityDB.Entry(site).State = EntityState.Modified;
                                entityDB.SaveChanges();
                            }
                        }
                        else
                        {
                            paymentLog.PaymentReceivedAmount = Convert.ToDecimal(prequal[1]);
                            paymentLog.PaymentReceivedDate = DateTime.Now;
                            entityDB.Entry(paymentLog).State = EntityState.Modified;
                            prequalificationRecord.TotalPaymentReceivedAmount = Math.Round(Convert.ToDecimal(prequal[1]), 2);
                        }
                        entityDB.Entry(prequalificationRecord).State = EntityState.Modified;
                        entityDB.SaveChanges();
                    }
                    else if (sections.Contains("additional"))
                    {
                        var additionalsplit = sections.Split(':');
                        var busplit = additionalsplit[1].Split(';');

                        PrequalificationPayments paymentLog = new PrequalificationPayments();
                        paymentLog.InvoiceAmount = Convert.ToDecimal(busplit[1]);
                        paymentLog.PaymentReceivedAmount = Convert.ToDecimal(busplit[1]);
                        paymentLog.PaymentReceivedDate = DateTime.Now;
                        paymentLog.PaymentCategory = 1;
                        //paymentLog.PaymentTransactionId = transactionId;
                        paymentLog.PrequalificationId = PreQualificationId;
                        //paymentLog.TransactionMetaData = strRequest;

                        helper.RecordFeecapLog(PreQualificationId, paymentLog.PaymentReceivedAmount ?? 0, entityDB);
                        entityDB.Entry(paymentLog).State = EntityState.Added;

                        entityDB.SaveChanges();

                        foreach (var buid in busplit[0].Split(','))
                        {
                            if (!string.IsNullOrEmpty(buid))
                            {
                                long preqSitesBuId = Convert.ToInt64(buid);
                                var prequalificationSites = entityDB.PrequalificationSites.Where(rec => rec.ClientBusinessUnitId == preqSitesBuId && rec.PrequalificationId == PreQualificationId).ToList();
                                foreach (var prequalSite in prequalificationSites)
                                {
                                    prequalSite.PrequalificationPaymentsId = paymentLog.PrequalificationPaymentsId;
                                    entityDB.Entry(prequalSite).State = EntityState.Modified;
                                    entityDB.SaveChanges();
                                }
                            }
                        }
                        prequalificationRecord.TotalPaymentReceivedAmount += Math.Round(Convert.ToDecimal(busplit[1]), 2);
                        entityDB.Entry(prequalificationRecord).State = EntityState.Modified;
                        entityDB.SaveChanges();

                        helper.AccumulateFeeCap(PreQualificationId, entityDB, 1, Convert.ToDecimal(Convert.ToDecimal(busplit[1])));
                    }
                    else if (sections.Contains("annual"))
                    {
                        // Sumanth on 06/24/2015
                        var annualsplit = sections.Split(':');
                        var busplit = annualsplit[1].Split(';');
                        int preqstartmonth = 0;
                        int todaymonth = 0;

                        long preqStatusid = prequalificationRecord.PrequalificationStatusId;
                        if (preqStatusid != 3 && preqStatusid != 15)
                        {
                            preqstartmonth = prequalificationRecord.PrequalificationStart.Month;
                            todaymonth = int.Parse(DateTime.Today.Month.ToString());
                        }
                        // Ends<<<

                        // Kiran on 08/26/2015
                        if ((todaymonth < preqstartmonth) || (todaymonth == prequalificationRecord.PrequalificationFinish.Month && DateTime.Now.Year == prequalificationRecord.PrequalificationFinish.Year))
                        {
                            ViewBag.monthdiff = (prequalificationRecord.PrequalificationFinish.Month - todaymonth) + 1;
                        }
                        else
                        {
                            ViewBag.monthdiff = 12 - (todaymonth - preqstartmonth);
                        }
                        // Ends<<<
                        PrequalificationTrainingAnnualFees record = new PrequalificationTrainingAnnualFees();

                        foreach (var templateBuid in busplit[0].Split(','))
                        {
                            long localTemplateBuId = Convert.ToInt64(templateBuid);
                            var cliettemplateforBu = entityDB.ClientTemplatesForBU.FirstOrDefault(rec => rec.ClientTemplateForBUId == localTemplateBuId);
                            // Sumanth on 06/24/2015
                            var clientTemplateBUGroupPrice = entityDB.ClientTemplatesBUGroupPrice.Find(cliettemplateforBu.ClientTemplateBUGroupId).GroupPrice;
                            decimal preqannualfee = Math.Round(clientTemplateBUGroupPrice * ViewBag.monthdiff / 12, 2, MidpointRounding.AwayFromZero);
                            // Ends<<<
                            record = entityDB.PrequalificationTrainingAnnualFees.FirstOrDefault(rec => rec.ClientTemplateBUGroupId == cliettemplateforBu.ClientTemplateBUGroupId && rec.PrequalificationId == PreQualificationId && rec.ClientTemplateForBUId == localTemplateBuId);
                            var recExist = entityDB.PrequalificationTrainingAnnualFees.FirstOrDefault(rec => rec.ClientTemplateBUGroupId == cliettemplateforBu.ClientTemplateBUGroupId && rec.PrequalificationId == PreQualificationId);
                            if (record == null)
                            {
                                PrequalificationTrainingAnnualFees annualFee = new PrequalificationTrainingAnnualFees();
                                if (recExist == null)
                                {
                                    // Sumanth on 06/24/2015
                                    //annualFee.AnnualFeeAmount = Convert.ToDecimal(busplit[1]);
                                    //annualFee.AnnualFeeReceivedAmount = Convert.ToDecimal(busplit[1]);
                                    annualFee.AnnualFeeAmount = preqannualfee;
                                    annualFee.AnnualFeeReceivedAmount = preqannualfee;
                                    // Ends<<<
                                }
                                else
                                {
                                    annualFee.AnnualFeeAmount = 0;
                                    annualFee.AnnualFeeReceivedAmount = 0;
                                }
                                annualFee.PrequalificationId = PreQualificationId;
                                annualFee.ClientTemplateForBUId = cliettemplateforBu.ClientTemplateForBUId;
                                annualFee.ClientTemplateBUGroupId = (long)cliettemplateforBu.ClientTemplateBUGroupId;
                                annualFee.FeeReceivedDate = DateTime.Now;
                                //annualFee.FeeTransactionId = transactionId;
                                //annualFee.FeeTransactionMetaData = strRequest;
                                entityDB.PrequalificationTrainingAnnualFees.Add(annualFee);
                                entityDB.SaveChanges();
                            }
                        }
                        if (record == null)
                        {
                            //prequalificationRecord.TotalAnnualFeeAmount += Convert.ToDecimal(busplit[1]);
                            if (prequalificationRecord.TotalAnnualFeeReceived != null)
                                prequalificationRecord.TotalAnnualFeeReceived += Math.Round(Convert.ToDecimal(busplit[1]), 2);
                            else
                                prequalificationRecord.TotalAnnualFeeReceived = Math.Round(Convert.ToDecimal(busplit[1]), 2);
                            entityDB.Entry(prequalificationRecord).State = EntityState.Modified;
                            entityDB.SaveChanges();
                        }
                    }
                }

                //prequalificationRecord.PaymentReceivedDate = DateTime.Now;
                // Ends<<<
                entityDB.Entry(prequalificationRecord).State = EntityState.Modified;
                entityDB.SaveChanges();

            }
            redirectionURL = "TemplateSectionsList?templateId=" + templateId + "&TemplateSectionID=" + TemplateSectionID + "&PreQualificationId=" + PreQualificationId + "&mode=" + mode;
            return Redirect(redirectionURL);
        }
        // Ends<<<



        //Rajesh on 12/30/2013
        public string getVendorDetails(Guid userId)
        {
            var user = entityDB.SystemUsers.Find(userId);
            var phoneNumber = "";
            try
            {
                phoneNumber = user.Contacts.FirstOrDefault().PhoneNumber;
            }
            catch { }
            var cTitle = "";
            try
            {
                cTitle = user.Contacts.FirstOrDefault().ContactTitle;
            }
            catch { }
            var userDetails = phoneNumber + "|" + user.Email + "|" + cTitle;
            return userDetails;
        }
        //Ends<<<
        //Rajesh on 2/6/2014
        public ActionResult GeneratePDF(long TemplateSectionID, long PreQualificationId)
        {
            var htmlText = @"<html><body style='font-family:arial,sans-serief;font-size:14px;'>";
            var currentPrequalification = entityDB.Prequalification.Find(PreQualificationId);

            var currentTSection = entityDB.TemplateSections.Find(TemplateSectionID);
            var clientName = currentPrequalification.Client.Name;
            var vendorName = currentPrequalification.Vendor.Name;
            var submittedDate = DateTime.Now;
            if (currentTSection != null)
            {
                var prequalificationCompletedSections = currentTSection.PrequalificationCompletedSections.FirstOrDefault();
                if (prequalificationCompletedSections != null)
                    if (prequalificationCompletedSections.SubmittedDate != null)
                        submittedDate = (DateTime)prequalificationCompletedSections.SubmittedDate;
            }
            var registeredName = "";
            var mainUser =
           currentPrequalification.Vendor.SystemUsersOrganizations.FirstOrDefault(r => r.PrimaryUser == true);
            if (mainUser != null)
            {
                var contact = mainUser.SystemUsers.Contacts;
                if (contact != null && contact.Any())
                    registeredName = contact.FirstOrDefault().FirstName + " " +
                                             contact.FirstOrDefault().LastName;
            }
            foreach (var Sec in currentTSection.TemplateSubSections.Where(rec => rec.SubSectionTypeCondition == "MPATitleHeader").ToList())
            {
                htmlText += "<br/>";
                htmlText += "<h2 style='float:left;font-size: 16px;font-weight: 700;margin: -0.2em 0px 0px;width:100%;'><u>" + Sec.SubSectionName + "</u></h2>";




                htmlText += "<p>" + Sec.SectionNotes.Replace("[_ClientName]", "“" + clientName + "”")
                    .Replace("[_VendorName]", "“" + vendorName + "”")
                    .Replace("[_SecSubmittedDate]", "" + submittedDate.ToString("MM/dd/yyyy") + "")
                    .Replace("[_RegisteredName]", "" + registeredName + "")
                    .Replace("[_VendorAddress1]", "" + currentPrequalification.Vendor.Address1 + "")
                    .Replace("[_VendorAddress2]", "" + currentPrequalification.Vendor.Address2 + "")
                    .Replace("[_VendorCity]", "" + currentPrequalification.Vendor.City + "")
                    .Replace("[_VendorState]", "" + currentPrequalification.Vendor.State + "")
                    .Replace("[_VendorZip]", "" + currentPrequalification.Vendor.Zip + "")

                    + "</p>";
            }

            foreach (var Header in currentTSection.TemplateSectionsHeaderFooter.Where(rec => rec.PositionType == 0).ToList())
            {
                htmlText += "<br/>";
                htmlText += "<h2 style='float:left;font-size: 16px;font-weight: 700;margin: -0.2em 0px 0px;width:100%;'><u>" + Header.PositionTitle + "</u></h2>";

                htmlText += "<p>" + Header.PostionContent.Replace("[_ClientName]", "“" + clientName + "”").Replace("[_VendorName]", "“" + vendorName + "”")
                    .Replace("[_VendorName]", "“" + vendorName + "”")
                    .Replace("[_SecSubmittedDate]", "" + submittedDate.ToString("MM/dd/yyyy") + "")
                    .Replace("[_RegisteredName]", "" + registeredName + "")
                    .Replace("[_VendorAddress1]", "" + currentPrequalification.Vendor.Address1 + "")
                    .Replace("[_VendorAddress2]", "" + currentPrequalification.Vendor.Address2 + "")
                    .Replace("[_VendorCity]", "" + currentPrequalification.Vendor.City + "")
                    .Replace("[_VendorState]", "" + currentPrequalification.Vendor.State + "")
                    .Replace("[_VendorZip]", "" + currentPrequalification.Vendor.Zip + "") + "</p>";
            }

            foreach (var Sec in currentTSection.TemplateSubSections.Where(rec => rec.SubSectionTypeCondition != "MPATitleHeader" && rec.SubSectionTypeCondition != "AgreementQueries").ToList())
            {
                if (Sec.SubSectionTypeCondition == "AgreementAddendum")
                {
                    var addendums = entityDB.PrequalificationSubsection.Where(
                        r => r.PQId == PreQualificationId && r.SubSectionId == Sec.SubSectionID);
                    if (SSession.Role == OrganizationType.Vendor)
                        addendums = addendums.Where(r => r.VisibleToVendor);
                    foreach (
                        var PQSubSec in
                        addendums.OrderBy(r => r.GroupNumber).ThenBy(r => r.Order).ToList())
                    {
                        htmlText += "<br/>";
                        htmlText +=
                            "<h2 style='float:left;font-size: 16px;font-weight: 700;margin: -0.2em 0px 0px;width:100%;'><u>" +
                            PQSubSec.SubSectionName + "</u></h2>";

                        htmlText += "<p>" + PQSubSec.SubSectionHeader.Replace("[SecSubmittedDate]", submittedDate.ToString("MM/dd/yyyy")) + "</p>";
                        htmlText += "<br/><table style='width:50%;float:left;'>";

                        foreach (var question in PQSubSec.PrequalificationQuestion.ToList())
                        {
                            htmlText += "<tr><td><b>" + question.QuestionText + "</b></td>";
                            htmlText += "<td>";
                            try
                            {
                                htmlText += question.QuestionInputValue;
                            }
                            catch
                            {
                            }
                            ;
                            htmlText += "</td>";
                            htmlText += "</tr>";
                        }
                        htmlText += "</table>";
                    }
                }
                else
                {
                    htmlText += "<br/>";
                    htmlText +=
                        "<h2 style='float:left;font-size: 16px;font-weight: 700;margin: -0.2em 0px 0px;width:100%;'><u>" +
                        Sec.SubSectionName + "</u></h2>";

                    htmlText += "<p>" + Sec.SectionNotes + "</p>";
                    htmlText += "<br/><table style='width:50%;float:left;'>";

                    foreach (var question in Sec.Questions.ToList())
                    {
                        htmlText += "<tr><td><b>" + question.QuestionText + "</b></td>";
                        htmlText += "<td>";
                        try
                        {
                            htmlText +=
                                question.QuestionColumnDetails.FirstOrDefault()
                                    .PrequalificationUserInputs.FirstOrDefault(
                                        rec => rec.PreQualificationId == PreQualificationId)
                                    .UserInput;
                        }
                        catch
                        {
                        }
                        ;
                        htmlText += "</td>";
                        htmlText += "</tr>";
                    }
                    htmlText += "</table>";
                }
            }



            //foreach (var Footer in currentTSection.TemplateSectionsHeaderFooter.Where(rec => rec.PositionType == 1).ToList())
            //{
            //    htmlText += "<br/>";
            //    htmlText += "<h2 style='float:left;font-size: 16px;font-weight: 700;margin: -0.2em 0px 0px;width:100%;'>" + Footer.PositionTitle + "</h2>";

            //    htmlText += "<p>" + Footer.PostionContent + "</p>";
            //}

            htmlText += "</body></html>";
            iTextSharp.text.Document document = new iTextSharp.text.Document(iTextSharp.text.PageSize.A4);

            //Rajesh on 2/13/2014
            Directory.CreateDirectory(Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["MPADrafts"])));
            var path = Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["MPADrafts"]), "MPADraft" + PreQualificationId + ".pdf");
            //Ends<<<
            Stream f = new FileStream(path, FileMode.Create);
            PdfWriter.GetInstance(document, f);
            document.Open();

            //iTextSharp.text.Font font = new iTextSharp.text.Font(iTextSharp.text.Font.HELVETICA, 9, iTextSharp.text.Font.NORMAL);
            //iTextSharp.text.Chunk chunk = new iTextSharp.text.Chunk("RajeIsh", font);
            //document.Add(chunk);

            iTextSharp.text.html.simpleparser.HTMLWorker hw =
                         new iTextSharp.text.html.simpleparser.HTMLWorker(document);
            hw.Parse(new StringReader(htmlText));
            document.Close();

            var fs = System.IO.File.OpenRead(path);
            //return File(fs, "file", userEnteredFilename);
            Response.AppendHeader("Content-Disposition", "inline; filename=Sample.pdf");
            return File(fs, "application/pdf");
        }
        //Ends<<<

        // Kiran on 3/12/2014 for prequalification site deletion
        public string deletePrequalSite(long prequalSiteId, long prequalId)
        {
            var prequalSiteRecord = entityDB.PrequalificationSites.FirstOrDefault(rec => rec.PrequalificationSitesId == prequalSiteId && rec.PrequalificationId == prequalId);
            var annaulFeeRec = entityDB.PrequalificationTrainingAnnualFees.FirstOrDefault(rec => rec.ClientTemplatesForBU.ClientBusinessUnitId == prequalSiteRecord.ClientBusinessUnitId && rec.PrequalificationId == prequalId);//Mani on 8/5/2015
            //// Kiran on 07/22/2015
            //if (prequalSiteRecord.PrequalificationPaymentsId == null && annaulFeeRec == null)
            //{
            //    entityDB.PrequalificationSites.Remove(prequalSiteRecord);
            //    entityDB.SaveChanges();
            //    return "SiteDeleteSuccess";
            //}
            //else
            //    return "CantDeletePaymentDone";
            //entityDB.PrequalificationSites.Remove(prequalSiteRecord);
            //entityDB.SaveChanges();

            // sumanth on 10/19/2015 for FVOS-92 to delete the sites in prequalification starts
            if (annaulFeeRec == null)
            {
                PrequalificationPayments paymentTransaction = null;
                if (prequalSiteRecord.PrequalificationPaymentsId != null)
                {
                    paymentTransaction = entityDB.PrequalificationPayments.FirstOrDefault(rec => rec.PrequalificationPaymentsId == prequalSiteRecord.PrequalificationPaymentsId);
                    if (paymentTransaction.PaymentCategory == 0)
                    {
                        prequalSiteRecord.PrequalificationPaymentsId = null;
                        entityDB.Entry(prequalSiteRecord).State = EntityState.Modified;
                    }
                }
                if (prequalSiteRecord.PrequalificationPaymentsId == null)
                    entityDB.PrequalificationSites.Remove(prequalSiteRecord);
                else if (prequalSiteRecord.PrequalificationPaymentsId != null && paymentTransaction.PaymentCategory == 1)
                    return "AdditionalPaymentDone";
            }
            else
            {
                return "TrainingPaymentDone";
            }
            // Sumanth ends on 10/19/2015 Ends<<<
            var locationsSec = entityDB.PrequalificationCompletedSections.FirstOrDefault(rec => rec.PrequalificationId == prequalId && rec.TemplateSections.TemplateSectionType == 7);
            if (locationsSec != null)
                entityDB.PrequalificationCompletedSections.Remove(locationsSec);


            entityDB.SaveChanges();
            return "SiteDeleteSuccess";


        }

        // Ends<<

        public JsonResult getPQStatuses()
        {
            var positive = new List<long>() { 9 };
            var nagative = new List<long>() { 10, 14, 8, 4, 23 };
            return Json(_prequalification.GetPQStatuses().Select(rec => new PQStatuses()
            {
                Key = rec.Key,
                Value = rec.Value,
                SType = positive.Contains(rec.Key) ? true : nagative.Contains(rec.Key) ? false : (bool?)null
            }));
        }


        public string SubSectionInterimsave(List<PQUserInput> userInputs, long prequalId, long subSectionId)
        {
            string result = _prequalification.SubSectionInterimSave(userInputs, prequalId);
            if (result == "Saved")
            {
                SendSubSectionNotificationForEverySubmission(subSectionId, prequalId);
            }
            return "Ok";
        }

        private void SendSubSectionNotificationForEverySubmission(long subSecId, long PreQualificationId)
        {
            var flag_SendNotification = true;
            TemplateSubSections subSec = entityDB.TemplateSubSections.Find(subSecId);

            if (Session["RoleName"].ToString().Equals("Client"))
            {
                flag_SendNotification = false;
                if (subSec.TemplateSections.TemplateSectionsPermission.FirstOrDefault().VisibleTo == 2)
                //If it is client data entry//Rajesh on 3/21/2014
                {
                    flag_SendNotification = true;
                }
                else
                {
                    try
                    {
                        if (
                            subSec.TemplateSubSectionsPermissions.FirstOrDefault(rec => rec.PermissionFor == "Client")
                                .PermissionType == 1)
                        {
                            var loginUserId = new Guid(Session["UserId"].ToString());
                            if (
                                subSec.TemplateSubSectionsPermissions.FirstOrDefault(
                                        rec => rec.PermissionFor == "Client")
                                    .TemplateSubSectionsUserPermissions.FirstOrDefault(rec => rec.UserId == loginUserId)
                                    .EditAccess)
                            {
                                flag_SendNotification = true;
                            }
                        }
                    }
                    catch
                    {
                    }
                }
            }

            if (flag_SendNotification)
            {
                Prequalification currentPreQualification = entityDB.Prequalification.Find(PreQualificationId);

                if (subSec.NotificationExist == true)
                {


                    var notification = subSec.TemplateSubSectionsNotifications.FirstOrDefault();

                    if (notification != null)
                    {
                        var template = entityDB.EmailTemplates.Find(notification.EmaiTemplateId);

                        var subject = template.EmailSubject;
                        var body = template.EmailBody;
                        var Comment = template.CommentText;
                        var clientEmail = "";
                        var strMessage = "";
                        var emailsToSent = "";
                        var clientSysUserOrgs = currentPreQualification.Client.SystemUsersOrganizations;
                        foreach (var clientUserOrg in clientSysUserOrgs)
                        {
                            var clientOrgRoles = clientUserOrg.SystemUsersOrganizationsRoles;
                            foreach (var clientOrgRole in clientOrgRoles)
                            {
                                if (clientOrgRole.SystemRoles.RoleName.Equals("Super User"))
                                {
                                    clientEmail = clientUserOrg.SystemUsers.Email;
                                }
                            }
                        }
                        subject = subject.Replace("[Date]", DateTime.Now + "");
                        subject = subject.Replace("[VendorName]", currentPreQualification.Vendor.Name);
                        subject = subject.Replace("[ClientName]", currentPreQualification.Client.Name);
                        subject = subject.Replace("[CustomerServicePhone]", currentPreQualification.Client.PhoneNumber);
                        subject = subject.Replace("[CustomerServiceEmail]", clientEmail);

                        body = body.Replace("[Date]", DateTime.Now + "");
                        body = body.Replace("[VendorName]", currentPreQualification.Vendor.Name);
                        body = body.Replace("[ClientName]", currentPreQualification.Client.Name);
                        body = body.Replace("[CustomerServicePhone]", currentPreQualification.Client.PhoneNumber);
                        body = body.Replace("[CustomerServiceEmail]", clientEmail);
                        body = body.Replace("[ReviewApprovalLink]", ConfigurationManager.AppSettings["CancelURL"].ToString() + QueryStringModule.Encrypt("templateid=" + subSec.TemplateSections.TemplateID + "&TemplateSectionID=" + subSec.TemplateSectionID + "&PreQualificationId=" + PreQualificationId));

                        var subSectionNotificationsToSent =
                            subSec.TemplateSubSectionsNotifications.Where(rec => rec.NotifyUserDisabled != true)
                                .ToList();
                        if (subSectionNotificationsToSent != null)
                        {

                            foreach (var notificationToSent in subSectionNotificationsToSent)
                            {
                                emailsToSent = emailsToSent + notificationToSent.AdminOrClientUser.Email + ",";
                            }
                            emailsToSent = emailsToSent.Substring(0, emailsToSent.Length - 1);
                        }
                        strMessage = SendMail.sendMail(emailsToSent, body, subject);
                        if (strMessage.ToString() == "Success")
                        {
                            if (!String.IsNullOrEmpty(Comment))
                            {
                                OrganizationsNotificationsSent NotificationSent =
                                    entityDB.OrganizationsNotificationsSent.FirstOrDefault(
                                        rec => rec.ClientId == currentPreQualification.ClientId && rec.SetupType == 2);
                                if (NotificationSent == null)
                                {
                                    NotificationSent = new OrganizationsNotificationsSent();
                                    NotificationSent.ClientId = currentPreQualification.ClientId;
                                    NotificationSent.SetupType = 2;
                                    entityDB.OrganizationsNotificationsSent.Add(NotificationSent);
                                }
                                var logs = new OrganizationsNotificationsEmailLogs();
                                logs.Comments = Comment;
                                logs.EmailSentDateTime = DateTime.Now;
                                logs.MailSentBy = new Guid(Session["UserId"].ToString());
                                logs.NotificationId = NotificationSent.NotificationId;
                                logs.NotificationNo = 1;
                                logs.NotificationStatus = 0;
                                logs.NotificationType = 0;
                                logs.PrequalificationId = currentPreQualification.PrequalificationId;
                                logs.VendorId = currentPreQualification.VendorId;
                                entityDB.OrganizationsNotificationsEmailLogs.Add(logs);
                                entityDB.SaveChanges();
                            }
                            //Ends<<<
                            foreach (var notificationSent in subSectionNotificationsToSent)
                            {
                                PrequalificationNotifications PreNotifications = new PrequalificationNotifications();
                                PreNotifications.PrequalificationId = PreQualificationId;
                                PreNotifications.TemplateSubSectionNotificationId =
                                    notificationSent.TemplateSubSectionNotificationId;
                                PreNotifications.NotificationDateTime = DateTime.Now;
                                entityDB.PrequalificationNotifications.Add(PreNotifications);
                                entityDB.SaveChanges();
                            }
                        }
                    }
                }
            }
        }

        //Ends<<<
        public ActionResult FinancialMonth()
        {

            return View();
        }
        [SessionExpireForView]
        public ActionResult FinancialDataEntryPopUpView(long PrequalificationId)
        {
            ViewBag.PrequalificationId = PrequalificationId;
            var preQualificationDetails = entityDB.LatestPrequalification.Find(PrequalificationId);
            var vendorId = preQualificationDetails.VendorId;
            ViewBag.Organizationid = vendorId;

            var finance = entityDB.Organizations.Find(vendorId);

            if (finance.FinancialMonth == null && finance.FinancialDate == null)
            {

                return View("FinancialMonth");
            }

            var currentDate = DateTime.Now;
            var financial = new DateTime(currentDate.Year, finance.FinancialMonth ?? 1, finance.FinancialDate ?? 1).Date;
            if (financial.Date > currentDate.Date)
                financial = financial.AddYears(-1);
            //var currentYear=new DateTime()
            //if (financial > currentDate)
            //{
            //    var temp = financial.AddMonths(-6);
            //    if (currentDate < temp) financial = financial.AddYears(-1);
            //}
            //else
            //{
            //    var temp = financial.AddMonths(6);
            //    if (currentDate > temp) financial = financial.AddYears(1);
            //}

            //var clientId = preQualificationDetails.ClientId;
            var vendorFinancialData = new List<FinancialAnalyticsModel>();
            var acceptedList = new List<long>() { 3, 15, 7 };
            //if (acceptedList.Contains(preQualificationDetails.PrequalificationStatusId))
            //{
            for (int i = 0; i < 4; i++)
            {
                var financialDate = financial.AddYears(-i);
                var finDataByDate = entityDB.FinancialAnalyticsModel
                    .FirstOrDefault(x => x.VendorId == vendorId && x.ClientId == preQualificationDetails.ClientId && x.DateOfFinancialStatements.HasValue
                    && x.DateOfFinancialStatements.Value == financialDate) ??

                    (SSession.Role.Equals(OrganizationType.Vendor) ?
                        entityDB.FinancialAnalyticsModel
                            .FirstOrDefault(x => x.VendorId == vendorId && x.DateOfFinancialStatements.HasValue
                            && x.DateOfFinancialStatements.Value == financialDate) :
                    new FinancialAnalyticsModel() { VendorId = vendorId, ClientId = preQualificationDetails.ClientId, DateOfFinancialStatements = financialDate }) ??
                new FinancialAnalyticsModel() { VendorId = vendorId, ClientId = preQualificationDetails.ClientId, DateOfFinancialStatements = financialDate };
                vendorFinancialData.Add(finDataByDate);

            }
            //vendorFinancialData = entityDB.FinancialAnalyticsModel.Where(x => x.VendorId == vendorId && x.ClientId == preQualificationDetails.ClientId).OrderByDescending(y => y.DateOfFinancialStatements).ToList();
            //var years = vendorFinancialData.Select(r => r.DateOfFinancialStatements);
            //vendorFinancialData.AddRange(entityDB.FinancialAnalyticsModel.Where(r => r.VendorId == vendorId && r.ClientId == null && !years.Contains(r.DateOfFinancialStatements)));
            //}
            //else
            //{
            //    vendorFinancialData = _prequalification.GetAnalytics(PrequalificationId);
            //    //entityDB.FinancialAnalyticsModel.Where(x => x.ClientId == preQualificationDetails.ClientId && x.VendorId == vendorId).OrderByDescending(y => y.DateOfFinancialStatements).Take(4).ToList();
            //}

            //for (int i = vendorFinancialData.Count; i < 4; i++)
            //{
            //    var financialDate = financial.AddYears(-i - 1);
            //    vendorFinancialData.Add(new FinancialAnalyticsModel() { VendorId = vendorId, ClientId = preQualificationDetails.ClientId, DateOfFinancialStatements = financialDate });

            //}
            ViewBag.disableOldDataEntry = SSession.Role != OrganizationType.Vendor || entityDB.FinancialAnalyticsModel.Any(r => r.VendorId == vendorId && r.ClientId != null);
            return View(vendorFinancialData);

        }
        public ActionResult LoadGraph(long PQId, string field)
        {
            var data = _prequalification.GetGraphData(PQId, field);
            ViewBag.field = ProcessFieldNamesForGraph(field);
            ViewBag.series = data.Series;
            ViewBag.CategoryAxis = data.CategoryAxis;
            return View();
        }
        private string ProcessFieldNamesForGraph(string fieldName)
        {
            switch (fieldName)
            {
                case "CurrentRatio": return "Current Ratio";
                case "QuickRatio": return "Quick Ratio";
                case "WorkingCapitalPercentageRevenue": return "Working Capital to Revenue";
                case "DaysCash": return "Days Cash";
                case "AfterTaxReturnCurrentEquity": return "Return on Equity (ROE)";
                case "NetProfitMargin": return "Net Profit Margin";
                default: return "";
            }
        }
        [SessionExpireForView]
        [HttpPost]
        public ActionResult FinancialDataEntryPopUpView(List<FinancialAnalyticsModel> model, long PrequalificationId, bool IsClose)
        {
            var pq = entityDB.Prequalification.Find(PrequalificationId);
            var clientId = pq.ClientId;
            var vendorId = pq.VendorId;
            var preQualificationDetails = entityDB.LatestPrequalification.FirstOrDefault(r => r.ClientId == clientId && r.VendorId == vendorId);

            var financialData = new List<FinancialAnalyticsModel>();
            using (var entities = new EFDbContext())
            {

                foreach (var item in model)
                {
                    if (string.IsNullOrEmpty(item.SourceOfFinancialStatements) || item.IsDisabled)
                        continue;
                    //ViewBag.pq = preQualificationDetails;
                    if (item.Id != 0)
                    {

                        var dbItem = entities.FinancialAnalyticsModel.FirstOrDefault(x => x.Id == item.Id);
                        var changes = item.GetChanges(dbItem);
                        if (changes.Count == 0)
                            continue;
                        dbItem.Id = item.Id;
                        dbItem.VendorId = item.VendorId;
                        dbItem.DateOfFinancialStatements = item.DateOfFinancialStatements;
                        dbItem.Dateofinterimfinancialstatement = item.Dateofinterimfinancialstatement;
                        dbItem.SourceOfFinancialStatements = item.SourceOfFinancialStatements;
                        dbItem.CashAndCashEquivalents = item.CashAndCashEquivalents;
                        dbItem.Investments = item.Investments;
                        dbItem.ReceivablesCurrent = item.ReceivablesCurrent;
                        dbItem.ReceivableRetainage = item.ReceivableRetainage;
                        dbItem.CostsAndEstimatedEarningsInExcessOfBillingsOnUncompletedContracts = item.CostsAndEstimatedEarningsInExcessOfBillingsOnUncompletedContracts;
                        dbItem.RefundableIncomeTaxes = item.RefundableIncomeTaxes;
                        dbItem.NoteReceivable = item.NoteReceivable;
                        dbItem.OtherCurrentAssets = item.OtherCurrentAssets;
                        dbItem.NetPropertyAndEquipment = item.NetPropertyAndEquipment;
                        dbItem.OtherAssets = item.OtherAssets;
                        dbItem.LinesOfCreditShortTermBorrowings = item.LinesOfCreditShortTermBorrowings;
                        dbItem.InstallmentNotesPayableDueWithinOneYear = item.InstallmentNotesPayableDueWithinOneYear;
                        dbItem.AccountsPayable = item.AccountsPayable;
                        dbItem.RetainagePayable = item.RetainagePayable;
                        dbItem.AccruedExpenses = item.AccruedExpenses;
                        dbItem.BillingsInExcessOfCostsAndEstimatedEarningsOnUncompletedContracts = item.BillingsInExcessOfCostsAndEstimatedEarningsOnUncompletedContracts;
                        dbItem.CurrentIncomeTaxesPayable = item.CurrentIncomeTaxesPayable;
                        dbItem.DeferredIncomeTaxesPayableCurrent = item.DeferredIncomeTaxesPayableCurrent;
                        dbItem.NotesPayableDueAfterOneYear = item.NotesPayableDueAfterOneYear;
                        dbItem.EquipmentLinesOfCredit = item.EquipmentLinesOfCredit;
                        dbItem.OtherLongTermLiabilities = item.OtherLongTermLiabilities;
                        dbItem.DeferredIncomeTaxesPayableLongTerm = item.DeferredIncomeTaxesPayableLongTerm;
                        dbItem.TotalStockholdersEquity = item.TotalStockholdersEquity;
                        dbItem.ConstructionRevenues = item.ConstructionRevenues;
                        dbItem.ConstructionDirectCosts = item.ConstructionDirectCosts;
                        dbItem.NonConstructionGrossProfitLoss = item.NonConstructionGrossProfitLoss;
                        dbItem.GeneralAndAdministrativeExpenses = item.GeneralAndAdministrativeExpenses;
                        dbItem.OtherIncomeDeductions = item.OtherIncomeDeductions;
                        dbItem.IncomeTaxExpenseBenifit = item.IncomeTaxExpenseBenifit;
                        dbItem.DepreciationAndAmortization = item.DepreciationAndAmortization;
                        dbItem.TotalBacklog = item.TotalBacklog;
                        dbItem.TotalLineOfCredit = item.TotalLineOfCredit;
                        dbItem.LineOfCreditAvailability = item.LineOfCreditAvailability;
                        dbItem.InterestExpense = item.InterestExpense;
                        if (SSession.Role != OrganizationType.Vendor) { dbItem.LastUpdatedOn = DateTime.Now; dbItem.LastUpdatedBy = SSession.UserId; }
                        //dbItem.ClientId = item.ClientId;
                        dbItem.TotalThreeLargestCompletedProjects = item.TotalThreeLargestCompletedProjects;
                        //entities.FinancialAnalyticsModel.Remove((FinancialAnalyticsModel)dbItem);
                        //entities.FinancialAnalyticsModel.Add(model.FirstOrDefault(x => x.Id == item.Id));
                        entityDB.Entry(dbItem).State = EntityState.Modified;
                        //entities.FinancialAnalyticsModel.Remove(dbItem);

                        //if (preQualificationDetails != null && (preQualificationDetails.PrequalificationStatusId == 7 || preQualificationDetails.PrequalificationStatusId == 3 || preQualificationDetails.PrequalificationStatusId == 15))
                        //{
                        //    dbItem.ClientId = null;
                        //}
                        //entities.FinancialAnalyticsModel.Add(locData);

                    }
                    else
                    {

                        if (SSession.Role.Equals(OrganizationType.Vendor) && preQualificationDetails != null && (preQualificationDetails.PrequalificationStatusId == 3 || preQualificationDetails.PrequalificationStatusId == 7 || preQualificationDetails.PrequalificationStatusId == 15))
                        {
                            item.ClientId = null;
                        }
                        else
                        {
                            item.ClientId = preQualificationDetails.ClientId;
                            item.LastUpdatedOn = DateTime.Now;
                            item.LastUpdatedBy = SSession.UserId;
                        }

                        entities.FinancialAnalyticsModel.Add(item);
                        var param = new JavaScriptSerializer().Serialize(model);
                        logs.Info("FinancialAnalytics Added Successfully " + item.Id, param + "&PreQualificationId=" + PrequalificationId + "&isclose=" + IsClose, "PrequalificationController/FinancialDataEntrypopUpView");


                    }



                    entities.SaveChanges();
                }
                ViewBag.isFormSubmitted = "true";
                if (IsClose)
                    return Redirect("~/QuizTemplateBuilder/PopUpCloseView");
                return Redirect("FinancialDataEntryPopUpView?PrequalificationId=" + PrequalificationId);
            }
        }
        public ActionResult FinancialAnalyticsWithPQ(long PQId)
        {
            var Latestpq = entityDB.Prequalification.FirstOrDefault(r => r.PrequalificationId == PQId);
            return Redirect("ClientAndAdminViewFinancialAnalytics?ClientId=" + Latestpq.ClientId + "&VendorId=" + Latestpq.VendorId);
        }
        [SessionExpire]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "FinancialAnalytics", sideMenuName = "", ViewName = "ClientAndAdminViewFinancialAnalytics")]
        public ActionResult ClientAndAdminViewFinancialAnalytics(long ClientId = 0, long VendorId = 0, long PQId = 0)
        {
            if (SSession.Role.Equals(OrganizationType.Client) || SSession.Role.Equals(OrganizationType.SuperClient))
                ClientId = SSession.OrganizationId;
            ViewBag.clientId = ClientId;
            ViewBag.vendorId = VendorId;
            ViewBag.PQId = PQId;
            Guid userId = new Guid(Session["UserId"].ToString());
            ViewBag.RestrictedAccess =
                FVGen3.BusinessLogic.RolesHelper.IsUserInRole(userId, 1000, "read", ClientId,
                   VendorId) || Session["RoleName"].Equals("Super Admin");

            if (SSession.Role == OrganizationType.Admin)
            {
                var client = entityDB.Organizations.Where(r => r.OrganizationType == "Client" || r.OrganizationType == OrganizationType.SuperClient).OrderBy(r => r.Name).ToList().Select(a => new SelectListItem() { Text = a.Name, Value = a.OrganizationID.ToString() }).ToList();
                client.Insert(0, new SelectListItem() { Text = "Select", Value = "0" });
                ViewBag.org = client;
            }
            if (SSession.Role == OrganizationType.Client || SSession.Role == OrganizationType.SuperClient)
            {
                var client = entityDB.Organizations.Where(r => (r.OrganizationType == "Client" || r.OrganizationType == OrganizationType.SuperClient) && r.OrganizationID == SSession.OrganizationId).ToList().Select(a => new SelectListItem() { Text = a.Name, Value = a.OrganizationID.ToString() }).ToList();
                client.Insert(0, new SelectListItem() { Text = "Select", Value = "0" });
                ViewBag.org = client;
            }
            //if (SSession.Role == OrganizationType.SuperClient)
            //{
            //    var client = entityDB.Organizations.Where(r => r.OrganizationType == OrganizationType.SuperClient && SuperClientids.Contains(r.OrganizationID)).ToList().Select(a => new SelectListItem() { Text = a.Name, Value = a.OrganizationID.ToString() }).ToList();
            //    client.Insert(0, new SelectListItem() { Text = "Select", Value = "0" });
            //    ViewBag.org = client;
            //}
            if (SSession.Role == "Admin" || SSession.Role == "Client" || SSession.Role == OrganizationType.SuperClient)
            {
                return PartialView("_ClientAndAdminViewFinancialAnalytics");
            }
            if (PQId == 0 && VendorId != 0)
                ViewBag.PQId = _prequalification.GetLatestPQ(ClientId, VendorId).PrequalificationId;//Assign Latest PQid as PQID
            return null;
        }

        //To populate values in Financial analysis tab for client Visible for restricted client users
        //[OutputCache(Duration =0)]
        public PartialViewResult FinancialAnalyticsViewOnTab(long ClientId, long VendorId, long PQId = 0)
        {



            if (PQId == 0)
            {
                var pq = entityDB.LatestPrequalification.FirstOrDefault(r => r.VendorId == VendorId && r.ClientId == ClientId);
                if (pq != null)
                    PQId = pq.PrequalificationId;
            }
            ViewBag.latestPq = entityDB.LatestPrequalification.Any(rec => rec.PrequalificationId == PQId);
            //var latestPq = entityDB.LatestPrequalification.FirstOrDefault(r => r.VendorId == VendorId && r.ClientId == ClientId);
            Guid userId = new Guid(Session["UserId"].ToString());
            ViewBag.RestrictedAccess =
                FVGen3.BusinessLogic.RolesHelper.IsUserInRole(userId, 1000, "read", ClientId,
                   VendorId) || Session["RoleName"].Equals("Super Admin");

            List<FinancialClientViewModel> vendorFinancialData = new List<FinancialClientViewModel>();
            try
            {
                var acceptedList = new List<long>() { 3, 15, 7 };
                if (acceptedList.Contains(entityDB.Prequalification.Find(PQId).PrequalificationStatusId))
                {
                    return PartialView("_FinancialAnalyticsViewOnTab", vendorFinancialData);
                }
            }
            catch { }
            if (PQId != 0 && ViewBag.RestrictedAccess)
            {
                ViewBag.PQId = PQId;
                var vendorFinancialData1 = _prequalification.GetAnalytics(VendorId, ClientId, PQId);//entityDB.FinancialAnalyticsModel.Where(x => x.ClientId == latestPq.ClientId && x.VendorId == latestPq.VendorId).Take(4).OrderByDescending(y => y.DateOfFinancialStatements).ToList();
                if (vendorFinancialData1.Count != 0)
                {
                    foreach (var item in vendorFinancialData1)
                    {

                        FinancialClientViewModel obj = new FinancialClientViewModel();
                        obj.DateOfFinancialStatements = item.DateOfFinancialStatements;
                        obj.LastUpdatedOn = item.LastUpdatedOn;
                        obj.SourceOfFinancialStatements = item.SourceOfFinancialStatements;
                        obj.Dateofinterimfinancialstatement = item.Dateofinterimfinancialstatement;
                        obj.WorkingCapital = (item.WorkingCapital).ToString("#,##0");
                        obj.DaysOverheadCoveredinWorkingCapital = item.DaysOverheadCoveredWorkingCapitalLbl;
                        obj.WorkingCapitalPercenttoRevenue = item.WorkingCapitalPercentageRevenueLbl;
                        obj.WorkingCapitalTurnover = item.WorkingCapitalTurnoverLbl;
                        obj.CurrentRatio = item.CurrentRatioLbl;
                        obj.QuickRatio = item.QuickRatioLbl;
                        obj.DefensiveIntervalRatio = item.DefensiveIntervalRatioLbl;
                        obj.NetOverBillings = (item.NetOverBillings).ToString("#,##0");
                        obj.NetOverBillingPercentageRevenue = item.NetOverBillingsPercentRevenueLbl;
                        obj.DaysRevenueinReceivables = item.DaysRevenueReceivableLbl;
                        obj.DaysCash = item.DaysCashLbl;
                        obj.CashtoOverBillings = item.CashOverBillingsLbl;
                        obj.CashtoEquity = item.CashEquityLbl;
                        obj.CashtoRevenue = item.CashRevenueLbl;
                        obj.DebtCoverageRatio = item.DebtCoverageRatioLbl;
                        obj.BondingCapacity = (item.BondingCapacity).ToString("#,##0");
                        obj.BacklogPercentageRevenue = item.BacklogPercentageRevenueLbl;
                        obj.BacklogPercentageBondingCapacity = item.BacklogPercentageBondingCapacityLbl;
                        obj.BacklogGrossProfitGAExpense = item.BacklogGrossProfitGAExpenseLbl;
                        obj.MonthsRevenueBacklog = item.MonthsRevenueBacklogLbl;
                        obj.EquityRevenue = item.EquityRevenueLbl;
                        obj.InterestBearingDebtEquity = item.InterestBearingDebtEquityLbl;
                        obj.DebtWorkingCapital = item.DebtWorkingCapitalLbl;
                        obj.TotalLiabilitiesEquity = item.TotalLiabilitiesToEquityLbl;
                        obj.GrossProfitPercentage = item.GrossProfitPercentageLbl;
                        obj.GAPercentage = item.GAPercentageLbl;
                        obj.PretaxEarningsPercentage = item.PreTaxEarningsPercentageLbl;
                        obj.PretaxReturnCurrentYearEquity = item.PreTaxReturnCurrentEquityLbl;
                        obj.PretaxReturnCurrentYearAssets = item.PreTaxReturnCurrentAssetsLbl;
                        obj.AfterTaxReturnCurrentYearAssets = item.AfterTaxReturnCurrentAssetsLbl;
                        obj.AfterTaxReturnCurrentYearEquity = item.AfterTaxReturnCurrentEquityLbl;
                        obj.EBITDA = (item.CEBITDA).ToString("#,##0");
                        obj.AsPercentageRevenue = item.AsPercentageRevenueLbl;

                        vendorFinancialData.Add(obj);

                    }
                }  //return PartialView("_FinancialAnalyticsViewOnTab", vendorFinancialData);

            }
            //else
            return PartialView("_FinancialAnalyticsViewOnTab", vendorFinancialData);


        }

        // to display values in JobMetrics tab for the client Visible for restricted client users
        //[OutputCache(Duration = 0)]
        public PartialViewResult JobMetrics(long ClientId, long VendorId, long PQId)
        {
            Guid userId = new Guid(Session["UserId"].ToString());
            ViewBag.RestrictedAccess =
                FVGen3.BusinessLogic.RolesHelper.IsUserInRole(userId, 1000, "read", ClientId,
                   VendorId) || Session["RoleName"].Equals("Super Admin");
            var analytics = _prequalification.GetAnalytics(VendorId, ClientId, PQId);
            List<JobMetricModel> JobMetricsData = new List<JobMetricModel>();
            try
            {
                var acceptedList = new List<long>() { 3, 15, 7 };
                if (acceptedList.Contains(entityDB.Prequalification.Find(PQId).PrequalificationStatusId))
                {
                    return PartialView("_JobMetricsView");
                }
            }
            catch { }

            if (analytics.Count != 0 && ViewBag.RestrictedAccess)
            {
                //var array = new List<FinancialAnalyticsModel>();
                //FinancialAnalyticsModel cal = analytics.FirstOrDefault();



                var vendorOrg = entityDB.Organizations.Find(VendorId);
                if (vendorOrg.FinancialMonth == null || vendorOrg.FinancialMonth == null)
                {
                    return PartialView("_JobMetricsView", new List<JobMetricModel>());
                }
                ViewBag.VendorName = vendorOrg.Name;

                foreach (var cal in analytics)
                {
                    JobMetricModel obj = new JobMetricModel();

                    obj.DateOfFinancialStatement = cal.DateOfFinancialStatements;
                    obj.LastUpdatedOn = cal.LastUpdatedOn;
                    obj.networthLoc = (cal.NetWorthAvailableLoc).ToString("#,##0");
                    obj.FivetimesWorkingCapital = (cal.FiveTimesWorkingCapital).ToString("#,##0");
                    obj.TwntyFivePrcntPriorYearRevenue = (cal.TwentyFivePrcntPriorYrRevenue).ToString("#,##0");
                    obj.SeventyFivePercentLargestCompletedProject = (cal.SeventyPercentOfThreeLargestCompletedContracts).ToString("#,##0");
                    obj.AvrgMeasuresForSingleJob = (cal.AverageMeasuresForSingleJob).ToString("#,##0");
                    obj.NetWorthTotalLineCredit = (cal.NetWorthTotalLineCredit).ToString("#,##0");
                    obj.TenTimesWorkingCapital = (cal.TenTimesWorkingCapital).ToString("#,##0");
                    obj.FiftyPercentPriorYearRevenue = (cal.FiftyPrcntPriorYrRevenue).ToString("#,##0");
                    obj.TenTimesNetWorth = (cal.TenTimesNetWorth).ToString("#,##0");
                    obj.AvrgMeasuresForAggrigate = (cal.AverageMeasuresForAggrigate).ToString("#,##0");
                    obj.TotalLinesCredit = (cal.TotalLineOfCredit ?? 0).ToString("#,##0");
                    obj.AvailableLineCredit = (cal.LineOfCreditAvailability ?? 0).ToString("#,##0");
                    obj.TotalOfThreeLargestCompletedProjects = (cal.TotalThreeLargestCompletedProjects ?? 0).ToString("#,##0");
                    //return PartialView("_JobMetricsView", obj);
                    JobMetricsData.Add(obj);
                }
                //return PartialView("_JobMetricsView", JobMetricsData);
            }

            //else
            return PartialView("_JobMetricsView", JobMetricsData);


        }

        //This is to display values in NewZscore tab Visible for restricted client users
        //[OutputCache(Duration = 0)]
        public PartialViewResult NewZScore(long ClientId, long VendorId, long PQId)
        {
            Guid userId = new Guid(Session["UserId"].ToString());
            ViewBag.RestrictedAccess =
                FVGen3.BusinessLogic.RolesHelper.IsUserInRole(userId, 1000, "read", ClientId,
                   VendorId) || Session["RoleName"].Equals("Super Admin");
            //var latestPQ = entityDB.LatestPrequalification.FirstOrDefault(r => r.VendorId == VendorId && r.ClientId == ClientId);
            List<NewZScoreModel> NewZscoreData = new List<NewZScoreModel>();
            try
            {
                var acceptedList = new List<long>() { 3, 15, 7 };
                if (acceptedList.Contains(entityDB.Prequalification.Find(PQId).PrequalificationStatusId))
                {
                    return PartialView("_NewZScoreView", NewZscoreData);
                }
            }
            catch { }
            //if (latestPQ != null)
            {
                var vendorFinancialData = _prequalification.GetAnalytics(VendorId, ClientId, PQId);//entityDB.FinancialAnalyticsModel.Where(x => x.ClientId == latestPQ.ClientId && x.VendorId == latestPQ.VendorId).Take(4).OrderByDescending(y => y.DateOfFinancialStatements).ToList();
                if (vendorFinancialData.Count != 0 && ViewBag.RestrictedAccess)
                {
                    var vendor = _organization.GetOrganization(VendorId);
                    ViewBag.VendorName = vendor.Name;
                    foreach (var item in vendorFinancialData)
                    {
                        NewZScoreModel obj = new NewZScoreModel();
                        obj.DateOfFinancialStatement = item.DateOfFinancialStatements;
                        obj.LastUpdatedOn = item.LastUpdatedOn;
                        obj.ZScore = item.ZScore;
                        obj.EarningsBeforeIntAndTax = item.OperatingIncome;
                        obj.TotalAssets = item.TotalAssets;
                        obj.CalculationForReturnOnTotalAssets = item.CalculationForReturnonTotalAssets;
                        obj.WeightFactorForReturnOnTotalAssets = "3.300";
                        obj.ReturnOnTotalAssets = item.ReturnonTotalAssets;

                        obj.NetSales = item.ConstructionRevenues ?? 0;
                        obj.CalculationForSalesOfTotalAssets = item.CalculationForSalestoTotalAssets;
                        obj.WeightFactorForSalesOfTotalAssets = "0.9990";
                        obj.SalesOfTotalAssets = item.SalestoTotalAssets;

                        obj.Equity = item.TotalStockholdersEquity ?? 0;
                        obj.TotalLiabilities = item.TotalLiabilities;
                        obj.CalculationForEquityToDebt = item.CalculationForEquityToDebt;
                        obj.WeightFactorForEquityToDebt = "0.6000";
                        obj.EquityToDebt = item.EquityToDebt;

                        obj.workingCapital = item.WorkingCapital;
                        obj.WeightFactorForWorkingCapitalToTotalAssets = "1.2000";
                        obj.CalculationForWorkingCapitalToTotalAssets = item.CalculationForWorkingCapitalToTotalAssets;
                        obj.WorkingCapitalToTotalAssets = item.WorkingCapitalToTotalAssets;

                        obj.RetainedEarnings = item.TotalStockholdersEquity ?? 0;
                        obj.CalculationForRetainedEarningsToTotalAssets = item.CalculationForRetainedEarningstoTotalAssets;
                        obj.WeightFactorForRetainedEarningsToTotalAssets = "1.4000";
                        obj.RetainedEarningsToTotalAssets = item.RetainedEarningstoTotalAssets;


                        NewZscoreData.Add(obj);
                    }
                    //  return PartialView("_NewZScoreView", NewZscoreData);
                }
            }
            //else
            //{
            return PartialView("_NewZScoreView", NewZscoreData);
            //      }

        }

        //To display values in PM Dashboard to all client users no restrictions
        //[OutputCache(Duration = 0)]
        public PartialViewResult PMDashboard(long ClientId, long VendorId, long PQId)
        {
            var latestPq = entityDB.LatestPrequalification.FirstOrDefault(r => r.VendorId == VendorId && r.ClientId == ClientId); ;
            var latest = 0L;
            if (latestPq != null)
                latest = latestPq.PrequalificationId;
            ViewBag.PrequalificationId = latest;

            ViewBag.latestPq = entityDB.LatestPrequalification.Any(rec => PQId == 0 || rec.PrequalificationId == PQId);
            List<PMDashboardModel> PMDashboardData = new List<PMDashboardModel>();
            try
            {
                var acceptedList = new List<long>() { 3, 15, 7 };
                if (acceptedList.Contains(entityDB.Prequalification.Find(PQId).PrequalificationStatusId))
                {
                    return PartialView("_PMDashboardView", PMDashboardData);
                }
            }
            catch { }
            if (latest != 0)
            {
                var vendorFinancialData = _prequalification.GetAnalytics(VendorId, ClientId, PQId);//entityDB.FinancialAnalyticsModel.Where(x => x.ClientId == latestPq.ClientId && x.VendorId == latestPq.VendorId).Take(4).OrderByDescending(y => y.DateOfFinancialStatements).ToList();
                var data = _prequalification.GetStatistics(VendorId, ClientId);

                ViewBag.EMR = data.EMR.Split(',').ToList();
                ViewBag.RIR = data.RIR.Split(',').ToList();
                ViewBag.DART = data.DART.Split(',').ToList();
                if (vendorFinancialData.Count != 0)
                {

                    foreach (var item in vendorFinancialData)
                    {
                        PMDashboardModel obj = new PMDashboardModel();
                        obj.DateOfFinancialStatement = item.DateOfFinancialStatements;
                        obj.LastUpdatedOn = item.LastUpdatedOn;

                        obj.CurrentAssets = item.TotalCurrentAssets;
                        obj.CurrentLiabilities = item.TotalCurrentLiabilities;
                        obj.CurrentRatio = item.CurrentRatioLbl;

                        obj.CashReceivables = item.cashInvRec;
                        obj.QuickRatio = item.QuickRatioLbl;

                        obj.WorkingCapital = item.WorkingCapital;
                        obj.Revenue = item.ConstructionRevenues ?? 0;
                        obj.WorkingCapitalRevenue = item.WorkingCapitalPercentageRevenueLbl;

                        obj.CashX360 = (item.CashAndCashEquivalents ?? 0 + item.Investments ?? 0) * 360;
                        obj.DaysCash = item.DaysCashLbl;

                        obj.NetIncome = item.NetIncome;
                        obj.ShareholderEquity = item.TotalStockholdersEquity ?? 0;
                        obj.ReturnEquity = item.AfterTaxReturnCurrentEquityLbl;

                        obj.NetProfit = item.NetIncome;
                        obj.NetProfitMargin = item.NetProfitMarginLbl;


                        PMDashboardData.Add(obj);
                    }
                    //  return PartialView("_PMDashboardView", PMDashboardData);
                }
            }
            // else
            // {
            return PartialView("_PMDashboardView", PMDashboardData);
            // }

        }

        public PartialViewResult SupportingDocumentsForFA(long ClientId, long VendorId, long PQId = 0)
        {
            var currentPrequalification = entityDB.Prequalification.FirstOrDefault(r => r.VendorId == VendorId && r.ClientId == ClientId);
            var latest = 0L;
            ViewBag.currentPrequalification = currentPrequalification;
            if (currentPrequalification != null)
            {

                latest = currentPrequalification.PrequalificationId;

                Guid userId = new Guid(Session["UserId"].ToString());
                ViewBag.RestrictedAccess =
                    FVGen3.BusinessLogic.RolesHelper.IsUserInRole(userId, 1000, "read", currentPrequalification.ClientId,
                        currentPrequalification.VendorId) || Session["RoleName"].Equals("Super Admin");



                var documentTypeIds =
                    entityDB.ClientTemplateReportingData.Where(
                            rec =>
                                rec.ReportingType == 2 && rec.ClientTemplateId == currentPrequalification.ClientTemplateId)
                        .Select(rec => rec.ReportingTypeId)
                        .ToList();

                if (Session["RoleName"].Equals("Admin") || Session["RoleName"].Equals("Vendor"))
                {
                    ViewBag.Documents =
                        entityDB.Document.Where(
                                rec =>
                                    rec.ReferenceType == "PreQualification" && rec.ReferenceRecordID == latest &&
                                    (rec.DocumentStatus != "Deleted" || rec.DocumentStatus == null) &&
                                    documentTypeIds.Contains(rec.DocumentTypeId))
                            .GroupBy(rec => rec.DocumentTypeId)
                            .ToList();

                }
                else
                {
                    ViewBag.Documents =
                        entityDB.Document.Where(
                                rec =>
                                    rec.ReferenceType == "PreQualification" && rec.ReferenceRecordID == latest &&
                                    rec.DocumentStatus == "Approved" && documentTypeIds.Contains(rec.DocumentTypeId))
                            .GroupBy(rec => rec.DocumentTypeId)
                            .ToList();

                }

            }
            return PartialView("_SupportingDocumentsForFA");
        }
        public ActionResult Getprequalification(long clientId, long vendorId)
        {
            var pq = entityDB.Prequalification.Where(r => r.ClientId == clientId && r.VendorId == vendorId).OrderByDescending(r => r.PrequalificationCreate).ToList().Select(a => new KeyValuePair<long, string>
                  (
                  a.PrequalificationId,
                  (a.PrequalificationStart.ToString("MM/dd/yy") + "-" +
                                  a.PrequalificationFinish.ToString("MM/dd/yy"))
                  )).ToList();
            return Json(pq);

        }

        public string UpdateInvitationFromDetails(long pqId, string InvitationFrom)
        {
            string result = _prequalification.UpdateInvitationFromInfo(pqId, InvitationFrom);
            return result;
        }
    }
}

