﻿using System.Linq;
using System.Web.Mvc;
using FVGen3.Domain.Entities;
using FVGen3.Domain.Concrete;
//using Excel = Microsoft.Office.Interop.Excel;
using FVGen3.BusinessLogic.Interfaces;
using System.Data.Entity;
using System.Web.Routing;
using Resources;
using System;
using FVGen3.WebUI.Constants;

namespace FVGen3.WebUI.Controllers
{
    public class MigrationController : BaseController
    {
        //
        // GET: /Migration/
        EFDbContext entityDB;
        private IMigrationBusiness _Migration;
        private IPrequalificationBusiness _prequalificationBusiness;
        public MigrationController(IMigrationBusiness _Migration, IPrequalificationBusiness _prequalificationBusiness)
        {
            entityDB = new EFDbContext();
            this._Migration = _Migration;
            this._prequalificationBusiness = _prequalificationBusiness;
        }
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult MigrateTOERITemplate(Migration migrateData, bool isSuccess = false)
        {
            var clientOrgs = from org in entityDB.Organizations.Where(r => r.OrganizationType == "Client").OrderBy(rec => rec.Name).ToList()
                             select new SelectListItem
                             {
                                 Text = org.Name,
                                 Value = org.OrganizationID.ToString()
                             };

            SelectListItem defaultItem = new SelectListItem();
            defaultItem.Text = "";
            defaultItem.Value = "-1";
            var clients = clientOrgs.ToList();
            clients.Insert(0, defaultItem);

            ViewBag.clientOrgs = clients;
            bool sucess = isSuccess;
            ViewBag.isSuccess = isSuccess;
            migrateData = new Migration();

            var eriTemplates = from Template in entityDB.ClientTemplates.Where(rec => rec.Templates.DefaultTemplate == false && rec.Templates.TemplateType == 0 && rec.Templates.TemplateStatus == 1 && rec.Templates.IsERI == true && rec.ClientID == -1).ToList()
                               select new SelectListItem
                               {
                                   Text = Template.Templates.TemplateName,
                                   Value = Template.TemplateID + ""
                               };

            ViewBag.ERITemplates = eriTemplates.ToList();

            return View(migrateData);
        }

        public ActionResult getClientTemplates(long clientid)
        {
            var clientTemplates = from Template in entityDB.ClientTemplates.Where(rec => rec.ClientID == clientid && rec.Templates.DefaultTemplate == false && rec.Templates.TemplateType == 0 && rec.Templates.TemplateStatus == 1 && rec.Templates.IsERI == false).ToList()
                                  select new SelectListItem
                                  {
                                      Text = Template.Templates.TemplateName,
                                      Value = Template.TemplateID + ""
                                  };

            return Json(clientTemplates);
        }

        [HttpPost]
        public ActionResult MigrateTODB(Migration form)
        {
            var errors = "";
            var Data = entityDB.QuestionsForMigration.Where(r => r.SourceTemplateId == form.sourceTemplateId && r.DestinationTemplateId == form.destinationTemplateId).ToList();
            var ERITemplateId = entityDB.ClientTemplates.FirstOrDefault(r => r.Organizations.OrganizationType == OrganizationType.SuperClient && r.TemplateID == form.destinationTemplateId).ClientTemplateID;
            var pqs = entityDB.LatestPrequalification.Where(r => r.ClientId == form.ClientId && r.ClientTemplates.TemplateID == form.sourceTemplateId && r.CanMigrate != false).ToList();
            foreach (var sourcePQ in pqs)
            {
                var destPQ = entityDB.Prequalification.FirstOrDefault(r => r.ClientId == -1 && r.VendorId == sourcePQ.VendorId && r.ClientTemplates.TemplateID == form.destinationTemplateId);
                if (destPQ != null)
                {
                    _Migration.AddPQClient(destPQ.PrequalificationId, form.ClientId);
                    _Migration.CopyDocuments(sourcePQ.PrequalificationId, destPQ.PrequalificationId, destPQ.VendorId, Data);
                    _Migration.CopySites(sourcePQ.PrequalificationId, destPQ.PrequalificationId);
                    if (sourcePQ.PaymentReceived == true)//This has to be after sites copy.
                    {
                        _Migration.RecordPayment(sourcePQ.PrequalificationId, destPQ.PrequalificationId, Data);
                    }
                    _Migration.MoveComments(sourcePQ.PrequalificationId, destPQ.PrequalificationId, Data);
                    _Migration.NotificationEmailComments(sourcePQ.PrequalificationId, destPQ.PrequalificationId);
                }
                else
                {
                    var clienttemplates = entityDB.ClientTemplates.Any(r => r.ClientID == form.ClientId && r.TemplateID == form.destinationTemplateId);

                    if (clienttemplates == false)
                    {
                        ClientTemplates ct = new ClientTemplates();
                        ct.TemplateID = form.destinationTemplateId;
                        ct.ClientID = form.ClientId;
                        ct.Visible = true;
                        entityDB.ClientTemplates.Add(ct);
                        entityDB.SaveChanges();
                    }

                    Prequalification destinationPQ = new Prequalification();      //prequalification creation
                    destinationPQ.VendorId = sourcePQ.VendorId;
                    destinationPQ.ClientId = -1;

                    destinationPQ.ClientTemplateId = ERITemplateId;
                    destinationPQ.locked = false;
                    destinationPQ.PrequalificationCreate = sourcePQ.PrequalificationCreate;
                    destinationPQ.PrequalificationStart = sourcePQ.PrequalificationStart;
                    destinationPQ.PrequalificationFinish = sourcePQ.PrequalificationFinish;
                    destinationPQ.PrequalificationSubmit = sourcePQ.PrequalificationSubmit;
                    destinationPQ.UserIdAsCompleter = sourcePQ.UserIdAsCompleter;

                    destinationPQ.PrequalificationStatusId = sourcePQ.PrequalificationStatusId;//_prequalificationBusiness.Finalstatus().Contains(pq.PrequalificationStatusId) ? pq.PrequalificationStatusId : 3;

                    entityDB.Prequalification.Add(destinationPQ);
                    entityDB.SaveChanges();

                    _Migration.CopyAnswers(sourcePQ.PrequalificationId, destinationPQ.PrequalificationId, Data);
                    try
                    {
                        _Migration.CopyEmRValues(sourcePQ.PrequalificationId, destinationPQ.PrequalificationId, destinationPQ.VendorId, Data);
                    }
                    catch { }
                    _Migration.AddPQClient(destinationPQ.PrequalificationId, form.ClientId);
                    _Migration.CopyDocuments(sourcePQ.PrequalificationId, destinationPQ.PrequalificationId, destinationPQ.VendorId, Data);
                    _Migration.CopySites(sourcePQ.PrequalificationId, destinationPQ.PrequalificationId);
                    if (sourcePQ.PaymentReceived == true)//This has to be after sites copy.
                    {
                        _Migration.RecordPayment(sourcePQ.PrequalificationId, destinationPQ.PrequalificationId, Data);
                    }
                    _Migration.MoveComments(sourcePQ.PrequalificationId, destinationPQ.PrequalificationId, Data);
                    _Migration.MoveReportingData(sourcePQ.PrequalificationId, destinationPQ.PrequalificationId);
                    _Migration.PQCompletedSections(sourcePQ.PrequalificationId, destinationPQ.PrequalificationId, Data);
                    _Migration.NotificationEmailComments(sourcePQ.PrequalificationId, destinationPQ.PrequalificationId);
                    _Migration.CopyVendorDetails(sourcePQ.PrequalificationId, destinationPQ.PrequalificationId);
                    try
                    {
                        _Migration.SectionstoDisplay(sourcePQ.PrequalificationId, destinationPQ.PrequalificationId, Data);
                    }
                    catch { }
                }
                _Migration.deletePrequalification(sourcePQ.PrequalificationId);
                //try
                //{
                //    _Migration.deletePrequalification(sourcePQ.PrequalificationId);
                //}
                //catch (Exception ee)
                //{
                //    
                //    errors += sourcePQ.PrequalificationId + ": " + ee.Message + "\n";
                //}
            }
            if (!string.IsNullOrEmpty(errors))
            {
                return Json(errors);
            }
            //To stop migrating rest of PQ's in next time
            var restofPqs = entityDB.LatestPrequalification.Where(r => r.ClientId == form.ClientId && r.ClientTemplates.TemplateID == form.sourceTemplateId).ToList();
            foreach(var pq in restofPqs)
            {
                pq.CanMigrate = false;
                entityDB.Entry(pq).State = EntityState.Modified;
            }
            entityDB.SaveChanges();

            return RedirectToAction("MigrateTOERITemplate", new RouteValueDictionary(new { controller = "Migration", action = "MigrateTOERITemplate", isSuccess = true }));
        }
    }
}
