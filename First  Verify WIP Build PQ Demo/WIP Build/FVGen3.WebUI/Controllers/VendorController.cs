﻿/*
Page Created Date:  05/07/2013
Created By: Kiran Talluri
Purpose:
Version: 1.0
***************************************************************************
SUMA 6/17/2013 for Vendor Dashboard, Client Templates and Prequalifications
***************************************************************************
Kiran 7/5/2013 Added code to handle Vendor registration with invitation Id
               Added parameter in SendMail Class - Parameter Name 'InvitationId' 
***************************************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FVGen3.Domain.Abstract;
using FVGen3.Domain.Entities;
using FVGen3.Domain.Concrete;
using FVGen3.WebUI.Annotations;
using System.Text.RegularExpressions;
using System.Configuration;
using System.Net.Mail;
using System.Text;
using System.Security.Cryptography;
using System.Data.Entity;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using FVGen3.BusinessLogic.Interfaces;
using FVGen3.WebUI.Enums;
using FVGen3.WebUI.Utilities;
using FVGen3.WebUI.Constants;
using FVGen3.WebUI.Models;
using AhaApps.Libraries.Extensions;
using Resources;

namespace FVGen3.WebUI.Controllers
{
    public class VendorController : BaseController
    {
        //
        // GET: /Vendor/

        EFDbContext entityDB;
        Messages.Messages messages;
        private IVendorBusiness _vendorBusiness;
        private IClientBusiness _clientBusiness;
        private IOrganizationBusiness _orgBusiness;
        private IPrequalificationBusiness _prequalificationBusiness;
        public VendorController(ISystemUsersRepository repositorySystemUsers, IClientBusiness clientBusiness, IVendorBusiness vendorBusiness, IPrequalificationBusiness prequalificationBusiness, IOrganizationBusiness organizationsBusiness)
        {
            _vendorBusiness = vendorBusiness;
            _orgBusiness = organizationsBusiness;
            _prequalificationBusiness = prequalificationBusiness;
            _clientBusiness = clientBusiness;
            entityDB = new EFDbContext();
            messages = new Messages.Messages();
        }

        /*
            Created Date:  05/07/2013   Created By: Kiran Talluri
            Purpose: To display the registration form
        */
        public ActionResult GetOrganizations()
        {
            return Json(_clientBusiness.GetClients(), JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetVendorOrgs(string name)
        {
            return Json(_vendorBusiness.GetOrganizations(name,LocalConstants.Vendor), JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetOrgVendors(string name)
        {
            return Json(_vendorBusiness.GetVendors(name, LocalConstants.Clayco_ClientId), JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetAllVendors()
        {
            return Json(_vendorBusiness.GetAllVendors(), JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public ActionResult VendorRegistration(string InvitationId)
        {
            ViewBag.PageType = "Registration";
            ViewBag.isEmailExists = false;
            ViewBag.hasAppid = false;
            ViewBag.checkBoxStatus = true;
            ViewBag.VBMailSent = true; // Kiran on 4/12/2014
            ViewBag.isTaxIdExists = false; // Kiran on 8/5/2014
            ViewBag.taxIdCountError = false; // Kiran on 8/25/2014
            var countries= _orgBusiness.GetCountries().Select(r => new SelectListItem() { Text = r.Value, Value = r.Value }).ToList();
            ViewBag.Countries = countries;

            LocalVendorRegistrationModel locVenReg_form = new LocalVendorRegistrationModel();
            HttpCookie cookie = Request.Cookies["Vendor"];
            if (cookie != null)
            {
                locVenReg_form.Password = cookie.Values[0];
                locVenReg_form.ConfirmPassword = cookie.Values[0];
            }

            // Kiran 7/5/2013 
            // added code to handle vendor registration with invitation id >>>
            if (InvitationId != null)
            {
                locVenReg_form.InvitationId = InvitationId.Equals("") ? new Guid() : new Guid(InvitationId.ToString());
                //Guid currentInvitationId = new Guid(InvitationId.ToString());
                var currentInvitationIdRecord = entityDB.VendorInviteDetails.FirstOrDefault(record => record.InvitationId == locVenReg_form.InvitationId);
                locVenReg_form.Email = currentInvitationIdRecord.VendorEmail;
                locVenReg_form.FirstName = currentInvitationIdRecord.VendorFirstName;
                locVenReg_form.LastName = currentInvitationIdRecord.VendorLastName;
                locVenReg_form.City = currentInvitationIdRecord.City;
                locVenReg_form.Address1 = currentInvitationIdRecord.Address1;
                locVenReg_form.Address2 = currentInvitationIdRecord.Address2;
                locVenReg_form.Address3 = currentInvitationIdRecord.Address3; // Kiran on 9/19/2014
                locVenReg_form.State = currentInvitationIdRecord.State;
                locVenReg_form.Zip = currentInvitationIdRecord.Zip;
                locVenReg_form.Country = currentInvitationIdRecord.Country;
                locVenReg_form.Name = currentInvitationIdRecord.VendorCompanyName;
                locVenReg_form.ClientID = currentInvitationIdRecord.ClientId;
                //List<SelectListItem> items = new List<SelectListItem> { new SelectListItem { Text = locVenReg_form.Country, Value = locVenReg_form.Country } };
                if (locVenReg_form.Country != null)
                {
                    countries.FirstOrDefault(r => r.Value == locVenReg_form.Country).Selected = true;
                }
                ViewBag.Countries = countries;
            }
            // Vendor registration with invitation id Ends <<<

            return View(locVenReg_form);
        }

        public ActionResult GetCountryStates(string country)
        {
            var states = _orgBusiness.GetCountryStates(country).Select(r => new SelectListItem() { Text = r.Value, Value = r.Value }).ToList();
            return Json(states, JsonRequestBehavior.AllowGet);
        }
        /*
            Created Date:  05/08/2013   Created By: Kiran Talluri
            Purpose: To save the form data into database
        */
        public bool isRegistrationValid(LocalVendorRegistrationModel locVenReg_form)
        {
            if (locVenReg_form.CountryType != (int)StateType.USA)
            {
                var taxerror = ModelState["TaxID"].Errors.FirstOrDefault(rec => rec.ErrorMessage == Resources.Resources.Common_InvalidTaxID);
                ModelState["TaxID"].Errors.Remove(taxerror);
            }

            return ModelState.IsValid;
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult   VendorRegistration(LocalVendorRegistrationModel locVenReg_form)
        {
            ViewBag.PageType = "Registration";
            ViewBag.checkBoxStatus = true;
            ViewBag.Countries = _orgBusiness.GetCountries().Select(r => new SelectListItem() { Text = r.Value, Value = r.Value }).ToList();
            if (isRegistrationValid(locVenReg_form))
            {
                if (locVenReg_form.CheckBoxStatus.Equals(false))
                {
                    ViewBag.checkBoxStatus = false;
                    return View(locVenReg_form);
                }
                ViewBag.isEmailExists = false;
                ViewBag.hasAppid = false;
                ViewBag.checkBoxStatus = true;
                ViewBag.isTaxIdExists = false; // Kiran on 8/5/2014

                // Kiran on 8/25/2014
                ViewBag.taxIdCountError = false;
                var taxIDCount = locVenReg_form.TaxID.Replace("-", "").Count();
                if (locVenReg_form.CountryType == (int)StateType.USA && (taxIDCount == 0 || taxIDCount != 9))
                {
                    ViewBag.taxIdCountError = true;
                    return View(locVenReg_form);
                }
                // Ends<<<

                SystemApplicationId sysAppId = entityDB.SystemApplicationId.FirstOrDefault();

                SystemUsersOrganizations sysUserOrg_currentUser = new SystemUsersOrganizations();
                SystemUsers sysUser_currentUser = entityDB.SystemUsers.FirstOrDefault(m => m.Email == locVenReg_form.Email);
                var vendorOrganizationWithTaxId = entityDB.Organizations.FirstOrDefault(record => record.TaxID.Replace("-", "") == locVenReg_form.TaxID.Replace("-", "")); // Kiran on 8/20/2014

                if (sysAppId == null)
                {
                    ViewBag.hasAppid = true;
                    return View(locVenReg_form);
                }
                if (sysUser_currentUser != null)
                {
                    ViewBag.isEmailExists = true;
                    return View(locVenReg_form);
                }
                // Kiran on 8/5/2014
                if (vendorOrganizationWithTaxId != null)
                {
                    ViewBag.isTaxIdExists = true;
                    ViewBag.VBTaxIdOrg = vendorOrganizationWithTaxId.Name;
                    return View(locVenReg_form);
                }
                // Ends<<<

                // Kiran on 02/09/2015
                if (locVenReg_form.InvitationId != null)
                {
                    var invitation_CurrentUser = entityDB.VendorInviteDetails.Find(locVenReg_form.InvitationId);
                    if (invitation_CurrentUser.VendorEmail.Trim() != locVenReg_form.Email.Trim())
                    {
                        try
                        {
                            invitation_CurrentUser.VendorEmail = locVenReg_form.Email.Trim();
                            entityDB.Entry(invitation_CurrentUser).State = EntityState.Modified;
                        }
                        catch
                        { }
                    }
                }
                // Ends<<<

                sysUserOrg_currentUser.SystemUsers = new SystemUsers();

                string strPassword = EncryptData(locVenReg_form.Password);

                sysUserOrg_currentUser.SystemUsers.ApplicationId = sysAppId.ApplicationId;
                sysUserOrg_currentUser.SystemUsers.Email = locVenReg_form.Email;
                sysUserOrg_currentUser.SystemUsers.Password = strPassword;
                sysUserOrg_currentUser.SystemUsers.UserName = locVenReg_form.Email;
                sysUserOrg_currentUser.SystemUsers.CreateDate = DateTime.Now;
                sysUserOrg_currentUser.SystemUsers.LastLoginDate = DateTime.Now;
                sysUserOrg_currentUser.SystemUsers.UserStatus = false;

                sysUserOrg_currentUser.Organizations = new Organizations();

                sysUserOrg_currentUser.SystemUsers.Contacts = new List<Contact>();

                sysUserOrg_currentUser.SystemUsers.Contacts.Add(new Contact());

                sysUserOrg_currentUser.Organizations.Name = locVenReg_form.Name;
                sysUserOrg_currentUser.Organizations.CountryType = locVenReg_form.CountryType;
                sysUserOrg_currentUser.Organizations.CopyAnswers = 2;
                sysUserOrg_currentUser.Organizations.Address1 = locVenReg_form.Address1;
                sysUserOrg_currentUser.Organizations.Address2 = locVenReg_form.Address2;
                sysUserOrg_currentUser.Organizations.Address3 = locVenReg_form.Address3; // kiran on 9/19/2014
                sysUserOrg_currentUser.Organizations.City = locVenReg_form.City;
                sysUserOrg_currentUser.Organizations.State = locVenReg_form.State;
                sysUserOrg_currentUser.Organizations.Zip = locVenReg_form.Zip;
                sysUserOrg_currentUser.Organizations.Country = locVenReg_form.Country;
                sysUserOrg_currentUser.Organizations.PhoneNumber = locVenReg_form.PhoneNumber;
                sysUserOrg_currentUser.Organizations.ExtNumber = locVenReg_form.ExtNumber; // Kiran on 8/26/2014
                sysUserOrg_currentUser.Organizations.FaxNumber = locVenReg_form.FaxNumber;
                sysUserOrg_currentUser.Organizations.WebsiteURL = locVenReg_form.WebsiteURL;
                sysUserOrg_currentUser.Organizations.FederalIDNumber = locVenReg_form.FederalIDNumber;
                sysUserOrg_currentUser.Organizations.OrganizationType = "Vendor";
                sysUserOrg_currentUser.Organizations.TaxID = locVenReg_form.TaxID; // Kiran on 8/5/2014
                sysUserOrg_currentUser.Organizations.PrincipalCompanyOfficerName = locVenReg_form.PrincipalCompanyOfficerName; // Kiran on  2/13/2014
                sysUserOrg_currentUser.Organizations.Language = locVenReg_form.Language;
                sysUserOrg_currentUser.Organizations.GeographicArea = string.Join(",",locVenReg_form.GeographicArea);
               sysUserOrg_currentUser.SystemUsers.Contacts[0].UserId = sysUserOrg_currentUser.SystemUsers.UserId;
                sysUserOrg_currentUser.SystemUsers.Contacts[0].FirstName = locVenReg_form.FirstName.Trim();
                sysUserOrg_currentUser.SystemUsers.Contacts[0].LastName = locVenReg_form.LastName.Trim();
                sysUserOrg_currentUser.SystemUsers.Contacts[0].ContactTitle = locVenReg_form.ContactTitle;
                sysUserOrg_currentUser.SystemUsers.Contacts[0].MobileNumber = locVenReg_form.MobileNumber;
                sysUserOrg_currentUser.SystemUsers.Contacts[0].PhoneNumber = locVenReg_form.PhoneNumber;

                // Kiran on 02/12/2015 to assign Super user role
                sysUserOrg_currentUser.PrimaryUser = true;
                sysUserOrg_currentUser.SystemUsersOrganizationsRoles = new List<SystemUsersOrganizationsRoles>();

                sysUserOrg_currentUser.SystemUsersOrganizationsRoles.Add(new SystemUsersOrganizationsRoles());
                var vendorSuperUserRoleId = entityDB.SystemRoles.FirstOrDefault(rec => rec.RoleName.Trim() == "Super User" && rec.OrganizationType == "Vendor").RoleId;

                sysUserOrg_currentUser.SystemUsersOrganizationsRoles[0].SysRoleId = vendorSuperUserRoleId;
                sysUserOrg_currentUser.SystemUsersOrganizationsRoles[0].SysUserOrgId = sysUserOrg_currentUser.SysUserOrganizationId;

                entityDB.SystemUsers.Add(sysUserOrg_currentUser.SystemUsers);
                entityDB.Organizations.Add(sysUserOrg_currentUser.Organizations);
                entityDB.SystemUsersOrganizations.Add(sysUserOrg_currentUser);
                // Ends<<<
                entityDB.Entry(sysUserOrg_currentUser.SystemUsers).State = EntityState.Added;
                entityDB.SaveChanges();

                Response.Cookies["Vendor"]["Password"] = locVenReg_form.Password;
                Response.Cookies["Vendor"]["ConfirmPassword"] = locVenReg_form.ConfirmPassword;

                // Kiran 07/05/2013 
                // Changed code to address registration with Invitation Id >>>
                // Kiran on 9/5/2014 - Displayed User name in email subject
                var subject = "Email Verification - " + locVenReg_form.Name.Trim();
                var languageCode = _orgBusiness.GetLanguageCode(locVenReg_form.Language);
                string strMessage = SendMailForVendorRegistration(locVenReg_form.Email, sysUserOrg_currentUser.SystemUsers.UserId + "", locVenReg_form.InvitationId + "", subject, languageCode);
                if (strMessage.ToString() == "Success")
                // Changes Ends <<<
                {
                    // Kiran on 8/7/2014
                    var pathForCSI = HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["WebResponseFiles"]);
                    try
                    {
                        Utilities.CSIWebServiceInitiateRequest.CSIWebService(sysUserOrg_currentUser.Organizations.OrganizationID, sysUserOrg_currentUser.Organizations.Name, sysUserOrg_currentUser.Organizations.PrincipalCompanyOfficerName, pathForCSI); // Kiran on 8/7/2014
                    }
                    catch { }
                    // Ends<<<
                    Session.Clear();
                    Session.Abandon();
                    HttpCookie cookie = Request.Cookies["Login"];
                    //cookie.Values["isLogin"] = "false";

                    if (cookie != null)  // Rajesh 07/08/2013 Added condition
                    {
                        Response.Cookies["Login"]["UserName"] = cookie.Values["UserName"];
                        //Response.Cookies["Login"].Expires = DateTime.Now.AddYears(1);
                        Response.Cookies["Login"]["Password"] = cookie.Values["Password"];
                        Response.Cookies["Login"]["isLogin"] = "false";

                        Response.Cookies["Login"].Expires = DateTime.Now.AddYears(1);
                    }
                    return View("~/Views/Vendor/VendorSuccessPage.cshtml");
                }
                else
                {
                    entityDB.Contact.Remove(sysUserOrg_currentUser.SystemUsers.Contacts[0]);
                    entityDB.SystemUsers.Remove(sysUserOrg_currentUser.SystemUsers);
                    entityDB.SystemUsersOrganizations.Remove(sysUserOrg_currentUser);
                    var user_Organization = entityDB.Organizations.FirstOrDefault(rec => rec.OrganizationID == sysUserOrg_currentUser.OrganizationId);
                    entityDB.Organizations.Remove(user_Organization);
                    entityDB.SaveChanges();

                    ViewBag.VBMailSent = false;
                    Response.Cookies["Vendor"]["Password"] = locVenReg_form.Password;
                    Response.Cookies["Vendor"]["ConfirmPassword"] = locVenReg_form.ConfirmPassword;
                    return View(locVenReg_form);
                }
                // Ends<<<

            }
            ViewBag.hasAppid = false;
            ViewBag.isEmailExists = false;
            Response.Cookies["Vendor"]["Password"] = locVenReg_form.Password;
            Response.Cookies["Vendor"]["ConfirmPassword"] = locVenReg_form.ConfirmPassword;
            return View(locVenReg_form);
        }

        /*
            Created Date:  06/14/2013   Created By: Kiran Talluri
            Purpose: To encrypt the password with a supplied key
        */
        private string EncryptData(string password)
        {
            try
            {
                TripleDESCryptoServiceProvider objDESCrypto = new TripleDESCryptoServiceProvider();
                MD5CryptoServiceProvider objHashMD5 = new MD5CryptoServiceProvider();
                byte[] byteHash, byteBuff;
                string strTempKey = ConfigurationSettings.AppSettings["encryptionkey"].ToString();
                byteHash = objHashMD5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(strTempKey));
                objHashMD5 = null;
                objDESCrypto.Key = byteHash;
                objDESCrypto.Mode = CipherMode.ECB; //CBC, CFB
                byteBuff = ASCIIEncoding.ASCII.GetBytes(password);
                return Convert.ToBase64String(objDESCrypto.CreateEncryptor().TransformFinalBlock(byteBuff, 0, byteBuff.Length));
            }
            catch (Exception ex)
            {
                return "Wrong Input. " + ex.Message;
            }
        }

        /*
            Created Date:  06/03/2013   Created By: Kiran Talluri
            Purpose: To send an verification email to vendor
        */
        public static string SendMailForVendorRegistration(String toAddress, string uid, string InvitationId, string subject, string languageCode)
        {
            String fromAddress = ConfigurationSettings.AppSettings["fromMail"].ToString();
            String fromPassword = ConfigurationSettings.AppSettings["fromPassword"].ToString();
            String smtpHost = ConfigurationSettings.AppSettings["smtpHost"].ToString();
            String smtpPort = ConfigurationSettings.AppSettings["smtpPort"].ToString();
            bool useSSL = ConfigurationSettings.AppSettings["useSSL"] == "True";
            bool isTest = ConfigurationSettings.AppSettings["IsTest"] == "true";

            string cc = ConfigurationSettings.AppSettings["VendorRegistrationAuthorizationEmail"].ToString();

            if (isTest)
            {
                toAddress = ConfigurationSettings.AppSettings["testMail"];
                cc = "";
            }

            //String subject = ConfigurationSettings.AppSettings["subject"].ToString();

            // Kiran 07/07/2013
            // Changes for vendor registration with invitation id
            //string url = ConfigurationSettings.AppSettings["redirectionURL"].ToString() + uid;
            string url = ConfigurationSettings.AppSettings["redirectionURL"].ToString() + uid + "&InvitationId= " + InvitationId;
            // Changes Ends <<<


            var fileName = "~/HtmlTemplates/VendorRegistration_EmailFormat";
            var file = Path.Combine(System.Web.HttpContext.Current.Server.MapPath(fileName));
            
            file = file + "_" + languageCode + ".htm";
            if (!System.IO.File.Exists(file))
            {
                file = System.Web.HttpContext.Current.Server.MapPath(fileName + "_EN.htm");
            }
            string body = System.IO.File.ReadAllText(file);
            
            body = body.Replace("urlLink", url);
            body = body.Replace("clickURL", url);
            //String body = "<!DOCTYPE HTML><html><head><meta http-equiv='Content-Type' content='text/html; rset=utf-8'><title>Email Format  </title></head><body><style>body{ margin:0px; padding:0px;}.eMailFormat_content{ float:left; width:500px; min-height:150px; height:auto;   font-family:Arial, Helvetica, sans-serif; font-size:12px;  }.eMailFormat_FV{ float:left; width:480px; min-height:10px; height:auto; padding:10px; font-size:14px; font-weight:bold;   text-transform:uppercase; }.eMailFormat_PassReset{ float:left; width:470px; min-height:150px; height:auto;  padding:70px 20px 10px 10px; font-size:12px;  border-bottom:3px solid #000; border-top:3px solid #000;  } .eMailFormat_PassReset_head{ float:left; font-size:16px; font-weight:bold;color:#A61417; padding-left:1px;   } .eMailFormat_PassReset_p { float:left; width:100%;  }.eMailFormat_PassReset_plink{float:left; width:80%; margin-top:-5px;   }a {}a:link{ text-decoration:underline; color:#0496ba;   }a:visited{ color:#0496ba;   }.botPara{ float:left; width:130px;  min-height:150px; height:auto; margin-top:20px; margin-left:10px;  line-height:1.5em; color:#ccc;  }</style><div class='eMailFormat_content'> <div class='eMailFormat_FV'>first,verify</div> <div class='eMailFormat_PassReset'> <div class='eMailFormat_PassReset_head'>Thank you for registering with FIRST, VERIFY. Please activate your account to continue.</div>  <p class='eMailFormat_PassReset_p'>Before you can login to FIRST, VERIFY, you need to activate your new account. Click the link below to activate your account and login to FIRST, VERIFY. If you have trouble clicking the link, please copy and paste the URL into your web browser. <p>Activate your new account:</p> <p class='eMailFormat_PassReset_plink'><a href=" + url + "> Click </a></p> </div> <br /> <p class='botPara'>Copyright 2013, FIRST, VERIFY <Br />FIRST, VERIFY <Br />PO Box:3414 <br />Richmond, VA 23235<br /></p></div></body></html>"; 
            //"<html><body><p>Body with Guid </p> <a href=" + url + "> Verify Email </a>  <p>";
            //body = body + uid + "</p> </body></html>";

            MailMessage mail = new MailMessage(fromAddress, toAddress);
            mail.Subject = subject;
            mail.Body = body;
            mail.BodyEncoding = Encoding.UTF8;
            mail.IsBodyHtml = true;

            if (!string.IsNullOrEmpty(cc))
                mail.CC.Add(cc); // Kiran on 8/20/2014
            //mail.CC.Add("jillianr@firstverify.com");// Mani on 8/13/2015 for fv-183

            SmtpClient client = new SmtpClient();
            client.Port = Convert.ToInt32(smtpPort);
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.Host = smtpHost;
            //client.EnableSsl = true;
            // Kiran on 10/14/2015
            if (useSSL)
            {
                client.EnableSsl = true;
            }
            // Ends<<<
            client.Credentials = new System.Net.NetworkCredential(fromAddress, fromPassword);

            try
            {
                client.Send(mail);
            }
            catch (SmtpException exception)
            {
                //Console.WriteLine("Mail Sending Failed");
                return "Mail Sending Failed" + exception.Message;
            }
            return "Success";
        }

        public ActionResult Index()
        {
            return View();
        }

        // SUMA On (MM/DD/YYYY)6/17/2013 for VendorDashboard >>>
        [SessionExpire]
        [HeaderAndSidebar(headerName = "DashBoard", sideMenuName = "", ViewName = "VendorDashboard")]
        public ActionResult VendorDashboard()
        {
            Guid guidRecordId = (Guid)Session["UserId"];
            ViewBag.VBrole = Session["RoleName"];
            ViewBag.VBheaderlist = new List<LocalSideMenuModel>()
            {
                   new LocalSideMenuModel("My Account",  "MyAccount", "SystemUsers", false),
                   new LocalSideMenuModel("Change Password",  "ChangePassword", "SystemUsers", true),
                   new LocalSideMenuModel("Logout",  "Logout", "SystemUsers", false)
            };

            var varVendorOrgList = from sysOrgList in entityDB.SystemUsersOrganizations.Where(m => m.UserId == guidRecordId).ToList()
                                   select new SelectListItem
                                   {
                                       Text = sysOrgList.Organizations.Name,
                                       Value = sysOrgList.OrganizationId.ToString()
                                   };

            ViewBag.VBVendorOrgList = varVendorOrgList.ToList();

            // Kiran 11/27/2013 >>>
            //var loginVendorId = Convert.ToInt64(Session["currentOrgId"]);
            //Session["LoginOrganizationName"] = entityDB.Organizations.FirstOrDefault(rec => rec.OrganizationID == loginVendorId).Name;
            // Ends <<<

            long vendorId = Convert.ToInt64(varVendorOrgList.FirstOrDefault().Value);
            Session["SelectedVendorId"] = vendorId;//RP 1/12/2013-->>For initali Org is selected Org



            //Rajesh on 2/14/2015




            List<LocalNotifications> notifications = new List<LocalNotifications>();


            var orgNSent = entityDB.OrganizationsNotificationsSent.Where(rec => rec.SetupType == 1).Select(rec => rec.NotificationId).ToList();
            //Rajesh on 2/13/2015
            var orgNotificationsEmailLogs = entityDB.OrganizationsNotificationsEmailLogs.Where(rec => rec.VendorId == vendorId && rec.NotificationType == 0 && orgNSent.Contains(rec.NotificationId)).OrderByDescending(rec => rec.EmailSentDateTime).Take(31);
            foreach (var orglog in orgNotificationsEmailLogs.ToList())
            {
                LocalNotifications locNotification = new LocalNotifications();

                if (notifications.Count >= 30)
                    break;

                //Rajesh on 12/12/2013
                locNotification.HasNext = false;
                locNotification.HasPrevious = false;
                locNotification.PageNumber = 0;
                locNotification.EmailSentDate = orglog.EmailSentDateTime; // kiran on 9/8/2014
                //<<Ends


                locNotification.ClientName = orglog.OrganizationsNotificationsSent.Client.Name;

                //Rajesh on 12/12/2013
                locNotification.VendorName = orglog.Vendor.Name + "";//entityDB.OrganizationsNotificationsEmailLogs.FirstOrDefault().Vendor.Name;//
                //Ends
                locNotification.Purpose = "";
                OrganizationsEmailSetupNotifications orgEmailSetupNotification = entityDB.OrganizationsEmailSetupNotifications.FirstOrDefault(rec => rec.OrgEmailSetupId == orglog.OrganizationsNotificationsSent.SetupTypeId);
                if (orgEmailSetupNotification != null)
                {
                    if (orgEmailSetupNotification.SetupType == 0)//Document
                    {
                        locNotification.Purpose = entityDB.DocumentType.Find(orgEmailSetupNotification.SetupTypeId).DocumentTypeName;
                    }
                    else if (orgEmailSetupNotification.SetupType == 1)//Status
                    {
                        locNotification.Purpose = entityDB.PrequalificationStatus.Find(orgEmailSetupNotification.SetupTypeId).PrequalificationStatusName;
                    }
                }

                //Dec 4th
                locNotification.Comments = orglog.Comments;
                if (orglog.Comments != null)
                {
                    if (orglog.Comments.Length > 30)
                        locNotification.SmallComment = orglog.Comments.Substring(0, 30);
                    else
                        locNotification.SmallComment = orglog.Comments;
                }
                else
                    locNotification.SmallComment = "";
                notifications.Add(locNotification);
            }

            if (orgNotificationsEmailLogs.Count() > 30)
            {
                notifications.ForEach(rec => rec.HasNext = true);
            }
            ViewBag.NotificationsSent = notifications;
            ViewBag.NotificationCount = entityDB.OrganizationsNotificationsEmailLogs.Where(rec => rec.VendorId == vendorId && rec.NotificationType == 0 && orgNSent.Contains(rec.NotificationId) && rec.ReadByVendor == 0).Count();

            //Ends<<<



            //Rajesh on 09/06/2013 >>>
            //siva on Dec 4th
            //int notificationCount = 0;
            //List<LocalNotifications> notifications = new List<LocalNotifications>();

            //var orgNSent = entityDB.OrganizationsNotificationsSent.Where(rec => rec.SetupType == 1).ToList();
            //int totalCount = orgNSent.Sum(recO => recO.OrganizationsNotificationsEmailLogs.Where(rec => rec.VendorId == vendorId && rec.NotificationType == 0).GroupBy(rec => rec.VendorId).Count());//Rajesh on 12/12/2013

            //foreach (var orgNotificationSent in orgNSent)
            //{
            //    if (notifications.Count >= 30)
            //        break;
            //    //var orgSentCount = orgNotificationSent.OrganizationsNotificationsEmailLogs.Where(rec => rec.VendorId == vendorId && rec.NotificationType == 0).GroupBy(rec => rec.VendorId).Count();
            //    foreach (var logs in orgNotificationSent.OrganizationsNotificationsEmailLogs.Where(rec => rec.VendorId == vendorId && rec.NotificationType == 0).GroupBy(rec => rec.VendorId))
            //    {

            //        var orglog = (OrganizationsNotificationsEmailLogs)logs.OrderBy(rec => rec.NotificationNo).LastOrDefault();

            //        if (notifications.Count >= 30)
            //            break;

            //        LocalNotifications locNotification = new LocalNotifications();

            //        //Rajesh on 12/12/2013
            //        locNotification.HasNext = false;
            //        locNotification.HasPrevious = false;
            //        locNotification.PageNumber = 0;
            //        locNotification.EmailSentDate = orglog.EmailSentDateTime; // kiran on 02/03/2015
            //        //<<Ends

            //        locNotification.ClientName = orgNotificationSent.Client.Name;
            //        locNotification.VendorName = orglog.Vendor.Name; //entityDB.OrganizationsNotificationsEmailLogs.FirstOrDefault(rec => rec.VendorId == vendorId).Vendor.Name;//orglog.Vendor.OrganizationID+"";
            //        locNotification.Purpose = "";
            //        OrganizationsEmailSetupNotifications orgEmailSetupNotification = entityDB.OrganizationsEmailSetupNotifications.FirstOrDefault(rec => rec.OrgEmailSetupId == orgNotificationSent.SetupTypeId);
            //        if (orgEmailSetupNotification != null)
            //        {
            //            if (orgEmailSetupNotification.SetupType == 0)//Document
            //            {
            //                locNotification.Purpose = entityDB.DocumentType.Find(orgEmailSetupNotification.SetupTypeId).DocumentTypeName;
            //            }
            //            else if (orgEmailSetupNotification.SetupType == 1)//Status
            //            {
            //                locNotification.Purpose = entityDB.PrequalificationStatus.Find(orgEmailSetupNotification.SetupTypeId).PrequalificationStatusName;
            //            }
            //        }

            //        //Dec 4th
            //        locNotification.Comments = orglog.Comments;
            //        if (orglog.Comments != null)
            //        {
            //            if (orglog.Comments.Length > 30)
            //                locNotification.SmallComment = orglog.Comments.Substring(0, 30);
            //            else
            //                locNotification.SmallComment = orglog.Comments;
            //        }
            //        else
            //            locNotification.SmallComment = "";

            //        if (orglog.ReadByVendor == 0) //0 means unread notification
            //            notificationCount += 1;
            //        //End Dec 4th

            //        notifications.Add(locNotification);
            //    }
            //}
            //if (totalCount > 30)
            //{
            //    notifications.ForEach(rec => rec.HasNext = true);
            //}
            ////Dec 4th
            //ViewBag.NotificationsSent = notifications;
            //ViewBag.NotificationCount = notificationCount;

            ViewBag.PreQualificationsStatus = entityDB.PrequalificationStatus.Where(rec => rec.ShowInDashBoard == true).ToList();
            //Ends<<<<

            //Siva on Nov 30th
            try
            {
                var filePath = Path.Combine(System.Web.HttpContext.Current.Server.MapPath("~/Views/SystemUsers/Instruction.txt"));
                if (System.IO.File.Exists(filePath))
                {
                    ViewBag.Instruction = System.IO.File.ReadAllText(filePath);
                }
            }
            catch (Exception ex)
            {
            }

            ViewBag.LogoPath = ConfigurationSettings.AppSettings["LogoUploadPath"].ToString() + "/";//Rajesh on 08/09/2013
            return View();
        }

        [HttpPost]
        public ActionResult NotificationsPartialView(int pageNumber)//Every Page contains 30 notifications
        {

            long vendorId = (long)Session["SelectedVendorId"];
            List<LocalNotifications> notifications = new List<LocalNotifications>();

            var NumberOfItemsToSkip = pageNumber * 30;

            var orgNSent = entityDB.OrganizationsNotificationsSent.Where(rec => rec.SetupType == 1).Select(rec => rec.NotificationId).ToList();
            //Rajesh on 2/13/2015
            var orgNotificationsEmailLogs = entityDB.OrganizationsNotificationsEmailLogs.Where(rec => rec.VendorId == vendorId && rec.NotificationType == 0 && orgNSent.Contains(rec.NotificationId)).OrderByDescending(rec => rec.EmailSentDateTime).Skip(NumberOfItemsToSkip).Take(31);
            foreach (var orglog in orgNotificationsEmailLogs.ToList())
            {
                LocalNotifications locNotification = new LocalNotifications();

                if (notifications.Count >= 30)
                    break;

                //Rajesh on 12/12/2013
                locNotification.HasNext = false;
                locNotification.HasPrevious = pageNumber != 0;
                locNotification.PageNumber = pageNumber;
                locNotification.EmailSentDate = orglog.EmailSentDateTime; // kiran on 9/8/2014
                //<<Ends


                locNotification.ClientName = orglog.OrganizationsNotificationsSent.Client.Name;

                //Rajesh on 12/12/2013
                locNotification.VendorName = orglog.Vendor.Name + "";//entityDB.OrganizationsNotificationsEmailLogs.FirstOrDefault().Vendor.Name;//
                //Ends
                locNotification.Purpose = "";
                OrganizationsEmailSetupNotifications orgEmailSetupNotification = entityDB.OrganizationsEmailSetupNotifications.FirstOrDefault(rec => rec.OrgEmailSetupId == orglog.OrganizationsNotificationsSent.SetupTypeId);
                if (orgEmailSetupNotification != null)
                {
                    if (orgEmailSetupNotification.SetupType == 0)//Document
                    {
                        locNotification.Purpose = entityDB.DocumentType.Find(orgEmailSetupNotification.SetupTypeId).DocumentTypeName;
                    }
                    else if (orgEmailSetupNotification.SetupType == 1)//Status
                    {
                        locNotification.Purpose = entityDB.PrequalificationStatus.Find(orgEmailSetupNotification.SetupTypeId).PrequalificationStatusName;
                    }
                }

                //Dec 4th
                locNotification.Comments = orglog.Comments;
                if (orglog.Comments != null)
                {
                    if (orglog.Comments.Length > 30)
                        locNotification.SmallComment = orglog.Comments.Substring(0, 30);
                    else
                        locNotification.SmallComment = orglog.Comments;
                }
                else
                    locNotification.SmallComment = "";

                notifications.Add(locNotification);
            }

            if (orgNotificationsEmailLogs.Count() > 30)
            {
                notifications.ForEach(rec => rec.HasNext = true);
            }
            ViewBag.NotificationsSent = notifications;
            ViewBag.NotificationCount = entityDB.OrganizationsNotificationsEmailLogs.Where(rec => rec.VendorId == vendorId && rec.NotificationType == 0 && orgNSent.Contains(rec.NotificationId) && rec.ReadByVendor == 0).Count();
            return PartialView();

        }
        //public ActionResult NotificationsPartialView(int pageNumber)//Every Page contains 30 notifications
        //{

        //    long vendorId = (long)Session["SelectedVendorId"];
        //    int notificationCount = 0;
        //    List<LocalNotifications> notifications = new List<LocalNotifications>();

        //    var NumberOfItemsToSkip = pageNumber * 30;

        //    var orgNSent = entityDB.OrganizationsNotificationsSent.Where(rec => rec.SetupType == 1).ToList();
        //    int totalCount = orgNSent.Sum(recO => recO.OrganizationsNotificationsEmailLogs.Where(rec => rec.VendorId == vendorId && rec.NotificationType == 0).GroupBy(rec => rec.VendorId).Count());//Rajesh on 12/12/2013


        //    foreach (var orgNotificationSent in orgNSent)
        //    {
        //        if (notifications.Count >= 30)
        //            break;
        //        //var orgSentCount = orgNotificationSent.OrganizationsNotificationsEmailLogs.Where(rec => rec.VendorId == vendorId && rec.NotificationType == 0).GroupBy(rec => rec.VendorId);
        //        foreach (var logs in orgNotificationSent.OrganizationsNotificationsEmailLogs.Where(rec => rec.VendorId == vendorId && rec.NotificationType == 0).GroupBy(rec => rec.VendorId))
        //        {
        //            if (NumberOfItemsToSkip > 0)
        //            {
        //                NumberOfItemsToSkip--;
        //                continue;
        //            }
        //            var orglog = (OrganizationsNotificationsEmailLogs)logs.OrderBy(rec => rec.NotificationNo).LastOrDefault();

        //            LocalNotifications locNotification = new LocalNotifications();
        //            if (notifications.Count >= 30)
        //                break;

        //            //Rajesh on 12/12/2013
        //            locNotification.HasNext = false;
        //            locNotification.HasPrevious = pageNumber != 0;
        //            locNotification.PageNumber = pageNumber;
        //            locNotification.EmailSentDate = orglog.EmailSentDateTime; // kiran on 02/03/2015
        //            //<<Ends

        //            locNotification.ClientName = orgNotificationSent.Client.Name;
        //            locNotification.VendorName = orglog.Vendor.Name; //entityDB.OrganizationsNotificationsEmailLogs.FirstOrDefault(rec => rec.VendorId == vendorId).Vendor.Name;//orglog.Vendor.OrganizationID+"";
        //            locNotification.Purpose = "";
        //            OrganizationsEmailSetupNotifications orgEmailSetupNotification = entityDB.OrganizationsEmailSetupNotifications.FirstOrDefault(rec => rec.OrgEmailSetupId == orgNotificationSent.SetupTypeId);
        //            if (orgEmailSetupNotification != null)
        //            {
        //                if (orgEmailSetupNotification.SetupType == 0)//Document
        //                {
        //                    locNotification.Purpose = entityDB.DocumentType.Find(orgEmailSetupNotification.SetupTypeId).DocumentTypeName;
        //                }
        //                else if (orgEmailSetupNotification.SetupType == 1)//Status
        //                {
        //                    locNotification.Purpose = entityDB.PrequalificationStatus.Find(orgEmailSetupNotification.SetupTypeId).PrequalificationStatusName;
        //                }
        //            }

        //            //Dec 4th
        //            locNotification.Comments = orglog.Comments;
        //            if (orglog.Comments != null)
        //            {
        //                if (orglog.Comments.Length > 30)
        //                    locNotification.SmallComment = orglog.Comments.Substring(0, 30);
        //                else
        //                    locNotification.SmallComment = orglog.Comments;
        //            }
        //            else
        //                locNotification.SmallComment = "";

        //            if (orglog.ReadByVendor == 0) //0 means unread notification
        //                notificationCount += 1;
        //            //End Dec 4th

        //            notifications.Add(locNotification);
        //        }
        //    }
        //    if (totalCount > 30 * (pageNumber + 1))
        //    {
        //        notifications.ForEach(rec => rec.HasNext = true);
        //    }
        //    //Dec 4th
        //    ViewBag.NotificationsSent = notifications;
        //    ViewBag.NotificationCount = notificationCount;
        //    return PartialView();

        //}
        // Suma Http Get Vendor Dashboard Ends <<<


        //public ActionResult VendorDashboard()
        //{
        //    Guid guidRecordId = (Guid)Session["UserId"];
        //    ViewBag.VBrole = Session["RoleName"];
        //    ViewBag.VBheaderlist = new List<LocalSideMenuModel>()
        //    {
        //           new LocalSideMenuModel("My Account",  "MyAccount", "SystemUsers", false),
        //           new LocalSideMenuModel("Change Password",  "ChangePassword", "SystemUsers", true),
        //           new LocalSideMenuModel("Logout",  "Logout", "SystemUsers", false)
        //    };

        //    var varVendorOrgList = from sysOrgList in entityDB.SystemUsersOrganizations.Where(m => m.UserId == guidRecordId).ToList()
        //                           select new SelectListItem
        //                           {
        //                               Text = sysOrgList.Organizations.Name,
        //                               Value = sysOrgList.OrganizationId.ToString()
        //                           };

        //    ViewBag.VBVendorOrgList = varVendorOrgList.ToList();

        //    // Kiran 11/27/2013 >>>
        //    var loginVendorId = Convert.ToInt64(Session["currentOrgId"]);
        //    Session["LoginOrganizationName"] = entityDB.Organizations.FirstOrDefault(rec => rec.OrganizationID == loginVendorId).Name;
        //    // Ends <<<

        //    long vendorId = Convert.ToInt64(varVendorOrgList.FirstOrDefault().Value);
        //    Session["SelectedVendorId"] = vendorId;//RP 1/12/2013-->>For initali Org is selected Org

        //    //Rajesh on 09/06/2013 >>>
        //    //siva on Dec 4th
        //    int notificationCount = 0;
        //    List<LocalNotifications> notifications = new List<LocalNotifications>();
        //    foreach (var orgNotificationSent in entityDB.OrganizationsNotificationsSent.Where( rec => rec.SetupType == 1).ToList())
        //    {
        //        foreach (var logs in orgNotificationSent.OrganizationsNotificationsEmailLogs.Where(rec => rec.VendorId == vendorId && rec.NotificationType == 0).GroupBy(rec => rec.VendorId))
        //        {
        //            var orglog = (OrganizationsNotificationsEmailLogs)logs.OrderBy(rec => rec.NotificationNo).LastOrDefault();

        //            LocalNotifications locNotification = new LocalNotifications();
        //            locNotification.ClientName = orgNotificationSent.Client.Name;
        //            locNotification.VendorName = orglog.Vendor.Name; //entityDB.OrganizationsNotificationsEmailLogs.FirstOrDefault(rec => rec.VendorId == vendorId).Vendor.Name;//orglog.Vendor.OrganizationID+"";
        //            locNotification.Purpose = "";
        //            OrganizationsEmailSetupNotifications orgEmailSetupNotification = entityDB.OrganizationsEmailSetupNotifications.FirstOrDefault(rec => rec.OrgEmailSetupId == orgNotificationSent.SetupTypeId);
        //            if (orgEmailSetupNotification != null)
        //            {
        //                if (orgEmailSetupNotification.SetupType == 0)//Document
        //                {
        //                    locNotification.Purpose = entityDB.DocumentType.Find(orgEmailSetupNotification.SetupTypeId).DocumentTypeName;
        //                }
        //                else if (orgEmailSetupNotification.SetupType == 1)//Status
        //                {
        //                    locNotification.Purpose = entityDB.PrequalificationStatus.Find(orgEmailSetupNotification.SetupTypeId).PrequalificationStatusName;
        //                }
        //            }

        //            //Dec 4th
        //            locNotification.Comments = orglog.Comments;
        //            if (orglog.Comments != null)
        //            {
        //                if (orglog.Comments.Length > 30)
        //                    locNotification.SmallComment = orglog.Comments.Substring(0, 30);
        //                else
        //                    locNotification.SmallComment = orglog.Comments;
        //            }
        //            else
        //                locNotification.SmallComment = "";

        //            if (orglog.ReadByVendor == 0) //0 means unread notification
        //                notificationCount += 1;
        //            //End Dec 4th

        //            notifications.Add(locNotification);
        //        }
        //    }

        //    //Dec 4th
        //    ViewBag.NotificationsSent = notifications;
        //    ViewBag.NotificationCount = notificationCount;

        //    ViewBag.PreQualificationsStatus = entityDB.PrequalificationStatus.ToList();
        //    //Ends<<<<

        //    //Siva on Nov 30th
        //    try
        //    {
        //        var filePath = Path.Combine(System.Web.HttpContext.Current.Server.MapPath("~/Views/SystemUsers/Instruction.txt"));
        //        if (System.IO.File.Exists(filePath))
        //        {
        //            ViewBag.Instruction = System.IO.File.ReadAllText(filePath);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //    }

        //    ViewBag.LogoPath = ConfigurationSettings.AppSettings["LogoUploadPath"].ToString() + "/";//Rajesh on 08/09/2013
        //    return View();
        //}
        //// Suma Http Get Vendor Dashboard Ends <<<

        //siva on Dec 4th
        [SessionExpire]
        [HeaderAndSidebar(headerName = "DashBoard", sideMenuName = "")]
        public string markAllNotificationsAsRead()
        {
            string statusStr = "updateSuccess";

            //List<OrganizationsNotificationsEmailLogs> logs = entityDB.OrganizationsNotificationsEmailLogs.Where(log => log.ReadByVendor == 0).ToList();
            //foreach (var emailLog in logs)
            //{
            //    emailLog.ReadByVendor = 1;
            //    entityDB.Entry(emailLog).State = EntityState.Modified;

            //    statusStr = "updateSuccess";
            //}
            //entityDB.SaveChanges();

            entityDB.Database.ExecuteSqlCommand(
                "update OrganizationsNotificationsEmailLogs set ReadByVendor =1 where ReadByVendor=0");

            ViewBag.NotificationCount = 0;

            return statusStr;
        }


        // SUMA On (MM/DD/YYYY)6/19/2013 for Vendor Dashboard Partial View >>>
        [SessionExpireForView]
        [OutputCache(Duration = 0)]
        public ActionResult VendorDashboardPartialView(long Vid)
        {
            ViewBag.LogoPath = ConfigurationSettings.AppSettings["LogoUploadPath"].ToString() + "/";//Rajesh on 08/09/2013
            Session["SelectedVendorId"] = Vid;
            long[] prequalificationStatus = { 1 };//Rajesh on 3/24/2014 // Kiran on 03/09/2015
            var VendorPdata = entityDB.Prequalification.Where(m => m.VendorId == Vid && !prequalificationStatus.Contains(m.PrequalificationStatusId) && (m.ClientTemplates == null || m.ClientTemplates.Templates.IsHide != true)).OrderByDescending(rec => rec.PrequalificationStart).ToList();
            var varPrequalificationlist = VendorPdata.ToList();
            ViewBag.VBPrequalificationdetails = varPrequalificationlist.ToList();

            //To Get Employees related to Vendor Rajesh on 11/06/2014
            var empTrainingRoleId = EmployeeRole.EmployeeRoleId;
           // var empTrainingRoleId = entityDB.SystemRoles.FirstOrDefault(rec => rec.RoleName == "Employee Training").RoleId;
            var empUserOrgRoles = entityDB.SystemUsersOrganizationsRoles.Where(rec => empTrainingRoleId.Contains(rec.SysRoleId) && rec.SystemUsersOrganizations.OrganizationId == Vid).ToList();
            // Kiran on 12/18/2014
            List<SystemUsersOrganizationsQuizzes> empQuizzes = new List<SystemUsersOrganizationsQuizzes>();
            foreach (var emplSysUserOrgRole in empUserOrgRoles)
            {
                var empSysUserOrgQuizzes = emplSysUserOrgRole.SystemUsersOrganizations.SystemUsersOrganizationsQuizzes.ToList();

                empQuizzes.AddRange(empSysUserOrgQuizzes.Where(rec => rec.AllowRetakeTraining == false && rec.QuizResult != true).ToList());// sumanth on 10/29/2015 for FVOS-100
            }
            ViewBag.empQuizzes = empQuizzes;
            //Ends<<<

            return PartialView("VendorDashboardPartialView");

        }
        //Rajesh on 11/06/2014
        [HttpGet]
        [HeaderAndSidebar(headerName = "PREQUALIFICATIONREQUIREMENTS", sideMenuName = "", ViewName = "QuizPayment")]
        [SessionExpire]//Rajesh on 11/10/2014
        public ActionResult QuizPayment(long Vid, long Cid)
        {
            //To Get Employees related to Vendor Rajesh on 11/06/2014
            var empTrainingRoleId = EmployeeRole.EmployeeRoleId;
           // var empTrainingRoleId = entityDB.SystemRoles.FirstOrDefault(rec => rec.RoleName == "Employee Training").RoleId;
            var empUserOrgRoles = entityDB.SystemUsersOrganizationsRoles.Where(rec => empTrainingRoleId.Contains(rec.SysRoleId) && rec.SystemUsersOrganizations.OrganizationId == Vid).ToList();
            // Kiran on 12/18/2014
            List<SystemUsersOrganizationsQuizzes> empQuizzes = new List<SystemUsersOrganizationsQuizzes>();
            foreach (var emplSysUserOrgRole in empUserOrgRoles)
            {
                var empSysUserOrgQuizzes = emplSysUserOrgRole.SystemUsersOrganizations.SystemUsersOrganizationsQuizzes.ToList();
                empQuizzes.AddRange(empSysUserOrgQuizzes.Where(rec => rec.ClientId == Cid && rec.AllowRetakeTraining == false && rec.QuizResult != true).ToList()); // sumanth on 10/29/2015 for FVOS-100
            }
            ViewBag.empQuizzes = empQuizzes;
            //Ends<<<

            return View();
        }
        // kiran on 11/7/2014
        [HttpPost]
        [HeaderAndSidebar(headerName = "PREQUALIFICATIONREQUIREMENTS", sideMenuName = "", ViewName = "QuizPayment")]
        [SessionExpire]//Rajesh on 11/10/2014
        public ActionResult QuizPayment(String Quizids, String payment, long Cid, long Vid)
        {
            LocalQuizPayment quizPaymentdetails = new LocalQuizPayment();
            quizPaymentdetails.ClientId = Cid;
            quizPaymentdetails.VendorId = Vid;
            quizPaymentdetails.PaymenyAmount = payment;
            quizPaymentdetails.EmpQuizIds = Quizids;
            return RedirectToAction("PostToPaypalForTraining", quizPaymentdetails);
        }

        public ActionResult PostToPaypalForTraining(LocalQuizPayment empQuizDetails)
        {
            LocalPaypalForTraining paypalForTraining = new LocalPaypalForTraining();
            paypalForTraining.cmd = "_xclick";
            paypalForTraining.business = ConfigurationManager.AppSettings["BusinessAccountKey"];
            bool useSandbox = Convert.ToBoolean(ConfigurationManager.AppSettings["UseSandbox"]);
            if (useSandbox)
                ViewBag.actionURL = "https://www.sandbox.paypal.com/cgi-bin/webscr";
            else
                ViewBag.actionURL = "https://www.paypal.com/cgi-bin/webscr";

            paypalForTraining.cancel_return = ConfigurationManager.AppSettings["CancelURLForTraining"];
            paypalForTraining.@return = ConfigurationManager.AppSettings["ReturnURLForQuizRetakeTraining"] + empQuizDetails.EmpQuizIds; // changed by sumanth on 10/29/2015 for FVOS-100 to update the quiz retake details when payment is done.
            paypalForTraining.notify_url = ConfigurationManager.AppSettings["NotifyURLForTraining"].ToString();
            paypalForTraining.currency_code = ConfigurationManager.AppSettings["CurrencyCode"];

            paypalForTraining.item_name = empQuizDetails.EmpQuizIds;
            paypalForTraining.amount = empQuizDetails.PaymenyAmount;
            return View(paypalForTraining);
        }

        //sumanth on 10/29/2015 for FVOS-100 to update the Allow Retake Training to when the retake fee is paid.
        public ActionResult UpdateQuizRetakePaymentDetails(String Quizids)
        {
            if (Quizids != null)
            {
                foreach (var employee in Quizids.Split(','))
                {
                    var empQuiz = entityDB.SystemUsersOrganizationsQuizzes.Find(Convert.ToInt64(employee));
                    empQuiz.AllowRetakeTraining = true;
                    empQuiz.RetakeFeeAmount = empQuiz.ClientTemplates.ClientTemplatesQuizDetails.FirstOrDefault().QuziRetakeFee;
                    //empQuiz.NoOfAttempts = 0;
                    entityDB.Entry(empQuiz).State = EntityState.Modified;
                    entityDB.SaveChanges();
                }
            }
            return Redirect("VendorDashboard");
        }

        [HttpPost]
        public ActionResult NotifyURLForQuiz()
        {
            byte[] param = Request.BinaryRead(Request.ContentLength);
            string strRequest = Encoding.ASCII.GetString(param);

            // append PayPal verification code to end of string
            strRequest += "&cmd=_notify-validate";

            // create an HttpRequest channel to perform handshake with PayPal
            bool useSandbox = Convert.ToBoolean(ConfigurationManager.AppSettings["UseSandbox"]);
            HttpWebRequest req;
            if (useSandbox)
                req = (HttpWebRequest)WebRequest.Create(@"https://www.sandbox.paypal.com/cgi-bin/webscr");
            else
                req = (HttpWebRequest)WebRequest.Create(@"https://www.paypal.com/cgi-bin/webscr");
            // Ends<<<
            req.Method = "POST";
            req.ContentType = "application/x-www-form-urlencoded";
            req.ContentLength = strRequest.Length;

            StreamWriter streamOut = new StreamWriter(req.GetRequestStream(), Encoding.ASCII);
            streamOut.Write(strRequest);
            streamOut.Close();

            // receive response from PayPal
            StreamReader streamIn = new StreamReader(req.GetResponse().GetResponseStream());
            string strResponse = streamIn.ReadToEnd();
            streamIn.Close();

            string txn_id = Request.Form["txn_id"];
            // if PayPal response is successful / verified
            if (strResponse.Equals("VERIFIED"))
            {
                // paypal has verified the data, it is safe for us to perform processing now

                // extract the form fields expected: buyer and seller email, payment status, amount
                string payerEmail = Request.Form["payer_email"];
                string paymentStatus = Request.Form["payment_status"];
                string receiverEmail = Request.Form["receiver_email"];
                string metaData = Request.Form["mc_gross"];
                string transactionId = Request.Form["txn_id"];
                string paymentAmount = Request.Form["mc_gross"];
                string itemName = Request.Form["item_name"];

                if (paymentStatus.Equals("Completed") || paymentStatus.Equals("Pending"))
                {
                    foreach (var employee in itemName.Split(','))
                    {
                        var empQuiz = entityDB.SystemUsersOrganizationsQuizzes.Find(employee); // kiran on 12/18/2014 
                        empQuiz.AllowRetakeTraining = true;
                        empQuiz.RetakeFeeAmount = empQuiz.ClientTemplates.ClientTemplatesQuizDetails.FirstOrDefault().QuziRetakeFee;
                        //empQuiz.NoOfAttempts = 0;
                        entityDB.Entry(empQuiz).State = EntityState.Modified;
                        entityDB.SaveChanges();
                    }
                }
            }
            return View();
        }

        [SessionExpire]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "MyAccount", sideMenuName = "EmailVendor", ViewName = "EmailVendor")]
        public ActionResult EmailVendor()
        {
            return View();
        }
        //[SessionExpire]
        //[HttpPost]
        //[HeaderAndSidebar(isPermissionRequired = true, headerName = "MyAccount", sideMenuName = "EmailVendor", ViewName = "EmailVendor")]
        //public ActionResult EmailVendor(LocalVendorEmail form)
        //{

        //    form.CCAddress = form.CCAddress ?? "";
        //    var clientSuperUser = _orgBusiness.GetSuperuser(form.ClientId);
        //    if (!string.IsNullOrEmpty(form.CCAddress)) form.CCAddress = form.CCAddress + ",";
        //    form.CCAddress += "info@firstverify.com," + clientSuperUser.Email;

        //    var name = _orgBusiness.GetOrganization(form.ClientId).Name;
        //    var status = SendMail.sendMail(string.Join(",",form.ToAddress.Split(',').Where(r=>r.IsEmail())), form.Body, "Important message from " + name, form.BCCAddress, form.CCAddress);
        //    form.EmailStatus = status.Equals("Success");
        //    if (!form.EmailStatus)
        //    {
        //        _vendorBusiness.AddVendorLog(form);
        //        return Json(false);
        //    }
        //    form.UserId = SSession.UserId;
        //    return Json(_vendorBusiness.AddVendorLog(form));
        //}

        [SessionExpire]
        [HttpPost]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "MyAccount", sideMenuName = "EmailVendor", ViewName = "EmailVendor")]
        public ActionResult EmailVendor(LocalVendorEmail form)
        {

            form.CCAddress = form.CCAddress ?? "";
            var clientSuperUser = _orgBusiness.GetSuperuser(form.ClientId);
            if (!string.IsNullOrEmpty(form.CCAddress)) form.CCAddress = form.CCAddress + ",";
            form.CCAddress += "info@firstverify.com," + clientSuperUser.Email;
            var name = _orgBusiness.GetOrganization(form.ClientId).Name;
            var status = SendMail.sendMail(string.Join(",", form.ToAddress.Split(',').Where(r => r.IsEmail())), form.Body, "Important message from " + name, form.BCCAddress, form.CCAddress);
            form.EmailStatus = status.Equals("Success");
            if (!form.EmailStatus)
            {
                _vendorBusiness.AddVendorLog(form);
                return Json(false);
            }
            form.UserId = SSession.UserId;
            return Json(_vendorBusiness.AddVendorLog(form));
        }

        public ActionResult GetVendors(long clientId)
        {
            return Json(_vendorBusiness.GetVendorOrganizationsByClientId(clientId));
        }

        [HttpPost]
        [SessionExpireForView]
        public ActionResult VendorSearchResultsForEmailing(string searchType, long clientId, long vendorId, string prequalStatus, int sortBy)//0-Name 1-Status
        {
            ViewBag.searchType = searchType;
            var data = _vendorBusiness.VendorEmailSearch(searchType, clientId, vendorId, prequalStatus);
            ViewBag.VBPrequalifications = sortBy == 0 ? data.OrderBy(r => r.Name).ToList() : data.OrderBy(r => r.PrequalificationStatusName).ToList();
            //if (searchType == "Name")
            //{
            //    if(Session["RoleName"].ToString().ToLower().Equals("client"))
            //    {
            //        long clientId = Convert.ToInt64(Session["currentOrgId"].ToString());
            //        long vendor = Convert.ToInt64(vendorId);
            //        LatestPrequalification latestPrequalification =
            //            entityDB.LatestPrequalification.FirstOrDefault(row => row.ClientId == clientId && row.VendorId == vendor);
            //        List<LatestPrequalification> prequalifications = new List<LatestPrequalification>();
            //        if(latestPrequalification != null)
            //            prequalifications.Add(latestPrequalification);
            //        ViewBag.VBPrequalifications = prequalifications;
            //        ViewBag.VBcount = prequalifications.Count;
            //    }
            //}
            //else if (searchType == "status")
            //{
            //    @ViewBag.VBAdvanced = 1;
            //    SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["EFDbContext"].ConnectionString);
            //    connection.Open();
            //    var command = connection.CreateCommand();
            //    var VendorSearchSQL = "";
            //    var strMainsql = "select distinct org.Name, ps.PrequalificationStatusName,p.PrequalificationStart," +
            //                      "p.PrequalificationFinish,p.VendorId ,p.PrequalificationId,p.PrequalificationStatusId from Prequalification AS p " +//Suma on 2/12/2013  //Suma on 4/12/2013,c.TemplateID
            //                      "INNER JOIN PrequalificationStatus AS ps " +
            //                      " ON ps.PrequalificationStatusId = p.PrequalificationStatusId " +
            //                      " inner join ClientTemplates c on p.ClientTemplateId = c.ClientTemplateID" +
            //                      " inner join Templates t on t.TemplateID=c.TemplateID " +
            //                      " INNER JOIN  Organizations AS org ON org.OrganizationID = p.VendorId " +
            //                      " INNER JOIN (select Prequalification.VendorId, MAX(PrequalificationStart) as prequalStart from Prequalification where ClientId=@clientId " +
            //                      " group by(Prequalification.VendorId)) latest ON (p.PrequalificationStart = latest.prequalStart and p.VendorId = latest.VendorId)"; // Kiran on 11/27/2014

            //    if (Session["RoleName"].ToString().ToLower().Equals("client"))
            //    {
            //        long clientId = Convert.ToInt64(Session["currentOrgId"].ToString());
            //        List<LocalPrequalificationAdvanced> PrequalificationAdvancedDetails = new List<LocalPrequalificationAdvanced>();
            //        //entityDB.LatestPrequalification.Where(rec => prequalStatus.Contains(rec.PrequalificationStatusId)).ToList();
            //        //List<LatestPrequalification> prequalifications =
            //        //    from prequalification in entityDB.LatestPrequalification
            //        //    where prequalStatus.Contains(prequalification.PrequalificationStatusId)
            //        //    select prequalification
            //        //    ;

            //        var strPrequalStatusSql = " where p.clientid = " + clientId +
            //                               " and p.PrequalificationStatusId in (" + prequalStatus + ")";

            //        VendorSearchSQL = strMainsql + strPrequalStatusSql;
            //        command.Parameters.AddWithValue("@clientId", clientId.ToStringNullSafe());
            //        command.CommandText = VendorSearchSQL;

            //        var vendorSearchResult = command.ExecuteReader();
            //        if (vendorSearchResult == null)
            //        {
            //            ViewBag.VBcount = 0;
            //        }
            //        else
            //        {
            //            while (vendorSearchResult.Read())
            //            {
            //                LocalPrequalificationAdvanced PrequalificationDetails = new LocalPrequalificationAdvanced();
            //                PrequalificationDetails.Name = vendorSearchResult[0].ToString();
            //                PrequalificationDetails.PrequalificationStatusName = vendorSearchResult[1].ToString();
            //                PrequalificationDetails.PrequalificationStart = (DateTime)vendorSearchResult[2];
            //                PrequalificationDetails.PrequalificationFinish = (DateTime)vendorSearchResult[3];
            //                PrequalificationDetails.VendorId = (long)vendorSearchResult[4];
            //                PrequalificationDetails.PrequalificationStatusId = (long)vendorSearchResult[6];
            //                PrequalificationAdvancedDetails.Add(PrequalificationDetails);
            //            }
            //        }
            //        ViewBag.VBPrequalifications = PrequalificationAdvancedDetails;
            //    }
            //}
            return PartialView();
        }
        //Ends<<<
        //Ends<<<
        // Suma Partial View Ends <<<

        // SUMA On (MM/DD/YYYY)6-/19/2013 for StartNew VendoPrequalificationr >>>       
        [SessionExpire(lastPageUrl = "~/SystemUsers/DashBoard")]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "PREQUALIFICATIONREQUIREMENTS", sideMenuName = "", ViewName = "ListOfPotentialClients")]
        public ActionResult ListOfPotentialClients()
        { 
            if (Session["SelectedVendorId"] == null)
                return Redirect("~/SystemUsers/DashBoard");
            ViewBag.hasListOfPotentialClientSideMenus = true;
            long lngSelectedVendorId = Convert.ToInt64(Session["SelectedVendorId"].ToString());
            //To get all the clientId for the selected Vendor 
            var varClientID = entityDB.Prequalification.Where(m => m.VendorId == lngSelectedVendorId&&m.ClientTemplates.Templates.IsHide!=true).Select(rec => rec.ClientId).Distinct();//Rajesh on 12/9/2014
            var varClientIDlist = varClientID.ToList();
            // Kiran on 4/10/2014
            var subClients = entityDB.SubClients.Select(r => r.ClientId).Distinct();
            var varOrglist = entityDB.Organizations.Where(m => (m.OrganizationType == OrganizationType.Client || m.OrganizationType == OrganizationType.SuperClient) && !subClients.Contains(m.OrganizationID) && m.ShowInApplication == true
            && (m.IsClientInviteOnly == null || m.IsClientInviteOnly == false) && m.ClientTemplates.Any(r => r.Templates.IsHide != true && r.Templates.TemplateType == 0) && !(varClientIDlist.Contains(m.OrganizationID))).OrderBy(m => m.Name);
           
            // Ends<<
            //var ERITemplates = entityDB.Templates.Where(rec => rec.IsERI == true && rec.TemplateStatus == 1 && rec.IsHide != true).Select(r => new LocalOrganizations()
            //{
            //    IsERI = true,
            //    IsLocked = r.LockedByDefault ?? false,
            //    OrganizationID = r.ClientTemplates.FirstOrDefault(r1 => r1.Organizations.OrganizationType == OrganizationType.SuperClient).ClientID,
            //    TemplateID = r.TemplateID,
            //    Name = r.TemplateName
            //}).ToList();

            //ViewBag.VBERITemplates = ERITemplates;
            var result = varOrglist.Select(r => new LocalOrganizations()
            {
                Address1 = r.Address1,
                City = r.City,
                IsERI = r.OrganizationType == OrganizationType.SuperClient,
                Name = r.Name,
                OrganizationID = r.OrganizationID,
                PhoneNumber = r.PhoneNumber,
                State = r.State,
             SubClients= entityDB.SubClients.Where(r1 => r1.SuperClientId == r.OrganizationID).Select(r1 => r1.SubClientOrganization.Name),
                CopyAnswers = r.CopyAnswers,
                Country = r.Country
            }).ToList();
            //result.ForEach(r =>r.SubClients = entityDB.SubClients.Where(r1 => r1.SuperClientId == r.OrganizationID).Select(r1 => r1.SubClientOrganization.Name).ToList());
            //result.AddRange(ERITemplates);
            ViewBag.VBClientdetails = result.OrderBy(r => r.Name).ToList();
            var VendorPdata = entityDB.Prequalification.Where(m => m.VendorId == lngSelectedVendorId && m.PrequalificationStatusId == 1).ToList();

            ViewBag.LogoPath = ConfigurationSettings.AppSettings["LogoUploadPath"].ToString() + "/";//Rajesh on 08/09/2013
            ViewBag.VBVendorPdatadetails = VendorPdata.ToList();
            ViewBag.CopyAnswers = entityDB.Organizations.Find(lngSelectedVendorId).CopyAnswers;
            //Rajesh on 3/24/2014
            List<Organizations> ReRenewal = new List<Organizations>();
            List<Organizations> Expired = new List<Organizations>();//Rajesh on 12/9/2014
            foreach (var ClientID in varClientID.ToList())
            {

                var PreQualificationsList = entityDB.Prequalification.Where(rec => rec.ClientId == ClientID && rec.VendorId == lngSelectedVendorId).OrderByDescending(rec => rec.PrequalificationStart).ToList();
                var FinishDate = PreQualificationsList.FirstOrDefault().PrequalificationFinish;
                if (PreQualificationsList != null && (FinishDate.Date.Ticks >= DateTime.Now.Date.Ticks && FinishDate.AddDays(-30).Date.Ticks <= DateTime.Now.Date.Ticks) && (PreQualificationsList.FirstOrDefault().PrequalificationStatusId == 4 || PreQualificationsList.FirstOrDefault().PrequalificationStatusId == 8 || PreQualificationsList.FirstOrDefault().PrequalificationStatusId == 9 || PreQualificationsList.FirstOrDefault().PrequalificationStatusId == 13 || PreQualificationsList.FirstOrDefault().PrequalificationStatusId == 23 || PreQualificationsList.FirstOrDefault().PrequalificationStatusId == 24 || PreQualificationsList.FirstOrDefault().PrequalificationStatusId == 25 || PreQualificationsList.FirstOrDefault().PrequalificationStatusId == 26))//Rajesh on 3/25/2014 // Kiran on 02/04/2015 for opening the renewal option for different statuses as per the email dated 02/03/2015.
                {
                    ReRenewal.Add(entityDB.Organizations.Find(ClientID));
                }

                //Rajesh on 12/9/2014
                var LatestPreQualification = PreQualificationsList.FirstOrDefault();
                //if (LatestPreQualification.PrequalificationStatusId == 10)
                if (LatestPreQualification.PrequalificationStatusId == 10 || (LatestPreQualification.PrequalificationStatusId == 4 && LatestPreQualification.PrequalificationFinish <= DateTime.Now))
                {
                    Expired.Add(LatestPreQualification.Client);
                }
                //Ends<<<
            }
            ViewBag.ReRenewal = ReRenewal.ToList();//Rajesh on 4/1/2014

            ViewBag.Expired = Expired.ToList();
            //Ends<<<





            return View();
        }
        // Suma Clients List Ends <<<

        // SUMA on (MM/DD/YYYY)6-21-2013 for StartPrequalification >>>
        [SessionExpireForView]
        public string StartPrequalification(long selectedClientID, string ClientCode, int? Renewal, bool copyAns, bool copyDoc, Guid? selectedTemplateId, int isERI = 0, string comments = "")//Rajesh on 3/24/2013 
        {
            long localSelectedClientId = selectedClientID;

            Session["SelectedClientID"] = selectedClientID;
            long lngSelectedVendorId = Convert.ToInt64(Session["SelectedVendorId"].ToString());
            long lngSelectedClientId = selectedClientID;
            var UserId = new Guid(Session["UserId"].ToString());
            string LoginUserEmail = entityDB.SystemUsers.Find(UserId).Email;

            //Rajesh on 4/1/2014
            if (Renewal != null && Renewal == 1 && isERI == 0)//He selects Renewal
            {
                //Prequalification oldPrequalification = entityDB.Prequalification.FirstOrDefault(rec => rec.VendorId == lngSelectedVendorId && rec.ClientId == lngSelectedClientId && rec.PrequalificationStatusId == 9);//To Get Last Prequalification
                Prequalification oldPrequalification = entityDB.Prequalification.OrderByDescending(rec => rec.PrequalificationStart).FirstOrDefault(rec => rec.VendorId == lngSelectedVendorId && rec.ClientId == lngSelectedClientId);//Rajesh on 12/10/2014

                if (oldPrequalification.PrequalificationStatusId == 15)
                    return "../Prequalification/TemplatesList?PreQualificationId=" + oldPrequalification.PrequalificationId;

                Prequalification p = new Prequalification();
                p.ClientId = oldPrequalification.ClientId;
                p.ClientTemplateId = oldPrequalification.ClientTemplateId;
                if (oldPrequalification.ClientTemplates.Templates.LockedByDefault == true && oldPrequalification.ClientTemplates.Templates.RenewalLock == true)
                    p.locked = oldPrequalification.ClientTemplates.Templates.LockedByDefault;
                    var InvitationDetails = _prequalificationBusiness.GetPQInviteDetails(oldPrequalification.PrequalificationId);


                if (InvitationDetails.InvitationFrom == 0 && !string.IsNullOrEmpty(InvitationDetails.InvitationSentByUser))
                    p.InvitationFrom = 2;
                else p.InvitationFrom = InvitationDetails.InvitationFrom;
                p.InvitationSentByUser = InvitationDetails.InvitationSentByUserId;

               
                p.PrequalificationCreate = DateTime.Now;
                //Rajesh on 4/5/2014
                DateTime StartDateTime = new DateTime();
                var preStatus = entityDB.PrequalificationStatus.FirstOrDefault(rec => rec.PrequalificationStatusId == 15);
                if (preStatus != null)
                {
                    if (preStatus.PeriodLengthUnit.ToLower().Equals("d"))
                        StartDateTime = oldPrequalification.PrequalificationFinish.AddDays(-preStatus.PeriodLength);
                    else if (preStatus.PeriodLengthUnit.ToLower().Equals("m"))
                        StartDateTime = oldPrequalification.PrequalificationFinish.AddMonths(-preStatus.PeriodLength);
                    else if (preStatus.PeriodLengthUnit.ToLower().Equals("y"))
                        StartDateTime = oldPrequalification.PrequalificationFinish.AddYears(-preStatus.PeriodLength);
                }
                p.PrequalificationFinish = oldPrequalification.PrequalificationFinish;
                p.PrequalificationStart = StartDateTime; p.PrequalificationStatusId = 15;

                //Ends<<<
                p.PrequalificationUserInputs = new List<PrequalificationUserInput>();
                //p.PrequalificationUserInputs.All(CurrentPreQualificationData.PrequalificationUserInputs.ToList());
                //= CurrentPreQualificationData.PrequalificationUserInputs;
                p.UserIdAsCompleter = oldPrequalification.UserIdAsCompleter;
                //p.UserIdAsSigner = CurrentPreQualificationData.UserIdAsSigner;
                p.VendorId = oldPrequalification.VendorId;
                entityDB.Prequalification.Add(p);
                logs.Info("Pq Added Successfully " + p.PrequalificationId, "SelectedClientId=" + selectedClientID + "&clientCode=" + ClientCode + "&Renewal=" + Renewal + "&copyAns=" + copyAns + "&copyDoc=" + copyDoc + "&selectedTemplateId=" + selectedTemplateId + "isERI=" + isERI + "comments = " + comments, "Vendorcontroller /StartPrequalification");

                //entityDB.SaveChanges();

                if (copyAns == true)
                {

                    //To Get oldprequalification EMRYears
                    var EMRYearsIDs = oldPrequalification.PrequalificationEMRStatsYears.Select(rec => rec.PrequalEMRStatsYearId).ToList();


                    var ColumnIds = entityDB.PrequalificationEMRStatsValues.Where(rec => EMRYearsIDs.Contains(rec.PrequalEMRStatsYearId)).Select(rec => rec.QuestionColumnId).ToList();

                    foreach (var UserInput in oldPrequalification.PrequalificationUserInputs.Where(rec => !ColumnIds.Contains(rec.QuestionColumnId)).ToList())
                    {
                        var templateSection = UserInput.QuestionColumnDetails.Questions.TemplateSubSections.TemplateSections;
                        if (templateSection.TemplateSectionType == 34 || templateSection.TemplateSectionsPermission.Any(r => r.VisibleTo == 2) ||
                        templateSection.SectionName.EndsWith("approval", StringComparison.CurrentCultureIgnoreCase))//TO stop client approval & Exception & probition
                            continue;
                        PrequalificationUserInput inputVal = new PrequalificationUserInput();
                        inputVal.PreQualificationId = p.PrequalificationId;
                        inputVal.QuestionColumnId = UserInput.QuestionColumnId;
                        inputVal.UserInput = UserInput.UserInput;
                        entityDB.PrequalificationUserInput.Add(inputVal);
                    }

                    foreach (var reportingData in oldPrequalification.PrequalificationReportingData.ToList())
                    {
                        var ReportingData_Loc = new PrequalificationReportingData();
                        ReportingData_Loc.PrequalificationId = p.PrequalificationId;
                        ReportingData_Loc.ClientId = p.ClientId;
                        ReportingData_Loc.VendorId = p.VendorId;
                        ReportingData_Loc.ReportingType = reportingData.ReportingType;
                        ReportingData_Loc.ReportingDataId = reportingData.ReportingDataId;
                        entityDB.PrequalificationReportingData.Add(ReportingData_Loc);
                    }
                    foreach (var sites in oldPrequalification.PrequalificationSites.ToList())
                    {
                        PrequalificationSites site = new PrequalificationSites();
                        site.PrequalificationId = p.PrequalificationId;
                        site.VendorId = sites.VendorId;
                        site.ClientBULocation = sites.ClientBULocation;
                        site.ClientBusinessUnitId = sites.ClientBusinessUnitId;
                        site.ClientBusinessUnitSiteId = sites.ClientBusinessUnitSiteId;
                        site.ClientId = sites.ClientId;
                        site.Comment = sites.Comment;
                        entityDB.PrequalificationSites.Add(site);
                    }
                    //Adding Documents



                }
                if (copyDoc == true)
                {
                    entityDB.SaveChanges(); // Rajesh on 12/10/2014
                    foreach (var sourceDoc in entityDB.Document.Where(rec => rec.ReferenceType == "PreQualification" && rec.ReferenceRecordID == oldPrequalification.PrequalificationId).ToList())
                    {
                        if (sourceDoc.DocumentStatus != "Deleted" || sourceDoc.DocumentStatus == null) // Kiran on 02/05/2015
                        {
                            try
                            {
                                //Guid id = Guid.NewGuid();
                                //var docId = new Guid(doc.Replace("Doc_", ""));
                                //var sourceDoc = entityDB.Document.Find(docId);
                                var destinationDoc = new Document();
                                destinationDoc.DocumentTypeId = sourceDoc.DocumentTypeId;
                                destinationDoc.OrganizationId = sourceDoc.OrganizationId;
                                destinationDoc.SystemUserAsUploader = sourceDoc.SystemUserAsUploader;
                                destinationDoc.ReferenceType = sourceDoc.ReferenceType;
                                destinationDoc.ReferenceRecordID = p.PrequalificationId;//This Only Changed
                                destinationDoc.DocumentName = sourceDoc.DocumentName;
                                destinationDoc.Uploaded = sourceDoc.Uploaded;
                                destinationDoc.Expires = sourceDoc.Expires;
                                destinationDoc.SectionId = sourceDoc.SectionId;
                                //destinationDoc.DocumentId = id;
                                entityDB.Document.Add(destinationDoc);
                                entityDB.SaveChanges();

                                var dotIndex = sourceDoc.DocumentName.LastIndexOf('.');
                                var extension = sourceDoc.DocumentName.Substring(dotIndex);
                                var path = Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["ClientUploadsPath"]), sourceDoc.DocumentId + extension);
                                var dest = Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["ClientUploadsPath"]), destinationDoc.DocumentId + extension);

                                System.IO.File.Copy(path, dest, true);
                            }
                            catch { }
                        }
                    }
                }
                entityDB.SaveChanges();

                _prequalificationBusiness.ZeroPayments(p.PrequalificationId);
                //To add Prequalification Logs

                PrequalificationStatusChangeLog preQualificationLog = new PrequalificationStatusChangeLog();
                preQualificationLog.PrequalificationId = p.PrequalificationId;
                preQualificationLog.PrequalificationStatusId = 15;
                preQualificationLog.StatusChangeDate = DateTime.Now;
                preQualificationLog.StatusChangedByUser = new Guid(Session["UserId"].ToString());
                preQualificationLog.PrequalificationStart = p.PrequalificationStart;
                preQualificationLog.PrequalificationFinish = p.PrequalificationFinish;
                preQualificationLog.CreatedDate = DateTime.Now;
                entityDB.PrequalificationStatusChangeLog.Add(preQualificationLog);
                entityDB.SaveChanges();
                //>>>Ends

                var QuestionWith1000 = new Questions();
                //Update EMRYears using questionId 1000
                foreach (var TemplateSection in oldPrequalification.ClientTemplates.Templates.TemplateSections.ToList())
                {
                    var YearWiseEMR = TemplateSection.TemplateSubSections.FirstOrDefault(rec => rec.SubSectionTypeCondition == "YearWiseStats");
                    if (YearWiseEMR != null)
                    {
                        var Question = YearWiseEMR.Questions.FirstOrDefault(rec => rec.QuestionBankId == 1000);
                        QuestionWith1000 = Question;
                        var CurrentYear = DateTime.Now.Year;
                        if (Question != null && Question.QuestionColumnDetails != null)//Rajesh on 4/5/2014
                            foreach (var QColumnDetails in Question.QuestionColumnDetails.OrderBy(rec => rec.ColumnNo).ToList())
                            {
                                PrequalificationUserInput PUserInput = entityDB.PrequalificationUserInput.FirstOrDefault(rec => rec.PreQualificationId == p.PrequalificationId && rec.QuestionColumnId == QColumnDetails.QuestionColumnId);
                                if (PUserInput != null)
                                {
                                    PUserInput.UserInput = --CurrentYear + "";
                                    entityDB.Entry(PUserInput).State = EntityState.Modified;
                                }
                                else
                                {
                                    PUserInput = new PrequalificationUserInput();
                                    PUserInput.PreQualificationId = p.PrequalificationId;
                                    PUserInput.QuestionColumnId = QColumnDetails.QuestionColumnId;
                                    PUserInput.UserInput = --CurrentYear + "";
                                    entityDB.PrequalificationUserInput.Add(PUserInput);
                                }


                            }
                    }
                }

                entityDB.SaveChanges();
                if (QuestionWith1000 != null && QuestionWith1000.QuestionColumnDetails != null)//Rajesh on 4/5/2014
                    foreach (var QcolumnDetails in QuestionWith1000.QuestionColumnDetails.ToList())
                    {
                        var QCValue = entityDB.PrequalificationUserInput.FirstOrDefault(rec => rec.PreQualificationId == p.PrequalificationId && rec.QuestionColumnId == QcolumnDetails.QuestionColumnId).UserInput;
                        if (QCValue != null)
                        {
                            var EMRYear = entityDB.PrequalificationEMRStatsYears.FirstOrDefault(rec => rec.PrequalificationId == oldPrequalification.PrequalificationId && rec.EMRStatsYear == QCValue);
                            if (EMRYear != null)
                            {

                                foreach (var EMRValues in EMRYear.PrequalificationEMRStatsValues.ToList())
                                {
                                    var QuestionColumnDetails = EMRValues.Questions.QuestionColumnDetails.FirstOrDefault(rec => rec.ColumnNo == QcolumnDetails.ColumnNo);
                                    PrequalificationUserInput PUserInput = entityDB.PrequalificationUserInput.FirstOrDefault(rec => rec.PreQualificationId == p.PrequalificationId && rec.QuestionColumnId == QuestionColumnDetails.QuestionColumnId);
                                    if (PUserInput != null)
                                    {
                                        PUserInput.UserInput = EMRValues.QuestionColumnIdValue + "";
                                        entityDB.Entry(PUserInput).State = EntityState.Modified;
                                    }
                                    else
                                    {
                                        PUserInput = new PrequalificationUserInput();
                                        PUserInput.PreQualificationId = p.PrequalificationId;
                                        PUserInput.QuestionColumnId = QuestionColumnDetails.QuestionColumnId;
                                        PUserInput.UserInput = EMRValues.QuestionColumnIdValue + "";
                                        entityDB.PrequalificationUserInput.Add(PUserInput);
                                    }
                                }
                            }
                        }
                    }
                entityDB.SaveChanges();
                var copyanalytics = _prequalificationBusiness.CopyAnalytics(oldPrequalification.PrequalificationId, entityDB);
                if (copyanalytics != null)
                {

                    foreach (var items in copyanalytics)
                    {
                        entityDB.FinancialAnalyticsLog.Add(items);

                    }
                    entityDB.SaveChanges();

                }
                //if(ClientCode.Length==12)
                //{
                //    var prequalification = entityDB.Prequalification.Find(p.PrequalificationId);
                //    var TemplateId = entityDB.MultipleInvitationCode.FirstOrDefault(r => r.ClientCode == ClientCode).TemplateId;
                //    ClientTemplates ct = new ClientTemplates();
                //    ct.ClientID = prequalification.ClientId;
                //    ct.TemplateID = TemplateId;
                //    ct.Visible = true;
                //    entityDB.ClientTemplates.Add(ct);
                //    entityDB.SaveChanges();

                //    prequalification.ClientTemplateId =ct.ClientTemplateID;
                //    entityDB.Entry(prequalification).State = EntityState.Modified;
                //    entityDB.SaveChanges();
                //    return "../Prequalification/TemplateSectionsList?templateId=" + TemplateId + "&TemplateSectionID=-1&PreQualificationId=" +p.PrequalificationId;
                //}

                return "../Prequalification/TemplatesList?PreQualificationId=" + p.PrequalificationId;
            }
            //Ends<<<
            if (!string.IsNullOrEmpty(ClientCode) && ClientCode.Length == 10)//Enters Invitation code.
            {
                var PqId = 0l;
                var res = _prequalificationBusiness.HandleInvitation(lngSelectedVendorId, ClientCode, SSession.UserId, comments, out PqId);
                if (!string.IsNullOrEmpty(res))
                {
                    if (PqId != 0)
                    {
                        var invitationRes = SendUnlockRequestNotification(PqId);
                        if (invitationRes != null)//Means template is locked
                        {
                            return "Template Locked";
                        }

                    }
                    return res;
                }
            }

            //Rajesh on 9/7/2013
            
            if (lngSelectedClientId == -1 && isERI == 0)//Means users enters client code
            {
              
               
                //var InviteDetails = entityDB.VendorInviteDetails.FirstOrDefault(rec => rec.VendorEmail == LoginUserEmail && rec.ClientCode == ClientCode);
                //if (InviteDetails == null)
                //    return "Invalid Client Code";
                //else
                //    lngSelectedClientId = InviteDetails.ClientId;

                //Rajesh on 7/22/2014 
                var orgDetails = new Organizations();
                if (ClientCode.Length == 12)
                {
                    var ValidCode = entityDB.MultipleInvitationCode.FirstOrDefault(r => r.ClientCode == ClientCode);
                    if (ValidCode == null) return "Invalid Client Code";
                    else orgDetails = entityDB.Organizations.FirstOrDefault(r => r.OrganizationID == ValidCode.ClientId);
                }
                else orgDetails = entityDB.Organizations.FirstOrDefault(rec => rec.SingleClientCode == ClientCode);
               
              
                if (orgDetails == null)
                    return "Invalid Client Code";
                else if (_clientBusiness.CheckClientIsInERIClients(orgDetails.OrganizationID))
                {
                    return "ERI Client - " + orgDetails.Name;
                }
                else
                    lngSelectedClientId = orgDetails.OrganizationID;


                var InviteDetails = entityDB.VendorInviteDetails.FirstOrDefault(rec => rec.VendorEmail == LoginUserEmail && rec.ClientCode == ClientCode);
                if (InviteDetails != null)
                {
                    InviteDetails.VendorValidated = true;
                    entityDB.Entry(InviteDetails).State = EntityState.Modified;
                    entityDB.SaveChanges();
                }
                //Ends<<<
            }
            //Ends <<<


            Prequalification vendorPrequalification_ToInsert = entityDB.Prequalification.Where(rec => rec.VendorId == lngSelectedVendorId && rec.ClientId == lngSelectedClientId).OrderByDescending(rec => rec.PrequalificationFinish).FirstOrDefault();//Rajesh on 3/25/2014
            //Rajesh on 3/25/2014
            if (localSelectedClientId == -1 && vendorPrequalification_ToInsert != null)
            {
                int[] allowedSections = { 1, 2 };
                int[] InprocessPrequalifications = { 9, 13, 14, 3, 4, 5, 7, 8, 15 };

                if (Array.IndexOf(allowedSections, vendorPrequalification_ToInsert.PrequalificationStatusId) != -1)
                {
                    //Do Nothing
                }
                //else if (vendorPrequalification_ToInsert.PrequalificationStatusId == 3)
                //    return "Already started";
                //else if (vendorPrequalification_ToInsert.PrequalificationStatusId == 4)
                //    return "This prequalification has marked as no response";
                //else if (vendorPrequalification_ToInsert.PrequalificationStatusId == 5)
                //    return "This prequalification has in review.";
                //else if (vendorPrequalification_ToInsert.PrequalificationStatusId == 7)
                //    return "Prequalification exist with incomplete sections";
                //else if (vendorPrequalification_ToInsert.PrequalificationStatusId == 8)
                //    return "you do not qualified for this prequalification";
                else if (Array.IndexOf(InprocessPrequalifications, vendorPrequalification_ToInsert.PrequalificationStatusId) != -1)
                    return "This questionnaire has already been selected";
                else if (vendorPrequalification_ToInsert.PrequalificationStatusId == 10)
                    return "../Prequalification/TemplatesList?PreQualificationId=-1&oldPreQualificationId=" + vendorPrequalification_ToInsert.PrequalificationId;
                //else if (vendorPrequalification_ToInsert.PrequalificationStatusId == 15)
                //    return "Prequalification alredy in process";

            }
            //Ends<<<


            if (vendorPrequalification_ToInsert == null)
            {
                vendorPrequalification_ToInsert = new Prequalification();
                vendorPrequalification_ToInsert.VendorId = lngSelectedVendorId;
                vendorPrequalification_ToInsert.ClientId = lngSelectedClientId;
                if (isERI == 1) vendorPrequalification_ToInsert.BlockClientDocsSelection = false;
                vendorPrequalification_ToInsert.PrequalificationCreate = DateTime.Now;
                //vendorPrequalification_ToInsert.PrequalificationStart = DateTime.Now;
                //vendorPrequalification_ToInsert.PrequalificationFinish = DateTime.Now;
                //vendorPrequalification_ToInsert.PrequalificationSubmit = DateTime.Now;
                var InvitationDetails = _prequalificationBusiness.AddInvitaionDetailsIntoPq(lngSelectedClientId, lngSelectedVendorId);
              
                vendorPrequalification_ToInsert.InvitationFrom = InvitationDetails.InvitationFrom;
                vendorPrequalification_ToInsert.InvitationSentByUser = InvitationDetails.InvitationSentByUserId;

                if (!string.IsNullOrEmpty(ClientCode) && ClientCode.Length == 8 &&SSession.Role==OrganizationType.Vendor)
                {
                    var InviteDetails = entityDB.VendorInviteDetails.FirstOrDefault(rec => rec.VendorEmail == LoginUserEmail && rec.ClientCode == ClientCode);
                    if (InviteDetails != null)
                    {
                        vendorPrequalification_ToInsert.InvitationId = InviteDetails.InvitationId;
                        if (InviteDetails.RiskLevel != null)
                        {
                           
                               var clienttemplate = entityDB.ClientTemplates.FirstOrDefault(r => r.ClientID == InviteDetails.ClientId && r.Templates.RiskLevel == InviteDetails.RiskLevel);
                                if (clienttemplate != null)
                                 {

                            vendorPrequalification_ToInsert.ClientTemplateId = clienttemplate.ClientTemplateID;
                            vendorPrequalification_ToInsert.locked = clienttemplate.Templates.LockedByDefault;
                            vendorPrequalification_ToInsert.Comments = comments;
                                }
                        }
                        foreach (var location in InviteDetails.VendorInviteLocations)
                        {
                            var pqSites = new PrequalificationSites();
                            pqSites.ClientId = InviteDetails.ClientId;
                            pqSites.VendorId = lngSelectedVendorId;
                            pqSites.PrequalificationId = vendorPrequalification_ToInsert.PrequalificationId;
                            pqSites.ClientBULocation = location.ClientBusinessUnitSites.BusinessUnitLocation;
                            pqSites.ClientBusinessUnitId = location.ClientBusinessUnitSites.ClientBusinessUnitId;
                            pqSites.ClientBusinessUnitSiteId = location.ClientBusinessUnitSiteID;
                            entityDB.PrequalificationSites.Add(pqSites);
                        }
                    }
                }
                if (!string.IsNullOrEmpty(ClientCode) && ClientCode.Length == 12)
                {
                    var HasInvitaion = entityDB.MultipleInvitationCode.FirstOrDefault(r => r.ClientCode == ClientCode);
                    vendorPrequalification_ToInsert.ClientTemplateId = entityDB.ClientTemplates.FirstOrDefault(r => r.ClientID == lngSelectedClientId && r.TemplateID == HasInvitaion.TemplateId).ClientTemplateID;
                    vendorPrequalification_ToInsert.InvitationFrom = 1;
                }
                    vendorPrequalification_ToInsert.PrequalificationStatusId = 2;

                //Rajesh DT:-8/7/2013>>>>
                vendorPrequalification_ToInsert.PrequalificationStart = DateTime.Now;
                DateTime FinishDateTime = DateTime.Now;
                var preStatus = entityDB.PrequalificationStatus.FirstOrDefault(rec => rec.PrequalificationStatusId == 2);
                if (preStatus != null)
                {
                    if (preStatus.PeriodLengthUnit.ToLower().Equals("d"))
                        FinishDateTime = DateTime.Now.AddDays(preStatus.PeriodLength);
                    else if (preStatus.PeriodLengthUnit.ToLower().Equals("m"))
                        FinishDateTime = DateTime.Now.AddMonths(preStatus.PeriodLength);
                    else if (preStatus.PeriodLengthUnit.ToLower().Equals("y"))
                        FinishDateTime = DateTime.Now.AddYears(preStatus.PeriodLength);
                }
                vendorPrequalification_ToInsert.PrequalificationFinish = FinishDateTime;
                // Ends <<<<<

                vendorPrequalification_ToInsert.UserIdAsCompleter = SSession.UserId;
                entityDB.Prequalification.Add(vendorPrequalification_ToInsert);
                logs.Info("Pq Added Successfully " + vendorPrequalification_ToInsert.PrequalificationId, "SelectedClientId=" + selectedClientID + "&clientCode=" + ClientCode+"&Renewal="+ Renewal + "&copyAns=" +copyAns + "&copyDoc=" + copyDoc+ "&selectedTemplateId=" + selectedTemplateId +"isERI="+ isERI+"comments = "+comments, "Vendorcontroller /StartPrequalification");

                //To add Prequalification Logs
                PrequalificationStatusChangeLog preQualificationLog = new PrequalificationStatusChangeLog();
                preQualificationLog.PrequalificationId = vendorPrequalification_ToInsert.PrequalificationId;
                preQualificationLog.PrequalificationStatusId = vendorPrequalification_ToInsert.PrequalificationStatusId;
                preQualificationLog.StatusChangeDate = DateTime.Now;
                preQualificationLog.StatusChangedByUser = SSession.UserId;
                preQualificationLog.PrequalificationStart = vendorPrequalification_ToInsert.PrequalificationStart;
                preQualificationLog.PrequalificationFinish = vendorPrequalification_ToInsert.PrequalificationFinish;
                preQualificationLog.CreatedDate = DateTime.Now;
                entityDB.PrequalificationStatusChangeLog.Add(preQualificationLog);

                //>>>Ends
            }
            else if (vendorPrequalification_ToInsert.PrequalificationStatusId == 2)//Rajesh on 09/07/2013
            {
                vendorPrequalification_ToInsert.PrequalificationStatusId = 2;
                entityDB.Entry(vendorPrequalification_ToInsert).State = EntityState.Modified;
            }
            entityDB.SaveChanges();

            //var varTemplates = entityDB.ClientTemplates.Where(m => m.ClientID == selectedClientID).ToList();
            //if (varTemplates == null)
            //{
            //    ModelState.AddModelError("", "No Template Id");
            //}
            //else
            //{
            //    ViewBag.VBTemplates = varTemplates.ToList();
            //}
            //if (isERI == 1)
            //{

            //    return "../Prequalification/TemplatesList?PreQualificationId=" + vendorPrequalification_ToInsert.PrequalificationId + "&templateId=" + selectedTemplateId;
            //}
            //else
            if (!string.IsNullOrEmpty(ClientCode) && ClientCode.Length == 12)
            {
                var HasInvitaion = entityDB.MultipleInvitationCode.FirstOrDefault(r => r.ClientCode == ClientCode);
                return "../Prequalification/TemplatesList?PreQualificationId=" + vendorPrequalification_ToInsert.PrequalificationId+ "&templateId=" + HasInvitaion.TemplateId;
                
            }
           
            if (!string.IsNullOrEmpty(ClientCode) && ClientCode.Length == 8 && SSession.Role == OrganizationType.Vendor)
            {
                var InviteDetails = entityDB.VendorInviteDetails.FirstOrDefault(rec => rec.VendorEmail == LoginUserEmail && rec.ClientCode == ClientCode);
                if(InviteDetails != null && InviteDetails.RiskLevel != null)
                return "../Prequalification/TemplatesList?PreQualificationId=" + vendorPrequalification_ToInsert.PrequalificationId + "&templateId=" + vendorPrequalification_ToInsert.ClientTemplates.TemplateID;
            }
            return "../Prequalification/TemplatesList?PreQualificationId=" + vendorPrequalification_ToInsert.PrequalificationId;
        }
        private bool? SendUnlockRequestNotification(long PreQualificationId)
        {
            //To Send Notifications for particular sections
            //Rajesh on 2/17/2014

            using (var entityDB = new EFDbContext())
            {
                Prequalification currentPreQualification = entityDB.Prequalification.Find(PreQualificationId);
                if (currentPreQualification.locked == null || currentPreQualification.locked==false)
                {
                    return null;
                }
                string adminEmails = currentPreQualification.ClientTemplates.Templates.unlockNotificationAdminEmails.ToStringNullSafe();

                if (!String.IsNullOrWhiteSpace(adminEmails))
                {
                    var template = entityDB.EmailTemplates.Find(currentPreQualification.ClientTemplates.Templates.EmaiTemplateId);
                    if (template == null) return false;


                    var subject = template.EmailSubject;
                    var body = template.EmailBody;
                    var Comment = template.CommentText;
                    var clientEmail = "";
                    var strMessage = "";
                    var emailsToSent = ""; // Kiran on 9/23/2014
                    var clientSysUserOrgs = currentPreQualification.Client.SystemUsersOrganizations;
                    foreach (var clientUserOrg in clientSysUserOrgs)
                    {
                        var clientOrgRoles = clientUserOrg.SystemUsersOrganizationsRoles;
                        foreach (var clientOrgRole in clientOrgRoles)
                        {
                            if (clientOrgRole.SystemRoles.RoleName.Equals("Super User"))
                            {
                                clientEmail = clientUserOrg.SystemUsers.Email;
                            }
                        }
                    }
                    subject = subject.Replace("[Date]", DateTime.Now + "");
                    subject = subject.Replace("[VendorName]", currentPreQualification.Vendor.Name);
                    subject = subject.Replace("[ClientName]", currentPreQualification.Client.Name);
                    subject = subject.Replace("[CustomerServicePhone]", currentPreQualification.Client.PhoneNumber);
                    subject = subject.Replace("[CustomerServiceEmail]", clientEmail);

                    body = body.Replace("[Date]", DateTime.Now + "");
                    body = body.Replace("[VendorName]", currentPreQualification.Vendor.Name);
                    body = body.Replace("[ClientName]", currentPreQualification.Client.Name);
                    body = body.Replace("[CustomerServicePhone]", currentPreQualification.Client.PhoneNumber);
                    body = body.Replace("[CustomerServiceEmail]", clientEmail);

                    strMessage = SendMail.sendMail(adminEmails, body, subject);


                }
            }
            return true;
            //Ends<<<
        }
        [SessionExpire]
        public ActionResult Prequalification(Guid SelectedClientTemplateId)
        {
            long lngSelectedVendorId = (long)Session["SelectedVendorId"];
            long lngSelectedClientId = (long)Session["SelectedClientID"];
            Guid guidclientTemplateId = SelectedClientTemplateId;

            Prequalification vendorPrequalification_ToInsert = new Prequalification();
            vendorPrequalification_ToInsert.VendorId = lngSelectedVendorId;
            vendorPrequalification_ToInsert.ClientId = lngSelectedClientId;
            vendorPrequalification_ToInsert.ClientTemplateId = SelectedClientTemplateId;
            vendorPrequalification_ToInsert.locked = entityDB.ClientTemplates.FirstOrDefault(rec => rec.ClientTemplateID == SelectedClientTemplateId).Templates.LockedByDefault;
            vendorPrequalification_ToInsert.PrequalificationCreate = DateTime.Now;
            vendorPrequalification_ToInsert.PrequalificationStart = DateTime.Now;
            vendorPrequalification_ToInsert.PrequalificationFinish = DateTime.Now;
            vendorPrequalification_ToInsert.PrequalificationSubmit = DateTime.Now;
            vendorPrequalification_ToInsert.PrequalificationStatusId = 2;
            var InvitationDetails = _prequalificationBusiness.AddInvitaionDetailsIntoPq(lngSelectedClientId, lngSelectedVendorId);
            vendorPrequalification_ToInsert.InvitationFrom = InvitationDetails.InvitationFrom;
            vendorPrequalification_ToInsert.InvitationSentByUser = InvitationDetails.InvitationSentByUserId;
            vendorPrequalification_ToInsert.InvitationId = InvitationDetails.InvitationId;
            entityDB.Prequalification.Add(vendorPrequalification_ToInsert);
            logs.Info("Pq Added Successfully " + vendorPrequalification_ToInsert.PrequalificationId, "SelectedClientTemplateId=" + SelectedClientTemplateId, "Vendorcontroller /Prequalification");
            //To add Prequalification Logs
            PrequalificationStatusChangeLog preQualificationLog = new PrequalificationStatusChangeLog();
            preQualificationLog.PrequalificationId = vendorPrequalification_ToInsert.PrequalificationId;
            preQualificationLog.PrequalificationStatusId = vendorPrequalification_ToInsert.PrequalificationStatusId;
            preQualificationLog.StatusChangeDate = DateTime.Now;
            preQualificationLog.StatusChangedByUser = new Guid(Session["UserId"].ToString());
            preQualificationLog.PrequalificationStart = vendorPrequalification_ToInsert.PrequalificationStart;
            preQualificationLog.PrequalificationFinish = vendorPrequalification_ToInsert.PrequalificationFinish;
            preQualificationLog.CreatedDate = DateTime.Now;
            entityDB.PrequalificationStatusChangeLog.Add(preQualificationLog);
            //>>>Ends


            entityDB.SaveChanges();


            return View();

        }
        public SelectListItem[] GetStateSelectListItems(long clientID, bool includeCanada = false)
        {
            var helper = new FVGen3.BusinessLogic.DropdDownDataSupplier();
            var states = helper.GetUnitedStatesTableData((int)clientID, false, includeCanada);
            List<SelectListItem> selectLists = new List<SelectListItem>();
            //'<option>' + datasite + '</option>'
            selectLists.Add(new SelectListItem() { Text = "Select", Value = "" });
            states.ForEach(s => selectLists.Add(new SelectListItem() { Text = s.Key, Value = s.Value }));
            return selectLists.ToArray();
        }
        public ActionResult GetStateSelectListItemsJson(long clientID, string country, bool includeCanada = false)
        {
            var helper = new FVGen3.BusinessLogic.DropdDownDataSupplier();
            var states = helper.GetUnitedStatesTableData((int)clientID, false, includeCanada, country ?? "");
            List<SelectListItem> selectLists = new List<SelectListItem>();
            //'<option>' + datasite + '</option>'
            selectLists.Add(new SelectListItem() { Text = "Select", Value = "" });
            states.ForEach(s => selectLists.Add(new SelectListItem() { Text = s.Key, Value = s.Value }));
            return Json(selectLists.ToArray(), JsonRequestBehavior.AllowGet);
        }
        // SUMA - Client Templates Ends <<<
        [SessionExpire]
        public ActionResult GetVendorSummary(long vendorId)
        {
            var clientId = 0L;
            if (SSession.Role == OrganizationType.Client)
                clientId = SSession.OrganizationId;
            return Json(_vendorBusiness.GetVendorSummary(vendorId, SSession.UserId, clientId));
        }

        public ActionResult GetVendorsAutoComplete(string prefix)
        {
            return Json(_vendorBusiness.GetVendors(prefix, 10), JsonRequestBehavior.AllowGet);
        }

    }
}

