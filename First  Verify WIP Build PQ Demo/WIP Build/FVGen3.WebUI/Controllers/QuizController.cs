﻿/*
Page Created Date:  08/25/2013
Created By: Rajesh Pendela
Purpose: Employees Quiz
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FVGen3.Domain.Abstract;
using FVGen3.Domain.Concrete;
using FVGen3.Domain.Entities;
using System.Data.SqlClient;
using FVGen3.WebUI.Annotations;
using System.Data.Entity;
using System.Configuration;
using System.IO;
using System.Text.RegularExpressions;
using System.Web.Script.Serialization;
using FVGen3.BusinessLogic;
using FVGen3.BusinessLogic.Interfaces;

namespace FVGen3.WebUI.Controllers
{
    public class QuizController : BaseController
    {
        //
        // GET: /Quiz/
        EFDbContext entityDB;

        private IOrganizationBusiness _organizationBusiness;
        private IQuizBusiness _quizBusiness;
        private ISystemUserBusiness _sysUserBusiness;
        
        public QuizController(ISystemUsersRepository repositorySystemUsers, IOrganizationBusiness orgBusiness, IQuizBusiness quizBusiness, ISystemUserBusiness sysUserBusiness)
        {
            entityDB = new EFDbContext();
            _organizationBusiness = orgBusiness;
            _quizBusiness = quizBusiness;
            _sysUserBusiness = sysUserBusiness;
        }
        public ActionResult Index()
        {
            return View();
        }

        // Kiran on 3/10/2014 for DB Changes
        [SessionExpire]
        [HeaderAndSidebar(headerName = "PREQUALIFICATIONREQUIREMENTS", sideMenuName = "")]
        public ActionResult StartQuiz(Guid clientTemplateId, long userQuizId)
        {
            ClientTemplates clientTemplate = entityDB.ClientTemplates.Find(clientTemplateId);

            var vendorId = (long)Session["currentOrgId"];
            var helper=new QuizHelper();
            //helper.GetTrainingPayment(userQuizId, vendorId);
            // kiran on 12/18/2014
            //EmployeeQuizzes quiz = entityDB.EmployeeQuizzes.FirstOrDefault(rec => rec.VendorId == vendorId && rec.ClientId == clientTemplate.ClientID && rec.SysUserOrgQuizId == userQuizId);
            var quiz = entityDB.SystemUsersOrganizationsQuizzes.FirstOrDefault(rec => rec.SysUserOrgQuizId == userQuizId);
            if (quiz == null)
            {
                quiz = new SystemUsersOrganizationsQuizzes();
                quiz.VendorId = vendorId;
                quiz.ClientId = clientTemplate.ClientID;
                quiz.SysUserOrgQuizId = userQuizId;
                quiz.ClientTemplateId = clientTemplateId;
                quiz.QuizDateStart = DateTime.Now;
                entityDB.SystemUsersOrganizationsQuizzes.Add(quiz);
                entityDB.SaveChanges();
            }
            // Kiran on 12/1/2014
            else
            {
                var submitedDate = quiz.QuizDateSubmitted;//Rajesh on 1/23/2015
                quiz.QuizDateStart = DateTime.Now;
                entityDB.SaveChanges();
                if (quiz.QuizExpirationDate != null && (DateTime.Now.AddDays(-30) <= (DateTime)quiz.QuizExpirationDate || quiz.QuizExpirationDate <= DateTime.Now)) // Kiran on 03/24/2015
                {
                    
                    helper.RecordQuizHistory(quiz, entityDB);
                    quiz.PreviousLastQuizTakenStatus = quiz.QuizResult;
                    quiz.PreviousLastQuizSubmittedDate = submitedDate;
                    quiz.QuizExpirationDate = null;
                    quiz.QuizResult = null;
                    quiz.AllowRetakeTraining = null;
                    quiz.NoOfAttempts = null;
                    quiz.RetakeFeeAmount = null;
                    quiz.QuizDateSubmitted = null;
                    entityDB.Entry(quiz).State = EntityState.Modified;

                    var empQuizUserInputLastSubmitted = entityDB.EmployeeQuizUserInput.Where(rec => rec.SysUserOrgQuizId == quiz.SysUserOrgQuizId).ToList(); // kiran on 12/18/2014
                    foreach (var userInput in empQuizUserInputLastSubmitted)
                    {
                        entityDB.Entry(userInput).State = EntityState.Deleted;
                    }
                    var templateSectionsToDelete = entityDB.TemplateSections.Where(rec => rec.TemplateID == quiz.ClientTemplates.Templates.TemplateID).ToList();
                    foreach (var section in templateSectionsToDelete)
                    {
                        try
                        {
                            var quizCompletedSection = entityDB.QuizzesCompletedSections.FirstOrDefault(rec => rec.TemplateSectionId == section.TemplateSectionID && rec.SysUserOrgQuizId == quiz.SysUserOrgQuizId); // kiran on 12/18/2014
                            entityDB.Entry(quizCompletedSection).State = EntityState.Deleted;
                        }
                        catch { }
                    }

                    entityDB.SaveChanges();
                }
            }
            // Ends<<<
            //return View();
            return Redirect("QuizSectionsList?templateId=" + clientTemplate.TemplateID + "&TemplateSectionID=-1&quizId=" + quiz.SysUserOrgQuizId); // kiran on 12/18/2014
        }





        //Sub Section Id is for present selected Id
        [SessionExpire]
        [HeaderAndSidebar(headerName = "PREQUALIFICATIONREQUIREMENTS", sideMenuName = "")]
        public ActionResult QuizSectionsList(Guid templateId, long TemplateSectionID, long quizId)
        {
            SystemUsersOrganizationsQuizzes quiz;
            ActionResult actionResult;
            PaymentVerification(quizId, out quiz, out actionResult);
            Templates selectedTemplate = entityDB.Templates.Find(templateId);
            try
            {
                if (TemplateSectionID == -1)//If TemplateSectionID=-1 then 1st one is selected one
                    TemplateSectionID = selectedTemplate.TemplateSections.ToList().OrderBy(rec => rec.DisplayOrder).FirstOrDefault().TemplateSectionID;
            }
            catch (Exception ee)
            {
            }

            // Kiran on 11/6/2014 for redirecting to videos Section if the user clicks on Questions Section without completing Videos.
            var templateSection = entityDB.TemplateSections.Find(TemplateSectionID);
            if (templateSection.TemplateSectionType == 1)
            {
                var videoSectionId = templateSection.Templates.TemplateSections.FirstOrDefault(rec => rec.TemplateSectionType == 21 || rec.TemplateSectionType == 22).TemplateSectionID;
                var checkQuizVideoSectionCompleted = entityDB.QuizzesCompletedSections.FirstOrDefault(rec => rec.SysUserOrgQuizId == quizId && rec.TemplateSectionId == videoSectionId); // kiran on 12/18/2014
                if (checkQuizVideoSectionCompleted == null)
                {
                    TemplateSectionID = videoSectionId;
                }
            }
            // Ends<<<

            //Rajesh on 10/31/2014
            Guid LoggedInUseruserId = (Guid)Session["UserId"];
            var contacts = entityDB.SystemUsers.Find(LoggedInUseruserId).Contacts.FirstOrDefault();
            try { ViewBag.UserName = contacts.LastName.Trim() + ", " + contacts.FirstName.Trim(); } // Sandhya on 05/08/2015
            catch { ViewBag.UserName = Session["UserName"].ToString(); }
            //Ends<<<

            

            //Rajesh on 11/06/2014
            if (quiz.AllowRetakeTraining == false)
            {
                return View("NoAccessForQuiz");
            }
            //Ends<<<

            ViewBag.quiz = quiz;

            // Kiran on 10/31/2014
            if (templateSection.TemplateSectionType == 21)
            {
                var videoSubSectionId = entityDB.Templates.Find(templateId).TemplateSections.FirstOrDefault(rec => rec.TemplateSectionType == 21).TemplateSubSections.FirstOrDefault(rec => rec.SubSectionTypeCondition == "QuizVideos").SubSectionID;
                ViewBag.VBVideo = entityDB.Document.FirstOrDefault(rec => rec.ReferenceType == "QuizVideos" && rec.ReferenceRecordID == videoSubSectionId);
                ViewBag.VBTrainingVideosPath = ConfigurationSettings.AppSettings["TrainingVideosPath"];


                // Kiran on 11/3/2014
                try
                {
                    var alternateFormatSubSectionId = entityDB.Templates.Find(templateId).TemplateSections.FirstOrDefault(rec => rec.TemplateSectionType == 21).TemplateSubSections.FirstOrDefault(rec => rec.SubSectionTypeCondition == "QuizAlternateFormats").SubSectionID;
                    ViewBag.VBAlternateFormatDocuments = entityDB.Document.Where(rec => rec.ReferenceType == "QuizAlternateFormats" && rec.ReferenceRecordID == alternateFormatSubSectionId).ToList();
                }
                catch { }
            }
            if (templateSection.TemplateSectionType == 22)
            {
                LocalQuizTemplateBuilderIFrame form = new LocalQuizTemplateBuilderIFrame();
                var Section = entityDB.Templates.Find(templateId).TemplateSections.FirstOrDefault(rec => rec.TemplateSectionType == 22);
                var SubSection = Section.TemplateSubSections.FirstOrDefault(rec => rec.SubSectionTypeCondition == "QuizIFrame");
                if (SubSection.MetaJSON != null)
                {
                    LocalQuizIFrameMeta metaData = (LocalQuizIFrameMeta)new JavaScriptSerializer().Deserialize<LocalQuizIFrameMeta>(SubSection.MetaJSON);
                    form.QuizIFrameURL = metaData.IFrameURL;
                    form.QuizIFrameWidth = metaData.Width;
                    form.QuizIFrameHeight = metaData.Height;
                }
                ViewBag.IFrameData = form;
            }
            // Ends<<<

            //Creating side Menus
            List<LocalTemplatesSideMenuModel> sideMenus = new List<LocalTemplatesSideMenuModel>();

            foreach (TemplateSections section in selectedTemplate.TemplateSections.OrderBy(x => x.DisplayOrder).ToList())
            {
                LocalTemplatesSideMenuModel menu = new LocalTemplatesSideMenuModel(section.SectionName, "QuizSectionsList", "Quiz", TemplateSectionID == section.TemplateSectionID, "templateId=" + section.TemplateID + "&TemplateSectionID=" + section.TemplateSectionID + "&quizId=" + quizId);
                menu.isSectionCompleted = (entityDB.QuizzesCompletedSections.FirstOrDefault(rec => rec.SysUserOrgQuizId == quizId && rec.TemplateSectionId == section.TemplateSectionID) != null); // kiran on 12/18/2014
                sideMenus.Add(menu);

            }

            //<a href="QuizSectionsList?templateId=@v.TemplateID&TemplateSectionID=@v.TemplateSectionID"> @v.SectionName</a><br />
            ViewBag.sideMenu = sideMenus;
            ViewBag.TemplateSectionID = TemplateSectionID;
            ViewBag.VBDocumentsList = entityDB.Document;
            ViewBag.quizId = quizId;

            ViewBag.videosPath = ConfigurationSettings.AppSettings["Quizvideos"];
            getCommand();
            return View(selectedTemplate);
        }

        private bool PaymentVerification(long sysUserOrgQuizId, out SystemUsersOrganizationsQuizzes quiz,
            out ActionResult actionResult)
        {
            quiz = entityDB.SystemUsersOrganizationsQuizzes.Find(sysUserOrgQuizId);
            if (quiz != null && !quiz.QuizPayments.Any(rec => rec.IsOpen != null && rec.IsOpen.Value))
            {
                {
                    actionResult = View("NoPaymentForQuiz");
                    return true;
                }
            }
            actionResult = null;
            return false;
        }

        // Commented by kiran on 2/12/2014 as new task has been developed within the same view i.e, to display the wrong answers along with questions.

        //////[HeaderAndSidebar(headerName = "PREQUALIFICATIONREQUIREMENTS", sideMenuName = "")]
        //////public ActionResult QuizResultPage(int noOfQuestions,int noOfCorrectAns)
        //////{
        //////    ViewBag.noOfQuestions = noOfQuestions;
        //////    ViewBag.noOfCorrectAns = noOfCorrectAns;
        //////    return View();
        //////}

        // kiran on 2/10/2014
        [HeaderAndSidebar(headerName = "PREQUALIFICATIONREQUIREMENTS", sideMenuName = "")]
        //public ActionResult QuizResultPage(int noOfQuestions, int noOfCorrectAns, String fieldsWithValues, long SectionId, long quizId)
        public ActionResult QuizResultPage(int noOfQuestions, int noOfCorrectAns, long SectionId, long quizId)
        {
            ViewBag.noOfQuestions = noOfQuestions;
            ViewBag.noOfCorrectAns = noOfCorrectAns;
            // Kiran on 2/21/2014
            var quiz = entityDB.SystemUsersOrganizationsQuizzes.FirstOrDefault(rec => rec.SysUserOrgQuizId == quizId); // kiran on 12/18/2014
            
            // Kiran on 10/31/2014
            var quizDetails = entityDB.ClientTemplatesQuizDetails.FirstOrDefault(rec => rec.ClientTemplateID == quiz.ClientTemplateId);
            // kiran on 9/11/2014
            decimal quizResult = (Convert.ToDecimal(noOfCorrectAns) / Convert.ToDecimal(noOfQuestions));
            var quizResultInPercentage = Math.Round(quizResult, 2) * 100;
            // Ends<<<
            bool quizPassed = false;
            if (quizResultInPercentage >= quizDetails.QuizPassRate)
            {
                quizPassed = true;
            }
            ViewBag.VBQuizPassed = quizPassed;
            ViewBag.VBQuizPassRate = quizDetails.QuizPassRate;
            ViewBag.VBWaiverForm = quizDetails.WaiverForm;
            ViewBag.TrainingCertificateRequired = quiz?.Client.TrainingCertificate;
            ViewBag.VBQuizId = quizId;
            // Ends<<<

            var Quiz = entityDB.SystemUsersOrganizationsQuizzes.Find(quizId); // kiran on 12/18/2014

            var templateSection = entityDB.TemplateSections.Find(SectionId);
            var QuestionsList = new List<LocalQuizResultAns>();
            foreach (var subSection in templateSection.TemplateSubSections.ToList())
            {
                foreach (var questions in subSection.Questions.Where(rec => rec.Visible == true).ToList()) // Kiran on 9/13/2014
                {
                    LocalQuizResultAns ans = new LocalQuizResultAns();

                    ans.Question = questions.QuestionText;
                    ans.CorrectAnswer = questions.CorrectAnswer; // Kiran on 11/3/2014
                    var questionColumnDetailsId = questions.QuestionColumnDetails.FirstOrDefault() == null ? -1 : questions.QuestionColumnDetails.FirstOrDefault().QuestionColumnId;
                    ans.Options = questions.QuestionColumnDetails.FirstOrDefault() == null ? "" : questions.QuestionColumnDetails.FirstOrDefault().ColumnValues;
                    ans.QuestionControlType = questions.QuestionColumnDetails.FirstOrDefault() == null ? -1 : questions.QuestionColumnDetails.FirstOrDefault().QuestionControlTypeId;
                    var empQuizuserinput = "";
                    try { empQuizuserinput = entityDB.EmployeeQuizUserInput.FirstOrDefault(rec => rec.SysUserOrgQuizId == quizId && rec.QuestionColumnId == questionColumnDetailsId).UserInput; } // kiran on 12/18/2014
                    catch { }
                    ans.EmployeeAnswer = empQuizuserinput;
                    QuestionsList.Add(ans);
                }
            }

            ViewBag.VBEmployeeQuizQuestionsWithAnswers = QuestionsList;

            return View();
        }
        // Ends<<<


        [HttpPost]
        [SessionExpire(lastPageUrl = "../SystemUsers/Dashboard")]
        public String SubmitFormData(String fieldsWithValues, long SectionId, long quizId)//This filedsWithValues contains QuestionColumnId ^ value|QuestionColumnId ^ value etc..
        {
            String result = "Success";
            var questionColumnIds=new List<long>();
            try
            {
                foreach (var record in fieldsWithValues.Split('|'))
                {

                    string[] questionID_Val = record.Split('^');//0-QuestionColumn Id,1-question Val
                    if (questionID_Val.Length == 2)
                    {
                        bool isInsert = false;
                        //long PreQualificationId = Convert.ToInt64(Session["PreQualificationId"].ToString());
                        long QuestionColumnId = Convert.ToInt64(questionID_Val[0]);
                        EmployeeQuizUserInput preUserInput_NewRecord;
                        //To get existing record
                        preUserInput_NewRecord = entityDB.EmployeeQuizUserInput.FirstOrDefault(rec => rec.SysUserOrgQuizId == quizId && rec.QuestionColumnId == QuestionColumnId); // kiran on 12/18/2014

                        //if no record founded
                        if (preUserInput_NewRecord == null)
                        {
                            isInsert = true;
                            preUserInput_NewRecord = new EmployeeQuizUserInput();
                        }

                        preUserInput_NewRecord.SysUserOrgQuizId = quizId; // kiran on 12/18/2014
                        preUserInput_NewRecord.QuestionColumnId = QuestionColumnId;
                        questionColumnIds.Add(QuestionColumnId);
                        preUserInput_NewRecord.UserInput = questionID_Val[1].Trim();
                        if (isInsert)
                        {
                            if (String.IsNullOrWhiteSpace(questionID_Val[1]))
                                continue;
                            entityDB.EmployeeQuizUserInput.Add(preUserInput_NewRecord);
                        }
                        else
                            entityDB.Entry(preUserInput_NewRecord).State = EntityState.Modified;
                        entityDB.SaveChanges();
                    }

                }
            }
            catch (Exception ee)
            {
                result = "Error : " + ee.Message;
            }

            // Rajesh on 07/27/2013
            string strNextSectionURL = "";
            //long preQualificationId = Convert.ToInt64(Session["PreQualificationId"]);


            QuizzesCompletedSections sectionCompleted = entityDB.QuizzesCompletedSections.FirstOrDefault(rec => rec.SysUserOrgQuizId == quizId && rec.TemplateSectionId == SectionId); // kiran on 12/18/2014

            if (sectionCompleted == null)
            {
                sectionCompleted = new QuizzesCompletedSections();
                sectionCompleted.SysUserOrgQuizId = quizId; // kiran on 12/18/2014
                sectionCompleted.TemplateSectionId = SectionId;
                entityDB.QuizzesCompletedSections.Add(sectionCompleted);
                entityDB.SaveChanges();
            }



            var quiz = entityDB.SystemUsersOrganizationsQuizzes.Find(quizId); // kiran on 12/18/2014
            List<TemplateSections> tempSections = quiz.ClientTemplates.Templates.TemplateSections.OrderBy(rec => rec.DisplayOrder).ToList();

            long longSecid = SectionId;
            try
            {
                longSecid = tempSections[tempSections.IndexOf(tempSections.FirstOrDefault(rec => rec.TemplateSectionID == SectionId)) + 1].TemplateSectionID;
            }
            catch (Exception ee)
            { //If This is last submission

                bool flag_Completed = true;
                foreach (TemplateSections section in quiz.ClientTemplates.Templates.TemplateSections.OrderBy(x => x.DisplayOrder).ToList())
                {
                    if (entityDB.QuizzesCompletedSections.FirstOrDefault(rec => rec.SysUserOrgQuizId == quizId && rec.TemplateSectionId == section.TemplateSectionID) == null) // kiran on 12/18/2014
                    {
                        flag_Completed = false;
                        break;
                    }
                }
                var numberOfQuestions = 0;
                var numberOfCorrectAns = 0;
                if (flag_Completed)//All Sections completd
                {
                    //foreach (var sections in quiz.ClientTemplates.Templates.TemplateSections.ToList())
                    //{
                        //foreach (var subSections in quiz.ClientTemplates.Templates.TemplateSections.SelectMany(r=>r.TemplateSubSections))
                        //{
                    //var d = DateTime.Now;
                            //foreach (var question in quiz.ClientTemplates.Templates.TemplateSections.SelectMany(r => r.TemplateSubSections).SelectMany(r=>r.Questions).Where(rec => rec.Visible ).ToList()) // Kiran on 9/13/2014
                            //{
                    var questionColumns =
                        entityDB.QuestionColumnDetails.Where(r => questionColumnIds.Contains(r.QuestionColumnId)).Where(r=>r.Questions.Visible);
                    foreach (var columnDetails in questionColumns.ToList())
                    {
                        numberOfQuestions++;

                        if(entityDB.EmployeeQuizUserInput.Any(r=>r.QuestionColumnId==columnDetails.QuestionColumnId && r.SysUserOrgQuizId==quizId && r.UserInput.Trim()==columnDetails.Questions.CorrectAnswer.Trim()))
                            numberOfCorrectAns++;

                        //var userAns =
                        //    columnDetails.EmployeeQuizUserInput.FirstOrDefault(rec => rec.SysUserOrgQuizId == quizId)
                        //        .UserInput; // kiran on 12/18/2014

                        //var correctAns = "";
                        //try
                        //{
                        //    correctAns = columnDetails.Questions.CorrectAnswer; // Kiran on 11/3/2014
                        //}
                        //catch (Exception eee)
                        //{
                        //}
                        //if (correctAns.ToLower().Trim().Equals(userAns.ToLower().Trim()))
                        //    numberOfCorrectAns++;
                    }
                    //}
                            //var d1 = DateTime.Now;
                        //}
                    //}

                    var loc_quiz = entityDB.SystemUsersOrganizationsQuizzes.Find(quizId); // kiran on 12/18/2014
                    //Rajesh on 11/29/2014
                    //if (numberOfQuestions == numberOfCorrectAns)
                    //    loc_quiz.QuizResult = true;
                    //else
                    //    loc_quiz.QuizResult = false;


                    // kiran on 12/18/2014
                    var empSysUserOrgQuiz = entityDB.SystemUsersOrganizationsQuizzes.FirstOrDefault(rec => rec.SysUserOrgQuizId == quizId);
                    var quizPassRate = entityDB.ClientTemplatesQuizDetails.FirstOrDefault(rec => rec.ClientTemplateID == empSysUserOrgQuiz.ClientTemplateId).QuizPassRate;
                    //Ends<<< 
                    decimal quizResult = (Convert.ToDecimal(numberOfCorrectAns) / Convert.ToDecimal(numberOfQuestions));
                    var quizResultInPercentage = Math.Round(quizResult, 2) * 100;

                    loc_quiz.QuizResult = false;
                    if (quizResultInPercentage >= quizPassRate)
                    {
                        loc_quiz.QuizResult = true;
                    }
                    //Ends<<<



                    //Rajesh on 11/06/2014
                    if (loc_quiz.PreviousLastQuizSubmittedDate == null)
                    {
                        loc_quiz.QuizDateSubmitted = DateTime.Now; // Kiran on 12/1/2014
                    }
                    else
                    {
                        try
                        {
                            // Kiran on 08/27/2015
                            //loc_quiz.QuizDateSubmitted = ((DateTime)loc_quiz.PreviousLastQuizSubmittedDate).AddYears(1); // Kiran on 12/1/2014
                            loc_quiz.QuizDateSubmitted = DateTime.Now;
                            // Ends on 08/27/2015
                            // rajesh on 02/04/2015
                            if (loc_quiz.QuizDateSubmitted < DateTime.Now)
                            {
                                loc_quiz.QuizDateSubmitted = DateTime.Now;
                            }
                            // Ends<<<
                        }
                        catch
                        {
                            loc_quiz.QuizDateSubmitted = DateTime.Now; // Kiran on 12/1/2014
                        }
                    }

                    if (loc_quiz.QuizResult == true)
                    {
                        var clientTempQuizDetails = loc_quiz.ClientTemplates.ClientTemplatesQuizDetails.FirstOrDefault();
                        if (clientTempQuizDetails.PeriodLengthUnit != null)
                        {
                            if ((loc_quiz.QuizExpirationDate == null || loc_quiz.PreviousLastQuizSubmittedDate == null) || (loc_quiz.PreviousLastQuizSubmittedDate.Value.AddYears(1) < DateTime.Now))
                            {
                                if (clientTempQuizDetails.PeriodLengthUnit.ToLower().Equals("d"))
                                    loc_quiz.QuizExpirationDate = ((DateTime)loc_quiz.QuizDateSubmitted).AddDays((int)clientTempQuizDetails.PeriodLength);
                                else if (clientTempQuizDetails.PeriodLengthUnit.ToLower().Equals("m"))
                                    loc_quiz.QuizExpirationDate = ((DateTime)loc_quiz.QuizDateSubmitted).AddMonths((int)clientTempQuizDetails.PeriodLength);
                                else if (clientTempQuizDetails.PeriodLengthUnit.ToLower().Equals("y"))
                                    loc_quiz.QuizExpirationDate = ((DateTime)loc_quiz.QuizDateSubmitted).AddYears((int)clientTempQuizDetails.PeriodLength);
                            }
                            // Kiran on 08/27/2015
                            else if (loc_quiz.PreviousLastQuizSubmittedDate.Value.AddYears(1) > DateTime.Now)
                            {
                                if (clientTempQuizDetails.PeriodLengthUnit.ToLower().Equals("d"))
                                    loc_quiz.QuizExpirationDate = ((DateTime)loc_quiz.PreviousLastQuizSubmittedDate.Value.AddYears(1)).AddDays((int)clientTempQuizDetails.PeriodLength);
                                else if (clientTempQuizDetails.PeriodLengthUnit.ToLower().Equals("m"))
                                    loc_quiz.QuizExpirationDate = ((DateTime)loc_quiz.PreviousLastQuizSubmittedDate.Value.AddYears(1)).AddMonths((int)clientTempQuizDetails.PeriodLength);
                                else if (clientTempQuizDetails.PeriodLengthUnit.ToLower().Equals("y"))
                                    loc_quiz.QuizExpirationDate = ((DateTime)loc_quiz.PreviousLastQuizSubmittedDate.Value.AddYears(1)).AddYears((int)clientTempQuizDetails.PeriodLength);
                            }

                            // Ends<<<
                            //Close the open period in quizpayments with this quiz.
                            var quizPayment = loc_quiz.QuizPayments.FirstOrDefault(rec => rec.IsOpen!=null && rec.IsOpen.Value);
                            if (quizPayment != null)
                            {
                                quizPayment.IsOpen = false;
                                quizPayment.SubmittedDate = loc_quiz.QuizDateSubmitted;
                                quizPayment.ExpirationDate = loc_quiz.QuizExpirationDate;
                            }
                        }
                    }
                    if (loc_quiz.NoOfAttempts == null)
                        loc_quiz.NoOfAttempts = 0;
                    loc_quiz.NoOfAttempts = loc_quiz.NoOfAttempts + 1;
                    var clientTemplateQuizDetails = quiz.ClientTemplates.ClientTemplatesQuizDetails.FirstOrDefault();
                    if (clientTemplateQuizDetails.QuizRetake == true)
                    {
                        if (clientTemplateQuizDetails.NumberOfAttempts == loc_quiz.NoOfAttempts)
                        {
                            loc_quiz.AllowRetakeTraining = false;
                        }
                    }
                    //Ends<<<

                    entityDB.Entry(loc_quiz).State = EntityState.Modified;
                    entityDB.SaveChanges();

                    //                    strNextSectionURL = "QuizResultPage?noOfQuestions="+numberOfQuestions+"&noOfCorrectAns=" + numberOfCorrectAns;
                    //strNextSectionURL = "QuizResultPage?noOfQuestions=" + numberOfQuestions + "&noOfCorrectAns=" + numberOfCorrectAns + "&fieldsWithValues=" + fieldsWithValues + "&SectionId=" + SectionId + "&quizId=" + quizId;
                    strNextSectionURL = "QuizResultPage?noOfQuestions=" + numberOfQuestions + "&noOfCorrectAns=" + numberOfCorrectAns + "&SectionId=" + SectionId + "&quizId=" + quizId;

                    if (result.Equals("Success"))
                        result = strNextSectionURL;
                    return result;

                }
            }
            strNextSectionURL = "../Quiz/QuizSectionsList?templateId=" + quiz.ClientTemplates.TemplateID + "&TemplateSectionID=" + longSecid + "&quizId=" + quizId;
            if (result.Equals("Success"))
                result = strNextSectionURL;
            // Ends <<<<

            return result;
        }



        public void getCommand()
        {
            SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["EFDbContext"].ConnectionString);
            connection.Open();
            ViewBag.command = connection.CreateCommand();

        }

        // Kiran on 12/2/2014
        [SessionExpire]
        public ActionResult DownloadDoc(string idAsFileName, string userEnteredFilename, long clientId)
        {
            try
            {
                var dotIndex = userEnteredFilename.LastIndexOf('.');
                var extension = userEnteredFilename.Substring(dotIndex);

                // kiran on 12/16/2014
                string invalidChars = Regex.Escape(new string(System.IO.Path.GetInvalidFileNameChars()));
                string invalidReStr = string.Format(@"[{0}]+", invalidChars);
                userEnteredFilename = Regex.Replace(userEnteredFilename, invalidReStr, "_").Replace(";", "").Replace(",", "");
                // Ends<<<

                //var filePath = HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["ClientUploadsPath"]) + "/" + idAsFileName + extension;
                var filePath = Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["TrainingVideosPath"]), idAsFileName + extension);
                if (System.IO.File.Exists(filePath))
                {
                    var fs = System.IO.File.OpenRead(filePath);
                    //return File(fs, "file", userEnteredFilename);
                    if (extension == ".pdf")
                    {
                        Response.AppendHeader("Content-Disposition", "inline; filename=" + userEnteredFilename + ".pdf");
                        return File(fs, "application/pdf");
                    }
                    else if (extension == ".mp4")
                    {
                        Response.AppendHeader("Content-Disposition", "inline; filename=" + userEnteredFilename + ".mp4");
                        return File(fs, "application/mp4");
                    }
                    else 
                    {
                        Response.AppendHeader("Content-Disposition", "inline; filename=" + userEnteredFilename + extension);
                        return File(fs, "application/octet-stream");
                    }

                }
                else
                {
                    return View("DocDownloadFailed");
                }
            }
            catch
            {
                throw new HttpException(404, @Resources.Resources.EmailTemplatesCouldNotFound + userEnteredFilename);
            }
        }

        [HttpGet]
        public ActionResult PartialQuizWaiverForm(long quizId)
        {
            LocalWaiverForm waiverForm = new LocalWaiverForm();
            var quizDetails = _quizBusiness.getQuizDetails(quizId);
            var vendorDetails = _organizationBusiness.GetOrganization((long)quizDetails.VendorId);
            var employeeDetails = _sysUserBusiness.GetUserInfo(quizDetails.UserId);
            
            waiverForm.EmployeeName = employeeDetails.LastName.Trim() + ", " + employeeDetails.FirstName.Trim();
            waiverForm.VendorCompany = vendorDetails.Name;
            waiverForm.Date = DateTime.Now;
            waiverForm.Content = quizDetails.WaiverFormContent;
            waiverForm.UserId = quizDetails.UserId;

            waiverForm.Content = waiverForm.Content.Replace("[EmployeeName]", waiverForm.EmployeeName);
            waiverForm.Content = waiverForm.Content.Replace("[VendorName]", waiverForm.VendorCompany);
            ViewBag.Content = waiverForm.Content;
            return View(waiverForm);
        }

        [HttpGet]
        public ActionResult QuizWaiverForm(long quizId)
        {
            LocalWaiverForm waiverForm = new LocalWaiverForm();
            var quizDetails = _quizBusiness.getQuizDetails(quizId);
            var vendorDetails = _organizationBusiness.GetOrganization((long)quizDetails.VendorId);
            var employeeDetails = _sysUserBusiness.GetUserInfo(quizDetails.UserId);
            //var quizWaiverFormCompletionLog = _quizBusiness.getWaiverFormLog(quizId, quizDetails.QuizSubmittedDate);

            waiverForm.EmployeeName = employeeDetails.LastName.Trim() + ", " + employeeDetails.FirstName.Trim();
            waiverForm.VendorCompany = vendorDetails.Name;
            waiverForm.Date = DateTime.Now;
            waiverForm.Content = quizDetails.WaiverFormContent;
            waiverForm.UserId = quizDetails.UserId;
            waiverForm.quizId = quizId;

            waiverForm.Content = waiverForm.Content.Replace("[EmployeeName]", waiverForm.EmployeeName);
            waiverForm.Content = waiverForm.Content.Replace("[VendorName]", waiverForm.VendorCompany);
            waiverForm.Content = waiverForm.Content.Replace("[Signature]", waiverForm.EmployeeName);
            //waiverForm.Content = waiverForm.Content.Replace("Signature", "");
            waiverForm.Content = waiverForm.Content.Replace("[WaiverFormSubmittedDate]", DateTime.Now.Date.ToShortDateString());
            //waiverForm.Content = waiverForm.Content.Replace("Date", "");

            return View(waiverForm);
        }
        [HttpPost]
        public ActionResult QuizWaiverForm(LocalWaiverForm waiverForm)
        {
            var quiz = _quizBusiness.getQuizDetails(waiverForm.quizId);
            waiverForm.Content = quiz.WaiverFormContent;
            

            waiverForm.Content = waiverForm.Content.Replace("[EmployeeName]", waiverForm.EmployeeName);
            waiverForm.Content = waiverForm.Content.Replace("[VendorName]", waiverForm.VendorCompany);
            waiverForm.Content = waiverForm.Content.Replace("[Signature]", waiverForm.EmployeeName);

            if (!waiverForm.Signature.Trim().Equals(waiverForm.EmployeeName.Trim()))
            {
                ViewBag.signatureMismatch = true;
                return View(waiverForm);
            }
            if(ModelState.IsValid)
            {
                

                var quizWaiveFormLog = _quizBusiness.getWaiverFormLog(waiverForm.quizId, quiz.QuizSubmittedDate);

                if(quizWaiveFormLog == null)
                {
                    QuizWaiverFormCompletionLog logQuizWaiverForm = new QuizWaiverFormCompletionLog();
                    logQuizWaiverForm.QuizId = waiverForm.quizId;
                    logQuizWaiverForm.SubmittedDate = DateTime.Now;
                    logQuizWaiverForm.SubmittedBy = waiverForm.UserId;
                    logQuizWaiverForm.Signature = waiverForm.Signature;

                    entityDB.Entry(logQuizWaiverForm).State = EntityState.Added;

                    entityDB.SaveChanges();
                }
                return RedirectToAction("EmployeeQuizzes", "Employee", new { @userId =waiverForm.UserId, @type =1});
            }
            return View(waiverForm);
        }
    }
}
