﻿using System.Web.Mvc;
using FVGen3.BusinessLogic.Interfaces;

namespace FVGen3.WebUI.Controllers.API
{
    public class ProficiencyController : Controller
    {
        //
        // GET: /Proficiency/
        private IProficiencyBusiness proficiencyBusiness;
        public ProficiencyController(IProficiencyBusiness proficiency)
        {
            proficiencyBusiness = proficiency;
        }
        [HttpPost]
        public JsonResult GetProficiencies()
        {
            return Json(proficiencyBusiness.GetProficiencies());
        }

    }
}
