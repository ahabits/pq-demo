﻿using FVGen3.BusinessLogic.Interfaces;
using FVGen3.Domain.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Hosting;
using System.Web.Http;
using FVGen3.Common;
using FVGen3.DataLayer.DTO;
using Newtonsoft.Json;
using FVGen3.WebUI.Models;

namespace FVGen3.WebUI.Controllers.API
{
    public class EmployeeController : ApiController
    {
        private IEmployeeBusiness _employee;
        IOrganizationBusiness _org;
        public EmployeeController(IEmployeeBusiness _employee,IOrganizationBusiness _org)

        {
            this._employee = _employee;
            this._org = _org;
        }
        [HttpPost]
        public object Trainings()
        {
            var clientSecret = "";
            try
            {
                clientSecret = Request.Headers.GetValues("ClientSecret").FirstOrDefault();
                var orgId = _org.GetOrganization(clientSecret).OrganizationID;
                return _employee.VendorEmployeeTrainings(orgId);
            }
            catch(Exception ee)
            {
                ExceptionModel ExceptionModel = new ExceptionModel();
                ExceptionModel.Message = "Unable to preocess the request,Please try again with valid client secret.";
                ExceptionModel.ExceptionMessage = ee.Message;                
            }
            return "";
        }

        [HttpPost]
        public string SinePro([FromBody] SignProCheckin param)
        {
            var path = "c:\\sinepro\\"+ DateTime.Now.ToString("yyyy MMMM dd HHmmss") + ".csv";
            try
            {
                var checkInCheckout = new SineProCheckinCheckout
                {
                    RecId = param.data.id,
                    LastName = param.data.lastName,
                    FirstName = param.data.firstName,
                    EmployeeEmail = param.data.email,
                    MobileNo = param.data.mobile,
                    ExpiryDate = param.data.expiresDate,
                    VisitorType = param.data.visitorType?.name,
                    VisitorId = param.data.visitorType?.id,
                    CompanyName = param.data.company,
                    SiteName = param.data.site?.name,
                    SiteId = param.data.site?.id,
                    Status = param.data.status,
                    ClientId = 13159
                };
                if (param.event_ == "signin_after")
                {
                    checkInCheckout.CheckinTime = param.data.startDate;
                    checkInCheckout.CheckinUser = param.data.host?.lastName + ", " + param.data.host?.firstName;
                }
                else
                {
                    checkInCheckout.CheckOutUser = param.data.host?.lastName + ", " + param.data.host?.firstName;
                    checkInCheckout.CheckoutTime = param.data.expiresDate;
                }

                checkInCheckout.HostId = param.data.host?.id;
                checkInCheckout.HostGroupId = param.data.host?.hostGroup;
                checkInCheckout.HostGroupName = param.data.host?.hostGroupName;
                _employee.SaveSignProCheckinCheckOut(checkInCheckout);
            }
            catch (Exception e)
            {
                //SendMail.sendMail("rajesh.pendela@ahaapps.com", e.Message + e.StackTrace, "Sine Pro Crash");
            }

            File.WriteAllText(path , JsonConvert.SerializeObject(param));
            return "Triggered Successfully";
        }
    }
}


