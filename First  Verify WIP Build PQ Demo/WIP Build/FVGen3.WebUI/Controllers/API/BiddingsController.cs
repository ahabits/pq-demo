﻿using System.Web.Mvc;
using FVGen3.BusinessLogic.Interfaces;

namespace FVGen3.WebUI.Controllers.API
{
    public class BiddingsController : Controller
    {
        private IBiddingInterestsBusiness biddingInterests;
        public BiddingsController(IBiddingInterestsBusiness bidding)
        {
            biddingInterests = bidding;
        }
        [HttpPost]
        public JsonResult GetBiddings()
        {
            return Json(biddingInterests.GetBiddings());
        }
    }
}
