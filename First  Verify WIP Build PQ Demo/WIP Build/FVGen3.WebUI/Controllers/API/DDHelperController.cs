﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using FVGen3.BusinessLogic;
using FVGen3.DataLayer.DTO;

namespace FVGen3.WebUI.Controllers.API
{
    public class DDHelperController : ApiController
    {
        [HttpGet]
        public List<KeyValuePair<string, string>> StateCodes()
        {
            DropdDownDataSupplier DDDS = new DropdDownDataSupplier();
            return DDDS.GetDynamicTableData("UnitedStates");
        }
        [HttpGet]
        public List<KeyValuePair<string, string>> StateCodes(int vendorID, string emptyResultString = "")
        {
            DropdDownDataSupplier DDDS = new DropdDownDataSupplier();
            if (String.IsNullOrEmpty(emptyResultString)) { emptyResultString = string.Empty; }
            var result = DDDS.GetDynamicTableData("UnitedStates", vendorID);
            //if (emptyResultString != "")
            {
                result.Insert(0, new KeyValuePair<string, string>(emptyResultString, emptyResultString));
            }
            return result;
        }
        //[HttpGet]
        //public List<KeyValuePair<string, string>> GetRiskLevels1(int clientID)
        //{
        //    DropdDownDataSupplier DDDS = new DropdDownDataSupplier();

        //    var result = DDDS.GetRiskLevels(clientID);
        //    if (result.Count > 1)
        //    {
        //        result.Insert(0, new KeyValuePair<string, string>("", ""));
        //    }
        //    return result;
        //}
        [HttpGet]
        public List<RiskLevelInfo> GetRiskLevelWithDetails(int clientID)
        {
            DropdDownDataSupplier DDDS = new DropdDownDataSupplier();

            var result = DDDS.GetRiskLevelWithDetails(clientID);
            //if (result.Count > 1)
            {
                result.Insert(0, new RiskLevelInfo() { Risk = "Select Risk Level", ClientTemplateId = "", IsLocked = false });
            }
            return result;
        }
        [HttpGet]
        public List<KeyValuePair<string, string>> GetLocations(int client)
        {
            DropdDownDataSupplier DDDS = new DropdDownDataSupplier();

            var result = DDDS.GetLocations(client);

            //if (result.Count > 1)
            //{
            //    result.Insert(0, new KeyValuePair<string, string>("", ""));
            //}
            return result;
        }
        [HttpGet]
        public List<KeyValuePair<string, string>> GetUserEmails(int orgID,long PqId=0)
        {
            DropdDownDataSupplier DDDS = new DropdDownDataSupplier();

            var result = DDDS.GetUserEmails(orgID, PqId);

            return result;
        }

    }
}
