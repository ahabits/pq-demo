﻿
using System.Web.Mvc;
using FVGen3.BusinessLogic;

namespace FVGen3.WebUI.Controllers.API
{
    public class CommonController : Controller
    {
        public JsonResult GetUnitedStates(bool includeCanada)
        {
            var helper = new DropdDownDataSupplier();
            return Json(helper.GetUnitedStatesTableData(-1, false, includeCanada));
        }

    }
}
