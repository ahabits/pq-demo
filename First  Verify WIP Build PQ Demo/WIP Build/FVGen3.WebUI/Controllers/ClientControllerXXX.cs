﻿/*
Page Created Date:  06/26/2013
Created By: Rajesh Pendela
Purpose:This page contains client dashboard
Version: 1.0
***************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FVGen3.Domain.Concrete;
using FVGen3.Domain.Abstract;
using FVGen3.WebUI.Annotations;
using FVGen3.Domain.Entities;
using System.Configuration;

namespace FVGen3.WebUI.Controllers
{
    public class ClientController : BaseController
    {
        //
        // GET: /ClientDashboard/
          EFDbContext entityDB;
        

        public ClientController(ISystemUsersRepository repositorySystemUsers) 
        {
            
            entityDB = new EFDbContext();
            
        }

        
        /*
           Created Date:  06/26/2013   Created By: Kiran Talluri
           Purpose: To display the Client Users List 
       */
        [SessionExpire]
        [HeaderAndSidebar(headerName = "MyAccount", sideMenuName = "SearchUser", ViewName = "ClientUsersList")]
        public ActionResult ClientUsersList()
        {
            Guid currentUserId = new Guid(Session["UserId"].ToString());
            var currentUserOrg = from sysUserOrgs in entityDB.SystemUsersOrganizations.Where(record => record.UserId == currentUserId).ToList()
                                 select sysUserOrgs.OrganizationId;

            var clientsList = entityDB.SystemUsersOrganizations.Where(record => currentUserOrg.Contains(record.OrganizationId)).ToList();
            
            ViewBag.VBclientsList = clientsList;
            return View();
        }

        //DV:RP DT:06-26-2013
        //Desc:To create a client dash board
        //[SessionExpire(lastPageUrl = "~/ClientDashboard/ClientDashboard")]
        [HeaderAndSidebar(headerName = "DashBoard", sideMenuName = "")]
        [SessionExpire]
        public ActionResult ClientDashboard()
        {
            Guid guidUserId = (Guid)Session["UserId"];
            var varClientOrgList = from sysOrgList in entityDB.SystemUsersOrganizations.Where(m => m.UserId == guidUserId).ToList()
                                   select new SelectListItem
                                   {
                                       Text = sysOrgList.Organizations.Name,
                                       Value = sysOrgList.OrganizationId.ToString(),
                                       
                                   };
            ViewBag.VBClientOrgList = varClientOrgList.ToList();
            //ViewBag.VBOrgId = ViewBag.varClientOrgList.FirstOrDefault().Value;

            ViewBag.LogoPath = ConfigurationSettings.AppSettings["LogoUploadPath"].ToString() + "/";//Rajesh on 08/09/2013

            //Rajesh on 09/09/2013
            var clientId = Convert.ToInt64(Session["currentOrgId"]);
            var varPrequalificationList = entityDB.Prequalification.Where(m => m.PrequalificationStatusId == m.PrequalificationStatus.PrequalificationStatusId && m.ClientId == clientId).ToList();

            ViewBag.VBPrequalificationList = varPrequalificationList;


            //Rajesh on 09/06/2013 >>>
            //Siva on Dec 4th >>>
            int notificationCount = 0;
            List<LocalNotifications> notifications = new List<LocalNotifications>();
            foreach (var orgNotificationSent in entityDB.OrganizationsNotificationsSent.Where(rec => rec.ClientId == clientId && rec.SetupType==1).ToList())
            {
                foreach (var logs in orgNotificationSent.OrganizationsNotificationsEmailLogs.Where(rec => rec.NotificationType == 0).GroupBy(rec => rec.VendorId))
                {
                    var orglog = (OrganizationsNotificationsEmailLogs)logs.OrderBy(rec => rec.NotificationNo).LastOrDefault();

                    LocalNotifications locNotification = new LocalNotifications();
                    locNotification.ClientName = orgNotificationSent.Client.Name;
                    locNotification.VendorName = entityDB.OrganizationsNotificationsEmailLogs.FirstOrDefault().Vendor.Name;//orglog.Vendor.OrganizationID+"";
                    locNotification.Purpose = "";
                    OrganizationsEmailSetupNotifications orgEmailSetupNotification = entityDB.OrganizationsEmailSetupNotifications.FirstOrDefault(rec => rec.OrgEmailSetupId == orgNotificationSent.SetupTypeId);
                    if (orgEmailSetupNotification != null)
                    {
                        if (orgEmailSetupNotification.SetupType == 0)//Document
                        {
                            locNotification.Purpose = entityDB.DocumentType.Find(orgEmailSetupNotification.SetupTypeId).DocumentTypeName;
                        }
                        else if (orgEmailSetupNotification.SetupType == 1)//Status
                        {
                            locNotification.Purpose = entityDB.PrequalificationStatus.Find(orgEmailSetupNotification.SetupTypeId).PrequalificationStatusName;
                        }
                    }

                    //Dec 4th
                    locNotification.Comments = orglog.Comments;
                    if (orglog.Comments != null)
                    {
                        if (orglog.Comments.Length > 30)
                            locNotification.SmallComment = orglog.Comments.Substring(0, 30);
                        else
                            locNotification.SmallComment = orglog.Comments;
                    }
                    else
                        locNotification.SmallComment = "";

                    if (orglog.ReadByClient == 0) //0 means unread notification
                        notificationCount += 1;
                    //End Dec 4th

                    notifications.Add(locNotification);
                }
            }

            //Dec 4th
            ViewBag.NotificationsSent = notifications;
            ViewBag.NotificationCount = notificationCount;

            ViewBag.PreQualificationsStatus = entityDB.PrequalificationStatus.ToList();
            //Ends<<<<

            return View();
        }

        //siva on Dec 4th
        [SessionExpire]
        [HeaderAndSidebar(headerName = "DashBoard", sideMenuName = "")]
        public string markAllNotificationsAsRead()
        {
            string statusStr = "updateSuccess";

            List<OrganizationsNotificationsEmailLogs> logs = entityDB.OrganizationsNotificationsEmailLogs.Where(log => log.ReadByClient == 0).ToList();
            foreach (var emailLog in logs)
            {
                emailLog.ReadByClient = 1;
                entityDB.Entry(emailLog).State = System.Data.EntityState.Modified;

                statusStr = "updateSuccess";
            }
            entityDB.SaveChanges();

            ViewBag.NotificationCount = 0;

            return statusStr;
        }

        //DV:RP DT:06-26-2013
        //Desc:To create a client dash board for pie chart
       
        //public String getPiChartURL(long clientId)
        //{
        //    String strUrl="";
        //    Session["currentOrgId"] = clientId;


        //    var query_prequalification = from preQualification in entityDB.Prequalification
        //                                 where preQualification.ClientId==clientId
        //                group preQualification by preQualification.PrequalificationStatus.PrequalificationStatusName into g
        //                select new
        //                {
        //                    name = g.Key,
        //                    count = g.Count()
        //                };

        //    //"http://chart.apis.google.com/chart?cht=p3&chs=500x200&chd=t:73,13,10,3,1&chco=80C65A,224499,FF0000&chl=Chocolate|Puff+Pastry|Cookies|Muffins|Gelato";
        //    //var url_googleChart = "http://chart.apis.google.com/chart?cht=p3&chs=360x150";
        //    //var chartData = "&chd=t:";
        //    //var chartlabels = "&chl=";
        //    //var chartLegend="&chdl=";
        //    //if (query_prequalification != null && query_prequalification.Count() > 0)
        //    //{
        //    //    foreach (var data in query_prequalification)
        //    //    {
        //    //        chartData += data.count + ",";
        //    //        chartlabels += data.count + "|";
        //    //        chartLegend += data.name + "|";
        //    //    }
                
        //    //    chartData = chartData.Substring(0, chartData.Length - 1);
        //    //    chartlabels = chartlabels.Substring(0, chartlabels.Length - 1);
        //    //    chartLegend = chartLegend.Substring(0, chartLegend.Length - 1);
                
        //    //}

        //    //else//For No Vendors
        //    //{
        //    //    chartData += "1" ;
        //    //    chartlabels += "0" ;
        //    //    chartLegend += "No Vendors";
        //    //}
        //    //strUrl = url_googleChart + chartData + chartlabels+chartLegend;
        //    //return strUrl;

        //    // Kiran on 12/04/2013
        //    var url_googleChart = "http://chart.apis.google.com/chart?cht=bhs&chs=400x125&chd=s:ello&chco=80C65A|224499|FF0000|F0F0F0&chxt=x&chxr=1,50,250,50";
        //    var chartData = "&chd=t:";
        //    //var chartlabels = "&chl=";
        //    var chartLegend="&chdl=";
        //    if (query_prequalification != null && query_prequalification.Count() > 0)
        //    {
        //        foreach (var data in query_prequalification)
        //        {
        //            chartData += data.count + ",";
        //            //chartlabels += data.count + "|";
        //            chartLegend += data.name + "|";
        //        }
                
        //        chartData = chartData.Substring(0, chartData.Length - 1);
        //        //chartlabels = chartlabels.Substring(0, chartlabels.Length - 1);
        //        chartLegend = chartLegend.Substring(0, chartLegend.Length - 1);
        //    }

        //    else//For No Vendors
        //    {
        //        chartData += "1";
        //        //chartlabels += "0" ;
        //        chartLegend += "No Vendors";
        //    }
        //    strUrl = url_googleChart + chartData + chartLegend;
        //    return strUrl;




        //}


        public String getPiChartURL(long clientId)
        {
            String strUrl = "";
            Session["currentOrgId"] = clientId;


            var query_prequalification = from preQualification in entityDB.Prequalification.Where(rec => rec.PrequalificationStatusId > 2)
                                         where preQualification.ClientId == clientId
                                         group preQualification by preQualification.PrequalificationStatus into g
                                         select new
                                         {
                                             Name = g.Key.PrequalificationStatusName,
                                             Count = g.Count(),
                                             Color = g.Key.Color
                                         };

            //"http://chart.apis.google.com/chart?cht=p3&chs=500x200&chd=t:73,13,10,3,1&chco=80C65A,224499,FF0000&chl=Chocolate|Puff+Pastry|Cookies|Muffins|Gelato";
            //var url_googleChart = "http://chart.apis.google.com/chart?cht=p3&chs=360x150";
            //var chartData = "&chd=t:";
            //var chartlabels = "&chl=";
            //var chartLegend="&chdl=";
            //if (query_prequalification != null && query_prequalification.Count() > 0)
            //{
            //    foreach (var data in query_prequalification)
            //    {
            //        chartData += data.count + ",";
            //        chartlabels += data.count + "|";
            //        chartLegend += data.name + "|";
            //    }

            //    chartData = chartData.Substring(0, chartData.Length - 1);
            //    chartlabels = chartlabels.Substring(0, chartlabels.Length - 1);
            //    chartLegend = chartLegend.Substring(0, chartLegend.Length - 1);

            //}

            //else//For No Vendors
            //{
            //    chartData += "1" ;
            //    chartlabels += "0" ;
            //    chartLegend += "No Vendors";
            //}
            //strUrl = url_googleChart + chartData + chartlabels+chartLegend;
            //return strUrl;

            // Kiran on 12/04/2013
            //http://chart.apis.google.com/chart?cht=bhs&chs=400x125&chco=80C65A|224499|FF0000|F0F0F0&chxt=x&chxr=0,0,1000,100&chd=t:100,1000,250,500&chdl=No%20Response|Prequalified&chbh=width&chds=0,1000
            var url_googleChart = "http://chart.apis.google.com/chart?cht=bhs&chs=400x350&chxt=x&chdlp=b";
            var chartData = "&chd=t:";
            //var chartlabels = "&chl=";
            var chartLegend = "&chdl=";
            var chartColor = "&chco=";

            var max = 1;
            try
            {
                max = query_prequalification.Max(rec => rec.Count);
            }
            catch (Exception ee) { }

            max = max + (5 - max % 5);
            var chartXR = "&chxr=0,0," + max + "," + (float)max / 5;
            var chartDS = "&chds=0," + max;
            var preQualificationStatusNumber = (from preQualifications in entityDB.Prequalification.ToList()
                                                group preQualifications by preQualifications.PrequalificationStatusId into g
                                                select g.Key).ToList();

            foreach (var data in entityDB.PrequalificationStatus.Where(rec => rec.PrequalificationStatusId > 2 && !preQualificationStatusNumber.Contains(rec.PrequalificationStatusId)))
            {
                chartData += 0 + ",";
                //chartlabels += data.count + "|";
                chartLegend += data.PrequalificationStatusName + "|";
                chartColor += data.Color + "|";
            }
            if (query_prequalification != null)
            {
                foreach (var data in query_prequalification)
                {
                    chartData += data.Count + ",";
                    //chartlabels += data.count + "|";
                    chartLegend += data.Name + "|";
                    chartColor += data.Color + "|";
                }


            }

            else//For No Vendors
            {
                chartData += "1";
                //chartlabels += "0" ;
                chartLegend += "No Vendors";

            }


            chartData = chartData.Substring(0, chartData.Length - 1);
            //chartlabels = chartlabels.Substring(0, chartlabels.Length - 1);
            chartLegend = chartLegend.Substring(0, chartLegend.Length - 1);
            chartColor = chartColor.Substring(0, chartColor.Length - 1);
            strUrl = url_googleChart + chartData + chartLegend + chartColor + chartXR + chartDS;
            return strUrl;




        }
    }
}
