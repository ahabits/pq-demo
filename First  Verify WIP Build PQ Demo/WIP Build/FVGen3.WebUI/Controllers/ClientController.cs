﻿/*
Page Created Date:  06/26/2013
Created By: Rajesh Pendela
Purpose:This page contains client dashboard
Version: 1.0
***************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FVGen3.Domain.Concrete;
using FVGen3.Domain.Abstract;
using FVGen3.WebUI.Annotations;
using FVGen3.Domain.Entities;
using System.Configuration;
using FVGen3.BusinessLogic.Interfaces;
using FVGen3.WebUI.Constants;
using FVGen3.DataLayer.DTO;
using Resources;

namespace FVGen3.WebUI.Controllers
{
    public class ClientController : BaseController
    {
        //
        // GET: /ClientDashboard/
        EFDbContext entityDB;

        private IVendorBusiness _vendorBusiness;
        private IClientBusiness _clientBusiness;
        private IOrganizationBusiness _organizationBusiness;
        public ClientController(ISystemUsersRepository repositorySystemUsers, IVendorBusiness _vendorBusiness, IClientBusiness clientBusiness, IOrganizationBusiness _organizationBusiness)
        {
            entityDB = new EFDbContext();
            this._vendorBusiness = _vendorBusiness;
            _clientBusiness = clientBusiness;
            this._organizationBusiness = _organizationBusiness;
        }
        [SessionExpire]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "MyAccount", sideMenuName = "SearchUser", ViewName = "ClientOrVendorUsersList")]
        public ActionResult ClientOrVendorUsersList()
        {

            var varorgId = SSession.OrganizationId;
            var varOrgList = _clientBusiness.GetSubClients(varorgId, true).Select(r => new SelectListItem() { Value = r.Key, Text = r.Value });
            varOrgList.ToList();
            ViewBag.VBSubClients = varOrgList;

            return View();
        }

        public ActionResult ClientUsersList(DataSourceRequest request, long? clientId, string filters)
        {
            if (clientId == 0 && SSession.Role != OrganizationType.Admin || clientId == null)
            {
                clientId = SSession.OrganizationId;
            }
            //const int PageSize = 10;
            var varorgId = SSession.OrganizationId;
            var varOrgList = _clientBusiness.GetSubClients(varorgId, true).Select(r => new SelectListItem() { Value = r.Key, Text = r.Value, Selected = (r.Key == clientId.ToString()) });
            varOrgList.ToList();
            ViewBag.VBSubClients = varOrgList;
            //return this.View(data);
            Guid currentUserId = new Guid(Session["UserId"].ToString());
            var currentUserOrg = entityDB.SystemUsersOrganizations.Where(record => record.UserId == currentUserId).Select(rec => rec.OrganizationId).ToList();


            // Kiran on 02/12/2015 under the guidance of rajesh
            List<SystemUsersOrganizations> usersList;
            if (@Session["RoleName"].ToString().Equals("Client") || @Session["RoleName"].ToString().Equals("Super Client"))
            {
                //if (@Session["RoleName"].ToString().Equals("Super Client"))
                //{

                //    var Subclients = entityDB.SubClients.Where(r => currentUserOrg.Contains(r.SuperClientId)&&r.IsActive==true).Select(r => r.ClientId).ToList();
                //    currentUserOrg.AddRange(Subclients);

                //}
                //usersList = entityDB.SystemUsersOrganizations.Where(record => currentUserOrg.Contains(record.OrganizationId)).ToList();
                usersList = entityDB.SystemUsersOrganizations.Where(record => record.OrganizationId == clientId).ToList();
            }
            else
            {
                var empTrainingRoleId = EmployeeRole.EmployeeRoleId;
                // var empTrainingRoleId = entityDB.SystemRoles.FirstOrDefault(rec => rec.RoleName == "Employee Training").RoleId;
                usersList = entityDB.SystemUsersOrganizations.Where(record => currentUserOrg.Contains(record.OrganizationId) && record.SystemUsersOrganizationsRoles.Where(rec => empTrainingRoleId.Contains(rec.SysRoleId)).Count() == 0).ToList();
            }
            ViewBag.clients = usersList.GroupBy(r => r.Organizations.Name).ToList();
            List<LocalUsersList> finalUsersList = new List<LocalUsersList>();
            foreach (var user in usersList)
            {
                LocalUsersList userDetails = new LocalUsersList();

                if (user.SystemUsers.Contacts.Count() != 0)
                    userDetails.Name = user.SystemUsers.Contacts[0].LastName + ", " + user.SystemUsers.Contacts[0].FirstName;
                else
                    userDetails.Name = user.SystemUsers.Email;

                userDetails.UserId = user.UserId;
                userDetails.Email = user.SystemUsers.Email;
                userDetails.OrgName = user.Organizations.Name;
                userDetails.OrganizationType = user.Organizations.OrganizationType;
                userDetails.UserStatus = user.SystemUsers.UserStatus;
                userDetails.IsSubClientUser = false;
                if (@Session["RoleName"].ToString().Equals("Super Client"))
                {
                    userDetails.IsSubClientUser = entityDB.SubClients.FirstOrDefault(r => r.ClientId == user.OrganizationId) != null ? true : false;
                }
                //userDetails.contacts = user.SystemUsers.Contacts;

                finalUsersList.Add(userDetails);
            }

            var VBclientsList = finalUsersList.OrderBy(r => r.OrgName).ThenBy(r => r.Name).ToList();
            if (!string.IsNullOrEmpty(filters))
            {
                VBclientsList = finalUsersList.Where(r => r.Email.ToLower().Contains(filters.ToLower())).ToList();
            }
            var Data = VBclientsList.Skip((request.PageSize * (request.Page - 1))).Take(request.PageSize).ToList();
            var result = new DataSourceResponse()
            {
                Total = VBclientsList.Count(),
                Data = Data
            };
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        /*
           Created Date:  06/26/2013   Created By: Kiran Talluri
           Purpose: To display the Client Users List 
       */
        //[SessionExpire]
        //[HeaderAndSidebar(isPermissionRequired = true, headerName = "MyAccount", sideMenuName = "SearchUser", ViewName = "ClientOrVendorUsersList")]
        //     public ActionResult ClientOrVendorUsersList(long clientId=0,int page = 0)
        //     {
        //         if(clientId==0 && SSession.Role != OrganizationType.Admin)
        //         {
        //             clientId = SSession.OrganizationId;
        //         }
        //         const int PageSize =10;
        //         var varorgId = SSession.OrganizationId;
        //         var varOrgList = _clientBusiness.GetSubClients(varorgId, true).Select(r => new SelectListItem() { Value = r.Key, Text = r.Value,Selected=(r.Key== clientId.ToString()) });
        //         varOrgList.ToList();
        //         ViewBag.VBSubClients = varOrgList;
        //         //return this.View(data);
        //         Guid currentUserId = new Guid(Session["UserId"].ToString());
        //         var currentUserOrg = entityDB.SystemUsersOrganizations.Where(record => record.UserId == currentUserId).Select(rec => rec.OrganizationId).ToList();


        //         // Kiran on 02/12/2015 under the guidance of rajesh
        //         List<SystemUsersOrganizations> usersList;
        //         if (@Session["RoleName"].ToString().Equals("Client") || @Session["RoleName"].ToString().Equals("Super Client"))
        //         {
        //             //if (@Session["RoleName"].ToString().Equals("Super Client"))
        //             //{

        //             //    var Subclients = entityDB.SubClients.Where(r => currentUserOrg.Contains(r.SuperClientId)&&r.IsActive==true).Select(r => r.ClientId).ToList();
        //             //    currentUserOrg.AddRange(Subclients);

        //             //}
        //             //usersList = entityDB.SystemUsersOrganizations.Where(record => currentUserOrg.Contains(record.OrganizationId)).ToList();
        //             usersList = entityDB.SystemUsersOrganizations.Where(record =>  record.OrganizationId == clientId).ToList();
        //         }
        //         else
        //         {
        //             var empTrainingRoleId = entityDB.SystemRoles.FirstOrDefault(rec => rec.RoleName == "Employee Training").RoleId;
        //             usersList = entityDB.SystemUsersOrganizations.Where(record => currentUserOrg.Contains(record.OrganizationId) && record.SystemUsersOrganizationsRoles.Where(rec => rec.SysRoleId == empTrainingRoleId).Count() == 0).ToList();
        //         }
        //         ViewBag.clients = usersList.GroupBy(r => r.Organizations.Name).ToList();
        //         List<LocalUsersList> finalUsersList = new List<LocalUsersList>();
        //         foreach (var user in usersList)
        //         {
        //             LocalUsersList userDetails = new LocalUsersList();

        //             if (user.SystemUsers.Contacts.Count() != 0)
        //                 userDetails.Name = user.SystemUsers.Contacts[0].LastName + ", " + user.SystemUsers.Contacts[0].FirstName;
        //             else
        //                 userDetails.Name = user.SystemUsers.Email;

        //             userDetails.UserId = user.UserId;
        //             userDetails.Email = user.SystemUsers.Email;
        //             userDetails.OrgName = user.Organizations.Name;
        //             userDetails.OrganizationType = user.Organizations.OrganizationType;
        //             userDetails.UserStatus = user.SystemUsers.UserStatus;
        //             userDetails.IsSubClientUser = false;
        //             if (@Session["RoleName"].ToString().Equals("Super Client"))
        //             {
        //                 userDetails.IsSubClientUser = entityDB.SubClients.FirstOrDefault(r => r.ClientId == user.OrganizationId) != null ? true : false;
        //             }
        //             userDetails.contacts = user.SystemUsers.Contacts;

        //             finalUsersList.Add(userDetails);
        //         }

        //         var VBclientsList = finalUsersList.OrderBy(r=>r.OrgName).ThenBy(r=>r.Name);

        //         var count = VBclientsList.Count();
        //         ViewBag.Count = count;
        //       // ViewBag.VBclientsList = VBclientsList.OrderBy(rec => rec.Name).ToList();
        //         var data = VBclientsList.Skip(page * PageSize).Take(PageSize).ToList();
        //// var  HasClient = data.Select(r => r.OrgName).Distinct().ToList();
        //         ViewBag.MaxPage = (count / PageSize) - (count % PageSize == 0 ? 1 : 0);
        //        ViewBag.VBclientsList = data.ToList();
        //         ViewBag.clientId = clientId;
        //        // ViewBag.HasClient = HasClient;
        //        ViewBag.Page = page;
        //         return View();
        //     }

        public ActionResult GetClientTemplates(long ClientId)
        {
            return Json(_clientBusiness.GetClientTemplates(ClientId), JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetFeeTemplates(long ClientId)
        {
            return Json(_clientBusiness.GetFeeTemplates(ClientId), JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetClients()
        {
            IEnumerable<KeyValuePair<string, string>> result = new List<KeyValuePair<string, string>>();
            if (SSession.Role.Equals(OrganizationType.SuperClient))
            {
                var currentOrg = _organizationBusiness.GetOrganization(SSession.OrganizationId);
                var info = result.ToList(); info.Add(new KeyValuePair<string, string>(currentOrg.OrganizationID.ToString(), currentOrg.Name));
                result = info;
            }
            else result = _clientBusiness.GetClients().Where(r => SSession.Role.Equals("Admin") || r.Key == SSession.OrganizationId.ToString());
            return Json(result);
        }
        //DV:RP DT:06-26-2013
        //Desc:To create a client dash board
        //[SessionExpire(lastPageUrl = "~/ClientDashboard/ClientDashboard")]
        [HeaderAndSidebar(headerName = "DashBoard", sideMenuName = "")]
        [SessionExpire]
        public ActionResult ClientDashboard()
        {
            Guid guidUserId = (Guid)Session["UserId"];
            var varClientOrgList = from sysOrgList in entityDB.SystemUsersOrganizations.Where(m => m.UserId == guidUserId).ToList()
                                   select new SelectListItem
                                   {
                                       Text = sysOrgList.Organizations.Name,
                                       Value = sysOrgList.OrganizationId.ToString(),

                                   };
            ViewBag.VBClientOrgList = varClientOrgList.ToList();
            //ViewBag.VBOrgId = ViewBag.varClientOrgList.FirstOrDefault().Value;

            ViewBag.LogoPath = ConfigurationSettings.AppSettings["LogoUploadPath"].ToString() + "/";//Rajesh on 08/09/2013

            //Rajesh on 09/09/2013
            var clientId = Convert.ToInt64(Session["currentOrgId"]);
            var totalPrequalifications = entityDB.Prequalification.Where(m => m.PrequalificationStatusId == 5 && m.ClientId == clientId).OrderByDescending(rec => rec.PrequalificationSubmit).ToList();//Rajesh on 1/21/2014
            var varPrequalificationList = (from prequalifications in totalPrequalifications.Take(30)
                                           select new LocalPrequalification
                                           {
                                               HasNext = totalPrequalifications.Count() > 30,
                                               HasPrevious = false,
                                               IndicationTextClient = prequalifications.PrequalificationStatus.IndicationTextClient,
                                               PageNumber = 0,
                                               PrequalificationCreate = prequalifications.PrequalificationCreate,
                                               PrequalificationFinish = prequalifications.PrequalificationFinish,
                                               PrequalificationId = prequalifications.PrequalificationId,
                                               PrequalificationStart = prequalifications.PrequalificationStart,
                                               PrequalificationSubmit = prequalifications.PrequalificationSubmit,
                                               VendorName = prequalifications.Vendor.Name
                                           }).ToList();

            ViewBag.VBPrequalificationList = varPrequalificationList;

            // Kiran on 9/26/2014
            var clientReviewPrequalifications = entityDB.Prequalification.Where(m => m.PrequalificationStatusId == 16 && m.ClientId == clientId).OrderByDescending(rec => rec.PrequalificationSubmit).ToList();
            var clientReviewPrequalificationList = (from prequalifications in clientReviewPrequalifications.Take(30)
                                                    select new LocalPrequalification
                                                    {
                                                        HasNext = clientReviewPrequalifications.Count() > 30,
                                                        HasPrevious = false,
                                                        IndicationTextClient = prequalifications.PrequalificationStatus.IndicationTextClient,
                                                        PageNumber = 0,
                                                        PrequalificationCreate = prequalifications.PrequalificationCreate,
                                                        PrequalificationFinish = prequalifications.PrequalificationFinish,
                                                        PrequalificationId = prequalifications.PrequalificationId,
                                                        PrequalificationStart = prequalifications.PrequalificationStart,
                                                        PrequalificationSubmit = prequalifications.PrequalificationSubmit,
                                                        VendorName = prequalifications.Vendor.Name
                                                    }).ToList();

            ViewBag.VBClientReviewPrequalificationList = clientReviewPrequalificationList;
            // Ends<<<

            //Rajesh on 09/06/2013 >>>
            //var vendorsCount = entityDB.VendorInviteDetails.Where(rec => rec.ClientId == clientId).ToList().Count();
            //var Subclients = _clientBusiness.GetSubClients(clientId).Select(r => long.Parse(r.Key)).ToList();
            //var invitedVendors = (from vendors in entityDB.VendorInviteDetails.Where(rec => rec.ClientId == clientId|| Subclients.Contains(rec.ClientId)).ToList().OrderByDescending(rec => rec.InvitationDate).Take(30) // Kiran on 9/29/2014
            //                      select new LocalinviteVendor
            //                      {
            //                          HasNext = vendorsCount > 30,
            //                          HasPrevious = false,
            //                          PageNumber = 0,
            //                          VendorCompanyName = vendors.VendorCompanyName,
            //                          FullName = vendors.InvitationSentByUser == null ? "FIRST, VERIFY." : (vendors.SystemUsers.Contacts.FirstOrDefault() != null ? vendors.SystemUsers.Contacts[0].LastName + ", " + vendors.SystemUsers.Contacts[0].FirstName : vendors.SystemUsers.Email), // Kiran on 03/18/2015
            //                          //LastName = vendors.VendorLastName,
            //                          EmailAddress = vendors.VendorEmail,
            //                          InvitationDate = vendors.InvitationDate
            //                      }).ToList();

            var invitedVendors = _clientBusiness.GetInvitedVendors(clientId, "", 0, false);

            ViewBag.invitedVendors = invitedVendors;

            List<LocalNotifications> notifications = new List<LocalNotifications>();



            var orgNSent = entityDB.OrganizationsNotificationsSent.Where(rec => rec.ClientId == clientId && rec.SetupType == 1).Select(rec => rec.NotificationId).ToList();
            //Rajesh on 2/13/2015
            var orgNotificationsEmailLogs = entityDB.OrganizationsNotificationsEmailLogs.Where(rec => rec.NotificationType == 0 && orgNSent.Contains(rec.NotificationId)).OrderByDescending(rec => rec.EmailSentDateTime).Take(31);
            foreach (var orglog in orgNotificationsEmailLogs.ToList())
            {
                LocalNotifications locNotification = new LocalNotifications();

                if (notifications.Count >= 30)
                    break;

                //Rajesh on 12/12/2013
                locNotification.HasNext = false;
                locNotification.HasPrevious = false;
                locNotification.PageNumber = 0;
                locNotification.EmailSentDate = orglog.EmailSentDateTime; // kiran on 9/8/2014
                //<<Ends


                locNotification.ClientName = orglog.OrganizationsNotificationsSent.Client.Name;

                //Rajesh on 12/12/2013
                locNotification.VendorName = orglog.Vendor.Name + "";//entityDB.OrganizationsNotificationsEmailLogs.FirstOrDefault().Vendor.Name;//
                //Ends
                locNotification.Purpose = "";
                OrganizationsEmailSetupNotifications orgEmailSetupNotification = entityDB.OrganizationsEmailSetupNotifications.FirstOrDefault(rec => rec.OrgEmailSetupId == orglog.OrganizationsNotificationsSent.SetupTypeId);
                if (orgEmailSetupNotification != null)
                {
                    if (orgEmailSetupNotification.SetupType == 0)//Document
                    {
                        locNotification.Purpose = entityDB.DocumentType.Find(orgEmailSetupNotification.SetupTypeId).DocumentTypeName;
                    }
                    else if (orgEmailSetupNotification.SetupType == 1)//Status
                    {
                        locNotification.Purpose = entityDB.PrequalificationStatus.Find(orgEmailSetupNotification.SetupTypeId).PrequalificationStatusName;
                    }
                }

                //Dec 4th
                locNotification.Comments = orglog.Comments;
                if (orglog.Comments != null)
                {
                    if (orglog.Comments.Length > 30)
                        locNotification.SmallComment = orglog.Comments.Substring(0, 30);
                    else
                        locNotification.SmallComment = orglog.Comments;
                }
                else
                    locNotification.SmallComment = "";

                notifications.Add(locNotification);
            }

            if (orgNotificationsEmailLogs.Count() > 30)
            {
                notifications.ForEach(rec => rec.HasNext = true);
            }
            ViewBag.NotificationsSent = notifications;
            ViewBag.NotificationCount = entityDB.OrganizationsNotificationsEmailLogs.Count(rec => rec.NotificationType == 0 && orgNSent.Contains(rec.NotificationId) && rec.ReadByClient == 0);
            var PQStatusesDescription = new List<PrequalificationStatus>();
            PQStatusesDescription = entityDB.PrequalificationStatus.Where(rec => rec.ShowInDashBoard == true).OrderBy(r => r.DisplayOrder).ToList();

            PrequalificationStatus multipleStatus = new PrequalificationStatus();
            multipleStatus.PrequalificationStatusName = "Multiple";
            multipleStatus.Description = "Vendor has a different status for multiple locations";
            multipleStatus.DisplayOrder = 6;
            PQStatusesDescription.Add(multipleStatus);

            ViewBag.PreQualificationsStatus = PQStatusesDescription.OrderBy(r => r.DisplayOrder).ToList();
            //Ends<<<<

            return View();
        }

        [HttpPost]

        public ActionResult InviteVendorsPartialView(int pageNumber, string SearchEmail, int invitationType)//0-New Vendor 1-Expired Vendor 2-OutsideVendor
        {
            ViewBag.emaildata = SearchEmail;
            var clientId = Convert.ToInt64(Session["currentOrgId"]);


            //var vendorsCount = vendors_list.OrderByDescending(rec => rec.InvitationId).Skip(pageNumber * 30).Count(); // Kiran on 9/29/2014
            //var invitedVendors = (from vendors in vendors_list.ToList().OrderByDescending(rec => rec.InvitationDate).Skip(pageNumber * 30).Take(30 * (pageNumber + 1)) // Kiran on 9/29/2014
            //                      select new LocalinviteVendor
            //                      {
            //                          HasNext = vendorsCount > 30,
            //                          HasPrevious = pageNumber != 0,
            //                          PageNumber = pageNumber,
            //                          VendorCompanyName = vendors.VendorCompanyName,
            //                          FullName = vendors.InvitationSentByUser == null ? "InfusionSoft" : (vendors.SystemUsers.Contacts.FirstOrDefault() != null ? vendors.SystemUsers.Contacts[0].LastName + ", " + vendors.SystemUsers.Contacts[0].FirstName : vendors.SystemUsers.Email), // kiran on 03/18/2015
            //                          //LastName = vendors.VendorLastName,
            //                          EmailAddress = vendors.VendorEmail,
            //                          InvitationDate = vendors.InvitationDate
            //                      }).ToList();
            var invitedVendors = _clientBusiness.GetInvitedVendors(clientId, SearchEmail, invitationType, true);
            ViewBag.invitedVendors = invitedVendors;
            return PartialView();
        }

        [HttpPost]
        public ActionResult PrequalificationPartialView(int pageNumber, int? type) // Kiran on 9/26/2014 // type 0 - Submitted, 1 - Client Review
        {
            ViewBag.type = type; // Kiran on 9/26/2014
            var clientId = Convert.ToInt64(Session["currentOrgId"]);
            // Kiran on 9/26/2014
            if (type == 0)
            {
                var totalPrequalifications = entityDB.Prequalification.Where(m => m.PrequalificationStatusId == 5 && m.ClientId == clientId).OrderByDescending(rec => rec.PrequalificationSubmit).Skip(pageNumber * 30).ToList();//Rajesh on 1/21/2014
                var varPrequalificationList = (from prequalifications in entityDB.Prequalification.Where(m => m.PrequalificationStatusId == m.PrequalificationStatus.PrequalificationStatusId && (m.PrequalificationStatus.PrequalificationStatusId == 5) && m.ClientId == clientId).ToList().OrderByDescending(rec => rec.PrequalificationSubmit).ToList().Skip(pageNumber * 30).Take(30 * (pageNumber + 1))
                                               select new LocalPrequalification
                                               {
                                                   HasNext = totalPrequalifications.Count() > 30,
                                                   HasPrevious = pageNumber != 0,
                                                   IndicationTextClient = prequalifications.PrequalificationStatus.IndicationTextClient,
                                                   PageNumber = pageNumber,
                                                   PrequalificationCreate = prequalifications.PrequalificationCreate,
                                                   PrequalificationFinish = prequalifications.PrequalificationFinish,
                                                   PrequalificationId = prequalifications.PrequalificationId,
                                                   PrequalificationStart = prequalifications.PrequalificationStart,
                                                   PrequalificationSubmit = prequalifications.PrequalificationSubmit,
                                                   VendorName = prequalifications.Vendor.Name
                                               }).ToList();

                ViewBag.VBPrequalificationList = varPrequalificationList;

            }
            else
            {
                var clientReviewPrequalifications = entityDB.Prequalification.Where(m => m.PrequalificationStatusId == 16 && m.ClientId == clientId).OrderByDescending(rec => rec.PrequalificationSubmit).Skip(pageNumber * 30).ToList();
                var varClientReviewPrequalificationList = (from prequalifications in entityDB.Prequalification.Where(m => m.PrequalificationStatusId == m.PrequalificationStatus.PrequalificationStatusId && (m.PrequalificationStatus.PrequalificationStatusId == 16 && m.ClientId == clientId)).ToList().OrderByDescending(rec => rec.PrequalificationSubmit).ToList().Skip(pageNumber * 30).Take(30 * (pageNumber + 1))
                                                           select new LocalPrequalification
                                                           {
                                                               HasNext = clientReviewPrequalifications.Count() > 30,
                                                               HasPrevious = pageNumber != 0,
                                                               IndicationTextClient = prequalifications.PrequalificationStatus.IndicationTextClient,
                                                               PageNumber = pageNumber,
                                                               PrequalificationCreate = prequalifications.PrequalificationCreate,
                                                               PrequalificationFinish = prequalifications.PrequalificationFinish,
                                                               PrequalificationId = prequalifications.PrequalificationId,
                                                               PrequalificationStart = prequalifications.PrequalificationStart,
                                                               PrequalificationSubmit = prequalifications.PrequalificationSubmit,
                                                               VendorName = prequalifications.Vendor.Name
                                                           }).ToList();

                ViewBag.VBPrequalificationList = varClientReviewPrequalificationList;
            }
            // Ends<<<
            return PartialView();
        }

        [HttpPost]
        public ActionResult NotificationsPartialView(int pageNumber)//Every Page contains 30 notifications
        {

            var clientId = Convert.ToInt64(Session["currentOrgId"]);
            List<LocalNotifications> notifications = new List<LocalNotifications>();

            var NumberOfItemsToSkip = pageNumber * 30;

            var orgNSent = entityDB.OrganizationsNotificationsSent.Where(rec => rec.ClientId == clientId && rec.SetupType == 1).Select(rec => rec.NotificationId).ToList();
            //Rajesh on 2/13/2015
            var orgNotificationsEmailLogs = entityDB.OrganizationsNotificationsEmailLogs.Where(rec => rec.NotificationType == 0 && orgNSent.Contains(rec.NotificationId)).OrderByDescending(rec => rec.EmailSentDateTime).Skip(NumberOfItemsToSkip).Take(31);
            foreach (var orglog in orgNotificationsEmailLogs.ToList())
            {
                LocalNotifications locNotification = new LocalNotifications();

                if (notifications.Count >= 30)
                    break;

                //Rajesh on 12/12/2013
                locNotification.HasNext = false;
                locNotification.HasPrevious = pageNumber != 0;
                locNotification.PageNumber = pageNumber;
                locNotification.EmailSentDate = orglog.EmailSentDateTime; // kiran on 9/8/2014
                //<<Ends


                locNotification.ClientName = orglog.OrganizationsNotificationsSent.Client.Name;

                //Rajesh on 12/12/2013
                locNotification.VendorName = orglog.Vendor.Name + "";//entityDB.OrganizationsNotificationsEmailLogs.FirstOrDefault().Vendor.Name;//
                //Ends
                locNotification.Purpose = "";
                OrganizationsEmailSetupNotifications orgEmailSetupNotification = entityDB.OrganizationsEmailSetupNotifications.FirstOrDefault(rec => rec.OrgEmailSetupId == orglog.OrganizationsNotificationsSent.SetupTypeId);
                if (orgEmailSetupNotification != null)
                {
                    if (orgEmailSetupNotification.SetupType == 0)//Document
                    {
                        locNotification.Purpose = entityDB.DocumentType.Find(orgEmailSetupNotification.SetupTypeId).DocumentTypeName;
                    }
                    else if (orgEmailSetupNotification.SetupType == 1)//Status
                    {
                        locNotification.Purpose = entityDB.PrequalificationStatus.Find(orgEmailSetupNotification.SetupTypeId).PrequalificationStatusName;
                    }
                }

                //Dec 4th
                locNotification.Comments = orglog.Comments;
                if (orglog.Comments != null)
                {
                    if (orglog.Comments.Length > 30)
                        locNotification.SmallComment = orglog.Comments.Substring(0, 30);
                    else
                        locNotification.SmallComment = orglog.Comments;
                }
                else
                    locNotification.SmallComment = "";

                notifications.Add(locNotification);
            }

            if (orgNotificationsEmailLogs.Count() > 30)
            {
                notifications.ForEach(rec => rec.HasNext = true);
            }
            ViewBag.NotificationsSent = notifications;
            ViewBag.NotificationCount = entityDB.OrganizationsNotificationsEmailLogs.Where(rec => rec.NotificationType == 0 && orgNSent.Contains(rec.NotificationId) && rec.ReadByClient == 0).Count();
            return PartialView();

        }

        [SessionExpire]
        [HeaderAndSidebar(headerName = "DashBoard", sideMenuName = "")]
        public string markAllNotificationsAsRead()
        {
            string statusStr = "updateSuccess";

            entityDB.Database.ExecuteSqlCommand(
                "update OrganizationsNotificationsEmailLogs set ReadByClient =1 where ReadByClient=0");
            //List<OrganizationsNotificationsEmailLogs> logs = entityDB.OrganizationsNotificationsEmailLogs.Where(log => log.ReadByClient == 0).ToList();
            //foreach (var emailLog in logs)
            //{
            //    emailLog.ReadByClient = 1;
            //    entityDB.Entry(emailLog).State = EntityState.Modified;

            //    statusStr = "updateSuccess";
            //}
            //entityDB.SaveChanges();

            ViewBag.NotificationCount = 0;

            return statusStr;
        }
        [HeaderAndSidebar(headerName = "DashBoard", sideMenuName = "")]
        public ActionResult ClientInvites()
        {
            return View();
        }

        public String getPiChartURL(long clientId)
        {
            String strUrl = "";
            var hasPowerbiUrl = _clientBusiness.GetPowerBiReportByName(clientId, SSession.UserId,SSession.Role,Resources.Resources.Report_Client_dashboard);
            if (!string.IsNullOrEmpty(hasPowerbiUrl))
                return hasPowerbiUrl;
            Session["currentOrgId"] = clientId;
            var ERIPqs = entityDB.PrequalificationClient.SqlQuery("select * from PrequalificationClient where ','+ClientIds+',' like @p0", "%," + clientId + ",%")
                           .Select(r => r.PQId).ToList();
            var query_prequalification = from preQualification in entityDB.LatestPrequalification.Where(rec => rec.PrequalificationStatusId > 2
                                         && (rec.ClientId == clientId || ERIPqs.Contains(rec.PrequalificationId))
                                         && rec.PrequalificationStatus.ShowInDashBoard == true)
                                         group preQualification by preQualification.PrequalificationStatus into g
                                         select new
                                         {
                                             Name = g.Key.PrequalificationStatusName,
                                             Count = g.Count(),
                                             Color = g.Key.Color
                                         };


            //formula for chg 100/max * (diff btw intervels)
            //http://chart.apis.google.com/chart?cht=bhs&chs=400x125&chco=80C65A|224499|FF0000|F0F0F0&chxt=x&chxr=0,0,1000,100&chd=t:100,1000,250,500&chdl=No%20Response|Prequalified&chbh=width&chds=0,1000
            var url_googleChart = "http://chart.apis.google.com/chart?cht=bhs&chs=650x450&chxt=x,y&chdlp=b&chm=N,000000,0,-1,11&chg=20,0,0.5&chbh=24,10,10&chxs=1,000000,12,1,_|0,000000,12,1,_";
            var chartData = "&chd=t:";
            var chartLegend = "";//Rajesh on 1/3/2014
            var chartColor = "&chco=";

            var max = 1;
            try
            {
                max = query_prequalification.Max(rec => rec.Count);
            }
            catch (Exception ee) { }

            max = max + (5 - max % 5);
            var chartXR = "&chxr=0,0," + max + "," + (float)max / 5;
            var chartDS = "&chds=0," + max;
            var preQualificationStatusNumber = (from preQualifications in entityDB.Prequalification.Where(preQualifications => preQualifications.ClientId == clientId || ERIPqs.Contains(preQualifications.PrequalificationId)).ToList()
                                                group preQualifications by preQualifications.PrequalificationStatusId into g
                                                select g.Key).ToList();
            var StatusNotInPre = entityDB.PrequalificationStatus.Where(rec => rec.PrequalificationStatusId > 2 && rec.ShowInDashBoard == true && !preQualificationStatusNumber.Contains(rec.PrequalificationStatusId));
            foreach (var data in StatusNotInPre)
            {
                chartData += 0 + ",";
                chartLegend += data.PrequalificationStatusName + "|";
                chartColor += data.Color + "|";
            }
            if (query_prequalification != null)
            {
                foreach (var data in query_prequalification.OrderBy(row => row.Count))
                {
                    chartData += data.Count + ",";
                    chartLegend += data.Name + "|";
                    chartColor += data.Color + "|";
                }
            }
            var chartLegendsWithParamsInReverse = "&chxl=1:|";
            foreach (var legend in chartLegend.Substring(0, chartLegend.Length - 1).Split('|').Reverse())
            {
                chartLegendsWithParamsInReverse += legend + "|";
            }
            chartData = chartData.Substring(0, chartData.Length - 1);
            chartLegendsWithParamsInReverse = chartLegendsWithParamsInReverse.Substring(0, chartLegendsWithParamsInReverse.Length - 1);
            chartColor = chartColor.Substring(0, chartColor.Length - 1);
            strUrl = url_googleChart + chartData + chartLegendsWithParamsInReverse + chartColor + chartXR + chartDS;
            return strUrl;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="vendorName"></param>
        /// <param name="city"></param>
        /// <param name="state"></param>
        /// <param name="biddings"></param>
        /// <param name="type">
        /// 0- local Vendors/Expired Vendor
        /// 1- Global Vendors/Out side N/w
        /// </param>
        /// <returns></returns>
        public ActionResult GetVendorsForInvite(int draw, int start, int length, string vendorName, string city, string state, string biddings,
            string proficiency, string claycoBiddings, int type, int status, long client, string Country, bool ERIOnly = false)
        {
            int orderBy = Convert.ToInt32(Request.QueryString["order[0][column]"]);
            bool asc = Request.QueryString["order[0][dir]"] == "asc";

            bool ERIsubclients = _clientBusiness.IsClientPartOfEri(client);
            var RoleName = SSession.Role;
            if (ERIsubclients == true && ERIOnly == true) ERIOnly = true; else ERIOnly = false;
            {
                int recordsFiltered = 0;
                var bids = new List<long>();
                if (!string.IsNullOrEmpty(biddings))
                    bids = biddings.Split(',').Select(rec => Convert.ToInt64(rec)).ToList();
                var claycobids = new List<long>();
                if (!string.IsNullOrEmpty(claycoBiddings))
                    claycobids = claycoBiddings.Split(',').Select(rec => Convert.ToInt64(rec)).ToList();
                var prof = new List<long>();
                if (!string.IsNullOrEmpty(proficiency))
                    prof = proficiency.Split(',').Select(rec => Convert.ToInt64(rec)).ToList();
                var info = type == 0
                    ? _vendorBusiness.GetClientVendors(SSession.UserId, false, false, orderBy, 0, asc, out recordsFiltered, draw, start, length, client, vendorName,
                        bids, claycobids, prof, city,
                        state, Country, false, 2)
                    : _vendorBusiness.GetClientVendors(SSession.UserId, ERIOnly, !ERIsubclients, orderBy, 0, asc, out recordsFiltered, draw, start, length, client, vendorName,
                        bids, claycobids, prof, city,
                        state, Country, true);
                var result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = info.Count,
                    recordsFiltered = recordsFiltered,
                    data = info
                }, JsonRequestBehavior.AllowGet);
                return result;
            }
            return null;
        }

    }
}
