﻿/*
Page Created Date:  29/04/2013
Created By: SivA Bommisetty
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FVGen3.Domain.Entities;
using FVGen3.Domain.Concrete;
using FVGen3.Domain.Abstract;
using System.IO;
using System.Configuration;
using FVGen3.WebUI.Annotations;
using System.Text.RegularExpressions;
using FVGen3.BusinessLogic.Interfaces;
using Resources;
using FVGen3.Domain.LocalModels;
using System.Net;
using System.Data.Entity;
using System.Threading.Tasks;

namespace FVGen3.WebUI.Controllers
{
    public class EmailTemplatesController : BaseController
    {
        //
        // GET: /EmailTemplates/

        private ISystemUsersRepository systemUsersRepository;
        private IEmailTemplateBusiness _emailTemplateRepository;
        private IClientBusiness _clientRepository;
        EFDbContext efdbContext;

        //Constructor to initialize EFDbcontext
        public EmailTemplatesController(ISystemUsersRepository systemUsersRepository, IEmailTemplateBusiness emailTemplateRepository, IClientBusiness clientRepository)
        {
            this.systemUsersRepository = systemUsersRepository;
            _emailTemplateRepository = emailTemplateRepository;
            _clientRepository = clientRepository;
            efdbContext = new EFDbContext();
        }

        //to display the list of EmailTemplates,
        //isSuccess is false, bcoz it is used to display success messsage at the top of view, when save successeded.
        //returns List of emailTemplates from efdbcontext.
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "MyAccount", sideMenuName = "EmailTemplates", ViewName = "EmailTemplatesList")]
        [SessionExpire]
        public async Task<ActionResult> EmailTemplatesList(int? messageType)//0 or null - normal, 1 - updated successfully, 2-unable to delete
        {
            ViewBag.messageType = messageType;
            var Languages =await _clientRepository.GetLanguagesAsync();
            List<SelectListItem> languages = new List<SelectListItem>();
            Languages.ForEach(s => languages.Add(new SelectListItem() { Text = s.Value, Value = s.Key }));

            //var clientOrg = clientOrg.ToList();
            SelectListItem defaultItem = new SelectListItem();
            defaultItem.Text = LocalConstants.Default_Value;
            defaultItem.Value = "0";
            languages.Insert(0, defaultItem);
            ViewBag.Languages = languages;
            return View(efdbContext.EmailTemplates.Where(rec => rec.EmailUsedFor == 0).OrderBy(m => m.Name).ToList());
        }
        [HttpPost]
        public ActionResult GetEmailData(int EmailTemplateId, string LanguageID)
        {
            return Json(_emailTemplateRepository.GetEmailData(EmailTemplateId, LanguageID), JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult SaveEmailData(EmailTemplates Template)
        {
            var Body = WebUtility.HtmlDecode(Template.EmailBody);
            var EmailTemplate = _emailTemplateRepository.SaveLanguageEmailTemplate(Template.EmailTemplateID, Template.LanguageId, Template.EmailSubject, Body, Template.CommentText);
            return Json(EmailTemplate, JsonRequestBehavior.AllowGet);
        }
        //to delete a template
        //takes templated of the template to be delete
        //returns remaining templates from efdbcontext.
        [HeaderAndSidebar(headerName = "MyAccount", sideMenuName = "EmailTemplates")]
        [SessionExpire]
        [HttpGet]
        public ActionResult deleteTemplate(int templateId)
        {
            EmailTemplates template = efdbContext.EmailTemplates.Find(templateId);
            if (template != null)
            {
                List<OrganizationsEmailSetupNotifications> notifications = efdbContext.OrganizationsEmailSetupNotifications.Where(rec => rec.EmailTemplateID == templateId).ToList();
                var notificationEmailLogs = efdbContext.OrganizationsNotificationsEmailLogs.Where(rec => rec.EmailTemplateId == templateId).ToList();
                if (notifications.Any() || notificationEmailLogs.Any())
                    return Redirect("EmailTemplatesList?messageType=2");
                else
                {
                    efdbContext.EmailTemplates.Remove(template);
                    efdbContext.SaveChanges();
                }

            }

            return Redirect("EmailTemplatesList?messageType=0");
        }

        //to add a new template or edit an existing template
        //takes template of the template to be edited, in case of editing
        //takes -1 as templateid in case adding new template.
        //returns view of template to be add or edit.
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "MyAccount", sideMenuName = "EmailTemplates", ViewName = "AddOrEditEmailTemplate")]
        [SessionExpire]
        public ActionResult AddOrEditEmailTemplate(int templateId)
        {
            var clientList = (from clients in _clientRepository.GetAllClients()
                              select new SelectListItem
                              {
                                  Text = clients.Value,
                                  Value = clients.Key.ToString()

                              }).ToList();

            SelectListItem defaultItem = new SelectListItem();
            defaultItem = new SelectListItem();
            defaultItem.Text = "Generic";
            defaultItem.Value = "-2";
            var clientList_defaultItem = clientList.ToList();
            clientList_defaultItem.Insert(0, defaultItem);
            ViewBag.VBclientList = clientList_defaultItem;

            var templateToAddOrEdit = efdbContext.EmailTemplates.Find(templateId);
            if (templateToAddOrEdit == null)
            {
                templateToAddOrEdit = new EmailTemplates();

                templateToAddOrEdit.EmailTemplateID = templateId;  // Siva on 08/07/2013
            }
            return View(templateToAddOrEdit);
        }

        //to save the changes/additions in adding/editing of a template.
        //takes EmailTemplate Form as argument.
        //returns EmailTemplatesListview with list of templates 
        //if modelState is not valid, returns the same form back.
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "MyAccount", sideMenuName = "EmailTemplates", ViewName = "AddOrEditEmailTemplate")]
        [SessionExpire]
        [HttpPost]
        public ActionResult AddOrEditEmailTemplate(EmailTemplates form)
        {
            Boolean flag_isInsert = false;
            if (ModelState.IsValid)
            {
                var templateToAddOrEdit = efdbContext.EmailTemplates.Find(form.EmailTemplateID);

                if (templateToAddOrEdit == null)
                {
                    Guid guidUserId = (Guid)Session["UserId"];
                    var systemUserLoggedIn = efdbContext.SystemUsers.First(m => m.UserId == guidUserId);

                    var SysUserOrg = systemUserLoggedIn.SystemUsersOrganizations.FirstOrDefault(m => m.SystemUsers.UserId == systemUserLoggedIn.UserId);


                    flag_isInsert = true;
                    templateToAddOrEdit = new EmailTemplates();

                    //templateToAddOrEdit.ClientOrganizationID = SysUserOrg.OrganizationId; // Not Required, commented by Suresh
                    templateToAddOrEdit.InsertDateTime = DateTime.Now;
                }

                templateToAddOrEdit.Name = form.Name;
                templateToAddOrEdit.EmailSubject = form.EmailSubject;
                templateToAddOrEdit.EmailBody = HttpUtility.HtmlDecode(form.EmailBody); // Kiran on 4/18/2014
                templateToAddOrEdit.CommentText = form.CommentText;
                templateToAddOrEdit.UpdateDateTime = DateTime.Now;

                templateToAddOrEdit.ClientOrganizationID = form.ClientOrganizationID;
                if (form.ClientOrganizationID == -2)
                {
                    ModelState.Remove("ClientOrganizationID");
                    templateToAddOrEdit.ClientOrganizationID = null;
                }

                //Nov 27th
                if (form.EmailSubject.Contains("[DocumentName]") || form.EmailSubject.Contains("[ExpirationDate]") || form.EmailBody.Contains("[DocumentName]") || form.EmailBody.Contains("[ExpirationDate]")) // kiran on 03/12/2015
                    templateToAddOrEdit.IsDocumentSpecific = true;
                else
                    templateToAddOrEdit.IsDocumentSpecific = false;
                //End Nov 27th


                if (flag_isInsert)
                    efdbContext.EmailTemplates.Add(templateToAddOrEdit);
                else
                    efdbContext.Entry(templateToAddOrEdit).State = EntityState.Modified;

                efdbContext.SaveChanges();

                ViewBag.isSuccess = true;
                //return RedirectToAction("EmailTemplatesList", efdbContext.EmailTemplates);
                return Redirect("EmailTemplatesList?messageType=1");  // Siva on 11/29/2013
            }

            // Otherwise, reshow form
            return View(form);
        }

        //to display addAttachmentView 
        //takes templated of the template to which the attachment to be added.
        //returns addAttachmentView.
        [SessionExpireForView]
        [HeaderAndSidebar(headerName = "MyAccount", sideMenuName = "EmailTemplates", ViewName = "AddEmailAttachment")]
        public ActionResult AddEmailAttachment(int templateId)
        {
            LocalEmailTemplateAttachments template = new LocalEmailTemplateAttachments();
            template.EmailTemplateID = templateId;
            return View(template);
        }

        //to save attachment to the template
        //takes file to be attached, and LocalEmailTemplateAttachments model as arguments
        //saves the file and attachment record and 
        //returns uploadFinishedView
        [SessionExpireForView]
        [HttpPost]
        [HeaderAndSidebar(headerName = "MyAccount", sideMenuName = "EmailTemplates", ViewName = "AddEmailAttachment")]
        public ActionResult AddEmailAttachment(HttpPostedFileBase file, LocalEmailTemplateAttachments localAttachment)
        {

            if (file != null && file.ContentLength > 0)
            {

                //save record in database...
                EmailTemplateAttachments attachment = new EmailTemplateAttachments();
                attachment.FileExtension = Path.GetExtension(file.FileName);
                attachment.FileName = Path.GetFileName(file.FileName); // Siva on 02/16/2015
                Guid guidUserId = (Guid)Session["UserId"];

                attachment.UploadedByUserId = guidUserId;
                attachment.InsertDateTime = DateTime.Now;
                attachment.UpdateDateTime = DateTime.Now;
                attachment.TemplateID = localAttachment.EmailTemplateID;

                efdbContext.EmailTemplateAttachments.Add(attachment);
                efdbContext.SaveChanges();

                //SAVE file
                var fileName = Path.GetFileName(file.FileName);
                Directory.CreateDirectory(Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["EmailTemplateAttachmentsPath"])));
                var attachId = attachment.AttachmentId + "";
                var path = Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["EmailTemplateAttachmentsPath"]), localAttachment.EmailTemplateID + attachId + fileName);
                file.SaveAs(path);

                return View("uploadFinishedView", localAttachment);
            }
            else
            {
                ModelState.AddModelError("FileName", "*");
            }

            return View();
        }


        //to display list attachments of a template in-case of it's editing.
        //takes templateid of the template, which attachments to be returned
        //returns partialView of attachmentslist.
        [SessionExpireForView]
        [HttpGet]
        [OutputCache(Duration = 0)]
        public ActionResult EmailAttachmentsList(int templateId)
        {
            int templateIdLocal = templateId;
            EmailTemplates templates = (EmailTemplates)efdbContext.EmailTemplates.FirstOrDefault(m => m.EmailTemplateID == templateIdLocal);
            if (templates == null)
            {
                templates = new EmailTemplates();
            }

            ViewBag.attachmentList = templates.EmailTemplateAttachments;

            return PartialView();
        }

        //to show download alertDialogBox of an attachment
        //takes filename of attachment and templateid of the template to which this attachment belongs to.
        //retuns attachment(File).
        public ActionResult Download(string fileName, string templateId, string attachmentId)
        {
            try
            {
                // kiran on 12/16/2014
                var path = Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["EmailTemplateAttachmentsPath"]), templateId + attachmentId + fileName);

                string invalidChars = Regex.Escape(new string(System.IO.Path.GetInvalidFileNameChars()));
                string invalidReStr = string.Format(@"[{0}]+", invalidChars);
                fileName = Regex.Replace(fileName, invalidReStr, "_").Replace(";", "").Replace(",", "");
                // Ends<<<

                if (System.IO.File.Exists(path))
                {
                    //var fs = System.IO.File.OpenRead(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["EmailTemplateAttachmentsPath"]) + "/" + templateId + attachmentId + fileName);
                    //return File(fs, "file", fileName);
                    var fileToOpen = System.IO.File.OpenRead(path);
                    Response.AppendHeader("Content-Disposition", "inline; filename=" + templateId + attachmentId + fileName);
                    return File(fileToOpen, "application/pdf");
                }
                else
                {

                    //return Json("File does not exist", JsonRequestBehavior.AllowGet);
                    return View("../Companies/DocDownloadFailed");
                }
            }
            catch (Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
                //throw new HttpException(404, @Resources.EmailTemplatesCouldNotFound + fileName);
            }
        }


        //to display deleteattachment (popover) view
        //takes attachmentId of the attachment to be delete
        //returns deleteEmailAttachment View.
        [SessionExpire]
        public ActionResult DeleteEmailAttachment(Guid attachmentId)
        {
            ViewBag.attachmentId = attachmentId;
            return View();
        }

        //to delete an attachment.
        //takes attachmentid of the attachment to be delete and templateid of the template to which this attachment belongs to
        //deletes the attachment file, and record and returns success String.
        [SessionExpireForView]
        [HttpPost]
        public string DeleteEmailAttachment(Guid attachmentId, int templateId)
        {
            EmailTemplateAttachments attachment = efdbContext.EmailTemplateAttachments.Find(attachmentId);
            if (attachment != null)
            {
                var attachId = attachmentId + "";
                //var path = Path.Combine(ConfigurationSettings.AppSettings["EmailTemplateAttachmentsPath"], attachment.TemplateID + attachId + attachment.FileName);
                var path = Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["EmailTemplateAttachmentsPath"]), attachment.TemplateID + attachId + attachment.FileName);

                if (path != null)
                {
                    var template = efdbContext.EmailTemplates.Find(attachment.TemplateID);

                    System.IO.File.Delete(path);

                    efdbContext.EmailTemplateAttachments.Remove(attachment);
                    efdbContext.SaveChanges();

                    return "DeleteSuccess";

                }
            }
            return "Fail";
        }

        [SessionExpire]
        public ActionResult GetEmailTemplates()
        {
            return Json(_emailTemplateRepository.GetGenericEmailTemplates(), JsonRequestBehavior.AllowGet);
        }

        [SessionExpire]
        public ActionResult Index()
        {
            return View();
        }

    }
}
