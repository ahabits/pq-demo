﻿/*
Page Created Date:  07/31/2013
Created By: Siva Bommisetty
Purpose: For Roles and Permissions
Version: 1.0
****************************************************
*/



using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FVGen3.Domain.Entities;
using FVGen3.Domain.Concrete;
using FVGen3.Domain.Abstract;
using FVGen3.WebUI.Annotations;
using FVGen3.WebUI.Utilities;
using Resources;
using FVGen3.BusinessLogic.Interfaces;
using FVGen3.WebUI.Constants;

namespace FVGen3.WebUI.Controllers
{
    public class RolesAndPermissionsController : BaseController
    {
        //
        // GET: /Roles/

        public ActionResult Index()
        {
            return View();
        }

        EFDbContext efdbContext;
        IOrganizationBusiness _org;
        //Constructor to initialize EFDbcontext
        public RolesAndPermissionsController(IOrganizationBusiness _org)
        {
            efdbContext = new EFDbContext();
            this._org = _org;
        }


        ////////////////////////////
        /////////////////////////////
        [SessionExpireForView]
        [HttpGet]
        public ActionResult SystemRolesList()
        {
            //var listSystemRoles = efdbContext.SystemRoles.Where(rec=>rec.RoleName!="Super User").GroupBy(row => row.OrganizationType).ToList();
            var listSystemRoles = efdbContext.SystemRoles.GroupBy(row => row.OrganizationType).ToList();

            ViewBag.listSystemRoles = listSystemRoles;

            return RedirectToAction("SystemRolesListPartial", "RolesAndPermissions");
        }
        public ActionResult OrganizationRoles(string roleId, bool all = true)
        {
            var data = _org.GetOrganizationsWithRole(roleId);
            ViewBag.checkAll = !_org.HasOrgRoles(roleId) && all;
            ViewBag.RoleId = roleId;
            return View(data);
        }
        public ActionResult OrganizationRolesWithExistingData(string roleId)
        {
            var data = _org.GetOrganizationsWithRole(roleId);
            ViewBag.checkAll = false;
            ViewBag.RoleId = roleId;
            return View("OrganizationRoles", data);
        }
        [OutputCache(Duration = 0)]
        public ActionResult SystemRolesListPartial()
        {
            var listSystemRoles = efdbContext.SystemRoles.GroupBy(row => row.OrganizationType).ToList();

            ViewBag.listSystemRoles = listSystemRoles;
            return PartialView();
        }
        //
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "MyAccount", sideMenuName = "RolesAndPermissions", ViewName = "AddSystemRole")]
        [SessionExpire]
        public ActionResult AddSystemRole()
        {
            getStaticOrgTypesAsViewBagElements();

            LocalSystemRoles newRole = new LocalSystemRoles();
            return View(newRole);
        }

        public void getStaticOrgTypesAsViewBagElements()
        {
            var orgTypesLocal = new List<SelectListItem>();

            SelectListItem newOrgType = new SelectListItem();
            newOrgType.Text = "Admin";
            orgTypesLocal.Insert(0, newOrgType);

            newOrgType = new SelectListItem();
            newOrgType.Text = "Client";
            orgTypesLocal.Insert(0, newOrgType);

            newOrgType = new SelectListItem();
            newOrgType.Text = "Vendor";
            orgTypesLocal.Insert(0, newOrgType);

            newOrgType = new SelectListItem();
            newOrgType.Text = OrganizationType.SuperClient;
            orgTypesLocal.Insert(0, newOrgType);
            ViewBag.orgTypes = orgTypesLocal;
        }

        [HeaderAndSidebar(isPermissionRequired = true, headerName = "MyAccount", sideMenuName = "RolesAndPermissions", ViewName = "AddSystemRole")]
        [SessionExpire]
        [HttpPost]
        public ActionResult AddSystemRole(LocalSystemRoles form)
        {
            // Siva on 08/052013 >>>
            SystemRoles sysRoleObject = efdbContext.SystemRoles.FirstOrDefault(rec => rec.RoleName == form.RoleName && rec.OrganizationType == form.OrgType);
            if (sysRoleObject != null)
            {
                ModelState.AddModelError("RoleName", "Enter another role name");
                getStaticOrgTypesAsViewBagElements();
                return View(form);
            }
            //if (form.RoleName.ToUpper().Trim().Equals("SUPER USER"))
            //{
            //    ModelState.AddModelError("RoleName", "Enter another role name");
            //    getStaticOrgTypesAsViewBagElements();
            //    return View(form);
            //}

            // Ends <<<
            SystemRoles newRole = new SystemRoles();
            newRole.RoleName = form.RoleName;
            newRole.OrganizationType = form.OrgType;
            newRole.LoweredRoleName = form.RoleName;
            List<SystemApplicationId> sysAppIds = efdbContext.SystemApplicationId.ToList();
            if (sysAppIds.Count() > 0)
                newRole.ApplicationId = sysAppIds[0].ApplicationId;
            else
                return View();

            efdbContext.SystemRoles.Add(newRole);
            efdbContext.SaveChanges();

            getStaticOrgTypesAsViewBagElements();

            SystemRolesList();

            return View(form);
        }

        [HeaderAndSidebar(isPermissionRequired = true, headerName = "MyAccount", sideMenuName = "RolesAndPermissions", ViewName = "AddSystemRole")]
        [SessionExpire]
        public ActionResult AddOrEditPermissions(string orgType, string roleId, string roleName)
        {
            LocalAddOrEditPermission newAddOrEditPermission = new LocalAddOrEditPermission();
            newAddOrEditPermission.RoleId = roleId;
            var EmployeeRoles = EmployeeRole.EmployeeRoleId;
            // Siva on Nov 9th >>>
            if (roleName.ToLower().Equals("super user") || EmployeeRoles.Contains(roleId)) // Kiran on 9/4/2014
                newAddOrEditPermission.isReadOnly = true;
            else
                newAddOrEditPermission.isReadOnly = false;
            // Ends <<<
            newAddOrEditPermission.RoleName = roleName;
            newAddOrEditPermission.OrganizationType = orgType;

            newAddOrEditPermission.permissions = new List<Permissions>();

            List<SystemViews> sysViews = efdbContext.SystemViews.Where(row => row.OrganizationType == orgType).ToList();
            bool readSelectAll = true;
            bool insertSelectAll = true;
            bool modifySelectAll = true;
            bool deleteSelectAll = true;
            foreach (var view in sysViews)
            {
                Permissions newPermissionLocal = new Permissions();
                newPermissionLocal.ViewId = view.SystemViewsId;
                newPermissionLocal.ViewName = view.SystemViewName;

                SystemRolesPermissions sysRolePermissions = efdbContext.SystemRolesPermissions.FirstOrDefault(row => row.SysRoleId == roleId && row.SysViewsId == view.SystemViewsId);
                if (sysRolePermissions != null)
                {
                    newPermissionLocal.readPermission = (bool)sysRolePermissions.ReadPermission;
                    newPermissionLocal.insertPermission = (bool)sysRolePermissions.InsertPermission;
                    newPermissionLocal.modifyPermission = (bool)sysRolePermissions.ModifyPermission;
                    newPermissionLocal.deletePermission = (bool)sysRolePermissions.DeletePermission;

                    if (!newPermissionLocal.readPermission)
                        readSelectAll = false;
                    if (!newPermissionLocal.insertPermission)
                        insertSelectAll = false;
                    if (!newPermissionLocal.modifyPermission)
                        modifySelectAll = false;
                    if (!newPermissionLocal.deletePermission)
                        deleteSelectAll = false;
                }
                else
                {
                    readSelectAll = false;
                    insertSelectAll = false;
                    modifySelectAll = false;
                    deleteSelectAll = false;
                }


                newAddOrEditPermission.permissions.Add(newPermissionLocal);
            }

            newAddOrEditPermission.ReadSelectAll = readSelectAll;
            newAddOrEditPermission.InsertSelectAll = insertSelectAll;
            newAddOrEditPermission.ModifySelectAll = modifySelectAll;
            newAddOrEditPermission.DeleteSelectAll = deleteSelectAll;

            newAddOrEditPermission.otherPermissions = new List<Permissions>();

            //if (orgType != "Vendor")
            {
                //OtherPermisions
                Permissions restrictedAccessPermission = new Permissions();
                restrictedAccessPermission.ViewId = 1000;
                restrictedAccessPermission.ViewName = "Restricted Folder Access";
                SystemRolesPermissions sysRolePermissionsOther = efdbContext.SystemRolesPermissions.FirstOrDefault(row => row.SysRoleId == roleId && row.SysViewsId == restrictedAccessPermission.ViewId);
                if (sysRolePermissionsOther != null)
                {
                    restrictedAccessPermission.readPermission = (bool)sysRolePermissionsOther.ReadPermission;
                }
                newAddOrEditPermission.otherPermissions.Add(restrictedAccessPermission);

                //OtherPermisions
                Permissions restrictedAccessPermission2 = new Permissions();
                restrictedAccessPermission2.ViewId = 1001;
                restrictedAccessPermission2.ViewName = "Restricted Section Access";
                SystemRolesPermissions sysRolePermissionsOther2 = efdbContext.SystemRolesPermissions.FirstOrDefault(row => row.SysRoleId == roleId && row.SysViewsId == restrictedAccessPermission2.ViewId);
                if (sysRolePermissionsOther2 != null)
                {
                    restrictedAccessPermission2.readPermission = (bool)sysRolePermissionsOther2.ReadPermission;
                }
                newAddOrEditPermission.otherPermissions.Add(restrictedAccessPermission2);

                //OtherPermisions
                Permissions restrictedSectionAccess2 = new Permissions();
                restrictedSectionAccess2.ViewId = LocalConstants.RESTRICTED_ACCESS_2_VIEW_ID;
                restrictedSectionAccess2.ViewName = LocalConstants.RESTRICTED_ACCESS_2_VIEW_NAME;
                SystemRolesPermissions sysrestrictedPermission2 = efdbContext.SystemRolesPermissions.FirstOrDefault(row => row.SysRoleId == roleId && row.SysViewsId == restrictedSectionAccess2.ViewId);
                if (sysrestrictedPermission2 != null)
                {
                    restrictedSectionAccess2.readPermission = (bool)sysrestrictedPermission2.ReadPermission;
                }
                newAddOrEditPermission.otherPermissions.Add(restrictedSectionAccess2);

                if (orgType.ToString().ToLower().Equals("admin"))
                {
                    Permissions loginAsAnyUserAccess = new Permissions();
                    loginAsAnyUserAccess.ViewId = LocalConstants.Login_As_Any_User_VIEW_ID;
                    loginAsAnyUserAccess.ViewName = LocalConstants.Login_As_Any_User_VIEW_NAME;
                    SystemRolesPermissions sysRoleLoginAsAnyUserPermission =
                        efdbContext.SystemRolesPermissions.FirstOrDefault(
                            row => row.SysRoleId == roleId && row.SysViewsId == loginAsAnyUserAccess.ViewId);
                    if (sysRoleLoginAsAnyUserPermission != null)
                    {
                        loginAsAnyUserAccess.readPermission = (bool)sysRoleLoginAsAnyUserPermission.ReadPermission;
                    }
                    newAddOrEditPermission.otherPermissions.Add(loginAsAnyUserAccess);
                }


                //Permissions EriUser = new Permissions();
                //EriUser.ViewId = LocalConstants.Eri_User_View_Id;
                //EriUser.ViewName = LocalConstants.Eri_User_View_Name;
                //SystemRolesPermissions sysRoleEriUser = efdbContext.SystemRolesPermissions.FirstOrDefault(row => row.SysRoleId == roleId && row.SysViewsId == EriUser.ViewId);
                //if (sysRoleEriUser != null)
                //{
                //    EriUser.readPermission = (bool)sysRoleEriUser.ReadPermission;
                //}
                //newAddOrEditPermission.otherPermissions.Add(EriUser);

                if (orgType.Equals(Constants.OrganizationType.Client)|| orgType.Equals(Constants.OrganizationType.SuperClient))
                {
                    Permissions SupportingDocuments = new Permissions();
                    SupportingDocuments.ViewId = LocalConstants.Supporting_Documents_View_Id;
                    SupportingDocuments.ViewName = LocalConstants.Supporting_Documents_View_Name;
                    SystemRolesPermissions sysRole = efdbContext.SystemRolesPermissions.FirstOrDefault(row => row.SysRoleId == roleId && row.SysViewsId == SupportingDocuments.ViewId);
                    if (sysRole != null)
                    {
                        SupportingDocuments.readPermission = (bool)sysRole.ReadPermission;
                    }
                    newAddOrEditPermission.otherPermissions.Add(SupportingDocuments);
                }
                if (orgType.Equals(Constants.OrganizationType.Client))
                {
                    Permissions TagNumber = new Permissions();
                    TagNumber.ViewId = LocalConstants.Tag_Number_View_Id;
                    TagNumber.ViewName = LocalConstants.Tag_Number_View__Name;
                    SystemRolesPermissions sysRole = efdbContext.SystemRolesPermissions.FirstOrDefault(row => row.SysRoleId == roleId && row.SysViewsId == TagNumber.ViewId);
                    if (sysRole != null)
                    {
                        TagNumber.readPermission = (bool)sysRole.ReadPermission;
                    }
                    newAddOrEditPermission.otherPermissions.Add(TagNumber);
                }
                if (orgType.Equals(Constants.OrganizationType.Client))
                {
                    Permissions ClientEmp = new Permissions();
                    ClientEmp.ViewId = LocalConstants.ClientEmployeeId;
                    ClientEmp.ViewName = LocalConstants.ClientEmployee_View__Name;
                    SystemRolesPermissions sysRole = efdbContext.SystemRolesPermissions.FirstOrDefault(row => row.SysRoleId == roleId && row.SysViewsId == ClientEmp.ViewId);
                    if (sysRole != null)
                    {
                        ClientEmp.readPermission = (bool)sysRole.ReadPermission;
                    }
                    newAddOrEditPermission.otherPermissions.Add(ClientEmp);
                }
                if (orgType.Equals(Constants.OrganizationType.Client))
                {
                    Permissions BadgeNumber = new Permissions();
                    BadgeNumber.ViewId = LocalConstants.Badge_Number_View_Id;
                    BadgeNumber.ViewName = LocalConstants.Badge_Number_View__Name;
                    SystemRolesPermissions sysRole = efdbContext.SystemRolesPermissions.FirstOrDefault(row => row.SysRoleId == roleId && row.SysViewsId == BadgeNumber.ViewId);
                    if (sysRole != null)
                    {
                        BadgeNumber.readPermission = (bool)sysRole.ReadPermission;
                    }
                    newAddOrEditPermission.otherPermissions.Add(BadgeNumber);
                }
                if (orgType.Equals(Constants.OrganizationType.Client))
                {
                    Permissions Sinepro = new Permissions();
                    Sinepro.ViewId = LocalConstants.SineproId;
                    Sinepro.ViewName = LocalConstants.Sinepro_View_Name;
                    SystemRolesPermissions sysRole = efdbContext.SystemRolesPermissions.FirstOrDefault(row => row.SysRoleId == roleId && row.SysViewsId == Sinepro.ViewId);
                    if (sysRole != null)
                    {
                        Sinepro.readPermission = (bool)sysRole.ReadPermission;
                    }
                    newAddOrEditPermission.otherPermissions.Add(Sinepro);
                }
                if (orgType.Equals(Constants.OrganizationType.Admin))
                {
                    Permissions Comments = new Permissions();
                    Comments.ViewId = LocalConstants.PQComments_ID;
                    Comments.ViewName = LocalConstants.PQComments_View_Name;
                    SystemRolesPermissions sysRole = efdbContext.SystemRolesPermissions.FirstOrDefault(row => row.SysRoleId == roleId && row.SysViewsId == Comments.ViewId);
                    if (sysRole != null)
                    {
                        Comments.readPermission = (bool)sysRole.ReadPermission;
                    }
                    newAddOrEditPermission.otherPermissions.Add(Comments);
                }
                if (orgType.Equals(Constants.OrganizationType.Client))
                {
                    Permissions Comments = new Permissions();
                    Comments.ViewId = LocalConstants.PQComments_ID;
                    Comments.ViewName = LocalConstants.PQComments_View_Name;
                    SystemRolesPermissions sysRole = efdbContext.SystemRolesPermissions.FirstOrDefault(row => row.SysRoleId == roleId && row.SysViewsId == Comments.ViewId);
                    if (sysRole != null)
                    {
                        Comments.readPermission = (bool)sysRole.ReadPermission;
                    }
                    newAddOrEditPermission.otherPermissions.Add(Comments);
                }
                if (orgType.Equals(Constants.OrganizationType.Vendor))
                {
                    Permissions FinancialAnalytics = new Permissions();
                    FinancialAnalytics.ViewId = LocalConstants.Financial_Analytics_View_Id;
                    FinancialAnalytics.ViewName = LocalConstants.Financial_Analytics_View_Name;
                    SystemRolesPermissions sysRole = efdbContext.SystemRolesPermissions.FirstOrDefault(row => row.SysRoleId == roleId && row.SysViewsId == FinancialAnalytics.ViewId);
                    if (sysRole != null)
                    {
                        FinancialAnalytics.readPermission = (bool)sysRole.ReadPermission;
                    }
                    newAddOrEditPermission.otherPermissions.Add(FinancialAnalytics);
                }
                if (!orgType.Equals(Constants.OrganizationType.Vendor))
                {
                    Permissions FinancialAnalyticsRatio = new Permissions();
                    FinancialAnalyticsRatio.ViewId = LocalConstants.Financial_Analytics_Ratios_View_Id;
                    FinancialAnalyticsRatio.ViewName = LocalConstants.Financial_Analytics_Ratios_View_Name;
                    SystemRolesPermissions sysRole = efdbContext.SystemRolesPermissions.FirstOrDefault(row => row.SysRoleId == roleId && row.SysViewsId == FinancialAnalyticsRatio.ViewId);
                    if (sysRole != null)
                    {
                        FinancialAnalyticsRatio.readPermission = (bool)sysRole.ReadPermission;
                    }
                    newAddOrEditPermission.otherPermissions.Add(FinancialAnalyticsRatio);
                }
            }
            return View(newAddOrEditPermission);
        }

        [HeaderAndSidebar(isPermissionRequired = true, headerName = "MyAccount", sideMenuName = "RolesAndPermissions", ViewName = "AddSystemRole")]
        [SessionExpire]
        [HttpPost]
        public ActionResult AddOrEditPermissions(LocalAddOrEditPermission form)
        {
            bool checksAvailable = false;
            foreach (var permission in form.permissions)
            {
                if (permission.readPermission == true || permission.insertPermission == true || permission.modifyPermission == true || permission.deletePermission == true)
                {
                    checksAvailable = true;
                    break;
                }
            }

            if (checksAvailable)
            {
                List<SystemRolesPermissions> existingRolePermissions = efdbContext.SystemRolesPermissions.Where(row => row.SysRoleId == form.RoleId).ToList();
                foreach (var existingPermission in existingRolePermissions)
                {
                    efdbContext.SystemRolesPermissions.Remove(existingPermission);
                    efdbContext.SaveChanges();
                }


                foreach (var permission in form.permissions)
                {
                    if (permission.readPermission == false && permission.insertPermission == false && permission.modifyPermission == false && permission.deletePermission == false)
                        continue;

                    SystemRolesPermissions newRolePermission = new SystemRolesPermissions();
                    newRolePermission.SysRoleId = form.RoleId;
                    newRolePermission.SysViewsId = permission.ViewId;
                    newRolePermission.PermissionName = form.PermissionName;
                    newRolePermission.ReadPermission = permission.readPermission;
                    newRolePermission.InsertPermission = permission.insertPermission;
                    newRolePermission.ModifyPermission = permission.modifyPermission;
                    newRolePermission.DeletePermission = permission.deletePermission;

                    efdbContext.SystemRolesPermissions.Add(newRolePermission);
                }
                foreach (var permission in form.otherPermissions)
                {
                    if (permission.readPermission == false && permission.insertPermission == false && permission.modifyPermission == false && permission.deletePermission == false)
                        continue;

                    SystemRolesPermissions newRolePermission = new SystemRolesPermissions();
                    newRolePermission.SysRoleId = form.RoleId;
                    newRolePermission.SysViewsId = permission.ViewId;
                    newRolePermission.PermissionName = form.PermissionName;
                    newRolePermission.ReadPermission = permission.readPermission;
                    newRolePermission.InsertPermission = permission.insertPermission;
                    newRolePermission.ModifyPermission = permission.modifyPermission;
                    newRolePermission.DeletePermission = permission.deletePermission;

                    efdbContext.SystemRolesPermissions.Add(newRolePermission);
                }

                efdbContext.SaveChanges();

                return RedirectToAction("AddSystemRole");
            }
            else
            {
                form.checksNotAvailable = true;
                return View(form);
            }

        }

        public string AssignOrgRoles(string roleId, List<long> orgIds)
        {
            return _org.AssignOrgRoles(roleId, orgIds);
        }

        //////////////////////////////

    }
}
