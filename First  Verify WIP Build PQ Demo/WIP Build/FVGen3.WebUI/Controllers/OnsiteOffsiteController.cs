﻿using FVGen3.Domain.Abstract;
using FVGen3.Domain.Concrete;
using FVGen3.WebUI.Annotations;
using FVGen3.WebUI.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FVGen3.BusinessLogic;
using FVGen3.BusinessLogic.Interfaces;
using Resources;
using FVGen3.WebUI.Constants;

namespace FVGen3.WebUI.Controllers
{

    [SessionExpire]
    [HeaderAndSidebar(headerName = "Onsite/Offsite", sideMenuName = "", ViewName = "Get")]
    public class OnsiteOffsiteController : BaseController
    {
        EFDbContext entityDB;
        private IOnSiteOffSiteBusiness _onsiteOffSite;
        private IClientBusiness _ClientBusiness;
        private ISystemUserBusiness _SystemUserBusiness;
        public OnsiteOffsiteController(IOnSiteOffSiteBusiness _onsiteOffSite, IClientBusiness _ClientBusiness, ISystemUserBusiness _SystemUserBusiness)
        {
            this._onsiteOffSite = _onsiteOffSite;
            this._ClientBusiness = _ClientBusiness;
            this._SystemUserBusiness = _SystemUserBusiness;
            entityDB = new EFDbContext();

        }

        //
        // GET: /OnsiteOffsite/

        public ActionResult Get()
        {
            ViewBag.HasSineproPermission = _SystemUserBusiness.IsUserHasPermission(SSession.UserId, LocalConstants.SineproId);
            ViewBag.HasClientEmpPermission = _SystemUserBusiness.IsUserHasPermission(SSession.UserId, LocalConstants.ClientEmployeeId);
            ViewBag.HasPermission = _SystemUserBusiness.IsUserHasPermission(SSession.UserId, LocalConstants.Tag_Number_View_Id);
            ViewBag.HasBadgePermission = _SystemUserBusiness.IsUserHasPermission(SSession.UserId, LocalConstants.Badge_Number_View_Id);
            Guid LoggedInUseruserId = (Guid)Session["UserId"];
            var contacts = entityDB.SystemUsers.Find(LoggedInUseruserId).Contacts.FirstOrDefault();
            try { ViewBag.FullName = contacts.LastName.Trim() + ", " + contacts.FirstName; } // Sandhya on 05/08/2015
            catch { ViewBag.FullName = Session["UserName"].ToString(); }
           var ClientId= Convert.ToInt64(Session["currentOrgId"]);
            ViewBag.ClientID = Convert.ToInt64(Session["currentOrgId"]);
            var Departments = _SystemUserBusiness.GetClientDepartments(ClientId);
            var SineproSites = _SystemUserBusiness.GetSineproSites();
            List<SelectListItem> Clientdepartments = new List<SelectListItem>();
            Departments.ForEach(s => Clientdepartments.Add(new SelectListItem() { Text = s.Key, Value = s.Value.ToString() }));

            //var clientOrg = clientOrg.ToList();
            SelectListItem DefaultItem = new SelectListItem();
            DefaultItem.Text = "Select Department";
            DefaultItem.Value = "";
            Clientdepartments.Insert(0, DefaultItem);
            ViewBag.Departments = Clientdepartments;
            List<SelectListItem> Sineprositeslist = new List<SelectListItem>();
            SineproSites.ForEach(s => Sineprositeslist.Add(new SelectListItem() { Text = s.Key, Value = s.Value.ToString() }));

            //var clientOrg = clientOrg.ToList();
            SelectListItem Default = new SelectListItem();
            Default.Text = "All Sites";
            Default.Value = "";
            Sineprositeslist.Insert(0, Default);
            ViewBag.SineproSites = Sineprositeslist;
            return View();
          
          
        }
        public ActionResult AddNewVisitor(long buID, long locID)
        {
            ViewBag.ClientID = Convert.ToInt64(Session["currentOrgId"]);
            ViewBag.BuID = buID;
            ViewBag.LocID = locID;

            return View();
        }
        public ActionResult EditVisitor(Guid visitorid)
        {
            var visitor = entityDB.Visitors.Find(visitorid);
            ViewBag.ClientID = SSession.OrganizationId;
            if (visitor != null)
            {
                ViewBag.BuID = visitor.ClientBusinessUnitID;
                ViewBag.LocID = visitor.ClientBusinessUnitSiteID;
                ViewBag.FirstName = visitor.FirstName;
                ViewBag.LastName = visitor.LastName;
                ViewBag.Company = visitor.Company;
                ViewBag.SiteContact = visitor.SiteContact;
                ViewBag.Visitorid = visitorid;
            }

            return View("AddNewVisitor");
        }
        [HttpPost]
        public ActionResult GetPQStatus(long clientid, long vendorid)
        {
            var helper = new PrequalificationHelper();
         var result = helper.GetLatestPrequalification(clientid, vendorid);
            return Json(result);
        }

        public JsonResult SaveVisitor(string firstName, string lastName, string siteContact, string company, int clientID, int buID, int locID, Guid? visitorid)
        {
            try
            {

                var visitor = entityDB.Visitors.Find(visitorid);
                
                if (visitor != null)
                {
                    entityDB.Entry(visitor).State=EntityState.Modified;
                }
                else
                {
                    visitor = new Domain.Entities.Visitors();
                    visitor.VisitorID = Guid.NewGuid();
                    entityDB.Visitors.Add(visitor);
                }
                visitor.ClientID = clientID;
                visitor.Company = company;
                visitor.FirstName = firstName;
                visitor.LastName = lastName;
                visitor.SiteContact = siteContact;
                visitor.ClientBusinessUnitID = buID;
                visitor.ClientBusinessUnitSiteID = locID;
                entityDB.SaveChanges();
            }
            catch (Exception ex)
            {

                return Json(ex.Message);
            }
            return Json("Success");
        }
        public JsonResult DeleteVisitor(Guid visitorid)
        {
            try
            {
                var visitor = entityDB.Visitors.Find(visitorid);
                if (visitor != null)
                {
                    foreach (var history in entityDB.OnsiteOffsiteVisitor.Where(r=>r.VisitorID==visitorid).ToList())
                    {
                        entityDB.OnsiteOffsiteVisitor.Remove(history);
                    }
                    entityDB.Entry(visitor).State = EntityState.Deleted;
                }
                entityDB.SaveChanges();
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
            return Json("Success");
        }

        //TODO this could be moved to a generic API

        public JsonResult GetBUs(long clientID)
        {
            var subClientIds = _ClientBusiness.GetSubClients(clientID).Select(r => long.Parse(r.Key)).ToList();
            var bus = entityDB.ClientBusinessUnits.Where(b => subClientIds.Contains(b.ClientId)||b.ClientId== clientID).Select(s => new DropdownGenericModel
            {
               ID = s.ClientBusinessUnitId,
             Name = s.BusinessUnitName

           }).OrderBy(o => o.Name).ToList();
            return Json(bus);
        }

        //TODO this could be moved to a generic API
        public JsonResult GetLocations(long buID)
        {
            //ClientBusinessUnitSites
            var locs = entityDB.ClientBusinessUnitSites.Where(s => s.ClientBusinessUnitId == buID).Select(s => new DropdownGenericModel
            {
                ID = s.ClientBusinessUnitSiteId,
                Name = s.SiteName

            }).OrderBy(o => o.Name).ToList();
            return Json(locs);
        }
        //TODO this could be moved to a generic API

        public JsonResult GetVendors(long clientID, long buID, long locID)
        {
            var subClientIds = _ClientBusiness.GetSubClients(clientID).Select(r => long.Parse(r.Key)).ToList();
            var locs = entityDB.PrequalificationSites.Join(entityDB.Organizations, p => p.VendorId,
                                                            o => o.OrganizationID,
                                                            (p, o) => new { p, o })
                          .Where(s => s.p.ClientBusinessUnitId == buID
                                && s.p.ClientBusinessUnitSiteId == locID
                                && (subClientIds.Contains(s.p.ClientId.Value)|| s.p.ClientId== clientID))
                        .Select(s => new DropdownGenericModel
                        {
                            ID = s.p.VendorId.Value,
                            Name = s.o.Name

                        }).Distinct().OrderBy(o => o.Name).ToList();
            return Json(locs);
        }

        public ActionResult GetOffSiteEmployees(int draw, int start, int length, long buID, long locID,string DepartmentId ,int UserType, int timeZoneOffset)
        {
            //IQueryable<EmployeeOnSiteOffsiteModel> preresult;
            //ProcessEmployees(-1, buID, locID, timeZoneOffset, out preresult);
            ViewBag.UserType = 1;
            if(locID<0 && UserType ==1)
              return Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = 0,
                    recordsFiltered = 0,
                    data=new List<EmployeeOnSiteOffsiteModel>()
                }, JsonRequestBehavior.AllowGet);

            var serverOffset = TimeZoneInfo.Local.GetUtcOffset(DateTime.UtcNow);
            var workingOffset = (timeZoneOffset / 60) + serverOffset.Hours;

            long clientId = Convert.ToInt32(Session["currentOrgId"]);
//var employee_RoleId = EmployeeRole.EmployeeRoleId;
//var employee_RoleId = entityDB.SystemRoles.FirstOrDefault(record => record.RoleName == "Employee Training").RoleId;
            var employee_sysUserOrgs = entityDB.SystemUsersOrganizationsRoles.Where(
                    record => EmployeeRole.VendorEmpRoleId==record.SysRoleId)
                .Select(rec => rec.SystemUsersOrganizations)
                .Where(rec => rec.SystemUsersOrganizationsQuizzes.Any(rec1 => rec1.ClientId == clientId));
            var contact = entityDB.Contact.Where(rec => true);
            var minDate = DateTime.Now;//StartOfWeek(DateTime.Now, DayOfWeek.Monday); //DateTime.Now.Date.AddDays(-14);
            var yesterday = DateTime.Now.AddDays(-1).Date;
            var Checkout = LocalConstants.Checkout;
            var IsormatUser = _SystemUserBusiness.CheckIsOrmatUser(SSession.UserId);
            if(IsormatUser)
            {
                Checkout = LocalConstants.OrmatCheckout;
            }
            //TODO: Filter by two weeks
            //Find 12:01AM last monday.

            List<Guid> requiredTrainings = GetRequiredTrainings(clientId, locID);
            //  var t1 = DateTime.Now;
            var allEmps = employee_sysUserOrgs.Where(re => re.SystemUsersOrganizationsQuizzes.Any(r => requiredTrainings.Contains((Guid)r.ClientTemplateId)))
                .Join(entityDB.SystemUsers.Where(rec => rec.UserStatus != false),
                    suo => suo.UserId,
                    su => su.UserId,
                    (suo, su) => new { su, suo })
                .Join(contact, su => su.su.UserId,
                    c => c.UserId,
                    (su, c) => new { su, c })
                .Select(s => new EmployeeOnSiteOffsiteModel
                {
                    OnsiteOffsiteID = null,
                    EmployeeID = s.su.su.UserId,
                    FirstName = s.c.FirstName,
                    LastName = s.c.LastName,
                    Checkin = null,
                    Checkout = null,
                    TagNumber = null,
                    OldTagId = s.su.su.TagNumber ?? entityDB.OnsiteOffsites.Where(rec =>
                        rec.VendorEmployeeID == s.su.su.UserId && rec.ClientBusinessUnitID == buID &&
                        rec.ClientBusinessUnitSiteID == locID && rec.Checkin.Value > yesterday).OrderByDescending(rec => rec.Checkin).FirstOrDefault(
                    ).TagNumber,
                    BadgeNumber = null,
                    OldBadgeId = s.su.su.Badgenumber ?? entityDB.OnsiteOffsites.Where(rec =>
                        rec.VendorEmployeeID == s.su.su.UserId && rec.ClientBusinessUnitID == buID &&
                        rec.ClientBusinessUnitSiteID == locID && rec.Checkin.Value > yesterday).OrderByDescending(rec => rec.Checkin).FirstOrDefault(
                    ).BadgeNumber,
                    VendorID = s.su.suo.OrganizationId,
                    VendorName = s.su.suo.Organizations.Name,
                    PassedOrientation = s.su.suo.SystemUsersOrganizationsQuizzes.Any(
                        rec =>
                            rec.ClientTemplateId != null &&
                            rec.QuizResult != null && rec.QuizResult == true && rec.ClientId == clientId &&
                            rec.QuizExpirationDate >= DateTime.Now)
                        ? "Yes"
                        : "No",
                    UserType = 1 //1-vendor 2-client
                });

               // .Where(rec => rec.PassedOrientation == "Yes");
            var onsiteEmps = employee_sysUserOrgs.Join(entityDB.SystemUsers.Where(rec => rec.UserStatus != false),
                    suo => suo.UserId,
                    su => su.UserId,
                    (suo, su) => new {su, suo})
                .Join(contact, su => su.su.UserId,
                    c => c.UserId,
                    (su, c) => new {su, c})
                .GroupJoin(entityDB.OnsiteOffsites,
                    u => u.su.su.UserId,
                    oo => oo.VendorEmployeeID,
                    (u, oo) => new {u, oo})
                .SelectMany(z => z.oo.DefaultIfEmpty(),
                    (z, d) => new {z.u, d})
                .Where(
                    w =>
                        (w.d.Checkin.HasValue && SqlFunctions.DateDiff("hour", (DateTime) w.d.Checkin, minDate) < Checkout &&
                         !w.d.Checkout.HasValue))
                .Where(rec=>rec.d.ClientID==clientId)
                
                .Select(s => s.u.su.su.UserId);
            if (UserType == 2)
            {
                ViewBag.UserType = 2;
                var ClientEmps = entityDB.SystemUsersOrganizationsRoles.Where(
                          record => record.SysRoleId == EmployeeRole.ClientEmployeeID)
                      .Select(rec => rec.SystemUsersOrganizations).Where(r=>r.OrganizationId==SSession.OrganizationId);
               allEmps = ClientEmps
                     .Join(entityDB.SystemUsers.Where(rec => rec.UserStatus != false),
                         suo => suo.UserId,
                         su => su.UserId,
                         (suo, su) => new { su, suo })
                     .Join(contact, su => su.su.UserId,
                         c => c.UserId,
                         (su, c) => new { su, c })
                     .Select(s => new EmployeeOnSiteOffsiteModel
                     {
                         OnsiteOffsiteID = null,
                         EmployeeID = s.su.su.UserId,
                         FirstName = s.c.FirstName,
                         LastName = s.c.LastName,
                         Checkin = null,
                         Checkout = null,
                         TagNumber = null,
                         OldTagId = s.su.su.TagNumber ?? entityDB.OnsiteOffsites.Where(rec =>
                             rec.VendorEmployeeID == s.su.su.UserId && rec.Checkin.Value > yesterday).OrderByDescending(rec => rec.Checkin).FirstOrDefault(
                         ).TagNumber,
                         BadgeNumber = null,
                         OldBadgeId = s.su.su.Badgenumber ?? entityDB.OnsiteOffsites.Where(rec =>
                             rec.VendorEmployeeID == s.su.su.UserId && rec.Checkin.Value > yesterday).OrderByDescending(rec => rec.Checkin).FirstOrDefault(
                         ).BadgeNumber,
                         VendorID = s.su.suo.OrganizationId,
                         VendorName = s.su.suo.Organizations.Name,
                         UserType = 2,  //1-vendor 2-client
                         DepartmentId=s.su.su.UserDepartments.FirstOrDefault().DepartmentId,
                         DepartmentName=s.su.su.UserDepartments.FirstOrDefault().Department.DepartmentName

                     });
                onsiteEmps = ClientEmps.Join(entityDB.SystemUsers.Where(rec => rec.UserStatus != false),
                          suo => suo.UserId,
                          su => su.UserId,
                          (suo, su) => new { su, suo })
                      .Join(contact, su => su.su.UserId,
                          c => c.UserId,
                          (su, c) => new { su, c })
                      .GroupJoin(entityDB.OnsiteOffsites,
                          u => u.su.su.UserId,
                          oo => oo.VendorEmployeeID,
                          (u, oo) => new { u, oo })
                      .SelectMany(z => z.oo.DefaultIfEmpty(),
                          (z, d) => new { z.u, d })
                      .Where(
                          w =>
                              (w.d.Checkin.HasValue && SqlFunctions.DateDiff("hour", (DateTime)w.d.Checkin, minDate) < Checkout &&
                               !w.d.Checkout.HasValue))
                      .Where(rec => rec.d.ClientID == clientId)

                      .Select(s => s.u.su.su.UserId);
            }
            var search = Request.QueryString["search[value]"];
            var preresult = allEmps.Where(rec =>rec.EmployeeID!=null && !onsiteEmps.Contains((Guid)rec.EmployeeID));
            var preData = preresult;
            if(UserType ==2)
            {
                if (!string.IsNullOrEmpty(DepartmentId))
                {
                    var DepId = Convert.ToInt32(DepartmentId);
                    preData = preData.Where(rec => rec.DepartmentId == DepId); }
                preData = preData.Where(rec => rec.VendorName.Contains(search) || (rec.LastName + ", " + rec.FirstName).Contains(search)|| rec.DepartmentName.ToLower().Contains(search.ToLower()));
            }
            else
            {
                preData = preData.Where(rec => rec.VendorName.Contains(search) || (rec.LastName + ", " + rec.FirstName).Contains(search));
            }
            //preData =
            //    preData.OrderBy(o => o.LastName)
            //        .ThenBy(o => o.FirstName)
            //        .ThenBy(o => o.EmployeeID)
            //        .ThenBy(o => o.OnsiteOffsiteID.HasValue)
            //        .ThenByDescending(o => o.Checkin.HasValue ? o.Checkin : o.Checkout);
            var data = preData
                .OrderBy(rec => rec.VendorName).ThenBy(rec=>rec.LastName).Skip(start).Take(length).ToList();
            data.ForEach(r =>
            {
                //r.OldTagId = GetOldTagId(r.EmployeeID.Value, buID, locID);
                r.Locked = IsLocked(data, r.OnsiteOffsiteID, r.EmployeeID.Value, r.Checkin, r.Checkout);
            });
            var result = this.Json(new
            {
                draw = Convert.ToInt32(draw),
                recordsTotal = data.Count,
                recordsFiltered = preData.Count(),
                data
            }, JsonRequestBehavior.AllowGet);
           // var t2 = DateTime.Now;
          //  var diff= t2.Subtract(t1);
            return result;

        }
        public JsonResult GetEmployeeCheckins(long vendorID, long buID, long locID, int timeZoneOffset)
        {
            ViewBag.HasClientEmpPermission = _SystemUserBusiness.IsUserHasPermission(SSession.UserId, LocalConstants.ClientEmployeeId);

            ViewBag.HasPermission = _SystemUserBusiness.IsUserHasPermission(SSession.UserId, LocalConstants.Tag_Number_View_Id);
            ViewBag.HasBadgePermission = _SystemUserBusiness.IsUserHasPermission(SSession.UserId, LocalConstants.Badge_Number_View_Id);
            IQueryable<EmployeeOnSiteOffsiteModel> preresult;
            var workingOffset = ProcessEmployees(vendorID, buID, locID, timeZoneOffset, out preresult);
            var result = vendorID <= 0
                ? preresult.Where(rec => rec.PassedOrientation == "Yes").OrderBy(rec => rec.VendorName).ToList()
                : preresult.ToList();
            result.ForEach(r =>
            {
                //r.PassedOrientation = GetPassedTraining(requiredTrainings, r.EmployeeID.Value, Convert.ToInt32(Session["currentOrgId"]));
                r.Locked = IsLocked(result, r.OnsiteOffsiteID, r.EmployeeID.Value, r.Checkin, r.Checkout);
              
            });
            var orderedResult = result.OrderBy(o => o.LastName).ThenBy(o => o.FirstName).ThenBy(o => o.EmployeeID).ThenBy(o => o.OnsiteOffsiteID.HasValue).ThenByDescending(o => o.Checkin.HasValue ? o.Checkin : o.Checkout).ToList();
            List<EmployeeOnSiteOffsiteModel> sanitizedResult = Sanitize(orderedResult, workingOffset);
            //if (vendorID <= 0)
            //    return Json(sanitizedResult.Where(rec => rec.Locked == false && rec.Checkin == null && rec.Checkout == null));
            return Json(sanitizedResult);
        }

        private int ProcessEmployees(long vendorID, long buID, long locID, int timeZoneOffset, out IQueryable<EmployeeOnSiteOffsiteModel> preresult)
        {
            var serverOffset = TimeZoneInfo.Local.GetUtcOffset(DateTime.UtcNow);
            var workingOffset = (timeZoneOffset / 60) + serverOffset.Hours;

            long clientId = Convert.ToInt32(Session["currentOrgId"]);
            var employee_RoleId = EmployeeRole.EmployeeRoleId;
           // var employee_RoleId = entityDB.SystemRoles.FirstOrDefault(record => record.RoleName == "Employee Training").RoleId;
            var employee_sysUserOrgs = entityDB.SystemUsersOrganizationsRoles.Where(
                    record =>employee_RoleId.Contains(record.SysRoleId))
                .Select(rec => rec.SystemUsersOrganizations)
                .Where(rec => rec.SystemUsersOrganizationsQuizzes.Any(rec1 => rec1.ClientId == clientId));
            var contact = entityDB.Contact.Where(rec => true);

            //if (vendorID > 0)
                employee_sysUserOrgs = employee_sysUserOrgs.Where(rec => rec.OrganizationId == vendorID);


            //TODO: Filter by two weeks
            //Find 12:01AM last monday.
            var minDate = StartOfWeek(DateTime.Now, DayOfWeek.Monday); //DateTime.Now.Date.AddDays(-14);

            List<Guid> requiredTrainings = GetRequiredTrainings(clientId, locID);
            var yesterday = DateTime.Now.AddDays(-1).Date;
            preresult = employee_sysUserOrgs.Join(entityDB.SystemUsers.Where(rec => rec.UserStatus != false), suo => suo.UserId,
                    su => su.UserId,
                    (suo, su) => new {su, suo})
                .Join(contact, su => su.su.UserId,
                    c => c.UserId,
                    (su, c) => new {su, c})
                .GroupJoin(entityDB.OnsiteOffsites,
                    u => u.su.su.UserId,
                    oo => oo.VendorEmployeeID,
                    (u, oo) => new {u, oo})
                .SelectMany(z => z.oo.DefaultIfEmpty(),
                    (z, d) => new {z.u, d})
                .Where(w => (w.d.Checkout.HasValue && w.d.Checkout > minDate) || (w.d.Checkin.HasValue && w.d.Checkin > minDate))
                .Where(w => (w.d.ClientBusinessUnitID == buID && w.d.ClientBusinessUnitSiteID == locID) || w.d.VendorID == null)
                .Select(s => new EmployeeOnSiteOffsiteModel
                {
                    OnsiteOffsiteID = s.d.OnsiteOffsiteID,
                    EmployeeID = s.u.su.su.UserId,
                    FirstName = s.u.c.FirstName,
                    LastName = s.u.c.LastName,
                    Checkin = s.d.Checkin,
                    Checkout = s.d.Checkout,
                    TagNumber = s.d.TagNumber,
                    OldTagId = entityDB.OnsiteOffsites.Where(rec =>
                        rec.VendorEmployeeID == s.u.su.su.UserId && rec.ClientBusinessUnitID == buID &&
                        rec.ClientBusinessUnitSiteID == locID && rec.Checkin.Value>yesterday).OrderByDescending(rec => rec.Checkin).FirstOrDefault(
                    ).TagNumber,
                    BadgeNumber = s.d.BadgeNumber,
                    OldBadgeId = entityDB.OnsiteOffsites.Where(rec =>
                        rec.VendorEmployeeID == s.u.su.su.UserId && rec.ClientBusinessUnitID == buID &&
                        rec.ClientBusinessUnitSiteID == locID && rec.Checkin.Value > yesterday).OrderByDescending(rec => rec.Checkin).FirstOrDefault(
                    ).BadgeNumber,
                    VendorID = s.u.su.suo.OrganizationId,
                    VendorName = s.u.su.suo.Organizations.Name,
                    PassedOrientation = s.u.su.suo.SystemUsersOrganizationsQuizzes.
                        Any(
                            rec =>
                                rec.ClientTemplateId != null && requiredTrainings.Contains((Guid) rec.ClientTemplateId) &&
                                rec.QuizResult != null && rec.QuizResult == true && rec.ClientId == clientId &&
                                rec.QuizExpirationDate >= DateTime.Now)
                        ? "Yes"
                        : "No"
                    //ClientID = s.d.ClientID,
                    //VendorID = s.d.VendorID
                })
                .Union(
                    employee_sysUserOrgs.Join(entityDB.SystemUsers.Where(rec => rec.UserStatus != false), suo => suo.UserId,
                            su => su.UserId,
                            (suo, su) => new {su, suo})
                        .Join(contact, su => su.su.UserId,
                            c => c.UserId,
                            (su, c) => new {su, c})
                        .Select(s => new EmployeeOnSiteOffsiteModel
                        {
                            OnsiteOffsiteID = null,
                            EmployeeID = s.su.su.UserId,
                            FirstName = s.c.FirstName,
                            LastName = s.c.LastName,
                            Checkin = null,
                            Checkout = null,
                            TagNumber = null,
                            OldTagId = entityDB.OnsiteOffsites.Where(rec =>
                            rec.VendorEmployeeID == s.su.su.UserId && rec.ClientBusinessUnitID == buID &&
                            rec.ClientBusinessUnitSiteID == locID && rec.Checkin.Value > yesterday).OrderByDescending(rec => rec.Checkin).FirstOrDefault(
                            ).TagNumber,
                            BadgeNumber = null,
                            OldBadgeId = entityDB.OnsiteOffsites.Where(rec =>
                                rec.VendorEmployeeID == s.su.su.UserId && rec.ClientBusinessUnitID == buID &&
                                rec.ClientBusinessUnitSiteID == locID && rec.Checkin.Value > yesterday).OrderByDescending(rec => rec.Checkin).FirstOrDefault(
                            ).BadgeNumber,
                            VendorID = s.su.suo.OrganizationId,
                            VendorName = s.su.suo.Organizations.Name,
                            PassedOrientation = s.su.suo.SystemUsersOrganizationsQuizzes.
                                Any(
                                    rec =>
                                        rec.ClientTemplateId != null && requiredTrainings.Contains((Guid) rec.ClientTemplateId) &&
                                        rec.QuizResult != null && rec.QuizResult == true && rec.ClientId == clientId &&
                                        rec.QuizExpirationDate >= DateTime.Now)
                                ? "Yes"
                                : "No"
                        })
                );
            Console.WriteLine(preresult);
            return workingOffset;
        }


        [HttpPost]
        public ActionResult GetCheckinsData(string location, int timeZoneOffset)
        {
            
            //var serverOffset = TimeZoneInfo.Local.GetUtcOffset(DateTime.UtcNow);
            //var workingOffset = (timeZoneOffset / 60) + serverOffset.Hours;
            ViewBag.HasClientEmpPermission = _SystemUserBusiness.IsUserHasPermission(SSession.UserId, LocalConstants.ClientEmployeeId);
            
            ViewBag.HasPermission = _SystemUserBusiness.IsUserHasPermission(SSession.UserId, LocalConstants.Tag_Number_View_Id);
            ViewBag.HasBadgePermission = _SystemUserBusiness.IsUserHasPermission(SSession.UserId, LocalConstants.Badge_Number_View_Id);
            var result = _onsiteOffSite.GetCheckins(location, 0,SSession.UserId, SSession.OrganizationId);
            Console.WriteLine(result);
            return Json(result);
        }
        [HttpPost]
        public string ResetTagNumber(Guid OnsiteOffsiteID,Guid EmployeeId,long buID,long locID,bool IsTagNumber,bool IsVisitor)
        { 
            var X = _onsiteOffSite.ResetTagorBadgeNumber(EmployeeId, IsTagNumber, OnsiteOffsiteID, IsVisitor);
            return "Sucess";
        }
        public JsonResult GetVisitorCheckins(long buID, long locID, int timeZoneOffset)
        {

            var serverOffset = TimeZoneInfo.Local.GetUtcOffset(DateTime.UtcNow);
            var workingOffset = (timeZoneOffset / 60) + serverOffset.Hours;


            //var employee_RoleId = entityDB.SystemRoles.FirstOrDefault(record => record.RoleName == "Employee Training").RoleId;
            //var employee_sysUserOrgs = entityDB.SystemUsersOrganizationsRoles.Where(record => record.SysRoleId == employee_RoleId)
            //.Select(rec => rec.SystemUsersOrganizations);
            //TODO: Filter by two weeks
            //Find 12:01AM last monday.
            var minDate = StartOfWeek(DateTime.Now, DayOfWeek.Monday);//DateTime.Now.Date.AddDays(-14);
            var yesterday = DateTime.Now.AddDays(-1).Date;
            //var requiredTrainings = GetRequiredTrainings(Convert.ToInt32(Session["currentOrgId"]), locID);
            var clientID = Convert.ToInt32(Session["currentOrgId"]);
            var tmp = entityDB.Visitors
                                .GroupJoin(entityDB.OnsiteOffsiteVisitor,
                                    u => u.VisitorID,
                                    oo => oo.VisitorID,
                                    (u, oo) => new { u, oo })
                                .SelectMany(z => z.oo.DefaultIfEmpty(),
                                    (z, d) => new { z.u, d })
                                .Where(w => (w.d.Checkout.HasValue && w.d.Checkout > minDate) || (w.d.Checkin.HasValue && w.d.Checkin > minDate))
                                .Where(w => (w.d.ClientBusinessUnitID == buID && w.d.ClientBusinessUnitSiteID == locID))// || w.d.VendorID == null)                                
                                .Select(s => new EmployeeOnSiteOffsiteModel
                                {
                                    OnsiteOffsiteID = s.d.OnsiteOffsiteID,
                                    EmployeeID = s.u.VisitorID,
                                    FirstName = s.u.FirstName,
                                    LastName = s.u.LastName,
                                    Company = s.u.Company,
                                    SiteContact = s.u.SiteContact,
                                    Checkin = s.d.Checkin,
                                    Checkout = s.d.Checkout,
                                    BadgeNumber = s.d.BadgeNumber,
                                    TagNumber = s.d.TagNumber,
                                    OldTagId = null,
                                    OldBadgeId = null
                                   
                                    //ClientID = s.d.ClientID,
                                    //VendorID = s.d.VendorID
                                })//.ToList()
                                .Union(
                                    entityDB.Visitors.Where(w => w.ClientID == clientID)
                                    .Where(w => (w.ClientBusinessUnitID == buID && w.ClientBusinessUnitSiteID == locID))// || w.d.VendorID == null)                                
                                .Select(s => new EmployeeOnSiteOffsiteModel
                                {
                                    OnsiteOffsiteID = null,
                                    EmployeeID = s.VisitorID,
                                    FirstName = s.FirstName,
                                    LastName = s.LastName,
                                    Company = s.Company,
                                    SiteContact = s.SiteContact,
                                    Checkin = null,
                                    Checkout = null,
                                    BadgeNumber = null,
                                    TagNumber = null,
                                    
                                    OldTagId = entityDB.OnsiteOffsiteVisitor.Where(rec =>
                                         rec.VisitorID == s.VisitorID && rec.ClientBusinessUnitID == buID &&
                                         rec.ClientBusinessUnitSiteID == locID && rec.Checkin.Value > yesterday).OrderByDescending(rec => rec.Checkin).FirstOrDefault().TagNumber,
                                    OldBadgeId = entityDB.OnsiteOffsiteVisitor.Where(rec =>
                                         rec.VisitorID == s.VisitorID && rec.ClientBusinessUnitID == buID &&
                                         rec.ClientBusinessUnitSiteID == locID && rec.Checkin.Value > yesterday).OrderByDescending(rec => rec.Checkin).FirstOrDefault().BadgeNumber
                                })//.ToList(),new EmployeeOnSiteOffsiteModelComparer()
                                );
            var result = tmp.ToList();
            result.ForEach(r =>
            {
                r.Locked = IsLocked(result, r.OnsiteOffsiteID, r.EmployeeID.Value, r.Checkin, r.Checkout);
                //r.HasPermission = _SystemUserBusiness.IsUserHasPermission(SSession.UserId, LocalConstants.Tag_Number_View_Id);
            });
            ViewBag.HasClientEmpPermission = _SystemUserBusiness.IsUserHasPermission(SSession.UserId, LocalConstants.ClientEmployeeId);
            ViewBag.HasPermission = _SystemUserBusiness.IsUserHasPermission(SSession.UserId, LocalConstants.Tag_Number_View_Id);
            ViewBag.HasBadgePermission = _SystemUserBusiness.IsUserHasPermission(SSession.UserId, LocalConstants.Badge_Number_View_Id);
            var orderedResult = result.OrderBy(o => o.LastName).ThenBy(o => o.FirstName).ThenBy(o => o.EmployeeID).ThenBy(o => o.OnsiteOffsiteID.HasValue).ThenByDescending(o => o.Checkin.HasValue ? o.Checkin : o.Checkout).ToList();
            List<EmployeeOnSiteOffsiteModel> sanitizedResult = Sanitize(orderedResult, workingOffset);
            return Json(sanitizedResult);
        }

        private List<System.Guid> GetRequiredTrainings(long clientID, long locID)
        {

            //Get a list of ClientTemplateIDs for client where ClientBusinessUnitSiteId = locid or ClientBusinessUnitSiteId = null

            var results = from ct in entityDB.ClientTemplates
                          join ctu in entityDB.ClientTemplatesForBU on ct.ClientTemplateID equals ctu.ClientTemplateId
                          join template in entityDB.Templates on ct.TemplateID equals template.TemplateID
                          join s in entityDB.ClientTemplatesForBUSites on ctu.ClientTemplateForBUId equals s.ClientTemplateForBUId into g
                          from oj in g.DefaultIfEmpty()
                          where (template.TemplateType == 1 && ct.ClientID == clientID && (oj.ClientBusinessUnitSiteId == locID))
                          select ct.ClientTemplateID;
            return results.ToList();


        }

        private List<EmployeeOnSiteOffsiteModel> Sanitize(List<EmployeeOnSiteOffsiteModel> list, int workingOffset)
        {

            for (int i = 0; i < list.Count; i++)
            {

                ////correct for localtimezone
                //if (list[i].Checkin.HasValue)
                //{
                //    list[i].Checkin = list[i].Checkin.Value.AddHours(workingOffset * -1 );
                //}
                //if (list[i].Checkout.HasValue)
                //{
                //    list[i].Checkout = list[i].Checkout.Value.AddHours(workingOffset * -1);
                //}

                //If this is an empty row lets see if we want to keep it.
                if (!list[i].Checkin.HasValue && !list[i].Checkout.HasValue)
                {
                    //If this is the last item leave it alone
                    if (i + 1 == list.Count) { continue; }
                    //if the next item is a different employee leave it alone
                    if (list[i].EmployeeID != list[i + 1].EmployeeID) { continue; }

                    //If the next item Checkin is from a prior day leave it alone
                    if (list[i + 1].Checkin.HasValue && (list[i + 1].Checkin.Value.Date < DateTime.Now.Date)) { continue; }

                    //If the next items Checkout is from a prior day leave it alone
                    if (list[i + 1].Checkout.HasValue && (list[i + 1].Checkout.Value.Date < DateTime.Now.Date)) { continue; }

                    //If after all this Checkout is null on the next record then set EmployeeID to null (We will remove these)
                    if (!list[i + 1].Checkout.HasValue)
                    {
                        list[i].EmployeeID = null;
                    }

                }
            }
            list.RemoveAll(r => r.EmployeeID == null);
            return list;
        }

       
        private bool IsLocked(List<EmployeeOnSiteOffsiteModel> allRecords, Guid? onsiteOffsiteID, Guid employeeID, DateTime? checkin, DateTime? checkout)
        {
            if (checkin.HasValue || checkout.HasValue)
            {
                //If there is now checkin date than
                if (checkin.HasValue)
                {
                    if (checkin.Value.Date < DateTime.Now.Date) { return true; }
                }
                else
                {
                    //if (checkout.Value.Date < DateTime.Now.Date) { return true; }
                }

                if (allRecords.FirstOrDefault(r => r.EmployeeID == employeeID && r.OnsiteOffsiteID != onsiteOffsiteID && (
                        ((r.Checkin.HasValue && checkin.HasValue && r.Checkin.Value > checkin)
                        || (r.Checkout.HasValue && checkout.HasValue && r.Checkout.Value > checkout)
                        || (r.Checkin.HasValue && checkout.HasValue && r.Checkin.Value > checkout)
                        || (r.Checkout.HasValue && checkin.HasValue && r.Checkout.Value > checkin)
                        ))) != null)
                {
                    return true;
                }

                return false;
            }


            return false;
        }

        private string GetPassedTraining(List<Guid> requiredTrainings, Guid userID, Int32 clientID)
        {
            if (requiredTrainings.Count == 0) { return "N/A"; }

            //Get a List of all passed trainings for user
            //select ClientTemplateId,* From SystemUsersOrganizationsQuizzes q
            //join SystemUsersOrganizations u on q.SysUserOrgId = u.SysUserOrganizationId
            //where quizresult = 1 and QuizExpirationDate >= GetDate()
            //and ClientId=1302 and UserId  = '{f0113d44-ff9d-46b0-bc41-0fef6fe8e053}'
            List<Guid?> passedTrainings = (from q in entityDB.SystemUsersOrganizationsQuizzes
                                           join u in entityDB.SystemUsersOrganizations on q.SysUserOrgId equals u.SysUserOrganizationId
                                           where (q.QuizResult == true && q.ClientId == clientID && u.UserId == userID && q.QuizExpirationDate >= DateTime.Now)
                                           select q.ClientTemplateId).ToList();
            //Loop through requiredTrainings
            var result = "Yes";
            requiredTrainings.ForEach(r =>
            {

                if (passedTrainings.Where(p => p.Value == r).Count() == 0) { result = "No"; }

            });
            return result;
            //in the loop look for a mathcing passed user training if there is no match return false
            //If I make it through (all have matched) return true

            //select * from SystemUsersOrganizationsQuizzes
            //select * From SystemUsersOrganizations


            //var items = entityDB.SystemUsersOrganizations
            //    .Join(entityDB.SystemUsersOrganizationsQuizzes, suo => suo.SysUserOrganizationId,
            //    q => q.SysUserOrgId, (suo, q) => new { suo, q })
            //    .Where(w => w.suo.UserId == userID && w.q.ClientId == clientID)
            //.Select(s => s.q).ToList();

            //if (items.Count == 0)
            //{
            //    return "No";
            //}
            ////TODO - this is wrong. I need to find out what the trainings are and then make sure they have a valid copy of each. Maybe pass this info in to reduce DB calls.

            ////Take in a list of required trainings  (passed in)
            ////Loop through list and then check items for a valid passed test. 
            ////If 
            //string result = "Yes";
            //items.ForEach(t =>
            //{
            //    if ((!t.QuizResult.HasValue || !t.QuizResult.Value)
            //        || (t.QuizDateSubmitted == null)
            //        || (t.QuizExpirationDate < DateTime.Now))
            //    { result = "No"; }
            //});
            //return result;
        }
        public JsonResult Checkin(Guid employeeID, Int32 vendorID, Int32 buID, Int32 locID, int timeZoneOffset, int? tagid, int? badgeid,int UserType,int? DepartmentId)
        {
            var serverOffset = TimeZoneInfo.Local.GetUtcOffset(DateTime.UtcNow);
            //var workingOffset = (timeZoneOffset / 60) + serverOffset.Hours;


            var newCheckin = new FVGen3.Domain.Entities.OnsiteOffsite();
            newCheckin.OnsiteOffsiteID = Guid.NewGuid();
            newCheckin.Checkin = DateTime.Now;
            newCheckin.CheckinClientUser = (Guid)Session["UserId"];
            newCheckin.VendorEmployeeID = employeeID;
            newCheckin.VendorID = vendorID;
            newCheckin.ClientID = Convert.ToInt32(Session["currentOrgId"]);
             newCheckin.TagNumber = tagid;
            newCheckin.BadgeNumber = badgeid;
             if (UserType ==2)
            {
                newCheckin.DepartmentId = DepartmentId; 

            }
            else
            {
                newCheckin.ClientBusinessUnitID = buID;
                newCheckin.ClientBusinessUnitSiteID = locID;
            }
            newCheckin.UserType = UserType;
            entityDB.OnsiteOffsites.Add(newCheckin);
            entityDB.SaveChanges();
            var Systemuser = entityDB.SystemUsers.Find(employeeID);
            Systemuser.TagNumber = tagid;
            Systemuser.Badgenumber = badgeid;
            entityDB.Entry(Systemuser).State = EntityState.Modified;
            entityDB.SaveChanges();
            return Json("Success");
        }


        public JsonResult CheckinVisitor(Guid employeeID, Int32 buID, Int32 locID, int timeZoneOffset, int? tagid, int? badgeid)
        {
            var serverOffset = TimeZoneInfo.Local.GetUtcOffset(DateTime.UtcNow);
            var workingOffset = (timeZoneOffset / 60) + serverOffset.Hours;


            var newCheckin = new FVGen3.Domain.Entities.OnsiteOffsiteVisitor();
            newCheckin.OnsiteOffsiteID = Guid.NewGuid();
            newCheckin.Checkin = DateTime.Now;
            newCheckin.CheckinClientUser = (Guid)Session["UserId"];
            newCheckin.VisitorID = employeeID;
            newCheckin.ClientID = Convert.ToInt32(Session["currentOrgId"]);
            newCheckin.ClientBusinessUnitID = buID;
            newCheckin.ClientBusinessUnitSiteID = locID;
            newCheckin.TagNumber = tagid;
            newCheckin.BadgeNumber = badgeid;
            entityDB.OnsiteOffsiteVisitor.Add(newCheckin);
            entityDB.SaveChanges();

            return Json("Success");
        }
        public JsonResult Checkout(Guid? OnsiteOffsiteID, Guid employeeID, Int32 vendorID, Int32 buID, Int32 locID, int timeZoneOffset)
        {
            var serverOffset = TimeZoneInfo.Local.GetUtcOffset(DateTime.UtcNow);
            var workingOffset = (timeZoneOffset / 60) + serverOffset.Hours;

            if (OnsiteOffsiteID.HasValue)
            {
                var checkinRecord = entityDB.OnsiteOffsites.Find(OnsiteOffsiteID);
                checkinRecord.Checkout = DateTime.Now;
                checkinRecord.CheckoutClientUser = (Guid)Session["UserId"];
                entityDB.SaveChanges();
            }
            else
            {
                var newCheckin = new FVGen3.Domain.Entities.OnsiteOffsite();
                newCheckin.OnsiteOffsiteID = Guid.NewGuid();
                newCheckin.Checkout = DateTime.Now;
                newCheckin.CheckoutClientUser = (Guid)Session["UserId"];
                newCheckin.VendorEmployeeID = employeeID;
                newCheckin.VendorID = vendorID;
                newCheckin.ClientID = Convert.ToInt32(Session["currentOrgId"]);
                newCheckin.ClientBusinessUnitID = buID;
                newCheckin.ClientBusinessUnitSiteID = locID;
                entityDB.OnsiteOffsites.Add(newCheckin);
                entityDB.SaveChanges();
            }
            return Json("Success");
        }

        public JsonResult CheckoutVisitor(Guid? OnsiteOffsiteID, Guid employeeID, Int32 buID, Int32 locID, int timeZoneOffset)
        {
            var serverOffset = TimeZoneInfo.Local.GetUtcOffset(DateTime.UtcNow);
            var workingOffset = (timeZoneOffset / 60) + serverOffset.Hours;

            if (OnsiteOffsiteID.HasValue)
            {
                var checkinRecord = entityDB.OnsiteOffsiteVisitor.Find(OnsiteOffsiteID);
                checkinRecord.Checkout = DateTime.Now;
                checkinRecord.CheckoutClientUser = (Guid)Session["UserId"];
                entityDB.SaveChanges();
            }
            else
            {
                var newCheckin = new FVGen3.Domain.Entities.OnsiteOffsiteVisitor();
                newCheckin.OnsiteOffsiteID = Guid.NewGuid();
                newCheckin.Checkout = DateTime.Now;
                newCheckin.CheckoutClientUser = (Guid)Session["UserId"];
                newCheckin.VisitorID = employeeID;
                newCheckin.ClientID = Convert.ToInt32(Session["currentOrgId"]);
                newCheckin.ClientBusinessUnitID = buID;
                newCheckin.ClientBusinessUnitSiteID = locID;
                entityDB.OnsiteOffsiteVisitor.Add(newCheckin);
                entityDB.SaveChanges();
            }
            return Json("Success");
        }

        public JsonResult Reset(Guid OnsiteOffsiteID)
        {
            var checkinRecord = entityDB.OnsiteOffsites.Find(OnsiteOffsiteID);
            if (checkinRecord.Checkout.HasValue && checkinRecord.Checkin.HasValue)
            {
                checkinRecord.Checkout = null;
                checkinRecord.CheckoutClientUser = null;
                entityDB.SaveChanges();
            }
            else
            {
                entityDB.OnsiteOffsites.Remove(checkinRecord);
                entityDB.SaveChanges();
            }
            var Employee = entityDB.SystemUsers.Find(checkinRecord.VendorEmployeeID);
            if (Employee != null)
            {
                Employee.TagNumber = 0;
                Employee.Badgenumber = 0;
                entityDB.SaveChanges();
            }
            return Json("Success");
        }
        public JsonResult ResetVisitor(Guid OnsiteOffsiteID)
        {
            var checkinRecord = entityDB.OnsiteOffsiteVisitor.Find(OnsiteOffsiteID);
            if (checkinRecord.Checkout.HasValue && checkinRecord.Checkin.HasValue)
            {
                checkinRecord.Checkout = null;
                checkinRecord.CheckoutClientUser = null;
                entityDB.SaveChanges();
            }
            else
            {
                entityDB.OnsiteOffsiteVisitor.Remove(checkinRecord);
                entityDB.SaveChanges();
            }
            return Json("Success");
        }
        public ActionResult SineproData()
        {
            var clientID = Convert.ToInt32(Session["currentOrgId"]);
            var data = _onsiteOffSite.GetSineProOnsiteData(clientID);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetPrequalID(int buID, int locID, int vendorID, Guid EmployeeID)
        {
            var clientID = Convert.ToInt32(Session["currentOrgId"]);
            var result = entityDB.LatestPrequalification.Where(w => w.ClientId == clientID && w.VendorId == vendorID).OrderByDescending(o => o.PrequalificationStart)
                .Join(entityDB.ClientTemplates, p => p.ClientTemplateId, c => c.ClientTemplateID, (p, c) => new { p, c })
                .Select(s => new
                {
                    templateID = s.c.TemplateID,
                    prequalificationID = s.p.PrequalificationId
                }).FirstOrDefault();
            return Json(result);
        }
        private DateTime StartOfWeek(DateTime dt, DayOfWeek startOfWeek)
        {
            int diff = dt.DayOfWeek - startOfWeek;
            if (diff < 0)
            {
                diff += 7;
            }
            return dt.AddDays(-1 * diff).Date;
        }

    }
    public class EmployeeOnSiteOffsiteModel
    {
        public EmployeeOnSiteOffsiteModel()
        {
            PassedOrientation = "";
            DepartmentName = "";
            UserType = 1;//Default to vendor
        }
        public Guid? OnsiteOffsiteID { get; set; }
        public Guid? EmployeeID { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public String Company { get; set; }
        public String SiteContact { get; set; }
        public String PassedOrientation { get; set; }
        public DateTime? Checkin { get; set; }
        public DateTime? Checkout { get; set; }
        public Int32? ClientID { get; set; }
        public Int64? VendorID { get; set; }
        public Int32? TagNumber { get; set; }
        public Int32? BadgeNumber { get; set; }
        public bool Locked { get; set; }
        public string VendorName { get; set; }
        public string PQStatus { get; set; }
        public int? OldTagId { get; set; }
        public int? OldBadgeId { get; set; }
        public int UserType { get; set; } //1-vendor 2-client
        public int? DepartmentId { get; set; }
        public string DepartmentName { get; set; }
      
    }
    class EmployeeOnSiteOffsiteModelComparer : IEqualityComparer<EmployeeOnSiteOffsiteModel>
    {
        public bool Equals(EmployeeOnSiteOffsiteModel x, EmployeeOnSiteOffsiteModel y)
        {
            return x.EmployeeID == y.EmployeeID && x.OnsiteOffsiteID == x.OnsiteOffsiteID;
        }

        // If Equals() returns true for a pair of objects 
        // then GetHashCode() must return the same value for these objects.

        public int GetHashCode(EmployeeOnSiteOffsiteModel myModel)
        {
            return myModel.EmployeeID.GetHashCode();
        }
      
    }
    
}
