﻿/*
Page Created Date:  29/04/2013
Created By: Rajesh Pendela
Purpose:
Version: 1.0
****************************************************
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FVGen3.BusinessLogic.Interfaces;
using FVGen3.Domain.Abstract;
using FVGen3.Domain.Entities;

namespace FVGen3.WebUI.Controllers
{
    public class OrganizationController : Controller
    {
        private IOrganizationRepository repository;
        private IOrganizationBusiness _orgBusiness;

        public OrganizationController(IOrganizationRepository orgRepository, IOrganizationBusiness organizationsBusiness) 
        {
            this.repository = orgRepository;
            _orgBusiness = organizationsBusiness;
        }

        public ViewResult Index()
        {
            return View(repository.Organization);
        }

        public ActionResult GetOrganizationSuperUser(long orgId)
        {
            return Json(_orgBusiness.GetSuperuser(orgId));
        }

    }
}
