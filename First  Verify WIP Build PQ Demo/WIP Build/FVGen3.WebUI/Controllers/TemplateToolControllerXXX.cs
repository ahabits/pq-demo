﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FVGen3.WebUI.Annotations;
using FVGen3.Domain.Abstract;
using FVGen3.Domain.Concrete;
using FVGen3.Domain.Entities;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;

namespace FVGen3.WebUI.Controllers
{
    public class TemplateToolController : BaseController
    {
        //
        // GET: /TemplateTool/
        EFDbContext entityDB;

        public TemplateToolController(ISystemUsersRepository repositorySystemUsers) 
        {
            
            entityDB = new EFDbContext();
            
        }

        //Siva on Nov 11th
        public virtual JsonResult uploadImage(HttpPostedFileBase file)
        {
            if (file != null && file.ContentLength > 0)
            {
                //SAVE file
                var fileName = Path.GetFileName(file.FileName);
                var extension = Path.GetExtension(file.FileName);

                Directory.CreateDirectory(Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["HeaderImagesPath"])));
                var path = Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["HeaderImagesPath"]), fileName + extension);
                file.SaveAs(path);

                Random randomObj = new Random();
                long randorNum = randomObj.Next(1, 1000000);
                string newFilePath = Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["HeaderImagesPath"]), randorNum + extension);
                if (!System.IO.File.Exists(newFilePath))
                {
                    System.IO.File.Move(path, newFilePath);
                }
                else
                {
                    randorNum = randomObj.Next(1, 1000000);
                    newFilePath = Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["HeaderImagesPath"]), randorNum + extension);
                    System.IO.File.Move(path, newFilePath);
                }

                string[] data = new string[1];
                data[0] = ConfigurationSettings.AppSettings["HeaderImagesPath"] + "/" + randorNum + extension;
                return Json(data);
                //return "docPath:" + ;
            }

            string[] response = new string[1];
            response[0] = "doc upload failed";
            return Json(response);
        }
        //End on Nov 11th

        //Rajesh changes on 12/19/2013
        [HttpPost]
        public ActionResult GetTemplates(int type, long? client)//0-custom 1-standard
        {
            var Data = (from Template in entityDB.Templates.Where(rec => rec.DefaultTemplate == true && rec.TemplateType == 0 && rec.TemplateStatus == 1).ToList()
                        select new SelectListItem
                        {
                            Text = Template.TemplateName,
                            Value = Template.TemplateID + ""
                        }).ToList();
            if (type == 0)
                Data = (from Template in entityDB.ClientTemplates.Where(rec => rec.ClientID == client && rec.Templates.DefaultTemplate == false && rec.Templates.TemplateType == 0 && rec.Templates.TemplateStatus == 1).ToList()
                        select new SelectListItem
                        {
                            Text = Template.Templates.TemplateName,
                            Value = Template.TemplateID + ""
                        }).ToList();
            return Json(Data);
        }
        //Ends<<<
        
        [HeaderAndSidebar(headerName = "QuestionnaireTool", sideMenuName = "CustomTemplateTool", ViewName = "CreateCustomTemplate")]
        [SessionExpire]
        public ActionResult CreateCustomTemplate()
        {
            var templates = entityDB.Templates.Where(tempRec => tempRec.TemplateType == 0 && tempRec.DefaultTemplate == false).ToList();//Rajesh on 10/28/2013
            return View(templates);

        }
        [HttpGet]
        [HeaderAndSidebar(headerName = "QuestionnaireTool", sideMenuName = "CustomTemplateTool", ViewName = "AddOrEditCustomTemplate")]
        public ActionResult AddOrEditCustomTemplate(Guid? TemplateId)
        {
            LocalTemplateModel template = new LocalTemplateModel();
            if (TemplateId != null)
            {
                Templates mainTemp = entityDB.Templates.Find(TemplateId);
                template.TemplateName = mainTemp.TemplateName;
                template.Templateid = mainTemp.TemplateID;
                template.TemplateNote = mainTemp.TemplateNotes;

                template.TemplateCode = mainTemp.TemplateCode;
            }

            ViewBag.Clients = (from clients in entityDB.Organizations.Where(rec => rec.OrganizationType == "Client").ToList()
                               select new SelectListItem()
                               {
                                   Text = clients.Name,
                                   Value = clients.OrganizationID + ""
                               }).ToList();
            ViewBag.Templates = (from temp in entityDB.Templates.Where(rec => rec.DefaultTemplate == false && rec.TemplateType == 1).ToList()
                                 select new SelectListItem()
                                 {
                                     Text = temp.TemplateName,
                                     Value = temp.TemplateID + ""
                                 }).ToList();
            return View(template);
        }
        [HttpPost]
        [HeaderAndSidebar(headerName = "QuestionnaireTool", sideMenuName = "CustomTemplateTool", ViewName = "AddOrEditCustomTemplate")]
        public ActionResult AddOrEditCustomTemplate(LocalTemplateModel Form)
        {

            ViewBag.Clients = (from clients in entityDB.Organizations.Where(rec => rec.OrganizationType == "Client").ToList()
                               select new SelectListItem()
                               {
                                   Text = clients.Name,
                                   Value = clients.OrganizationID + ""
                               }).ToList();
            ViewBag.Templates = (from temp in entityDB.Templates.Where(rec => rec.DefaultTemplate == false && rec.TemplateType == 1).ToList()
                                 select new SelectListItem()
                                 {
                                     Text = temp.TemplateName,
                                     Value = temp.TemplateID + ""
                                 }).ToList();
            if (!ModelState.IsValid)
                return View(Form);
            Templates newTemplate = new Templates();
            if (Form.Templateid != null)//Update Template
            {
                newTemplate = entityDB.Templates.Find(Form.Templateid);
                newTemplate.TemplateName = Form.TemplateName;
                newTemplate.TemplateNotes = Form.TemplateNote;
                newTemplate.TemplateCode = Form.TemplateCode;
                entityDB.Entry(newTemplate).State = EntityState.Modified;
            }
            else//Save Template
            {
                newTemplate.TemplateCode = Form.TemplateCode;
                newTemplate.TemplateName = Form.TemplateName;
                newTemplate.TemplateType = 0;
                newTemplate.TemplateNotes = Form.TemplateNote;
                newTemplate.DefaultTemplate = false;
                newTemplate.TemplateStatus = 0;

                ClientTemplates clientTemplate = new ClientTemplates();
                clientTemplate.ClientID = Convert.ToInt64(Form.ClientId);
                clientTemplate.DisplayOrder = 1;
                clientTemplate.Visible = true;
                clientTemplate.DefaultTemplate = false;
                newTemplate.ClientTemplates = new List<ClientTemplates>();
                newTemplate.ClientTemplates.Add(clientTemplate);



                if (Form.isCopyTemplateSelected)//Copy The Template
                {
                    Guid? selectedTemplateId = null;
                    try { selectedTemplateId = new Guid(Form.CopyTemplateId); }
                    catch { }
                    if (selectedTemplateId != null)
                    {
                        Templates selectedTemplate = entityDB.Templates.Find(selectedTemplateId);
                        if (selectedTemplate != null)
                        {
                            //if (Form.isSectionSelected)//Adding sections
                            {
                                newTemplate.TemplateSections = new List<TemplateSections>();

                                foreach (var sections in selectedTemplate.TemplateSections)
                                {
                                    TemplateSections newSection = new TemplateSections();
                                    newSection.DisplayOrder = sections.DisplayOrder;
                                    newSection.Visible = sections.Visible;
                                    newSection.SectionName = sections.SectionName;
                                    newSection.TemplateSectionType = sections.TemplateSectionType;
                                    newSection.VisibleToClient = sections.VisibleToClient;
                                    newTemplate.TemplateSections.Add(newSection);
                                    newSection.TemplateSubSections = new List<TemplateSubSections>();

                                    if (sections.TemplateSectionType == 3 || sections.TemplateSectionType == 4 && sections.TemplateSectionType == 5 && sections.TemplateSectionType == 6)
                                    {
                                        var subSections = sections.TemplateSubSections.ToList();
                                        foreach (var subSection in subSections)
                                        {
                                            TemplateSubSections newSubSection = new TemplateSubSections();
                                            newSubSection.SubSectionType = subSection.SubSectionType;
                                            newSubSection.SubSectionName = subSection.SubSectionName;
                                            newSubSection.DisplayOrder = subSection.DisplayOrder;
                                            newSubSection.Visible = subSection.Visible;
                                            newSubSection.SubHeader = subSection.SubHeader;
                                            newSubSection.SectionNotes = subSection.SectionNotes;
                                            newSubSection.NoOfColumns = subSection.NoOfColumns;
                                            newSubSection.SubSectionTypeCondition = subSection.SubSectionTypeCondition;

                                            newSection.TemplateSubSections.Add(newSubSection);
                                        }
                                        continue;
                                    }
                                    
                                    //Adding Headers
                                    newSection.TemplateSectionsHeaderFooter = new List<TemplateSectionsHeaderFooter>();
                                    foreach (var HorF in sections.TemplateSectionsHeaderFooter.ToList())
                                    {
                                        TemplateSectionsHeaderFooter newHorF = new TemplateSectionsHeaderFooter();
                                        newHorF.PositionType = HorF.PositionType;
                                        newHorF.PositionTitle = HorF.PositionTitle;
                                        newHorF.PostionContent = HorF.PostionContent;
                                        newHorF.DisplayImages = HorF.DisplayImages;
                                        newHorF.AttachmentExist = HorF.AttachmentExist;
                                        newSection.TemplateSectionsHeaderFooter.Add(newHorF);
                                    }


                                    if (Form.isSubSectionSelected)//Adding Subsections
                                    {
                                        
                                        var subSections = sections.TemplateSubSections.ToList();
                                        //if (Form.TemplateType == "0")
                                        //{
                                        //    var clientId_Loc = Convert.ToInt64(Form.ClientOfTemplate);
                                        //    subSections = sections.TemplateSubSections.Where(rec => rec.ClientId == clientId_Loc).ToList();
                                        //}
                                        foreach (var subSection in subSections)
                                        {
                                            TemplateSubSections newSubSection = new TemplateSubSections();
                                            newSubSection.SubSectionType = subSection.SubSectionType;
                                            newSubSection.SubSectionName = subSection.SubSectionName;
                                            newSubSection.DisplayOrder = subSection.DisplayOrder;
                                            newSubSection.Visible = subSection.Visible;
                                            newSubSection.SubHeader = subSection.SubHeader;
                                            newSubSection.SectionNotes = subSection.SectionNotes;
                                            newSubSection.NoOfColumns = subSection.NoOfColumns;
                                            newSubSection.SubSectionTypeCondition = subSection.SubSectionTypeCondition;

                                            newSection.TemplateSubSections.Add(newSubSection);

                                            if (Form.isQuestionsSelected && sections.TemplateSectionType != 4 && sections.TemplateSectionType != 5 && sections.TemplateSectionType != 6)//To prevent reporting data
                                            {
                                                newSubSection.Questions = new List<Questions>();
                                                foreach (var question in subSection.Questions)
                                                {
                                                    Questions newQuestion = new Questions();
                                                    newQuestion.QuestionText = question.QuestionText;
                                                    newQuestion.DisplayOrder = question.DisplayOrder;
                                                    newQuestion.Visible = question.Visible;
                                                    newQuestion.NumberOfColumns = question.NumberOfColumns;
                                                    newQuestion.IsMandatory = question.IsMandatory;
                                                    newQuestion.IsBold = question.IsBold;
                                                    newQuestion.ResponseRequired = question.ResponseRequired;
                                                    newQuestion.DisplayResponse = question.DisplayResponse;
                                                    newQuestion.HasDependantQuestions = question.HasDependantQuestions;
                                                    newQuestion.QuestionBankId = question.QuestionBankId;
                                                    newSubSection.Questions.Add(newQuestion);

                                                    //Rajesh on 12/24/2013
                                                    newQuestion.QuestionColumnDetails = new List<QuestionColumnDetails>();
                                                    newQuestion.QuestionsDependants = new List<QuestionsDependants>();

                                                    foreach (var Qdetails in question.QuestionColumnDetails.ToList())
                                                    {
                                                        QuestionColumnDetails newQDetails = new QuestionColumnDetails();
                                                        //newQDetails.QuestionId = newQuestion.QuestionID;
                                                        newQDetails.QuestionControlTypeId = Qdetails.QuestionControlTypeId;
                                                        newQDetails.ColumnNo = Qdetails.ColumnNo;
                                                        newQDetails.ColumnValues = Qdetails.ColumnValues;
                                                        newQDetails.DisplayType = Qdetails.DisplayType;
                                                        newQDetails.TableReference = Qdetails.TableReference;
                                                        newQDetails.TableReferenceField = Qdetails.TableReferenceField;
                                                        newQDetails.TableRefFieldValueDisplay = Qdetails.TableRefFieldValueDisplay;
                                                        newQDetails.DependantColumnNo = Qdetails.DependantColumnNo;
                                                        newQDetails.QFormula = Qdetails.QFormula;
                                                        newQuestion.QuestionColumnDetails.Add(newQDetails);
                                                        //entityDB.QuestionColumnDetails.Add(newQDetails);

                                                    }
                                                    foreach (var QDependent in question.QuestionsDependants.ToList())
                                                    {
                                                        QuestionsDependants newQDependent = new QuestionsDependants();
                                                        //newQDependent.QuestionID = newQuestion.QuestionID;
                                                        newQDependent.DependantType = QDependent.DependantType;
                                                        newQDependent.DependantId = QDependent.DependantId;
                                                        newQDependent.DependantValue = QDependent.DependantValue;
                                                        newQDependent.DependantDisplayOrder = QDependent.DependantDisplayOrder;
                                                        //entityDB.QuestionsDependants.Add(newQDependent);
                                                        newQuestion.QuestionsDependants.Add(newQDependent);
                                                    }
                                                    //Ends<<

                                                }
                                            }

                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                entityDB.Templates.Add(newTemplate);
            }
            entityDB.SaveChanges();
            return Redirect("PopUpCloseView");
        }


        [SessionExpire]
        [HeaderAndSidebar(headerName = "QuestionnaireTool", sideMenuName = "TemplateTool", ViewName = "TemplatesAndSections")]
        public ActionResult TemplatesAndSections()
        {
            var templates = entityDB.Templates.Where(tempRec => tempRec.TemplateType == 0 && tempRec.DefaultTemplate == true).ToList();//Rajesh on 10/28/2013
            return View(templates);
        }

        [HttpGet]
        [SessionExpire]
        [HeaderAndSidebar(headerName = "QuestionnaireTool", sideMenuName = "TemplateTool", ViewName = "AddOrEditTemplate")]
        public ActionResult AddOrEditTemplate(Guid? TemplateId)
        {
            LocalTemplateModel template= new LocalTemplateModel();
            if (TemplateId != null)
            {
                Templates mainTemp = entityDB.Templates.Find(TemplateId);
                template.TemplateName = mainTemp.TemplateName;
                template.Templateid = mainTemp.TemplateID;
                template.TemplateNote = mainTemp.TemplateNotes;
                template.TemplateCode = mainTemp.TemplateCode;
            }

            ViewBag.Clients = (from clients in entityDB.Organizations.Where(rec => rec.OrganizationType == "Client").ToList()
                              select new SelectListItem()
                              {
                                  Text=clients.Name,
                                  Value=clients.OrganizationID+""
                              }).ToList();
            ViewBag.Templates = (from temp in entityDB.Templates.Where(rec => rec.DefaultTemplate == true).ToList()
                               select new SelectListItem()
                               {
                                   Text = temp.TemplateName,
                                   Value = temp.TemplateID+ ""
                               }).ToList();
            return View(template);
        }
        [HttpPost]
        [SessionExpire]
        [HeaderAndSidebar(headerName = "QuestionnaireTool", sideMenuName = "TemplateTool", ViewName = "AddOrEditTemplate")]
        public ActionResult AddOrEditTemplate(LocalTemplateModel Form)
        {

            ViewBag.Clients = (from clients in entityDB.Organizations.Where(rec => rec.OrganizationType == "Client").ToList()
                               select new SelectListItem()
                               {
                                   Text = clients.Name,
                                   Value = clients.OrganizationID + ""
                               }).ToList();
            ViewBag.Templates = (from temp in entityDB.Templates.Where(rec=>rec.DefaultTemplate==true).ToList()
                                 select new SelectListItem()
                                 {
                                     Text = temp.TemplateName,
                                     Value = temp.TemplateID + ""
                                 }).ToList();
            if (!ModelState.IsValid)
                return View(Form);
            Templates newTemplate = new Templates();
            if (Form.Templateid != null)//Update Template
            {
                newTemplate = entityDB.Templates.Find(Form.Templateid);
                newTemplate.TemplateName = Form.TemplateName;
                newTemplate.TemplateNotes = Form.TemplateNote;
                newTemplate.TemplateCode = Form.TemplateCode;
                entityDB.Entry(newTemplate).State = EntityState.Modified;
            }
            else//Save Template
            {
                newTemplate.TemplateCode = Form.TemplateCode;
                newTemplate.TemplateName = Form.TemplateName;
                newTemplate.TemplateType = 0;
                newTemplate.TemplateNotes = Form.TemplateNote;
                newTemplate.DefaultTemplate = true;
                newTemplate.TemplateStatus = 0;
                //if (Form.TemplateType.Equals("1"))//Means he selected Custom Template
                //{
                //    ClientTemplates clientTemplate=new ClientTemplates();
                //    clientTemplate.ClientID = Convert.ToInt64(Form.ClientId);
                //    clientTemplate.DisplayOrder = 1;
                //    clientTemplate.Visible = true;
                //    clientTemplate.DefaultTemplate = true;
                //    newTemplate.ClientTemplates = new List<ClientTemplates>();
                //    newTemplate.ClientTemplates.Add(clientTemplate);
                //}

                
                //if (Form.isCopyTemplateSelected)//Copy The Template
                //{
                //    Guid? selectedTemplateId=null;
                //    try { selectedTemplateId = new Guid(Form.CopyTemplateId); }
                //    catch { }
                //    if (selectedTemplateId != null)
                //    {
                //        Templates selectedTemplate = entityDB.Templates.Find(selectedTemplateId);
                //        if (selectedTemplate != null)
                //        {
                //            if (Form.isSectionSelected)//Adding sections
                //            {
                //                newTemplate.TemplateSections = new List<TemplateSections>();
                                
                //                foreach (var sections in selectedTemplate.TemplateSections)
                //                {
                //                    TemplateSections newSection = new TemplateSections();
                //                    newSection.DisplayOrder = sections.DisplayOrder;
                //                    newSection.Visible = sections.Visible;
                //                    newSection.SectionName = sections.SectionName;
                //                    newSection.TemplateSectionType = sections.TemplateSectionType;
                //                    newSection.VisibleToClient = sections.VisibleToClient;
                //                    newTemplate.TemplateSections.Add(newSection);

                                    
                //                    if (Form.isSubSectionSelected)//Adding Subsections
                //                    {
                //                        newSection.TemplateSubSections = new List<TemplateSubSections>();
                //                        foreach (var subSection in sections.TemplateSubSections)
                //                        {
                //                            TemplateSubSections newSubSection = new TemplateSubSections();
                //                            newSubSection.SubSectionType = subSection.SubSectionType;
                //                            newSubSection.SubSectionName = subSection.SubSectionName;
                //                            newSubSection.DisplayOrder = subSection.DisplayOrder;
                //                            newSubSection.Visible = subSection.Visible;
                //                            newSubSection.SubHeader = subSection.SubHeader;
                //                            newSubSection.SectionNotes = subSection.SectionNotes;
                //                            newSubSection.NoOfColumns = subSection.NoOfColumns;
                //                            newSubSection.SubSectionTypeCondition = subSection.SubSectionTypeCondition;

                //                            newSection.TemplateSubSections.Add(newSubSection);

                //                            if (Form.isQuestionsSelected)//Adding Questions
                //                            {
                //                                newSubSection.Questions = new List<Questions>();
                //                                foreach (var question in subSection.Questions)
                //                                {
                //                                    Questions newQuestion = new Questions();
                //                                    newQuestion.QuestionText = question.QuestionText;
                //                                    newQuestion.DisplayOrder = question.DisplayOrder;
                //                                    newQuestion.Visible = question.Visible;
                //                                    newQuestion.NumberOfColumns = question.NumberOfColumns;
                //                                    newQuestion.IsMandatory = question.IsMandatory;
                //                                    newQuestion.IsBold = question.IsBold;
                //                                    newQuestion.ResponseRequired = question.ResponseRequired;
                //                                    newQuestion.DisplayResponse = question.DisplayResponse;
                //                                    newQuestion.HasDependantQuestions = question.HasDependantQuestions;
                //                                    newSubSection.Questions.Add(newQuestion);
                //                                }
                //                            }

                //                        }
                //                    }
                //                }
                //            }
                //        }
                //    }
                //}
                entityDB.Templates.Add(newTemplate);
            }
            entityDB.SaveChanges();
            return Redirect("PopUpCloseView");
        }

        [HttpGet]
        public string DeleteTemplate(Guid TemplateId)
        {
            var template = entityDB.Templates.Find(TemplateId);

            if (template.ClientTemplates != null)
            {
                foreach(var Ctemplate in template.ClientTemplates.ToList())
                {
                    foreach(var CTData in Ctemplate.ClientTemplateReportingData.ToList())
                        entityDB.Entry(CTData).State = EntityState.Deleted;
                    
                    entityDB.Entry(Ctemplate).State = EntityState.Deleted;
                }
            }
            if (template.TemplateSections != null)
                foreach (var Tsection in template.TemplateSections.ToList())
                {
                    if (Tsection.TemplateSectionsHeaderFooter != null)
                        foreach (var HandF in Tsection.TemplateSectionsHeaderFooter.ToList())
                        {
                            entityDB.Entry(HandF).State = EntityState.Deleted;
                        }
                    if (Tsection.TemplateSubSections != null)
                        foreach (var Ssection in Tsection.TemplateSubSections.ToList())
                        {

                            if (Ssection.Questions != null)
                                foreach (var question in Ssection.Questions.ToList())//Delete Questions
                                {
                                    if (question.QuestionColumnDetails != null)
                                        foreach (var Qcolumn in question.QuestionColumnDetails.ToList())
                                            entityDB.Entry(Qcolumn).State = EntityState.Deleted;
                                    if (question.QuestionsDependants != null)
                                    {
                                        foreach (var Qdependent in question.QuestionsDependants.ToList())
                                            entityDB.Entry(Qdependent).State = EntityState.Deleted;
                                    }
                                    entityDB.Entry(question).State = EntityState.Deleted;
                                }
                            entityDB.Entry(Ssection).State = EntityState.Deleted;
                        }
                    entityDB.Entry(Tsection).State = EntityState.Deleted;
                }
            entityDB.Entry(template).State = EntityState.Deleted;
            entityDB.SaveChanges();
            return "OK";
        }

        public ActionResult PopUpCloseView()
        {
            return View();
        }
        public string ChangeTemplateSectionsOrder(string templateSectionIds,Guid TemplateId)
        {
            string result = "Ok" + templateSectionIds + "\n";
            int orderNumber = 0;
            var template = entityDB.Templates.Find(TemplateId);
            if(template!=null)
                foreach (var ids in templateSectionIds.Split('|'))
                {
                    try
                    {
                        long secId = Convert.ToInt64(ids);
                        result += secId;
                        TemplateSections section = template.TemplateSections.ToList().FirstOrDefault(rec => rec.TemplateSectionID == secId);
                        if (section == null)
                            continue;
                        orderNumber++;
                        section.DisplayOrder = orderNumber;
                        entityDB.Entry(section).State = EntityState.Modified;
                    }
                    catch (Exception ee) { result += ee.Message+""; }
                }
            entityDB.SaveChanges();
            return result;
        }
        public string ChangeTemplateSubSectionsOrder(string templateSubSectionIds, long TemplateSecId)
        {
            string result = "Ok" + templateSubSectionIds + "\n";
            int orderNumber = 0;
            var templateSec = entityDB.TemplateSections.Find(TemplateSecId);
            if (templateSec != null)
                foreach (var ids in templateSubSectionIds.Split('|'))
                {
                    try
                    {
                        long secId = Convert.ToInt64(ids);
                        result += secId;
                        TemplateSubSections subSection = templateSec.TemplateSubSections.ToList().FirstOrDefault(rec => rec.SubSectionID == secId);
                        if (subSection == null)
                            continue;
                        orderNumber++;
                        subSection.DisplayOrder = orderNumber;
                        entityDB.Entry(subSection).State = EntityState.Modified;
                    }
                    catch (Exception ee) { result += ee.Message + ""; }
                }
            entityDB.SaveChanges();
            return result;
        }

        [SessionExpireForView]
        [HeaderAndSidebar(headerName = "QuestionnaireTool", sideMenuName = "TemplateTool", ViewName = "AddOrEditSection")]
        public ActionResult AddOrEditSection(Guid TemplateId, long TemplateSectionID)
        {
            LocalSectionsModel section = new LocalSectionsModel();
            section.Templateid = TemplateId;
            section.TemplateSectionID = TemplateSectionID;

            ViewBag.Template = entityDB.Templates.Find(TemplateId);//Rajesh on 10/22/2013

            if (TemplateSectionID != -1)
            {
                var Tsection = entityDB.TemplateSections.Find(TemplateSectionID);
                section.VisibleToClient = Tsection.VisibleToClient==true;
                section.SectionName = Tsection.SectionName;
                section.SectionType = Tsection.TemplateSectionType == null ? 0 : (int)Tsection.TemplateSectionType;
                section.Visible = !(Tsection.Visible == true);
            }
            return View(section);
        }

        [HttpPost]
        [SessionExpireForView]
        [HeaderAndSidebar(headerName = "QuestionnaireTool", sideMenuName = "TemplateTool", ViewName = "AddOrEditSection")]
        public ActionResult AddOrEditSection(LocalSectionsModel Form)
        {
            ViewBag.Template = entityDB.Templates.Find(Form.Templateid);//Rajesh on 10/22/2013
            if (!ModelState.IsValid)
                return View(Form);
            TemplateSections section = new TemplateSections();
            if (Form.TemplateSectionID == -1)//Insert
            {
                section.Visible = true;
                entityDB.TemplateSections.Add(section);

                if (Form.SectionType != 4 && Form.SectionType != 5)//Means others-->ProficiencyCapability,Bidding Interests
                    section.TemplateSectionType = Form.SectionType;
                else
                    section.TemplateSectionType = Convert.ToInt32(Form.OtherSectionTypes);
                try
                {
                    section.DisplayOrder = entityDB.Templates.Find(Form.Templateid).TemplateSections.Count() + 1;
                }
                catch
                {
                    section.DisplayOrder = 1;
                }
            }
            else
            {
                section = entityDB.TemplateSections.Find(Form.TemplateSectionID);
                entityDB.Entry(section).State = EntityState.Modified;
                section.Visible = !Form.Visible;


               
            }

            ViewBag.Template = entityDB.Templates.Find(Form.Templateid);//Rajesh on 10/22/2013
            
           
            section.SectionName = Form.SectionName;
            section.TemplateID = Form.Templateid;

            section.VisibleToClient = Form.VisibleToClient;

            if (Form.TemplateSectionID == -1)//Insert
            {
                if (section.TemplateSubSections == null)
                    section.TemplateSubSections = new List<TemplateSubSections>();
                if (section.TemplateSectionType == 4)//Supporting documents 
                {
                    TemplateSubSections subSec = new TemplateSubSections();
                    subSec.DisplayOrder = 1;
                    subSec.NoOfColumns = 0;
                    subSec.SectionNotes = "";
                    subSec.SubSectionName = "Supporting Documents";
                    subSec.SubSectionType = 1;
                    subSec.SubSectionTypeCondition = "SupportingDocument";
                    subSec.Visible = true;
                    section.TemplateSubSections.Add(subSec);
                }
                else if (section.TemplateSectionType == 7)//BU Sites
                {
                    TemplateSubSections subSec = new TemplateSubSections();
                    subSec.DisplayOrder = 1;
                    subSec.NoOfColumns = 0;
                    subSec.SectionNotes = "";
                    subSec.SubSectionName = "Locations";
                    subSec.SubSectionType = 1;
                    subSec.SubSectionTypeCondition = "SitePrequalification";
                    subSec.Visible = true;
                    section.TemplateSubSections.Add(subSec);
                    
                }
                else if (section.TemplateSectionType == 3)//Payment
                {
                    TemplateSubSections subSec = new TemplateSubSections();
                    subSec.DisplayOrder = 1;
                    subSec.NoOfColumns = 0;
                    subSec.SectionNotes = "";
                    subSec.SubSectionName = "COMPLETE PAYMENT";
                    subSec.SubSectionType = 2;
                    subSec.SubSectionTypeCondition = "PrequalificationPayment";
                    subSec.Visible = true;
                    section.TemplateSubSections.Add(subSec);
                }
                else if (section.TemplateSectionType == 99)//Finalize and submited
                {
                    section.TemplateSectionsHeaderFooter = new List<TemplateSectionsHeaderFooter>();
                    TemplateSectionsHeaderFooter HandF = new TemplateSectionsHeaderFooter();
                    HandF.PositionType = 0;
                    HandF.PositionTitle = "FINALIZE & SUBMIT";
                    HandF.PostionContent = "<div class='contentSub'><p>To submit your prequalification data for review, verify the data you have entered in the previous steps, sign below and click <b>Submit Data.</b></p></div>";
                    section.TemplateSectionsHeaderFooter.Add(HandF);
                    TemplateSubSections subSec = new TemplateSubSections();
                    subSec.DisplayOrder = 1;
                    subSec.NoOfColumns = 0;
                    subSec.SectionNotes = "";
                    subSec.SubSectionName = "Submitted by (must be an officer of the company)";
                    subSec.SubSectionType = 2;
                    subSec.SubSectionTypeCondition = "PrequalificationFinalize";
                    subSec.Visible = true;
                    subSec.SubHeader = false;//Rajesh on 1/11/2014
                    section.TemplateSubSections.Add(subSec);
                }
                //Rajesh on 1/11/2014
                else if (section.TemplateSectionType == 34)//IsExceptionProbationRequest
                {
                    section.TemplateSectionsHeaderFooter = new List<TemplateSectionsHeaderFooter>();
                    TemplateSubSections subSec = new TemplateSubSections();
                    subSec.DisplayOrder = 1;
                    subSec.NoOfColumns = 0;
                    subSec.SectionNotes = "";
                    subSec.SubSectionName = "Vendor Information";
                    subSec.SubSectionType = 2;
                    subSec.SubSectionTypeCondition = "UserDetailsForEP";
                    subSec.Visible = true;
                    subSec.SubHeader = false;
                    section.TemplateSubSections.Add(subSec);

                }
                //Ends<<
            }



            entityDB.SaveChanges();
            return Redirect("PopUpCloseView");
        }
        //Rajesh on 10/21/2013

        public string DeleteSection(long TemplateSectionID)
        {
            var Tsection = entityDB.TemplateSections.Find(TemplateSectionID);


            foreach (var HandF in Tsection.TemplateSectionsHeaderFooter.ToList())
            {
                entityDB.Entry(HandF).State = EntityState.Deleted;
            }
            if(Tsection.TemplateSectionType==4 || Tsection.TemplateSectionType==5 || Tsection.TemplateSectionType==6)//Doc,Proficiancey,Bidding
            {
                foreach (var cTemplates in Tsection.Templates.ClientTemplates.ToList())
                {
                    var reportingType = 2;
                    if (Tsection.TemplateSectionType == 5)
                        reportingType = 1;
                    else if (Tsection.TemplateSectionType == 6)
                        reportingType = 0;
                    foreach (var CTRData in cTemplates.ClientTemplateReportingData.Where(rec => rec.ReportingType == reportingType).ToList())
                    {
                        entityDB.ClientTemplateReportingData.Remove(CTRData);
                    }

                }
            }
            foreach (var Ssection in Tsection.TemplateSubSections.ToList())//Delete SubSections
            {

                foreach (var question in Ssection.Questions.ToList())//Delete Questions
                {
                    if (question.QuestionColumnDetails != null)
                        foreach (var Qcolumn in question.QuestionColumnDetails.ToList())
                            entityDB.Entry(Qcolumn).State = EntityState.Deleted;
                    if (question.QuestionsDependants != null)
                    {
                        foreach (var Qdependent in question.QuestionsDependants.ToList())
                            entityDB.Entry(Qdependent).State = EntityState.Deleted;
                    }
                    entityDB.Entry(question).State = EntityState.Deleted;
                }
                entityDB.Entry(Ssection).State = EntityState.Deleted;
            }
            entityDB.Entry(Tsection).State = EntityState.Deleted;
            entityDB.SaveChanges();
            return "OK";
        }
        //<<<Ends
        
        [SessionExpire]
        [HeaderAndSidebar(headerName = "QuestionnaireTool", sideMenuName = "TemplateTool", ViewName = "SubSectionView")]
        public ActionResult SubSectionView(long SectionId)
        {

            
            ViewBag.Clients = entityDB.Organizations.Where(rec => rec.OrganizationType == "Client").ToList();
           


            var Section = entityDB.TemplateSections.Find(SectionId);
            if (Section.Templates.DefaultTemplate == false)
            {
                var clientId = Section.Templates.ClientTemplates.FirstOrDefault().ClientID;
                ViewBag.Clients = entityDB.Organizations.Where(rec => rec.OrganizationType == "Client" && rec.OrganizationID == clientId).ToList();
            }

            //To Change Side Menu Selection>>>
            if (Section.Templates.DefaultTemplate == null || Section.Templates.DefaultTemplate == false)
            {
                List<LocalHeaderMenuModel> headerMenu = (List<LocalHeaderMenuModel>)ViewBag.headerMenus;
                headerMenu.FirstOrDefault(record => record.menuName == "QuestionnaireTool").sideMenus.ForEach(record => record.isChecked = false);
                headerMenu.FirstOrDefault(record => record.menuName == "QuestionnaireTool").sideMenus.FirstOrDefault(record => record.menuName == "CustomTemplateTool").isChecked = true;
                ViewBag.headerMenus = headerMenu;
            }

            //Ends<<<



            //To Handle Dropdown Change in subsection view
            //If user Select the section of bidding(6) or proficiency(5)
            if(Section.TemplateSectionType==5 ||Section.TemplateSectionType==6)
                return Redirect("AddOrEditPreSubSections?SectionId=" + Section.TemplateSectionID);
            if (Section.TemplateSectionType == 0)//Is Profile
            {
                return Redirect("TemplatesAndSections");
            }

            ViewBag.Section = Section;
            ViewBag.SectionId = SectionId;
            ViewBag.TemplateId=Section.Templates.TemplateID;//Rajesh on 19/10/2013
            ViewBag.SubSections = entityDB.TemplateSubSections.Where(rec => rec.TemplateSectionID == SectionId).ToList();

            ViewBag.Header = Section.TemplateSectionsHeaderFooter.FirstOrDefault(rec=>rec.PositionType==0);
            ViewBag.Footer = Section.TemplateSectionsHeaderFooter.FirstOrDefault(rec => rec.PositionType == 1);

            ViewBag.SectionName = Section.SectionName;
            ViewBag.TemplateName = Section.Templates.TemplateName;
            ViewBag.Templates = (from templates in entityDB.Templates.Where(rec => rec.TemplateType == 0).ToList()
                                select new SelectListItem()
                                {
                                    Text=templates.TemplateName,
                                    Value=templates.TemplateID+"",
                                    Selected=templates.TemplateID==Section.Templates.TemplateID
                                }).ToList();//Rajesh on 10/19/2013

            //Rajesh on 1/4/2014
            if (Section.TemplateSectionType == 33)
            {
                ViewBag.AllHeaders = Section.TemplateSectionsHeaderFooter.Where(rec => rec.PositionType == 0).ToList();
                ViewBag.AllFooters = Section.TemplateSectionsHeaderFooter.Where(rec => rec.PositionType == 1).ToList();
                return View("AgreementSubSectionView");
            }
            else
                return View();
            //Ends<<<
        }
        [HttpPost]
        public ActionResult GetSections(Guid TemplateId,long SectionId)
        {
            
            var curretUserOrgBusUnits = (from sec in entityDB.TemplateSections.Where(rec => rec.TemplateID == TemplateId && rec.TemplateSectionType!=0).ToList()
                                        select new SelectListItem
                                        {
                                            Text = sec.SectionName,
                                            Value = sec.TemplateSectionID+"",
                                            Selected=SectionId==sec.TemplateSectionID
                                        }).ToList();



            return Json(curretUserOrgBusUnits);
        }
        [SessionExpireForView]
        [HeaderAndSidebar(headerName = "QuestionnaireTool", sideMenuName = "TemplateTool", ViewName = "AddOrEditSubSections")]
        public ActionResult AddOrEditSubSections(long SectionId,long subSecId)
        {
            var section = entityDB.TemplateSections.Find(SectionId);
            ViewBag.sectionType = section.TemplateSectionType;

            LocalSubSection subSection = new LocalSubSection();
            subSection.SectionId = SectionId;
            ViewBag.Section = section;//Rajesh on 1/7/2014
            subSection.SubSectionType = "0";
            subSection.SubSecId = subSecId;
            if (subSecId != -1)
            {
                var subSec = entityDB.TemplateSubSections.Find(subSecId);
                if (subSec.SubSectionType != null)
                    subSection.SubSectionType = subSec.SubSectionType + "";
                subSection.SubSectionTypeCondition = subSec.SubSectionTypeCondition;//Rajesh on 1/7/2014
                switch((int)subSec.SubSectionType)
                {
                    case 1: subSection.Dynamic_Condition = subSec.SubSectionTypeCondition; break;
                    case 2: subSection.Predefined_Condition = subSec.SubSectionTypeCondition; break;
                    case 3: subSection.Reporting_Condition = subSec.SubSectionTypeCondition; break;
                }
                subSection.SubSectionName = subSec.SubSectionName;
                subSection.SubSectionNote = subSec.SectionNotes;
                subSection.NoOfColumns = (int)subSec.NoOfColumns;
                subSection.Visible = !(bool)subSec.Visible;
                subSection.isSubSubSection= (bool)subSec.SubHeader;
            }
            return View(subSection);
        }
        [HttpPost]
        [SessionExpireForView]
        [HeaderAndSidebar(headerName = "QuestionnaireTool", sideMenuName = "TemplateTool", ViewName = "AddOrEditSubSections")]
        public ActionResult AddOrEditSubSections(LocalSubSection Form)
        {
            TemplateSubSections subSection = new TemplateSubSections();
            int displayOrder = 1;
            var TSection = entityDB.TemplateSections.Find(Form.SectionId);

            try { displayOrder = TSection.TemplateSubSections.Count() + 1; }
            catch { }
            
            if (Form.SubSecId != -1)
            {
                subSection = entityDB.TemplateSubSections.Find(Form.SubSecId);
                subSection.Visible = !Form.Visible;
            }
            else//Insertion
            {
                subSection.DisplayOrder = displayOrder;
                subSection.Visible = true;

                subSection.SubSectionType = Convert.ToInt32(Form.SubSectionType);
                subSection.TemplateSectionID = Form.SectionId;

                string condition = "";
                if (Form.SubSectionType.Equals("1"))//Dynamic
                {
                    condition = Form.Dynamic_Condition;
                }
                else if (Form.SubSectionType.Equals("2"))//Predefinied
                {
                    //condition = Form.Predefined_Condition;
                    
                    TSection.TemplateSectionType = 32;//IsVendorContactInfo
                    entityDB.Entry(TSection).State = EntityState.Modified;
                    condition = "UserOrganization";
                }
                else if (Form.SubSectionType.Equals("3"))//Reporting Data
                {
                    condition = Form.Reporting_Condition;
                }
                else if (Form.SubSectionType.Equals("4"))//Reporting Data
                {
                    condition = Form.Reporting_Condition;
                }
                else if (Form.SubSectionType.Equals("102"))//Agreement between parties
                {
                    subSection.SubSectionType = 2;
                    condition = "MPATitleHeader";
                }
                else if (Form.SubSectionType.Equals("103"))//PrequalificationComments
                {
                    subSection.SubSectionType = 1;
                    condition = "PrequalComments";
                }
                subSection.SubSectionTypeCondition = condition;
            }
            
            subSection.SubSectionName = Form.SubSectionName;
            //subSection.SectionNotes = Form.SubSectionType;
            subSection.NoOfColumns = Form.NoOfColumns;
            subSection.SectionNotes = Form.SubSectionNote;
            subSection.SubHeader = Form.isSubSubSection;
            
            if (Form.SubSecId == -1)
                entityDB.TemplateSubSections.Add(subSection);
            else
                entityDB.Entry(subSection).State = EntityState.Modified;
            entityDB.SaveChanges();
            return Redirect("PopUpCloseView");
        }
        //Rajesh on 10/21/2013

        public string DeleteSubSection(long SubSectionID)
        {
            var Ssection = entityDB.TemplateSubSections.Find(SubSectionID);


            foreach (var question in Ssection.Questions.ToList())//Delete Questions
            {
                if (question.QuestionColumnDetails != null)
                    foreach (var Qcolumn in question.QuestionColumnDetails.ToList())
                        entityDB.Entry(Qcolumn).State = EntityState.Deleted;
                if (question.QuestionsDependants != null)
                {
                    foreach (var Qdependent in question.QuestionsDependants.ToList())
                        entityDB.Entry(Qdependent).State = EntityState.Deleted;
                }
                entityDB.Entry(question).State = EntityState.Deleted;
            }


            entityDB.Entry(Ssection).State = EntityState.Deleted;
            entityDB.SaveChanges();
            return "OK";
        }
        //<<<Ends

        //Rajesh on 10/21/2013

        public string DeleteQuestion(long Questionid)
        {
            var question = entityDB.Questions.Find(Questionid);
            if (question.QuestionColumnDetails != null)
            {
                foreach (var Qcolumn in question.QuestionColumnDetails.ToList())
                    entityDB.Entry(Qcolumn).State = EntityState.Deleted;
            }
            if (question.QuestionsDependants != null)
            {
                foreach (var Qdependent in question.QuestionsDependants.ToList())
                    entityDB.Entry(Qdependent).State = EntityState.Deleted;
            }
            entityDB.Entry(question).State = EntityState.Deleted;
            entityDB.SaveChanges();
            return "OK";
        }
        //<<<Ends


        [HttpPost]

        public ActionResult AddHeaderOrFooter(long headerFooterId, long SectionId, int type, string title, string content)
        {
            TemplateSectionsHeaderFooter headerFooter = new TemplateSectionsHeaderFooter();
            try
            {
                content = HttpUtility.HtmlDecode(content);//Rajesh On 10/18/2013
                title = HttpUtility.HtmlDecode(title);//Rajesh on 10/18/2013
                if (headerFooterId != -1)
                {
                    headerFooter = entityDB.TemplateSectionsHeaderFooter.Find(headerFooterId);
                }
                headerFooter.TemplateSectionId = SectionId;
                headerFooter.PositionType = type;
                headerFooter.PositionTitle = title.Trim();
                headerFooter.PostionContent = content.Trim();
                if (headerFooterId == -1)
                    entityDB.TemplateSectionsHeaderFooter.Add(headerFooter);
                else
                    entityDB.Entry(headerFooter).State = EntityState.Modified;
                entityDB.SaveChanges();
            }
            catch (Exception ee) { return Json(ee.Message); }
            var Section = entityDB.TemplateSections.Find(SectionId);
            var data = from HandF in entityDB.TemplateSectionsHeaderFooter.ToList() select HandF;
            if (Section.TemplateSectionType == 33)
            {
                ViewBag.HorF = entityDB.TemplateSectionsHeaderFooter.Where(rec => rec.PositionType == type && rec.TemplateSectionId == SectionId).ToList();
                ViewBag.type = type;
                return PartialView("HeaderOrFooterData");
            }


            return Json(headerFooter.HeaderFooterId + "");
        }


        public string DeleteHeaderOrFooter(long headerFooterId)
        {
            entityDB.TemplateSectionsHeaderFooter.Remove(entityDB.TemplateSectionsHeaderFooter.Find(headerFooterId));
            entityDB.SaveChanges();
            return "Ok";
        }
        [SessionExpire]
        [HeaderAndSidebar(headerName = "QuestionnaireTool", sideMenuName = "TemplateTool", ViewName = "Questionslist")]
        public ActionResult Questionslist(long SubSectionId)
        {
            var SubSection = entityDB.TemplateSubSections.Find(SubSectionId);


            //To Change Side Menu Selection>>>
            if (SubSection.TemplateSections.Templates.DefaultTemplate == null || SubSection.TemplateSections.Templates.DefaultTemplate == false)
            {
                List<LocalHeaderMenuModel> headerMenu = (List<LocalHeaderMenuModel>)ViewBag.headerMenus;
                headerMenu.FirstOrDefault(record => record.menuName == "QuestionnaireTool").sideMenus.ForEach(record => record.isChecked = false);
                headerMenu.FirstOrDefault(record => record.menuName == "QuestionnaireTool").sideMenus.FirstOrDefault(record => record.menuName == "CustomTemplateTool").isChecked = true;
                ViewBag.headerMenus = headerMenu;
            }

            //Ends<<<
            ViewBag.TemplateStatus = SubSection.TemplateSections.Templates.TemplateStatus;


            ViewBag.TemplateType = SubSection.TemplateSections.Templates.DefaultTemplate == true ? 1 : 0;//To Know Template Type is standard or Custom

            ViewBag.SectionId = SubSection.TemplateSectionID;
            ViewBag.TemplateName = SubSection.TemplateSections.Templates.TemplateName;
            ViewBag.SectionName = SubSection.TemplateSections.SectionName;
            ViewBag.SubSectionName = SubSection.SubSectionName;

            ViewBag.SubSectionType = SubSection.SubSectionType;
            ViewBag.Condition = SubSection.SubSectionTypeCondition;

            ViewBag.SubSections = (from subsections in SubSection.TemplateSections.TemplateSubSections.ToList()
                                  select new SelectListItem
                                  {
                                      Text = subsections.SubSectionName,
                                      Value = subsections.SubSectionID + "",
                                      Selected = SubSectionId == subsections.SubSectionID
                                  }).ToList();

            ViewBag.SubSectionId = SubSectionId;
            ViewBag.QuestionsBankList = entityDB.QuestionsBank.Where(rec => rec.QuestionReference == 2 && rec.QuestionStatus == 1 && rec.UsedFor == 0).ToList();

            ViewBag.QuestionsList = entityDB.Questions.Where(rec => rec.SubSectionId == SubSectionId).OrderBy(rec => rec.DisplayOrder).ToList();//Rajesh on 11/25/2013
            return View();
        }

        ////Rajesh on 10/25/2013
        //[SessionExpireForView]
        //public ActionResult AddOrEditQuestion(long QuestionId, long SubSectionId)
        //{
        //    LocalQuestionsModel question = new LocalQuestionsModel();
        //    question.QuestionID = QuestionId;
        //    question.SubSectionId = SubSectionId;
        //    ViewBag.Controls = (from controls in entityDB.QuestionControlType.Where(rec=>rec.Visible==true).ToList()
        //                        select new SelectListItem()
        //                        {
        //                            Text = controls.QuestionTypeDisplayName,
        //                            Value = controls.QuestionTypeID + ""
        //                        }).ToList();
        //    return View(question);
        //}
        [HttpPost]
        public string AddQuestionBank(string QuestionString)
        {
            var currentQuestionRecord = entityDB.QuestionsBank.FirstOrDefault(record => record.QuestionString.ToUpper() == QuestionString.ToUpper());
            if (currentQuestionRecord != null)
            {
                ViewBag.isQuestionExits = true;
                return "Question being added, exist in question bank";
            }
            QuestionsBank Question = new QuestionsBank();
            Question.QuestionString = QuestionString;
            Question.QuestionReference = 2;
            entityDB.QuestionsBank.Add(Question);
            Question.UsedFor = 0;
            Question.QuestionStatus = 1;
            
            entityDB.SaveChanges();

            return "Ok";
        }
        //Rajesh on 10/25/2013
        [HttpPost]
        public string AddOrEditQuestion(string QuestionText, long SubSectionId, int NumberOfColumns, long QuestionBankId,long QuestionId)
        {
            var subSection = entityDB.TemplateSubSections.Find(SubSectionId);
            int questionOrderNumber=1;
            try{questionOrderNumber=subSection.Questions.Count()+1;}catch{}
            Questions question;
            long questionId=-1;
            if (QuestionId == -1)
            {
                var SubSection = entityDB.TemplateSubSections.Find(SubSectionId);
                question = new Questions();
                question.QuestionText = QuestionText;
                question.SubSectionId = SubSectionId;
                question.DisplayOrder = questionOrderNumber;
                question.Visible = true;
                question.NumberOfColumns = NumberOfColumns;
                if (SubSection.SubSectionTypeCondition == "BiddingReporting" || SubSection.SubSectionTypeCondition == "ProficiencyReporting")
                {
                    question.IsMandatory = SubSection.Questions == null || SubSection.Questions.Count() == 0;
                }
                else
                    question.IsMandatory = true;
                question.IsBold = true;
                question.ResponseRequired = true;
                question.DisplayResponse = true;
                question.HasDependantQuestions = false;
                question.QuestionBankId = QuestionBankId;
                entityDB.Questions.Add(question);
                questionId=question.QuestionID;

                //Rajesh on 11/23/2013
                

                if (SubSection.SubSectionType == 4 && SubSection.SubSectionTypeCondition == "BiddingReporting")
                {
                    question.QuestionColumnDetails = new List<QuestionColumnDetails>();
                    QuestionColumnDetails details = new QuestionColumnDetails();
                    details.ColumnNo = 1;

                    details.DisplayType = 0;
                    details.QuestionControlTypeId = 9;
                    details.QuestionId = question.QuestionID;

                    details.ColumnValues = "";
                    details.TableReference = "BiddingInterests";

                    details.TableReferenceField = "BiddingInterestId";
                    details.TableRefFieldValueDisplay = "BiddingInterestName";



                    question.QuestionColumnDetails.Add(details);
                }

            }
            else
            {
                question = entityDB.Questions.Find(QuestionId);
                
                if (NumberOfColumns != question.NumberOfColumns && question.QuestionColumnDetails!=null)
                {
                    foreach (var QColumn in question.QuestionColumnDetails.ToList())
                        entityDB.Entry(QColumn).State = EntityState.Deleted;
                }
                question.NumberOfColumns = NumberOfColumns;
                questionId = question.QuestionID;

                entityDB.Entry(question).State = EntityState.Modified;
            }

            entityDB.SaveChanges();
            return ""+question.QuestionID;
        }
        //<<Ends 
        [SessionExpire]
        [HeaderAndSidebar(headerName = "QuestionnaireTool", sideMenuName = "TemplateTool", ViewName = "AddOrEditPreSubSections")]
        public ActionResult AddOrEditPreSubSections(long SectionId)
        {

            var section = entityDB.TemplateSections.Find(SectionId);

            //To Change Side Menu Selection>>>
            if (section.Templates.DefaultTemplate == null || section.Templates.DefaultTemplate == false)
            {
                List<LocalHeaderMenuModel> headerMenu = (List<LocalHeaderMenuModel>)ViewBag.headerMenus;
                headerMenu.FirstOrDefault(record => record.menuName == "QuestionnaireTool").sideMenus.ForEach(record => record.isChecked = false);
                headerMenu.FirstOrDefault(record => record.menuName == "QuestionnaireTool").sideMenus.FirstOrDefault(record => record.menuName == "CustomTemplateTool").isChecked = true;
                ViewBag.headerMenus = headerMenu;
            }

            //Ends<<<

            ViewBag.Section = section;
            ViewBag.SectionType = section.TemplateSectionType;
            ViewBag.templateId = section.Templates.TemplateID;
            ViewBag.Clients = entityDB.Organizations.Where(rec => rec.OrganizationType == "Client").ToList();
            // Kiran on 12/21/2013

            if (section.Templates.DefaultTemplate == false)
            {
                var clientId = section.Templates.ClientTemplates.FirstOrDefault().ClientID;
                ViewBag.Clients = entityDB.Organizations.Where(rec => rec.OrganizationType == "Client" && rec.OrganizationID == clientId).ToList();
            }
            // Ends<<<

            ViewBag.TemplateName = section.Templates.TemplateName;
            ViewBag.SectionName = section.SectionName;//Rajesh on 10/23/2013

            ViewBag.SectionId = SectionId;
            return View();
        }
        [HttpPost]
        public string SaveBiddings(long clientId, Guid TemplateId, string clientTemplateData, long SectionId)
        {
            var clientTemplate = entityDB.ClientTemplates.FirstOrDefault(rec => rec.ClientID == clientId && TemplateId == rec.TemplateID);

            var SectionType = entityDB.TemplateSections.Find(SectionId).TemplateSectionType;
            //If user selects none of the clients(-1)...
            if (clientId == -1)
            {
                return "Ok";
            }

            if (clientTemplate == null)
            {
                clientTemplate = new ClientTemplates();
                clientTemplate.ClientID = clientId;
                clientTemplate.TemplateID = TemplateId;
                entityDB.ClientTemplates.Add(clientTemplate);
            }
            else
            {
                if (SectionType == 6)//Bidding
                {
                    foreach (var CLRData in entityDB.ClientTemplateReportingData.Where(rec => rec.ReportingType == 0 && rec.ClientTemplateId == clientTemplate.ClientTemplateID))
                    {
                        entityDB.Entry(CLRData).State = EntityState.Deleted;
                    }


                }
                else if (SectionType == 4)//Supporting Documents
                {
                    foreach (var CLRData in entityDB.ClientTemplateReportingData.Where(rec => rec.ReportingType == 2 && rec.ClientTemplateId == clientTemplate.ClientTemplateID))
                    {
                        entityDB.Entry(CLRData).State = EntityState.Deleted;
                    }
                }
                else//5-Proficiency
                {
                    foreach (var CLRData in entityDB.ClientTemplateReportingData.Where(rec => rec.ReportingType == 1 && rec.ClientTemplateId == clientTemplate.ClientTemplateID))
                    {
                        entityDB.Entry(CLRData).State = EntityState.Deleted;
                    }
                }
            }
            foreach (var clientBiddings in clientTemplateData.Split(','))
            {
                try
                {
                    ClientTemplateReportingData report = new ClientTemplateReportingData();
                    report.ClientTemplateId = clientTemplate.ClientTemplateID;
                    if (SectionType == 6)//Bidding
                        report.ReportingType = 0;//Bidding
                    else if (SectionType == 4)//Supporting Doc
                    {
                        report.ReportingType = 2;
                    }
                    else
                        report.ReportingType = 1;//Proficiency & Capabilities

                    if (SectionType == 4)//It gets id_DocId|DocExpires
                    {
                        report.ReportingTypeId = Convert.ToInt64(clientBiddings.Split('|')[0].Substring(3));
                        report.DocumentExpires = Convert.ToBoolean(clientBiddings.Split('|')[1]);
                    }
                    else
                        report.ReportingTypeId = Convert.ToInt64(clientBiddings.Substring(3));
                    entityDB.ClientTemplateReportingData.Add(report);
                }
                catch { }
            }
            entityDB.SaveChanges();
            if (SectionType == 6)//Biddings has one default subsec..
            {

                TemplateSubSections SubSec = entityDB.TemplateSubSections.FirstOrDefault(rec => rec.TemplateSectionID == SectionId && rec.ClientId == clientId);
                if (SubSec == null)
                {
                    SubSec = new TemplateSubSections();
                    SubSec.ClientId = clientTemplate.ClientID;
                    SubSec.DisplayOrder = 1;
                    SubSec.NoOfColumns = 1;
                    SubSec.SubSectionName = "Add Bidding Interests";
                    SubSec.SubSectionType = 0;
                    SubSec.TemplateSectionID = SectionId;
                    SubSec.Visible = true;
                    SubSec.SubSectionType = 4;
                    SubSec.SubSectionTypeCondition = "BiddingReporting";
                    SubSec.Questions = new List<Questions>();
                    entityDB.TemplateSubSections.Add(SubSec);
                }
                else
                {
                    foreach(var question in SubSec.Questions.ToList())//Delete all questions related to section--------- and insert again
                    {
                        foreach (var QcolumnDetails in question.QuestionColumnDetails.ToList())
                        {
                            entityDB.Entry(QcolumnDetails).State = EntityState.Deleted;
                        }
                        entityDB.Entry(question).State = EntityState.Deleted;
                    }
                }
                foreach (var clientBiddings in clientTemplateData.Split(','))
                {
                    try
                    {
                        long BiddingId = Convert.ToInt64(clientBiddings.Substring(3));
                        var BiddingName = entityDB.BiddingInterests.Find(BiddingId).BiddingInterestName;

                        QuestionsBank QBank = entityDB.QuestionsBank.FirstOrDefault(rec => rec.QuestionReference == 0 && rec.QuestionReferenceId == BiddingId);
                        if (QBank == null)
                        {

                            QBank = new QuestionsBank();
                            QBank.QuestionReference = 0;
                            QBank.QuestionReferenceId = BiddingId;
                            QBank.QuestionString = BiddingName;

                            QBank.UsedFor = 0;
                            QBank.QuestionStatus = 1;

                            Questions question = new Questions();
                            question.QuestionText = BiddingName;
                            question.DisplayOrder = 1;
                            question.DisplayResponse = true;
                            question.NumberOfColumns = 1;
                            question.QuestionBankId = QBank.QuestionBankId;
                            question.ResponseRequired = true;
                            question.SubSectionId = SubSec.SubSectionID;
                            question.Visible = true;


                            question.QuestionColumnDetails = new List<QuestionColumnDetails>();

                            QuestionColumnDetails QColumnDetails = new QuestionColumnDetails();
                            QColumnDetails.ColumnNo = 1;
                            QColumnDetails.ColumnValues = "Yes,No,Not Applicable";
                            QColumnDetails.DisplayType = 0;
                            QColumnDetails.QuestionControlTypeId = 7;

                            question.QuestionColumnDetails.Add(QColumnDetails);

                            QBank.Questions = new List<Questions>();
                            QBank.Questions.Add(question);


                            entityDB.QuestionsBank.Add(QBank);
                        }
                        else
                        {
                            Questions question = new Questions();
                            question.QuestionText = BiddingName;
                            question.DisplayOrder = 1;
                            question.DisplayResponse = true;
                            question.NumberOfColumns = 1;
                            question.QuestionBankId = QBank.QuestionBankId;
                            question.ResponseRequired = true;
                            question.SubSectionId = SubSec.SubSectionID;
                            question.Visible = true;


                            question.QuestionColumnDetails = new List<QuestionColumnDetails>();

                            QuestionColumnDetails QColumnDetails = new QuestionColumnDetails();
                            QColumnDetails.ColumnNo = 1;
                            QColumnDetails.ColumnValues = "Yes,No,Not Applicable";
                            QColumnDetails.DisplayType = 0;
                            QColumnDetails.QuestionControlTypeId = 7;

                            question.QuestionColumnDetails.Add(QColumnDetails);
                            entityDB.Questions.Add(question);
                        }



                    }
                    catch (Exception ee)
                    {
                    }
                }
            }
            else if (SectionType == 5)//Add Subsecs for proficiency(For Distinct Category)
            {
                
                var CategoryName = "";
                var SubCategryName = "";
                foreach (var subSec in entityDB.TemplateSections.Find(SectionId).TemplateSubSections.Where(rec => rec.TemplateSectionID == SectionId && rec.ClientId == clientId).ToList())
                {
                    foreach (var questions in subSec.Questions.ToList())
                    {
                        foreach (var QCol in questions.QuestionColumnDetails.ToList())
                        {
                            entityDB.Entry(QCol).State = EntityState.Deleted;
                        }
                        entityDB.Entry(questions).State = EntityState.Deleted;
                    }
                    entityDB.Entry(subSec).State = EntityState.Deleted;
                }

                entityDB.SaveChanges();
                //List<long> selectedProficiency = new List<long>();
                //foreach (var clientProficiency in clientTemplateData.Split(','))
                //{
                //    long proficiencyId = Convert.ToInt64(clientProficiency.Substring(3));
                //    selectedProficiency.Add(proficiencyId);
                //}
                //entityDB.ProficiencyCapabilities.Where(rec => selectedProficiency.Contains((long)rec.ProficiencyId)).ToList().GroupBy(rec=>rec.ProficiencyCategory).th;
                
                TemplateSubSections SubSubSec = new TemplateSubSections();
                TemplateSubSections SubSec = new TemplateSubSections();
                foreach (var clientBiddings in clientTemplateData.Split(','))
                {
                    try
                    {

                        //clientBiddings.Substring(3)--Proficiency Id
                        long proficiencyId = Convert.ToInt64(clientBiddings.Substring(3));
                        var Pcategory = entityDB.ProficiencyCapabilities.Find(proficiencyId);
                        if (Pcategory.ProficiencyCategory != CategoryName)
                        {

                            //var subSec=entityDB.TemplateSubSections.FirstOrDefault(rec => rec.TemplateSectionID == SectionId && rec.SubSectionName == Pcategory.ProficiencyCategory);
                            //if (subSec == null)//
                            {
                                
                                SubSec = new TemplateSubSections();
                                SubSec.ClientId = clientTemplate.ClientID;
                                SubSec.DisplayOrder = 1;
                                SubSec.NoOfColumns = 1;
                                SubSec.SubSectionName = Pcategory.ProficiencyCategory;
                                SubSec.SubSectionType = 0;
                                SubSec.TemplateSectionID = SectionId;
                                SubSec.Visible = true;
                                SubSec.SubSectionType = 4;
                                SubSec.SubSectionTypeCondition = "ProficiencyReporting";
                                entityDB.TemplateSubSections.Add(SubSec);

                                if (!string.IsNullOrEmpty(Pcategory.ProficiencySubCategroy))
                                {
                                    SubSubSec = new TemplateSubSections();
                                    SubSubSec.ClientId = clientTemplate.ClientID;
                                    SubSubSec.DisplayOrder = 1;
                                    SubSubSec.NoOfColumns = 1;
                                    SubSubSec.SubSectionName = Pcategory.ProficiencySubCategroy;
                                    SubSubSec.SubSectionType = 1;
                                    SubSubSec.TemplateSectionID = SectionId;
                                    SubSubSec.Visible = true;
                                    SubSubSec.SubHeader = true;
                                    SubSubSec.SubSectionType = 4;
                                    SubSubSec.SubSectionTypeCondition = "ProficiencyReporting";
                                    SubSubSec.Questions = new List<Questions>();
                                    entityDB.TemplateSubSections.Add(SubSubSec);
                                }
                                else
                                {
                                    SubSubSec = SubSec;
                                }
                                entityDB.SaveChanges();

                                AddQuestionForProficiencyCapability(SubSubSec, proficiencyId, Pcategory);

                            }
                            //else
                            //{
                            //    subSec.Visible = true;
                            //    entityDB.Entry(subSec).State=EntityState.Modified;

                            //    TemplateSubSections SubSubSec = new TemplateSubSections();
                            //    SubSubSec.ClientId = clientTemplate.ClientID;
                            //    SubSubSec.DisplayOrder = 0;
                            //    SubSubSec.NoOfColumns = 1;
                            //    SubSubSec.SubSectionName = Pcategory.ProficiencySubCategroy;
                            //    SubSubSec.SubSectionType = 0;
                            //    SubSubSec.TemplateSectionID = SectionId;
                            //    SubSubSec.Visible = true;
                            //    SubSubSec.SubSectionType = 4;
                            //    SubSubSec.SubSectionTypeCondition = "ProficiencyReporting";

                            //    entityDB.TemplateSubSections.Add(SubSubSec);
                            //}
                            CategoryName = Pcategory.ProficiencyCategory;
                            SubCategryName = Pcategory.ProficiencySubCategroy;
                        }
                        else
                        {
                            if (SubCategryName != Pcategory.ProficiencySubCategroy)//If Section is empty we need to insert question in category (so subsubsec=subsec)
                            {
                                if (!string.IsNullOrEmpty(Pcategory.ProficiencySubCategroy))
                                {
                                    SubSubSec = new TemplateSubSections();
                                    SubSubSec.ClientId = clientTemplate.ClientID;
                                    SubSubSec.DisplayOrder = 1;
                                    SubSubSec.NoOfColumns = 1;
                                    SubSubSec.SubSectionName = Pcategory.ProficiencySubCategroy;
                                    SubSubSec.SubSectionType = 1;
                                    SubSubSec.TemplateSectionID = SectionId;
                                    SubSubSec.SubHeader = true;
                                    SubSubSec.Visible = true;
                                    SubSubSec.SubSectionType = 4;
                                    SubSubSec.SubSectionTypeCondition = "ProficiencyReporting";
                                    SubSubSec.Questions = new List<Questions>();
                                    entityDB.TemplateSubSections.Add(SubSubSec);
                                    SubCategryName = Pcategory.ProficiencySubCategroy;
                                }
                                else
                                {
                                    SubSubSec = SubSec;
                                }

                                AddQuestionForProficiencyCapability(SubSubSec, proficiencyId, Pcategory);




                            }
                            else
                            {
                                AddQuestionForProficiencyCapability(SubSubSec, proficiencyId, Pcategory);
                            }
                        }

                    }
                    catch { }
                    entityDB.SaveChanges();
                }
            }

            entityDB.SaveChanges();
            return "Ok";
        }

        private void AddQuestionForProficiencyCapability(TemplateSubSections SubSubSec, long proficiencyId, ProficiencyCapabilities Pcategory)
        {
            QuestionsBank QBank = entityDB.QuestionsBank.FirstOrDefault(rec => rec.QuestionReference == 1 && rec.QuestionReferenceId == proficiencyId);
            if (QBank == null)
            {

                QBank = new QuestionsBank();
                QBank.QuestionReference = 1;
                QBank.QuestionReferenceId = proficiencyId;
                QBank.QuestionString = Pcategory.ProficiencyName;
                QBank.UsedFor = 0;
                QBank.QuestionStatus = 1;

                entityDB.QuestionsBank.Add(QBank);

                Questions question = new Questions();
                question.QuestionText = Pcategory.ProficiencyName;
                question.DisplayOrder = 1;
                question.DisplayResponse = true;
                question.NumberOfColumns = 1;
                question.QuestionBankId = QBank.QuestionBankId;
                question.ResponseRequired = true;
                question.SubSectionId = SubSubSec.SubSectionID;
                question.Visible = true;

                question.QuestionBankId = QBank.QuestionBankId;

                question.QuestionColumnDetails = new List<QuestionColumnDetails>();

                QuestionColumnDetails QColumnDetails = new QuestionColumnDetails();
                QColumnDetails.ColumnNo = 1;
                QColumnDetails.ColumnValues = "Yes,No,Not Applicable";
                QColumnDetails.DisplayType = 0;
                QColumnDetails.QuestionControlTypeId = 7;

                question.QuestionColumnDetails.Add(QColumnDetails);

                QBank.Questions = new List<Questions>();
                QBank.Questions.Add(question);

                entityDB.Questions.Add(question);
                //SubSubSec.Questions.Add(question);
                //entityDB.QuestionsBank.Add(QBank);
            }
            else
            {
                Questions question = new Questions();
                question.QuestionText = Pcategory.ProficiencyName;
                question.DisplayOrder = 1;
                question.DisplayResponse = true;
                question.NumberOfColumns = 1;
                question.QuestionBankId = QBank.QuestionBankId;
                question.ResponseRequired = true;
                question.SubSectionId = SubSubSec.SubSectionID;
                question.Visible = true;


                question.QuestionColumnDetails = new List<QuestionColumnDetails>();

                QuestionColumnDetails QColumnDetails = new QuestionColumnDetails();
                QColumnDetails.ColumnNo = 1;
                QColumnDetails.ColumnValues = "Yes,No,Not Applicable";
                QColumnDetails.DisplayType = 0;
                QColumnDetails.QuestionControlTypeId = 7;

                question.QuestionColumnDetails.Add(QColumnDetails);
                entityDB.Questions.Add(question);
            }
        }
        //Rajesh on 10/23/2013 >>>
        [HttpPost]
        public ActionResult GetBiddings(long clientOrgId, Guid templateId, long SectionId)
        {

            var Section = entityDB.TemplateSections.Find(SectionId);
            if (Section.TemplateSectionType == 6)//Biddings
            {
                var curretBiddings = (from bidding in entityDB.BiddingInterests.ToList()
                                      where bidding.Status == 1
                                      select new BiddingCheckBoxes
                                      {
                                          id = bidding.BiddingInterestId + "",
                                          Text = bidding.BiddingInterestName + "",
                                          Header="",
                                          type = 0
                                      }).ToList();
                try
                {
                    var clientTemplateId = entityDB.ClientTemplates.FirstOrDefault(rec => rec.ClientID == clientOrgId && rec.TemplateID == templateId).ClientTemplateID;
                    var clientTemplateBiddings = entityDB.ClientTemplateReportingData.ToList().Where(rec => rec.ReportingType == 0 && rec.ClientTemplateId == clientTemplateId);
                    foreach (var Cbiddings in clientTemplateBiddings)
                    {
                        curretBiddings.FirstOrDefault(rec => rec.id == Cbiddings.ReportingTypeId + "").status = "true";
                    }
                }
                catch (Exception ee)
                {
                    //Means no client template
                }
                return Json(curretBiddings);
            }
            else if (Section.TemplateSectionType == 4)//For Documents
            {
                var curretBiddings = (from documents in entityDB.DocumentType.Where(rec=>rec.PrequalificationUseOnly==true).ToList()
                                      select new BiddingCheckBoxes
                                      {
                                          id = documents.DocumentTypeId + "",
                                          Text = documents.DocumentTypeName + "",
                                          Header = "",
                                          type = 0,
                                          isExpire=false

                                      }).ToList();
                try
                {
                    var clientTemplateId = entityDB.ClientTemplates.FirstOrDefault(rec => rec.ClientID == clientOrgId && rec.TemplateID == templateId).ClientTemplateID;
                    var clientTemplateBiddings = entityDB.ClientTemplateReportingData.ToList().Where(rec => rec.ReportingType == 2 && rec.ClientTemplateId == clientTemplateId);
                    foreach (var Cbiddings in clientTemplateBiddings)
                    {
                        curretBiddings.FirstOrDefault(rec => rec.id == Cbiddings.ReportingTypeId + "").status = "true";
                        curretBiddings.FirstOrDefault(rec => rec.id == Cbiddings.ReportingTypeId + "").isExpire = Cbiddings.DocumentExpires == true;
                    }
                }
                catch (Exception ee)
                {
                    //Means no client template
                }
                return Json(curretBiddings);
            }
            else//5-Proficiency
            {
                var currentProficiencys = (from proficiency in entityDB.ProficiencyCapabilities.ToList().OrderBy(rec => rec.ProficiencyCategory).ThenBy(rec => rec.ProficiencySubCategroy).ToList()
                                           where proficiency.Status == 1
                                           select new BiddingCheckBoxes
                                           {
                                               id = proficiency.ProficiencyId + "",
                                               Header = proficiency.ProficiencyCategory,
                                               Text = (string.IsNullOrEmpty(proficiency.ProficiencySubCategroy) ? "" : proficiency.ProficiencySubCategroy + "-") + proficiency.ProficiencyName,
                                               type = 1
                                           }).ToList();
                try
                {
                    var clientTemplateId = entityDB.ClientTemplates.FirstOrDefault(rec => rec.ClientID == clientOrgId && rec.TemplateID == templateId).ClientTemplateID;
                    var clientTemplateBiddings = entityDB.ClientTemplateReportingData.ToList().Where(rec => rec.ReportingType == 1 && rec.ClientTemplateId == clientTemplateId);
                    foreach (var Cbiddings in clientTemplateBiddings)
                    {
                        currentProficiencys.FirstOrDefault(rec => rec.id == Cbiddings.ReportingTypeId + "").status = "true";
                    }
                }
                catch (Exception ee)
                {
                    //Means no client template
                }
                return Json(currentProficiencys);

            }
        }


        //Ends <<<


        /*
   Created Date:  10/24/2013   Created By: Kiran Talluri
   Purpose: To display the Questions Bank
*/

        [HttpGet]
        [SessionExpire]
        [HeaderAndSidebar(headerName = "QuestionnaireTool", sideMenuName = "QuestionsBank", ViewName = "QuestionsBank")]
        public ActionResult QuestionsBank()
        {
            var QuestionsBankList = entityDB.QuestionsBank.GroupBy(record => record.UsedFor).ToList();
            ViewBag.VBQuestionsBankList = QuestionsBankList;
            return View();
        }

        /*
           Created Date:  10/24/2013   Created By: Kiran Talluri
           Purpose: To add a question in Questions Bank
       */

        [HttpGet]
        [SessionExpireForView]
        [HeaderAndSidebar(headerName = "QuestionnaireTool", sideMenuName = "QuestionsBank", ViewName = "AddQuestionInQuestionBank")]
        public ActionResult AddQuestionInQuestionBank(long QuestionBankId)
        {
            LocalAddQuestionInQuestionBank addQuestion = new LocalAddQuestionInQuestionBank();
            ViewBag.isQuestionExits = false;
            if (QuestionBankId != -1)
            {
                var currentQuestionBankrecord = entityDB.QuestionsBank.FirstOrDefault(record => record.QuestionBankId == QuestionBankId);
                addQuestion.isEdit = true;
                addQuestion.QuestionString = currentQuestionBankrecord.QuestionString;

                addQuestion.QuestionFor = (int)currentQuestionBankrecord.UsedFor; // Kiran 11/29/2013

                //Kiran on 11/23/2013
                if (currentQuestionBankrecord.QuestionStatus != null)
                {
                    int currentQuestionStatus = (int)currentQuestionBankrecord.QuestionStatus;
                    if (currentQuestionStatus == 1)
                    {
                        addQuestion.QuestionStatus = true;
                    }
                    else
                    {
                        addQuestion.QuestionStatus = false;
                    }
                }
                // Ends<<<

                return View(addQuestion);
            }
            addQuestion.isEdit = false;
            addQuestion.QuestionStatus = true;
            return View(addQuestion);
        }

        /*
           Created Date:  10/24/2013   Created By: Kiran Talluri
           Purpose: To add a question in Questions Bank
       */
        [HttpPost]
        [SessionExpireForView]
        [HeaderAndSidebar(headerName = "QuestionnaireTool", sideMenuName = "QuestionsBank", ViewName = "AddQuestionInQuestionBank")]
        public ActionResult AddQuestionInQuestionBank(LocalAddQuestionInQuestionBank addQuestion)
        {
            if (ModelState.IsValid)
            {
                var currentQuestionRecord = entityDB.QuestionsBank.FirstOrDefault(record => record.QuestionString.ToUpper() == addQuestion.QuestionString.Trim().ToUpper() && record.QuestionBankId != addQuestion.QuestionBankId);
                if (currentQuestionRecord != null)
                {
                    ViewBag.isQuestionExits = true;
                    return View(addQuestion);
                }

                if (addQuestion.isEdit == false)
                {
                    QuestionsBank Question = new QuestionsBank();
                    Question.QuestionString = addQuestion.QuestionString.Trim();
                    Question.QuestionReference = 2;

                    Question.UsedFor = addQuestion.QuestionFor; // Kiran 11/29/2013

                    // Kiran on 11/23/2013
                    bool QuestionStatus = addQuestion.QuestionStatus;
                    if (QuestionStatus == true)
                    {
                        Question.QuestionStatus = 1;
                    }
                    else
                    {
                        Question.QuestionStatus = 0;
                    }
                    // Ends<<<

                    entityDB.QuestionsBank.Add(Question);
                    entityDB.SaveChanges();
                }
                else
                {
                    var currentQuestionBankrecord = entityDB.QuestionsBank.FirstOrDefault(record => record.QuestionBankId == addQuestion.QuestionBankId);
                    currentQuestionBankrecord.QuestionString = addQuestion.QuestionString.Trim();
                    currentQuestionBankrecord.UsedFor = addQuestion.QuestionFor; // Kiran 11/29/2013

                    // Kiran on 11/23/2013
                    bool QuestionStatus = addQuestion.QuestionStatus;
                    if (QuestionStatus == true)
                    {
                        currentQuestionBankrecord.QuestionStatus = 1;
                    }
                    else
                    {
                        currentQuestionBankrecord.QuestionStatus = 0;
                    }
                    // Ends<<<

                    var questionTextRecordsInQuestions = currentQuestionBankrecord.Questions.ToList();

                    foreach (var question in questionTextRecordsInQuestions)
                    {
                        question.QuestionText = addQuestion.QuestionString;
                        entityDB.Entry(question).State = System.Data.EntityState.Modified;
                        entityDB.SaveChanges();
                    }
                    entityDB.Entry(currentQuestionBankrecord).State = System.Data.EntityState.Modified;
                    entityDB.SaveChanges();
                }
                return RedirectToAction("PopUpCloseView");
            }
            return View(addQuestion);
        }

        public ActionResult CloseQuestionBank()
        {
            return View();
        }

        //Ends<<<





        //Rajesh on 10/25/2013
        [SessionExpireForView]
        [HeaderAndSidebar(headerName = "QuestionnaireTool", sideMenuName = "TemplateTool", ViewName = "AddOrEditQuestionColumnDetails")]
        public ActionResult AddOrEditQuestionColumnDetails(long QuestionId)
        {
            //Rajesh on 12/24/2013
            SelectListItem EmptyItem = new SelectListItem() { Text = "Select Type", Value = "-1" };
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(EmptyItem);
            items.AddRange((from controls in entityDB.QuestionControlType.Where(rec => rec.Visible == true).ToList()
                            select new SelectListItem
                            {
                                Text = controls.QuestionTypeDisplayName,
                                Value = controls.QuestionTypeID + ""
                            }).ToList());

            ViewBag.QuestionControlTypes = items;

            //Ends<<

            var Question = entityDB.Questions.Find(QuestionId);
            ViewBag.Question = Question;
            LocalQuestionColumnDetails details = new LocalQuestionColumnDetails();
            details.QuestionId = QuestionId;
            details.IsBold = (bool)Question.IsBold;
            details.IsMandatory = (bool)Question.IsMandatory;
            details.IsRepeat = Question.QuestionColumnDetails.Count() == 0;
            details.ResponseRequired = Question.ResponseRequired;
            details.Visible = !Question.Visible;
            details.ColumnDetails = new List<ColumnDetails>();
            details.HasDependent = (bool)Question.HasDependantQuestions;
            if (Question.QuestionColumnDetails.Count() == 0)//Add Question Columns
            {
                for (int i = 0; i < Question.NumberOfColumns; i++)
                {
                    ColumnDetails c = new ColumnDetails();
                    c.QuestionValues = new List<string>();
                    foreach (var allQColDetails in entityDB.QuestionColumnDetails.Where(rec => rec.Questions.SubSectionId == Question.SubSectionId && rec.QuestionId != QuestionId).GroupBy(rec => rec.QuestionId).ToList())
                    {

                        string question = "";//allQColDetails.Questions.QuestionText + "| Q" + allQColDetails.QuestionColumnId + " ";
                        bool initialFlag = true;
                        var maxRows = 1;
                        foreach (var QcolumnDetails in allQColDetails.OrderBy(rec => rec.ColumnNo))
                        {
                            int num = allQColDetails.Max(rec => rec.ColumnNo);
                            if (maxRows < num)
                                maxRows = num;

                            if (initialFlag)
                                question = QcolumnDetails.Questions.QuestionText;
                            initialFlag = false;
                            if (QcolumnDetails.QuestionControlTypeId != 3)
                                question += "|-";
                            else
                                question += "| Q" + QcolumnDetails.QuestionColumnId + " ";

                        }
                        c.MaxQuestionColumns = maxRows;
                        c.QuestionValues.Add(question);
                    }
                    c.HasFormula = false;
                    details.ColumnDetails.Add(c);


                }
            }
            else//Edit Question Columns
            {
                foreach (var column in Question.QuestionColumnDetails)
                {
                    ColumnDetails c = new ColumnDetails();



                    c.QuestionValues = new List<string>();
                    var maxRows = 1;
                    foreach (var allQColDetails in entityDB.QuestionColumnDetails.Where(rec => rec.Questions.SubSectionId == column.Questions.SubSectionId && rec.QuestionId != QuestionId).GroupBy(rec => rec.QuestionId).ToList())
                    {
                        int num = allQColDetails.Max(rec => rec.ColumnNo);
                        if (maxRows < num)
                            maxRows = num;
                        //string question = allQColDetails.Questions.QuestionText + "| Q" + allQColDetails.QuestionColumnId + " ";
                        string question = "";//allQColDetails.Questions.QuestionText + "| Q" + allQColDetails.QuestionColumnId + " ";
                        bool initialFlag = true;
                        foreach (var QcolumnDetails in allQColDetails.OrderBy(rec => rec.ColumnNo))
                        {
                            if (initialFlag)
                                question = QcolumnDetails.Questions.QuestionText;

                            if (QcolumnDetails.QuestionControlTypeId != 3)
                                question += "|-";
                            else
                                question += "| Q" + QcolumnDetails.QuestionColumnId + " ";
                            initialFlag = false;
                        }
                        c.QuestionValues.Add(question);
                    }
                    c.MaxQuestionColumns = maxRows;
                    c.ControlType = column.QuestionControlTypeId + "";
                    c.DisplayType = column.DisplayType;
                    c.HasTableReference = !string.IsNullOrWhiteSpace(column.TableReference);
                    c.RefTableName = column.TableReference;
                    c.Value = column.ColumnValues;
                    c.FormulaValue = column.QFormula;
                    if (!string.IsNullOrEmpty(column.QFormula))
                    {
                        c.HasFormula = true;
                    }
                    details.ColumnDetails.Add(c);
                }
            }
            return View(details);
        }

        [HttpPost]
        [SessionExpireForView]
        [HeaderAndSidebar(headerName = "QuestionnaireTool", sideMenuName = "TemplateTool", ViewName = "AddOrEditQuestionColumnDetails")]
        public ActionResult AddOrEditQuestionColumnDetails(LocalQuestionColumnDetails form)
        {
            //Rajesh on 12/24/2013
            SelectListItem EmptyItem = new SelectListItem() { Text = "Select Type", Value = "-1" };
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(EmptyItem);
            items.AddRange((from controls in entityDB.QuestionControlType.Where(rec => rec.Visible == true).ToList()
                            select new SelectListItem
                            {
                                Text = controls.QuestionTypeDisplayName,
                                Value = controls.QuestionTypeID + ""
                            }).ToList());

            ViewBag.QuestionControlTypes = items;

            //Ends<<

            var Question = entityDB.Questions.Find(form.QuestionId);

            var errorMessage = "";

            ViewBag.Question = Question;

            Question.IsBold = form.IsBold;
            Question.IsMandatory = form.IsMandatory;
            Question.Visible = !form.Visible;
            Question.ResponseRequired = form.ResponseRequired;
            Question.HasDependantQuestions = form.HasDependent;
            //Rajesh on 12/31/2013
            if (!form.HasDependent)//If user unselects hasDependent remove all depends
            {
                if (Question.QuestionsDependants != null)
                {
                    foreach (var QDepends in Question.QuestionsDependants.ToList())
                    {
                        entityDB.QuestionsDependants.Remove(QDepends);
                    }
                }
            }
            //Ends<<<
            if (Question.QuestionColumnDetails == null || !(Question.QuestionColumnDetails.Where(rec => rec.PrequalificationUserInputs.Count() > 0).Count() > 0))
            {
                foreach (var column in Question.QuestionColumnDetails.ToList())
                {
                    entityDB.Entry(column).State = EntityState.Deleted;
                }
                if (form.ColumnDetails != null)
                    for (int i = 0; i < form.ColumnDetails.Count(); i++)
                    {
                        if (form.IsRepeat)
                        {
                            if (form.ColumnDetails[0].ControlType == "-1" || form.ColumnDetails[0].ControlType == null)//Rajesh on 1/3/2014
                            {
                                errorMessage = "Column 1 ";
                            }
                            else if (form.ColumnDetails[0].ControlType == "7" || form.ColumnDetails[0].ControlType == "10")//Radio or MultiSelect
                            {
                                if (string.IsNullOrEmpty(form.ColumnDetails[0].Value))
                                {
                                    errorMessage = "Column 1 ";
                                }
                            }
                            else if (!form.ColumnDetails[0].HasTableReference && form.ColumnDetails[0].ControlType == "9")
                            {
                                if (string.IsNullOrEmpty(form.ColumnDetails[0].Value))
                                {
                                    errorMessage = "Column 1 ";
                                }
                            }
                            QuestionColumnDetails details = new QuestionColumnDetails();
                            details.ColumnNo = i + 1;
                            details.ColumnValues = form.ColumnDetails[0].Value;
                            if (i == 0)
                                details.QFormula = form.ColumnDetails[0].FormulaValue;
                            if (form.ColumnDetails[0].HasFormula == false)
                            {
                                details.QFormula = null;
                            }
                            details.DisplayType = Convert.ToInt32(form.ColumnDetails[0].DisplayType);
                            details.QuestionControlTypeId = Convert.ToInt32(form.ColumnDetails[0].ControlType);
                            details.QuestionId = Question.QuestionID;
                            if (form.ColumnDetails[0].HasTableReference)
                            {
                                details.ColumnValues = "";
                                details.TableReference = form.ColumnDetails[0].RefTableName;
                                if (form.ColumnDetails[0].RefTableName == "BiddingInterests")
                                {
                                    details.TableReferenceField = "BiddingInterestId";
                                    details.TableRefFieldValueDisplay = "BiddingInterestName";
                                }
                                else if (form.ColumnDetails[0].RefTableName == "UnitedStates")
                                {
                                    details.TableReferenceField = "StatesId";
                                    details.TableRefFieldValueDisplay = "StateName";
                                }
                                else if (form.ColumnDetails[0].RefTableName == "CanadianProvinces")
                                {
                                    details.TableReferenceField = "ProvinceId";
                                    details.TableRefFieldValueDisplay = "ProvinceName";
                                }

                            }
                            Question.QuestionColumnDetails.Add(details);
                        }
                        else
                        {
                            if (form.ColumnDetails[0].ControlType == "-1" || form.ColumnDetails[0].ControlType == null)////Rajesh on 1/3/2014
                            {
                                errorMessage += " Column " + (i + 1) + ",";
                            }
                            else if (form.ColumnDetails[i].ControlType == "7" || form.ColumnDetails[i].ControlType == "10")//Radio or MultiSelect
                            {
                                if (string.IsNullOrEmpty(form.ColumnDetails[0].Value))
                                {
                                    errorMessage += " Column " + (i + 1) + ",";
                                }
                            }
                            else if (!form.ColumnDetails[i].HasTableReference && form.ColumnDetails[i].ControlType == "9")
                            {
                                if (string.IsNullOrEmpty(form.ColumnDetails[0].Value))
                                {
                                    errorMessage += " Column " + (i + 1) + ",";
                                }
                            }

                            QuestionColumnDetails details = new QuestionColumnDetails();
                            details.ColumnNo = i + 1;
                            details.ColumnValues = form.ColumnDetails[i].Value;
                            details.DisplayType = Convert.ToInt32(form.ColumnDetails[i].DisplayType);
                            details.QuestionControlTypeId = Convert.ToInt32(form.ColumnDetails[i].ControlType);

                            details.QFormula = form.ColumnDetails[i].FormulaValue;
                            if (form.ColumnDetails[i].HasFormula == false)
                            {
                                details.QFormula = null;
                            }

                            details.QuestionId = Question.QuestionID;
                            if (form.ColumnDetails[i].HasTableReference)
                            {
                                details.TableReference = form.ColumnDetails[0].RefTableName;
                                if (form.ColumnDetails[i].RefTableName == "BiddingInterests")
                                {
                                    details.TableReferenceField = "BiddingInterestId";
                                    details.TableRefFieldValueDisplay = "BiddingInterestName";
                                }
                                else if (form.ColumnDetails[0].RefTableName == "UnitedStates")
                                {
                                    details.TableReferenceField = "StatesId";
                                    details.TableRefFieldValueDisplay = "StateName";
                                }
                                else if (form.ColumnDetails[0].RefTableName == "CanadianProvinces")
                                {
                                    details.TableReferenceField = "ProvinceId";
                                    details.TableRefFieldValueDisplay = "ProvinceName";
                                }
                            }
                            Question.QuestionColumnDetails.Add(details);
                        }

                    }
            }
            if (errorMessage.Equals(""))
            {
                entityDB.SaveChanges();
                return Redirect("PopUpCloseView");
            }
            else
            {

                ////For Formula Fields

                var questions = entityDB.Questions.Find(form.QuestionId);
                var maxRows = 0;
                List<string> qValues = new List<string>();
                foreach (var allQColDetails in entityDB.QuestionColumnDetails.Where(rec => rec.Questions.SubSectionId == questions.SubSectionId && rec.QuestionId != form.QuestionId).GroupBy(rec => rec.QuestionId).ToList())
                {
                    int num = allQColDetails.Max(rec => rec.ColumnNo);
                    if (maxRows < num)
                        maxRows = num;
                    //string question = allQColDetails.Questions.QuestionText + "| Q" + allQColDetails.QuestionColumnId + " ";
                    string question = "";//allQColDetails.Questions.QuestionText + "| Q" + allQColDetails.QuestionColumnId + " ";
                    bool initialFlag = true;
                    foreach (var QcolumnDetails in allQColDetails.OrderBy(rec => rec.ColumnNo))
                    {
                        if (initialFlag)
                            question = QcolumnDetails.Questions.QuestionText;

                        if (QcolumnDetails.QuestionControlTypeId != 3)
                            question += "|-";
                        else
                            question += "| Q" + QcolumnDetails.QuestionColumnId + " ";
                        initialFlag = false;

                    }
                    qValues.Add(question);
                }
                foreach (var QColumns in form.ColumnDetails)
                {
                    QColumns.QuestionValues = qValues;
                }
                //Ends<<<


                ModelState.AddModelError("", "Incorrect data in: " + errorMessage.Substring(0, errorMessage.Length - 1));
                return View(form);
            }
        }
        

       
        [HttpPost]
        public string DeleteQuestions(string questionBankIds,long subSectionId)
        {
            if (questionBankIds == "1")//Delete Blank Questions
            {
                foreach (var question in entityDB.Questions.Where(rec => rec.QuestionBankId == 1 && rec.SubSectionId == subSectionId).ToList())
                {
                    //entityDB.Entry(question).State = EntityState.Deleted;
                    if (question.QuestionColumnDetails != null)
                        foreach (var Qcolumn in question.QuestionColumnDetails.ToList())
                            entityDB.Entry(Qcolumn).State = EntityState.Deleted;
                    if (question.QuestionsDependants != null)
                    {
                        foreach (var Qdependent in question.QuestionsDependants.ToList())
                            entityDB.Entry(Qdependent).State = EntityState.Deleted;
                    }
                    entityDB.Entry(question).State = EntityState.Deleted;
                }
                entityDB.SaveChanges();
                return "Ok";
            }
            
            foreach (var QBId in questionBankIds.Split(','))
            {
                
                try
                {
                    long QBankId = Convert.ToInt64(QBId);
                    foreach (var question in entityDB.Questions.Where(rec => rec.QuestionBankId == QBankId && rec.SubSectionId == subSectionId).ToList())
                    {
                        if (question.QuestionColumnDetails != null)
                            foreach (var Qcolumn in question.QuestionColumnDetails.ToList())
                                entityDB.Entry(Qcolumn).State = EntityState.Deleted;
                        if (question.QuestionsDependants != null)
                        {
                            foreach (var Qdependent in question.QuestionsDependants.ToList())
                                entityDB.Entry(Qdependent).State = EntityState.Deleted;
                        }
                        entityDB.Entry(question).State = EntityState.Deleted;
                    }
                }
                catch (Exception ee) { }
            }
            entityDB.SaveChanges();
            return "OK";
        }
        [SessionExpireForView]
        [HeaderAndSidebar(headerName = "QuestionnaireTool", sideMenuName = "TemplateTool", ViewName = "DependentQuestions")]
        public ActionResult DependentQuestions(long QuestionId)
        {
            var question=entityDB.Questions.Find(QuestionId);
            ViewBag.QuestionName = question.QuestionText;
            ViewBag.QuestionId = QuestionId;
            ViewBag.Question = question;
            getCommand();
            ViewBag.depends = (from sec in question.TemplateSubSections.TemplateSections.TemplateSubSections.Where(rec => rec.SubSectionID != question.SubSectionId).ToList()
                              select new SelectListItem
                              {
                                  Text=sec.SubSectionName,
                                  Value=sec.SubSectionID+"",
                                  
                              }).ToList();
            
            if (question.QuestionsDependants != null && question.QuestionsDependants.Count() != 0)
            {
                var questionDependent=question.QuestionsDependants.ToList();
                ViewBag.DependentValue = questionDependent[0].DependantValue;
                ViewBag.DependentType = questionDependent[0].DependantType;

                if (questionDependent[0].DependantType == 0)
                {
                    ViewBag.depends = (from questions in question.TemplateSubSections.Questions.Where(rec => rec.QuestionID != QuestionId).ToList()
                                       select new SelectListItem
                                       {
                                           Text = questions.QuestionText,
                                           Value = questions.QuestionID + "",
                                           Selected = questionDependent.FirstOrDefault(rec => rec.DependantId == questions.QuestionID && rec.DependantValue == questionDependent[0].DependantValue) != null//Rajesh on 11/26/2013
                                       }).ToList();
                }
                else
                {
                    ViewBag.depends = (from sec in question.TemplateSubSections.TemplateSections.TemplateSubSections.Where(rec => rec.SubSectionID != question.SubSectionId).ToList()
                                       select new SelectListItem
                                       {
                                           Text = sec.SubSectionName,
                                           Value = sec.SubSectionID + "",
                                           Selected = questionDependent.FirstOrDefault(rec => rec.DependantId == sec.SubSectionID && rec.DependantValue == questionDependent[0].DependantValue) != null//Rajesh on 11/26/2013
                                       }).ToList();
                }
            }

            return View();
        }

        [HttpPost]
        public ActionResult getDependentValues(long QuestionId, int type, string value)//0-Questions 1-Sections
        {
            var question = entityDB.Questions.Find(QuestionId);
            var data = (from sec in question.TemplateSubSections.TemplateSections.TemplateSubSections.Where(rec=>rec.SubSectionID!=question.SubSectionId).ToList()
                        select new SelectListItem
                        {
                            Text = sec.SubSectionName,
                            Value = sec.SubSectionID + "",

                        }).ToList();
            if (type == 0)
                data = (from questions in question.TemplateSubSections.Questions.Where(rec => rec.QuestionID != QuestionId).ToList()
                        select new SelectListItem
                        {
                            Text = questions.QuestionText,
                            Value = questions.QuestionID + ""
                        }).ToList();

            if (question.QuestionsDependants != null && question.QuestionsDependants.Count > 0)
            {
                var questionDependent = question.QuestionsDependants.Where(rec => rec.DependantValue == value).ToList();//Rajesh on 11/26/2013
                if (type == 0)
                {
                    data = (from questions in question.TemplateSubSections.Questions.Where(rec=>rec.QuestionID!=QuestionId).ToList()
                                       select new SelectListItem
                                       {
                                           Text = questions.QuestionText,
                                           Value = questions.QuestionID + "",
                                           Selected = questionDependent.FirstOrDefault(rec => rec.DependantId == questions.QuestionID) != null
                                       }).ToList();
                }
                else
                {
                    data = (from sec in question.TemplateSubSections.TemplateSections.TemplateSubSections.Where(rec => rec.SubSectionID != question.SubSectionId).ToList()
                                       select new SelectListItem
                                       {
                                           Text = sec.SubSectionName,
                                           Value = sec.SubSectionID + "",
                                           Selected = questionDependent.FirstOrDefault(rec => rec.DependantId == sec.SubSectionID) != null
                                       }).ToList();
                }
            }
            return Json(data);
        }


        [SessionExpire]
        [HeaderAndSidebar(headerName = "QuestionnaireTool", sideMenuName = "TemplateTool", ViewName = "TemplatePreview")]
        public ActionResult TemplatePreview(long TemplateSectionID,long ClientId,int type,Guid? templateId)//1-Standard 0-Custom
        {


            Templates selectedTemplate = entityDB.Templates.FirstOrDefault(rec=>rec.TemplateStatus==0 &&rec.DefaultTemplate==true);

            if(type==0)
                selectedTemplate = entityDB.Templates.FirstOrDefault(rec => rec.TemplateStatus == 0 && rec.DefaultTemplate == false);


            if (templateId != null)
            {
                selectedTemplate = entityDB.Templates.Find(templateId);
            }
            ViewBag.Type = type;

            ViewBag.TemplateId = selectedTemplate.TemplateID;//Rajesh on 12/21/2013
            //To Change Side Menu Selection>>>
            if (selectedTemplate.DefaultTemplate == null || selectedTemplate.DefaultTemplate == false)
            {
                List<LocalHeaderMenuModel> headerMenu = (List<LocalHeaderMenuModel>)ViewBag.headerMenus;
                headerMenu.FirstOrDefault(record => record.menuName == "QuestionnaireTool").sideMenus.ForEach(record => record.isChecked = false);
                headerMenu.FirstOrDefault(record => record.menuName == "QuestionnaireTool").sideMenus.FirstOrDefault(record => record.menuName == "CustomTemplateTool").isChecked = true;
                ViewBag.headerMenus = headerMenu;
            }

            //Ends<<<


            var templateSection=selectedTemplate.TemplateSections.OrderBy(rec=>rec.DisplayOrder).FirstOrDefault(rec=>rec.TemplateSectionID==TemplateSectionID);
            if (templateSection == null)
                templateSection=selectedTemplate.TemplateSections.Where(rec=>rec.TemplateSectionType!=0).OrderBy(rec => rec.DisplayOrder).FirstOrDefault();


            if (ClientId == -1)
            {
                ClientId=entityDB.Organizations.Where(rec => rec.OrganizationType == "Client").FirstOrDefault().OrganizationID;
            }
            TemplateSectionID = templateSection.TemplateSectionID;
            ViewBag.AllSections = (from sec in selectedTemplate.TemplateSections.Where(rec=>rec.TemplateSectionType!=0).OrderBy(rec=>rec.DisplayOrder).ToList()
                                  select new SelectListItem
                                  {
                                      Text=sec.SectionName,
                                      Value=sec.TemplateSectionID+"",
                                      Selected=sec.TemplateSectionID==TemplateSectionID
                                  }).ToList();
            if (type == 1)
                ViewBag.AllClients = (from org in entityDB.Organizations.Where(rec => rec.OrganizationType == "Client").ToList()
                                      select new SelectListItem
                                      {
                                          Text = org.Name,
                                          Value = org.OrganizationID + "",
                                          Selected = org.OrganizationID == ClientId
                                      }).ToList();

            else
            {
                var clientId = selectedTemplate.ClientTemplates.FirstOrDefault().ClientID;
                ViewBag.AllClients = (from org in entityDB.Organizations.Where(rec => rec.OrganizationType == "Client" && rec.OrganizationID==clientId).ToList()
                                      select new SelectListItem
                                      {
                                          Text = org.Name,
                                          Value = org.OrganizationID + "",
                                          Selected = org.OrganizationID == ClientId
                                      }).ToList();
            }
            getCommand();
            @ViewBag.PreviewTemplateName = selectedTemplate.TemplateName;
            ViewBag.TemplatePrice = "XXX";
            ViewBag.previewTemplateType = templateSection.TemplateSectionType;
            ViewBag.TemplateSectionID = TemplateSectionID;
            @ViewBag.ClientId = ClientId;
            return View(selectedTemplate);
        }
        public string saveDependancies(string DependentValue,int dependentType,string dependentIds,long QuestionId)
        {
            foreach (var depends in entityDB.QuestionsDependants.Where(rec => rec.QuestionID == QuestionId && rec.DependantValue == DependentValue).ToList())//Rajesh on 11/26/2013
            {
                entityDB.QuestionsDependants.Remove(depends);
            }
            foreach(var dependentid in dependentIds.Split(','))
            {
                try
                {
                    var id = Convert.ToInt64(dependentid);
                    var dependants = new QuestionsDependants();
                    dependants.QuestionID = QuestionId;
                    dependants.DependantValue = DependentValue;
                    dependants.DependantType = dependentType;
                    dependants.DependantId = id;
                    dependants.DependantDisplayOrder = 1;
                    entityDB.QuestionsDependants.Add(dependants);
                }
                catch (Exception ee)
                {
                }
            }
            entityDB.SaveChanges();
            return "Ok";
        }
        public void getCommand()
        {
            SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["EFDbContext"].ConnectionString);
            connection.Open();
            ViewBag.command = connection.CreateCommand();

        }

        public string ApproveTemplate(int TemplateType)//1-Standard 0-Custom
        {
            if (TemplateType == 1)
            {
                var template = entityDB.Templates.FirstOrDefault(rec => rec.DefaultTemplate == true && rec.TemplateStatus==0);
                template.TemplateStatus = 1;
                entityDB.Entry(template).State = EntityState.Modified;

                if (template.TemplateSections.Where(rec => rec.TemplateSectionType == 3 || rec.TemplateSectionType == 99 || rec.TemplateSectionType == 4 || rec.TemplateSectionType == 7).Count() < 4)
                {
                    return "Incomplete Sections";
                }
                var sectionWithoutSubsecs=template.TemplateSections.FirstOrDefault(rec => rec.TemplateSectionType != 0 && rec.TemplateSectionType != 5 && rec.TemplateSectionType != 6  && rec.TemplateSectionType != 99 && (rec.TemplateSubSections == null || rec.TemplateSubSections.Count() == 0));
                if (sectionWithoutSubsecs != null)
                {
                    return "Incomplete Sections";
                }

                
                var currentOrgClientTemplates = entityDB.ClientTemplates.Where(record => record.TemplateID == template.TemplateID).ToList();
                foreach (var clientTemplate in currentOrgClientTemplates)
                {
                    foreach (var client in clientTemplate.Organizations.ClientBusinessUnits.ToList())
                    {
                        if (entityDB.ClientTemplatesForBU.FirstOrDefault(rec=>rec.ClientBusinessUnitId==client.ClientBusinessUnitId &&rec.ClientTemplateId==clientTemplate.ClientTemplateID) == null)
                        {
                            ClientTemplatesForBU currentClientTemplateBU = new ClientTemplatesForBU();
                            currentClientTemplateBU.ClientTemplateId = clientTemplate.ClientTemplateID;
                            currentClientTemplateBU.ClientBusinessUnitId = client.ClientBusinessUnitId;
                            entityDB.ClientTemplatesForBU.Add(currentClientTemplateBU);
                            entityDB.SaveChanges();
                        }
                    }
                    
                }

                entityDB.SaveChanges();
            }
            else
            {
                var template = entityDB.Templates.FirstOrDefault(rec => rec.DefaultTemplate == false && rec.TemplateStatus == 0);
                template.TemplateStatus = 1;
                entityDB.Entry(template).State = EntityState.Modified;


                if (template.TemplateSections.Where(rec => rec.TemplateSectionType == 3 || rec.TemplateSectionType == 99 || rec.TemplateSectionType == 4 || rec.TemplateSectionType == 7).Count() < 4)
                {
                    return "Incomplete Sections";
                }
                var sectionWOSubSection=template.TemplateSections.FirstOrDefault(rec => rec.TemplateSectionType != 0 && rec.TemplateSectionType != 99 && (rec.TemplateSubSections == null || rec.TemplateSubSections.Count() == 0));
                if (sectionWOSubSection != null)
                {
                    return "Incomplete Sections";
                }

                var currentOrgClientTemplates = entityDB.ClientTemplates.Where(record => record.TemplateID == template.TemplateID).ToList();
                foreach (var clientTemplate in currentOrgClientTemplates)
                {
                    foreach (var client in clientTemplate.Organizations.ClientBusinessUnits.ToList())
                    {
                        if (entityDB.ClientTemplatesForBU.FirstOrDefault(rec => rec.ClientBusinessUnitId == client.ClientBusinessUnitId && rec.ClientTemplateId == clientTemplate.ClientTemplateID) == null)
                        {
                            ClientTemplatesForBU currentClientTemplateBU = new ClientTemplatesForBU();
                            currentClientTemplateBU.ClientTemplateId = clientTemplate.ClientTemplateID;
                            currentClientTemplateBU.ClientBusinessUnitId = client.ClientBusinessUnitId;
                            entityDB.ClientTemplatesForBU.Add(currentClientTemplateBU);
                            entityDB.SaveChanges();
                        }
                    }

                }
                entityDB.SaveChanges();
            }
            return "Ok";
        }


        //Rajesh on 10/28/2013
        [SessionExpire]
        [HeaderAndSidebar(headerName = "QuestionnaireTool", sideMenuName = "AssignedTemplatesList", ViewName = "AssignedTemplatesList")]
        public ActionResult AssignedTemplatesList()
        {

            var clientTemplates = entityDB.ClientTemplates.Where(rec=>rec.Templates.TemplateType==0 && rec.Templates.TemplateStatus == 1 &&rec.Templates.DefaultTemplate==true).GroupBy(record => record.Templates.TemplateName).ToList();
            ViewBag.VBClientTemplates = clientTemplates;//Here we will show only standard templates
            return View();
        }

        [HttpGet]
        [SessionExpire]
        [HeaderAndSidebar(headerName = "QuestionnaireTool", sideMenuName = "AssignedTemplatesList", ViewName = "AssignTemplates")]
        public ActionResult AssignTemplates(Guid? clientTemplateId)
        {

            long clientId = -1;
            Guid? templateId = null;

            if (clientTemplateId != null)
            {
                var clientTemplate = entityDB.ClientTemplates.Find(clientTemplateId);
                clientId = clientTemplate.ClientID;
                templateId = clientTemplate.TemplateID;
            }

            LocalAssignTemplates assignTemplate = new LocalAssignTemplates();

            ViewBag.Templates = (from templates in entityDB.Templates.ToList()
                                 where templates.TemplateType == 0 && templates.TemplateStatus == 1 && templates.DefaultTemplate == true
                                 select new SelectListItem
                                 {
                                     Text = templates.TemplateName,
                                     Value = templates.TemplateID + "",
                                     Selected = templateId == null ? false : templateId == templates.TemplateID
                                 }).ToList();

            ViewBag.Clients = (from clients in entityDB.Organizations.ToList()
                               where clients.OrganizationType == "Client"
                               select new SelectListItem
                               {
                                   Text = clients.Name,
                                   Value = clients.OrganizationID + "",
                                   Selected = clientId == clients.OrganizationID
                               }).ToList();

            assignTemplate.DocumentTypes = (from documentType in entityDB.DocumentType.ToList()
                                            where documentType.PrequalificationUseOnly == true
                                            select new CheckBoxesControl
                                            {
                                                Text = documentType.DocumentTypeName,
                                                id = documentType.DocumentTypeId + ""
                                            }).ToList();

            assignTemplate.ProficiencyCapabilities = (from proficiency in entityDB.ProficiencyCapabilities.ToList().OrderBy(rec =>rec.ProficiencyCategory).ThenBy(rec => rec.ProficiencySubCategroy).ToList()
                                                      where proficiency.Status == 1
                                                      select new CheckBoxesControl
                                                      {
                                                          id = proficiency.ProficiencyId + "",
                                                          Header = proficiency.ProficiencyCategory,
                                                          Text = (string.IsNullOrEmpty(proficiency.ProficiencySubCategroy) ? "" :

proficiency.ProficiencySubCategroy + "-") + proficiency.ProficiencyName,
                                                          type = 1
                                                      }).ToList();

            assignTemplate.BiddingInterests = (from biddings in entityDB.BiddingInterests.ToList()
                                               where biddings.Status ==1
                                               select new CheckBoxesControl
                                               {
                                                   Text = biddings.BiddingInterestName,
                                                   id = biddings.BiddingInterestId + ""
                                               }).ToList();



            return View(assignTemplate);
        }

        [HttpPost]
        [SessionExpire]
        [HeaderAndSidebar(headerName = "QuestionnaireTool", sideMenuName = "AssignedTemplatesList", ViewName = "AssignTemplates")]
        public ActionResult AssignTemplates(LocalAssignTemplates form)
        {
            if (string.IsNullOrEmpty(form.SelectedTemplate))
            {
                ModelState.AddModelError("SelectedTemplate", "*");
                
            }
            if (string.IsNullOrEmpty(form.SelectedClientId))
            {
                ModelState.AddModelError("SelectedClientId", "*");
            }
            if (!ModelState.IsValid)
            {
                ViewBag.Templates = (from templates in entityDB.Templates.ToList()
                                     where templates.TemplateType == 0 && templates.TemplateStatus == 1 && templates.DefaultTemplate == true
                                     select new SelectListItem
                                     {
                                         Text = templates.TemplateName,
                                         Value = templates.TemplateID + "",
                                         
                                     }).ToList();

                ViewBag.Clients = (from clients in entityDB.Organizations.ToList()
                                   where clients.OrganizationType == "Client"
                                   select new SelectListItem
                                   {
                                       Text = clients.Name,
                                       Value = clients.OrganizationID + ""
                                       
                                   }).ToList();
                ViewBag.EditingMode = true;
                return View(form);
            }

            var isInsert = false;
            var clientId = Convert.ToInt64(form.SelectedClientId);
            var TemplateID = new Guid(form.SelectedTemplate);
            //foreach (var clients in form.Clients.Where(rec => rec.Status == true))
            {
                var CTemplate = entityDB.ClientTemplates.FirstOrDefault(rec => rec.ClientID == clientId && rec.TemplateID == TemplateID);
                if (CTemplate == null)
                {
                    isInsert = true;
                    CTemplate = new ClientTemplates();
                    CTemplate.ClientID = Convert.ToInt64(form.SelectedClientId);
                    CTemplate.DefaultTemplate = false;
                    CTemplate.DisplayOrder = 0;
                    CTemplate.TemplateID = new Guid(form.SelectedTemplate);
                    CTemplate.Visible = true;

                    CTemplate.ClientTemplateReportingData = new List<ClientTemplateReportingData>();

                    // Kiran on 12/05/2013
                    var clientBusUnits = entityDB.ClientBusinessUnits.Where(record => record.ClientId == clientId);
                    foreach (var unit in clientBusUnits)
                    {
                        ClientTemplatesForBU addClientTemplateBU = new ClientTemplatesForBU();
                        addClientTemplateBU.ClientBusinessUnitId = unit.ClientBusinessUnitId;
                        addClientTemplateBU.ClientTemplateId = CTemplate.ClientTemplateID;
                        entityDB.ClientTemplatesForBU.Add(addClientTemplateBU);
                    }
                    // Ends<<


                }
                if(form.DocumentTypes!=null)
                foreach (var doctype in form.DocumentTypes.Where(rec => rec.Status == true))//For Documents
                {
                    var id = Convert.ToInt64(doctype.id);
                    if (entityDB.ClientTemplateReportingData.FirstOrDefault(rec => rec.ReportingType == 2 && rec.ReportingTypeId == id && rec.ClientTemplateId == CTemplate.ClientTemplateID) == null)
                    {
                        ClientTemplateReportingData report = new ClientTemplateReportingData();
                        report.ReportingType = 2;
                        report.ReportingTypeId = id;
                        report.DocumentExpires = doctype.isExpire;
                        CTemplate.ClientTemplateReportingData.Add(report);
                    }
                }
                if (form.ProficiencyCapabilities != null)
                foreach (var proficiency in form.ProficiencyCapabilities.Where(rec => rec.Status == true))//For proficiency
                {
                    var id = Convert.ToInt64(proficiency.id);
                    if (entityDB.ClientTemplateReportingData.FirstOrDefault(rec => rec.ReportingType == 1 && rec.ReportingTypeId == id && rec.ClientTemplateId == CTemplate.ClientTemplateID) == null)
                    {
                        ClientTemplateReportingData report = new ClientTemplateReportingData();
                        report.ReportingType = 1;
                        report.ReportingTypeId = Convert.ToInt64(proficiency.id);

                        CTemplate.ClientTemplateReportingData.Add(report);
                    }
                }
                if (form.BiddingInterests != null)
                foreach (var bidding in form.BiddingInterests.Where(rec => rec.Status == true))//For Documents
                {
                    var id = Convert.ToInt64(bidding.id);
                    if (entityDB.ClientTemplateReportingData.FirstOrDefault(rec => rec.ReportingType == 0 && rec.ReportingTypeId == id && rec.ClientTemplateId == CTemplate.ClientTemplateID) == null)
                    {
                        ClientTemplateReportingData report = new ClientTemplateReportingData();
                        report.ReportingType = 0;
                        report.ReportingTypeId = Convert.ToInt64(bidding.id);

                        CTemplate.ClientTemplateReportingData.Add(report);
                    }
                }
                if (isInsert)
                    entityDB.ClientTemplates.Add(CTemplate);
                else
                    entityDB.Entry(CTemplate).State = EntityState.Modified;
                entityDB.SaveChanges();
            }
            return Redirect("PopUpCloseView");
        }
        //<<<Ends
        //Rajesh on 11/25/2013
        public string ChangeQuestionsOrder(string QuestionIds, long templateSubSectionId)
        {
            string result = "Ok" + QuestionIds + "\n";
            try
            {
                int orderNumber = 0;
                var templateSubSec = entityDB.TemplateSubSections.Find(templateSubSectionId);
                if (templateSubSec != null)
                    foreach (var ids in QuestionIds.Split('|'))
                    {
                        try
                        {
                            long secId = Convert.ToInt64(ids);
                            result += secId;
                            Questions Question = templateSubSec.Questions.ToList().FirstOrDefault(rec => rec.QuestionID == secId);
                            if (Question == null)
                                continue;
                            orderNumber++;
                            Question.DisplayOrder = orderNumber;
                            entityDB.Entry(Question).State = EntityState.Modified;
                        }
                        catch (Exception ee) { }
                    }
                entityDB.SaveChanges();
            }
            catch (Exception ee) { result += ee.Message; }
            return result;
        }
        //Ends
        public ActionResult GetClientDocsProficiencyBiddings(string selectedTemplate, string selectedClient)
        {
            long ClientId = Convert.ToInt64(selectedClient);
            var selectedClientTemplate = entityDB.ClientTemplates.FirstOrDefault(record => record.TemplateID == new Guid(selectedTemplate) &&

record.ClientID == ClientId).ClientTemplateID;

            var data = (from report in entityDB.ClientTemplateReportingData.ToList()
                        where report.ClientTemplateId == selectedClientTemplate
                        select new BiddingCheckBoxes
                        {
                            id = report.ReportingTypeId + "",
                            status = true.ToString(),
                            isExpire = report.DocumentExpires == null ? false : (bool)report.DocumentExpires,
                            type = report.ReportingType
                        }).ToList();

            return Json(data);
        }
    }


    public class BiddingCheckBoxes
    {
        public string id { get; set; }
        public string Text { get; set; }
        public string status { get; set; }
        public int type { get; set; }//0 Bidding 1-proficancy & category
        public string Header { get; set; }//this is For Proficiency Category
        public bool isExpire { get; set; }//only for documents
    }
}
