﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FVGen3.WebUI.Annotations;
using FVGen3.Domain.Abstract;
using FVGen3.Domain.Concrete;
using FVGen3.Domain.Entities;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using AhaApps.Libraries.Extensions;
using FVGen3.BusinessLogic.Interfaces;
using FVGen3.WebUI.Constants;
using FVGen3.DataLayer.DTO;
using FVGen3.Domain.LocalModels;
using Resources;
using FVGen3.Domain.LocalModels;
using System.Threading.Tasks;

namespace FVGen3.WebUI.Controllers
{
    public class TemplateToolController : BaseController
    {
        //
        // GET: /TemplateTool/
        EFDbContext entityDB;
        private ITemplateToolBusiness _templateTool;
        private IClientBusiness _client;
        private IOrganizationBusiness _org;
        private IPrequalificationBusiness _preq;
        public TemplateToolController(ITemplateToolBusiness templateTool, IClientBusiness client, IOrganizationBusiness org, IPrequalificationBusiness preq)
        {
            _templateTool = templateTool;
            _client = client;
            _org = org;
            _preq = preq;
            entityDB = new EFDbContext();
        }

        //Siva on Nov 11th
        public virtual JsonResult uploadImage(HttpPostedFileBase file)
        {
            if (file != null && file.ContentLength > 0)
            {
                //SAVE file
                var fileName = Path.GetFileName(file.FileName);
                var extension = Path.GetExtension(file.FileName);

                Directory.CreateDirectory(Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["HeaderImagesPath"])));
                var path = Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["HeaderImagesPath"]), fileName + extension);
                file.SaveAs(path);

                Random randomObj = new Random();
                long randorNum = randomObj.Next(1, 1000000);
                string newFilePath = Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["HeaderImagesPath"]), randorNum + extension);
                if (!System.IO.File.Exists(newFilePath))
                {
                    System.IO.File.Move(path, newFilePath);
                }
                else
                {
                    randorNum = randomObj.Next(1, 1000000);
                    newFilePath = Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["HeaderImagesPath"]), randorNum + extension);
                    System.IO.File.Move(path, newFilePath);
                }

                string[] data = new string[1];
                data[0] = ConfigurationSettings.AppSettings["HeaderImagesPath"] + "/" + randorNum + extension;
                return Json(data);
                //return "docPath:" + ;
            }

            string[] response = new string[1];
            response[0] = "doc upload failed";
            return Json(response);
        }
        //End on Nov 11th

        //Rajesh changes on 12/19/2013
        [HttpPost]
        public ActionResult GetTemplates(int type, long? client)//0-custom 1-standard
        {
            var Data = (from Template in entityDB.Templates.Where(rec => rec.DefaultTemplate == true && rec.TemplateType == 0 && rec.TemplateStatus == 1).ToList()
                        select new SelectListItem
                        {
                            Text = Template.TemplateName,
                            Value = Template.TemplateID + ""
                        }).ToList();
            if (type == 0)
            {
                Data = (from Template in entityDB.ClientTemplates.Where(rec => rec.ClientID == client && rec.Templates.DefaultTemplate == false && rec.Templates.TemplateType == 0 && rec.Templates.TemplateStatus == 1).ToList()
                        select new SelectListItem
                        {
                            Text = Template.Templates.TemplateName,
                            Value = Template.TemplateID + ""
                        }).ToList();
            }
            return Json(Data);
        }
        //Ends<<<
        [OutputCache(Duration = 0)]
        public ActionResult GetTemplateQuestins(Guid TemplateId)
        {

            var questions = _templateTool.GetTemplateQuestins(TemplateId);



            return Json(questions, JsonRequestBehavior.AllowGet);
        }
        public string HideTemplate(Guid TemplateId, bool IsChecked)
        {
            var templates = entityDB.Templates.Find(TemplateId);
            templates.IsHide = IsChecked;

            entityDB.Entry(templates).State = EntityState.Modified;
            entityDB.SaveChanges();

            return "Success";

        }
        public string SelectedTemplateQuestions(long questionid, decimal? MaxValue, bool checkbox)
        {
            var result = "";
            var Data = entityDB.VendorDetailQuestions.FirstOrDefault(r => r.QuestionId == questionid && r.Type == 1);
            if (Data != null)
            {
                result = "Data Exist";
            }
            else
            {
                var TemplateID = entityDB.Questions.Find(questionid).TemplateSubSections.TemplateSections.TemplateID;

                VendorDetailQuestions vs = new VendorDetailQuestions();
                vs.QuestionId = questionid;
                vs.TemplateId = TemplateID;
                vs.Type = 1;
                vs.Value = MaxValue;
                vs.IsMax = checkbox;
                entityDB.VendorDetailQuestions.Add(vs);


                entityDB.SaveChanges();
                result = "Data Exist";
            }

            return result;
        }
        public string DeleteSelectedTemplateQuestions(long questionid)
        {
            var result = "";
            var Data = entityDB.VendorDetailQuestions.FirstOrDefault(r => r.QuestionId == questionid && r.Type == 1);
            if (Data != null)
            {
                entityDB.VendorDetailQuestions.Remove(Data);
                entityDB.SaveChanges();
                result = "Record Deleted";
            }

            return result;
        }
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "QuestionnaireTool", sideMenuName = "CustomTemplateTool", ViewName = "CreateCustomTemplate")]
        [SessionExpire]

        public ActionResult CreateCustomTemplate()
        {
            var templates = entityDB.Templates.Where(tempRec => tempRec.TemplateType == 0 && tempRec.DefaultTemplate == false).OrderBy(rec => rec.TemplateName).ToList();//Rajesh on 10/28/2013 // Kiran on 03/17/2015

            return View(templates);

        }
        [HttpGet]
        //[HandleError(View = "Error")]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "QuestionnaireTool", sideMenuName = "CustomTemplateTool", ViewName = "AddOrEditCustomTemplate")]
        public ActionResult AddOrEditCustomTemplate(Guid? TemplateId)
        {
            ViewBag.RiskLevels = Properties.Settings.Default.RIskLevels.Cast<string>().ToList().Select(x =>
                      new SelectListItem()
                      {
                          Text = x.ToString()
                      }).ToList();

            LocalTemplateModel template = new LocalTemplateModel();
            if (TemplateId != null)
            {
                Templates mainTemp = entityDB.Templates.Find(TemplateId);
                template.TemplateName = mainTemp.TemplateName;
                template.Templateid = mainTemp.TemplateID;
                template.TemplateNote = mainTemp.TemplateNotes;
                template.ClientId = template.IsERI ? mainTemp.ClientTemplates.FirstOrDefault(r => r.Organizations.OrganizationType == OrganizationType.SuperClient).ClientID : mainTemp.ClientTemplates.FirstOrDefault().ClientID;
                template.TemplateCode = mainTemp.TemplateCode;
                template.TemplateSequence = mainTemp.TemplateSequence;
                template.RiskLevel = mainTemp.RiskLevel;
                template.LockComments = mainTemp.LockComments;
                template.LanguageId = mainTemp.LanguageId;
                template.LockedByDefault = (mainTemp.LockedByDefault != null && mainTemp.LockedByDefault.Value);
                template.RenewalLockedByDefault = (mainTemp.RenewalLock != null && mainTemp.RenewalLock.Value);
                template.ExcludePQNotifications = mainTemp.ExcludePQNotifications ?? false;
                template.HideClients = mainTemp.HideClients ?? false;
                template.HasLocationNotification = mainTemp.HasLocationNotification;
                template.HasVendorDetailLocation = mainTemp.HasVendorDetailLocation;
                template.HideCommentsSec = mainTemp.HideCommentsSec;
                template.TemplateStatus = mainTemp.TemplateStatus;
                template.ClientName = entityDB.Organizations.FirstOrDefault(r => r.OrganizationID == template.ClientId).Name;
                if (!String.IsNullOrWhiteSpace(mainTemp.unlockNotificationAdminEmails))
                {
                    template.unlockNotificationAdminEmails = mainTemp.unlockNotificationAdminEmails.Split(',').ToList();
                }

                template.EmaiTemplateId = -1;
                if (mainTemp.EmaiTemplateId != null)
                {
                    template.EmaiTemplateId = mainTemp.EmaiTemplateId.Value;
                }
                template.IsERI = mainTemp.IsERI;
                if (template.IsERI)
                {
                    var superClientId = mainTemp.ClientTemplates.Where(r => r.Organizations.OrganizationType == OrganizationType.SuperClient).FirstOrDefault().ClientID;
                    template.ERIClients = mainTemp.ClientTemplates.Where(rec => rec.Organizations.OrganizationType != OrganizationType.SuperClient).Select(rec => rec.ClientID).ToList();
                    template.SubClients = (from subClients in entityDB.SubClients.Where(r => r.SuperClientId == superClientId).ToList()
                                           select new SelectListItem
                                           {
                                               Text = subClients.SubClientOrganization.Name,
                                               Value = subClients.SubClientOrganization.OrganizationID.ToString(),
                                               Selected = template.ERIClients.Contains(subClients.ClientId)
                                           }).ToList();
                }

            }
            int clientID = template.ClientId.ToIntNullSafe();

            template.ListOfAdminUsersForNotificationsTosent = (from users in entityDB.SystemUsersOrganizations.Where(rec => rec.Organizations.OrganizationType == "Admin" && rec.SystemUsers.UserStatus == true).ToList()
                                                               select new SelectListItem
                                                               {
                                                                   Text = users.SystemUsers.Contacts == null || users.SystemUsers.Contacts.Count == 0 ? "" : users.SystemUsers.Contacts.FirstOrDefault().LastName + ", " + users.SystemUsers.Contacts.FirstOrDefault().FirstName,//Rajesh on 2/11/2014
                                                                   Value = users.SystemUsers.Email.ToString()
                                                               }).ToList();


            ViewBag.EmailTemplates = (from emailtemplate in entityDB.EmailTemplates.Where(rec => rec.EmailUsedFor == 0).OrderBy(o => o.Name).ToList()
                                      select new SelectListItem
                                      {
                                          Text = emailtemplate.Name,
                                          Value = emailtemplate.EmailTemplateID + ""
                                      }).ToList();
            ((List<SelectListItem>)ViewBag.EmailTemplates).Insert(0, new SelectListItem() { Text = "Select Email Template", Value = "-1" });
            var SubClients = entityDB.SubClients.Select(r => r.ClientId);
            ViewBag.Clients = (from clients in entityDB.Organizations.Where(rec => (rec.OrganizationType == OrganizationType.Client || rec.OrganizationType == OrganizationType.SuperClient) && !SubClients.Contains(rec.OrganizationID)).ToList().OrderBy(rec => rec.Name)
                               select new SelectListItem()
                               {
                                   Text = clients.Name,
                                   Value = clients.OrganizationID + ""
                               }).ToList();
            ViewBag.Templates = (from temp in entityDB.Templates.Where(rec => rec.DefaultTemplate == false && rec.TemplateType == 1).OrderBy(rec => rec.TemplateName).ToList()//sumanth on 10/23/2015 for FVOS-83
                                 select new SelectListItem()
                                 {
                                     Text = temp.TemplateName,
                                     Value = temp.TemplateID + ""
                                 }).ToList();
            ViewBag.Languages = (from languages in entityDB.Language.ToList().OrderBy(rec => rec.LanguageName)
                                 select new SelectListItem()
                                 {
                                     Text = languages.LanguageName,
                                     Value = languages.LanguageId + ""
                                 }).ToList();
            return View(template);
        }

        [HttpPost]
        public JsonResult AgreementEmailConfiguration(AgreementSubsectionModel form)
        {
            var result = "Configuration created successfully";
            form.LoginUser = SSession.UserId;
            try
            {
                _templateTool.ConfigureAgreementEmail(form);
            }
            catch (Exception e)
            {
                result = "Unable to process the request. Please try again.";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetSubClients(long orgID)
        {
            return Json(_client.GetSubClients(orgID), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetAgreementConfiguration(long subSectionId)
        {
            return Json(_templateTool.GetAgreementConfiguration(subSectionId), JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "QuestionnaireTool", sideMenuName = "CustomTemplateTool", ViewName = "AddOrEditCustomTemplate")]
        public ActionResult AddOrEditCustomTemplate(LocalTemplateModel Form)
        {

            ViewBag.RiskLevels = Properties.Settings.Default.RIskLevels.Cast<string>().ToList().Select(x =>
                                  new SelectListItem()
                                  {
                                      Text = x.ToString()
                                  }).ToList();
            ViewBag.Clients = (from clients in entityDB.Organizations.Where(rec => (rec.OrganizationType == OrganizationType.Client || rec.OrganizationType == OrganizationType.SuperClient)).OrderBy(rec => rec.Name).ToList()
                               select new SelectListItem()
                               {
                                   Text = clients.Name,
                                   Value = clients.OrganizationID + ""
                               }).ToList();
            ViewBag.Templates = (from temp in entityDB.Templates.Where(rec => rec.DefaultTemplate == false && rec.TemplateType == 1).OrderBy(rec => rec.TemplateName).ToList()
                                 select new SelectListItem()
                                 {
                                     Text = temp.TemplateName,
                                     Value = temp.TemplateID + ""
                                 }).ToList();
            ViewBag.EmailTemplates = (from emailtemplate in entityDB.EmailTemplates.Where(rec => rec.EmailUsedFor == 0).OrderBy(o => o.Name).ToList()
                                      select new SelectListItem
                                      {
                                          Text = emailtemplate.Name,
                                          Value = emailtemplate.EmailTemplateID + ""
                                      }).ToList();
            ViewBag.Languages = (from languages in entityDB.Language.ToList().OrderBy(rec => rec.LanguageName)
                                 select new SelectListItem()
                                 {
                                     Text = languages.LanguageName,
                                     Value = languages.LanguageId + ""
                                 }).ToList();
            Form.ListOfAdminUsersForNotificationsTosent = (from users in entityDB.SystemUsersOrganizations.Where(rec => rec.Organizations.OrganizationType == "Admin" && rec.SystemUsers.UserStatus == true).ToList()
                                                           select new SelectListItem
                                                           {
                                                               Text = users.SystemUsers.Contacts == null || users.SystemUsers.Contacts.Count == 0 ? "" : users.SystemUsers.Contacts.FirstOrDefault().LastName + ", " + users.SystemUsers.Contacts.FirstOrDefault().FirstName,//Rajesh on 2/11/2014
                                                               Value = users.SystemUsers.Email.ToString()
                                                           }).ToList();
            var clientOrg = _org.GetOrganization(Form.ClientId);
            if (clientOrg.OrganizationType != OrganizationType.SuperClient)
                ModelState.Remove("ERIClients");
            //else
            //    ModelState.Remove("ClientId");
            if (!ModelState.IsValid)
                return View(Form);
            Templates newTemplate = new Templates();
            if (Form.Templateid != null)//Update Template
            {
                newTemplate = entityDB.Templates.Find(Form.Templateid);

                RecordTemplateChange(Form, newTemplate);
                newTemplate.LockComments = Form.LockComments;
                newTemplate.TemplateName = Form.TemplateName;
                newTemplate.TemplateNotes = Form.TemplateNote;
                newTemplate.TemplateCode = Form.TemplateCode;
                newTemplate.TemplateSequence = Form.TemplateSequence;
                newTemplate.RiskLevel = Form.RiskLevel;
                newTemplate.LockedByDefault = Form.LockedByDefault;
                newTemplate.RenewalLock = Form.RenewalLockedByDefault;
                newTemplate.LanguageId = Form.LanguageId;
                newTemplate.HasLocationNotification = Form.HasLocationNotification;
                newTemplate.HasVendorDetailLocation = Form.HasVendorDetailLocation;
                newTemplate.HideCommentsSec = Form.HideCommentsSec;
                newTemplate.HideClients = Form.HideClients;
                if (Form.unlockNotificationAdminEmails != null)
                {
                    newTemplate.unlockNotificationAdminEmails = String.Join(",", Form.unlockNotificationAdminEmails);
                }
                else
                {
                    newTemplate.unlockNotificationAdminEmails = null;
                }
                newTemplate.EmaiTemplateId = Form.EmaiTemplateId;
                newTemplate.ExcludePQNotifications = Form.ExcludePQNotifications;
                if (Form.TemplateStatus != 1)
                {
                    if (clientOrg.OrganizationType == OrganizationType.SuperClient)
                    {
                        var clientTemplates = newTemplate.ClientTemplates.Where(r => r.Organizations.OrganizationType != OrganizationType.SuperClient).ToList();

                        foreach (var clientTemplate in clientTemplates)
                        {
                            entityDB.Entry(clientTemplate).State = EntityState.Deleted;
                        }
                        entityDB.SaveChanges();

                        foreach (var eri in Form.ERIClients)
                        {
                            var clientTemplate = new ClientTemplates
                            {
                                ClientID = Convert.ToInt64(eri),
                                DisplayOrder = 1,
                                Visible = true,
                                DefaultTemplate = false
                            };
                            newTemplate.ClientTemplates.Add(clientTemplate);
                        }
                    }
                }
                entityDB.Entry(newTemplate).State = EntityState.Modified;
                entityDB.SaveChanges();
            }
            else//Save Template
            {
                newTemplate.ExcludePQNotifications = Form.ExcludePQNotifications;
                newTemplate.LockComments = Form.LockComments;
                newTemplate.TemplateCode = Form.TemplateCode;
                newTemplate.TemplateSequence = Form.TemplateSequence;
                newTemplate.TemplateName = Form.TemplateName;
                newTemplate.TemplateType = 0;
                newTemplate.TemplateNotes = Form.TemplateNote;
                newTemplate.LockedByDefault = Form.LockedByDefault;
                
                newTemplate.RenewalLock = Form.RenewalLockedByDefault;
                newTemplate.IsERI = clientOrg.OrganizationType == OrganizationType.SuperClient;
                newTemplate.HideClients = Form.HideClients;
                newTemplate.HasLocationNotification = Form.HasLocationNotification;
                newTemplate.HasVendorDetailLocation = Form.HasVendorDetailLocation;
                newTemplate.HideCommentsSec = Form.HideCommentsSec;
                newTemplate.LanguageId = Form.LanguageId;
                if (Form.unlockNotificationAdminEmails != null)
                {
                    newTemplate.unlockNotificationAdminEmails = string.Join(",", Form.unlockNotificationAdminEmails);
                }
                else
                {
                    newTemplate.unlockNotificationAdminEmails = null;
                }
                newTemplate.EmaiTemplateId = Form.EmaiTemplateId;
                newTemplate.DefaultTemplate = false;
                newTemplate.TemplateStatus = 0;
                newTemplate.RiskLevel = Form.RiskLevel;
                newTemplate.ClientTemplates = new List<ClientTemplates>();
                //if (!Form.IsERI)
                //{
                var clientTemplate = new ClientTemplates
                {
                    ClientID = Convert.ToInt64(Form.ClientId),
                    DisplayOrder = 1,
                    Visible = true,
                    DefaultTemplate = false
                };
                newTemplate.ClientTemplates.Add(clientTemplate);
                //}
                //else
                if (clientOrg.OrganizationType == OrganizationType.SuperClient)
                {
                    //clientTemplate = new ClientTemplates
                    //{
                    //    ClientID = Convert.ToInt64(Form.ClientId),
                    //    DisplayOrder = 1,
                    //    Visible = true,
                    //    DefaultTemplate = false
                    //};
                    //newTemplate.ClientTemplates.Add(clientTemplate);
                    foreach (var eri in Form.ERIClients)
                    {
                        clientTemplate = new ClientTemplates
                        {
                            ClientID = Convert.ToInt64(eri),
                            DisplayOrder = 1,
                            Visible = true,
                            DefaultTemplate = false
                        };
                        newTemplate.ClientTemplates.Add(clientTemplate);
                    }
                }
                entityDB.Templates.Add(newTemplate);
                entityDB.SaveChanges();

                if (Form.isCopyTemplateSelected)//Copy The Template
                {
                    Guid? selectedTemplateId = null;
                    try { selectedTemplateId = new Guid(Form.CopyTemplateId); }
                    catch { }
                    if (selectedTemplateId != null)
                    {
                        Templates selectedTemplate = entityDB.Templates.Find(selectedTemplateId);
                        if (selectedTemplate != null)
                        {
                            //if (Form.isSectionSelected)//Adding sections
                            {
                                newTemplate.TemplateSections = new List<TemplateSections>();

                                foreach (var sections in selectedTemplate.TemplateSections)
                                {
                                    TemplateSections newSection = new TemplateSections();
                                    newSection.DisplayOrder = sections.DisplayOrder;
                                    newSection.Visible = sections.Visible;
                                    newSection.SectionName = sections.SectionName;
                                    newSection.TemplateSectionType = sections.TemplateSectionType;
                                    newSection.VisibleToClient = sections.VisibleToClient;
                                    newTemplate.TemplateSections.Add(newSection);
                                    //Rajesh on 2/15/2014
                                    newSection.TemplateSectionsPermission = new List<TemplateSectionsPermission>();
                                    foreach (var permission in sections.TemplateSectionsPermission.ToList())
                                    {
                                        TemplateSectionsPermission newPermission = new TemplateSectionsPermission();
                                        newPermission.VisibleTo = permission.VisibleTo;
                                        newSection.TemplateSectionsPermission.Add(newPermission);
                                    }

                                    newSection.TemplateSubSections = new List<TemplateSubSections>();

                                    entityDB.SaveChanges();
                                    //Ends<<<

                                    if (sections.TemplateSectionType == 3 || sections.TemplateSectionType == 4 && sections.TemplateSectionType == 5 && sections.TemplateSectionType == 6 && sections.TemplateSectionType == 8)
                                    {
                                        var subSections = sections.TemplateSubSections.ToList();
                                        foreach (var subSection in subSections)
                                        {
                                            TemplateSubSections newSubSection = new TemplateSubSections();
                                            newSubSection.SubSectionType = subSection.SubSectionType;
                                            newSubSection.SubSectionName = subSection.SubSectionName;
                                            newSubSection.DisplayOrder = subSection.DisplayOrder;
                                            newSubSection.Visible = subSection.Visible;
                                            newSubSection.SubHeader = subSection.SubHeader;
                                            newSubSection.SectionNotes = subSection.SectionNotes;
                                            newSubSection.NoOfColumns = subSection.NoOfColumns;
                                            newSubSection.SubSectionTypeCondition = subSection.SubSectionTypeCondition;

                                            //Rajesh on 2/17/2014
                                            newSubSection.NotificationExist = subSection.NotificationExist;
                                            newSection.TemplateSubSections.Add(newSubSection);
                                            entityDB.SaveChanges();
                                            newSubSection.TemplateSubSectionsNotifications = new List<TemplateSubSectionsNotifications>();

                                            if (subSection.TemplateSubSectionsNotifications != null)
                                            {

                                                if (Form.TemplateType == "0")//If It is Custom Template Copy
                                                {
                                                    //If Selected Client Id Copy Template Client Id is Same Then only we can copy sub sec User permission
                                                    //if (Form.IsERI)
                                                    if (clientOrg.OrganizationType == OrganizationType.SuperClient)
                                                        if (newTemplate.ClientTemplates.FirstOrDefault().ClientID == selectedTemplate.ClientTemplates.FirstOrDefault().ClientID)
                                                        {
                                                            foreach (var notification in subSection.TemplateSubSectionsNotifications.ToList())
                                                            {
                                                                // Kiran on 9/23/2014
                                                                TemplateSubSectionsNotifications newNotification = new TemplateSubSectionsNotifications();
                                                                newNotification.RecipientUserId = notification.RecipientUserId;
                                                                //newNotification.ClientUserId = notification.ClientUserId;
                                                                newNotification.EmaiTemplateId = notification.EmaiTemplateId;
                                                                newNotification.SenderType = notification.SenderType;
                                                                newNotification.SenderEmailId = notification.SenderEmailId;
                                                                newSubSection.TemplateSubSectionsNotifications.Add(newNotification);
                                                            }
                                                        }
                                                }
                                            }


                                            newSubSection.TemplateSubSectionsPermissions = new List<TemplateSubSectionsPermissions>();
                                            if (subSection.TemplateSubSectionsPermissions != null)
                                            {
                                                foreach (var SubPermission in subSection.TemplateSubSectionsPermissions.ToList())
                                                {
                                                    var newSubPermission = new TemplateSubSectionsPermissions();
                                                    if (SubPermission.PermissionFor != OrganizationType.SuperClient)
                                                    {

                                                        newSubPermission.SectionsPermissionId = newSection.TemplateSectionsPermission.FirstOrDefault().SectionsPermissionId;
                                                        newSubPermission.PermissionFor = SubPermission.PermissionFor;
                                                        newSubPermission.PermissionType = SubPermission.PermissionType;
                                                        newSubSection.TemplateSubSectionsPermissions.Add(newSubPermission);
                                                    }
                                                    if (clientOrg.OrganizationType == OrganizationType.SuperClient && SubPermission.PermissionFor == OrganizationType.Client)
                                                    {
                                                        var newSubPermissions = new TemplateSubSectionsPermissions();
                                                        newSubPermissions.SectionsPermissionId = newSection.TemplateSectionsPermission.FirstOrDefault().SectionsPermissionId;
                                                        newSubPermissions.PermissionFor = OrganizationType.SuperClient;
                                                        newSubPermissions.PermissionType = SubPermission.PermissionType;
                                                        newSubSection.TemplateSubSectionsPermissions.Add(newSubPermissions);
                                                    }
                                                    newSubPermission.TemplateSubSectionsUserPermissions = new List<TemplateSubSectionsUserPermissions>();

                                                    if (SubPermission.TemplateSubSectionsUserPermissions != null)
                                                    {
                                                        if (Form.TemplateType == "0")//If It is Custom Template Copy
                                                        {
                                                            //If Selected Client Id Copy Template Client Id is Same Then only we can copy sub sec User permission
                                                            //if (Form.IsERI)
                                                            if (clientOrg.OrganizationType == OrganizationType.SuperClient)
                                                                if (newTemplate.ClientTemplates.FirstOrDefault().ClientID == selectedTemplate.ClientTemplates.FirstOrDefault().ClientID)
                                                                {
                                                                    foreach (var SubUserPermission in SubPermission.TemplateSubSectionsUserPermissions.ToList())
                                                                    {
                                                                        var newSubUserPermission = new TemplateSubSectionsUserPermissions();
                                                                        newSubUserPermission.UserId = SubUserPermission.UserId;
                                                                        newSubUserPermission.ReadOnlyAccess = SubUserPermission.ReadOnlyAccess;
                                                                        newSubUserPermission.EditAccess = SubUserPermission.EditAccess;
                                                                        newSubPermission.TemplateSubSectionsUserPermissions.Add(newSubUserPermission);
                                                                    }
                                                                }
                                                            //if(clientTemplate.ClientID==selectedTemplate.ClientTemplates.)
                                                        }
                                                    }
                                                    //Ends<<<
                                                }
                                            }
                                            //Ends<<



                                        }
                                        continue;
                                    }

                                    //Adding Headers
                                    newSection.TemplateSectionsHeaderFooter = new List<TemplateSectionsHeaderFooter>();
                                    foreach (var HorF in sections.TemplateSectionsHeaderFooter.ToList())
                                    {
                                        TemplateSectionsHeaderFooter newHorF = new TemplateSectionsHeaderFooter();
                                        newHorF.PositionType = HorF.PositionType;
                                        newHorF.PositionTitle = HorF.PositionTitle;
                                        newHorF.PostionContent = HorF.PostionContent;
                                        newHorF.DisplayImages = HorF.DisplayImages;
                                        newHorF.AttachmentExist = HorF.AttachmentExist;
                                        newSection.TemplateSectionsHeaderFooter.Add(newHorF);
                                    }


                                    if (Form.isSubSectionSelected)//Adding Subsections
                                    {

                                        var subSections = sections.TemplateSubSections.ToList();
                                        //if (Form.TemplateType == "0")
                                        //{
                                        //    var clientId_Loc = Convert.ToInt64(Form.ClientOfTemplate);
                                        //    subSections = sections.TemplateSubSections.Where(rec => rec.ClientId == clientId_Loc).ToList();
                                        //}
                                        foreach (var subSection in subSections)
                                        {
                                            TemplateSubSections newSubSection = new TemplateSubSections();
                                            newSubSection.SubSectionType = subSection.SubSectionType;
                                            newSubSection.SubSectionName = subSection.SubSectionName;
                                            newSubSection.DisplayOrder = subSection.DisplayOrder;
                                            newSubSection.Visible = subSection.Visible;
                                            newSubSection.SubHeader = subSection.SubHeader;
                                            newSubSection.SectionNotes = subSection.SectionNotes;
                                            newSubSection.NoOfColumns = subSection.NoOfColumns;
                                            newSubSection.SubSectionTypeCondition = subSection.SubSectionTypeCondition;
                                            newSection.TemplateSubSections.Add(newSubSection);
                                            newSubSection.NotificationExist = subSection.NotificationExist;
                                            entityDB.SaveChanges();
                                            //Rajesh on 2/17/2014



                                            newSubSection.TemplateSubSectionsNotifications = new List<TemplateSubSectionsNotifications>();

                                            if (subSection.TemplateSubSectionsNotifications != null)
                                            {

                                                if (Form.TemplateType == "0")//If It is Custom Template Copy
                                                {
                                                    //If Selected Client Id Copy Template Client Id is Same Then only we can copy sub sec User permission
                                                    //if (Form.IsERI)
                                                    if (clientOrg.OrganizationType == OrganizationType.SuperClient)
                                                        if (newTemplate.ClientTemplates.FirstOrDefault().ClientID == selectedTemplate.ClientTemplates.FirstOrDefault().ClientID)
                                                        {
                                                            foreach (var notification in subSection.TemplateSubSectionsNotifications.ToList())
                                                            {
                                                                // Kiran on 9/23/2014
                                                                TemplateSubSectionsNotifications newNotification = new TemplateSubSectionsNotifications();
                                                                newNotification.RecipientUserId = notification.RecipientUserId;
                                                                //newNotification.ClientUserId = notification.ClientUserId;
                                                                newNotification.EmaiTemplateId = notification.EmaiTemplateId;
                                                                newNotification.SenderType = notification.SenderType;
                                                                newNotification.SenderEmailId = notification.SenderEmailId;
                                                                newSubSection.TemplateSubSectionsNotifications.Add(newNotification);
                                                            }
                                                        }
                                                }
                                            }


                                            newSubSection.TemplateSubSectionsPermissions = new List<TemplateSubSectionsPermissions>();
                                            if (subSection.TemplateSubSectionsPermissions != null)
                                            {
                                                foreach (var SubPermission in subSection.TemplateSubSectionsPermissions.ToList())
                                                {
                                                    var newSubPermission = new TemplateSubSectionsPermissions();
                                                    if (SubPermission.PermissionFor != OrganizationType.SuperClient)
                                                    {

                                                        newSubPermission.SectionsPermissionId = newSection.TemplateSectionsPermission.FirstOrDefault().SectionsPermissionId;
                                                        newSubPermission.PermissionFor = SubPermission.PermissionFor;
                                                        newSubPermission.PermissionType = SubPermission.PermissionType;
                                                        newSubSection.TemplateSubSectionsPermissions.Add(newSubPermission);
                                                    }
                                                    if (clientOrg.OrganizationType == OrganizationType.SuperClient && SubPermission.PermissionFor == OrganizationType.Client)
                                                    {
                                                        var newSubPermissions = new TemplateSubSectionsPermissions();
                                                        newSubPermissions.SectionsPermissionId = newSection.TemplateSectionsPermission.FirstOrDefault().SectionsPermissionId;
                                                        newSubPermissions.PermissionFor = OrganizationType.SuperClient;
                                                        newSubPermissions.PermissionType = SubPermission.PermissionType;
                                                        newSubSection.TemplateSubSectionsPermissions.Add(newSubPermissions);
                                                    }
                                                    //Rajesh on 2/17/2014
                                                    newSubPermission.TemplateSubSectionsUserPermissions = new List<TemplateSubSectionsUserPermissions>();

                                                    if (SubPermission.TemplateSubSectionsUserPermissions != null)
                                                    {
                                                        if (Form.TemplateType == "0")//If It is Custom Template Copy
                                                        {
                                                            //If Selected Client Id Copy Template Client Id is Same Then only we can copy sub sec User permission
                                                            //todo:Permission Checkup required for ERI
                                                            //if (Form.IsERI && newTemplate.ClientTemplates.FirstOrDefault().ClientID == selectedTemplate.ClientTemplates.FirstOrDefault().ClientID)
                                                            if (clientOrg.OrganizationType == OrganizationType.SuperClient && newTemplate.ClientTemplates.FirstOrDefault().ClientID == selectedTemplate.ClientTemplates.FirstOrDefault().ClientID)
                                                            {
                                                                foreach (var subUserPermission in SubPermission.TemplateSubSectionsUserPermissions.ToList())
                                                                {
                                                                    var newSubUserPermission =
                                                                        new TemplateSubSectionsUserPermissions
                                                                        {
                                                                            UserId = subUserPermission.UserId,
                                                                            ReadOnlyAccess = subUserPermission
                                                                                .ReadOnlyAccess,
                                                                            EditAccess = subUserPermission.EditAccess
                                                                        };
                                                                    newSubPermission.TemplateSubSectionsUserPermissions.Add(newSubUserPermission);
                                                                }
                                                            }
                                                            //if(clientTemplate.ClientID==selectedTemplate.ClientTemplates.)
                                                        }
                                                    }
                                                    //Ends<<<
                                                }
                                            }
                                            //Ends<<




                                            if (Form.isQuestionsSelected && sections.TemplateSectionType != 4 && sections.TemplateSectionType != 5 && sections.TemplateSectionType != 6)//To prevent reporting data
                                            {
                                                newSubSection.Questions = new List<Questions>();
                                                foreach (var question in subSection.Questions)
                                                {
                                                    Questions newQuestion = new Questions();
                                                    newQuestion.QuestionText = question.QuestionText;
                                                    newQuestion.DisplayOrder = question.DisplayOrder;
                                                    newQuestion.Visible = question.Visible;
                                                    newQuestion.NumberOfColumns = question.NumberOfColumns;
                                                    newQuestion.IsMandatory = question.IsMandatory;
                                                    newQuestion.IsBold = question.IsBold;
                                                    newQuestion.ResponseRequired = question.ResponseRequired;
                                                    newQuestion.DisplayResponse = question.DisplayResponse;
                                                    newQuestion.HasDependantQuestions = question.HasDependantQuestions;
                                                    newQuestion.QuestionBankId = question.QuestionBankId;
                                                    newSubSection.Questions.Add(newQuestion);

                                                    //Rajesh on 12/24/2013
                                                    newQuestion.QuestionColumnDetails = new List<QuestionColumnDetails>();
                                                    newQuestion.QuestionsDependants = new List<QuestionsDependants>();

                                                    foreach (var Qdetails in question.QuestionColumnDetails.ToList())
                                                    {
                                                        QuestionColumnDetails newQDetails = new QuestionColumnDetails();
                                                        //newQDetails.QuestionId = newQuestion.QuestionID;
                                                        newQDetails.QuestionControlTypeId = Qdetails.QuestionControlTypeId;
                                                        newQDetails.ColumnNo = Qdetails.ColumnNo;
                                                        newQDetails.ColumnValues = Qdetails.ColumnValues;
                                                        newQDetails.DisplayType = Qdetails.DisplayType;
                                                        newQDetails.TableReference = Qdetails.TableReference;
                                                        newQDetails.TableReferenceField = Qdetails.TableReferenceField;
                                                        newQDetails.TableRefFieldValueDisplay = Qdetails.TableRefFieldValueDisplay;
                                                        newQDetails.DependantColumnNo = Qdetails.DependantColumnNo;
                                                        newQDetails.QFormula = "";
                                                        newQuestion.QuestionColumnDetails.Add(newQDetails);
                                                        //entityDB.QuestionColumnDetails.Add(newQDetails);

                                                    }
                                                    //foreach (var QDependent in question.QuestionsDependants.ToList())
                                                    //{
                                                    //    QuestionsDependants newQDependent = new QuestionsDependants();
                                                    //    //newQDependent.QuestionID = newQuestion.QuestionID;
                                                    //    newQDependent.DependantType = QDependent.DependantType;
                                                    //    newQDependent.DependantId = QDependent.DependantId;
                                                    //    newQDependent.DependantValue = QDependent.DependantValue;
                                                    //    newQDependent.DependantDisplayOrder = QDependent.DependantDisplayOrder;
                                                    //    //entityDB.QuestionsDependants.Add(newQDependent);
                                                    //    newQuestion.QuestionsDependants.Add(newQDependent);
                                                    //}
                                                    //Ends<<

                                                }
                                            }

                                        }
                                    }
                                    entityDB.Entry(newTemplate).State = EntityState.Modified;

                                    entityDB.SaveChanges();
                                }
                            }
                        }
                    }
                }

            }

            return Redirect("PopUpCloseView");
        }

        private void RecordTemplateChange(LocalTemplateModel Form, Templates newTemplate)
        {
            if (HasTemplateDataChange(Form, newTemplate))
            {
                var log = new TemplateLog();
                log.TemplateId = newTemplate.TemplateID;
                log.LockedByDefaultVal = Form.LockedByDefault;
                log.ChangedBy = new Guid(Session["UserId"].ToString());
                log.ChangedDate = DateTime.Now;
                entityDB.TemplateLog.Add(log);
            }
        }

        private static bool HasTemplateDataChange(LocalTemplateModel Form, Templates newTemplate)
        {
            return newTemplate.LockedByDefault != Form.LockedByDefault;
        }



        //public ActionResult AddOrEditCustomTemplate(LocalTemplateModel Form)
        //{

        //    ViewBag.Clients = (from clients in entityDB.Organizations.Where(rec => rec.OrganizationType == "Client").ToList()
        //                       select new SelectListItem()
        //                       {
        //                           Text = clients.Name,
        //                           Value = clients.OrganizationID + ""
        //                       }).ToList();
        //    ViewBag.Templates = (from temp in entityDB.Templates.Where(rec => rec.DefaultTemplate == false && rec.TemplateType == 1).ToList()
        //                         select new SelectListItem()
        //                         {
        //                             Text = temp.TemplateName,
        //                             Value = temp.TemplateID + ""
        //                         }).ToList();
        //    if (!ModelState.IsValid)
        //        return View(Form);
        //    Templates newTemplate = new Templates();
        //    if (Form.Templateid != null)//Update Template
        //    {
        //        newTemplate = entityDB.Templates.Find(Form.Templateid);
        //        newTemplate.TemplateName = Form.TemplateName;
        //        newTemplate.TemplateNotes = Form.TemplateNote;
        //        newTemplate.TemplateCode = Form.TemplateCode;
        //        entityDB.Entry(newTemplate).State = EntityState.Modified;
        //    }
        //    else//Save Template
        //    {
        //        newTemplate.TemplateCode = Form.TemplateCode;
        //        newTemplate.TemplateName = Form.TemplateName;
        //        newTemplate.TemplateType = 0;
        //        newTemplate.TemplateNotes = Form.TemplateNote;
        //        newTemplate.DefaultTemplate = false;
        //        newTemplate.TemplateStatus = 0;

        //        ClientTemplates clientTemplate = new ClientTemplates();
        //        clientTemplate.ClientID = Convert.ToInt64(Form.ClientId);
        //        clientTemplate.DisplayOrder = 1;
        //        clientTemplate.Visible = true;
        //        clientTemplate.DefaultTemplate = false;
        //        newTemplate.ClientTemplates = new List<ClientTemplates>();
        //        newTemplate.ClientTemplates.Add(clientTemplate);



        //        if (Form.isCopyTemplateSelected)//Copy The Template
        //        {
        //            Guid? selectedTemplateId = null;
        //            try { selectedTemplateId = new Guid(Form.CopyTemplateId); }
        //            catch { }
        //            if (selectedTemplateId != null)
        //            {
        //                Templates selectedTemplate = entityDB.Templates.Find(selectedTemplateId);
        //                if (selectedTemplate != null)
        //                {
        //                    //if (Form.isSectionSelected)//Adding sections
        //                    {
        //                        newTemplate.TemplateSections = new List<TemplateSections>();

        //                        foreach (var sections in selectedTemplate.TemplateSections)
        //                        {
        //                            TemplateSections newSection = new TemplateSections();
        //                            newSection.DisplayOrder = sections.DisplayOrder;
        //                            newSection.Visible = sections.Visible;
        //                            newSection.SectionName = sections.SectionName;
        //                            newSection.TemplateSectionType = sections.TemplateSectionType;
        //                            newSection.VisibleToClient = sections.VisibleToClient;
        //                            newTemplate.TemplateSections.Add(newSection);
        //                            //Rajesh on 2/15/2014
        //                            newSection.TemplateSectionsPermission = new List<TemplateSectionsPermission>();
        //                            foreach (var permission in sections.TemplateSectionsPermission.ToList())
        //                            {
        //                                TemplateSectionsPermission newPermission = new TemplateSectionsPermission();
        //                                newPermission.VisibleTo = permission.VisibleTo;
        //                                newSection.TemplateSectionsPermission.Add(newPermission);
        //                            }

        //                            newSection.TemplateSubSections = new List<TemplateSubSections>();

        //                            entityDB.SaveChanges();
        //                            //Ends<<<

        //                            if (sections.TemplateSectionType == 3 || sections.TemplateSectionType == 4 && sections.TemplateSectionType == 5 && sections.TemplateSectionType == 6)
        //                            {
        //                                var subSections = sections.TemplateSubSections.ToList();
        //                                foreach (var subSection in subSections)
        //                                {
        //                                    TemplateSubSections newSubSection = new TemplateSubSections();
        //                                    newSubSection.SubSectionType = subSection.SubSectionType;
        //                                    newSubSection.SubSectionName = subSection.SubSectionName;
        //                                    newSubSection.DisplayOrder = subSection.DisplayOrder;
        //                                    newSubSection.Visible = subSection.Visible;
        //                                    newSubSection.SubHeader = subSection.SubHeader;
        //                                    newSubSection.SectionNotes = subSection.SectionNotes;
        //                                    newSubSection.NoOfColumns = subSection.NoOfColumns;
        //                                    newSubSection.SubSectionTypeCondition = subSection.SubSectionTypeCondition;
        //                                    newSubSection.NotificationExist = subSection.NotificationExist;


        //                                    newSubSection.TemplateSubSectionsNotifications = new List<TemplateSubSectionsNotifications>();

        //                                    if (subSection.TemplateSubSectionsNotifications != null)
        //                                    {

        //                                        if (Form.TemplateType == "0")//If It is Custom Template Copy
        //                                        {
        //                                            //If Selected Client Id Copy Template Client Id is Same Then only we can copy sub sec User permission
        //                                            if (clientTemplate.ClientID == selectedTemplate.ClientTemplates.FirstOrDefault().ClientID)
        //                                            {
        //                                                foreach (var notification in subSection.TemplateSubSectionsNotifications.ToList())
        //                                                {
        //                                                    TemplateSubSectionsNotifications newNotification = new TemplateSubSectionsNotifications();
        //                                                    newNotification.AdminUserId = notification.AdminUserId;
        //                                                    newNotification.ClientUserId = notification.ClientUserId;
        //                                                    newNotification.EmaiTemplateId = notification.EmaiTemplateId;
        //                                                    newNotification.SenderType = notification.SenderType;
        //                                                    newNotification.SenderEmailId = notification.SenderEmailId;
        //                                                    newSubSection.TemplateSubSectionsNotifications.Add(newNotification);
        //                                                }
        //                                            }
        //                                        }
        //                                    }


        //                                    newSubSection.TemplateSubSectionsPermissions = new List<TemplateSubSectionsPermissions>();
        //                                    if (subSection.TemplateSubSectionsPermissions != null)
        //                                    {
        //                                        foreach (var SubPermission in subSection.TemplateSubSectionsPermissions.ToList())
        //                                        {
        //                                            var newSubPermission = new TemplateSubSectionsPermissions();
        //                                            newSubPermission.SectionsPermissionId = newSection.TemplateSectionsPermission.FirstOrDefault().SectionsPermissionId;
        //                                            newSubPermission.PermissionFor = SubPermission.PermissionFor;
        //                                            newSubPermission.PermissionType = SubPermission.PermissionType;


        //                                            //Rajesh on 2/17/2014

        //                                            newSubPermission.TemplateSubSectionsUserPermissions = new List<TemplateSubSectionsUserPermissions>();
        //                                            newSubSection.TemplateSubSectionsPermissions.Add(newSubPermission);
        //                                            if (SubPermission.TemplateSubSectionsUserPermissions != null)
        //                                            {
        //                                                if (Form.TemplateType == "0")//If It is Custom Template Copy
        //                                                {
        //                                                    //If Selected Client Id Copy Template Client Id is Same Then only we can copy sub sec User permission
        //                                                    if (clientTemplate.ClientID == selectedTemplate.ClientTemplates.FirstOrDefault().ClientID)
        //                                                    {
        //                                                        foreach (var SubUserPermission in SubPermission.TemplateSubSectionsUserPermissions.ToList())
        //                                                        {
        //                                                            var newSubUserPermission = new TemplateSubSectionsUserPermissions();
        //                                                            newSubUserPermission.UserId = SubUserPermission.UserId;
        //                                                            newSubUserPermission.ReadOnlyAccess = SubUserPermission.ReadOnlyAccess;
        //                                                            newSubUserPermission.EditAccess = SubUserPermission.EditAccess;
        //                                                            newSubPermission.TemplateSubSectionsUserPermissions.Add(newSubUserPermission);
        //                                                        }
        //                                                    }
        //                                                    //if(clientTemplate.ClientID==selectedTemplate.ClientTemplates.)
        //                                                }
        //                                            }
        //                                            //Ends<<<
        //                                        }
        //                                    }
        //                                    //Ends<<

        //                                    newSection.TemplateSubSections.Add(newSubSection);
        //                                }
        //                                continue;
        //                            }

        //                            //Adding Headers
        //                            newSection.TemplateSectionsHeaderFooter = new List<TemplateSectionsHeaderFooter>();
        //                            foreach (var HorF in sections.TemplateSectionsHeaderFooter.ToList())
        //                            {
        //                                TemplateSectionsHeaderFooter newHorF = new TemplateSectionsHeaderFooter();
        //                                newHorF.PositionType = HorF.PositionType;
        //                                newHorF.PositionTitle = HorF.PositionTitle;
        //                                newHorF.PostionContent = HorF.PostionContent;
        //                                newHorF.DisplayImages = HorF.DisplayImages;
        //                                newHorF.AttachmentExist = HorF.AttachmentExist;
        //                                newSection.TemplateSectionsHeaderFooter.Add(newHorF);
        //                            }


        //                            if (Form.isSubSectionSelected)//Adding Subsections
        //                            {

        //                                var subSections = sections.TemplateSubSections.ToList();
        //                                //if (Form.TemplateType == "0")
        //                                //{
        //                                //    var clientId_Loc = Convert.ToInt64(Form.ClientOfTemplate);
        //                                //    subSections = sections.TemplateSubSections.Where(rec => rec.ClientId == clientId_Loc).ToList();
        //                                //}
        //                                foreach (var subSection in subSections)
        //                                {
        //                                    TemplateSubSections newSubSection = new TemplateSubSections();
        //                                    newSubSection.SubSectionType = subSection.SubSectionType;
        //                                    newSubSection.SubSectionName = subSection.SubSectionName;
        //                                    newSubSection.DisplayOrder = subSection.DisplayOrder;
        //                                    newSubSection.Visible = subSection.Visible;
        //                                    newSubSection.SubHeader = subSection.SubHeader;
        //                                    newSubSection.SectionNotes = subSection.SectionNotes;
        //                                    newSubSection.NoOfColumns = subSection.NoOfColumns;
        //                                    newSubSection.SubSectionTypeCondition = subSection.SubSectionTypeCondition;
        //                                    newSubSection.NotificationExist = subSection.NotificationExist;


        //                                    newSubSection.TemplateSubSectionsNotifications = new List<TemplateSubSectionsNotifications>();

        //                                    if (subSection.TemplateSubSectionsNotifications != null)
        //                                    {

        //                                        if (Form.TemplateType == "0")//If It is Custom Template Copy
        //                                        {
        //                                            //If Selected Client Id Copy Template Client Id is Same Then only we can copy sub sec User permission
        //                                            if (clientTemplate.ClientID == selectedTemplate.ClientTemplates.FirstOrDefault().ClientID)
        //                                            {
        //                                                foreach (var notification in subSection.TemplateSubSectionsNotifications.ToList())
        //                                                {
        //                                                    TemplateSubSectionsNotifications newNotification = new TemplateSubSectionsNotifications();
        //                                                    newNotification.AdminUserId = notification.AdminUserId;
        //                                                    newNotification.ClientUserId = notification.ClientUserId;
        //                                                    newNotification.EmaiTemplateId = notification.EmaiTemplateId;
        //                                                    newNotification.SenderType = notification.SenderType;
        //                                                    newNotification.SenderEmailId = notification.SenderEmailId;
        //                                                    newSubSection.TemplateSubSectionsNotifications.Add(newNotification);
        //                                                }
        //                                            }
        //                                        }
        //                                    }


        //                                    newSubSection.TemplateSubSectionsPermissions = new List<TemplateSubSectionsPermissions>();
        //                                    if (subSection.TemplateSubSectionsPermissions != null)
        //                                    {
        //                                        foreach (var SubPermission in subSection.TemplateSubSectionsPermissions.ToList())
        //                                        {
        //                                            var newSubPermission = new TemplateSubSectionsPermissions();
        //                                            newSubPermission.SectionsPermissionId = newSection.TemplateSectionsPermission.FirstOrDefault().SectionsPermissionId;
        //                                            newSubPermission.PermissionFor = SubPermission.PermissionFor;
        //                                            newSubPermission.PermissionType = SubPermission.PermissionType;


        //                                            //Rajesh on 2/17/2014
        //                                            newSubPermission.TemplateSubSectionsUserPermissions = new List<TemplateSubSectionsUserPermissions>();
        //                                            newSubSection.TemplateSubSectionsPermissions.Add(newSubPermission);
        //                                            if (SubPermission.TemplateSubSectionsUserPermissions != null)
        //                                            {
        //                                                if (Form.TemplateType == "0")//If It is Custom Template Copy
        //                                                {
        //                                                    //If Selected Client Id Copy Template Client Id is Same Then only we can copy sub sec User permission
        //                                                    if (clientTemplate.ClientID == selectedTemplate.ClientTemplates.FirstOrDefault().ClientID)
        //                                                    {
        //                                                        foreach (var SubUserPermission in SubPermission.TemplateSubSectionsUserPermissions.ToList())
        //                                                        {
        //                                                            var newSubUserPermission = new TemplateSubSectionsUserPermissions();
        //                                                            newSubUserPermission.UserId = SubUserPermission.UserId;
        //                                                            newSubUserPermission.ReadOnlyAccess = SubUserPermission.ReadOnlyAccess;
        //                                                            newSubUserPermission.EditAccess = SubUserPermission.EditAccess;
        //                                                            newSubPermission.TemplateSubSectionsUserPermissions.Add(newSubUserPermission);
        //                                                        }
        //                                                    }
        //                                                    //if(clientTemplate.ClientID==selectedTemplate.ClientTemplates.)
        //                                                }
        //                                            }
        //                                            //Ends<<<
        //                                        }
        //                                    }

        //                                    newSection.TemplateSubSections.Add(newSubSection);

        //                                    if (Form.isQuestionsSelected && sections.TemplateSectionType != 4 && sections.TemplateSectionType != 5 && sections.TemplateSectionType != 6)//To prevent reporting data
        //                                    {
        //                                        newSubSection.Questions = new List<Questions>();
        //                                        foreach (var question in subSection.Questions)
        //                                        {
        //                                            Questions newQuestion = new Questions();
        //                                            newQuestion.QuestionText = question.QuestionText;
        //                                            newQuestion.DisplayOrder = question.DisplayOrder;
        //                                            newQuestion.Visible = question.Visible;
        //                                            newQuestion.NumberOfColumns = question.NumberOfColumns;
        //                                            newQuestion.IsMandatory = question.IsMandatory;
        //                                            newQuestion.IsBold = question.IsBold;
        //                                            newQuestion.ResponseRequired = question.ResponseRequired;
        //                                            newQuestion.DisplayResponse = question.DisplayResponse;
        //                                            newQuestion.HasDependantQuestions = question.HasDependantQuestions;
        //                                            newQuestion.QuestionBankId = question.QuestionBankId;
        //                                            newSubSection.Questions.Add(newQuestion);

        //                                            //Rajesh on 12/24/2013
        //                                            newQuestion.QuestionColumnDetails = new List<QuestionColumnDetails>();
        //                                            newQuestion.QuestionsDependants = new List<QuestionsDependants>();

        //                                            foreach (var Qdetails in question.QuestionColumnDetails.ToList())
        //                                            {
        //                                                QuestionColumnDetails newQDetails = new QuestionColumnDetails();
        //                                                //newQDetails.QuestionId = newQuestion.QuestionID;
        //                                                newQDetails.QuestionControlTypeId = Qdetails.QuestionControlTypeId;
        //                                                newQDetails.ColumnNo = Qdetails.ColumnNo;
        //                                                newQDetails.ColumnValues = Qdetails.ColumnValues;
        //                                                newQDetails.DisplayType = Qdetails.DisplayType;
        //                                                newQDetails.TableReference = Qdetails.TableReference;
        //                                                newQDetails.TableReferenceField = Qdetails.TableReferenceField;
        //                                                newQDetails.TableRefFieldValueDisplay = Qdetails.TableRefFieldValueDisplay;
        //                                                newQDetails.DependantColumnNo = Qdetails.DependantColumnNo;
        //                                                newQDetails.QFormula = Qdetails.QFormula;
        //                                                newQuestion.QuestionColumnDetails.Add(newQDetails);
        //                                                //entityDB.QuestionColumnDetails.Add(newQDetails);

        //                                            }
        //                                            foreach (var QDependent in question.QuestionsDependants.ToList())
        //                                            {
        //                                                QuestionsDependants newQDependent = new QuestionsDependants();
        //                                                //newQDependent.QuestionID = newQuestion.QuestionID;
        //                                                newQDependent.DependantType = QDependent.DependantType;
        //                                                newQDependent.DependantId = QDependent.DependantId;
        //                                                newQDependent.DependantValue = QDependent.DependantValue;
        //                                                newQDependent.DependantDisplayOrder = QDependent.DependantDisplayOrder;
        //                                                //entityDB.QuestionsDependants.Add(newQDependent);
        //                                                newQuestion.QuestionsDependants.Add(newQDependent);
        //                                            }
        //                                            //Ends<<

        //                                        }
        //                                    }

        //                                }
        //                            }
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //        entityDB.Templates.Add(newTemplate);
        //    }
        //    entityDB.SaveChanges();
        //    return Redirect("PopUpCloseView");
        //}


        [SessionExpire]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "QuestionnaireTool", sideMenuName = "TemplateTool", ViewName = "TemplatesAndSections")]
        public ActionResult TemplatesAndSections()
        {
            var templates = entityDB.Templates.Where(tempRec => tempRec.TemplateType == 0 && tempRec.DefaultTemplate == true).ToList();//Rajesh on 10/28/2013
            return View(templates);
        }

        [HttpGet]
        [SessionExpire]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "QuestionnaireTool", sideMenuName = "TemplateTool", ViewName = "AddOrEditTemplate")]
        public ActionResult AddOrEditTemplate(Guid? TemplateId)
        {
            LocalTemplateModel template = new LocalTemplateModel();
            if (TemplateId != null)
            {
                Templates mainTemp = entityDB.Templates.Find(TemplateId);
                template.TemplateName = mainTemp.TemplateName;
                template.Templateid = mainTemp.TemplateID;
                template.TemplateNote = mainTemp.TemplateNotes;
                template.TemplateCode = mainTemp.TemplateCode;
            }

            ViewBag.Clients = (from clients in entityDB.Organizations.Where(rec => rec.OrganizationType == "Client").ToList()
                               select new SelectListItem()
                               {
                                   Text = clients.Name,
                                   Value = clients.OrganizationID + ""
                               }).ToList();
            ViewBag.Templates = (from temp in entityDB.Templates.Where(rec => rec.DefaultTemplate == true).ToList()
                                 select new SelectListItem()
                                 {
                                     Text = temp.TemplateName,
                                     Value = temp.TemplateID + ""
                                 }).ToList();
            return View(template);
        }
        [HttpPost]
        [SessionExpire]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "QuestionnaireTool", sideMenuName = "TemplateTool", ViewName = "AddOrEditTemplate")]
        public ActionResult AddOrEditTemplate(LocalTemplateModel Form)
        {

            ViewBag.Clients = (from clients in entityDB.Organizations.Where(rec => rec.OrganizationType == "Client").ToList()
                               select new SelectListItem()
                               {
                                   Text = clients.Name,
                                   Value = clients.OrganizationID + ""
                               }).ToList();
            ViewBag.Templates = (from temp in entityDB.Templates.Where(rec => rec.DefaultTemplate == true).ToList()
                                 select new SelectListItem()
                                 {
                                     Text = temp.TemplateName,
                                     Value = temp.TemplateID + ""
                                 }).ToList();
            if (!ModelState.IsValid)
                return View(Form);
            Templates newTemplate = new Templates();
            if (Form.Templateid != null)//Update Template
            {
                newTemplate = entityDB.Templates.Find(Form.Templateid);
                newTemplate.TemplateName = Form.TemplateName;
                newTemplate.TemplateNotes = Form.TemplateNote;
                newTemplate.TemplateCode = Form.TemplateCode;
                entityDB.Entry(newTemplate).State = EntityState.Modified;
            }
            else//Save Template
            {
                newTemplate.TemplateCode = Form.TemplateCode;
                newTemplate.TemplateName = Form.TemplateName;
                newTemplate.TemplateType = 0;
                newTemplate.TemplateNotes = Form.TemplateNote;
                newTemplate.DefaultTemplate = true;
                newTemplate.TemplateStatus = 0;
                newTemplate.LanguageId = 0;
                newTemplate.LanguageId = Languages.English;
                //if (Form.TemplateType.Equals("1"))//Means he selected Custom Template
                //{
                //    ClientTemplates clientTemplate=new ClientTemplates();
                //    clientTemplate.ClientID = Convert.ToInt64(Form.ClientId);
                //    clientTemplate.DisplayOrder = 1;
                //    clientTemplate.Visible = true;
                //    clientTemplate.DefaultTemplate = true;
                //    newTemplate.ClientTemplates = new List<ClientTemplates>();
                //    newTemplate.ClientTemplates.Add(clientTemplate);
                //}


                //if (Form.isCopyTemplateSelected)//Copy The Template
                //{
                //    Guid? selectedTemplateId=null;
                //    try { selectedTemplateId = new Guid(Form.CopyTemplateId); }
                //    catch { }
                //    if (selectedTemplateId != null)
                //    {
                //        Templates selectedTemplate = entityDB.Templates.Find(selectedTemplateId);
                //        if (selectedTemplate != null)
                //        {
                //            if (Form.isSectionSelected)//Adding sections
                //            {
                //                newTemplate.TemplateSections = new List<TemplateSections>();

                //                foreach (var sections in selectedTemplate.TemplateSections)
                //                {
                //                    TemplateSections newSection = new TemplateSections();
                //                    newSection.DisplayOrder = sections.DisplayOrder;
                //                    newSection.Visible = sections.Visible;
                //                    newSection.SectionName = sections.SectionName;
                //                    newSection.TemplateSectionType = sections.TemplateSectionType;
                //                    newSection.VisibleToClient = sections.VisibleToClient;
                //                    newTemplate.TemplateSections.Add(newSection);


                //                    if (Form.isSubSectionSelected)//Adding Subsections
                //                    {
                //                        newSection.TemplateSubSections = new List<TemplateSubSections>();
                //                        foreach (var subSection in sections.TemplateSubSections)
                //                        {
                //                            TemplateSubSections newSubSection = new TemplateSubSections();
                //                            newSubSection.SubSectionType = subSection.SubSectionType;
                //                            newSubSection.SubSectionName = subSection.SubSectionName;
                //                            newSubSection.DisplayOrder = subSection.DisplayOrder;
                //                            newSubSection.Visible = subSection.Visible;
                //                            newSubSection.SubHeader = subSection.SubHeader;
                //                            newSubSection.SectionNotes = subSection.SectionNotes;
                //                            newSubSection.NoOfColumns = subSection.NoOfColumns;
                //                            newSubSection.SubSectionTypeCondition = subSection.SubSectionTypeCondition;

                //                            newSection.TemplateSubSections.Add(newSubSection);

                //                            if (Form.isQuestionsSelected)//Adding Questions
                //                            {
                //                                newSubSection.Questions = new List<Questions>();
                //                                foreach (var question in subSection.Questions)
                //                                {
                //                                    Questions newQuestion = new Questions();
                //                                    newQuestion.QuestionText = question.QuestionText;
                //                                    newQuestion.DisplayOrder = question.DisplayOrder;
                //                                    newQuestion.Visible = question.Visible;
                //                                    newQuestion.NumberOfColumns = question.NumberOfColumns;
                //                                    newQuestion.IsMandatory = question.IsMandatory;
                //                                    newQuestion.IsBold = question.IsBold;
                //                                    newQuestion.ResponseRequired = question.ResponseRequired;
                //                                    newQuestion.DisplayResponse = question.DisplayResponse;
                //                                    newQuestion.HasDependantQuestions = question.HasDependantQuestions;
                //                                    newSubSection.Questions.Add(newQuestion);
                //                                }
                //                            }

                //                        }
                //                    }
                //                }
                //            }
                //        }
                //    }
                //}
                entityDB.Templates.Add(newTemplate);
            }
            entityDB.SaveChanges();
            return Redirect("PopUpCloseView");
        }

        [HttpGet]
        public string DeleteTemplate(Guid TemplateId)
        {
            var template = entityDB.Templates.Find(TemplateId);

            if (template.ClientTemplates != null)
            {
                foreach (var Ctemplate in template.ClientTemplates.ToList())
                {
                    foreach (var CTData in Ctemplate.ClientTemplateReportingData.ToList())
                        entityDB.Entry(CTData).State = EntityState.Deleted;
                    foreach (var CTForBU in Ctemplate.ClientTemplatesForBU.ToList())
                    {
                        entityDB.Entry(CTForBU).State = EntityState.Deleted;
                    }
                    entityDB.Entry(Ctemplate).State = EntityState.Deleted;
                }
            }
            if (template.TemplateSections != null)
                foreach (var Tsection in template.TemplateSections.ToList())
                {
                    if (Tsection.TemplateSectionsHeaderFooter != null)
                        foreach (var HandF in Tsection.TemplateSectionsHeaderFooter.ToList())
                        {
                            entityDB.Entry(HandF).State = EntityState.Deleted;
                        }
                    if (Tsection.TemplateSubSections != null)
                        foreach (var Ssection in Tsection.TemplateSubSections.ToList())
                        {

                            if (Ssection.Questions != null)
                                foreach (var question in Ssection.Questions.ToList())//Delete Questions
                                {
                                    if (question.QuestionColumnDetails != null)
                                        foreach (var Qcolumn in question.QuestionColumnDetails.ToList())
                                        {
                                            if (Qcolumn.ClientUserSignature.Any())
                                                foreach (var clientUserSign in Qcolumn.ClientUserSignature.ToList())
                                                {
                                                    entityDB.Entry(clientUserSign).State = EntityState.Deleted;
                                                }
                                            entityDB.Entry(Qcolumn).State = EntityState.Deleted;
                                        }
                                    if (question.QuestionsDependants != null)
                                    {
                                        foreach (var Qdependent in question.QuestionsDependants.ToList())
                                            entityDB.Entry(Qdependent).State = EntityState.Deleted;
                                    }
                                    entityDB.Entry(question).State = EntityState.Deleted;
                                }

                            if (Ssection.AgreementSubSectionConfiguration.Any())
                            {
                                foreach (var agreementSubsection in Ssection.AgreementSubSectionConfiguration.ToList())
                                {
                                    entityDB.Entry(agreementSubsection).State = EntityState.Deleted;
                                }
                            }

                            //Rajesh on 2/11/2014
                            if (Ssection.TemplateSubSectionsPermissions != null)
                            {
                                foreach (var SubSecPermission in Ssection.TemplateSubSectionsPermissions.ToList())
                                {
                                    if (SubSecPermission.TemplateSubSectionsUserPermissions != null)
                                    {
                                        foreach (var SubSecUserPermission in SubSecPermission.TemplateSubSectionsUserPermissions.ToList())
                                        {
                                            entityDB.Entry(SubSecUserPermission).State = EntityState.Deleted;
                                        }
                                    }
                                    entityDB.Entry(SubSecPermission).State = EntityState.Deleted;
                                }
                                if (Ssection.TemplateSubSectionsNotifications != null)
                                    foreach (var notification in Ssection.TemplateSubSectionsNotifications.ToList())
                                    {
                                        entityDB.Entry(notification).State = EntityState.Deleted;
                                    }
                            }
                            //Ends<<
                            entityDB.Entry(Ssection).State = EntityState.Deleted;//Rajesh on 2/20/2014
                        }
                    //Rajesh on 2/11/2014
                    if (Tsection.TemplateSectionsPermission != null)
                    {
                        foreach (var permission in Tsection.TemplateSectionsPermission.ToList())
                        {
                            entityDB.Entry(permission).State = EntityState.Deleted;
                        }
                    }
                    // Kiran on 03/02/2015
                    if (Tsection.ClientTemplateProfiles != null)
                    {
                        foreach (var clientTemplateProfile in Tsection.ClientTemplateProfiles.ToList())
                        {
                            entityDB.Entry(clientTemplateProfile).State = EntityState.Deleted;
                        }
                    }
                    //Ends<<<
                    //Ends<<<
                    entityDB.Entry(Tsection).State = EntityState.Deleted;
                }
            entityDB.Entry(template).State = EntityState.Deleted;
            entityDB.SaveChanges();
            return "OK";
        }

        public ActionResult PopUpCloseView()
        {
            return View();
        }
        public string ChangeTemplateSectionsOrder(string templateSectionIds, Guid TemplateId)
        {
            string result = "Ok" + templateSectionIds + "\n";
            int orderNumber = 0;
            var template = entityDB.Templates.Find(TemplateId);
            if (template != null)
                foreach (var ids in templateSectionIds.Split('|'))
                {
                    try
                    {
                        long secId = Convert.ToInt64(ids);
                        result += secId;
                        TemplateSections section = template.TemplateSections.ToList().FirstOrDefault(rec => rec.TemplateSectionID == secId);
                        if (section == null)
                            continue;
                        orderNumber++;
                        section.DisplayOrder = orderNumber;
                        entityDB.Entry(section).State = EntityState.Modified;
                    }
                    catch (Exception ee) { result += ee.Message + ""; }
                }
            entityDB.SaveChanges();
            return result;
        }
        public string ChangeTemplateSubSectionsOrder(string templateSubSectionIds, long TemplateSecId)
        {
            string result = "Ok" + templateSubSectionIds + "\n";
            int orderNumber = 0;
            var templateSec = entityDB.TemplateSections.Find(TemplateSecId);
            if (templateSec != null)
                foreach (var ids in templateSubSectionIds.Split('|'))
                {
                    try
                    {
                        long secId = Convert.ToInt64(ids);
                        result += secId;
                        TemplateSubSections subSection = templateSec.TemplateSubSections.ToList().FirstOrDefault(rec => rec.SubSectionID == secId);
                        if (subSection == null)
                            continue;
                        orderNumber++;
                        subSection.DisplayOrder = orderNumber;
                        entityDB.Entry(subSection).State = EntityState.Modified;
                    }
                    catch (Exception ee) { result += ee.Message + ""; }
                }
            entityDB.SaveChanges();
            return result;
        }

        [SessionExpireForView]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "QuestionnaireTool", sideMenuName = "TemplateTool", ViewName = "AddOrEditSection")]
        public ActionResult AddOrEditSection(Guid TemplateId, long TemplateSectionID)
        {
            LocalSectionsModel section = new LocalSectionsModel();
            section.Templateid = TemplateId;
            section.TemplateSectionID = TemplateSectionID;

            section.Permission = 3;//All Permissions   Rajesh on 2/11/2014

            ViewBag.Template = entityDB.Templates.Find(TemplateId);//Rajesh on 10/22/2013
            //Rajesh on 1/20/2014
            if (!ViewBag.Template.DefaultTemplate)
            {
                var clientOrgId = entityDB.ClientTemplates.FirstOrDefault(rec => rec.TemplateID == TemplateId).ClientID;
                section.Clients = (from clients in entityDB.SystemUsersOrganizations.Where(rec => rec.OrganizationId == clientOrgId).ToList()
                                   select new SelectListItem
                                   {
                                       Value = clients.SystemUsers.UserId + "",
                                       Text = clients.SystemUsers.Contacts.FirstOrDefault() == null ? clients.SystemUsers.Email : clients.SystemUsers.Contacts.FirstOrDefault().LastName + ", " + clients.SystemUsers.Contacts.FirstOrDefault().FirstName,
                                       Selected = false
                                   }).ToList();
            }
            //Ends<<<
            if (TemplateSectionID != -1)
            {
                var Tsection = entityDB.TemplateSections.Find(TemplateSectionID);
                section.VisibleToClient = Tsection.VisibleToClient == true;
                section.SectionName = Tsection.SectionName;
                section.SectionType = Tsection.TemplateSectionType == null ? 0 : (int)Tsection.TemplateSectionType;
                section.Visible = !(Tsection.Visible == true);
                section.HideFromPrequalification = Tsection.HideFromPrequalification ?? false;
                //Rajesh on 2/11/2014
                section.Permission = 3;
                try
                {
                    section.Permission = Tsection.TemplateSectionsPermission.FirstOrDefault().VisibleTo;
                }
                catch { }
                //Ends<<<
                //foreach (var permissions in Tsection.TemplateSectionsPermission.ToList())
                //{
                //    section.Clients.FirstOrDefault(rec=>rec.Value==permissions.ClientUserId+"").Selected = true;
                //}
            }
            return View(section);
        }
        [HttpPost]
        public ActionResult GetClientSignData(long subSectionId)
        {
            return Json(_templateTool.GetClientSignature(subSectionId));
        }

        public ActionResult UpdateClientSign(ClientSignature form)
        {
            return Json(_templateTool.UpdateSign(form));
        }
        [HttpPost]
        [SessionExpireForView]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "QuestionnaireTool", sideMenuName = "TemplateTool", ViewName = "AddOrEditSection")]
        public ActionResult AddOrEditSection(LocalSectionsModel form)
        {
            ViewBag.Template = entityDB.Templates.Find(form.Templateid);//Rajesh on 10/22/2013
            if (form.SectionType != 33 || form.TemplateSectionID != -1)
            {
                ModelState["Signed"].Errors.Clear();
                ModelState["Title"].Errors.Clear();
            }
            if (!ModelState.IsValid)
                return View(form);
            if (form.Permission == 1)
            {
                form.VisibleToClient = false;
            }
            TemplateSections section = new TemplateSections();
            if (form.TemplateSectionID == -1)//Insert
            {
                section.Visible = true;
                section.HideFromPrequalification = form.HideFromPrequalification;
                entityDB.TemplateSections.Add(section);

                if (form.SectionType != 4 && form.SectionType != 5)//Means others-->ProficiencyCapability,Bidding Interests
                    section.TemplateSectionType = form.SectionType;
                else
                    section.TemplateSectionType = Convert.ToInt32(form.OtherSectionTypes);
                try
                {
                    section.DisplayOrder = entityDB.Templates.Find(form.Templateid).TemplateSections.Count() + 1;
                }
                catch
                {
                    section.DisplayOrder = 1;
                }
                //Rajesh on 2/11/2014
                TemplateSectionsPermission permission = new TemplateSectionsPermission();
                permission.VisibleTo = form.Permission;
                section.TemplateSectionsPermission = new List<TemplateSectionsPermission>();
                section.TemplateSectionsPermission.Add(permission);
                //Ends<<
            }
            //Rajesh on 1/21/2014
            else//Update
            {

                section = entityDB.TemplateSections.Find(form.TemplateSectionID);
                var isPermissionSame = section.TemplateSectionsPermission.FirstOrDefault().VisibleTo == form.Permission;

                if (section.TemplateSectionsPermission == null || section.TemplateSectionsPermission.Count == 0)
                {
                    //Rajesh on 2/11/2014
                    TemplateSectionsPermission permission = new TemplateSectionsPermission();
                    permission.VisibleTo = form.Permission;
                    section.TemplateSectionsPermission = new List<TemplateSectionsPermission>();
                    section.TemplateSectionsPermission.Add(permission);
                    //Ends<<
                }
                else
                {
                    section.TemplateSectionsPermission.FirstOrDefault().VisibleTo = form.Permission;
                }
                //Rajesh on 2/11/2014


                if (!isPermissionSame && section.TemplateSectionType != 33)//If it is not a MPA Section
                {
                    if (section.TemplateSubSections != null)
                        foreach (var subSec in section.TemplateSubSections.ToList())
                        {
                            //Rajesh on 3/29/2014  //If Main section change this subsection permissions cannot be changed
                            if (subSec.SubSectionTypeCondition == "CSIWebServiceResponse")
                                continue;
                            //Ends<<<
                            if (subSec.TemplateSubSectionsPermissions != null)
                                foreach (var subSecPermission in subSec.TemplateSubSectionsPermissions.ToList())
                                {
                                    if (subSecPermission.TemplateSubSectionsUserPermissions != null)
                                        foreach (var subsecUserPermissions in subSecPermission.TemplateSubSectionsUserPermissions.ToList())
                                        {
                                            entityDB.Entry(subsecUserPermissions).State = EntityState.Deleted;
                                        }
                                    entityDB.Entry(subSecPermission).State = EntityState.Deleted;
                                }
                        }
                    foreach (var subSection in section.TemplateSubSections.ToList())
                    {
                        CreateTemplateSubSecPermission(0, subSection, section);
                    }
                }

                //if (section.TemplateSectionsPermission == null || section.TemplateSectionsPermission.Count == 0)
                //{
                //    //Rajesh on 2/15/2014
                //    //To Remove SubSec permission and subsec Users
                //    if (section.TemplateSectionType != 33)//If it is not a MPA Section
                //    {
                //        if (section.TemplateSubSections != null)
                //            foreach (var subSec in section.TemplateSubSections.ToList())
                //            {
                //                //Rajesh on 3/29/2014  //If Main section change this subsection permissions cannot be changed
                //                if (subSec.SubSectionTypeCondition == "CSIWebServiceResponse")
                //                    continue;
                //                //Ends<<<
                //                if (subSec.TemplateSubSectionsPermissions != null)
                //                    foreach (var subSecPermission in subSec.TemplateSubSectionsPermissions.ToList())
                //                    {
                //                        if (subSecPermission.TemplateSubSectionsUserPermissions != null)
                //                            foreach (var subsecUserPermissions in subSecPermission.TemplateSubSectionsUserPermissions.ToList())
                //                            {
                //                                entityDB.Entry(subsecUserPermissions).State = EntityState.Deleted;
                //                            }
                //                        entityDB.Entry(subSecPermission).State = EntityState.Deleted;
                //                    }
                //            }
                //        foreach (var subSection in section.TemplateSubSections.ToList())
                //        {
                //            CreateTemplateSubSecPermission(0, subSection, section);
                //        }
                //    }
                //    //Ends<<<
                //}
                //else
                //{
                //    //Rajesh on 2/15/2014
                //    //To Remove SubSec permission and subsec Users
                //    if (section.TemplateSectionType != 33)//If it is not a MPA Section
                //    {
                //        if (section.TemplateSubSections != null)
                //            foreach (var subSec in section.TemplateSubSections.ToList())
                //            {
                //                if (subSec.TemplateSubSectionsPermissions != null)
                //                    foreach (var subSecPermission in subSec.TemplateSubSectionsPermissions.ToList())
                //                    {
                //                        if (subSecPermission.TemplateSubSectionsUserPermissions != null)
                //                            foreach (var subsecUserPermissions in subSecPermission.TemplateSubSectionsUserPermissions.ToList())
                //                            {
                //                                entityDB.Entry(subsecUserPermissions).State = EntityState.Deleted;
                //                            }
                //                        entityDB.Entry(subSecPermission).State = EntityState.Deleted;
                //                    }
                //            }
                //        foreach (var subSection in section.TemplateSubSections.ToList())
                //        {
                //            CreateTemplateSubSecPermission(0, subSection, section);
                //        }
                //    }
                //Ends<<<

                //section.TemplateSectionsPermission.FirstOrDefault().VisibleTo = Form.Permission;
                //}
                //Ends<<<
                entityDB.Entry(section).State = EntityState.Modified;
                section.Visible = !form.Visible;
                section.HideFromPrequalification = form.HideFromPrequalification;
                //if (section.TemplateSectionType == 35)
                //{
                //    //this is insertion so no need to delete permissions for this template....
                //    if (section.TemplateSectionsPermission == null)
                //        section.TemplateSectionsPermission = new List<TemplateSectionsPermission>();
                //    else
                //    {
                //        foreach (var sectionpermission in section.TemplateSectionsPermission.ToList())
                //        {
                //            entityDB.TemplateSectionsPermission.Remove(sectionpermission);
                //        }
                //    }
                //    foreach (var selectedClient in Form.Clients.Where(rec => rec.Selected))
                //    {
                //        TemplateSectionsPermission permissions = new TemplateSectionsPermission();
                //        //permissions.ClientUserId = new Guid(selectedClient.Value);
                //        section.TemplateSectionsPermission.Add(permissions);
                //    }

                //}


            }
            //Ends<<<
            ViewBag.Template = entityDB.Templates.Find(form.Templateid);//Rajesh on 10/22/2013
            long languageId = (long)entityDB.Templates.Find(form.Templateid).LanguageId;
            var languageCode = _org.GetLanguageCode(languageId);
            section.SectionName = form.SectionName;
            section.TemplateID = form.Templateid;

            section.VisibleToClient = form.VisibleToClient;

            if (form.TemplateSectionID == -1)//Insert
            {
                if (section.TemplateSubSections == null)
                    section.TemplateSubSections = new List<TemplateSubSections>();
                if (section.TemplateSectionType == 4)//Supporting documents 
                {
                    TemplateSubSections subSec = new TemplateSubSections();
                    subSec.DisplayOrder = 1;
                    subSec.NoOfColumns = 0;
                    subSec.SectionNotes = "";
                    subSec.SubSectionName = "Supporting Documents";
                    subSec.SubSectionType = 1;
                    subSec.SubSectionTypeCondition = "SupportingDocument";
                    subSec.Visible = true;
                    section.TemplateSubSections.Add(subSec);
                    CreateTemplateSubSecPermission(0, subSec, section);//Rajesh on 2/15/2014
                }
                if (section.TemplateSectionType == 12)//Prequalification documents 
                {
                    TemplateSubSections subSec = new TemplateSubSections();
                    subSec.DisplayOrder = 1;
                    subSec.NoOfColumns = 0;
                    subSec.SectionNotes = "";
                    subSec.SubSectionName = "Prequalification Documents";
                    subSec.SubSectionType = 1;
                    subSec.SubSectionTypeCondition = "PQDocuments2";
                    subSec.Visible = true;
                    section.TemplateSubSections.Add(subSec);
                    CreateTemplateSubSecPermission(0, subSec, section);
                }
                else if (section.TemplateSectionType == 7)//BU Sites
                {
                    TemplateSubSections subSec = new TemplateSubSections();
                    subSec.DisplayOrder = 1;
                    subSec.NoOfColumns = 0;
                    subSec.SectionNotes = "";
                    subSec.SubSectionName = "Locations";
                    subSec.SubSectionType = 1;
                    subSec.SubSectionTypeCondition = "SitePrequalification";
                    subSec.Visible = true;
                    section.TemplateSubSections.Add(subSec);
                    CreateTemplateSubSecPermission(0, subSec, section);//Rajesh on 2/15/2014
                }
                else if (section.TemplateSectionType == 3)//Payment
                {
                    string subSectionName = LocalConstants.GetTranslatedResource("Com_Payment", languageCode);
                    TemplateSubSections subSec = new TemplateSubSections();
                    subSec.DisplayOrder = 1;
                    subSec.NoOfColumns = 0;
                    subSec.SectionNotes = "";
                    subSec.SubSectionName = subSectionName;
                    subSec.SubSectionType = 2;
                    subSec.SubSectionTypeCondition = "PrequalificationPayment";
                    subSec.Visible = true;
                    section.TemplateSubSections.Add(subSec);
                    CreateTemplateSubSecPermission(0, subSec, section);//Rajesh on 2/15/2014
                }
                else if (section.TemplateSectionType == 36)//Location Approval
                {
                    TemplateSubSections subSec = new TemplateSubSections();
                    subSec.DisplayOrder = 1;
                    subSec.NoOfColumns = 0;
                    subSec.SectionNotes = "";
                    subSec.SubSectionName = "Assign Status by Location";
                    subSec.SubSectionType = 2;
                    subSec.SubSectionTypeCondition = "LocationApproval";
                    subSec.Visible = true;
                    section.TemplateSubSections.Add(subSec);
                    CreateTemplateSubSecPermission(0, subSec, section);//Rajesh on 2/15/2014
                }
                else if (section.TemplateSectionType == 99)//Finalize and submited
                {
                    string positionContent = LocalConstants.GetTranslatedResource("PQ_Note", languageCode);
                    string positionTitle = LocalConstants.GetTranslatedResource("Finalizebutton", languageCode);
                    string subSectionName = LocalConstants.GetTranslatedResource("Submitted_by", languageCode);
                    section.TemplateSectionsHeaderFooter = new List<TemplateSectionsHeaderFooter>();
                    TemplateSectionsHeaderFooter HandF = new TemplateSectionsHeaderFooter();
                    HandF.PositionType = 0;
                    HandF.PositionTitle = positionTitle;
                    HandF.PostionContent = "<div class='contentSub'><p>" + positionContent + "</p></div>";
                    section.TemplateSectionsHeaderFooter.Add(HandF);
                    TemplateSubSections subSec = new TemplateSubSections();
                    subSec.DisplayOrder = 1;
                    subSec.NoOfColumns = 0;
                    subSec.SectionNotes = "";
                    subSec.SubSectionName = "";
                    subSec.SubSectionType = 2;
                    subSec.SubSectionTypeCondition = "PrequalificationFinalize";
                    subSec.Visible = true;
                    subSec.SubHeader = false;//Rajesh on 1/11/2014
                    section.TemplateSubSections.Add(subSec);
                    CreateTemplateSubSecPermission(0, subSec, section);//Rajesh on 2/15/2014
                }
                else if (section.TemplateSectionType == 40)//Client Selection 
                {
                    TemplateSubSections subSec = new TemplateSubSections();
                    subSec.DisplayOrder = 1;
                    subSec.NoOfColumns = 1;
                    subSec.SubHeader = false;
                    subSec.NotificationExist = false;
                    //subSec.SectionNotes = "In the first window below, choose your operating area by state/province, region or country. (To choose multiple states, press the Ctrl key and click.) Clients that have locations in your operating area will appear in the second window. Select one or more clients from that window.  (To choose multiple clients, press the Ctrl key and click.) Save the page, and then select the client sites in the Locations section.";
                    subSec.SubSectionName = "Select Clients";
                    subSec.SubSectionType = 2;
                    subSec.SubSectionTypeCondition = "ERIClientSelection";
                    subSec.Visible = true;
                    section.TemplateSubSections.Add(subSec);
                    CreateTemplateSubSecPermission(0, subSec, section);//Rajesh on 2/15/2014
                }
                //Rajesh on 1/11/2014
                else if (section.TemplateSectionType == 34)//IsExceptionProbationRequest
                {
                    section.TemplateSectionsHeaderFooter = new List<TemplateSectionsHeaderFooter>();
                    TemplateSubSections subSec = new TemplateSubSections();
                    subSec.DisplayOrder = 1;
                    subSec.NoOfColumns = 0;
                    subSec.SectionNotes = "";
                    subSec.SubSectionName = "Vendor Information";
                    subSec.SubSectionType = 2;
                    subSec.SubSectionTypeCondition = "UserDetailsForEP";
                    subSec.Visible = true;
                    subSec.SubHeader = false;
                    section.TemplateSubSections.Add(subSec);
                    CreateTemplateSubSecPermission(0, subSec, section);//Rajesh on 2/15/2014
                }
                //Ends<<<
                //Rajesh on 1/21/2014
                else if (section.TemplateSectionType == 35)//For Client Data Entry
                {
                    //this is insertion so no need to delete permissions for this template....
                    //section.TemplateSectionsPermission = new List<TemplateSectionsPermission>();

                    //foreach (var selectedClient in Form.Clients.Where(rec => rec.Selected))
                    //{
                    //    //TemplateSectionsPermission permissions = new TemplateSectionsPermission();
                    //    ////permissions.ClientUserId = new Guid(selectedClient.Value);
                    //    //section.TemplateSectionsPermission.Add(permissions);
                    //}

                }
                //Ends<<
                //Rajesh on 2/6/2014
                else if (section.TemplateSectionType == 33)//For Agreement
                {
                    CreateAgreementSection(form, section);
                }
                //Ends<<

            }



            entityDB.SaveChanges();
            return Redirect("PopUpCloseView");
        }

        private void CreateAgreementSection(LocalSectionsModel Form, TemplateSections section)
        {
            var SignedQBank = entityDB.QuestionsBank.FirstOrDefault(rec => rec.QuestionString == "Signed:");

            if (SignedQBank == null)
            {
                SignedQBank = new QuestionsBank();
                SignedQBank.QuestionString = "Signed:";
                SignedQBank.QuestionStatus = 1;
                SignedQBank.UsedFor = 0;
                entityDB.QuestionsBank.Add(SignedQBank);
            }

            var TitleQBank = entityDB.QuestionsBank.FirstOrDefault(rec => rec.QuestionString == "Title:");

            if (TitleQBank == null)
            {
                TitleQBank = new QuestionsBank();
                TitleQBank.QuestionString = "Title:";
                TitleQBank.QuestionStatus = 1;
                TitleQBank.UsedFor = 0;
                entityDB.QuestionsBank.Add(TitleQBank);
            }

            var DateQBank = entityDB.QuestionsBank.FirstOrDefault(rec => rec.QuestionString == "Date:");

            if (DateQBank == null)
            {
                DateQBank = new QuestionsBank();
                DateQBank.QuestionString = "Date:";
                DateQBank.QuestionStatus = 1;
                DateQBank.UsedFor = 0;
                entityDB.QuestionsBank.Add(DateQBank);
            }
            entityDB.SaveChanges();
            //This is Agreement SubSection
            //subSec.DisplayOrder = 1;
            //subSec.NoOfColumns = 0;
            //subSec.SectionNotes = "[_ClientName] (the “Buyer”), and [_VendorName], (the “Seller”), in consideration for purchases by the Buyer, agree that the following terms shall apply to all sales contracts entered into between Buyer and Seller whether or not such terms are expressly set forth or referenced in any future purchase order by Buyer to Seller.";
            //subSec.SubSectionName = "";
            //subSec.SubSectionType = 2;
            //subSec.SubSectionTypeCondition = "MPATitleHeader";
            //subSec.Visible = true;
            //subSec.SubHeader = false;
            //section.TemplateSubSections.Add(subSec);
            //CreateTemplateSubSecPermission(0, subSec, section);//Rajesh on 2/15/2014
            //This is Vendor Signatures
            section.TemplateSectionsHeaderFooter = new List<TemplateSectionsHeaderFooter>();
            var headerFooter = new TemplateSectionsHeaderFooter
            {
                PositionType = 0,
                PositionTitle = "",
                PostionContent =
                    "[_ClientName] (the “Buyer”), and [_VendorName], (the “Seller”), in consideration for purchases by the Buyer, agree that the following terms shall apply to all sales contracts entered into between Buyer and Seller effective on [_SecSubmittedDate] whether or not such terms are expressly set forth or referenced in any future purchase order by Buyer to Seller."
            };
            //headerFooter.TemplateSectionId = section.TemplateSectionID;
            section.TemplateSectionsHeaderFooter.Add(headerFooter);
            //Ends<<

            var subSec = new TemplateSubSections
            {
                DisplayOrder = 2,
                NoOfColumns = 0,
                SectionNotes =
                    "In accordance with the Electronic Signatures in Global and National Commerce Act (ESIGN Act, 15 U.S.C.§§ 7001-7031), entering your full name below is a signature with full legal force and effect",
                SubSectionName = "Vendor Signature",
                SubSectionType = 0,
                SubSectionTypeCondition = "VendorSign",
                Visible = true,
                SubHeader = false
            };
            section.TemplateSubSections.Add(subSec);
            CreateTemplateSubSecPermission(0, subSec, section); //Rajesh on 2/15/2014
            //subSec.TemplateSubSectionsPermissions.Remove(subSec.TemplateSubSectionsPermissions.FirstOrDefault(rec => rec.PermissionFor == "Client"));//Rajesh on 2/15/2014

            //To Add Sign Question
            subSec.Questions = new List<Questions>();
            var question = new Questions
            {
                DisplayOrder = 1,
                DisplayResponse = true,
                HasDependantQuestions = false,
                IsBold = true,
                IsMandatory = true,
                NumberOfColumns = 1,
                QuestionBankId = SignedQBank.QuestionBankId,
                QuestionText = SignedQBank.QuestionString,
                ResponseRequired = false,
                Visible = true
            };
            subSec.Questions.Add(question);
            question.QuestionColumnDetails = new List<QuestionColumnDetails>();
            var details = new QuestionColumnDetails
            {
                ColumnNo = 1,
                DisplayType = 0,
                QuestionControlTypeId = 2
            };
            question.QuestionColumnDetails.Add(details);


            question = new Questions
            {
                DisplayOrder = 1,
                DisplayResponse = true,
                HasDependantQuestions = false,
                IsBold = true,
                IsMandatory = true,
                NumberOfColumns = 1,
                QuestionBankId = TitleQBank.QuestionBankId,
                QuestionText = TitleQBank.QuestionString,
                ResponseRequired = false,
                Visible = true
            };
            subSec.Questions.Add(question);
            question.QuestionColumnDetails = new List<QuestionColumnDetails>();
            details = new QuestionColumnDetails();
            details.ColumnNo = 1;
            details.DisplayType = 0;
            details.QuestionControlTypeId = 2;
            question.QuestionColumnDetails.Add(details);

            question = new Questions
            {
                DisplayOrder = 1,
                DisplayResponse = true,
                HasDependantQuestions = false,
                IsBold = true,
                IsMandatory = true,
                NumberOfColumns = 1,
                QuestionBankId = DateQBank.QuestionBankId,
                QuestionText = DateQBank.QuestionString,
                ResponseRequired = false,
                Visible = true
            };
            subSec.Questions.Add(question);
            question.QuestionColumnDetails = new List<QuestionColumnDetails>();
            var vendorDatedetails = new QuestionColumnDetails
            {
                ColumnNo = 1,
                DisplayType = 0,
                QuestionControlTypeId = 11
            };
            question.QuestionColumnDetails.Add(vendorDatedetails);

            //This is Client Signatures
            subSec = new TemplateSubSections
            {
                DisplayOrder = 3,
                NoOfColumns = 0,
                SectionNotes =
                    "In accordance with the Electronic Signatures in Global and National Commerce Act (ESIGN Act, 15 U.S.C.§§ 7001-7031), entering your full name below is a signature with full legal force and effect",
                SubSectionName = "Client Signature",
                SubSectionType = 0,
                SubSectionTypeCondition = "ClientSign",
                Visible = true,
                SubHeader = false
            };
            section.TemplateSubSections.Add(subSec);
            CreateTemplateSubSecPermission(0, subSec, section); //Rajesh on 2/15/2014
            //subSec.TemplateSubSectionsPermissions.Remove(
            //        subSec.TemplateSubSectionsPermissions.FirstOrDefault(rec => rec.PermissionFor == "Vendor"));
            //Rajesh on 2/15/2014
            subSec.TemplateSubSectionsPermissions.FirstOrDefault(rec => rec.PermissionFor == "Client").PermissionType = 1;
            //Rajesh on 2/15/2014


            //To Add Sign Question
            subSec.Questions = new List<Questions>();
            question = new Questions
            {
                DisplayOrder = 1,
                DisplayResponse = true,
                HasDependantQuestions = false,
                IsBold = true,
                IsMandatory = true,
                NumberOfColumns = 1,
                QuestionBankId = SignedQBank.QuestionBankId,
                QuestionText = SignedQBank.QuestionString,
                ResponseRequired = false,
                Visible = true
            };
            subSec.Questions.Add(question);
            question.QuestionColumnDetails = new List<QuestionColumnDetails>();
            details = new QuestionColumnDetails
            {
                ColumnNo = 1,
                DisplayType = 0,
                QuestionControlTypeId = 2
            };
            question.QuestionColumnDetails.Add(details);

            CreateClientuserSign(Form, question, details, null); //Rajesh on 6/26/2014
            question = new Questions
            {
                DisplayOrder = 1,
                DisplayResponse = true,
                HasDependantQuestions = false,
                IsBold = true,
                IsMandatory = true,
                NumberOfColumns = 1,
                QuestionBankId = TitleQBank.QuestionBankId,
                QuestionText = TitleQBank.QuestionString,
                ResponseRequired = false,
                Visible = true
            };
            subSec.Questions.Add(question);
            question.QuestionColumnDetails = new List<QuestionColumnDetails>();
            details = new QuestionColumnDetails
            {
                ColumnNo = 1,
                DisplayType = 0,
                QuestionControlTypeId = 2
            };
            question.QuestionColumnDetails.Add(details);

            CreateClientuserSign(Form, question, details, null); //Rajesh on 6/26/2014

            question = new Questions
            {
                DisplayOrder = 1,
                DisplayResponse = true,
                HasDependantQuestions = false,
                IsBold = true,
                IsMandatory = true,
                NumberOfColumns = 1,
                QuestionBankId = DateQBank.QuestionBankId,
                QuestionText = DateQBank.QuestionString,
                ResponseRequired = false,
                Visible = true
            };
            subSec.Questions.Add(question);
            question.QuestionColumnDetails = new List<QuestionColumnDetails>();
            details = new QuestionColumnDetails
            {
                ColumnNo = 1,
                DisplayType = 0,
                QuestionControlTypeId = 11
            };
            question.QuestionColumnDetails.Add(details);




            subSec = new TemplateSubSections
            {
                DisplayOrder = 4,
                NoOfColumns = 0,
                SectionNotes = "",
                SubSectionName = "",
                SubSectionType = 2,
                SubSectionTypeCondition = SubSectionTypeConditionType.AgreementQueries,
                Visible = true,
                SubHeader = false
            };
            CreateTemplateSubSecPermission(0, subSec, section);
            subSec.TemplateSubSectionsPermissions.Remove(
                    subSec.TemplateSubSectionsPermissions.FirstOrDefault(rec => rec.PermissionFor == "Client"));
            section.TemplateSubSections.Add(subSec);


            subSec = new TemplateSubSections
            {
                DisplayOrder = 5,
                NoOfColumns = 0,
                SectionNotes = "",
                SubSectionName = "",
                SubSectionType = 2,
                SubSectionTypeCondition = SubSectionTypeConditionType.AgreementAddendum,
                Visible = true,
                SubHeader = false
            };
            section.TemplateSubSections.Add(subSec);
            CreateTemplateSubSecPermission(0, subSec, section);
            entityDB.SaveChanges();
            CreateClientuserSign(Form, question, details, vendorDatedetails); //Rajesh on 6/26/2014
        }

        //Rajesh on 6/26/2014
        private void CreateClientuserSign(LocalSectionsModel form, Questions question, QuestionColumnDetails details, QuestionColumnDetails valueQuestionId)
        {
            ClientUserSignature clientSign = new ClientUserSignature();
            try
            {
                var template = entityDB.Templates.FirstOrDefault(rec => rec.TemplateID == form.Templateid);
                if (template == null || (template.DefaultTemplate == null || template.DefaultTemplate == true))
                    return;
                var templates = entityDB.Templates.FirstOrDefault(rec => rec.TemplateID == form.Templateid);
                if (templates != null)
                {
                    var clientTemplates = templates.ClientTemplates.FirstOrDefault();
                    if (clientTemplates != null)
                    {
                        var clientId = clientTemplates.ClientID;
                        clientSign.ClientId = clientId;
                    }
                }
            }
            catch
            {
                // ignored
            }
            clientSign.QuestionColumnId = details.QuestionColumnId;
            clientSign.QuestionText = question.QuestionText;
            if (valueQuestionId != null)
                clientSign.GetValueQuestionColumId = valueQuestionId.QuestionColumnId;
            details.ClientUserSignature = new List<ClientUserSignature>();
            if (question.QuestionText == "Signed:")
                clientSign.QuestionColumnIdValue = form.Signed;
            else if (question.QuestionText == "Title:")
                clientSign.QuestionColumnIdValue = form.Title;
            if (entityDB.ClientUserSignature.FirstOrDefault(rec => rec.ClientId == clientSign.ClientId && rec.QuestionColumnId == details.QuestionColumnId) == null)
                details.ClientUserSignature.Add(clientSign);
        }
        // Ends<<<
        //Rajesh on 10/21/2013

        public string DeleteSection(long TemplateSectionID)
        {
            var Tsection = entityDB.TemplateSections.Find(TemplateSectionID);

            ////Rajesh on 1/21/2014
            //foreach (var TemplatePermissions in Tsection.TemplateSectionsPermission.ToList())
            //{
            //    entityDB.Entry(TemplatePermissions).State = EntityState.Deleted;
            //}
            ////Ends<<<
            foreach (var HandF in Tsection.TemplateSectionsHeaderFooter.ToList())
            {
                entityDB.Entry(HandF).State = EntityState.Deleted;
            }
            if (Tsection.TemplateSectionType == 4 || Tsection.TemplateSectionType == 5 || Tsection.TemplateSectionType == 6)//Doc,Proficiancey,Bidding
            {
                foreach (var cTemplates in Tsection.Templates.ClientTemplates.ToList())
                {
                    var reportingType = 2;
                    if (Tsection.TemplateSectionType == 5)
                        reportingType = 1;
                    else if (Tsection.TemplateSectionType == 6)
                        reportingType = 0;
                    else if (Tsection.TemplateSectionType == 8)
                        reportingType = 3;
                    foreach (var CTRData in cTemplates.ClientTemplateReportingData.Where(rec => rec.ReportingType == reportingType).ToList())
                    {
                        entityDB.ClientTemplateReportingData.Remove(CTRData);
                    }

                }
            }
            foreach (var ssection in Tsection.TemplateSubSections.ToList())//Delete SubSections
            {

                foreach (var question in ssection.Questions.ToList())//Delete Questions
                {
                    if (question.QuestionColumnDetails != null)
                        foreach (var qcolumn in question.QuestionColumnDetails.ToList())
                        {
                            foreach (var clientUserSign in qcolumn.ClientUserSignature.ToList())
                                entityDB.Entry(clientUserSign).State = EntityState.Deleted;
                            entityDB.Entry(qcolumn).State = EntityState.Deleted;
                        }
                    if (question.QuestionsDependants != null)
                    {
                        foreach (var qdependent in question.QuestionsDependants.ToList())
                            entityDB.Entry(qdependent).State = EntityState.Deleted;
                    }
                    entityDB.Entry(question).State = EntityState.Deleted;
                }
                foreach (var agreementSubsec in ssection.AgreementSubSectionConfiguration.ToList())
                {
                    entityDB.Entry(agreementSubsec).State = EntityState.Deleted;
                }
                if (ssection.PrequalificationSubsection.Any())
                {
                    foreach (var pqSection in ssection.PrequalificationSubsection.ToList())
                    {
                        foreach (var pqQuestions in pqSection.PrequalificationQuestion.ToList())
                        {
                            entityDB.Entry(pqQuestions).State = EntityState.Deleted;
                        }
                        entityDB.Entry(pqSection).State = EntityState.Deleted;
                    }
                }
                //Rajesh on 2/11/2014
                if (ssection.TemplateSubSectionsPermissions != null)
                {
                    foreach (var SubSecPermission in ssection.TemplateSubSectionsPermissions.ToList())
                    {
                        if (SubSecPermission.TemplateSubSectionsUserPermissions != null)
                        {
                            foreach (var SubSecUserPermission in SubSecPermission.TemplateSubSectionsUserPermissions.ToList())
                            {
                                entityDB.Entry(SubSecUserPermission).State = EntityState.Deleted;
                            }
                        }
                        entityDB.Entry(SubSecPermission).State = EntityState.Deleted;
                    }
                    if (ssection.TemplateSubSectionsNotifications != null)
                        foreach (var notification in ssection.TemplateSubSectionsNotifications.ToList())
                        {
                            entityDB.Entry(notification).State = EntityState.Deleted;
                        }
                }
                //Ends<<
                entityDB.Entry(ssection).State = EntityState.Deleted;
            }
            //Rajesh on 2/11/2014
            if (Tsection.TemplateSectionsPermission != null)
            {
                foreach (var permission in Tsection.TemplateSectionsPermission.ToList())
                {
                    entityDB.Entry(permission).State = EntityState.Deleted;
                }
            }
            //Ends<<<
            entityDB.Entry(Tsection).State = EntityState.Deleted;
            entityDB.SaveChanges();
            return "OK";
        }
        //<<<Ends

        [SessionExpire]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "QuestionnaireTool", sideMenuName = "TemplateTool", ViewName = "SubSectionView")]
        public ActionResult SubSectionView(long SectionId)
        {
            TemplateSections Section = entityDB.TemplateSections.Find(SectionId);
            var langugaeCode = _org.GetLanguageCode((long)Section.Templates.LanguageId);
            _preq.SetCulture(langugaeCode);
            if (Section.TemplateSectionType == 33)
            {
                //ViewBag.AllHeaders = Section.TemplateSectionsHeaderFooter.Where(rec => rec.PositionType == 0).ToList();
                //ViewBag.AllFooters = Section.TemplateSectionsHeaderFooter.Where(rec => rec.PositionType == 1).ToList();
                return Redirect("AgreementSubSectionView?SectionId=" + SectionId);
            }
            ActionResult actionResult;
            if (ConfigureSubSections(SectionId, Section, out actionResult)) return actionResult;

            //Rajesh on 1/4/2014


            return View();
            //Ends<<<
        }
        [SessionExpire]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "QuestionnaireTool", sideMenuName = "TemplateTool", ViewName = "SubSectionView")]
        public ActionResult AgreementSubSectionView(long SectionId)
        {
            TemplateSections Section = entityDB.TemplateSections.Find(SectionId);
            ActionResult actionResult;
            if (ConfigureSubSections(SectionId, Section, out actionResult)) return actionResult;

            ViewBag.AllHeaders = Section.TemplateSectionsHeaderFooter.Where(rec => rec.PositionType == 0).ToList();
            ViewBag.AllFooters = Section.TemplateSectionsHeaderFooter.Where(rec => rec.PositionType == 1).ToList();
            return View();

        }
        private bool ConfigureSubSections(long SectionId, TemplateSections Section, out ActionResult actionResult)
        {



            if (Section.Templates.DefaultTemplate == false)
            {

                ViewBag.Clients =
                    Section.Templates.ClientTemplates.Select(r => r.Organizations).ToList();
            }
            else
            {
                ViewBag.Clients = entityDB.Organizations.Where(rec => rec.OrganizationType == "Client").ToList();
            }
            //To Change Side Menu Selection>>>
            if (Section.Templates.DefaultTemplate == null || Section.Templates.DefaultTemplate == false)
            {
                List<LocalHeaderMenuModel> headerMenu = (List<LocalHeaderMenuModel>)ViewBag.headerMenus;
                headerMenu.FirstOrDefault(record => record.menuName == "QuestionnaireTool")
                    .sideMenus.ForEach(record => record.isChecked = false);
                headerMenu.FirstOrDefault(record => record.menuName == "QuestionnaireTool")
                    .sideMenus.FirstOrDefault(record => record.menuName == "CustomTemplateTool")
                    .isChecked = true;
                ViewBag.headerMenus = headerMenu;
            }

            //Ends<<<


            //To Handle Dropdown Change in subsection view
            //If user Select the section of bidding(6) or proficiency(5)
            if (Section.TemplateSectionType == 6)
            {
                actionResult = Redirect("AddOrEditPreSubSections?SectionId=" + Section.TemplateSectionID);
                return true;
            }
            if (Section.TemplateSectionType == 0) //Is Profile
            {
                {
                    actionResult = Redirect("TemplatesAndSections");
                    return true;
                }
            }

            ViewBag.Section = Section;
            ViewBag.SectionId = SectionId;
            ViewBag.TemplateId = Section.Templates.TemplateID; //Rajesh on 19/10/2013
            ViewBag.SubSections = entityDB.TemplateSubSections.Where(rec => rec.TemplateSectionID == SectionId).ToList();

            ViewBag.Header = Section.TemplateSectionsHeaderFooter.FirstOrDefault(rec => rec.PositionType == 0);
            ViewBag.Footer = Section.TemplateSectionsHeaderFooter.FirstOrDefault(rec => rec.PositionType == 1);

            ViewBag.SectionName = Section.SectionName;
            ViewBag.TemplateName = Section.Templates.TemplateName;
            ViewBag.Templates = (from templates in entityDB.Templates.Where(rec => rec.TemplateType == 0).ToList()
                                 select new SelectListItem()
                                 {
                                     Text = templates.TemplateName,
                                     Value = templates.TemplateID + "",
                                     Selected = templates.TemplateID == ViewBag.TemplateId
                                 }).ToList(); //Rajesh on 10/19/2013
            actionResult = null;
            return false;
        }

        [HttpPost]
        public ActionResult GetSections(Guid TemplateId, long SectionId)
        {

            var curretUserOrgBusUnits = (from sec in entityDB.TemplateSections.Where(rec => rec.TemplateID == TemplateId && rec.TemplateSectionType != 0).ToList()
                                         select new SelectListItem
                                         {
                                             Text = sec.SectionName,
                                             Value = sec.TemplateSectionID + "",
                                             Selected = SectionId == sec.TemplateSectionID
                                         }).ToList();



            return Json(curretUserOrgBusUnits);
        }
        [SessionExpireForView]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "QuestionnaireTool", sideMenuName = "TemplateTool", ViewName = "AddOrEditSubSections")]
        public ActionResult AddOrEditSubSections(long SectionId, long subSecId)
        {

            var section = entityDB.TemplateSections.Find(SectionId);
            ViewBag.IsERI = section.Templates.IsERI;
            ViewBag.sectionType = section.TemplateSectionType;
            ViewBag.isDefault = section.Templates.DefaultTemplate;
            LocalSubSection subSection = new LocalSubSection();
            subSection.PermissionType = 0;//Rajesh on 2/11/2014
            subSection.SectionId = SectionId;
            ViewBag.Section = section;//Rajesh on 1/7/2014
            subSection.SubSectionType = "0";
            subSection.NotificationExist = false;//Rajesh on 2/7/2014
            subSection.SubSecId = subSecId;
            // Kiran on 4/22/2014
            try
            {
                subSection.InVisibleToClientSection = entityDB.TemplateSections.Find(SectionId).TemplateSectionsPermission.FirstOrDefault(rec => rec.VisibleTo == 1).VisibleTo;
            }
            catch { }
            if (subSecId != -1)
            {
                var subSec = entityDB.TemplateSubSections.Find(subSecId);

                //Rajesh on 2/11/2014
                if (subSec.TemplateSubSectionsPermissions == null || subSec.TemplateSubSectionsPermissions.Count == 0)
                    subSection.PermissionType = 0;
                else if (ViewBag.IsERI && section.TemplateSectionsPermission.Any(r => r.VisibleTo == 2))
                    subSection.PermissionType = 1;
                else
                    try { subSection.PermissionType = subSec.TemplateSubSectionsPermissions.FirstOrDefault(rec => rec.PermissionFor == "Client").PermissionType; }
                    catch { }//Rajesh on 3/29/2014
                //Ends<<

                if (subSec.SubSectionType != null)
                    subSection.SubSectionType = subSec.SubSectionType + "";
                subSection.SubSectionTypeCondition = subSec.SubSectionTypeCondition;//Rajesh on 1/7/2014
                switch ((int)subSec.SubSectionType)
                {
                    case 1: subSection.Dynamic_Condition = subSec.SubSectionTypeCondition; break;
                    case 2: subSection.Predefined_Condition = subSec.SubSectionTypeCondition; break;
                    case 3: subSection.Reporting_Condition = subSec.SubSectionTypeCondition; break;
                }
                subSection.SubSectionName = subSec.SubSectionName;
                subSection.SubSectionNote = subSec.SectionNotes;
                subSection.NoOfColumns = (int)subSec.NoOfColumns;
                subSection.Visible = !(bool)subSec.Visible;
                subSection.isSubSubSection = subSec.SubHeader ?? false;
                subSection.NotificationExist = subSec.NotificationExist == null ? false : (bool)subSec.NotificationExist;//Rajesh on 2/7/2014
            }
            else if (ViewBag.IsERI && section.TemplateSectionsPermission.Any(r => r.VisibleTo == 2))
                subSection.PermissionType = 1;
            return View(subSection);
        }
        [HttpPost]
        [SessionExpireForView]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "QuestionnaireTool", sideMenuName = "TemplateTool", ViewName = "AddOrEditSubSections")]
        public ActionResult AddOrEditSubSections(LocalSubSection Form)
        {

            TemplateSubSections subSection = new TemplateSubSections();
            int displayOrder = 1;
            var TSection = entityDB.TemplateSections.Find(Form.SectionId);
            ViewBag.IsERI = TSection.Templates.IsERI;
            try { displayOrder = TSection.TemplateSubSections.Count() + 1; }
            catch { }

            if (Form.SubSecId != -1)
            {
                subSection = entityDB.TemplateSubSections.Find(Form.SubSecId);
                subSection.Visible = !Form.Visible;
                //Rajesh on 2/15/2014
                if (subSection.TemplateSubSectionsPermissions == null || subSection.TemplateSubSectionsPermissions.Count == 0)
                {
                    CreateTemplateSubSecPermission(Form.PermissionType, subSection, TSection);
                }
                else//Update
                {
                    if (subSection.SubSectionTypeCondition != "CSIWebServiceResponse")//Rajesh on 3/29/2014
                    {
                        //To Remove SubSecPermission
                        if (subSection.TemplateSections.TemplateSectionType != 33 && !(TSection.Templates.IsERI && TSection.TemplateSectionsPermission.Any(r => r.VisibleTo == 2)))//If it is not a MPA Section
                        {


                            foreach (var SubPermission in subSection.TemplateSubSectionsPermissions.ToList())
                            {
                                foreach (var subUserPermission in SubPermission.TemplateSubSectionsUserPermissions.ToList())
                                {
                                    entityDB.Entry(subUserPermission).State = EntityState.Deleted;
                                }
                                entityDB.Entry(SubPermission).State = EntityState.Deleted;
                            }

                            CreateTemplateSubSecPermission(Form.PermissionType, subSection, TSection);
                        }
                    }
                }
                //Ends<<<
            }
            else//Insertion
            {
                subSection.DisplayOrder = displayOrder;
                subSection.Visible = true;

                subSection.SubSectionType = Convert.ToInt32(Form.SubSectionType);
                subSection.TemplateSectionID = Form.SectionId;

                string condition = "";
                if (Form.SubSectionType.Equals("1"))//Dynamic
                {
                    condition = Form.Dynamic_Condition;
                }
                else if (Form.SubSectionType.Equals("2"))//Predefinied
                {
                    //condition = Form.Predefined_Condition;

                    TSection.TemplateSectionType = 32;//IsVendorContactInfo
                    entityDB.Entry(TSection).State = EntityState.Modified;
                    condition = "UserOrganization";
                }
                else if (Form.SubSectionType.Equals("3"))//Reporting Data
                {
                    condition = Form.Reporting_Condition;
                }
                else if (Form.SubSectionType.Equals("4"))//Reporting Data
                {
                    condition = Form.Reporting_Condition;
                }
                else if (Form.SubSectionType.Equals("102"))//Agreement between parties
                {
                    subSection.SubSectionType = 2;
                    condition = "MPATitleHeader";
                }
                else if (Form.SubSectionType.Equals("103"))//PrequalificationComments
                {
                    subSection.SubSectionType = 1;
                    condition = "PrequalComments";
                }
                else if (Form.SubSectionType.Equals("107"))//ERI Cleint Selections
                {
                    subSection.SubSectionType = 2;
                    condition = LocalConstants.SubSectionTypeCondition.ERIClientSelection;
                    //Form.SubSectionName = "ERI Clients & Locations";


                    //subSection.Questions = new List<Questions>();
                    //var displayOrder_loc = 0;
                    //var questionBank = entityDB.QuestionsBank.Find(94);
                    //CreateQuestion(subSection, questionBank, ++displayOrder_loc, 0, false, 3);
                    //subSection.Questions[0].QuestionColumnDetails = new List<QuestionColumnDetails>();
                    //subSection.Questions[0].NumberOfColumns = 1;
                    //subSection.Questions[0].QuestionColumnDetails.Add(new QuestionColumnDetails()
                    //{
                    //    QuestionControlTypeId = 10,
                    //    ColumnNo = 1,
                    //    DisplayType = 0,
                    //    TableReference = "UnitedStates",
                    //    TableReferenceField = "StatesId",
                    //    TableRefFieldValueDisplay = "StateName",

                    //});
                    //CreateQuestion(subSection, questionBank, ++displayOrder_loc, 0, false, 3);
                    //subSection.Questions[1].QuestionColumnDetails = new List<QuestionColumnDetails>();
                    //subSection.Questions[1].NumberOfColumns = 1;
                    //subSection.Questions[1].QuestionColumnDetails.Add(new QuestionColumnDetails()
                    //{
                    //    QuestionControlTypeId = 10,
                    //    ColumnNo = 1,
                    //    DisplayType = 0,
                    //    TableReference = "UnitedStates",
                    //    TableReferenceField = "StatesId",
                    //    TableRefFieldValueDisplay = "StateName",

                    //});
                }
                //Rajesh on 3/28/2014
                else if (Form.SubSectionType.Equals("105"))//PrequalificationComments
                {
                    subSection.SubSectionType = 0;
                    Form.SubSectionName = "Safety Performance History";
                    condition = "YearWiseStats";
                    subSection.Questions = new List<Questions>();
                    var displayOrder_loc = 0;
                    var questionBank = entityDB.QuestionsBank.Find(567);
                    CreateQuestion(subSection, questionBank, ++displayOrder_loc, 0, false, 3);
                    questionBank = entityDB.QuestionsBank.Find(130);
                    CreateQuestion(subSection, questionBank, ++displayOrder_loc, 0, false, 3);


                    questionBank = entityDB.QuestionsBank.Find(1000);
                    CreateQuestion(subSection, questionBank, ++displayOrder_loc, 3, true, 2);


                    questionBank = entityDB.QuestionsBank.Find(121);
                    CreateQuestion(subSection, questionBank, ++displayOrder_loc, 3, true, 3);
                    questionBank = entityDB.QuestionsBank.Find(122);
                    CreateQuestion(subSection, questionBank, ++displayOrder_loc, 3, true, 3);
                    questionBank = entityDB.QuestionsBank.Find(123);
                    CreateQuestion(subSection, questionBank, ++displayOrder_loc, 3, true, 3);
                    questionBank = entityDB.QuestionsBank.Find(124);
                    CreateQuestion(subSection, questionBank, ++displayOrder_loc, 3, true, 3);
                    questionBank = entityDB.QuestionsBank.Find(125);
                    CreateQuestion(subSection, questionBank, ++displayOrder_loc, 3, true, 3);
                    questionBank = entityDB.QuestionsBank.Find(126);
                    CreateQuestion(subSection, questionBank, ++displayOrder_loc, 3, true, 3);
                    questionBank = entityDB.QuestionsBank.Find(568);
                    CreateQuestion(subSection, questionBank, ++displayOrder_loc, 3, true, 3);

                }
                //Ends<<<

                subSection.SubSectionTypeCondition = condition;
                //Rajesh on 2/15/2014
                CreateTemplateSubSecPermission(Form.PermissionType, subSection, TSection);
                //Ends<<

                //Rajesh on 2/17/2014
                if (Form.SubSectionType.Equals("2"))
                {
                    var subSec = new TemplateSubSections();
                    subSec.SubSectionType = 2;
                    subSec.SubSectionName = "CSI Information";
                    subSec.DisplayOrder = displayOrder + 1;
                    subSec.Visible = true;
                    subSec.SubHeader = false;
                    subSec.NoOfColumns = 1;
                    subSec.SubSectionTypeCondition = "CSIWebServiceResponse";
                    subSec.TemplateSectionID = Form.SectionId;
                    entityDB.TemplateSubSections.Add(subSec);
                    CreateTemplateSubSecPermission(Form.PermissionType, subSec, TSection);
                    subSec.TemplateSubSectionsPermissions.Remove(subSec.TemplateSubSectionsPermissions.FirstOrDefault(rec => rec.PermissionFor == "Client"));
                    try { subSec.TemplateSubSectionsPermissions.Remove(subSec.TemplateSubSectionsPermissions.FirstOrDefault(rec => rec.PermissionFor == "Vendor")); }
                    catch { }
                }
                //Ends<<<
            }

            subSection.SubSectionName = Form.SubSectionName;
            //subSection.SectionNotes = Form.SubSectionType;
            subSection.NoOfColumns = Form.NoOfColumns;
            subSection.SectionNotes = Form.SubSectionNote;
            subSection.SubHeader = Form.isSubSubSection;
            subSection.NotificationExist = Form.NotificationExist;//Rajesh on 2/7/2014
            //Rajesh on 3/21/2014 To Delete notifications
            if (Form.NotificationExist == false)
            {
                if (subSection.TemplateSubSectionsNotifications != null)
                    foreach (var notification in subSection.TemplateSubSectionsNotifications.ToList())
                    {
                        // Kiran on 9/27/2014
                        if (notification.PrequalificationNotifications.Count() == 0)
                        {
                            entityDB.TemplateSubSectionsNotifications.Remove(notification);
                        }
                        else
                        {
                            notification.NotifyUserDisabled = true;
                            entityDB.Entry(notification).State = EntityState.Modified;
                        }// Ends<<<
                    }
            }
            //Ends<<<
            if (Form.SubSecId == -1)
                entityDB.TemplateSubSections.Add(subSection);
            else
                entityDB.Entry(subSection).State = EntityState.Modified;
            entityDB.SaveChanges();
            return Redirect("PopUpCloseView");
        }



        private static void CreateQuestion(TemplateSubSections subSection, Domain.Entities.QuestionsBank questionBank, int displayOrder, int numberOfQuestions, bool IsMandatory, long QuestionType)
        {
            Questions q = new Questions();
            q.QuestionText = questionBank.QuestionString;
            q.DisplayOrder = displayOrder;
            q.Visible = true;
            q.NumberOfColumns = numberOfQuestions;
            q.IsMandatory = IsMandatory;
            q.IsBold = true;
            q.ResponseRequired = false;
            q.DisplayResponse = true;
            q.HasDependantQuestions = false;
            q.QuestionBankId = questionBank.QuestionBankId;
            subSection.Questions.Add(q);

            q.QuestionColumnDetails = new List<QuestionColumnDetails>();
            if (numberOfQuestions != 0)
            {
                QuestionColumnDetails column1 = new QuestionColumnDetails();
                column1.QuestionControlTypeId = QuestionType;
                column1.ColumnNo = 1;
                column1.DisplayType = 0;
                q.QuestionColumnDetails.Add(column1);
                QuestionColumnDetails column2 = new QuestionColumnDetails();
                column2.QuestionControlTypeId = QuestionType;
                column2.ColumnNo = 2;
                column2.DisplayType = 0;
                q.QuestionColumnDetails.Add(column2);
                QuestionColumnDetails column3 = new QuestionColumnDetails();
                column3.QuestionControlTypeId = QuestionType;
                column3.ColumnNo = 3;
                column3.DisplayType = 0;
                q.QuestionColumnDetails.Add(column3);
            }
        }
        //Rajesh on 2/17/2014

        public string DeleteSubSection(long SubSectionID)
        {
            try
            {
                var Ssection = entityDB.TemplateSubSections.Find(SubSectionID);
                if (Ssection.SubSectionTypeCondition.Equals("UserOrganization"))
                {
                    //To Change Parent section to general
                    Ssection.TemplateSections.TemplateSectionType = 1;

                    var SsectionForCSI = Ssection.TemplateSections.TemplateSubSections.FirstOrDefault(rec => rec.SubSectionTypeCondition == "CSIWebServiceResponse");
                    if (SsectionForCSI != null)
                        DeleteSubSection(SsectionForCSI);
                }
                DeleteSubSection(Ssection);

                entityDB.SaveChanges();
                return "OK";
            }
            catch (Exception ee) { return ee.Message + ""; }
        }

        private void DeleteSubSection(TemplateSubSections Ssection)
        {
            //Rajesh on 2/11/2014
            if (Ssection.TemplateSubSectionsPermissions != null)
            {
                foreach (var SubSecPermission in Ssection.TemplateSubSectionsPermissions.ToList())
                {
                    if (SubSecPermission.TemplateSubSectionsUserPermissions != null)
                    {
                        foreach (var SubSecUserPermission in SubSecPermission.TemplateSubSectionsUserPermissions.ToList())
                        {
                            entityDB.Entry(SubSecUserPermission).State = EntityState.Deleted;
                        }
                    }
                    entityDB.Entry(SubSecPermission).State = EntityState.Deleted;
                }
                if (Ssection.TemplateSubSectionsNotifications != null)
                    foreach (var notification in Ssection.TemplateSubSectionsNotifications.ToList())
                    {
                        entityDB.Entry(notification).State = EntityState.Deleted;
                    }
            }
            //Ends<<
            foreach (var question in Ssection.Questions.ToList())//Delete Questions
            {
                if (question.QuestionColumnDetails != null)
                    foreach (var Qcolumn in question.QuestionColumnDetails.ToList())
                        entityDB.Entry(Qcolumn).State = EntityState.Deleted;
                if (question.QuestionsDependants != null)
                {
                    foreach (var Qdependent in question.QuestionsDependants.ToList())
                        entityDB.Entry(Qdependent).State = EntityState.Deleted;
                }
                entityDB.Entry(question).State = EntityState.Deleted;
            }


            entityDB.Entry(Ssection).State = EntityState.Deleted;
        }
        //<<<Ends


        //Rajesh on 10/21/2013

        public string DeleteQuestion(long Questionid)
        {
            var question = entityDB.Questions.Find(Questionid);
            if (question.QuestionColumnDetails != null)
            {
                foreach (var Qcolumn in question.QuestionColumnDetails.ToList())
                    entityDB.Entry(Qcolumn).State = EntityState.Deleted;
            }
            if (question.QuestionsDependants != null)
            {
                foreach (var Qdependent in question.QuestionsDependants.ToList())
                    entityDB.Entry(Qdependent).State = EntityState.Deleted;
            }
            entityDB.Entry(question).State = EntityState.Deleted;
            entityDB.SaveChanges();
            return "OK";
        }
        //<<<Ends


        [HttpPost]

        public ActionResult AddHeaderOrFooter(long headerFooterId, long SectionId, int type, string title, string content)
        {
            TemplateSectionsHeaderFooter headerFooter = new TemplateSectionsHeaderFooter();
            try
            {
                content = HttpUtility.HtmlDecode(content);//Rajesh On 10/18/2013
                title = HttpUtility.HtmlDecode(title);//Rajesh on 10/18/2013
                if (headerFooterId != -1)
                {
                    headerFooter = entityDB.TemplateSectionsHeaderFooter.Find(headerFooterId);
                }
                headerFooter.TemplateSectionId = SectionId;
                headerFooter.PositionType = type;
                headerFooter.PositionTitle = title.Trim();
                headerFooter.PostionContent = content.Trim();
                if (headerFooterId == -1)
                    entityDB.TemplateSectionsHeaderFooter.Add(headerFooter);
                else
                    entityDB.Entry(headerFooter).State = EntityState.Modified;
                entityDB.SaveChanges();
            }
            catch (Exception ee) { return Json(ee.Message); }
            var Section = entityDB.TemplateSections.Find(SectionId);
            var data = entityDB.TemplateSectionsHeaderFooter.ToList();
            if (Section.TemplateSectionType == 33)
            {
                ViewBag.HorF = entityDB.TemplateSectionsHeaderFooter.Where(rec => rec.PositionType == type && rec.TemplateSectionId == SectionId).ToList();
                ViewBag.type = type;
                return PartialView("HeaderOrFooterData");
            }


            return Json(headerFooter.HeaderFooterId + "");
        }


        public string DeleteHeaderOrFooter(long headerFooterId)
        {
            entityDB.TemplateSectionsHeaderFooter.Remove(entityDB.TemplateSectionsHeaderFooter.Find(headerFooterId));
            entityDB.SaveChanges();
            return "Ok";
        }
        [SessionExpire]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "QuestionnaireTool", sideMenuName = "TemplateTool", ViewName = "Questionslist")]
        public ActionResult Questionslist(long SubSectionId)
        {
            var SubSection = entityDB.TemplateSubSections.Find(SubSectionId);


            //To Change Side Menu Selection>>>
            if (SubSection.TemplateSections.Templates.DefaultTemplate == null || SubSection.TemplateSections.Templates.DefaultTemplate == false)
            {
                List<LocalHeaderMenuModel> headerMenu = (List<LocalHeaderMenuModel>)ViewBag.headerMenus;
                headerMenu.FirstOrDefault(record => record.menuName == "QuestionnaireTool").sideMenus.ForEach(record => record.isChecked = false);
                headerMenu.FirstOrDefault(record => record.menuName == "QuestionnaireTool").sideMenus.FirstOrDefault(record => record.menuName == "CustomTemplateTool").isChecked = true;
                ViewBag.headerMenus = headerMenu;
            }

            //Ends<<<
            ViewBag.TemplateStatus = SubSection.TemplateSections.Templates.TemplateStatus;


            ViewBag.TemplateType = SubSection.TemplateSections.Templates.DefaultTemplate == true ? 1 : 0;//To Know Template Type is standard or Custom

            ViewBag.SectionId = SubSection.TemplateSectionID;
            ViewBag.TemplateName = SubSection.TemplateSections.Templates.TemplateName;
            ViewBag.SectionName = SubSection.TemplateSections.SectionName;
            ViewBag.TemplateSectionType = SubSection.TemplateSections.TemplateSectionType;
            ViewBag.SubSectionName = SubSection.SubSectionName;

            ViewBag.SubSectionType = SubSection.SubSectionType;
            ViewBag.Condition = SubSection.SubSectionTypeCondition;

            ViewBag.SubSections = (from subsections in SubSection.TemplateSections.TemplateSubSections.ToList()
                                   select new SelectListItem
                                   {
                                       Text = subsections.SubSectionName,
                                       Value = subsections.SubSectionID + "",
                                       Selected = SubSectionId == subsections.SubSectionID
                                   }).ToList();

            ViewBag.SubSectionId = SubSectionId;
            ViewBag.QuestionsBankList = entityDB.QuestionsBank.Where(rec => rec.QuestionReference == 2 && rec.QuestionStatus == 1 && rec.UsedFor == 0 && rec.QuestionBankId != 1000).ToList();

            ViewBag.QuestionsList = entityDB.Questions.Where(rec => rec.SubSectionId == SubSectionId).OrderBy(rec => rec.DisplayOrder).ToList();//Rajesh on 11/25/2013
            return View();
        }

        ////Rajesh on 10/25/2013
        //[SessionExpireForView]
        //public ActionResult AddOrEditQuestion(long QuestionId, long SubSectionId)
        //{
        //    LocalQuestionsModel question = new LocalQuestionsModel();
        //    question.QuestionID = QuestionId;
        //    question.SubSectionId = SubSectionId;
        //    ViewBag.Controls = (from controls in entityDB.QuestionControlType.Where(rec=>rec.Visible==true).ToList()
        //                        select new SelectListItem()
        //                        {
        //                            Text = controls.QuestionTypeDisplayName,
        //                            Value = controls.QuestionTypeID + ""
        //                        }).ToList();
        //    return View(question);
        //}
        [HttpPost]
        public string AddQuestionBank(string QuestionString)
        {
            var currentQuestionRecord = entityDB.QuestionsBank.FirstOrDefault(record => record.QuestionString.ToUpper() == QuestionString.ToUpper() && record.QuestionReference == 2 && record.UsedFor == 0); // Kiran on 7/3/2014
            if (currentQuestionRecord != null)
            {
                ViewBag.isQuestionExits = true;
                return "Question being added, exist in question bank";
            }
            QuestionsBank Question = new QuestionsBank();
            Question.QuestionString = QuestionString;
            Question.QuestionReference = 2;
            entityDB.QuestionsBank.Add(Question);
            Question.UsedFor = 0;
            Question.QuestionStatus = 1;

            entityDB.SaveChanges();

            return "Ok";
        }
        //Rajesh on 10/25/2013
        [HttpPost]
        public string AddOrEditQuestion(string QuestionText, long SubSectionId, int NumberOfColumns, long QuestionBankId, long QuestionId)
        {
            var subSection = entityDB.TemplateSubSections.Find(SubSectionId);
            int questionOrderNumber = 1;
            try { questionOrderNumber = subSection.Questions.Count() + 1; }
            catch { }
            Questions question;
            long questionId = -1;
            if (QuestionId == -1)
            {
                var SubSection = entityDB.TemplateSubSections.Find(SubSectionId);
                question = new Questions();
                question.QuestionText = QuestionText;
                question.SubSectionId = SubSectionId;
                question.DisplayOrder = questionOrderNumber;
                question.Visible = true;
                question.NumberOfColumns = NumberOfColumns;
                if (SubSection.SubSectionTypeCondition == "BiddingReporting" || SubSection.SubSectionTypeCondition == "ProficiencyReporting" || SubSection.SubSectionTypeCondition == "ClaycoBiddingReporting")
                {
                    question.IsMandatory = SubSection.Questions == null || SubSection.Questions.Count() == 0;
                }
                else
                    question.IsMandatory = true;
                question.IsBold = true;
                question.ResponseRequired = true;
                question.DisplayResponse = true;
                question.HasDependantQuestions = false;
                question.QuestionBankId = QuestionBankId;
                entityDB.Questions.Add(question);
                questionId = question.QuestionID;

                //Rajesh on 11/23/2013


                if (SubSection.SubSectionType == 4 && SubSection.SubSectionTypeCondition == "BiddingReporting")
                {
                    question.QuestionColumnDetails = new List<QuestionColumnDetails>();
                    QuestionColumnDetails details = new QuestionColumnDetails();
                    details.ColumnNo = 1;

                    details.DisplayType = 0;
                    details.QuestionControlTypeId = 9;
                    details.QuestionId = question.QuestionID;

                    details.ColumnValues = "";
                    details.TableReference = "BiddingInterests";

                    details.TableReferenceField = "BiddingInterestId";
                    details.TableRefFieldValueDisplay = "BiddingInterestName";
                    details.SortReferenceField = "BiddingInterestName";//Rajesh on 12/31/2014


                    question.QuestionColumnDetails.Add(details);
                }
                if (SubSection.SubSectionType == 4 && SubSection.SubSectionTypeCondition == "ClaycoBiddingReporting")
                {
                    question.QuestionColumnDetails = new List<QuestionColumnDetails>();
                    QuestionColumnDetails details = new QuestionColumnDetails();
                    details.ColumnNo = 1;

                    details.DisplayType = 0;
                    details.QuestionControlTypeId = 9;
                    details.QuestionId = question.QuestionID;

                    details.ColumnValues = "";
                    details.TableReference = "ClaycoBiddingInterests";

                    details.TableReferenceField = "BiddingInterestId";
                    details.TableRefFieldValueDisplay = "BiddingInterestName";
                    details.SortReferenceField = "BiddingInterestName";//Rajesh on 12/31/2014


                    question.QuestionColumnDetails.Add(details);
                }
            }
            else
            {
                question = entityDB.Questions.Find(QuestionId);
                question.HasDependantQuestions = false;//Rajesh on 3/1/2014                
                if (NumberOfColumns != question.NumberOfColumns && question.QuestionColumnDetails != null)
                {
                    foreach (var QColumn in question.QuestionColumnDetails.ToList())
                        entityDB.Entry(QColumn).State = EntityState.Deleted;
                }
                question.NumberOfColumns = NumberOfColumns;
                questionId = question.QuestionID;

                entityDB.Entry(question).State = EntityState.Modified;
            }

            entityDB.SaveChanges();
            return "" + question.QuestionID;
        }
        //<<Ends 
        [SessionExpire]
        [HeaderAndSidebar(headerName = "QuestionnaireTool", sideMenuName = "TemplateTool", ViewName = "AddOrEditPreSubSections")]
        public ActionResult AddOrEditPreSubSections(long SectionId)
        {

            var section = entityDB.TemplateSections.Find(SectionId);

            //To Change Side Menu Selection>>>
            if (section.Templates.DefaultTemplate == null || section.Templates.DefaultTemplate == false)
            {
                List<LocalHeaderMenuModel> headerMenu = (List<LocalHeaderMenuModel>)ViewBag.headerMenus;
                headerMenu.FirstOrDefault(record => record.menuName == "QuestionnaireTool").sideMenus.ForEach(record => record.isChecked = false);
                headerMenu.FirstOrDefault(record => record.menuName == "QuestionnaireTool").sideMenus.FirstOrDefault(record => record.menuName == "CustomTemplateTool").isChecked = true;
                ViewBag.headerMenus = headerMenu;
            }

            //Ends<<<

            ViewBag.Section = section;
            ViewBag.SectionType = section.TemplateSectionType;
            ViewBag.templateId = section.Templates.TemplateID;
            ViewBag.Clients = entityDB.Organizations.Where(rec => rec.OrganizationType == "Client").OrderBy(rec => rec.Name).ToList();
            // Kiran on 12/21/2013

            if (section.Templates.DefaultTemplate == false)
            {
                var clientId = section.Templates.ClientTemplates.FirstOrDefault().ClientID;
                ViewBag.Clients = entityDB.Organizations.Where(rec => rec.OrganizationType == "Client" && rec.OrganizationID == clientId).ToList();
            }
            // Ends<<<

            ViewBag.TemplateName = section.Templates.TemplateName;
            ViewBag.SectionName = section.SectionName;//Rajesh on 10/23/2013

            ViewBag.SectionId = SectionId;
            return View();
        }
        [HttpPost]
        public string SaveBiddings(long clientId, Guid TemplateId, string clientTemplateData, long SectionId)
        {
            var clientTemplate = entityDB.ClientTemplates.FirstOrDefault(rec => rec.ClientID == clientId && TemplateId == rec.TemplateID);
            var Section = entityDB.TemplateSections.Find(SectionId);//Rajesh on 2/24/2014
            var SectionType = entityDB.TemplateSections.Find(SectionId).TemplateSectionType;
            //If user selects none of the clients(-1)...
            //if (clientId == -1)
            //{
            //    return "Ok";
            //}

            if (clientTemplate == null)
            {
                clientTemplate = new ClientTemplates();
                clientTemplate.ClientID = clientId;
                clientTemplate.TemplateID = TemplateId;
                clientTemplate.Templates = entityDB.Templates.Find(TemplateId);
                entityDB.ClientTemplates.Add(clientTemplate);

            }
            else
            {

                if (SectionType == 6 || SectionType == 8)//Bidding
                {
                    int rptType = 0;
                    if (SectionType == 8)
                    {
                        rptType = 3;
                    }
                    foreach (var CLRData in entityDB.ClientTemplateReportingData.Where(rec => rec.ReportingType == 0 && rec.ClientTemplateId == clientTemplate.ClientTemplateID).ToList())
                    {
                        if (clientTemplate.Templates.TemplateStatus == 0)//If it is in process direct delete
                            entityDB.Entry(CLRData).State = EntityState.Deleted;
                        // Kiran on 10/6/2014
                        else if (entityDB.PrequalificationReportingData.FirstOrDefault(rec => rec.ReportingType == 0 && rec.ReportingDataId == CLRData.ReportingTypeId && rec.ClientId == clientId) == null)//else if not exist in prequalificationreporting data
                            entityDB.Entry(CLRData).State = EntityState.Deleted;
                        else if (entityDB.PrequalificationReportingData.FirstOrDefault(rec => rec.ReportingType == 0 && rec.ReportingDataId == CLRData.ReportingTypeId && rec.ClientId == clientId) != null)
                        {
                            CLRData.ReportingStatus = false;
                            entityDB.Entry(CLRData).State = EntityState.Modified;
                        }
                        // Ends<<<
                    }

                }

                else if (SectionType == 4)//Supporting Documents
                {
                    foreach (var CLRData in entityDB.ClientTemplateReportingData.Where(rec => rec.ReportingType == 2 && rec.ClientTemplateId == clientTemplate.ClientTemplateID && rec.SectionId == SectionId).ToList())
                    {
                        if (clientTemplate.Templates.TemplateStatus == 0)//If it is in process direct delete
                            entityDB.Entry(CLRData).State = EntityState.Deleted;
                        // Kiran on 10/6/2014
                        else if (entityDB.PrequalificationReportingData.FirstOrDefault(rec => rec.ReportingType == 2 && rec.ReportingDataId == CLRData.ReportingTypeId && rec.ClientId == clientId) == null)//else if not exist in prequalificationreporting data
                            entityDB.Entry(CLRData).State = EntityState.Deleted;
                        else if (entityDB.PrequalificationReportingData.FirstOrDefault(rec => rec.ReportingType == 2 && rec.ReportingDataId == CLRData.ReportingTypeId && rec.ClientId == clientId) != null)
                        {
                            CLRData.ReportingStatus = false;
                            entityDB.Entry(CLRData).State = EntityState.Modified;
                        }
                        // Ends<<<
                    }
                }
                else//5-Proficiency
                {
                    foreach (var CLRData in entityDB.ClientTemplateReportingData.Where(rec => rec.ReportingType == 1 && rec.ClientTemplateId == clientTemplate.ClientTemplateID).ToList())
                    {
                        if (clientTemplate.Templates.TemplateStatus == 0)//If it is in process direct delete
                            entityDB.Entry(CLRData).State = EntityState.Deleted;
                        // Kiran on 10/6/2014
                        else if (entityDB.PrequalificationReportingData.FirstOrDefault(rec => rec.ReportingType == 1 && rec.ReportingDataId == CLRData.ReportingTypeId && rec.ClientId == clientId) == null)//else if not exist in prequalificationreporting data
                            entityDB.Entry(CLRData).State = EntityState.Deleted;
                        else if (entityDB.PrequalificationReportingData.FirstOrDefault(rec => rec.ReportingType == 1 && rec.ReportingDataId == CLRData.ReportingTypeId && rec.ClientId == clientId) != null)//else if not exist in prequalificationreporting data
                        {
                            CLRData.ReportingStatus = false;
                            entityDB.Entry(CLRData).State = EntityState.Modified;
                        }
                        // Ends<<<
                    }
                }
            }
            foreach (var clientBiddings in clientTemplateData.Split(','))
            {
                try
                {
                    ClientTemplateReportingData report = new ClientTemplateReportingData();
                    report.ClientTemplateId = clientTemplate.ClientTemplateID;
                    report.ReportingStatus = true; // Kiran on 10/6/2014
                    if (SectionType == 6)//Bidding //
                        report.ReportingType = 0;//Bidding
                    else if (SectionType == 8)//Clayco Bidding Type
                    {
                        report.ReportingType = 3;
                    }
                    else if (SectionType == 4)//Supporting Doc
                    {
                        report.ReportingType = 2;
                    }
                    else
                        report.ReportingType = 1;//Proficiency & Capabilities

                    if (SectionType == 4)//It gets id_DocId|DocExpires|docMandatory // kiran on 8/5/2014
                    {
                        report.ReportingTypeId = Convert.ToInt64(clientBiddings.Split('|')[0].Substring(3));
                        report.DocumentExpires = Convert.ToBoolean(clientBiddings.Split('|')[2]); // Kiran on 8/5/2014
                        report.DocIsMandatory = Convert.ToBoolean(clientBiddings.Split('|')[1]); // Kiran on 8/5/2014
                        report.SectionId = SectionId;
                    }
                    else
                        report.ReportingTypeId = Convert.ToInt64(clientBiddings.Substring(3));
                    if (clientTemplate.Templates.TemplateStatus == 0)
                        entityDB.ClientTemplateReportingData.Add(report);
                    else if (entityDB.PrequalificationReportingData.FirstOrDefault(rec => rec.ReportingType == report.ReportingType && rec.ReportingDataId == report.ReportingTypeId && rec.ClientId == clientId) == null)//else if not exist in prequalificationreporting data
                    {
                        entityDB.ClientTemplateReportingData.Add(report);
                    }
                    else if (entityDB.ClientTemplateReportingData.FirstOrDefault(rec => rec.ReportingType == report.ReportingType && rec.ReportingTypeId == report.ReportingTypeId && rec.ClientTemplateId == report.ClientTemplateId) == null)//else if not exist in ClientTemplateReportingData data
                    {
                        entityDB.ClientTemplateReportingData.Add(report);
                    }
                }
                catch { }
            }

            //Rajesh on 1/30/2014
            //IF Template(Custom) was Approved Then Insert Every thing in ClientTemplate For BU
            if (clientTemplate.Templates.TemplateStatus == 1)
            {
                foreach (var client in clientTemplate.Organizations.ClientBusinessUnits.ToList())
                {
                    if (entityDB.ClientTemplatesForBU.FirstOrDefault(rec => rec.ClientBusinessUnitId == client.ClientBusinessUnitId && rec.ClientTemplateId == clientTemplate.ClientTemplateID) == null)
                    {
                        ClientTemplatesForBU currentClientTemplateBU = new ClientTemplatesForBU();
                        currentClientTemplateBU.ClientTemplateId = clientTemplate.ClientTemplateID;
                        currentClientTemplateBU.ClientBusinessUnitId = client.ClientBusinessUnitId;
                        entityDB.ClientTemplatesForBU.Add(currentClientTemplateBU);

                    }
                }

            }

            entityDB.SaveChanges();
            if (clientTemplate.Templates.DefaultTemplate == true)//If it is standard template and it contains no chailds clienttemplate reporting data remove ClientTemplate
            {
                if (clientTemplate.ClientTemplateReportingData.ToList().Count == 0)
                {
                    entityDB.ClientTemplates.Remove(clientTemplate);
                    entityDB.SaveChanges();
                }
            }

            //Ends

            if (SectionType == 6 || SectionType == 8)//Biddings has one default subsec..
            {

                TemplateSubSections SubSec = entityDB.TemplateSubSections.FirstOrDefault(rec => rec.TemplateSectionID == SectionId && rec.ClientId == clientId);
                if (SubSec == null)
                {
                    SubSec = new TemplateSubSections();
                    SubSec.ClientId = clientTemplate.ClientID;
                    SubSec.DisplayOrder = 1;
                    SubSec.NoOfColumns = 1;
                    SubSec.SubSectionName = "Add Bidding Interests";
                    SubSec.SubSectionType = 0;
                    SubSec.TemplateSectionID = SectionId;
                    SubSec.Visible = true;
                    SubSec.SubSectionType = 4;
                    SubSec.SubSectionTypeCondition = "BiddingReporting";
                    SubSec.Questions = new List<Questions>();
                    entityDB.TemplateSubSections.Add(SubSec);
                    CreateTemplateSubSecPermission(0, SubSec, Section);//Rajesh on 2/24/2014

                }
                else
                {
                    foreach (var question in SubSec.Questions.ToList())//Delete all questions related to section--------- and insert again
                    {
                        if (question.QuestionColumnDetails.FirstOrDefault().PrequalificationUserInputs.Count > 0)//If this question have data(Prequalification User Input)
                        {
                            question.Visible = false;
                        }
                        else
                        {
                            foreach (var QcolumnDetails in question.QuestionColumnDetails.ToList())
                            {
                                entityDB.Entry(QcolumnDetails).State = EntityState.Deleted;
                            }
                            entityDB.Entry(question).State = EntityState.Deleted;
                        }
                    }
                }
                foreach (var clientBiddings in clientTemplateData.Split(','))
                {
                    try
                    {
                        long BiddingId = Convert.ToInt64(clientBiddings.Substring(3));
                        var BiddingName = entityDB.BiddingInterests.Find(BiddingId).BiddingInterestName;

                        QuestionsBank QBank = entityDB.QuestionsBank.FirstOrDefault(rec => rec.QuestionReference == 0 && rec.QuestionReferenceId == BiddingId);
                        if (QBank == null)
                        {

                            QBank = new QuestionsBank();
                            QBank.QuestionReference = 0;
                            QBank.QuestionReferenceId = BiddingId;
                            QBank.QuestionString = BiddingName;

                            QBank.UsedFor = 0;
                            QBank.QuestionStatus = 1;

                            Questions question = new Questions();
                            question.QuestionText = BiddingName;
                            question.DisplayOrder = 1;
                            question.DisplayResponse = true;
                            question.NumberOfColumns = 1;
                            question.QuestionBankId = QBank.QuestionBankId;
                            question.ResponseRequired = true;
                            question.SubSectionId = SubSec.SubSectionID;
                            question.Visible = true;


                            question.QuestionColumnDetails = new List<QuestionColumnDetails>();

                            QuestionColumnDetails QColumnDetails = new QuestionColumnDetails();
                            QColumnDetails.ColumnNo = 1;
                            QColumnDetails.ColumnValues = "Yes|No|Not Applicable";//Rajesh on 2/24/2014
                            QColumnDetails.DisplayType = 0;
                            QColumnDetails.QuestionControlTypeId = 7;

                            question.QuestionColumnDetails.Add(QColumnDetails);

                            QBank.Questions = new List<Questions>();
                            QBank.Questions.Add(question);


                            entityDB.QuestionsBank.Add(QBank);
                        }
                        else
                        {
                            //Rajesh on 10/6/2014
                            Questions QuestionExist = null; try { QuestionExist = SubSec.Questions.FirstOrDefault(rec => rec.QuestionBankId == QBank.QuestionBankId); }
                            catch { }
                            if (QuestionExist == null)
                            {
                                Questions question = new Questions();
                                question.QuestionText = BiddingName;
                                question.DisplayOrder = 1;
                                question.DisplayResponse = true;
                                question.NumberOfColumns = 1;
                                question.QuestionBankId = QBank.QuestionBankId;
                                question.ResponseRequired = true;
                                question.SubSectionId = SubSec.SubSectionID;
                                question.Visible = true;


                                question.QuestionColumnDetails = new List<QuestionColumnDetails>();

                                QuestionColumnDetails QColumnDetails = new QuestionColumnDetails();
                                QColumnDetails.ColumnNo = 1;
                                QColumnDetails.ColumnValues = "Yes|No|Not Applicable";//Rajesh on 2/24/2014
                                QColumnDetails.DisplayType = 0;
                                QColumnDetails.QuestionControlTypeId = 7;

                                question.QuestionColumnDetails.Add(QColumnDetails);
                                entityDB.Questions.Add(question);
                            }
                            else
                            {
                                QuestionExist.Visible = true;
                                entityDB.Entry(QuestionExist).State = EntityState.Modified;
                            }
                            //Ends<<
                        }

                    }
                    catch (Exception ee)
                    {
                    }
                }
            }
            else if (SectionType == 5)//Add Subsecs for proficiency(For Distinct Category)
            {

                var CategoryName = "";
                var SubCategryName = "";

                foreach (var subSec in entityDB.TemplateSections.Find(SectionId).TemplateSubSections.Where(rec => rec.TemplateSectionID == SectionId && rec.ClientId == clientId).ToList())
                {
                    var isSubSecVisible = true;
                    foreach (var questions in subSec.Questions.ToList())
                    {
                        // Kiran on 10/6/2014
                        if (questions.QuestionColumnDetails.FirstOrDefault().PrequalificationUserInputs.Count > 0)//If this question have data(Prequalification User Input)
                        {
                            questions.Visible = false;
                            isSubSecVisible = false;
                        }
                        else
                        {
                            foreach (var QCol in questions.QuestionColumnDetails.ToList())
                            {
                                entityDB.Entry(QCol).State = EntityState.Deleted;
                            }
                            entityDB.Entry(questions).State = EntityState.Deleted;
                        }
                        // Ends<<<
                    }
                    if (isSubSecVisible == true)
                    {
                        foreach (var permission in subSec.TemplateSubSectionsPermissions.ToList())
                        {
                            entityDB.Entry(permission).State = EntityState.Deleted;
                        }
                        entityDB.Entry(subSec).State = EntityState.Deleted;
                    }
                    else
                    {
                        if (subSec.Questions.FirstOrDefault(rec => rec.Visible == true) == null)
                        {
                            subSec.Visible = false;
                            entityDB.Entry(subSec).State = EntityState.Modified;
                        }
                    }
                }

                entityDB.SaveChanges();
                //List<long> selectedProficiency = new List<long>();
                //foreach (var clientProficiency in clientTemplateData.Split(','))
                //{
                //    long proficiencyId = Convert.ToInt64(clientProficiency.Substring(3));
                //    selectedProficiency.Add(proficiencyId);
                //}
                //entityDB.ProficiencyCapabilities.Where(rec => selectedProficiency.Contains((long)rec.ProficiencyId)).ToList().GroupBy(rec=>rec.ProficiencyCategory).th;

                TemplateSubSections SubSubSec = new TemplateSubSections();
                TemplateSubSections SubSec = new TemplateSubSections();
                foreach (var clientBiddings in clientTemplateData.Split(','))
                {
                    try
                    {

                        //clientBiddings.Substring(3)--Proficiency Id
                        long proficiencyId = Convert.ToInt64(clientBiddings.Substring(3));
                        var Pcategory = entityDB.ProficiencyCapabilities.Find(proficiencyId);
                        if (Pcategory.ProficiencyCategory != CategoryName)
                        {

                            //var subSec=entityDB.TemplateSubSections.FirstOrDefault(rec => rec.TemplateSectionID == SectionId && rec.SubSectionName == Pcategory.ProficiencyCategory);
                            //if (subSec == null)//
                            {

                                SubSec = new TemplateSubSections();
                                SubSec.ClientId = clientTemplate.ClientID;
                                SubSec.DisplayOrder = 1;
                                SubSec.NoOfColumns = 1;
                                SubSec.SubSectionName = Pcategory.ProficiencyCategory;
                                SubSec.SubSectionType = 0;
                                SubSec.TemplateSectionID = SectionId;
                                SubSec.Visible = true;
                                SubSec.SubSectionType = 4;
                                SubSec.SubSectionTypeCondition = "ProficiencyReporting";
                                var DBSubSec = entityDB.TemplateSections.Find(SectionId).TemplateSubSections.FirstOrDefault(rec => rec.SubSectionName == Pcategory.ProficiencyCategory);
                                if (DBSubSec == null || DBSubSec.SubSectionID == 0)
                                {
                                    entityDB.TemplateSubSections.Add(SubSec);
                                    CreateTemplateSubSecPermission(0, SubSec, Section);//Rajesh on 2/24/2014
                                }
                                else
                                {
                                    DBSubSec.Visible = true;
                                    entityDB.Entry(DBSubSec).State = EntityState.Modified;
                                    SubSec = DBSubSec;
                                }
                                if (!string.IsNullOrEmpty(Pcategory.ProficiencySubCategroy))
                                {
                                    SubSubSec = new TemplateSubSections();
                                    SubSubSec.ClientId = clientTemplate.ClientID;
                                    SubSubSec.DisplayOrder = 1;
                                    SubSubSec.NoOfColumns = 1;
                                    SubSubSec.SubSectionName = Pcategory.ProficiencySubCategroy;
                                    SubSubSec.SubSectionType = 1;
                                    SubSubSec.TemplateSectionID = SectionId;
                                    SubSubSec.Visible = true;
                                    SubSubSec.SubHeader = true;
                                    SubSubSec.SubSectionType = 4;
                                    SubSubSec.SubSectionTypeCondition = "ProficiencyReporting";
                                    SubSubSec.Questions = new List<Questions>();
                                    var DBSubSubSec = entityDB.TemplateSections.Find(SectionId).TemplateSubSections.FirstOrDefault(rec => rec.SubSectionName == Pcategory.ProficiencySubCategroy);
                                    if (DBSubSubSec == null || DBSubSubSec.SubSectionID == 0)
                                    {
                                        entityDB.TemplateSubSections.Add(SubSubSec);
                                        CreateTemplateSubSecPermission(0, SubSubSec, Section);//Rajesh on 2/24/2014
                                    }
                                    else
                                    {
                                        DBSubSubSec.Visible = true;
                                        entityDB.Entry(DBSubSubSec).State = EntityState.Modified;
                                        SubSubSec = DBSubSubSec;
                                    }
                                }
                                else
                                {
                                    SubSubSec = SubSec;
                                }
                                entityDB.SaveChanges();

                                AddQuestionForProficiencyCapability(SubSubSec, proficiencyId, Pcategory);

                            }
                            //else
                            //{
                            //    subSec.Visible = true;
                            //    entityDB.Entry(subSec).State=EntityState.Modified;

                            //    TemplateSubSections SubSubSec = new TemplateSubSections();
                            //    SubSubSec.ClientId = clientTemplate.ClientID;
                            //    SubSubSec.DisplayOrder = 0;
                            //    SubSubSec.NoOfColumns = 1;
                            //    SubSubSec.SubSectionName = Pcategory.ProficiencySubCategroy;
                            //    SubSubSec.SubSectionType = 0;
                            //    SubSubSec.TemplateSectionID = SectionId;
                            //    SubSubSec.Visible = true;
                            //    SubSubSec.SubSectionType = 4;
                            //    SubSubSec.SubSectionTypeCondition = "ProficiencyReporting";

                            //    entityDB.TemplateSubSections.Add(SubSubSec);
                            //}
                            CategoryName = Pcategory.ProficiencyCategory;
                            SubCategryName = Pcategory.ProficiencySubCategroy;
                        }
                        else
                        {
                            if (SubCategryName != Pcategory.ProficiencySubCategroy)//If Section is empty we need to insert question in category (so subsubsec=subsec)
                            {
                                if (!string.IsNullOrEmpty(Pcategory.ProficiencySubCategroy))
                                {
                                    SubSubSec = new TemplateSubSections();
                                    SubSubSec.ClientId = clientTemplate.ClientID;
                                    SubSubSec.DisplayOrder = 1;
                                    SubSubSec.NoOfColumns = 1;
                                    SubSubSec.SubSectionName = Pcategory.ProficiencySubCategroy;
                                    SubSubSec.SubSectionType = 1;
                                    SubSubSec.TemplateSectionID = SectionId;
                                    SubSubSec.SubHeader = true;
                                    SubSubSec.Visible = true;
                                    SubSubSec.SubSectionType = 4;
                                    SubSubSec.SubSectionTypeCondition = "ProficiencyReporting";
                                    SubSubSec.Questions = new List<Questions>();

                                    SubCategryName = Pcategory.ProficiencySubCategroy;


                                    var DBSubSubSec = entityDB.TemplateSections.Find(SectionId).TemplateSubSections.FirstOrDefault(rec => rec.SubSectionName == Pcategory.ProficiencySubCategroy);
                                    if (DBSubSubSec == null || DBSubSubSec.SubSectionID == 0)
                                    {
                                        entityDB.TemplateSubSections.Add(SubSubSec);
                                        CreateTemplateSubSecPermission(0, SubSubSec, Section);//Rajesh on 2/24/2014
                                    }
                                    else
                                    {
                                        DBSubSubSec.Visible = true;
                                        entityDB.Entry(DBSubSubSec).State = EntityState.Modified;
                                        SubSubSec = DBSubSubSec;
                                    }


                                }
                                else
                                {
                                    SubSubSec = SubSec;
                                }

                                AddQuestionForProficiencyCapability(SubSubSec, proficiencyId, Pcategory);
                            }
                            else
                            {
                                AddQuestionForProficiencyCapability(SubSubSec, proficiencyId, Pcategory);
                            }
                        }

                    }
                    catch { }
                    entityDB.SaveChanges();
                }
            }

            entityDB.SaveChanges();
            return "Ok";
        }

        private void AddQuestionForProficiencyCapability(TemplateSubSections SubSubSec, long proficiencyId, ProficiencyCapabilities Pcategory)
        {
            QuestionsBank QBank = entityDB.QuestionsBank.FirstOrDefault(rec => rec.QuestionReference == 1 && rec.QuestionReferenceId == proficiencyId);
            if (QBank == null)
            {

                QBank = new QuestionsBank();
                QBank.QuestionReference = 1;
                QBank.QuestionReferenceId = proficiencyId;
                QBank.QuestionString = Pcategory.ProficiencyName;
                QBank.UsedFor = 0;
                QBank.QuestionStatus = 1;

                entityDB.QuestionsBank.Add(QBank);

                Questions question = new Questions();
                question.QuestionText = Pcategory.ProficiencyName;
                question.DisplayOrder = 1;
                question.DisplayResponse = true;
                question.NumberOfColumns = 1;
                question.QuestionBankId = QBank.QuestionBankId;
                question.ResponseRequired = true;
                question.SubSectionId = SubSubSec.SubSectionID;
                question.Visible = true;

                question.QuestionBankId = QBank.QuestionBankId;

                question.QuestionColumnDetails = new List<QuestionColumnDetails>();

                QuestionColumnDetails QColumnDetails = new QuestionColumnDetails();
                QColumnDetails.ColumnNo = 1;
                QColumnDetails.ColumnValues = "Yes|No|Not Applicable";//Rajesh on 2/24/2014
                QColumnDetails.DisplayType = 0;
                QColumnDetails.QuestionControlTypeId = 7;

                question.QuestionColumnDetails.Add(QColumnDetails);

                QBank.Questions = new List<Questions>();
                QBank.Questions.Add(question);

                entityDB.Questions.Add(question);
                //SubSubSec.Questions.Add(question);
                //entityDB.QuestionsBank.Add(QBank);
            }
            else
            {
                //Rajesh on 10/6/2014
                Questions QuestionExist = null;
                try { QuestionExist = SubSubSec.Questions.FirstOrDefault(rec => rec.QuestionBankId == QBank.QuestionBankId); }
                catch { }
                if (QuestionExist == null)
                {
                    Questions question = new Questions();
                    question.QuestionText = Pcategory.ProficiencyName;
                    question.DisplayOrder = 1;
                    question.DisplayResponse = true;
                    question.NumberOfColumns = 1;
                    question.QuestionBankId = QBank.QuestionBankId;
                    question.ResponseRequired = true;
                    question.SubSectionId = SubSubSec.SubSectionID;
                    question.Visible = true;


                    question.QuestionColumnDetails = new List<QuestionColumnDetails>();

                    QuestionColumnDetails QColumnDetails = new QuestionColumnDetails();
                    QColumnDetails.ColumnNo = 1;
                    QColumnDetails.ColumnValues = "Yes|No|Not Applicable";//Rajesh on 2/24/2014
                    QColumnDetails.DisplayType = 0;
                    QColumnDetails.QuestionControlTypeId = 7;

                    question.QuestionColumnDetails.Add(QColumnDetails);
                    entityDB.Questions.Add(question);
                }
                else
                {
                    QuestionExist.Visible = true;
                    entityDB.Entry(QuestionExist).State = EntityState.Modified;
                }
                //Ends<<<
            }
        }
        //Rajesh on 10/23/2013 >>>
        [HttpPost]
        public ActionResult GetBiddings(long clientOrgId, Guid templateId, long SectionId)
        {

            var Section = entityDB.TemplateSections.Find(SectionId);
            if (Section.TemplateSectionType == 6)//Biddings
            {
                var curretBiddings = (from bidding in entityDB.BiddingInterests.Where(rec => rec.Status == 1).ToList()
                                      select new BiddingCheckBoxes
                                      {
                                          id = bidding.BiddingInterestId + "",
                                          Text = bidding.BiddingInterestName + "",
                                          Header = "",
                                          type = 0
                                      }).ToList();
                try
                {
                    var clientTemplateId = entityDB.ClientTemplates.FirstOrDefault(rec => rec.ClientID == clientOrgId && rec.TemplateID == templateId).ClientTemplateID;
                    var clientTemplateBiddings = entityDB.ClientTemplateReportingData.ToList().Where(rec => rec.ReportingType == 0 && rec.ClientTemplateId == clientTemplateId);
                    foreach (var Cbiddings in clientTemplateBiddings)
                    {
                        try
                        {
                            if (Cbiddings.ReportingStatus == true) // Kiran on 10/6/2014
                                curretBiddings.FirstOrDefault(rec => rec.id == Cbiddings.ReportingTypeId + "").status = "true";
                        }
                        catch { }
                    }
                }
                catch (Exception ee)
                {
                    //Means no client template
                }
                return Json(curretBiddings);
            }
            if (Section.TemplateSectionType == 8)//Clayco Biddings
            {
                var curretBiddings = (from bidding in entityDB.ClaycoBiddingInterests.Where(rec => rec.Status == 1).ToList()
                                      select new BiddingCheckBoxes
                                      {
                                          id = bidding.BiddingInterestId + "",
                                          Text = bidding.BiddingInterestName + "",
                                          Header = "",
                                          type = 0
                                      }).ToList();
                try
                {
                    var clientTemplateId = entityDB.ClientTemplates.FirstOrDefault(rec => rec.ClientID == clientOrgId && rec.TemplateID == templateId).ClientTemplateID;
                    var clientTemplateBiddings = entityDB.ClientTemplateReportingData.ToList().Where(rec => rec.ReportingType == 3 && rec.ClientTemplateId == clientTemplateId);
                    foreach (var Cbiddings in clientTemplateBiddings)
                    {
                        try
                        {
                            if (Cbiddings.ReportingStatus == true) // Kiran on 10/6/2014
                                curretBiddings.FirstOrDefault(rec => rec.id == Cbiddings.ReportingTypeId + "").status = "true";
                        }
                        catch { }
                    }
                }
                catch (Exception ee)
                {
                    //Means no client template
                }
                return Json(curretBiddings);
            }
            else if (Section.TemplateSectionType == 4)//For Documents
            {
                var clientTemplateId = entityDB.ClientTemplates.FirstOrDefault(rec => rec.ClientID == clientOrgId && rec.TemplateID == templateId).ClientTemplateID;
                var otherSectionBiddings = entityDB.ClientTemplateReportingData.ToList().Where(rec => rec.ReportingType == 2 && rec.ClientTemplateId == clientTemplateId && rec.SectionId != SectionId).Select(r => r.ReportingTypeId);
                var curretBiddings = (from documents in entityDB.DocumentType.Where(rec => rec.PrequalificationUseOnly == true && !otherSectionBiddings.Contains(rec.DocumentTypeId)).ToList()
                                      select new BiddingCheckBoxes
                                      {
                                          id = documents.DocumentTypeId + "",
                                          Text = documents.DocumentTypeName + "",
                                          Header = "",
                                          type = 0,
                                          isExpire = false,
                                          isDocMandatory = false // kiran on 8/5/2014

                                      }).OrderBy(r => r.Text).ToList();
                try
                {

                    var clientTemplateBiddings = entityDB.ClientTemplateReportingData.ToList().Where(rec => rec.ReportingType == 2 && rec.ClientTemplateId == clientTemplateId && rec.SectionId == SectionId);
                    foreach (var Cbiddings in clientTemplateBiddings)
                    {
                        try
                        {
                            if (Cbiddings.ReportingStatus == true) // Kiran on 10/6/2014
                                curretBiddings.FirstOrDefault(rec => rec.id == Cbiddings.ReportingTypeId + "").status = "true";
                            curretBiddings.FirstOrDefault(rec => rec.id == Cbiddings.ReportingTypeId + "").isExpire = Cbiddings.DocumentExpires == true;
                            curretBiddings.FirstOrDefault(rec => rec.id == Cbiddings.ReportingTypeId + "").isDocMandatory = Cbiddings.DocIsMandatory == true; // kiran on 8/5/2014
                        }
                        catch { }
                    }
                }
                catch (Exception ee)
                {
                    //Means no client template
                }
                return Json(curretBiddings);
            }
            else//5-Proficiency
            {
                var currentProficiencys = (from proficiency in entityDB.ProficiencyCapabilities.Where(rec => rec.Status == 1).ToList().OrderBy(rec => rec.ProficiencyCategory).ThenBy(rec => rec.ProficiencySubCategroy).ToList()
                                           select new BiddingCheckBoxes
                                           {
                                               id = proficiency.ProficiencyId + "",
                                               Header = proficiency.ProficiencyCategory,
                                               Text = (string.IsNullOrEmpty(proficiency.ProficiencySubCategroy) ? "" : proficiency.ProficiencySubCategroy + "-") + proficiency.ProficiencyName,
                                               type = 1
                                           }).ToList();
                try
                {
                    var clientTemplateId = entityDB.ClientTemplates.FirstOrDefault(rec => rec.ClientID == clientOrgId && rec.TemplateID == templateId).ClientTemplateID;
                    var clientTemplateBiddings = entityDB.ClientTemplateReportingData.ToList().Where(rec => rec.ReportingType == 1 && rec.ClientTemplateId == clientTemplateId);
                    foreach (var Cbiddings in clientTemplateBiddings)
                    {
                        try
                        {
                            if (Cbiddings.ReportingStatus == true) // Kiran on 10/6/2014
                                currentProficiencys.FirstOrDefault(rec => rec.id == Cbiddings.ReportingTypeId + "").status = "true";
                        }
                        catch { }
                    }
                }
                catch (Exception ee)
                {
                    //Means no client template
                }
                return Json(currentProficiencys);

            }
        }


        //Ends <<<


        /*
   Created Date:  10/24/2013   Created By: Kiran Talluri
   Purpose: To display the Questions Bank
*/

        [HttpGet]
        [SessionExpire]
        [HeaderAndSidebar(headerName = "QuestionnaireTool", sideMenuName = "QuestionsBank", ViewName = "QuestionsBank")]
        public ActionResult QuestionsBank()
        {
            var Questions = entityDB.QuestionsBank.Where(rec => rec.QuestionReference == 2).ToList(); // Kiran on 6/30/2014
            var QuestionsBankList = Questions.GroupBy(record => record.UsedFor).ToList(); // Kiran on 6/30/2014
            ViewBag.VBQuestionsBankList = QuestionsBankList;
            return View();
        }

        /*
           Created Date:  10/24/2013   Created By: Kiran Talluri
           Purpose: To add a question in Questions Bank
       */

        [HttpGet]
        [SessionExpireForView]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "QuestionnaireTool", sideMenuName = "QuestionsBank", ViewName = "AddQuestionInQuestionBank")]
        public ActionResult AddQuestionInQuestionBank(long QuestionBankId)
        {
            LocalAddQuestionInQuestionBank addQuestion = new LocalAddQuestionInQuestionBank();
            ViewBag.isQuestionExits = false;
            if (QuestionBankId != -1)
            {
                var currentQuestionBankrecord = entityDB.QuestionsBank.FirstOrDefault(record => record.QuestionBankId == QuestionBankId);
                addQuestion.isEdit = true;
                addQuestion.QuestionString = currentQuestionBankrecord.QuestionString;

                addQuestion.QuestionFor = (int)currentQuestionBankrecord.UsedFor; // Kiran 11/29/2013

                //Kiran on 11/23/2013
                if (currentQuestionBankrecord.QuestionStatus != null)
                {
                    int currentQuestionStatus = (int)currentQuestionBankrecord.QuestionStatus;
                    if (currentQuestionStatus == 1)
                    {
                        addQuestion.QuestionStatus = true;
                    }
                    else
                    {
                        addQuestion.QuestionStatus = false;
                    }
                }
                // Ends<<<

                return View(addQuestion);
            }
            addQuestion.isEdit = false;
            addQuestion.QuestionStatus = true;
            return View(addQuestion);
        }

        /*
           Created Date:  10/24/2013   Created By: Kiran Talluri
           Purpose: To add a question in Questions Bank
       */
        [HttpPost]
        [SessionExpireForView]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "QuestionnaireTool", sideMenuName = "QuestionsBank", ViewName = "AddQuestionInQuestionBank")]
        public ActionResult AddQuestionInQuestionBank(LocalAddQuestionInQuestionBank addQuestion)
        {
            if (ModelState.IsValid)
            {
                var currentQuestionRecord = entityDB.QuestionsBank.FirstOrDefault(record => record.QuestionString.ToUpper() == addQuestion.QuestionString.Trim().ToUpper() && record.QuestionBankId != addQuestion.QuestionBankId && record.QuestionReference == 2 && record.UsedFor == addQuestion.QuestionFor); // Kiran on 7/2/2014
                if (currentQuestionRecord != null)
                {
                    ViewBag.isQuestionExits = true;
                    return View(addQuestion);
                }

                if (addQuestion.isEdit == false)
                {
                    QuestionsBank Question = new QuestionsBank();
                    Question.QuestionString = addQuestion.QuestionString.Trim();
                    Question.QuestionReference = 2;

                    Question.UsedFor = addQuestion.QuestionFor; // Kiran 11/29/2013

                    // Kiran on 11/23/2013
                    bool QuestionStatus = addQuestion.QuestionStatus;
                    if (QuestionStatus == true)
                    {
                        Question.QuestionStatus = 1;
                    }
                    else
                    {
                        Question.QuestionStatus = 0;
                    }
                    // Ends<<<

                    entityDB.QuestionsBank.Add(Question);
                    entityDB.SaveChanges();
                }
                else
                {
                    var currentQuestionBankrecord = entityDB.QuestionsBank.FirstOrDefault(record => record.QuestionBankId == addQuestion.QuestionBankId);
                    currentQuestionBankrecord.QuestionString = addQuestion.QuestionString.Trim();
                    currentQuestionBankrecord.UsedFor = addQuestion.QuestionFor; // Kiran 11/29/2013

                    // Kiran on 11/23/2013
                    bool QuestionStatus = addQuestion.QuestionStatus;
                    if (QuestionStatus == true)
                    {
                        currentQuestionBankrecord.QuestionStatus = 1;
                    }
                    else
                    {
                        currentQuestionBankrecord.QuestionStatus = 0;
                    }
                    // Ends<<<

                    var questionTextRecordsInQuestions = currentQuestionBankrecord.Questions.ToList();

                    foreach (var question in questionTextRecordsInQuestions)
                    {
                        question.QuestionText = addQuestion.QuestionString;
                        entityDB.Entry(question).State = EntityState.Modified;
                        entityDB.SaveChanges();
                    }
                    entityDB.Entry(currentQuestionBankrecord).State = EntityState.Modified;
                    entityDB.SaveChanges();
                }
                return RedirectToAction("PopUpCloseView");
            }
            return View(addQuestion);
        }

        public ActionResult CloseQuestionBank()
        {
            return View();
        }

        //Ends<<<





        //Rajesh on 10/25/2013
        //[SessionExpireForView]
        //[HeaderAndSidebar(isPermissionRequired = true, headerName = "QuestionnaireTool", sideMenuName = "TemplateTool", ViewName = "AddOrEditQuestionColumnDetails")]
        //public ActionResult AddOrEditQuestionColumnDetails(long QuestionId)
        //{
        //    //Rajesh on 12/24/2013
        //    SelectListItem EmptyItem = new SelectListItem() { Text = "Select Type", Value = "-1" };
        //    List<SelectListItem> items = new List<SelectListItem>();
        //    items.Add(EmptyItem);
        //    items.AddRange((from controls in entityDB.QuestionControlType.Where(rec => rec.Visible == true).ToList()
        //                    select new SelectListItem
        //                    {
        //                        Text = controls.QuestionTypeDisplayName,
        //                        Value = controls.QuestionTypeID + ""
        //                    }).ToList());

        //    ViewBag.QuestionControlTypes = items;

        //    //Ends<<

        //    var Question = entityDB.Questions.Find(QuestionId);
        //    ViewBag.Question = Question;
        //    LocalQuestionColumnDetails details = new LocalQuestionColumnDetails();
        //    details.QuestionId = QuestionId;
        //    details.IsBold = (bool)Question.IsBold;
        //    details.IsMandatory = (bool)Question.IsMandatory;
        //    details.IsRepeat = Question.QuestionColumnDetails.Count() == 0;
        //    details.ResponseRequired = Question.ResponseRequired;
        //    details.Visible = !Question.Visible;
        //    details.ColumnDetails = new List<ColumnDetails>();
        //    details.HasDependent = (bool)Question.HasDependantQuestions;
        //    details.ReferenceType = Question.ReportForKeyword ?? "";
        //    details.GroupNum = Question.GroupAsOneRecord;
        //    details.HasVendorDetails = Question.VendorDetailQuestions.Any(r => r.Type == 0);
        //    if (Question.QuestionColumnDetails.Count() == 0)//Add Question Columns
        //    {
        //        for (int i = 0; i < Question.NumberOfColumns; i++)
        //        {
        //            ColumnDetails c = new ColumnDetails();
        //            c.QuestionValues = new List<string>();
        //            foreach (var allQColDetails in entityDB.QuestionColumnDetails.Where(rec => rec.Questions.SubSectionId == Question.SubSectionId && rec.QuestionId != QuestionId).GroupBy(rec => rec.QuestionId).ToList())
        //            {

        //                string question = "";//allQColDetails.Questions.QuestionText + "| Q" + allQColDetails.QuestionColumnId + " ";
        //                bool initialFlag = true;
        //                var maxRows = 1;
        //                foreach (var QcolumnDetails in allQColDetails.OrderBy(rec => rec.ColumnNo))
        //                {
        //                    int num = allQColDetails.Max(rec => rec.ColumnNo);
        //                    if (maxRows < num)
        //                        maxRows = num;

        //                    if (initialFlag)
        //                        question = QcolumnDetails.Questions.QuestionText;
        //                    initialFlag = false;
        //                    if (QcolumnDetails.QuestionControlTypeId != 3)
        //                        question += "|-";
        //                    else
        //                        question += "| Q" + QcolumnDetails.QuestionColumnId + " ";

        //                }
        //                c.MaxQuestionColumns = maxRows;
        //                c.QuestionValues.Add(question);
        //            }
        //            c.HasFormula = false;

        //            details.ColumnDetails.Add(c);


        //        }
        //    }
        //    else//Edit Question Columns
        //    {
        //        foreach (var column in Question.QuestionColumnDetails)
        //        {
        //            ColumnDetails c = new ColumnDetails();



        //            c.QuestionValues = new List<string>();
        //            var maxRows = 1;
        //            foreach (var allQColDetails in entityDB.QuestionColumnDetails.Where(rec => rec.Questions.SubSectionId == column.Questions.SubSectionId && rec.QuestionId != QuestionId).GroupBy(rec => rec.QuestionId).ToList())
        //            {
        //                int num = allQColDetails.Max(rec => rec.ColumnNo);
        //                if (maxRows < num)
        //                    maxRows = num;
        //                //string question = allQColDetails.Questions.QuestionText + "| Q" + allQColDetails.QuestionColumnId + " ";
        //                string question = "";//allQColDetails.Questions.QuestionText + "| Q" + allQColDetails.QuestionColumnId + " ";
        //                bool initialFlag = true;
        //                foreach (var QcolumnDetails in allQColDetails.OrderBy(rec => rec.ColumnNo))
        //                {
        //                    if (initialFlag)
        //                        question = QcolumnDetails.Questions.QuestionText;

        //                    if (QcolumnDetails.QuestionControlTypeId != 3)
        //                        question += "|-";
        //                    else
        //                        question += "| Q" + QcolumnDetails.QuestionColumnId + " ";
        //                    initialFlag = false;
        //                }
        //                c.QuestionValues.Add(question);
        //            }
        //            c.MaxQuestionColumns = maxRows;
        //            c.ButtonTypeReferenceValue = column.ButtonTypeReferenceValue ?? 0;
        //            c.ControlType = column.QuestionControlTypeId + "";
        //            c.DisplayType = column.DisplayType;
        //            c.HasTableReference = !string.IsNullOrWhiteSpace(column.TableReference);
        //            c.RefTableName = column.TableReference;
        //            c.Value = column.ColumnValues;
        //            c.FormulaValue = column.QFormula;
        //            if (!string.IsNullOrEmpty(column.QFormula))
        //            {
        //                c.HasFormula = true;
        //            }
        //            details.ColumnDetails.Add(c);
        //        }
        //    }
        //    return View(details);
        //}

        //[HttpPost]
        //[SessionExpireForView]
        //[HeaderAndSidebar(isPermissionRequired = true, headerName = "QuestionnaireTool", sideMenuName = "TemplateTool", ViewName = "AddOrEditQuestionColumnDetails")]
        //public ActionResult AddOrEditQuestionColumnDetails(LocalQuestionColumnDetails form)
        //{
        //    //Rajesh on 12/24/2013
        //    SelectListItem EmptyItem = new SelectListItem() { Text = "Select Type", Value = "-1" };
        //    List<SelectListItem> items = new List<SelectListItem>();
        //    items.Add(EmptyItem);
        //    items.AddRange((from controls in entityDB.QuestionControlType.Where(rec => rec.Visible == true).ToList()
        //                    select new SelectListItem
        //                    {
        //                        Text = controls.QuestionTypeDisplayName,
        //                        Value = controls.QuestionTypeID + ""
        //                    }).ToList());

        //    ViewBag.QuestionControlTypes = items;

        //    //Ends<<

        //    var Question = entityDB.Questions.Find(form.QuestionId);

        //    var errorMessage = "";

        //    ViewBag.Question = Question;

        //    Question.IsBold = form.IsBold;
        //    Question.IsMandatory = form.IsMandatory;
        //    Question.Visible = !form.Visible;
        //    Question.ResponseRequired = form.ResponseRequired;
        //    Question.HasDependantQuestions = form.HasDependent;
        //    if (string.IsNullOrEmpty(Question.ReportForKeyword) ||
        //        Question.ReportForKeyword.ToLower().Equals("vendordetails") ||
        //        Question.ReportForKeyword.ToLower().Equals("clientvendorid"))
        //    {
        //        Question.ReportForKeyword = form.ReferenceType;
        //        Question.GroupAsOneRecord = form.GroupNum;
        //    }
        //    if (form.HasVendorDetails == true)
        //    {
        //        if (!Question.VendorDetailQuestions.Any())
        //        {
        //            VendorDetailQuestions vdq = new VendorDetailQuestions();

        //            vdq.QuestionId = form.QuestionId;
        //            var templateid = Question.TemplateSubSections.TemplateSections.TemplateID;
        //            vdq.TemplateId = templateid;
        //            entityDB.VendorDetailQuestions.Add(vdq);
        //        }
        //    }
        //    else
        //    {
        //        var x = entityDB.VendorDetailQuestions.FirstOrDefault(r => r.QuestionId == form.QuestionId && r.Type == 0);
        //        if (x != null)
        //        {
        //            entityDB.VendorDetailQuestions.Remove(x);
        //        }
        //    }
        //    //Rajesh on 12/31/2013
        //    if (!form.HasDependent)//If user unselects hasDependent remove all depends
        //    {
        //        if (Question.QuestionsDependants != null)
        //        {
        //            foreach (var QDepends in Question.QuestionsDependants.ToList())
        //            {
        //                entityDB.QuestionsDependants.Remove(QDepends);
        //            }
        //        }
        //    }
        //    //Ends<<<
        //    if (Question.QuestionColumnDetails == null || !(Question.QuestionColumnDetails.Where(rec => rec.PrequalificationUserInputs.Count() > 0).Count() > 0))
        //    {
        //        foreach (var column in Question.QuestionColumnDetails.ToList())
        //        {
        //            entityDB.Entry(column).State = EntityState.Deleted;
        //        }
        //        if (form.ColumnDetails != null)
        //            for (int i = 0; i < form.ColumnDetails.Count(); i++)
        //            {
        //                if (form.IsRepeat)
        //                {
        //                    if (form.ColumnDetails[0].ControlType == "-1" || form.ColumnDetails[0].ControlType == null)//Rajesh on 1/3/2014
        //                    {
        //                        errorMessage = "Column 1 ";
        //                    }
        //                    else if (form.ColumnDetails[0].ControlType == "7" || form.ColumnDetails[0].ControlType == "10")//Radio or MultiSelect
        //                    {
        //                        if (string.IsNullOrEmpty(form.ColumnDetails[0].Value))
        //                        {
        //                            errorMessage = "Column 1 ";
        //                        }
        //                    }
        //                    else if (!form.ColumnDetails[0].HasTableReference && form.ColumnDetails[0].ControlType == "9")
        //                    {
        //                        if (string.IsNullOrEmpty(form.ColumnDetails[0].Value))
        //                        {
        //                            errorMessage = "Column 1 ";
        //                        }
        //                    }
        //                    QuestionColumnDetails details = new QuestionColumnDetails();
        //                    details.ColumnNo = i + 1;
        //                    details.ColumnValues = form.ColumnDetails[0].Value;
        //                    if (i == 0)
        //                        details.QFormula = form.ColumnDetails[0].FormulaValue;
        //                    if (form.ColumnDetails[0].HasFormula == false)
        //                    {
        //                        details.QFormula = null;
        //                    }
        //                    details.DisplayType = Convert.ToInt32(form.ColumnDetails[0].DisplayType);
        //                    details.QuestionControlTypeId = Convert.ToInt32(form.ColumnDetails[0].ControlType);
        //                    details.QuestionId = Question.QuestionID;
        //                    if (form.ColumnDetails[0].HasTableReference)
        //                    {
        //                        details.ColumnValues = "";
        //                        details.TableReference = form.ColumnDetails[0].RefTableName;
        //                        if (form.ColumnDetails[0].RefTableName == "BiddingInterests")
        //                        {
        //                            details.TableReferenceField = "BiddingInterestId";
        //                            details.TableRefFieldValueDisplay = "BiddingInterestName";
        //                            details.SortReferenceField = "BiddingInterestName";//Rajesh on 12/31/2014
        //                        }
        //                        else if (form.ColumnDetails[0].RefTableName == "UnitedStates")
        //                        {
        //                            details.TableReferenceField = "StatesId";
        //                            details.TableRefFieldValueDisplay = "StateName";
        //                        }
        //                        else if (form.ColumnDetails[0].RefTableName == "CanadianProvinces")
        //                        {
        //                            details.TableReferenceField = "ProvinceId";
        //                            details.TableRefFieldValueDisplay = "ProvinceName";
        //                        }
        //                        else if (form.ColumnDetails[0].RefTableName == "ClientContractTypes")
        //                        {
        //                            details.TableReferenceField = "ClientContractId";
        //                            details.TableRefFieldValueDisplay = "ContractType";
        //                        }
        //                        else if (form.ColumnDetails[0].RefTableName == "ProductCategories")
        //                        {
        //                            details.TableReferenceField = "ProductCategoryId";
        //                            details.TableRefFieldValueDisplay = "ProductCategoryName";
        //                            details.SortReferenceField = "ProductCategoryName";
        //                        }

        //                    }
        //                    Question.QuestionColumnDetails.Add(details);
        //                }
        //                else
        //                {
        //                    if (form.ColumnDetails[0].ControlType == "-1" || form.ColumnDetails[0].ControlType == null)////Rajesh on 1/3/2014
        //                    {
        //                        errorMessage += " Column " + (i + 1) + ",";
        //                    }
        //                    else if (form.ColumnDetails[i].ControlType == "7" || form.ColumnDetails[i].ControlType == "10")//Radio or MultiSelect
        //                    {
        //                        if (string.IsNullOrEmpty(form.ColumnDetails[0].Value))
        //                        {
        //                            errorMessage += " Column " + (i + 1) + ",";
        //                        }
        //                    }
        //                    else if (!form.ColumnDetails[i].HasTableReference && form.ColumnDetails[i].ControlType == "9")
        //                    {
        //                        if (string.IsNullOrEmpty(form.ColumnDetails[0].Value))
        //                        {
        //                            errorMessage += " Column " + (i + 1) + ",";
        //                        }
        //                    }

        //                    QuestionColumnDetails details = new QuestionColumnDetails();
        //                    details.ColumnNo = i + 1;
        //                    details.ColumnValues = form.ColumnDetails[i].Value;
        //                    details.DisplayType = Convert.ToInt32(form.ColumnDetails[i].DisplayType);
        //                    details.QuestionControlTypeId = Convert.ToInt32(form.ColumnDetails[i].ControlType);
        //                    if (form.ColumnDetails[i].ControlType == "12")
        //                        details.ButtonTypeReferenceValue = form.ColumnDetails[i].ButtonTypeReferenceValue;
        //                    details.QFormula = form.ColumnDetails[i].FormulaValue;
        //                    if (form.ColumnDetails[i].HasFormula == false)
        //                    {
        //                        details.QFormula = null;
        //                    }

        //                    details.QuestionId = Question.QuestionID;
        //                    if (form.ColumnDetails[i].HasTableReference)
        //                    {
        //                        details.TableReference = form.ColumnDetails[0].RefTableName;
        //                        if (form.ColumnDetails[i].RefTableName == "BiddingInterests" || form.ColumnDetails[i].RefTableName == "ClaycoBiddingInterests")
        //                        {
        //                            details.TableReferenceField = "BiddingInterestId";
        //                            details.TableRefFieldValueDisplay = "BiddingInterestName";
        //                            details.SortReferenceField = "BiddingInterestName";//Rajesh on 12/31/2014
        //                        }
        //                        else if (form.ColumnDetails[0].RefTableName == "UnitedStates")
        //                        {
        //                            details.TableReferenceField = "StatesId";

        //                            details.TableRefFieldValueDisplay = "StateName";
        //                        }
        //                        else if (form.ColumnDetails[0].RefTableName == "CanadianProvinces")
        //                        {
        //                            details.TableReferenceField = "ProvinceId";
        //                            details.TableRefFieldValueDisplay = "ProvinceName";
        //                        }
        //                        else if (form.ColumnDetails[0].RefTableName == "ClientContractTypes")
        //                        {
        //                            details.TableReferenceField = "ClientContractId";
        //                            details.TableRefFieldValueDisplay = "ContractType";
        //                        }
        //                        else if (form.ColumnDetails[0].RefTableName == "ProductCategories")
        //                        {
        //                            details.TableReferenceField = "ProductCategoryId";
        //                            details.TableRefFieldValueDisplay = "ProductCategoryName";
        //                            details.SortReferenceField = "ProductCategoryName";
        //                        }
        //                    }
        //                    Question.QuestionColumnDetails.Add(details);
        //                }

        //            }
        //    }
        //    if (errorMessage.Equals(""))
        //    {
        //        entityDB.SaveChanges();
        //        return Redirect("PopUpCloseView");
        //    }
        //    else
        //    {

        //        ////For Formula Fields

        //        var questions = entityDB.Questions.Find(form.QuestionId);
        //        var maxRows = 0;
        //        List<string> qValues = new List<string>();
        //        foreach (var allQColDetails in entityDB.QuestionColumnDetails.Where(rec => rec.Questions.SubSectionId == questions.SubSectionId && rec.QuestionId != form.QuestionId).GroupBy(rec => rec.QuestionId).ToList())
        //        {
        //            int num = allQColDetails.Max(rec => rec.ColumnNo);
        //            if (maxRows < num)
        //                maxRows = num;
        //            //string question = allQColDetails.Questions.QuestionText + "| Q" + allQColDetails.QuestionColumnId + " ";
        //            string question = "";//allQColDetails.Questions.QuestionText + "| Q" + allQColDetails.QuestionColumnId + " ";
        //            bool initialFlag = true;
        //            foreach (var QcolumnDetails in allQColDetails.OrderBy(rec => rec.ColumnNo))
        //            {
        //                if (initialFlag)
        //                    question = QcolumnDetails.Questions.QuestionText;

        //                if (QcolumnDetails.QuestionControlTypeId != 3)
        //                    question += "|-";
        //                else
        //                    question += "| Q" + QcolumnDetails.QuestionColumnId + " ";
        //                initialFlag = false;

        //            }
        //            qValues.Add(question);
        //        }
        //        foreach (var QColumns in form.ColumnDetails)
        //        {
        //            QColumns.QuestionValues = qValues;
        //        }
        //        //Ends<<<


        //        ModelState.AddModelError("", "Incorrect data in: " + errorMessage.Substring(0, errorMessage.Length - 1));
        //        return View(form);
        //    }
        //}

        [SessionExpireForView]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "QuestionnaireTool", sideMenuName = "TemplateTool", ViewName = "AddOrEditQuestionColumnDetails")]
        public ActionResult AddOrEditQuestionColumnDetails(long QuestionId)
        {
            //Rajesh on 12/24/2013
            SelectListItem EmptyItem = new SelectListItem() { Text = "Select Type", Value = "-1" };
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(EmptyItem);
            items.AddRange((from controls in entityDB.QuestionControlType.Where(rec => rec.Visible == true).ToList()
                            select new SelectListItem
                            {
                                Text = controls.QuestionTypeDisplayName,
                                Value = controls.QuestionTypeID + ""
                            }).ToList());

            ViewBag.QuestionControlTypes = items;

            //Ends<<

            var Question = entityDB.Questions.Find(QuestionId);
            ViewBag.Question = Question;
            LocalQuestionColumnDetails details = new LocalQuestionColumnDetails();
            details.QuestionId = QuestionId;
            // var HasColor=entityDB.QuestionColors.FirstOrDefault(r => r.QuestionID == QuestionId);
            //  if (HasColor != null)
            details.IsColor = Question.HasHighlights;
            details.IsBold = (bool)Question.IsBold;
            details.IsMandatory = (bool)Question.IsMandatory;
            details.IsRepeat = Question.QuestionColumnDetails.Count() == 0;
            details.ResponseRequired = Question.ResponseRequired;
            details.Visible = !Question.Visible;
            details.QuestionAnswer = Question.CorrectAnswer;
            details.ColumnDetails = new List<ColumnDetails>();
            details.HasDependent = (bool)Question.HasDependantQuestions;
            details.ReferenceType = Question.ReportForKeyword ?? "";
            details.GroupNum = Question.GroupAsOneRecord;
            details.HasVendorDetails = Question.VendorDetailQuestions.Any(r => r.Type == 0);
            if (Question.QuestionColumnDetails.Count() == 0)//Add Question Columns
            {
                for (int i = 0; i < Question.NumberOfColumns; i++)
                {
                    ColumnDetails c = new ColumnDetails();
                    c.QuestionValues = new List<string>();
                    foreach (var allQColDetails in entityDB.QuestionColumnDetails.Where(rec => rec.Questions.SubSectionId == Question.SubSectionId && rec.QuestionId != QuestionId).GroupBy(rec => rec.QuestionId).ToList())
                    {

                        string question = "";//allQColDetails.Questions.QuestionText + "| Q" + allQColDetails.QuestionColumnId + " ";
                        bool initialFlag = true;
                        var maxRows = 1;
                        foreach (var QcolumnDetails in allQColDetails.OrderBy(rec => rec.ColumnNo))
                        {
                            int num = allQColDetails.Max(rec => rec.ColumnNo);
                            if (maxRows < num)
                                maxRows = num;

                            if (initialFlag)
                                question = QcolumnDetails.Questions.QuestionText;
                            initialFlag = false;
                            if (QcolumnDetails.QuestionControlTypeId != 3)
                                question += "|-";
                            else
                                question += "| Q" + QcolumnDetails.QuestionColumnId + " ";

                        }
                        c.MaxQuestionColumns = maxRows;
                        c.QuestionValues.Add(question);
                    }
                    c.HasFormula = false;

                    details.ColumnDetails.Add(c);


                }
            }
            else//Edit Question Columns
            {
                foreach (var column in Question.QuestionColumnDetails)
                {
                    ColumnDetails c = new ColumnDetails();



                    c.QuestionValues = new List<string>();
                    var maxRows = 1;
                    foreach (var allQColDetails in entityDB.QuestionColumnDetails.Where(rec => rec.Questions.SubSectionId == column.Questions.SubSectionId && rec.QuestionId != QuestionId).GroupBy(rec => rec.QuestionId).ToList())
                    {
                        int num = allQColDetails.Max(rec => rec.ColumnNo);
                        if (maxRows < num)
                            maxRows = num;
                        //string question = allQColDetails.Questions.QuestionText + "| Q" + allQColDetails.QuestionColumnId + " ";
                        string question = "";//allQColDetails.Questions.QuestionText + "| Q" + allQColDetails.QuestionColumnId + " ";
                        bool initialFlag = true;
                        foreach (var QcolumnDetails in allQColDetails.OrderBy(rec => rec.ColumnNo))
                        {
                            if (initialFlag)
                                question = QcolumnDetails.Questions.QuestionText;

                            if (QcolumnDetails.QuestionControlTypeId != 3)
                                question += "|-";
                            else
                                question += "| Q" + QcolumnDetails.QuestionColumnId + " ";
                            initialFlag = false;
                        }
                        c.QuestionValues.Add(question);
                    }
                    c.MaxQuestionColumns = maxRows;
                    c.ButtonTypeReferenceValue = column.ButtonTypeReferenceValue ?? 0;
                    c.ControlType = column.QuestionControlTypeId + "";
                    c.DisplayType = column.DisplayType;
                    c.HasTableReference = !string.IsNullOrWhiteSpace(column.TableReference);
                    c.RefTableName = column.TableReference;
                    c.Value = column.ColumnValues;
                    c.FormulaValue = column.QFormula;
                    if (!string.IsNullOrEmpty(column.QFormula))
                    {
                        c.HasFormula = true;
                    }
                    details.ColumnDetails.Add(c);
                }
            }
            return View(details);
        }

        [HttpPost]
        [SessionExpireForView]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "QuestionnaireTool", sideMenuName = "TemplateTool", ViewName = "AddOrEditQuestionColumnDetails")]
        public ActionResult AddOrEditQuestionColumnDetails(LocalQuestionColumnDetails form)
        {
            //Rajesh on 12/24/2013
            SelectListItem EmptyItem = new SelectListItem() { Text = "Select Type", Value = "-1" };
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(EmptyItem);
            items.AddRange((from controls in entityDB.QuestionControlType.Where(rec => rec.Visible == true).ToList()
                            select new SelectListItem
                            {
                                Text = controls.QuestionTypeDisplayName,
                                Value = controls.QuestionTypeID + ""
                            }).ToList());

            ViewBag.QuestionControlTypes = items;

            ////Ends<<
            //List<SelectListItem> Colors = new List<SelectListItem>();
            //SelectListItem EmptyColor = new SelectListItem() { Text = "None", Value = "0" };
            //Colors.Add(EmptyColor);
            //Colors.Add(new SelectListItem() { Text = "Red", Value = "Red" });
            //Colors.Add(new SelectListItem() { Text = "Greeen", Value = "Greeen" });
            //Colors.Add(new SelectListItem() { Text = "Yellow", Value = "Yellow" });
            //Colors.Add(new SelectListItem() { Text = "Blue", Value = "Blue" });
            ////Ends<<
            //ViewBag.Colors = Colors;

            var Question = entityDB.Questions.Find(form.QuestionId);

            var errorMessage = "";

            ViewBag.Question = Question;

            Question.IsBold = form.IsBold;
            Question.IsMandatory = form.IsMandatory;
            Question.Visible = !form.Visible;
            Question.CorrectAnswer = form.QuestionAnswer;
            Question.ResponseRequired = form.ResponseRequired;
            Question.HasDependantQuestions = form.HasDependent;

            if (string.IsNullOrEmpty(Question.ReportForKeyword) ||
                Question.ReportForKeyword.ToLower().Equals("vendordetails") ||
                Question.ReportForKeyword.ToLower().Equals("clientvendorid"))
            {
                Question.ReportForKeyword = form.ReferenceType;
                Question.GroupAsOneRecord = form.GroupNum;
            }

            Question.HasHighlights = form.IsColor;


            if (form.HasVendorDetails == true)
            {
                if (!Question.VendorDetailQuestions.Any())
                {
                    VendorDetailQuestions vdq = new VendorDetailQuestions();

                    vdq.QuestionId = form.QuestionId;
                    var templateid = Question.TemplateSubSections.TemplateSections.TemplateID;
                    vdq.TemplateId = templateid;
                    entityDB.VendorDetailQuestions.Add(vdq);
                }
            }
            else
            {
                var x = entityDB.VendorDetailQuestions.FirstOrDefault(r => r.QuestionId == form.QuestionId && r.Type == 0);
                if (x != null)
                {
                    entityDB.VendorDetailQuestions.Remove(x);
                }
            }
            //Rajesh on 12/31/2013
            if (!form.HasDependent)//If user unselects hasDependent remove all depends
            {
                if (Question.QuestionsDependants != null)
                {
                    foreach (var QDepends in Question.QuestionsDependants.ToList())
                    {
                        entityDB.QuestionsDependants.Remove(QDepends);
                    }
                }
            }
            //Ends<<<
            if (Question.QuestionColumnDetails == null || !(Question.QuestionColumnDetails.Where(rec => rec.PrequalificationUserInputs.Count() > 0).Count() > 0))
            {
                foreach (var column in Question.QuestionColumnDetails.ToList())
                {
                    entityDB.Entry(column).State = EntityState.Deleted;
                }
                if (form.ColumnDetails != null)
                    for (int i = 0; i < form.ColumnDetails.Count(); i++)
                    {
                        if (form.IsRepeat)
                        {
                            if (form.ColumnDetails[0].ControlType == "-1" || form.ColumnDetails[0].ControlType == null)//Rajesh on 1/3/2014
                            {
                                errorMessage = "Column 1 ";
                            }
                            else if (form.ColumnDetails[0].ControlType == "7" || form.ColumnDetails[0].ControlType == "10")//Radio or MultiSelect
                            {
                                if (string.IsNullOrEmpty(form.ColumnDetails[0].Value))
                                {
                                    errorMessage = "Column 1 ";
                                }
                            }
                            else if (!form.ColumnDetails[0].HasTableReference && form.ColumnDetails[0].ControlType == "9")
                            {
                                if (string.IsNullOrEmpty(form.ColumnDetails[0].Value))
                                {
                                    errorMessage = "Column 1 ";
                                }
                            }
                            QuestionColumnDetails details = new QuestionColumnDetails();
                            details.ColumnNo = i + 1;
                            details.ColumnValues = form.ColumnDetails[0].Value;
                            if (i == 0)
                                details.QFormula = form.ColumnDetails[0].FormulaValue;
                            if (form.ColumnDetails[0].HasFormula == false)
                            {
                                details.QFormula = null;
                            }
                            details.DisplayType = Convert.ToInt32(form.ColumnDetails[0].DisplayType);
                            details.QuestionControlTypeId = Convert.ToInt32(form.ColumnDetails[0].ControlType);
                            details.QuestionId = Question.QuestionID;
                            if (form.ColumnDetails[0].HasTableReference)
                            {
                                details.ColumnValues = "";
                                details.TableReference = form.ColumnDetails[0].RefTableName;
                                if (form.ColumnDetails[0].RefTableName == "BiddingInterests")
                                {
                                    details.TableReferenceField = "BiddingInterestId";
                                    details.TableRefFieldValueDisplay = "BiddingInterestName";
                                    details.SortReferenceField = "BiddingInterestName";//Rajesh on 12/31/2014
                                }
                                else if (form.ColumnDetails[0].RefTableName == "UnitedStates")
                                {
                                    details.TableReferenceField = "StatesId";
                                    details.TableRefFieldValueDisplay = "StateName";
                                }
                                else if (form.ColumnDetails[0].RefTableName == "CanadianProvinces")
                                {
                                    details.TableReferenceField = "ProvinceId";
                                    details.TableRefFieldValueDisplay = "ProvinceName";
                                }
                                else if (form.ColumnDetails[0].RefTableName == "ClientContractTypes")
                                {
                                    details.TableReferenceField = "ClientContractId";
                                    details.TableRefFieldValueDisplay = "ContractType";
                                }
                                else if (form.ColumnDetails[0].RefTableName == "ProductCategories")
                                {
                                    details.TableReferenceField = "ProductCategoryId";
                                    details.TableRefFieldValueDisplay = "ProductCategoryName";
                                    details.SortReferenceField = "ProductCategoryName";
                                }

                            }
                            Question.QuestionColumnDetails.Add(details);
                        }
                        else
                        {
                            if (form.ColumnDetails[0].ControlType == "-1" || form.ColumnDetails[0].ControlType == null)////Rajesh on 1/3/2014
                            {
                                errorMessage += " Column " + (i + 1) + ",";
                            }
                            else if (form.ColumnDetails[i].ControlType == "7" || form.ColumnDetails[i].ControlType == "10")//Radio or MultiSelect
                            {
                                if (string.IsNullOrEmpty(form.ColumnDetails[0].Value))
                                {
                                    errorMessage += " Column " + (i + 1) + ",";
                                }
                            }
                            else if (!form.ColumnDetails[i].HasTableReference && form.ColumnDetails[i].ControlType == "9")
                            {
                                if (string.IsNullOrEmpty(form.ColumnDetails[0].Value))
                                {
                                    errorMessage += " Column " + (i + 1) + ",";
                                }
                            }

                            QuestionColumnDetails details = new QuestionColumnDetails();
                            details.ColumnNo = i + 1;
                            details.ColumnValues = form.ColumnDetails[i].Value;
                            details.DisplayType = Convert.ToInt32(form.ColumnDetails[i].DisplayType);
                            details.QuestionControlTypeId = Convert.ToInt32(form.ColumnDetails[i].ControlType);
                            if (form.ColumnDetails[i].ControlType == "12")
                                details.ButtonTypeReferenceValue = form.ColumnDetails[i].ButtonTypeReferenceValue;
                            details.QFormula = form.ColumnDetails[i].FormulaValue;
                            if (form.ColumnDetails[i].HasFormula == false)
                            {
                                details.QFormula = null;
                            }

                            details.QuestionId = Question.QuestionID;
                            if (form.ColumnDetails[i].HasTableReference)
                            {
                                details.TableReference = form.ColumnDetails[0].RefTableName;
                                if (form.ColumnDetails[i].RefTableName == "BiddingInterests" || form.ColumnDetails[i].RefTableName == "ClaycoBiddingInterests")
                                {
                                    details.TableReferenceField = "BiddingInterestId";
                                    details.TableRefFieldValueDisplay = "BiddingInterestName";
                                    details.SortReferenceField = "BiddingInterestName";//Rajesh on 12/31/2014
                                }
                                else if (form.ColumnDetails[0].RefTableName == "UnitedStates")
                                {
                                    details.TableReferenceField = "StatesId";

                                    details.TableRefFieldValueDisplay = "StateName";
                                }
                                else if (form.ColumnDetails[0].RefTableName == "CanadianProvinces")
                                {
                                    details.TableReferenceField = "ProvinceId";
                                    details.TableRefFieldValueDisplay = "ProvinceName";
                                }
                                else if (form.ColumnDetails[0].RefTableName == "ClientContractTypes")
                                {
                                    details.TableReferenceField = "ClientContractId";
                                    details.TableRefFieldValueDisplay = "ContractType";
                                }
                                else if (form.ColumnDetails[0].RefTableName == "ProductCategories")
                                {
                                    details.TableReferenceField = "ProductCategoryId";
                                    details.TableRefFieldValueDisplay = "ProductCategoryName";
                                    details.SortReferenceField = "ProductCategoryName";
                                }
                            }
                            Question.QuestionColumnDetails.Add(details);
                        }

                    }
            }
            if (errorMessage.Equals(""))
            {
                entityDB.SaveChanges();
                return Redirect("PopUpCloseView");
            }
            else
            {

                ////For Formula Fields

                var questions = entityDB.Questions.Find(form.QuestionId);
                var maxRows = 0;
                List<string> qValues = new List<string>();
                foreach (var allQColDetails in entityDB.QuestionColumnDetails.Where(rec => rec.Questions.SubSectionId == questions.SubSectionId && rec.QuestionId != form.QuestionId).GroupBy(rec => rec.QuestionId).ToList())
                {
                    int num = allQColDetails.Max(rec => rec.ColumnNo);
                    if (maxRows < num)
                        maxRows = num;
                    //string question = allQColDetails.Questions.QuestionText + "| Q" + allQColDetails.QuestionColumnId + " ";
                    string question = "";//allQColDetails.Questions.QuestionText + "| Q" + allQColDetails.QuestionColumnId + " ";
                    bool initialFlag = true;
                    foreach (var QcolumnDetails in allQColDetails.OrderBy(rec => rec.ColumnNo))
                    {
                        if (initialFlag)
                            question = QcolumnDetails.Questions.QuestionText;

                        if (QcolumnDetails.QuestionControlTypeId != 3)
                            question += "|-";
                        else
                            question += "| Q" + QcolumnDetails.QuestionColumnId + " ";
                        initialFlag = false;

                    }
                    qValues.Add(question);
                }
                foreach (var QColumns in form.ColumnDetails)
                {
                    QColumns.QuestionValues = qValues;
                }
                //Ends<<<


                ModelState.AddModelError("", "Incorrect data in: " + errorMessage.Substring(0, errorMessage.Length - 1));
                return View(form);
            }
        }

        [HttpPost]
        public string DeleteQuestions(string questionBankIds, long subSectionId)
        {
            if (questionBankIds == "1")//Delete Blank Questions
            {
                foreach (var question in entityDB.Questions.Where(rec => rec.QuestionBankId == 1 && rec.SubSectionId == subSectionId).ToList())
                {
                    //entityDB.Entry(question).State = EntityState.Deleted;
                    if (question.QuestionColumnDetails != null)
                        foreach (var Qcolumn in question.QuestionColumnDetails.ToList())
                            entityDB.Entry(Qcolumn).State = EntityState.Deleted;
                    if (question.QuestionsDependants != null)
                    {
                        foreach (var Qdependent in question.QuestionsDependants.ToList())
                            entityDB.Entry(Qdependent).State = EntityState.Deleted;
                    }
                    entityDB.Entry(question).State = EntityState.Deleted;
                }
                entityDB.SaveChanges();
                return "Ok";
            }

            foreach (var QBId in questionBankIds.Split(','))
            {

                try
                {
                    long QBankId = Convert.ToInt64(QBId);
                    foreach (var question in entityDB.Questions.Where(rec => rec.QuestionBankId == QBankId && rec.SubSectionId == subSectionId).ToList())
                    {
                        if (question.QuestionColumnDetails != null)
                            foreach (var Qcolumn in question.QuestionColumnDetails.ToList())
                                entityDB.Entry(Qcolumn).State = EntityState.Deleted;
                        if (question.QuestionsDependants != null)
                        {
                            foreach (var Qdependent in question.QuestionsDependants.ToList())
                                entityDB.Entry(Qdependent).State = EntityState.Deleted;
                        }
                        entityDB.Entry(question).State = EntityState.Deleted;
                    }
                }
                catch (Exception ee) { }
            }
            entityDB.SaveChanges();
            return "OK";
        }
        [SessionExpireForView]
        [HeaderAndSidebar(headerName = "QuestionnaireTool", sideMenuName = "TemplateTool", ViewName = "DependentQuestions")]
        public ActionResult DependentQuestions(long QuestionId)
        {
            var question = entityDB.Questions.Find(QuestionId);
            ViewBag.QuestionName = question.QuestionText;
            ViewBag.QuestionId = QuestionId;
            ViewBag.Question = question;
            getCommand();
            ViewBag.depends = (from sec in question.TemplateSubSections.TemplateSections.TemplateSubSections.Where(rec => rec.SubSectionID != question.SubSectionId).OrderBy(rec => rec.DisplayOrder).ToList()
                               select new SelectListItem
                               {
                                   Text = sec.SubSectionName,
                                   Value = sec.SubSectionID + "",

                               }).ToList();

            if (question.QuestionsDependants != null && question.QuestionsDependants.Count() != 0)
            {
                var questionDependent = question.QuestionsDependants.ToList();
                ViewBag.DependentValue = questionDependent[0].DependantValue;
                ViewBag.DependentType = questionDependent[0].DependantType;

                if (questionDependent[0].DependantType == 0)
                {
                    ViewBag.depends = (from questions in question.TemplateSubSections.Questions.Where(rec => rec.QuestionID != QuestionId).OrderBy(rec => rec.DisplayOrder).ToList()
                                       select new SelectListItem
                                       {
                                           Text = questions.QuestionText,
                                           Value = questions.QuestionID + "",
                                           Selected = questionDependent.FirstOrDefault(rec => rec.DependantId == questions.QuestionID && rec.DependantValue == questionDependent[0].DependantValue) != null//Rajesh on 11/26/2013
                                       }).ToList();
                }
                else
                {
                    ViewBag.depends = (from sec in question.TemplateSubSections.TemplateSections.TemplateSubSections.Where(rec => rec.SubSectionID != question.SubSectionId).OrderBy(rec => rec.DisplayOrder).ToList()
                                       select new SelectListItem
                                       {
                                           Text = sec.SubSectionName,
                                           Value = sec.SubSectionID + "",
                                           Selected = questionDependent.FirstOrDefault(rec => rec.DependantId == sec.SubSectionID && rec.DependantValue == questionDependent[0].DependantValue) != null//Rajesh on 11/26/2013
                                       }).ToList();
                }
            }

            return View();
        }

        [HttpPost]
        public ActionResult getDependentValues(long QuestionId, int type, string value)//0-Questions 1-Sections
        {
            var question = entityDB.Questions.Find(QuestionId);
            var data = (from sec in question.TemplateSubSections.TemplateSections.TemplateSubSections.Where(rec => rec.SubSectionID != question.SubSectionId).OrderBy(rec => rec.DisplayOrder).ToList()
                        select new SelectListItem
                        {
                            Text = sec.SubSectionName,
                            Value = sec.SubSectionID + "",

                        }).ToList();
            if (type == 0)
                data = (from questions in question.TemplateSubSections.Questions.Where(rec => rec.QuestionID != QuestionId).OrderBy(rec => rec.DisplayOrder).ToList()
                        select new SelectListItem
                        {
                            Text = questions.QuestionText,
                            Value = questions.QuestionID + ""
                        }).ToList();

            if (question.QuestionsDependants != null && question.QuestionsDependants.Count > 0)
            {
                var questionDependent = question.QuestionsDependants.Where(rec => rec.DependantValue == value).ToList();//Rajesh on 11/26/2013
                if (type == 0)
                {
                    data = (from questions in question.TemplateSubSections.Questions.Where(rec => rec.QuestionID != QuestionId).OrderBy(rec => rec.DisplayOrder).ToList()
                            select new SelectListItem
                            {
                                Text = questions.QuestionText,
                                Value = questions.QuestionID + "",
                                Selected = questionDependent.FirstOrDefault(rec => rec.DependantId == questions.QuestionID) != null
                            }).ToList();
                }
                else
                {
                    data = (from sec in question.TemplateSubSections.TemplateSections.TemplateSubSections.Where(rec => rec.SubSectionID != question.SubSectionId).OrderBy(rec => rec.DisplayOrder).ToList()
                            select new SelectListItem
                            {
                                Text = sec.SubSectionName,
                                Value = sec.SubSectionID + "",
                                Selected = questionDependent.FirstOrDefault(rec => rec.DependantId == sec.SubSectionID) != null
                            }).ToList();
                }
            }
            return Json(data);
        }


        [SessionExpire]
        [HeaderAndSidebar(headerName = "QuestionnaireTool", sideMenuName = "TemplateTool", ViewName = "TemplatePreview")]
        public ActionResult TemplatePreview(long TemplateSectionID, long ClientId, int type, Guid? templateId)//1-Standard 0-Custom
        {
            Templates selectedTemplate = entityDB.Templates.FirstOrDefault(rec => rec.TemplateStatus == 0 && rec.TemplateType == 0 && rec.DefaultTemplate == true);

            if (type == 0)
                selectedTemplate = entityDB.Templates.FirstOrDefault(rec => rec.TemplateStatus == 0 && rec.TemplateType == 0 && rec.DefaultTemplate == false);
            if (TemplateSectionID != -1)
            {
                TemplateSections Section = entityDB.TemplateSections.Find(TemplateSectionID);
                var langugaeCode = _org.GetLanguageCode((long)Section.Templates.LanguageId);
                _preq.SetCulture(langugaeCode);
            }
            if (templateId != null)
            {
                selectedTemplate = entityDB.Templates.Find(templateId);
            }
            ViewBag.Type = type;

            ViewBag.TemplateId = selectedTemplate.TemplateID;//Rajesh on 12/21/2013
            //To Change Side Menu Selection>>>
            if (selectedTemplate.DefaultTemplate == null || selectedTemplate.DefaultTemplate == false)
            {
                List<LocalHeaderMenuModel> headerMenu = (List<LocalHeaderMenuModel>)ViewBag.headerMenus;
                headerMenu.FirstOrDefault(record => record.menuName == "QuestionnaireTool").sideMenus.ForEach(record => record.isChecked = false);
                headerMenu.FirstOrDefault(record => record.menuName == "QuestionnaireTool").sideMenus.FirstOrDefault(record => record.menuName == "CustomTemplateTool").isChecked = true;
                ViewBag.headerMenus = headerMenu;
            }

            //Ends<<<


            var templateSection = selectedTemplate.TemplateSections.OrderBy(rec => rec.DisplayOrder).FirstOrDefault(rec => rec.TemplateSectionID == TemplateSectionID);
            if (templateSection == null)
                templateSection = selectedTemplate.TemplateSections.Where(rec => rec.TemplateSectionType != 0 && rec.TemplateSectionType != 31).OrderBy(rec => rec.DisplayOrder).FirstOrDefault();


            //Rajesh on 4/9/2014
            if (ClientId == -1 && (templateSection.TemplateSectionType == 5 || templateSection.TemplateSectionType == 6 || templateSection.TemplateSectionType == 8))
            {
                if (type == 1)
                    ClientId = entityDB.Organizations.Where(rec => rec.OrganizationType == "Client").FirstOrDefault().OrganizationID;
                else
                {
                    var clientId = selectedTemplate.ClientTemplates.FirstOrDefault().ClientID;
                    ClientId = entityDB.Organizations.Where(rec => rec.OrganizationType == "Client" && rec.OrganizationID == clientId).FirstOrDefault().OrganizationID;
                }
            }
            //Ends<<<<
            TemplateSectionID = templateSection.TemplateSectionID;
            ViewBag.AllSections = (from sec in selectedTemplate.TemplateSections.Where(rec => rec.TemplateSectionType != 0 && rec.TemplateSectionType != 31).OrderBy(rec => rec.DisplayOrder).ToList()
                                   select new SelectListItem
                                   {
                                       Text = sec.SectionName,
                                       Value = sec.TemplateSectionID + "",
                                       Selected = sec.TemplateSectionID == TemplateSectionID
                                   }).ToList();
            if (type == 1)
                ViewBag.AllClients = (from org in entityDB.Organizations.Where(rec => rec.OrganizationType == "Client").ToList()
                                      select new SelectListItem
                                      {
                                          Text = org.Name,
                                          Value = org.OrganizationID + "",
                                          Selected = org.OrganizationID == ClientId
                                      }).ToList();

            else
            {
                var clientId = selectedTemplate.ClientTemplates.FirstOrDefault().ClientID;
                ViewBag.AllClients = (from org in entityDB.Organizations.Where(rec => rec.OrganizationType == "Client" && rec.OrganizationID == clientId).ToList()
                                      select new SelectListItem
                                      {
                                          Text = org.Name,
                                          Value = org.OrganizationID + "",
                                          Selected = org.OrganizationID == ClientId
                                      }).ToList();
            }
            getCommand();
            @ViewBag.PreviewTemplateName = selectedTemplate.TemplateName;
            ViewBag.TemplatePrice = "XXX";
            ViewBag.previewTemplateType = templateSection.TemplateSectionType;
            ViewBag.TemplateSectionID = TemplateSectionID;
            @ViewBag.ClientId = ClientId;
            return View(selectedTemplate);
        }
        public string saveDependancies(string DependentValue, int dependentType, string dependentIds, long QuestionId)
        {
            foreach (var depends in entityDB.QuestionsDependants.Where(rec => rec.QuestionID == QuestionId && rec.DependantValue == DependentValue).ToList())//Rajesh on 11/26/2013
            {
                entityDB.QuestionsDependants.Remove(depends);
            }
            foreach (var dependentid in dependentIds.Split(','))
            {
                try
                {
                    var id = Convert.ToInt64(dependentid);
                    var dependants = new QuestionsDependants();
                    dependants.QuestionID = QuestionId;
                    dependants.DependantValue = DependentValue;
                    dependants.DependantType = dependentType;
                    dependants.DependantId = id;
                    dependants.DependantDisplayOrder = 1;
                    entityDB.QuestionsDependants.Add(dependants);
                }
                catch (Exception ee)
                {
                }
            }
            entityDB.SaveChanges();
            return "Ok";
        }
        public void getCommand()
        {
            SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["EFDbContext"].ConnectionString);
            connection.Open();
            ViewBag.command = connection.CreateCommand();

        }

        public string ApproveTemplate(int TemplateType)//1-Standard 0-Custom
        {
            var parameterToReturn = "";
            if (TemplateType == 1)
            {
                var template = entityDB.Templates.FirstOrDefault(rec => rec.DefaultTemplate == true && rec.TemplateStatus == 0);
                template.TemplateStatus = 1;
                entityDB.Entry(template).State = EntityState.Modified;

                if (template.TemplateSections.Where(rec => rec.TemplateSectionType == 3 || rec.TemplateSectionType == 99 || rec.TemplateSectionType == 4 || rec.TemplateSectionType == 7).Count() < 4)
                {
                    return "Incomplete Sections";
                }
                var sectionWithoutSubsecs = template.TemplateSections.FirstOrDefault(rec => rec.TemplateSectionType != 31 && rec.TemplateSectionType != 0 && rec.TemplateSectionType != 5 && rec.TemplateSectionType != 6 && rec.TemplateSectionType != 99 && (rec.TemplateSubSections == null || rec.TemplateSubSections.Count() == 0));//Rajesh on 1/22/2014
                if (sectionWithoutSubsecs != null)
                {
                    return "Incomplete Sections";
                }


                var currentOrgClientTemplates = entityDB.ClientTemplates.Where(record => record.TemplateID == template.TemplateID).ToList();
                foreach (var clientTemplate in currentOrgClientTemplates)
                {
                    foreach (var client in clientTemplate.Organizations.ClientBusinessUnits.ToList())
                    {
                        if (entityDB.ClientTemplatesForBU.FirstOrDefault(rec => rec.ClientBusinessUnitId == client.ClientBusinessUnitId && rec.ClientTemplateId == clientTemplate.ClientTemplateID) == null)
                        {
                            ClientTemplatesForBU currentClientTemplateBU = new ClientTemplatesForBU();
                            currentClientTemplateBU.ClientTemplateId = clientTemplate.ClientTemplateID;
                            currentClientTemplateBU.ClientBusinessUnitId = client.ClientBusinessUnitId;
                            entityDB.ClientTemplatesForBU.Add(currentClientTemplateBU);
                            entityDB.SaveChanges();
                        }
                    }

                }

                entityDB.SaveChanges();
                parameterToReturn = "Ok";
            }
            else
            {
                var template = entityDB.Templates.FirstOrDefault(rec => rec.DefaultTemplate == false && rec.TemplateStatus == 0);
                //template.TemplateStatus = 1;
                //entityDB.Entry(template).State = EntityState.Modified;


                if (template.TemplateSections.Where(rec => rec.TemplateSectionType == 3 || rec.TemplateSectionType == 99 || rec.TemplateSectionType == 4 || rec.TemplateSectionType == 7).Count() < 4)
                {
                    return "Incomplete Sections";
                }
                var sectionWOSubSection = template.TemplateSections.FirstOrDefault(rec => rec.TemplateSectionType != 31 && rec.TemplateSectionType != 0 && rec.TemplateSectionType != 99 && (rec.TemplateSubSections == null || rec.TemplateSubSections.Count() == 0));//Rajesh on 1/22/2014
                if (sectionWOSubSection != null)
                {
                    return "Incomplete Sections";
                }

                //var currentOrgClientTemplates = entityDB.ClientTemplates.Where(record => record.TemplateID == template.TemplateID).ToList();
                //foreach (var clientTemplate in currentOrgClientTemplates)
                //{
                //    foreach (var client in clientTemplate.Organizations.ClientBusinessUnits.ToList())
                //    {
                //        if (entityDB.ClientTemplatesForBU.FirstOrDefault(rec => rec.ClientBusinessUnitId == client.ClientBusinessUnitId && rec.ClientTemplateId == clientTemplate.ClientTemplateID) == null)
                //        {
                //            ClientTemplatesForBU currentClientTemplateBU = new ClientTemplatesForBU();
                //            currentClientTemplateBU.ClientTemplateId = clientTemplate.ClientTemplateID;
                //            currentClientTemplateBU.ClientBusinessUnitId = client.ClientBusinessUnitId;
                //            entityDB.ClientTemplatesForBU.Add(currentClientTemplateBU);
                //            entityDB.SaveChanges();
                //        }
                //    }

                //}
                //entityDB.SaveChanges();
                parameterToReturn = "RedirectToBUView";// Mani on 8/18/2015
            }
            return parameterToReturn;
        }


        //Rajesh on 10/28/2013
        [SessionExpire]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "QuestionnaireTool", sideMenuName = "AssignedTemplatesList", ViewName = "AssignedTemplatesList")]
        public ActionResult AssignedTemplatesList()
        {

            var clientTemplates = entityDB.ClientTemplates.Where(rec => rec.Templates.TemplateType == 0 && rec.Templates.TemplateStatus == 1 && rec.Templates.DefaultTemplate == true).GroupBy(record => record.Templates.TemplateName).ToList();
            ViewBag.VBClientTemplates = clientTemplates;//Here we will show only standard templates
            return View();
        }

        [HttpGet]
        [SessionExpire]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "QuestionnaireTool", sideMenuName = "AssignedTemplatesList", ViewName = "AssignTemplates")]
        public ActionResult AssignTemplates(Guid? clientTemplateId)
        {

            long clientId = -1;
            Guid? templateId = null;

            if (clientTemplateId != null)
            {
                var clientTemplate = entityDB.ClientTemplates.Find(clientTemplateId);
                clientId = clientTemplate.ClientID;
                templateId = clientTemplate.TemplateID;
            }

            LocalAssignTemplates assignTemplate = new LocalAssignTemplates();

            ViewBag.Templates = (from templates in entityDB.Templates.Where(templates => templates.TemplateType == 0 && templates.TemplateStatus == 1 && templates.DefaultTemplate == true).ToList()
                                 select new SelectListItem
                                 {
                                     Text = templates.TemplateName,
                                     Value = templates.TemplateID + "",
                                     Selected = templateId == null ? false : templateId == templates.TemplateID
                                 }).ToList();

            ViewBag.Clients = (from clients in entityDB.Organizations.Where(rec => rec.OrganizationType == "Client").ToList()
                               select new SelectListItem
                               {
                                   Text = clients.Name,
                                   Value = clients.OrganizationID + "",
                                   Selected = clientId == clients.OrganizationID
                               }).ToList();

            assignTemplate.DocumentTypes = (from documentType in entityDB.DocumentType.Where(rec => rec.PrequalificationUseOnly == true).ToList()
                                            select new CheckBoxesControl
                                            {
                                                Text = documentType.DocumentTypeName,
                                                id = documentType.DocumentTypeId + ""
                                            }).ToList();

            assignTemplate.ProficiencyCapabilities = (from proficiency in entityDB.ProficiencyCapabilities.Where(rec => rec.Status == 1).ToList().OrderBy(rec => rec.ProficiencyCategory).ThenBy(rec => rec.ProficiencySubCategroy).ToList()

                                                      select new CheckBoxesControl
                                                      {
                                                          id = proficiency.ProficiencyId + "",
                                                          Header = proficiency.ProficiencyCategory,
                                                          Text = (string.IsNullOrEmpty(proficiency.ProficiencySubCategroy) ? "" :

proficiency.ProficiencySubCategroy + "-") + proficiency.ProficiencyName,
                                                          type = 1
                                                      }).ToList();

            assignTemplate.BiddingInterests = (from biddings in entityDB.BiddingInterests.Where(rec => rec.Status == 1).ToList()
                                               select new CheckBoxesControl
                                               {
                                                   Text = biddings.BiddingInterestName,
                                                   id = biddings.BiddingInterestId + ""
                                               }).ToList();



            return View(assignTemplate);
        }

        [HttpPost]
        [SessionExpire]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "QuestionnaireTool", sideMenuName = "AssignedTemplatesList", ViewName = "AssignTemplates")]
        public ActionResult AssignTemplates(LocalAssignTemplates form)
        {
            if (string.IsNullOrEmpty(form.SelectedTemplate))
            {
                ModelState.AddModelError("SelectedTemplate", "*");

            }
            if (string.IsNullOrEmpty(form.SelectedClientId))
            {
                ModelState.AddModelError("SelectedClientId", "*");
            }
            if (!ModelState.IsValid)
            {
                ViewBag.Templates = (from templates in entityDB.Templates.Where(templates => templates.TemplateType == 0 && templates.TemplateStatus == 1 && templates.DefaultTemplate == true).ToList()
                                     select new SelectListItem
                                     {
                                         Text = templates.TemplateName,
                                         Value = templates.TemplateID + "",

                                     }).ToList();

                ViewBag.Clients = (from clients in entityDB.Organizations.Where(rec => rec.OrganizationType == "Client").ToList()
                                   select new SelectListItem
                                   {
                                       Text = clients.Name,
                                       Value = clients.OrganizationID + ""

                                   }).ToList();
                ViewBag.EditingMode = true;
                return View(form);
            }

            var isInsert = false;
            var clientId = Convert.ToInt64(form.SelectedClientId);
            var TemplateID = new Guid(form.SelectedTemplate);
            //foreach (var clients in form.Clients.Where(rec => rec.Status == true))
            {
                var CTemplate = entityDB.ClientTemplates.FirstOrDefault(rec => rec.ClientID == clientId && rec.TemplateID == TemplateID);
                if (CTemplate == null)
                {
                    isInsert = true;
                    CTemplate = new ClientTemplates();
                    CTemplate.ClientID = Convert.ToInt64(form.SelectedClientId);
                    CTemplate.DefaultTemplate = false;
                    CTemplate.DisplayOrder = 0;
                    CTemplate.TemplateID = new Guid(form.SelectedTemplate);
                    CTemplate.Visible = true;

                    CTemplate.ClientTemplateReportingData = new List<ClientTemplateReportingData>();

                    // Kiran on 12/05/2013
                    var clientBusUnits = entityDB.ClientBusinessUnits.Where(record => record.ClientId == clientId);
                    foreach (var unit in clientBusUnits)
                    {
                        ClientTemplatesForBU addClientTemplateBU = new ClientTemplatesForBU();
                        addClientTemplateBU.ClientBusinessUnitId = unit.ClientBusinessUnitId;
                        addClientTemplateBU.ClientTemplateId = CTemplate.ClientTemplateID;
                        entityDB.ClientTemplatesForBU.Add(addClientTemplateBU);
                    }
                    // Ends<<


                }
                if (form.DocumentTypes != null)
                    foreach (var doctype in form.DocumentTypes.Where(rec => rec.Status == true))//For Documents
                    {
                        var id = Convert.ToInt64(doctype.id);
                        if (entityDB.ClientTemplateReportingData.FirstOrDefault(rec => rec.ReportingType == 2 && rec.ReportingTypeId == id && rec.ClientTemplateId == CTemplate.ClientTemplateID) == null)
                        {
                            ClientTemplateReportingData report = new ClientTemplateReportingData();
                            report.ReportingType = 2;
                            report.ReportingTypeId = id;
                            report.DocumentExpires = doctype.isExpire;
                            CTemplate.ClientTemplateReportingData.Add(report);
                        }
                    }
                if (form.ProficiencyCapabilities != null)
                    foreach (var proficiency in form.ProficiencyCapabilities.Where(rec => rec.Status == true))//For proficiency
                    {
                        var id = Convert.ToInt64(proficiency.id);
                        if (entityDB.ClientTemplateReportingData.FirstOrDefault(rec => rec.ReportingType == 1 && rec.ReportingTypeId == id && rec.ClientTemplateId == CTemplate.ClientTemplateID) == null)
                        {
                            ClientTemplateReportingData report = new ClientTemplateReportingData();
                            report.ReportingType = 1;
                            report.ReportingTypeId = Convert.ToInt64(proficiency.id);

                            CTemplate.ClientTemplateReportingData.Add(report);
                        }
                    }
                if (form.BiddingInterests != null)
                    foreach (var bidding in form.BiddingInterests.Where(rec => rec.Status == true))//For Documents
                    {
                        var id = Convert.ToInt64(bidding.id);
                        if (entityDB.ClientTemplateReportingData.FirstOrDefault(rec => rec.ReportingType == 0 && rec.ReportingTypeId == id && rec.ClientTemplateId == CTemplate.ClientTemplateID) == null)
                        {
                            ClientTemplateReportingData report = new ClientTemplateReportingData();
                            report.ReportingType = 0;
                            report.ReportingTypeId = Convert.ToInt64(bidding.id);

                            CTemplate.ClientTemplateReportingData.Add(report);
                        }
                    }
                if (isInsert)
                    entityDB.ClientTemplates.Add(CTemplate);
                else
                    entityDB.Entry(CTemplate).State = EntityState.Modified;
                entityDB.SaveChanges();
            }
            return Redirect("PopUpCloseView");
        }
        //<<<Ends
        //Rajesh on 11/25/2013
        public string ChangeQuestionsOrder(string QuestionIds, long templateSubSectionId)
        {
            string result = "Ok" + QuestionIds + "\n";
            try
            {
                int orderNumber = 0;
                var templateSubSec = entityDB.TemplateSubSections.Find(templateSubSectionId);
                if (templateSubSec != null)
                    foreach (var ids in QuestionIds.Split('|'))
                    {
                        try
                        {
                            long secId = Convert.ToInt64(ids);
                            result += secId;
                            Questions Question = templateSubSec.Questions.ToList().FirstOrDefault(rec => rec.QuestionID == secId);
                            if (Question == null)
                                continue;
                            orderNumber++;
                            Question.DisplayOrder = orderNumber;
                            entityDB.Entry(Question).State = EntityState.Modified;
                        }
                        catch (Exception ee) { }
                    }
                entityDB.SaveChanges();
            }
            catch (Exception ee) { result += ee.Message; }
            return result;
        }
        //Ends
        public ActionResult GetClientDocsProficiencyBiddings(string selectedTemplate, string selectedClient)
        {
            long ClientId = Convert.ToInt64(selectedClient);
            var selectedClientTemplate = entityDB.ClientTemplates.FirstOrDefault(record => record.TemplateID == new Guid(selectedTemplate) && record.ClientID == ClientId).ClientTemplateID;

            var data = (from report in entityDB.ClientTemplateReportingData.Where(rec => rec.ClientTemplateId == selectedClientTemplate).ToList()
                        select new BiddingCheckBoxes
                        {
                            id = report.ReportingTypeId + "",
                            status = true.ToString(),
                            isExpire = report.DocumentExpires == null ? false : (bool)report.DocumentExpires,
                            type = report.ReportingType
                        }).ToList();

            return Json(data);
        }
        //Kiran  on 9/23/2014
        [HttpGet]
        public ActionResult SubSectionNotifications(long TemplateSubSectionId)
        {
            var TSubSec = entityDB.TemplateSubSections.Find(TemplateSubSectionId);
            var clientId = TSubSec.TemplateSections.Templates.ClientTemplates.Select(r => r.ClientID).Distinct().ToList();

            ViewBag.EmailTemplates = (from emailtemplate in entityDB.EmailTemplates.Where(rec => rec.EmailUsedFor == 0 && rec.IsDocumentSpecific == false).OrderBy(o => o.Name).ToList()
                                      select new SelectListItem
                                      {
                                          Text = emailtemplate.Name,
                                          Value = emailtemplate.EmailTemplateID + ""
                                      }).ToList();
            ((List<SelectListItem>)ViewBag.EmailTemplates).Insert(0, new SelectListItem() { Text = "Select Email Template", Value = "-1" });
            LocalTemplateSubSectionsNotifications notifications = new LocalTemplateSubSectionsNotifications();

            notifications.ListOfAdminUsersForNotificationsTosent = (from users in entityDB.SystemUsersOrganizations.Where(rec => rec.Organizations.OrganizationType == "Admin" && rec.SystemUsers.UserStatus == true).ToList()
                                                                    select new SelectListItem
                                                                    {
                                                                        Text = users.SystemUsers.Contacts == null || users.SystemUsers.Contacts.Count == 0 ? "" : users.SystemUsers.Contacts.FirstOrDefault().LastName + ", " + users.SystemUsers.Contacts.FirstOrDefault().FirstName,//Rajesh on 2/11/2014
                                                                        Value = users.UserId.ToString()
                                                                    }).OrderBy(r => r.Text).ToList();

            if (!GetClientUsers(TemplateSubSectionId, TSubSec, notifications, clientId))
                return Json("Please set the client specific permissions for subsection before proceeding further.", JsonRequestBehavior.AllowGet);

            if (TSubSec.TemplateSubSectionsNotifications == null || TSubSec.TemplateSubSectionsNotifications.Count == 0)
            {
                notifications.TemplateSubSectionID = TemplateSubSectionId;
                notifications.TemplateSubSectionNotificationId = -1;
            }
            else
            {
                var notif = TSubSec.TemplateSubSectionsNotifications.FirstOrDefault();
                var adminRecipientUsers = from adminRecipients in entityDB.TemplateSubSectionsNotifications.Where(adminRecipients => adminRecipients.TemplateSubSectionID == notif.TemplateSubSectionID && adminRecipients.RecipientUserType == 0 && adminRecipients.NotifyUserDisabled == false)
                                          select adminRecipients; // Kiran on 9/27/2014

                notifications.AdminUsers = new List<string>();
                foreach (var adminUser in adminRecipientUsers)
                {
                    notifications.AdminUsers.Add(adminUser.RecipientUserId.ToString());
                }
                var clientRecipientusers = entityDB.TemplateSubSectionsNotifications.Where(clientRecipients => clientRecipients.TemplateSubSectionID == notif.TemplateSubSectionID && clientRecipients.RecipientUserType == 1 && clientRecipients.NotifyUserDisabled == false).ToList(); // Kiran on 9/27/2014
                notifications.ClientUsers = new List<string>();
                foreach (var clientUser in clientRecipientusers)
                {
                    notifications.ClientUsers.Add(clientUser.RecipientUserId.ToString());
                }
                // Kiran on 9/25/2014
                if (notifications.AdminUsers.Count == 0 || notifications.AdminUsers == null)
                    notifications.ForAdmin = false;
                else
                    notifications.ForAdmin = true;

                if (notifications.ClientUsers.Count == 0 || notifications.ClientUsers == null)
                    notifications.ForClient = false;
                else
                    notifications.ForClient = true;
                // Ends<<<
                notifications.EmaiTemplateId = notif.EmaiTemplateId;
                notifications.TemplateSubSectionNotificationId = notif.TemplateSubSectionNotificationId;
                notifications.ListOfAdminUsersForNotificationsTosent = (from users in entityDB.SystemUsersOrganizations.Where(rec => rec.Organizations.OrganizationType == "Admin" && rec.SystemUsers.UserStatus == true).ToList()
                                                                        select new SelectListItem
                                                                        {
                                                                            Text = users.SystemUsers.Contacts == null || users.SystemUsers.Contacts.Count == 0 ? "" : users.SystemUsers.Contacts.FirstOrDefault().LastName + ", " + users.SystemUsers.Contacts.FirstOrDefault().FirstName,//Rajesh on 2/11/2014
                                                                            Value = users.UserId.ToString(),
                                                                            Selected = notifications.AdminUsers.Contains(users.UserId + "")
                                                                        }).OrderBy(r => r.Text).ToList();
                if (!GetClientUsers(TemplateSubSectionId, TSubSec, notifications, clientId))
                    return Json("Please set the client specific permissions for subsection before proceeding further.", JsonRequestBehavior.AllowGet);

                return View(notifications);
            }
            return View(notifications);
        }
        public bool GetClientUsers(long TemplateSubSectionId, TemplateSubSections TSubSec, LocalTemplateSubSectionsNotifications notifications, List<long> clientId)
        {
            if (TSubSec.TemplateSections.TemplateSectionsPermission.FirstOrDefault().VisibleTo == 2 && TSubSec.TemplateSections.Templates.IsERI)
            {
                getSubsectionSpecificClientUsers(TemplateSubSectionId, TSubSec, notifications);
                if (!notifications.ListOfClientUsersForNotificationsTosent.Any())
                {
                    return false;
                }
            }
            else
                notifications.ListOfClientUsersForNotificationsTosent = (from users in entityDB.SystemUsersOrganizations.Where(rec => rec.Organizations.OrganizationType == "Client" && clientId.Contains(rec.OrganizationId)).ToList()
                                                                         select new SelectListItem
                                                                         {
                                                                             Text = users.SystemUsers.Contacts == null || users.SystemUsers.Contacts.Count == 0 ? users.SystemUsers.Email : users.SystemUsers.Contacts.FirstOrDefault().LastName + ", " + users.SystemUsers.Contacts.FirstOrDefault().FirstName,
                                                                             Value = users.UserId.ToString(),
                                                                             Selected = notifications.ClientUsers != null ? notifications.ClientUsers.Contains(users.UserId + "") : false
                                                                         }).OrderBy(r => r.Text).ToList();
            return true;
        }

        private void getSubsectionSpecificClientUsers(long TemplateSubSectionId, TemplateSubSections TSubSec, LocalTemplateSubSectionsNotifications notifications)
        {

            {
                var clientUserAccess = entityDB.TemplateSubSectionsPermissions.FirstOrDefault(rec => rec.SubSectionId == TemplateSubSectionId && rec.PermissionFor == "Client").TemplateSubSectionsUserPermissions.ToList();
                if (clientUserAccess == null)
                {
                    return;
                }

                //var clientSubSectionNotification = entityDB.TemplateSubSectionsNotifications.FirstOrDefault(rec => rec.RecipientUserType == 1 && rec.TemplateSubSectionID == TemplateSubSectionId);
                List<long> clientOrganizationId = new List<long>();
                foreach (var access in clientUserAccess)
                {
                    var OrganizationId = entityDB.SystemUsersOrganizations.FirstOrDefault(rec => rec.UserId == access.UserId).OrganizationId;
                    clientOrganizationId.Add(OrganizationId);
                }

                notifications.ListOfClientUsersForNotificationsTosent = (from users in entityDB.SystemUsersOrganizations.Where(rec => rec.Organizations.OrganizationType == "Client" && clientOrganizationId.Distinct().Contains(rec.OrganizationId)).ToList()
                                                                         select new SelectListItem
                                                                         {
                                                                             Text = users.SystemUsers.Contacts == null || users.SystemUsers.Contacts.Count == 0 ? users.SystemUsers.Email : users.SystemUsers.Contacts.FirstOrDefault().LastName + ", " + users.SystemUsers.Contacts.FirstOrDefault().FirstName,
                                                                             Value = users.UserId.ToString(),
                                                                             Selected = notifications.ClientUsers != null ? notifications.ClientUsers.Contains(users.UserId + "") : false
                                                                         }).OrderBy(r => r.Text).ToList();


            }
        }

        [HttpPost]
        public ActionResult SubSectionNotifications(LocalTemplateSubSectionsNotifications form)
        {
            var flag = true;
            if (form.ForAdmin == false && form.ForClient == false)
            {
                ModelState.AddModelError("", "Please Select atleast one user to send notifications");
                ViewBag.ErrorMessage = "Please Select atleast one user to send notifications";
                flag = false;
            }
            // Kiran on 9/23/2014
            if ((form.ForAdmin == true && form.AdminUsers == null) || (form.ForAdmin == true && form.AdminUsers.Count == 0))
            {
                flag = false;
            }
            if ((form.ForClient == true && form.ClientUsers == null) || (form.ForClient == true && form.ClientUsers.Count == 0))
            {
                flag = false;
            }
            // Ends<<<
            if (form.EmaiTemplateId == -1)
            {
                flag = false;
                ModelState.AddModelError("EmaiTemplateId", "*");
            }
            if (!flag)
            {
                var TSubSec = entityDB.TemplateSubSections.Find(form.TemplateSubSectionID);

                var clientId = TSubSec.TemplateSections.Templates.ClientTemplates.Select(r => r.ClientID).ToList();
                ViewBag.EmailTemplates = (from emailtemplate in entityDB.EmailTemplates.Where(rec => rec.EmailUsedFor == 0).OrderBy(o => o.Name).ToList()
                                          select new SelectListItem
                                          {
                                              Text = emailtemplate.Name,
                                              Value = emailtemplate.EmailTemplateID + ""
                                          }).ToList();
                ((List<SelectListItem>)ViewBag.EmailTemplates).Insert(0, new SelectListItem() { Text = "Select Email Template", Value = "-1" });

                LocalTemplateSubSectionsNotifications notifications = new LocalTemplateSubSectionsNotifications();

                // Kiran on 9/25/2014
                form.ListOfAdminUsersForNotificationsTosent = (from users in entityDB.SystemUsersOrganizations.Where(rec => rec.Organizations.OrganizationType == "Admin" && rec.SystemUsers.UserStatus == true).ToList()
                                                               select new SelectListItem
                                                               {
                                                                   Text = users.SystemUsers.Contacts == null || users.SystemUsers.Contacts.Count == 0 ? "" : users.SystemUsers.Contacts.FirstOrDefault().LastName + ", " + users.SystemUsers.Contacts.FirstOrDefault().FirstName,//Rajesh on 2/11/2014
                                                                   Value = users.UserId.ToString()
                                                               }).OrderBy(r => r.Text).ToList();

                //form.ListOfClientUsersForNotificationsTosent = (from users in entityDB.SystemUsersOrganizations.Where(rec => rec.Organizations.OrganizationType == "Client" && rec.OrganizationId == clientId).ToList()
                //                                                select new SelectListItem
                //                                                {
                //                                                    Text = users.SystemUsers.Contacts == null || users.SystemUsers.Contacts.Count == 0 ? users.SystemUsers.Email : users.SystemUsers.Contacts.FirstOrDefault().LastName + ", " + users.SystemUsers.Contacts.FirstOrDefault().FirstName,
                //                                                    Value = users.UserId.ToString()
                //                                                }).ToList();
                if (!GetClientUsers(form.TemplateSubSectionID, TSubSec, notifications, clientId))
                    return Json("Please set the client specific permissions for subsection before proceeding further.", JsonRequestBehavior.AllowGet);


                return View(form);
            }

            if (form.TemplateSubSectionNotificationId == -1)
            {
                // Kiran on 9/23/2014
                TemplateSubSectionsNotifications notification = new TemplateSubSectionsNotifications();
                notification.EmaiTemplateId = (int)form.EmaiTemplateId;
                notification.TemplateSubSectionID = form.TemplateSubSectionID;
                notification.SenderType = form.SenderType;
                switch (notification.SenderType)
                {
                    case 0: notification.SenderEmailId = "notification@firstverify.com"; break;
                    case 1: notification.SenderEmailId = form.SenderEmailId; break;
                    case 2: notification.SenderEmailId = form.SenderEmailId; break;
                }
                if (form.ForAdmin)
                    foreach (var admin in form.AdminUsers)
                    {
                        notification.RecipientUserId = new Guid(admin);
                        notification.RecipientUserType = 0;
                        notification.NotifyUserDisabled = false; // Kiran on 9/27/2014
                        entityDB.TemplateSubSectionsNotifications.Add(notification);
                        entityDB.SaveChanges();
                    }
                if (form.ForClient)
                    foreach (var client in form.ClientUsers)
                    {
                        notification.RecipientUserId = new Guid(client);
                        notification.RecipientUserType = 1;
                        notification.NotifyUserDisabled = false; // Kiran on 9/27/2014
                        entityDB.TemplateSubSectionsNotifications.Add(notification);
                        entityDB.SaveChanges();
                    }
            }
            else
            {
                // Kiran on 9/27/2014
                var existingTemplateSubSectionNotifications = entityDB.TemplateSubSectionsNotifications.Where(record => record.TemplateSubSectionID == form.TemplateSubSectionID).ToList();

                if (existingTemplateSubSectionNotifications != null)
                {
                    foreach (var setupNotification in existingTemplateSubSectionNotifications)
                    {
                        if (setupNotification.PrequalificationNotifications.Count() == 0)
                        {
                            entityDB.TemplateSubSectionsNotifications.Remove(setupNotification);
                        }
                        else
                        {
                            setupNotification.EmaiTemplateId = (int)form.EmaiTemplateId;
                            setupNotification.NotifyUserDisabled = true;
                            entityDB.Entry(setupNotification).State = EntityState.Modified;
                        }
                    }
                    entityDB.SaveChanges();

                    TemplateSubSectionsNotifications notification = new TemplateSubSectionsNotifications();
                    notification.EmaiTemplateId = (int)form.EmaiTemplateId;
                    notification.TemplateSubSectionID = form.TemplateSubSectionID;
                    notification.SenderType = form.SenderType;
                    switch (notification.SenderType)
                    {
                        case 0: notification.SenderEmailId = "notification@firstverify.com"; break;
                        case 1: notification.SenderEmailId = form.SenderEmailId; break;
                        case 2: notification.SenderEmailId = form.SenderEmailId; break;
                    }
                    if (form.ForAdmin)
                        foreach (var admin in form.AdminUsers)
                        {
                            if (entityDB.TemplateSubSectionsNotifications.FirstOrDefault(rec => rec.TemplateSubSectionID == form.TemplateSubSectionID && rec.RecipientUserId == new Guid(admin)) == null)
                            {
                                notification.RecipientUserId = new Guid(admin);
                                notification.RecipientUserType = 0;
                                notification.NotifyUserDisabled = false;
                                entityDB.TemplateSubSectionsNotifications.Add(notification);
                                entityDB.SaveChanges();
                            }
                            else
                            {
                                var currentUserSetUpNotification = entityDB.TemplateSubSectionsNotifications.FirstOrDefault(rec => rec.TemplateSubSectionID == form.TemplateSubSectionID && rec.RecipientUserId == new Guid(admin));
                                currentUserSetUpNotification.NotifyUserDisabled = false;
                                entityDB.Entry(currentUserSetUpNotification).State = EntityState.Modified;
                                entityDB.SaveChanges();
                            }
                        }
                    if (form.ForClient)
                        foreach (var client in form.ClientUsers)
                        {
                            if (entityDB.TemplateSubSectionsNotifications.FirstOrDefault(rec => rec.TemplateSubSectionID == form.TemplateSubSectionID && rec.RecipientUserId == new Guid(client)) == null)
                            {
                                notification.RecipientUserId = new Guid(client);
                                notification.RecipientUserType = 1;
                                notification.NotifyUserDisabled = false;
                                entityDB.TemplateSubSectionsNotifications.Add(notification);
                                entityDB.SaveChanges();
                            }
                            else
                            {
                                var currentUserSetUpNotification = entityDB.TemplateSubSectionsNotifications.FirstOrDefault(rec => rec.TemplateSubSectionID == form.TemplateSubSectionID && rec.RecipientUserId == new Guid(client));
                                currentUserSetUpNotification.NotifyUserDisabled = false;
                                entityDB.Entry(currentUserSetUpNotification).State = EntityState.Modified;
                                entityDB.SaveChanges();
                            }
                        }
                }// Ends<<<
            }
            entityDB.SaveChanges();
            return Redirect("PopUpCloseView");
        }

        //Ends<<

        //Rajesh on 2/11/2014
        public ActionResult SubSectionUserPermission(long SubSectionId, long clientId = 0)
        {
            var SubSection = entityDB.TemplateSubSections.Find(SubSectionId);
            var subSecPermission = SubSection.TemplateSubSectionsPermissions.FirstOrDefault(rec => rec.PermissionFor == "Client");
            var sectionPermission = SubSection.TemplateSections.TemplateSectionsPermission.FirstOrDefault().VisibleTo;

            LocalTemplateSubSectionUserPermission permission = new LocalTemplateSubSectionUserPermission();
            permission.SubSectionId = SubSectionId;
            permission.UserPermissions = new List<UserPermissions>();


            //if (sectionPermission == 2)//For Client
            {
                //var clientTemplat = SubSection.TemplateSections.Templates.ClientTemplates.FirstOrDefault();
                //if (clientId != 0)
                //{
                //    clientTemplat = SubSection.TemplateSections.Templates.ClientTemplates.FirstOrDefault(r => r.ClientID == clientId);
                //}
                //else
                //{
                //    if (subSecPermission.TemplateSubSectionsUserPermissions.Any())
                //    {
                //        clientId = subSecPermission.TemplateSubSectionsUserPermissions.FirstOrDefault().SystemUsers.SystemUsersOrganizations.FirstOrDefault().OrganizationId;
                //        clientTemplat = SubSection.TemplateSections.Templates.ClientTemplates.FirstOrDefault(r => r.ClientID == clientId);
                //    }
                //    else
                //        clientId = clientTemplat.ClientID;
                //}
                //var clienttemplates = SubSection.TemplateSections.Templates.ClientTemplates.Where(rec => rec.ClientID != -1);
                ////If has user input data.
                //if (SubSection.Questions.Any(r => r.QuestionColumnDetails.Any(r1 => r1.PrequalificationUserInputs.Any())))
                //{
                //    try
                //    {
                //        if (subSecPermission.TemplateSubSectionsUserPermissions.Any())
                //        {
                //            clientId = subSecPermission.TemplateSubSectionsUserPermissions.FirstOrDefault().SystemUsers.SystemUsersOrganizations.FirstOrDefault().OrganizationId;
                //        }
                //        clienttemplates = clienttemplates.Where(r => r.ClientID == clientId).ToList();
                //    }
                //    catch { }
                //}
                //permission.ClientId = clientId;
                //var clients = clienttemplates.Select(r => new SelectListItem() { Value = r.Organizations.OrganizationID.ToString(), Text = r.Organizations.Name, Selected = r.ClientID == clientId }).OrderBy(r => r.Text).ToList();
                ////clients.Add(new SelectListItem() { Text = "Select", Value = "0" });
                //ViewBag.clients = clients;

                var clientTemplat = SubSection.TemplateSections.Templates.ClientTemplates.Select(r => r.ClientID).Distinct();
                //var SuperClients = entityDB.SubClients.Select(r => r.SuperClientId).ToList();

                List<SystemUsersOrganizations> sysorgs = entityDB.SystemUsersOrganizations.Where(r => clientTemplat.Contains(r.OrganizationId)).ToList();
                //if (clientTemplat != null)

                foreach (var sysOrg in sysorgs)
                {
                    UserPermissions uPermission = new UserPermissions();
                    uPermission.UserId = sysOrg.UserId;
                    uPermission.ClientName = sysOrg.Organizations.Name;
                    uPermission.ClientId = sysOrg.OrganizationId;
                    uPermission.UserName = sysOrg.SystemUsers.Contacts == null || sysOrg.SystemUsers.Contacts.Count == 0 ? sysOrg.SystemUsers.Email : sysOrg.SystemUsers.Contacts.FirstOrDefault().LastName + ", " + sysOrg.SystemUsers.Contacts.FirstOrDefault().FirstName;
                    uPermission.Edit = false;
                    uPermission.Read = false;
                    permission.UserPermissions.Add(uPermission);
                }

            }
            if (subSecPermission.TemplateSubSectionsUserPermissions != null && subSecPermission.TemplateSubSectionsUserPermissions.Count != 0)
            {
                foreach (var SubPermission in subSecPermission.TemplateSubSectionsUserPermissions.ToList())
                {
                    try
                    {
                        var details = permission.UserPermissions.FirstOrDefault(rec => rec.UserId == SubPermission.UserId);
                        details.Edit = SubPermission.EditAccess;
                        details.Read = SubPermission.ReadOnlyAccess;
                    }
                    catch { }
                }
            }

            permission.UserPermissions = permission.UserPermissions.OrderBy(r => r.ClientName).ThenBy(r => r.UserName).ToList();

            return View(permission);
        }
        [HttpPost]
        public ActionResult SubSectionUserPermission(LocalTemplateSubSectionUserPermission form)
        {
            var SubSection = entityDB.TemplateSubSections.Find(form.SubSectionId);
            var subSecPermission = SubSection.TemplateSubSectionsPermissions.FirstOrDefault(rec => rec.PermissionFor == "Client");
            var clientTemplat = SubSection.TemplateSections.Templates.ClientTemplates.FirstOrDefault();
            if (form.ClientId != 0)
            {
                clientTemplat = SubSection.TemplateSections.Templates.ClientTemplates.FirstOrDefault(r => r.ClientID == form.ClientId);
            }
            else
            {
                form.ClientId = clientTemplat.ClientID;
            }
            var clienttemplates = SubSection.TemplateSections.Templates.ClientTemplates;
            //If has user input data.
            if (SubSection.Questions.Any(r => r.QuestionColumnDetails.Any(r1 => r1.PrequalificationUserInputs.Any())))
            {
                try
                {
                    if (subSecPermission.TemplateSubSectionsUserPermissions.Any())
                    {
                        form.ClientId = subSecPermission.TemplateSubSectionsUserPermissions.FirstOrDefault().SystemUsers.SystemUsersOrganizations.FirstOrDefault().OrganizationId;
                    }
                    clienttemplates.Where(r => r.ClientID == form.ClientId);
                }
                catch { }
            }

            var clients = clienttemplates.Select(r => new SelectListItem() { Value = r.Organizations.OrganizationID.ToString(), Text = r.Organizations.Name, Selected = r.ClientID == form.ClientId }).OrderBy(r => r.Text).ToList();
            //clients.Add(new SelectListItem() { Text = "Select", Value = "0" });
            ViewBag.clients = clients;

            if (form.UserPermissions == null || form.UserPermissions.Where(rec => rec.Edit == true || rec.Read == true).ToList().Count == 0)
            {
                ViewBag.ErrorMessage = "Please select atleaset one user to set user permissions";
                return View(form);
            }

            var subSectionPermissionId = SubSection.TemplateSubSectionsPermissions.FirstOrDefault(rec => rec.PermissionFor == "Client").SubSectionPermissionId;
            try
            {
                foreach (var permission in entityDB.TemplateSubSections.Find(form.SubSectionId).TemplateSubSectionsPermissions.FirstOrDefault(rec => rec.PermissionFor == "Client").TemplateSubSectionsUserPermissions.ToList())
                {
                    entityDB.TemplateSubSectionsUserPermissions.Remove(permission);
                }
            }
            catch { }
            foreach (var permission in form.UserPermissions.Where(rec => rec.Edit == true || rec.Read == true).ToList())
            {
                TemplateSubSectionsUserPermissions Upermission = new TemplateSubSectionsUserPermissions();
                Upermission.EditAccess = permission.Edit;
                Upermission.ReadOnlyAccess = permission.Read;
                Upermission.SubSectionPermissionId = subSectionPermissionId;
                Upermission.UserId = permission.UserId;
                entityDB.TemplateSubSectionsUserPermissions.Add(Upermission);
            }
            entityDB.SaveChanges();
            return Redirect("PopUpCloseView");
        }

        //Ends<<<

        //Rajesh on 2/11/2014
        public ActionResult GetClientOrAdminUsers(long SubSecId, int type)//0-Admin,1-Client
        {


            var list = (from users in entityDB.SystemUsersOrganizations.Where(rec => rec.Organizations.OrganizationType == "Admin").ToList()
                        select new SelectListItem
                        {
                            Text = users.SystemUsers.Contacts == null || users.SystemUsers.Contacts.Count == 0 ? "" : users.SystemUsers.Contacts.FirstOrDefault().LastName + ", " + users.SystemUsers.Contacts.FirstOrDefault().FirstName,//Rajesh on 2/11/2014
                            Value = users.SystemUsers.Email + ""//Rajesh on 2/17/2014
                        }).ToList();
            if (type == 1)
            {
                var TSubSec = entityDB.TemplateSubSections.Find(SubSecId);
                var clientId = TSubSec.TemplateSections.Templates.ClientTemplates.FirstOrDefault().ClientID;


                list = (from orgs in entityDB.SystemUsersOrganizations.Where(rec => rec.OrganizationId == clientId).ToList()
                        select new SelectListItem
                        {
                            Text = orgs.SystemUsers.Contacts == null || orgs.SystemUsers.Contacts.Count == 0 ? "" : orgs.SystemUsers.Contacts.FirstOrDefault().LastName + ", " + orgs.SystemUsers.Contacts.FirstOrDefault().FirstName,//Rajesh on 2/11/2014
                            Value = orgs.SystemUsers.Email + ""
                        }).ToList();

            }
            return Json(list);
        }
        //Ends<<<
        private static void CreateTemplateSubSecPermission(int permissionType, TemplateSubSections subSection, TemplateSections TSection)
        {
            //Rajesh on 2/15/2014
            try
            {
                var VisibleTo = 3;
                try
                {
                    VisibleTo = VisibleTo = TSection.TemplateSectionsPermission.FirstOrDefault().VisibleTo;
                }
                catch { }
                if (VisibleTo == 2)
                {
                    TemplateSubSectionsPermissions permission = new TemplateSubSectionsPermissions();
                    permission.SectionsPermissionId = TSection.TemplateSectionsPermission.FirstOrDefault().SectionsPermissionId;
                    permission.PermissionType = 0;
                    permission.PermissionFor = "Admin";
                    subSection.TemplateSubSectionsPermissions = new List<TemplateSubSectionsPermissions>();
                    subSection.TemplateSubSectionsPermissions.Add(permission);

                    permission = new TemplateSubSectionsPermissions();
                    permission.SectionsPermissionId = TSection.TemplateSectionsPermission.FirstOrDefault().SectionsPermissionId;
                    permission.PermissionType = permissionType;
                    permission.PermissionFor = "Client";

                    subSection.TemplateSubSectionsPermissions.Add(permission);

                    permission = new TemplateSubSectionsPermissions();
                    permission.SectionsPermissionId = TSection.TemplateSectionsPermission.FirstOrDefault().SectionsPermissionId;
                    permission.PermissionType = permissionType;
                    permission.PermissionFor = OrganizationType.SuperClient;

                    subSection.TemplateSubSectionsPermissions.Add(permission);

                }
                else if (VisibleTo == 3 || VisibleTo == 4 || VisibleTo == 5)//RAB - Clayco project
                {
                    TemplateSubSectionsPermissions permission = new TemplateSubSectionsPermissions();
                    permission.SectionsPermissionId = TSection.TemplateSectionsPermission.FirstOrDefault().SectionsPermissionId;
                    permission.PermissionType = 0;
                    permission.PermissionFor = "Admin";
                    subSection.TemplateSubSectionsPermissions = new List<TemplateSubSectionsPermissions>();
                    subSection.TemplateSubSectionsPermissions.Add(permission);

                    permission = new TemplateSubSectionsPermissions();
                    permission.SectionsPermissionId = TSection.TemplateSectionsPermission.FirstOrDefault().SectionsPermissionId;
                    permission.PermissionType = permissionType;
                    permission.PermissionFor = "Client";

                    subSection.TemplateSubSectionsPermissions.Add(permission);
                    permission = new TemplateSubSectionsPermissions();
                    permission.SectionsPermissionId = TSection.TemplateSectionsPermission.FirstOrDefault().SectionsPermissionId;
                    permission.PermissionType = permissionType;
                    permission.PermissionFor = OrganizationType.SuperClient;

                    subSection.TemplateSubSectionsPermissions.Add(permission);
                    permission = new TemplateSubSectionsPermissions();
                    permission.SectionsPermissionId = TSection.TemplateSectionsPermission.FirstOrDefault().SectionsPermissionId;
                    permission.PermissionType = permissionType;
                    permission.PermissionFor = "Vendor";

                    subSection.TemplateSubSectionsPermissions.Add(permission);
                }
                // Kiran on 4/22/2014
                else if (VisibleTo == 1)
                {
                    TemplateSubSectionsPermissions permission = new TemplateSubSectionsPermissions();
                    permission.SectionsPermissionId = TSection.TemplateSectionsPermission.FirstOrDefault().SectionsPermissionId;
                    permission.PermissionType = 0;
                    permission.PermissionFor = "Admin";
                    subSection.TemplateSubSectionsPermissions = new List<TemplateSubSectionsPermissions>();
                    subSection.TemplateSubSectionsPermissions.Add(permission);

                    permission = new TemplateSubSectionsPermissions();
                    permission.SectionsPermissionId = TSection.TemplateSectionsPermission.FirstOrDefault().SectionsPermissionId;
                    permission.PermissionType = permissionType;
                    permission.PermissionFor = "Vendor";

                    subSection.TemplateSubSectionsPermissions.Add(permission);
                }
                // Ends<<<
            }
            catch { }
            //Ends<<
        }
        public ActionResult AssignUnits(Guid TemplateId)
        {
            var clientTemplate = entityDB.ClientTemplates.Where(rec => rec.TemplateID == TemplateId).ToList();
            //var ClientTemplateID = entityDB.ClientTemplates.Where(rec => rec.TemplateID == TemplateId).Select(r => r.ClientTemplateID).ToList();
            //var isParent = entityDB.OrganizationsJVs.FirstOrDefault(rec => rec.JointVenturesID == clientTemplate.ClientID) == null;
            List<long> orgIds = new List<long>();
            //var pqid = entityDB.Prequalification.Where(r => ClientTemplateID.Contains(r.ClientTemplateId.Value)).Select(r => r.PrequalificationId).ToList();
            //var Bu = entityDB.PrequalificationSites.Where(r => pqid.Contains(r.PrequalificationId.Value)).Select(r => r.ClientBusinessUnitId).Distinct().ToList();
            //ViewBag.Bu = Bu;

            List<string> Prices = new List<string>();
            var clientTemplateIds = clientTemplate.Select(r => r.ClientTemplateID);
            var ClienttemplateGroupBUs = entityDB.ClientTemplatesBUGroupPrice.Where(rec => clientTemplateIds.Contains((Guid)rec.ClientTemplateID) && rec.GroupingFor == 0).OrderBy(rec => rec.GroupPriceType).ToList();

            if (ClienttemplateGroupBUs.Count == 0 && clientTemplate.FirstOrDefault().Templates.IsERI)
                orgIds.AddRange(clientTemplate.Where(rec => rec.Organizations.OrganizationType != OrganizationType.SuperClient).Select(r => r.Organizations.OrganizationID));
            else
                orgIds.AddRange(clientTemplate.Select(r => r.Organizations.OrganizationID));
            var assignBUs = entityDB.ClientBusinessUnits.Where(rec => orgIds.Contains(rec.ClientId))
                .Select(rec => new LocalBUDetails
                {
                    BUId = rec.ClientBusinessUnitId,
                    BUName = rec.BusinessUnitName,
                    OrgName = rec.Organization.Name,
                    GroupNumber = -1,
                    IsDeletable = !rec.PrequalificationSites.Any(),
                    ClientId = rec.ClientId
                }).OrderBy(rec => rec.BUName).ToList();

            int i = 0;
            foreach (var group in ClienttemplateGroupBUs)
            {
                var ClientBUIds = group.ClientTemplatesForBU.Select(rec => rec.ClientBusinessUnitId).ToList();
                assignBUs.ForEach(rec => rec.GroupNumber = ClientBUIds.Contains(rec.BUId) ? i : rec.GroupNumber);
                Prices.Add(group.GroupPrice + "");
                i++;
            }
            if (assignBUs.Count == 0 && ClienttemplateGroupBUs.Count == 0)
            {
                ViewBag.VBCount = 0;

            }
            if (ClienttemplateGroupBUs.Count == 0 && clientTemplate.FirstOrDefault().Templates.IsERI)
            {
                var superClient = clientTemplate.FirstOrDefault(r => r.Organizations.OrganizationType == OrganizationType.SuperClient).ClientID;
                var eriBU = entityDB.ClientBusinessUnits.FirstOrDefault(rec => rec.ClientId == superClient);
                LocalBUDetails addBU = new LocalBUDetails();
                addBU.BUId = eriBU.ClientBusinessUnitId;
                addBU.BUName = eriBU.BusinessUnitName;
                addBU.OrgName = eriBU.Organization.Name;
                addBU.GroupNumber = 0;
                addBU.ClientId = eriBU.ClientId;
                assignBUs.Add(addBU);
                Prices.Add("0.00");
            }
            LocalModelForBusites BUTemplate = new LocalModelForBusites();
            BUTemplate.BUDetails = assignBUs.OrderBy(rec => rec.OrgName).ThenBy(rec => rec.BUName).ToList(); // sumanth on 10/23/2015 for FVOS-74
                                                                                                             //BUTemplate.ClientTemplateId = clientTemplate.Select(r=>r.ClientTemplateID).ToList();

            //var buid = BUTemplate.BUDetails.Select(r => r.BUId).ToList();
            //ViewBag.Buids = buid;
            BUTemplate.Price = Prices;
            BUTemplate.TemplateId = TemplateId;

            ViewBag.TemplateName = clientTemplate.FirstOrDefault().Templates.TemplateName;
            ViewBag.isERI = clientTemplate.FirstOrDefault().Templates.IsERI;
            ViewBag.OrgName = string.Join(", ", clientTemplate.Where(r => r.Organizations.OrganizationType != OrganizationType.SuperClient).Select(r => r.Organizations.Name));
            return View(BUTemplate);
        }
        [HttpPost]
        public ActionResult AssignUnits(LocalModelForBusites form)
        {
            var TemplateInfo = entityDB.Templates.Find(form.TemplateId);
            //Deleting all records
            var clientTemplateIds = TemplateInfo.ClientTemplates.Select(r => r.ClientTemplateID);
            var clientTemplateGroupIds = entityDB.ClientTemplatesBUGroupPrice.Where(rec => rec.GroupingFor == 0 && clientTemplateIds.Contains((Guid)rec.ClientTemplateID)).Select(rec => rec.ClientTemplateBUGroupId).ToList();

            var CTBUsList = entityDB.ClientTemplatesForBU.Where(rec => clientTemplateGroupIds.Contains((long)rec.ClientTemplateBUGroupId)).ToList();

            List<long?> groupPriceBUId = new List<long?>();
            foreach (var CTBU in CTBUsList)
            {
                if (CTBU.ClientTemplateBUGroupId != null && !groupPriceBUId.Contains(CTBU.ClientTemplateBUGroupId))
                {
                    groupPriceBUId.Add(CTBU.ClientTemplateBUGroupId);
                }
                entityDB.ClientTemplatesForBU.Remove(CTBU);
            }

            foreach (var ids in groupPriceBUId)
            {
                var CTBUPrice = entityDB.ClientTemplatesBUGroupPrice.Find(ids);
                entityDB.ClientTemplatesBUGroupPrice.Remove(CTBUPrice);
            }
            entityDB.SaveChanges();

            //Delete Ends<<<
            //if (TemplateInfo.IsERI)
            //{
            //    LocalBUDetails addERIBU = new LocalBUDetails();
            //    addERIBU.BUId = 0;
            //    addERIBU.ClientId = -1;
            //    addERIBU.BUName = "ERI Business Unit";
            //    addERIBU.GroupNumber = 0;
            //    addERIBU.OrgName = "ERI";
            //    form.BUDetails.Add(addERIBU);
            //}
            var groupNumber = -1;
            var GroupPrice = new ClientTemplatesBUGroupPrice();
            foreach (var BUTemplate in form.BUDetails.OrderBy(rec => rec.GroupNumber))// sumanth on 10/23/2015 for FVOS-74 to display BUs in alphabetical order.
            {
                var clientTemplateForBU = entityDB.ClientTemplatesForBU.FirstOrDefault(rec => clientTemplateIds.Contains((Guid)rec.ClientTemplateId) && rec.ClientTemplatesBUGroupPrice.GroupingFor == 0 && rec.ClientBusinessUnitId == BUTemplate.BUId);
                if (clientTemplateForBU == null)
                {
                    if (groupNumber != BUTemplate.GroupNumber)
                    {
                        GroupPrice = new ClientTemplatesBUGroupPrice();
                        GroupPrice.ClientTemplateID = entityDB.ClientTemplates.FirstOrDefault(r => r.ClientID == BUTemplate.ClientId && r.TemplateID == form.TemplateId).ClientTemplateID;
                        GroupPrice.GroupPrice = Convert.ToDecimal(form.Price[(int)BUTemplate.GroupNumber]);
                        GroupPrice.GroupingFor = 0;
                        GroupPrice.GroupPriceType = BUTemplate.GroupNumber == 0 ? 0 : 1;
                        entityDB.ClientTemplatesBUGroupPrice.Add(GroupPrice);
                        entityDB.SaveChanges();
                    }
                    groupNumber = (int)BUTemplate.GroupNumber;

                    clientTemplateForBU = new ClientTemplatesForBU();
                    clientTemplateForBU.ClientBusinessUnitId = BUTemplate.BUId;
                    clientTemplateForBU.ClientTemplateId = entityDB.ClientTemplates.FirstOrDefault(r => r.ClientID == BUTemplate.ClientId && r.TemplateID == form.TemplateId).ClientTemplateID;
                    clientTemplateForBU.ClientTemplateBUGroupId = GroupPrice.ClientTemplateBUGroupId;
                    entityDB.ClientTemplatesForBU.Add(clientTemplateForBU);
                }

            }
            //try
            //{
            //    var template = entityDB.Templates.FirstOrDefault(rec => rec.DefaultTemplate == false && rec.TemplateStatus == 0);
            //    template.TemplateStatus = 1;
            //    entityDB.Entry(template).State = EntityState.Modified;
            //}
            //catch { }
            entityDB.SaveChanges();
            //Guid TemplatedId = form.ClientTemplateId;
            //return Redirect("PopUpCloseView");
            //var TemplateId = entityDB.ClientTemplates.FirstOrDefault(rec => form.ClientTemplateId.Contains(rec.ClientTemplateID)).TemplateID;
            return Redirect("AssignAnnualUnits?TemplateId=" + form.TemplateId);
        }

        public ActionResult AssignAnnualUnits(Guid TemplateId)
        {
            //var clientTemplate = entityDB.ClientTemplates.FirstOrDefault(rec => rec.TemplateID == TemplateId);
            //var isParent = entityDB.OrganizationsJVs.FirstOrDefault(rec => rec.JointVenturesID == clientTemplate.ClientID) == null;
            List<long> orgIds = new List<long>();
            //if (isParent)
            //{
            //    orgIds = clientTemplate.Organizations.OrganizationsJVs.Select(rec => rec.JointVenturesID).ToList();
            //    orgIds.Add(clientTemplate.ClientID);
            //}
            //else
            //{
            //    orgIds = entityDB.OrganizationsJVs.FirstOrDefault(rec => rec.JointVenturesID == clientTemplate.ClientID).BaseCompany.OrganizationsJVs.Select(rec => rec.JointVenturesID).ToList();
            //}
            var template = entityDB.Templates.Find(TemplateId);
            var clientTemplatteIds = template.ClientTemplates.Where(r => r.Organizations.OrganizationType != OrganizationType.SuperClient).Select(r => r.ClientTemplateID);
            orgIds.AddRange(template.ClientTemplates.Where(row => row.Organizations.OrganizationType != OrganizationType.SuperClient).Select(r => r.Organizations.OrganizationID));
            var assignBUs = entityDB.ClientBusinessUnits.Where(rec => orgIds.Contains(rec.ClientId)).Select(rec => new LocalBUDetails { BUId = rec.ClientBusinessUnitId, BUName = rec.BusinessUnitName, OrgName = rec.Organization.Name, ClientId = rec.ClientId, GroupNumber = -1, }).OrderBy(rec => rec.BUName).ToList();//sumanth on 10/23/2015 for FVOS-74 dispaly BUs in alphabetical order.

            List<string> Prices = new List<string>();
            List<long?> PrequalificationTrainingAnnualFee = new List<long?>();
            var ClienttemplateGroupBUs = entityDB.ClientTemplatesBUGroupPrice.Where(rec => clientTemplatteIds.Contains((Guid)rec.ClientTemplateID) && rec.GroupingFor == 1).OrderBy(rec => rec.GroupPriceType).ToList();
            int i = 0;
            foreach (var group in ClienttemplateGroupBUs)
            {
                var ClientBUIds = group.ClientTemplatesForBU.Select(rec => rec.ClientBusinessUnitId).ToList();
                assignBUs.ForEach(rec => rec.GroupNumber = ClientBUIds.Contains(rec.BUId) ? group.GroupPriceType : rec.GroupNumber);
                if (group.GroupPriceType != -1)
                {
                    var PrequalificationTraining = entityDB.PrequalificationTrainingAnnualFees.FirstOrDefault(rec => rec.ClientTemplateBUGroupId == group.ClientTemplateBUGroupId);
                    if (PrequalificationTraining != null)
                        PrequalificationTrainingAnnualFee.Add(PrequalificationTraining.PreqAnnualFeeRecId);
                    else
                        PrequalificationTrainingAnnualFee.Add(0);
                    Prices.Add(group.GroupPrice + "");
                }
                i++;
            }

            LocalModelForBusites BUTemplate = new LocalModelForBusites();
            BUTemplate.BUDetails = assignBUs.OrderBy(rec => rec.OrgName).ThenBy(rec => rec.BUName).ToList();//sumanth on 10/23/2015 for FVOS-74 dispaly BUs in alphabetical order.
            //BUTemplate.ClientTemplateId[0] = clientTemplate.ClientTemplateID;
            BUTemplate.TemplateId = template.TemplateID;
            BUTemplate.Price = Prices;
            BUTemplate.PrequalificationTrainingChek = PrequalificationTrainingAnnualFee;


            ViewBag.TemplateName = template.TemplateName;
            ViewBag.OrgName = String.Join(", ", template.ClientTemplates.Where(r => r.Organizations.OrganizationType != OrganizationType.SuperClient).Select(r => r.Organizations.Name));
            return View(BUTemplate);
        }

        [HttpPost]
        public ActionResult AssignAnnualUnits(LocalModelForBusites form)
        {
            var clientIds = form.BUDetails.Select(r => r.ClientId);
            var clienttemplateIds = entityDB.ClientTemplates.Where(r => clientIds.Contains(r.ClientID) && r.TemplateID == form.TemplateId).Select(r => r.ClientTemplateID).ToList();

            foreach (var BUTemplate in form.BUDetails.OrderBy(rec => rec.GroupNumber).OrderBy(rec => rec.BUName))//sumanth on 10/23/2015 for FVOS-74 dispaly BUs in alphabetical order.
            {
                ClientTemplatesBUGroupPrice GroupPrice = new ClientTemplatesBUGroupPrice();
                //var clientTemplateBUGroupPrice = entityDB.ClientTemplatesBUGroupPrice.FirstOrDefault(rec => rec.ClientTemplateID == form.ClientTemplateId[0] && rec.GroupPriceType == BUTemplate.GroupNumber && rec.GroupingFor == 1);
                var clientTemplateBUGroupPrice = entityDB.ClientTemplatesBUGroupPrice.
                    FirstOrDefault(rec => clienttemplateIds.Contains((Guid)rec.ClientTemplateID)
                    && rec.GroupPriceType == BUTemplate.GroupNumber
                    && rec.GroupingFor == 1);
                GroupPrice.ClientTemplateID = entityDB.ClientTemplates.FirstOrDefault(r => r.ClientID == BUTemplate.ClientId && r.TemplateID == form.TemplateId).ClientTemplateID;
                GroupPrice.GroupingFor = 1;
                if (BUTemplate.GroupNumber != -1)
                {
                    GroupPrice.GroupPrice = Convert.ToDecimal(form.Price[(int)BUTemplate.GroupNumber]);
                    GroupPrice.GroupPriceType = BUTemplate.GroupNumber;
                }
                else
                {
                    GroupPrice.GroupPrice = 0;
                    GroupPrice.GroupPriceType = -1;
                }
                if (clientTemplateBUGroupPrice == null)
                {
                    entityDB.ClientTemplatesBUGroupPrice.Add(GroupPrice);
                }
                else if (clientTemplateBUGroupPrice != null && BUTemplate.GroupNumber != -1)
                {
                    clientTemplateBUGroupPrice.GroupPrice = Convert.ToDecimal(form.Price[(int)BUTemplate.GroupNumber]);
                    entityDB.Entry(clientTemplateBUGroupPrice).State = EntityState.Modified;
                }
                entityDB.SaveChanges();

                var checkforTemplatesBU = entityDB.ClientTemplatesForBU.FirstOrDefault(rec => clienttemplateIds.Contains((Guid)rec.ClientTemplateId) && rec.ClientBusinessUnitId == BUTemplate.BUId && rec.ClientTemplatesBUGroupPrice.GroupingFor == 1);
                ClientTemplatesForBU BU = new ClientTemplatesForBU();
                BU.ClientTemplateId = entityDB.ClientTemplates.FirstOrDefault(r => r.ClientID == BUTemplate.ClientId && r.TemplateID == form.TemplateId).ClientTemplateID;
                BU.ClientBusinessUnitId = BUTemplate.BUId;
                if (clientTemplateBUGroupPrice == null)
                    BU.ClientTemplateBUGroupId = GroupPrice.ClientTemplateBUGroupId;
                else
                    BU.ClientTemplateBUGroupId = clientTemplateBUGroupPrice.ClientTemplateBUGroupId;
                if (checkforTemplatesBU == null)
                {
                    entityDB.ClientTemplatesForBU.Add(BU);
                    entityDB.SaveChanges();
                }
                else
                {
                    if (clientTemplateBUGroupPrice == null)
                        checkforTemplatesBU.ClientTemplateBUGroupId = GroupPrice.ClientTemplateBUGroupId;
                    else
                        checkforTemplatesBU.ClientTemplateBUGroupId = clientTemplateBUGroupPrice.ClientTemplateBUGroupId;
                    entityDB.Entry(checkforTemplatesBU).State = EntityState.Modified;
                    entityDB.SaveChanges();
                }

            }

            var formgroupnos = form.BUDetails.Select(rec => rec.GroupNumber).Distinct();
            var dbgroupnos = entityDB.ClientTemplatesBUGroupPrice.Where(rec => clienttemplateIds.Contains((Guid)rec.ClientTemplateID) && rec.GroupingFor == 1 && !formgroupnos.Contains(rec.GroupPriceType)).ToList();
            foreach (var groups in dbgroupnos)
            {
                var prequalificationTrainingAnnualFees = entityDB.PrequalificationTrainingAnnualFees.FirstOrDefault(rec => rec.ClientTemplateBUGroupId == groups.ClientTemplateBUGroupId);
                if (prequalificationTrainingAnnualFees == null && groups.GroupPriceType != 0)
                    entityDB.Entry(groups).State = EntityState.Deleted;
                entityDB.SaveChanges();
            }

            try
            {
                var template = entityDB.Templates.FirstOrDefault(rec => rec.DefaultTemplate == false && rec.TemplateStatus == 0);
                template.TemplateStatus = 1;
                entityDB.Entry(template).State = EntityState.Modified;
            }
            catch { }
            entityDB.SaveChanges();
            return Redirect("PopUpCloseView");
        }
        public JsonResult GetTemplateLog(Guid templateId)
        {
            var data = entityDB.TemplateLog.Where(rec => rec.TemplateId == templateId).Select(rec => new
            {
                FullName = rec.SystemUsers.Contacts.FirstOrDefault().FirstName + " " + rec.SystemUsers.Contacts.FirstOrDefault().LastName,
                rec.ChangedDate,
                rec.LockedByDefaultVal
            }).OrderByDescending(rec => rec.ChangedDate).ToList();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult AddClients(Guid templateId)
        {
            var addedClientsForTemplate = new List<long>();
            addedClientsForTemplate = entityDB.ClientTemplates.Where(row => row.TemplateID == templateId).Select(rec => rec.ClientID).ToList();
            var superClientId = entityDB.ClientTemplates.Where(r => r.TemplateID == templateId && r.Organizations.OrganizationType == OrganizationType.SuperClient).FirstOrDefault().ClientID;

            var subClients = entityDB.SubClients.Where(r => r.SuperClientId == superClientId && r.IsActive != false).Select(rec => rec.ClientId).ToList();
            ViewBag.Clients = (from clients in entityDB.Organizations.Where(rec => rec.OrganizationType == "Client" && !addedClientsForTemplate.Contains(rec.OrganizationID) && subClients.Contains(rec.OrganizationID)).OrderBy(row => row.Name).ToList()
                               select new SelectListItem
                               {
                                   Text = clients.Name,
                                   Value = clients.OrganizationID.ToString()
                               }).ToList();
            //ViewBag.clients = (from clients in entityDB.SubClients.Where(rec => rec.SuperClientId == superClientId && !addedClientsForTemplate.Contains(rec.ClientId)).OrderBy(r => r.SubClientOrganization.Name).ToList()
            //                  select new SelectListItem
            //                  {
            //                      Text = clients.SubClientOrganization.Name,
            //                      Value = clients.SubClientOrganization.OrganizationID.ToString()
            //                  }).ToList();
            LocalTemplateModel addClients = new LocalTemplateModel();
            addClients.Templateid = templateId;
            return View(addClients);
        }

        [HttpPost]
        public ActionResult AddClients(LocalTemplateModel form)
        {
            if (form.Templateid != null)
            {
                Templates newTemplate = entityDB.Templates.Find(form.Templateid);
                foreach (var eri in form.ERIClients)
                {
                    var clientTemplate = new ClientTemplates
                    {
                        ClientID = Convert.ToInt64(eri),
                        DisplayOrder = 1,
                        Visible = true,
                        DefaultTemplate = false
                    };
                    newTemplate.ClientTemplates.Add(clientTemplate);
                    entityDB.SaveChanges();
                }
            }
            return Redirect("PopUpCloseView");
        }

        [HttpGet]
        public ActionResult SetupLocationStatusNotification(long SubSectionID)
        {
            ViewBag.EmailTemplates = (from emailtemplate in entityDB.EmailTemplates.Where(rec => rec.EmailUsedFor == 0).OrderBy(o => o.Name).ToList()
                                      select new SelectListItem
                                      {
                                          Text = emailtemplate.Name,
                                          Value = emailtemplate.EmailTemplateID + ""
                                      }).ToList();

            var subSectionNotification = entityDB.TemplateSubSectionsNotifications.FirstOrDefault(rec => rec.TemplateSubSectionID == SubSectionID);

            LocalTemplateSubSectionsNotifications locationNotification = new LocalTemplateSubSectionsNotifications();
            if (subSectionNotification != null)
                locationNotification.EmaiTemplateId = subSectionNotification.EmaiTemplateId;
            locationNotification.TemplateSubSectionID = SubSectionID;

            return View(locationNotification);
        }
        [HttpPost]
        public ActionResult SetupLocationStatusNotification(LocalTemplateSubSectionsNotifications locationNotification)
        {
            ModelState.Remove("AdminUserId");
            ModelState.Remove("ClientUserId");
            ModelState.Remove("AdminUsers");
            ModelState.Remove("ClientUsers");

            if (ModelState.IsValid)
            {
                TemplateSubSectionsNotifications setupLocationNotification = new TemplateSubSectionsNotifications();

                setupLocationNotification.EmaiTemplateId = (int)locationNotification.EmaiTemplateId;
                setupLocationNotification.TemplateSubSectionID = locationNotification.TemplateSubSectionID;
                setupLocationNotification.SenderType = 0;
                setupLocationNotification.SenderEmailId = "notification@firstverify.com";


                entityDB.TemplateSubSectionsNotifications.Add(setupLocationNotification);

                entityDB.SaveChanges();
            }
            return Redirect("PopUpCloseView");
        }
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "QuestionnaireTool", sideMenuName = "CopyInformation", ViewName = "CopyInformation")]
        [SessionExpire]
        public ActionResult CopyInformation(Guid? SourceTemplateId, Guid? DestinationTemplateId, long? SourceSectionId, long? DestinationSectionId, long? SourceSubSectionId, long? DestinationSubSectionId)
        {

            var Template = from temp in entityDB.Templates.Where(temp => temp.TemplateType == 0).OrderBy(temp => temp.TemplateName).ToList()
                           select new SelectListItem
                           {
                               Text = temp.TemplateName,
                               Value = temp.TemplateID.ToString(),

                           };
            var DestTemplate = from temp in entityDB.Templates.Where(temp => temp.TemplateType == 0).OrderBy(temp => temp.TemplateName).ToList()
                               select new SelectListItem
                               {
                                   Text = temp.TemplateName,
                                   Value = temp.TemplateID.ToString(),

                               };

            if (SourceTemplateId != Guid.Empty && SourceTemplateId != null) { Template.FirstOrDefault(r => r.Value == SourceTemplateId.ToString()).Selected = true; }
            ViewBag.SourceTemplate = Template;
            ViewBag.TemplateSource = SourceTemplateId.ToString();
            ViewBag.TemplateDestination = DestinationTemplateId.ToString();
            if (DestinationTemplateId != Guid.Empty && SourceTemplateId != null) { DestTemplate.FirstOrDefault(r => r.Value == DestinationTemplateId.ToString()).Selected = true; }
            ViewBag.DestinationTemplate = DestTemplate;
            ViewBag.SourceSectionId = SourceSectionId;
            ViewBag.DestinationSectionid = DestinationSectionId;
            ViewBag.SourceSubSectionID = SourceSubSectionId;
            ViewBag.DestinationSubSectionId = DestinationSubSectionId;

            return View();
        }

        public ActionResult GetSections(Guid Template)
        {
            //var Sections = _templateTool.GetTemplateSections(Template);
            //  ViewBag.SourceSectionId = (long)TempData["mydata"];
            return Json(_templateTool.GetTemplateSections(Template), JsonRequestBehavior.AllowGet);


        }
        public ActionResult GetSubSections(long SectionId)
        {

            return Json(_templateTool.GetTemplateSubSections(SectionId), JsonRequestBehavior.AllowGet);

        }
        public string IsCompatibleQuestion(long DestinationquestionId, long SourceQuestionId)
        {
            return _templateTool.IsCompatibleQuestion(DestinationquestionId, SourceQuestionId);
        }

        public ActionResult CopyQuestions(Guid SourceTemplate, Guid DestinationTemplate, long SourceSectionID, long DestinationSectionid, long SourceSubSectionId, long DestinationSubSectionId)
        {

            LocalCopyQuestions CopyQuestions = new LocalCopyQuestions();
            var mappedQuestions = _templateTool.GetMappedQuestions(SourceSubSectionId, DestinationSubSectionId);

            //var DQuestions= _templateTool.GetQuestions(DestinationSubSectionId).Select(r => new QuestionsList()
            //{
            //    DestinationQuestionId = r.QuestionID,
            //    DestinationQuestionText = r.QuestionText,
            //    DestinationBankId = r.QuestionBankId.Value,
            //    DestinationNoOfColumns = r.NumberOfColumns,
            //    DestinationDisplayorder = r.DisplayOrder,

            //}).ToList();
            //var SourceQuestions = _templateTool.GetQuestions(SourceSubSectionId).Select(r => new QuestionsList()
            //{
            //    SourceQuestionId = r.QuestionID,
            //    SourceQuestionText = r.QuestionText,
            //    SourceBankId = r.QuestionBankId.Value,
            //    SourceNoOfColumns = r.NumberOfColumns,
            //    SourceDisplayorder=r.DisplayOrder,           
            //    SourceQuestionControltype =entityDB.QuestionColumnDetails.FirstOrDefault(col => col.QuestionId == r.QuestionID)!=null?entityDB.QuestionColumnDetails.FirstOrDefault(col => col.QuestionId == r.QuestionID).QuestionControlTypeId:0

            //    }).ToList();
            //foreach (var Questions in SourceQuestions)
            //{

            //        var MatchedQuestion = DQuestions.Where(r => r.DestinationBankId == Questions.SourceBankId).ToList();
            //        if (MatchedQuestion.Count > 0)
            //        {
            //            var DestinationQuestionId = MatchedQuestion.Count == 1 ? MatchedQuestion.Select(r => r.DestinationQuestionId).FirstOrDefault() : MatchedQuestion.FirstOrDefault(r => r.DestinationDisplayorder == Questions.SourceDisplayorder).DestinationQuestionId;

            //            MappedQuestions CopyQuestionsList = new MappedQuestions();
            //            if (entityDB.MappedQuestions.FirstOrDefault(r => r.SourceQuestionId == Questions.SourceQuestionId && r.SourceSubSectionId == SourceSubSectionId) == null)
            //            {
            //                CopyQuestionsList.SourceQuestionId = Questions.SourceQuestionId;

            //                CopyQuestionsList.DestinationQuestionId = DestinationQuestionId;
            //                CopyQuestionsList.SourceSubSectionId = SourceSubSectionId;
            //                CopyQuestionsList.DestinationSubSectionId = DestinationSubSectionId;
            //                CopyQuestionsList.IsMapped = true;
            //                entityDB.MappedQuestions.Add(CopyQuestionsList);
            //                entityDB.SaveChanges();
            //            }
            //            //CopyQuestionsList.SourceQuestionId = Questions.DestinationQuestionId;
            //            //CopyQuestionsList.DestinationQuestionId = Questions.SourceQuestionId;
            //            //CopyQuestionsList.SourceSubSectionId = CopyQuestions.DestionationSubSectionId;
            //            //CopyQuestionsList.DestinationSubSectionId = CopyQuestions.SourceSubSectionId;
            //            //entityDB.MappedQuestions.Add(CopyQuestionsList);
            //            //entityDB.SaveChanges();

            //    }
            //}
            //List<LocalCopyQuestions> Questions = new List<LocalCopyQuestions>();

            CopyQuestions.SourceTemplateId = SourceTemplate;
            CopyQuestions.DestinationTemplateId = DestinationTemplate;
            CopyQuestions.SourceSectionId = SourceSectionID;
            CopyQuestions.DestionationSectionId = DestinationSectionid;
            CopyQuestions.SourceSubSectionId = SourceSubSectionId;

            CopyQuestions.DestionationSubSectionId = DestinationSubSectionId;
            CopyQuestions.QuestionsList = mappedQuestions.SourceQuestions;
            //var sQuestions = SourceQuestions.Select(r => r.SourceQuestionId);
            ViewBag.MappedQuestions = mappedQuestions.SDQuestion;//entityDB.MappedQuestions.Where(r => sQuestions.Contains(r.SourceQuestionId) && r.SourceSubSectionId == SourceSubSectionId).Select(r => new SDQuestion() { SourceQuestionId = r.SourceQuestionId, DestinationQuestionId = r.DestinationQuestionId }).ToList();


            var DestQuestions = mappedQuestions.DestinationQuestions.Select(r => new SelectListItem()
            {
                Value = r.DestinationQuestionId + "",
                Text = r.DestinationQuestionText

            }).ToList();
            DestQuestions.Insert(0, new SelectListItem() { Value = "0", Text = "None" });
            ViewBag.Destquestions = DestQuestions;
            //if (QuestionbankId != null)
            //{
            //    DestQuestions.FirstOrDefault(r => r.Value == QuestionbankId.ToString()).Selected = true;
            //}

            return View(CopyQuestions);


        }
        [HttpPost]
        public string SaveData(string fieldsWithValues)
        {
            _templateTool.mappingQuestionsTest(fieldsWithValues);
            return "Success";
        }
        [HttpPost]
        public ActionResult CopyQuestions(LocalCopyQuestions CopyQuestions)
        {
            var DestQuestions = _templateTool.GetQuestions(CopyQuestions.DestionationSubSectionId).Select(r => new SelectListItem()
            {
                Value = r.QuestionID + "",
                Text = r.QuestionText

            }).ToList();
            ViewBag.Destquestions = DestQuestions;
            //foreach (var Questions in CopyQuestions.QuestionsList)
            //{
            //    CopyQuestionsList CopyQuestionsList = new CopyQuestionsList();
            //    var HasQuestion = entityDB.CopyQuestionsList.FirstOrDefault(r => r.SourceQuestionId == Questions.SourceQuestionId && r.SourceSubSectionId == CopyQuestions.SourceSubSectionId);
            //    if (HasQuestion != null) entityDB.CopyQuestionsList.Remove(HasQuestion);
            //    if (Questions.DestinationQuestionId == 0) continue;
            //    var DesNumberOfColumns = entityDB.Questions.FirstOrDefault(r => r.QuestionID == Questions.DestinationQuestionId).NumberOfColumns;

            //    if (Questions.DestinationQuestionId == 0 && (DesNumberOfColumns != Questions.SourceNoOfColumns)) continue;
            //    CopyQuestionsList.SourceQuestionId = Questions.SourceQuestionId;
            //    CopyQuestionsList.DestinationQuestionId = Questions.DestinationQuestionId;
            //    CopyQuestionsList.SourceSubSectionId = CopyQuestions.SourceSubSectionId;
            //    CopyQuestionsList.DestinationSubSectionId = CopyQuestions.DestionationSubSectionId;
            //    entityDB.CopyQuestionsList.Add(CopyQuestionsList);
            //}
            //entityDB.SaveChanges();
            _templateTool.mappingQuestions(CopyQuestions);

            return Redirect("CopyInformation?SourceTemplateId=" + CopyQuestions.SourceTemplateId + "&DestinationTemplateId=" + CopyQuestions.DestinationTemplateId + "&SourceSectionId=" + CopyQuestions.SourceSectionId + "&DestinationSectionId=" + CopyQuestions.DestionationSectionId + "&SourceSubSectionId=" + CopyQuestions.SourceSubSectionId + "DestinationSubSectionId=" + CopyQuestions.DestionationSubSectionId);
        }
        public string ChangeSubSectionName(long SubsecId, string SubsectionName)
        {
            try
            {
                return _templateTool.ChangeSubSectionName(SubsecId, SubsectionName);
            }
            catch
            {
                return Resources.Resources.Common_unableToProcess;
            }
        }
        public ActionResult HighlightedQuestion(long QuestionId)
        {
            var question = entityDB.Questions.Find(QuestionId);
            // var question = entityDB.Questions.Find(QuestionId);
            ViewBag.QuestionName = question.QuestionText;
            ViewBag.QuestionId = QuestionId;
            ViewBag.Question = question;

            List<SelectListItem> Colors = new List<SelectListItem>();
            SelectListItem EmptyColor = new SelectListItem() { Text = "None", Value = "0" };
            Colors.Add(EmptyColor);
            Colors.Add(new SelectListItem() { Text = "Red", Value = "Red" });
            Colors.Add(new SelectListItem() { Text = "Green", Value = "Green" });
            Colors.Add(new SelectListItem() { Text = "Yellow", Value = "Yellow" });
            Colors.Add(new SelectListItem() { Text = "Blue", Value = "Blue" });
            //Ends<<
            ViewBag.Colors = Colors;
            getCommand();
            LocalQuestionColumnDetails details = new LocalQuestionColumnDetails();
            details.CQuestion = new List<CQuestion>();
            var DependentValue = entityDB.QuestionColors.Where(r => r.QuestionID == QuestionId).ToList();

            foreach (var detail in DependentValue)
            {
                details.CQuestion.Add(new CQuestion() { QuestionId = QuestionId, Color = detail.Color, Answer = detail.Answer });
            }
            ViewBag.cQuestions = details.CQuestion;
            if (DependentValue.Count > 0 && DependentValue != null)
            {
                ViewBag.DependentValue = DependentValue.FirstOrDefault().Answer;
            }
            else ViewBag.DependentValue = "";

            return View(details);
        }
        [HttpPost]
        public async Task<string> UpdateSubSectionVisiblity(long subSecId, bool IsShow)
        {
            return await _templateTool.UpdateSubSectionVisiblity(subSecId, IsShow);
        }
        public void saveQuestionColors(long QuestionId, string DependentValue, string Colors)
        {
            var Hascolor = entityDB.QuestionColors.Where(r => r.QuestionID == QuestionId);
            foreach (var color in Hascolor)
            {
                entityDB.QuestionColors.Remove(color);
            }
            //  var Question = entityDB.Questions.FirstOrDefault(r => r.QuestionID == QuestionId);
            // Question.CorrectAnswer = DependentValue;
            //  entityDB.Entry(Question).State = EntityState.Modified;
            foreach (var Questions in DependentValue.Split(','))
            {
                QuestionColors colors = new QuestionColors();

                string[] QColor = Questions.Split('|');
                if (QColor[0] != "" && QColor[1] != "0")
                {
                    colors.QuestionID = QuestionId;
                    // string[] anscolor = questions.Split('|');
                    colors.Color = QColor[1];
                    colors.Answer = QColor[0];

                    entityDB.QuestionColors.Add(colors);
                }
            }
            entityDB.SaveChanges();


        }
    }


    public class BiddingCheckBoxes
    {
        public string id { get; set; }
        public string Text { get; set; }
        public string status { get; set; }
        public int type { get; set; }//0 Bidding 1-proficancy & category
        public string Header { get; set; }//this is For Proficiency Category
        public bool isExpire { get; set; }//only for documents
        public bool isDocMandatory { get; set; } //only for documents // kiran on 8/5/2014
    }
}
