﻿/*
Page Created Date:  06/05/2013
Created By: Rajesh Pendela
Purpose: All operations of admin related to vendor(Invite vendor,Vendor search) will be controlled here
Version: 1.0
Notes: Naming of the folder is done as AdminVendors indicating that this controller deals with all the activities that a admin 
       performs towards a vendor. Similarly for Client it will be AdminClients.
****************************************************
*/
using System;
using System.Collections.Generic;

using System.Linq;
using System.Web.Mvc;
using FVGen3.Domain.Concrete;
using System.IO;
using FVGen3.Domain.Entities;
using FVGen3.WebUI.Annotations;
using FVGen3.WebUI.Utilities;
using System.Configuration;
using System.Data.SqlClient;
using System.Data.Entity;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Web.Routing;
using FVGen3.BusinessLogic;
using AhaApps.Libraries.Extensions;
using AutoMapper;
using FVGen3.BusinessLogic.Interfaces;
using FVGen3.DataLayer.DTO;
using FVGen3.WebUI.Constants;
using FVGen3.WebUI.Models;
using WebGrease.Css.Extensions;
using FVGen3.Domain.LocalModels;
using Resources;
using FVGen3.Domain.ViewModels;
using FVGen3.Common;

namespace FVGen3.WebUI.Controllers
{
    public class AdminVendorsController : BaseController
    {
        //
        // GET: /AdminVendors/
        //EFDbContext entityDB;
        //Messages.Messages messages;

        // Kiran on 03/13/2015
        private EFEmployeeQuizCategoriesRepository empQuizCategoriesRepository;
        private IOrganizationBusiness _organization;
        private ISystemUserBusiness _sysUser;
        private IPrequalificationBusiness _prequalification;
        private IClientBusiness _clientBusiness;
        private IEmployeeBusiness _employee;
        private IVendorBusiness _vendor;
        private IDocumentBusiness _Document;
        private IEmailTemplateBusiness _emailTemplate;

        private IUtilityBusiness _utility;
        // Kiran on 03/13/2015 Ends<<<

        public AdminVendorsController(IOrganizationBusiness _organization, ISystemUserBusiness _sysUser, IPrequalificationBusiness _prequalification, IClientBusiness _clientBusiness, IEmployeeBusiness _employee, IVendorBusiness _vendor, IDocumentBusiness _Document, IEmailTemplateBusiness _emailTemplate, IUtilityBusiness _utility)
        {
            Mapper.Initialize(cfg => cfg.CreateMap<OrganizationData, LocalinviteVendor>());
            //entityDB = new EFDbContext();
            //messages = new Messages.Messages();
            this.empQuizCategoriesRepository = new EFEmployeeQuizCategoriesRepository(); // Kiran on 03/13/2015
            this._organization = _organization;
            this._sysUser = _sysUser;
            this._prequalification = _prequalification;
            this._clientBusiness = _clientBusiness;
            this._employee = _employee;
            this._vendor = _vendor;
            this._Document = _Document;
            this._emailTemplate = _emailTemplate;
            this._utility = _utility;
        }

        // Rajesh 01/05/2013 
        // Invite Vendor Get >>>
        [SessionExpire]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "Vendors", sideMenuName = "InviteVendor", ViewName = "InviteVendor")]
        public ActionResult InviteVendor()
        {
            List<KeyValuePair<string, string>> clientOrganizations = _organization.GetClientOrganizations();
            var Orglist =
                from orgs in clientOrganizations.ToList()
                select new SelectListItem
                {
                    Text = orgs.Key,
                    Value = orgs.Value
                };
            ViewBag.IsSuperClient = SSession.Role.Equals(OrganizationType.SuperClient) ? true : false;
            if (SSession.Role.Equals(OrganizationType.Client))
            {
                long userOrgId = (long)SSession.OrganizationId;
                List<KeyValuePair<string, long>> userOrganizations = _sysUser.GetOrganizations(userOrgId);
                Orglist = from loc_orgs in userOrganizations.ToList()
                          select new SelectListItem
                          {
                              Text = loc_orgs.Key,
                              Value = loc_orgs.Value.ToString()
                          };
            }
            else if (SSession.Role.Equals(OrganizationType.SuperClient))
            {
                if (_sysUser.IsERIUser(SSession.UserId))
                {
                    Orglist = _clientBusiness.GetSubClients(SSession.OrganizationId).Select(r => new SelectListItem() { Value = r.Key, Text = r.Value });

                }
            }
            ViewBag.VBlist = Orglist;
            //ViewBag.Subclients = entityDB.SubClients.Select(r => r.ClientId).ToList();
            ViewBag.Countries = _organization.GetCountries().Select(r => new SelectListItem() { Text = r.Value, Value = r.Value }).ToList();
            var Countries = _organization.GetCountries().Select(r => new SelectListItem() { Text = r.Value, Value = r.Value }).ToList();

            Countries.Insert(0, (new SelectListItem
            {
                Text = "Any",
                Value = ""
            }));
            ViewBag.Countrieswithdefault = Countries;

            return View();
        }
        // Invite Vendor Get Ends <<<
        public ActionResult IsTemplateLocked(string invitationCode)
        {
            return Json(_organization.IsTemplateLocked(invitationCode), JsonRequestBehavior.AllowGet);
        }
        // Invite Vendor Post >>>
        [HttpPost]
        [SessionExpire]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "Vendors", sideMenuName = "InviteVendor", ViewName = "InviteVendor")]
        public ActionResult InviteVendor(LocalinviteVendor modelInviteVendor)
        {
            ViewBag.IsSuperClient = SSession.Role.Equals(OrganizationType.SuperClient) ? true : false;
            ViewBag.Countries = _organization.GetCountries().Select(r => new SelectListItem() { Text = r.Value, Value = r.Value }).ToList();
            var Countries = _organization.GetCountries().Select(r => new SelectListItem() { Text = r.Value, Value = r.Value }).ToList();

            Countries.Insert(0, (new SelectListItem
            {
                Text = "Any",
                Value = ""
            }));
            ViewBag.Countrieswithdefault = Countries;
            ViewBag.VBMailSent = true;
            List<KeyValuePair<string, string>> clientOrganizations = _organization.GetClientOrganizations();
            var Orglist =
                from orgs in clientOrganizations.ToList()
                select new SelectListItem
                {
                    Text = orgs.Key,
                    Value = orgs.Value
                };

            if (Session["RoleName"].ToString().Equals("Client"))
            {
                long userOrgId = (long)SSession.OrganizationId;
                List<KeyValuePair<string, long>> userOrganizations = _sysUser.GetOrganizations(userOrgId);
                Orglist = from loc_orgs in userOrganizations.ToList()
                          select new SelectListItem
                          {
                              Text = loc_orgs.Key,
                              Value = loc_orgs.Value.ToString()
                          };
            }
            if (modelInviteVendor.client == 0)
            {
                ModelState.AddModelError("client", "*");
            }
            if (modelInviteVendor.RiskLevel == "Select Risk Level")
            {
                ModelState.AddModelError("RiskLevel", "*");
            }
            ViewBag.VBlist = Orglist;
            if (!ModelState.IsValid)
            {
                ViewBag.errorMsg = "Please fill all required fields";
            }
            if (ModelState.IsValid)
            {
                bool successMessage = _vendor.PostVendorInvite(modelInviteVendor, SSession.UserId);
                if (successMessage == true)
                    ViewBag.isSuccess = true;
                else
                    ViewBag.VBMailSent = false;
            }

            return View();
        }

        public ActionResult GetLocalInvitation(long clientId, long vendorId)
        {
            var vendorInfo = _organization.GetOrganization(vendorId);
            var vendorSuperUserInfo = _organization.GetSuperuser(vendorId);
            var vendorLanguageCode = _organization.GetLanguageCode(vendorInfo.Language);
            var file = Server.MapPath(@"~/HtmlTemplates/ExpiredVendorTemplate");
            file = file + "_" + vendorLanguageCode + ".html";
            if (!System.IO.File.Exists(file))
            {
                file = Server.MapPath("~/HtmlTemplates/ExpiredVendorTemplate_EN.html");
            }
            var template = System.IO.File.ReadAllText(file);
            var clientInfo = _organization.GetOrganization(clientId);
            var clientUser = _sysUser.GetUserInfo(SSession.UserId);
            template = template.Replace("[Client_UserName]", clientUser.FullName);
            template = template.Replace("[Client_EmailAddress]", clientUser.Email);
            template = template.Replace("[Client_Name]", clientInfo.Name);

            return Json(template);
        }
        private string RiskFee(long client, string riskLevel, bool isERI)
        {

            List<decimal?> a = new List<decimal?>();
            var groupingPriceType = 0;
            if (isERI)
                groupingPriceType = 1;
            using (var entityDB = new EFDbContext())
            {
                if (riskLevel != null)
                {
                    var fee = from t in entityDB.Templates
                              join ct in entityDB.ClientTemplates on t.TemplateID equals ct.TemplateID
                              join ctgp in entityDB.ClientTemplatesBUGroupPrice on ct.ClientTemplateID equals ctgp.ClientTemplateID
                              where
                              ctgp.GroupingFor == 0 && ct.ClientID == client && ctgp.GroupPriceType == groupingPriceType &&
                              t.RiskLevel == riskLevel
                              select ctgp.GroupPrice;
                    a = fee.ToList();
                }
            }
            return a.Count == 0 ? "" : a.FirstOrDefault().ToString();
        }

        private string Location(List<VendorInviteLocations> InviteVen)
        {
            string location = string.Empty;
            using (var entityDB = new EFDbContext())
            {
                if (InviteVen != null)
                {
                    InviteVen.ForEach(rec =>
                    {
                        var loc = entityDB.ClientBusinessUnitSites.Find(rec.ClientBusinessUnitSiteID);

                        if (loc != null)
                        {
                            if (!string.IsNullOrWhiteSpace(location))
                            {
                                location += ", ";
                            }
                            location = location + loc.SiteName + " " + loc.BusinessUnitLocation;
                        }
                    });
                }
            }
            return location;
        }
        [HttpPost]
        public ActionResult GetPQVendorDetailsByVendorId(long vendorId, long clientId)
        {
            var IsSubClient = _organization.IsSubClient(clientId);
            if (IsSubClient)
            {
                clientId = _sysUser.GetSuperClientId(clientId);
            }
            var pq = _prequalification.GetLatestPQ(clientId, vendorId);
            var emails = String.Join(",", _prequalification.GetPQVendorUser(pq.PrequalificationId).Select(r => r.Email).Distinct(StringComparer.CurrentCultureIgnoreCase));
            var superUser = _sysUser.GetSuperUserInfo(vendorId);
            if (superUser != null && emails.IndexOf(superUser.Email, StringComparison.CurrentCultureIgnoreCase) == -1)
            {
                emails += "," + superUser.Email;
            }
            return Json(emails, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetPQVendorDetailsList(string PqIds)
        {
            //if (!SSession.Role.Equals(Role.Client))
            //    return Json("");
            var pqs = PqIds.Split(',').Select(long.Parse).ToList();
            var emails = String.Join(",", _prequalification.GetPQVendorUser(pqs).Select(r => r.Email).Distinct(StringComparer.CurrentCultureIgnoreCase));
            return Json(emails, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult OutSideInvitation(OutsideInvitation form)//-1 --Expired 2-Outside N/w
        {
            if (form.riskLevel == "Select Risk Level") form.riskLevel = null;

            form.isERI = _clientBusiness.IsClientPartOfEri(form.clientId);
            var file = "";
            if (form.invitationType != 1)
            {
                if (form.isERI)
                    file = "~/HtmlTemplates/OutsideERIVendorTemplate";
                else
                    file = "~/HtmlTemplates/OutsideVendorTemplate";
            }
            var clientInfo = _organization.GetOrganization(form.clientId);
            var ven = _organization.GetOrganization(form.vendorId);
            var languageCode = _organization.GetLanguageCode(ven.Language);
            var file1 = file + "_" + languageCode + ".html";
            if (!System.IO.File.Exists(Server.MapPath(file1)))
            {
                file = file + "_EN.html";
            }
            else
            {
                file = file + "_" + languageCode + ".html";
            }
            string template = "";
            string subject = "Invitation from FIRST, VERIFY";
            var HasInvitationTemplateConfig = _clientBusiness.GetClientConfiguredInvitationTemplate(form.clientId, form.invitationType);
            var HasInvationTemplateforall = _clientBusiness.GetClientConfiguredInvitationTemplate(form.clientId, null);
            if (HasInvitationTemplateConfig != null)
            {
                template = HasInvitationTemplateConfig.EmailTemplates.EmailBody;
                subject = HasInvitationTemplateConfig.EmailTemplates.EmailSubject;
            }
            else if (HasInvationTemplateforall != null)
            {
                template = HasInvationTemplateforall.EmailTemplates.EmailBody;
                subject = HasInvationTemplateforall.EmailTemplates.EmailSubject;
            }
            else
            {
                template = form.invitationType == 1
                        ? form.htmlTemplate : System.IO.File.ReadAllText(Server.MapPath(file));
            }

            var clientUser = _sysUser.GetUserInfo(SSession.UserId);
            var venInviteLocations = new List<VendorInviteLocations>();
            if (!string.IsNullOrEmpty(form.locations))
            {
                form.locations.Split(',').ForEach(rec =>
                {
                    var temploc = new VendorInviteLocations();
                    temploc.ClientBusinessUnitSiteID = Convert.ToInt64(rec);
                    venInviteLocations.Add(temploc);
                });
            }
            var a = _vendor.RiskFee(clientInfo.OrganizationID, form.riskLevel, form.isERI);
            var loc = _vendor.Location(venInviteLocations);
            var clientName = clientInfo.Name;
            OrganizationData superClientOrg = null;
            if (form.isERI)
            {
                clientName = clientInfo.Name;
                superClientOrg = _organization.GetSuperClient(form.clientId);
            }
            loc = string.IsNullOrEmpty(loc) ? "N/A" : loc;
            if (form.isERI)
                form.riskLevel = string.IsNullOrEmpty(form.riskLevel) ? "Insurance Program - Safety" : form.riskLevel;
            else
                form.riskLevel = string.IsNullOrEmpty(form.riskLevel) ? "N/A" : form.riskLevel;
            template = template.Replace("[ClientName]", clientName);
            template = template.Replace("[VendorName]", ven.Name);
            template = template.Replace("[Date]", DateTime.Now.ToString());
            template = template.Replace("[Vendor_CompanyName]", ven.Name);
            template = template.Replace("[Client_UserName]", clientUser.FullName);
            template = template.Replace("[Client_EmailAddress]", clientUser.Email);
            template = template.Replace("[Client_Name]", clientName);
            template = template.Replace("[Location_Name]", loc);
            template = template.Replace("[Vendor_RiskLevel]", form.riskLevel);
            template = template.Replace("[Client_Comments]", form.comments);
            template = template.Replace("[Risk_LevelFee]", a);
            subject = subject.Replace("[ClientName]", clientName);
            subject = subject.Replace("[VendorName]", ven.Name);
            subject = subject.Replace("[Date]", DateTime.Now.ToString());
            subject = subject.Replace("[Vendor_CompanyName]", ven.Name);
            subject = subject.Replace("[Client_UserName]", clientUser.FullName);
            subject = subject.Replace("[Client_EmailAddress]", clientUser.Email);
            subject = subject.Replace("[Client_Name]", clientName);
            subject = subject.Replace("[Location_Name]", loc);
            subject = subject.Replace("[Vendor_RiskLevel]", form.riskLevel);
            subject = subject.Replace("[Client_Comments]", form.comments);
            subject = subject.Replace("[Risk_LevelFee]", a);
            subject = subject.Replace("[ClientName]", clientName);
            subject = subject.Replace("[VendorName]", ven.Name);
            if (superClientOrg != null)
            {
                template = template.Replace("[SuperClient_Name]", superClientOrg.Name);
                template = template.Replace("[SuperClient]", superClientOrg.Name);
                subject = subject.Replace("[SuperClient]", superClientOrg.Name);
                subject = subject.Replace("[SuperClient_Name]", superClientOrg.Name);
            }
            //template = template.Replace("[InvitationCode]", a);
            var error = "";
            string inviteVenUrl = ConfigurationSettings.AppSettings["InviteVendorInvitationURL"];
            LatestPrequalification latestPQ = null;
            if (form.invitationType != 1)
            {
                var clients = _prequalification.GetVendorClients(form.vendorId);
                if (clients.Any())
                    latestPQ = _prequalification.GetLatestPQ(clients.FirstOrDefault(), form.vendorId);
            }
            else
            {
                var IsSubClient = _organization.IsSubClient(clientInfo.OrganizationID);
                if (IsSubClient)
                {
                    latestPQ = _prequalification.GetLatestPQ(_sysUser.GetSuperClientId(clientInfo.OrganizationID), form.vendorId);
                }
                else latestPQ = _prequalification.GetLatestPQ(clientInfo.OrganizationID, form.vendorId);
            }
            List<UserInfo> userInfos = null;
            if (latestPQ == null)
                return Json("Unable to pick the vendor information. Please contact system Admin.");
            if (string.IsNullOrEmpty(form.toAddress))
            {
                userInfos = _prequalification.GetPQVendorUser(latestPQ.PrequalificationId);
                if (!userInfos.Any())
                {
                    var order = new List<string>() { "Super User", "Administrator" };
                    userInfos =
                        _organization.GetOrganizationUsers(form.vendorId)
                            .OrderBy(r => order.IndexOf(r.RoleName))
                            .Take(5)
                            .ToList();
                }
                if (!userInfos.Any())
                    return Json("Unable to pick the vendor information. Please contact system Admin.");
            }
            else
            {
                if (form.toAddress.Split(',')
                        .Distinct(StringComparer.CurrentCultureIgnoreCase).Count() > 5)
                    return Json("To Address cannot be more than 5 emails");
                userInfos =
                    form.toAddress.Split(',')
                        .Distinct(StringComparer.CurrentCultureIgnoreCase)
                        .Select(email => _sysUser.GetUserInfo(email) ?? new UserInfo()
                        {
                            LastName = "",
                            FirstName = "",
                            Email = email,
                            RoleName = ""
                        }).ToList();
            }
            foreach (var users in userInfos.GroupBy(r => r.Email))
            {
                if (String.IsNullOrEmpty(users.Key))
                    continue;
                var invitation = InsertInvitation(form.invitationType, ven, clientInfo, users.FirstOrDefault(), venInviteLocations, form.riskLevel, form.PqId, form.sendCC, form.sendBCC, form.isERI);
                var invitationId = invitation.InvitationId;
                var body = template;
                body = body.Replace("[ClientName]", clientName);
                body = body.Replace("[SubClient]", clientName);
                if (superClientOrg != null)
                {
                    body = body.Replace("[SuperClient]", superClientOrg.Name);
                    subject = subject.Replace("[SuperClient]", superClientOrg.Name);
                }
                body = body.Replace("[VendorName]", ven.Name);
                subject = subject.Replace("[ClientName]", clientName);
                subject = subject.Replace("[VendorName]", ven.Name);
                body = body.Replace("[Date]", DateTime.Now.ToString());
                subject = subject.Replace("[Vendor_FirstLastNames]", users.FirstOrDefault().FullName);
                subject = subject.Replace("[Vendor_EmailAddress]", users.FirstOrDefault().Email);
                body = body.Replace("[Vendor_FirstLastNames]", users.FirstOrDefault().FullName);
                body = body.Replace("[Vendor_EmailAddress]", users.FirstOrDefault().Email);
                body = body.Replace("[Invitation_Code]", invitation.InvitationCode + "");
                body = body.Replace("[Client_Name]", clientName);
                body = body.Replace("[URL]",
                    "<a href='" + inviteVenUrl + invitationId + "'>" + inviteVenUrl + invitationId +
                    "</a>");
                var cc = form.sendCC;
                if (form.isSendMeCC)
                {
                    cc += "," + clientUser.Email;
                    if (cc.StartsWith(","))
                        cc = cc.Substring(1);
                }
                var bcc = form.sendBCC;
                if (form.isSendMeBCC)
                {
                    bcc += "," + clientUser.Email;
                    if (bcc.StartsWith(","))
                        bcc = bcc.Substring(1);
                }
                if (!string.IsNullOrEmpty(cc))
                    cc += ",";
                cc += "info@firstverify.com";
                var HasClientUsers = _sysUser.CheckInviteClients(form.clientId);
                if (HasClientUsers)
                    cc += "," + _sysUser.CheckClientUsersforInvitation(form.clientId).UserEmails;
                if (!string.IsNullOrEmpty(bcc))
                    bcc += ",";
                bcc += "info@firstverify.com";
                _sysUser.UsersInInvitation(invitationId, bcc, cc);
                error += SendMailProcess(users.FirstOrDefault(), body, subject, invitation, bcc, cc);
            }
            var result = error == "" ? "Success" : "Unable to send mails to \n" + error;
            return Json(result);
        }

        private string SendMailProcess(UserInfo users, string body, string subject, VendorInviteDetails invitation, string bcc, string cc)
        {
            var entityDB = new EFDbContext();
            {
                //new Thread(() =>
                //{
                var strMessage = Common.SendMail.sendMail(users.Email, body, subject, bcc, cc);
                if (!strMessage.Equals("Success"))
                {
                    entityDB.VendorInviteDetails.Remove(invitation);
                    entityDB.SaveChanges();

                    return users.Email + "\n";
                }
            }
            //}).Start();
            return String.Empty;
        }



        private VendorInviteDetails InsertInvitation(int invitationType, OrganizationData ven, OrganizationData clientInfo,
            UserInfo users, List<VendorInviteLocations> loc, string riskLevel, long? pqid, string bcc, string cc, bool? isERI)
        {
            var inviteVen = new VendorInviteDetails
            {
                VendorInviteLocations = (List<VendorInviteLocations>)loc.Clone(),
                Address1 = ven.Address1,
                Address2 = ven.Address2,
                Address3 = ven.Address3,
                City = ven.City,
                ClientId = clientInfo.OrganizationID,
                Country = ven.Country,
                State = ven.State,
                RiskLevel = riskLevel,
                VendorEmail = users.Email,
                VendorFirstName = users.FullName,
                VendorLastName = users.LastName,
                Zip = ven.Zip
            };
            //InviteVen.ClientCode = EncryptionDecryption.GetUniqueKey();
            var clientUniqueKey = clientInfo.SingleClientCode;
            inviteVen.ClientCode = clientUniqueKey;
            inviteVen.InvitationDate = DateTime.Now;
            inviteVen.VendorCompanyName = ven.Name;
            inviteVen.InvitationSentFrom = 0; //Rajesh on 2/21/2014
            inviteVen.InvitationSentByUser = SSession.UserId; //Rajesh on 2/21/2014
            inviteVen.VendorId = ven.OrganizationID;
            inviteVen.InvitationType = invitationType;
            inviteVen.PQId = pqid;
            inviteVen.SendBcc = bcc;
            inviteVen.SendCc = cc;
            if (isERI == true)
            {
                inviteVen.InvitationCode = FVGen3.Common.EncryptionDecryption.GetUniqueKey(10);
            }
            using (var entityDB = new EFDbContext())
            {
                entityDB.VendorInviteDetails.Add(inviteVen);
                entityDB.SaveChanges();
            }
            return inviteVen;
        }


        //======================On 18-05-2013 for Vendor Awaiting=================>
        public ActionResult VendorAwaiting()
        {
            var entityDB = new EFDbContext();
            {
                var list = entityDB.Prequalification.Where(m => m.PrequalificationStatusId == m.PrequalificationStatus.PrequalificationStatusId && m.PrequalificationStatus.PrequalificationStatusId == 5).ToList();

                ViewBag.list = list;
            }
            return View();
        }

        public string GetLockTemplateComments(Guid clientTemplateId)
        {
            return _prequalification.GetLockTemplateComments(clientTemplateId);
        }

        [SessionExpire]
        [HeaderAndSidebar(headerName = "DashBoard", sideMenuName = "")]
        public ActionResult AdminDashboard()
        {
            using (var entityDB = new EFDbContext())
            {
                ViewBag.PreQualificationsStatus = _utility.GetStatusDescriptions();
                ViewBag.Reporturl = "";
                ViewBag.HasPowerbiPermission = false;
                var hasPowerbiUrl = _clientBusiness.GetPowerBiReportByName(SSession.OrganizationId, SSession.UserId, SSession.Role, Resources.Resources.Report_Admin_Dashboard);
                if (!string.IsNullOrEmpty(hasPowerbiUrl))
                {
                    var reportfilter = _clientBusiness.GetReportFilter(Resources.Resources.Report_Admin_Dashboard);
                    reportfilter = reportfilter.Replace(nameof(PowerBiReportKeywords.LoginUserId), "'" + SSession.UserId + "'");
                    ViewBag.Reporturl = hasPowerbiUrl + reportfilter;
                    ViewBag.HasPowerbiPermission = true;
                }
                ViewBag.LogoPath = ConfigurationSettings.AppSettings["LogoUploadPath"].ToString() + "/";
            }
            return View();
        }

        [HttpPost]
        [OutputCache(Duration = 0)]
        public ActionResult GetRecentDocs(DataSourceRequest request, string filters)
        {
            return Json(_Document.GetRecentDocs(filters, request, (string)Session["RoleName"]));
        }
        [HttpPost]
        [SessionExpire]
        public ActionResult RemoveDocument(Guid documentId)
        {
            var entityDB = new EFDbContext();
            {
                Document document = entityDB.Document.Find(documentId);
                try
                {
                    if (document != null)
                    {
                        document.ShowOnDashboard = false;
                        entityDB.Entry(document).State = EntityState.Modified;
                        entityDB.SaveChanges();
                    }
                }
                catch
                {
                    document.ShowOnDashboard = false;
                    entityDB.Entry(document).State = EntityState.Modified;
                    entityDB.SaveChanges();
                }
            }
            return Json("Ok");
        }
        //<<< sumanth Ends on 10/05/2015.


        //sumanth on 10/09/2015 for SMI-268
        //Desc:-To allow admin to delete the vendor from list on dashboard. 
        [HttpPost]
        [SessionExpire]
        public ActionResult RemoveVendorfromVendorsInvited(Guid invitationId)
        {
            var entityDB = new EFDbContext();
            {
                VendorInviteDetails vendorInviteDetails = entityDB.VendorInviteDetails.Find(invitationId);
                try
                {
                    if (vendorInviteDetails != null)
                    {

                        vendorInviteDetails.ShowOnDashboard = false;
                        entityDB.Entry(vendorInviteDetails).State = EntityState.Modified;
                        entityDB.SaveChanges();

                    }
                }
                catch
                {
                    vendorInviteDetails.ShowOnDashboard = false;
                    entityDB.Entry(vendorInviteDetails).State = EntityState.Modified;
                    entityDB.SaveChanges();
                }
            }
            return Json("Ok");
        }
        //<<< sumanth Ends on 10/05/2015.


        [HttpPost]
        public ActionResult InviteVendorsPartialView(int pageNumber, string SearchEmail, int InvitationType)//0-New Vendor 1-Expired 2-Outside N/w
        {
            ViewBag.emaildata = SearchEmail;
            var clientId = Convert.ToInt64(Session["currentOrgId"]);
            // Kiran on 9/29/2014 //sumanth on 10/09/2015 for SMI-268

            var invitedVendors = _vendor.GetInvitedVendors(pageNumber, SearchEmail, InvitationType);

            ViewBag.invitedVendors = invitedVendors;

            return PartialView();
        }


        [HttpPost]
        public ActionResult NotificationsPartialView(int pageNumber)//Every Page contains 30 notifications
        {
            var entityDB = new EFDbContext();
            {
                //var clientId = Convert.ToInt64(Session["currentOrgId"]);
                List<LocalNotifications> notifications = new List<LocalNotifications>();

                var NumberOfItemsToSkip = pageNumber * 30;

                //var orgNSent = entityDB.OrganizationsNotificationsSent.Where(rec => rec.SetupType == 1).Select(rec => rec.NotificationId).ToList();
                //Rajesh on 2/13/2015
                var orgNotificationsEmailLogs = entityDB.OrganizationsNotificationsEmailLogs.Where(rec => rec.OrganizationsNotificationsSent.SetupType == 1 && rec.NotificationType == 0).OrderByDescending(rec => rec.EmailSentDateTime).Skip(NumberOfItemsToSkip).Take(31);

                foreach (var orglog in orgNotificationsEmailLogs.ToList())
                {

                    LocalNotifications locNotification = new LocalNotifications();

                    if (notifications.Count >= 30)
                        break;

                    //Rajesh on 12/12/2013
                    locNotification.HasNext = false;
                    locNotification.HasPrevious = pageNumber != 0;
                    locNotification.PageNumber = pageNumber;
                    locNotification.EmailSentDate = orglog.EmailSentDateTime; // kiran on 9/8/2014
                                                                              //<<Ends


                    locNotification.ClientName = orglog.OrganizationsNotificationsSent.Client.Name;

                    //Rajesh on 12/12/2013
                    locNotification.VendorName = orglog.Vendor.Name + "";//entityDB.OrganizationsNotificationsEmailLogs.FirstOrDefault().Vendor.Name;//
                                                                         //Ends
                    locNotification.Purpose = "";
                    var orgEmailSetupNotification = entityDB.OrganizationsEmailSetupNotifications.FirstOrDefault(rec => rec.OrgEmailSetupId == orglog.OrganizationsNotificationsSent.SetupTypeId);
                    if (orgEmailSetupNotification != null)
                    {
                        if (orgEmailSetupNotification.SetupType == 0)//Document
                        {
                            locNotification.Purpose = entityDB.DocumentType.Find(orgEmailSetupNotification.SetupTypeId).DocumentTypeName;
                        }
                        else if (orgEmailSetupNotification.SetupType == 1)//Status
                        {
                            locNotification.Purpose = entityDB.PrequalificationStatus.Find(orgEmailSetupNotification.SetupTypeId).PrequalificationStatusName;
                        }
                    }

                    //Dec 4th
                    locNotification.Comments = orglog.Comments;
                    if (orglog.Comments != null)
                    {
                        if (orglog.Comments.Length > 30)
                            locNotification.SmallComment = orglog.Comments.Substring(0, 30);
                        else
                            locNotification.SmallComment = orglog.Comments;
                    }
                    else
                        locNotification.SmallComment = "";
                    notifications.Add(locNotification);
                }

                if (orgNotificationsEmailLogs.Count() > 30)
                {
                    notifications.ForEach(rec => rec.HasNext = true);
                }
                ViewBag.NotificationsSent = notifications;
            }
            return PartialView();

        }



        [HttpPost]
        public ActionResult PrequalificationPartialView(int pageNumber, int? type)//Null or 0-All Prequalifications 1-Incomplete 2 - Client review
        {

            ViewBag.type = type;
            var clientId = Convert.ToInt64(Session["currentOrgId"]);
            ViewBag.VBPrequalificationList = _prequalification.GetPrequalificationsforAdminDashboard(pageNumber, type);
            return PartialView();
        }

        public ActionResult DownloadPrequalifications(int? type)//Null or 0-All Prequalifications 1-Incomplete 2 - Client review
        {
            var info = _prequalification.GetPrequalificationsforAdminDashboard(-10, type);
            var exportList = info.Select(r => new[]
            {
                "\""+r.VendorName+"\"",
                "\""+r.ClientName+"\"",
                "\""+r.PrequalificationSubmit?.ToString("G")+"\"",
                "\""+r.PrequalificationStart.ToString("MM/dd/yyyy")+" - "+r.PrequalificationFinish.ToString("MM/dd/yyyy")+"\"",
                "\""+r.Initials+"\""
            }).ToList();
            exportList.Insert(0, new String[] { "Vendor", "Client", "Submitted On", "Period", "Initials" });
            var sb = new StringBuilder();
            foreach (var data in exportList)
            {
                sb.AppendLine(string.Join(",", data));
            }
            var fileName = "";
            if (type == 1)
                fileName = "Incomplete Prequalifications";
            else if (type == 2)
                fileName = "Client Review Prequalifications";
            else
                fileName = "Processing Prequalifications";
            return File(new UTF8Encoding().GetBytes(sb.ToString()), "text/csv", $"{fileName}.csv");
        }

        [SessionExpire]
        [HeaderAndSidebar(headerName = "DashBoard", sideMenuName = "")]
        public string markAllNotificationsAsRead()
        {
            string statusStr = "updateSuccess";
            var entityDB = new EFDbContext();
            {
                entityDB.Database.ExecuteSqlCommand(
                    "update OrganizationsNotificationsEmailLogs set ReadByAdmin =1 where ReadByAdmin=0");
                //List<OrganizationsNotificationsEmailLogs> logs = entityDB.OrganizationsNotificationsEmailLogs.Where(log => log.ReadByAdmin == 0).ToList();
                //foreach (var emailLog in logs)
                //{
                //    emailLog.ReadByAdmin = 1;
                //    entityDB.Entry(emailLog).State = EntityState.Modified;

                //    statusStr = "updateSuccess";
                //}
                //entityDB.SaveChanges();

                ViewBag.NotificationCount = 0;
            }
            return statusStr;
        }
        // End <<<--



        //============================SUMA on (MM/DD/YYYY)5-25-2013 for VENDOR Search==========================>
        [HttpGet]
        [SessionExpire]
        [OutputCache(Duration = 0)]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "Vendors", sideMenuName = "VendorSearch", ViewName = "VendorSearch")]
        public ActionResult VendorSearch()
        {
            var entityDB = new EFDbContext();
            {
                var Countries = _organization.GetCountries().Select(r => new SelectListItem()
                {
                    Text = r.Value,
                    Value = r.Value
                }).ToList();

                Countries.Insert(0, (new SelectListItem
                {
                    Text = "Any",
                    Value = ""
                }));
                ViewBag.Countries = Countries;
                var Regions = entityDB.Regions.Select(r => new SelectListItem() { Text = r.RegionName, Value = r.RegionName }).ToList();
                Regions.Insert(0, (new SelectListItem
                {
                    Text = "Any",
                    Value = ""
                }));
                ViewBag.Regions = Regions;
                LocalSearchVendors searchVendors = new LocalSearchVendors();
                searchVendors.StatusList = new List<bool>();
                searchVendors.BiddingList = new List<bool>();
                searchVendors.RegionsList = new List<bool>();
                //Suma on 9/7/2013 to restrict vendorsearch view based on logintype==>>>
                //Client Names along with their ID's for Dropdown list

                var RoleName = Session["RoleName"];
                var varorgId = (long)Session["currentOrgId"];
                if (RoleName.Equals("Client") || RoleName.Equals(OrganizationType.SuperClient))
                {
                    if (_sysUser.IsERIUser(SSession.UserId))
                    {
                        var varOrgList = _clientBusiness.GetSubClients(varorgId, true).Select(r => new SelectListItem() { Value = r.Key, Text = r.Value, Selected = r.Key == varorgId.ToString() ? true : false });
                        searchVendors.Name = varorgId;
                        ViewBag.VBOrglist = varOrgList;

                    }
                    else
                    {
                        var varOrgList = from org in entityDB.Organizations.Where(records => records.OrganizationID == varorgId).OrderBy(rec => rec.Name).ToList()
                                         select new SelectListItem
                                         {
                                             Text = org.Name,
                                             Value = org.OrganizationID.ToString()
                                         };

                        ViewBag.VBOrglist = varOrgList;
                    }

                }
                else if (RoleName.Equals("Admin"))
                {
                    //if (_sysUser.IsERIUser(SSession.UserId))
                    //{
                    //    var varOrgList = _clientBusiness.GetEriClients().Select(r => new SelectListItem() { Value = r.Key, Text = r.Value });
                    //    ViewBag.VBOrglist = varOrgList;
                    //}
                    //else
                    {
                        var varOrgList = (from org in entityDB.Organizations.Where(records => (records.OrganizationType == OrganizationType.Client || records.OrganizationType == OrganizationType.SuperClient) && records.ShowInApplication == true).OrderBy(rec => rec.Name).ToList()
                                          select new SelectListItem
                                          {
                                              Text = org.Name,
                                              Value = org.OrganizationID.ToString()
                                          }).ToList();
                        //varOrgList.Insert(0, new SelectListItem() { Text = Resources.Resources.ERI, Value = LocalConstants.ERI.ToString() });
                        ViewBag.VBOrglist = varOrgList;
                    }
                }
                //===>>>Ends

                //Client Names along with their ID's for Dropdown list
                //var varOrgList = from org in entityDB.Organizations.Where(records => records.OrganizationType == "Client").ToList()
                //                 select new SelectListItem
                //                 {
                //                     Text = org.Name,
                //                     Value = org.OrganizationID.ToString()
                //                 };

                //ViewBag.VBOrglist = varOrgList;

                //Get prequalificationStatusNames
                //var varStatusname = from preStatus in entityDB.PrequalificationStatus.ToList().Where(rec=>rec.ShowInDashBoard==true)
                //                    select preStatus;
                var varStatusname = entityDB.PrequalificationStatus.ToList().Where(rec => rec.ShowInDashBoard == true).OrderBy(rec => rec.PrequalificationStatusName); // Kiran on 01/13/2015


                //Status CheckBoxes
                searchVendors.PrequalificationStatusList = varStatusname.ToList();
                foreach (var i in searchVendors.PrequalificationStatusList)
                {
                    searchVendors.StatusList.Add(false);
                }
                //var Regions = entityDB.Regions.OrderBy(r => r.RegionName).ToList();
                //searchVendors.GeographicArea = Regions;
                //foreach (var i in searchVendors.GeographicArea)
                //{
                //    searchVendors.RegionsList.Add(false);
                //}
                //for (int i = 0; i < 13; i++)
                //{
                //    searchVendors.BiddingList.Add(false);
                //}
                //////////var biddingName = from n in entityDB.BiddingInterests.ToList()
                //////////                  select n;
                //////////searchVendors.BiddingInterestList = biddingName.ToList();

                //////////foreach (var i in searchVendors.BiddingInterestList)
                //////////{
                //////////    searchVendors.BiddingList.Add(false);
                //////////}

                return View(searchVendors);
            }
        }
        // Ends <<<

        public ActionResult GetClientContractTypes(long clientId)
        {
            var clientContractTypes = _clientBusiness.GetClientContractTypes(clientId);


            clientContractTypes.Insert(0, new KeyValuePair<string, string>("None", "None"));

            return Json(clientContractTypes, JsonRequestBehavior.AllowGet);
        }

        /*
        Created Date:  10/02/2013   Created By: Kiran Talluri
        Purpose: To Display the list of Bidding Interests as well as Proficiency Capabilities under advanced tab in vendor search
        */
        public ActionResult BiddingOrProficiencyResultsPartial(long ClientId)
        {
            var entityDB = new EFDbContext();
            {
                // kiran on 10/2/2014
                var clientTemplates = entityDB.ClientTemplates.Where(rec => rec.ClientID == ClientId).ToList();
                List<long?> templateLanguages = new List<long?>();
                foreach (var clientTemplate in clientTemplates)
                {
                    if (ViewBag.VBBiddingsSectionExists != null && ViewBag.VBBiddingsSectionExists == true)
                        break;
                    var templateSections = entityDB.TemplateSections.Where(rec => rec.TemplateID == clientTemplate.TemplateID && rec.TemplateSectionType == 1 && rec.Visible == true).ToList();
                    foreach (var templateSection in templateSections)
                    {
                        if (ViewBag.VBBiddingsSectionExists != null && ViewBag.VBBiddingsSectionExists == true)
                            break;
                        if (templateSection.TemplateSubSections.Any(templateSubsection => templateSubsection.SubSectionType == 4 && templateSubsection.Visible == true && templateSubsection.SubSectionTypeCondition == "BiddingReporting"))
                        {
                            ViewBag.VBBiddingsSectionExists = true;
                        }
                    }
                    templateLanguages.Add(clientTemplate.Templates.LanguageId);
                }
                //check for clayco bidding interest
                foreach (var clientTemplate in clientTemplates)
                {
                    if (ViewBag.VBClaycoBiddingsSectionExists != null && ViewBag.VBClaycoBiddingsSectionExists == true)
                        break;
                    var templateSections = entityDB.TemplateSections.Where(rec => rec.TemplateID == clientTemplate.TemplateID && rec.TemplateSectionType == 1 && rec.Visible == true).ToList();
                    foreach (var templateSection in templateSections)
                    {
                        if (ViewBag.VBClaycoBiddingsSectionExists != null && ViewBag.VBClaycoBiddingsSectionExists == true)
                            break;
                        if (templateSection.TemplateSubSections.Any(templateSubsection => templateSubsection.SubSectionType == 4 && templateSubsection.Visible == true && templateSubsection.SubSectionTypeCondition == "ClaycoBiddingReporting"))
                        {
                            ViewBag.VBClaycoBiddingsSectionExists = true;
                        }
                    }
                    templateLanguages.Add(clientTemplate.Templates.LanguageId);
                }
                // check for MSC industrial product categories subsection
                foreach (var clientTemplate in clientTemplates)
                {
                    if (ViewBag.VBProductCategories != null && ViewBag.VBProductCategories == true)
                        break;
                    var templateSections = entityDB.TemplateSections.Where(rec => rec.TemplateID == clientTemplate.TemplateID && rec.TemplateSectionType == 1 && rec.Visible == true).ToList();
                    foreach (var templateSection in templateSections)
                    {
                        if (ViewBag.VBProductCategories != null && ViewBag.VBProductCategories == true)
                            break;
                        if (templateSection.TemplateSubSections.Any(templateSubsection => templateSubsection.SubSectionType == 4 && templateSubsection.Visible == true && templateSubsection.SubSectionTypeCondition == "ProductCategoriesReporting"))
                        {
                            ViewBag.VBProductCategories = true;
                        }
                    }
                    templateLanguages.Add(clientTemplate.Templates.LanguageId);
                }
                if (ViewBag.VBClaycoBiddingsSectionExists == true)
                {
                    ViewBag.VBClaycoBiddingInterestList = entityDB.ClaycoBiddingInterests.Where(rec => rec.Status == 1 && templateLanguages.Contains(rec.LanguageId)).OrderBy(rec => rec.BiddingInterestName).ToList(); // Kiran on 01/20/2015 // sumanth on 10/31/2015 for FVOS-108 to filter out inactive bidding interests
                }
                if (ViewBag.VBBiddingsSectionExists == true)
                {
                    ViewBag.VBBiddingInterestList = entityDB.BiddingInterests.Where(rec => rec.Status == 1 && templateLanguages.Contains(rec.LanguageId)).OrderBy(rec => rec.BiddingInterestName).ToList(); // Kiran on 01/20/2015 // sumanth on 10/31/2015 for FVOS-108 to filter out inactive bidding interests
                }
                else
                {
                    var biddingInterestsIds =
                        entityDB.ClientTemplateReportingData.Where(
                                record => record.ClientTemplates.ClientID == ClientId && record.ReportingType == 0)
                            .Select(rec => rec.ReportingTypeId)
                            .ToList();

                    var clientBiddingInterests =
                            entityDB.BiddingInterests.Where(
                                record => biddingInterestsIds.Contains(record.BiddingInterestId) && record.Status == 1 && templateLanguages.Contains(record.LanguageId)).ToList();
                    // sumanth on 10/31/2015 for FVOS-108 to filter out inactive bidding interests

                    ViewBag.VBBiddingInterestList = clientBiddingInterests.ToList();
                }
                var proficiencyCapabilitiesIds = entityDB.ClientTemplateReportingData.Where(record => record.ClientTemplates.ClientID == ClientId && record.ReportingType == 1).Select(rec => rec.ReportingTypeId).ToList();

                var clientProfiencyCapabilities = entityDB.ProficiencyCapabilities.Where(record => proficiencyCapabilitiesIds.Contains(record.ProficiencyId) && record.Status == 1).GroupBy(record => record.ProficiencyCategory).ToList();// sumanth on 10/31/2015 for FVOS-108 to filter out inactive bidding interests

                ViewBag.VBProficiencyCapabilitiesList = clientProfiencyCapabilities.ToList();

                if (ViewBag.VBProductCategories == true)
                {
                    ViewBag.VBProductCategories = entityDB.ProductCategories.Where(rec => rec.Status == 1).OrderBy(rec => rec.ProductCategoryName).ToList();
                }
                return PartialView();
            }
            // Ends<<<

        }

        // Ends <<<

        // Suma 25/05/2013 -->
        // Search by Client Business Units --->
        [HttpPost]
        public JsonResult BUnitDetails(long BUid)
        {
            var entityDB = new EFDbContext();
            {
                var varBusinessUnitList = from Units in entityDB.ClientBusinessUnits.Where(Units => Units.ClientId == BUid).ToList()
                                          select new SelectListItem
                                          {
                                              Text = Units.BusinessUnitName,
                                              Value = Units.ClientBusinessUnitId.ToString()
                                          };

                var result = varBusinessUnitList.ToList();

                return Json(varBusinessUnitList, JsonRequestBehavior.AllowGet);
            }

        }
        // Ends <<<

        // Suma 25/05/2013 -->
        // Search by Client Business Unit's Sites --->
        [HttpPost]
        public JsonResult BUSiteDetails(string BUSiteid)// changes from long to string by mani on 8/3/2015 for fv-98
        {
            var entityDB = new EFDbContext();
            {
                BUSiteid = BUSiteid.Replace("Any", "1");
                var Siteids = Array.ConvertAll(BUSiteid.Split(','), p => Convert.ToInt64(p));
                var varSiteList = from UnitSites in entityDB.ClientBusinessUnitSites.Where(rec => Siteids.Contains(rec.ClientBusinessUnitId)).OrderBy(rec => rec.SiteName).ToList()
                                  select new SelectListItem
                                  {
                                      Text = UnitSites.SiteName + " - " + UnitSites.BusinessUnitLocation,
                                      Value = UnitSites.ClientBusinessUnitSiteId.ToString()
                                  };

                var result = varSiteList.ToList();

                return Json(varSiteList, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [SessionExpireForView]
        [OutputCache(Duration = 0)]
        public ActionResult VendorSearchResultsPartialView(DataSourceRequest request, string Subsite, string vendorCity, string Country, string vendorState, string biddingIdList,
                                string statusIdList, string proficiencyIdList, string VendorName, string periodVal, string sortType, long clientId, string searchType,
                                string firstLetterVal, Guid? clientTemplateid, string Site, string clientvendorrefid, string claycoBiddingIdList, string clientContractId, string productCategoryList, string GeographicArea, bool resubOnly = false)
        {
            //var entityDB = new EFDbContext();
            //{
            var HasAnalyticsPermission = _sysUser.HasScreenPermission("FinancialAnalytics", "", "ClientAndAdminViewFinancialAnalytics", SSession.HeaderMenus);
            VendorName = Server.UrlDecode(VendorName);
            LocalSearchVendors searchVendors = new LocalSearchVendors();
            ViewBag.VBAdvanced = 0;
            var venname = VendorName.Trim(); // Kiran on 03/18/2015
            var period = periodVal;
            var sort = sortType;
            var clientrefid = clientvendorrefid.Trim();
            //var scope = Name;

            var search = searchType;
            var varclientId = clientId;
            Session["SelectedClientID"] = varclientId;

            //var varOrglist = from org in entityDB.Organizations.ToList()
            //                 select new SelectListItem
            //                 {
            //                     Text = org.Name,
            //                     Value = org.OrganizationID.ToString()
            //                 };

            //ViewBag.VBOrglist = varOrglist;
            //========Search BY NAME============ 
            if (searchType == "Name" || searchType == "FirstLetter")
            {
                var result = _vendor.GetGroupedPrequalifications(clientId, searchType, sortType, firstLetterVal, period, SSession.UserId, SSession.Role, venname, HasAnalyticsPermission, SSession.OrganizationId, request);
                // result.Data.ForEach(r => { r.Status = GetPQStatusForBanner(r.PrequalificationId, r.Status); r.HasPermission = HasAnalyticsPermission; });
                //    var result = new DataSourceResponse() { Total = varPrequalificationlist.Count(), Data = Data };
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            //    var listOfPrequalifications = GetGroupedPrequalifications(clientId,entityDB);
            //var varPrequalificationlist = listOfPrequalifications;
            //if (searchType == "Name")
            //{
            //    ViewBag.VBAdvanced = 0;

            //    //Vendor Name field is empty
            //    if (string.IsNullOrEmpty(venname))
            //    {

            //        if (period == "searchAll")
            //        {
            //            varPrequalificationlist = listOfPrequalifications.Where(rec => rec.ShowInDashBoard == true);
            //        }
            //        else if (period == "searchParticular")
            //        {
            //            var baseDate = DateTime.Now.AddDays(-25);
            //            varPrequalificationlist = listOfPrequalifications.Where(rec => rec.PrequalificationFinish >= baseDate && rec.ShowInDashBoard == true);
            //        }

            //    }
            //    else
            //    {

            //        if (period == "searchAll")
            //        {
            //            varPrequalificationlist = listOfPrequalifications.Where(rec => rec.VendorName.ToLower().Contains(venname.ToLower()) && rec.ShowInDashBoard == true);
            //        }
            //        else if (period == "searchParticular")
            //        {
            //            var baseDate = DateTime.Now.AddDays(-25);
            //            varPrequalificationlist = listOfPrequalifications.Where(rec => rec.VendorName.ToLower().Contains(venname.ToLower()) && rec.PrequalificationFinish >= baseDate && rec.ShowInDashBoard == true);
            //        }
            //    }
            //    varPrequalificationlist = SortingBasedOnNameStatus(sortType, varPrequalificationlist);

            //    var Data = varPrequalificationlist.Skip((request.PageSize * (request.Page - 1))).Take(request.PageSize).ToList();
            //    Data.ForEach(r => { r.Status = GetPQStatusForBanner(r.PrequalificationId, r.Status); r.HasPermission = HasAnalyticsPermission; });
            //    var result = new DataSourceResponse() { Total = varPrequalificationlist.Count(), Data = Data };
            //    return Json(result, JsonRequestBehavior.AllowGet);

            //}

            //////=========SEARCH BY FIRST LETTER================
            //if (searchType == "FirstLetter")
            //{
            //    ViewBag.VBAdvanced = 0;
            //    if (period == "searchAll")
            //    {
            //        varPrequalificationlist = listOfPrequalifications.Where(rec => rec.VendorName.ToLower().StartsWith(firstLetterVal) && rec.ShowInDashBoard == true);
            //    }
            //    else if (period == "searchParticular")
            //    {
            //        var baseDate = DateTime.Now.AddDays(-25);
            //        varPrequalificationlist = listOfPrequalifications.Where(rec => rec.VendorName.ToLower().StartsWith(firstLetterVal) && rec.PrequalificationFinish >= baseDate && rec.ShowInDashBoard == true);
            //    }
            //    varPrequalificationlist = SortingBasedOnNameStatus(sortType, varPrequalificationlist);
            //    //ViewBag.VBPrequalificationList = varPrequalificationlist;
            //    //ViewBag.VBCount = varPrequalificationlist.Count;
            //    // return PartialView("VendorSearchResultsPartialView");
            //    var Data = varPrequalificationlist.Skip((request.PageSize * (request.Page - 1))).Take(request.PageSize).ToList();
            //    Data.ForEach(r => { r.Status = GetPQStatusForBanner(r.PrequalificationId, r.Status); r.HasPermission = HasAnalyticsPermission; });
            //    var result = new DataSourceResponse() { Total = varPrequalificationlist.Count(), Data = Data };
            //    return Json(result, JsonRequestBehavior.AllowGet);
            //}

            //==================SEARCH BY ADVANCED==============
            //Changes on 10/9/2013 by Suma =======>>>>>>>>>
            else if (searchType == "Advanced")
            {

                ViewBag.VBAdvanced = 1;
                //SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["EFDbContext"].ConnectionString);
                //connection.Open();
                //var command = connection.CreateCommand();

                //                   List<LocalSortingForNameStatusAndStatusPeriod> PrequalificationAdvancedDetails = new List<LocalSortingForNameStatusAndStatusPeriod>();


                //                   //For Vendor city State and Name 

                //                   if (vendorState == "select") vendorState = "";

                //                   var strStateCityName = "";
                //                   if ((String.IsNullOrEmpty(venname)) && (String.IsNullOrEmpty(vendorState)) && (String.IsNullOrEmpty(vendorCity)))
                //                       strStateCityName = "";

                //                   else if ((!String.IsNullOrEmpty(venname)) && (!String.IsNullOrEmpty(vendorState)) && (!String.IsNullOrEmpty(vendorCity)))
                //                       strStateCityName = " org.State  like @vendorState and  org.City like @vendorCity and org.Name like @venname";

                //                   else if (String.IsNullOrEmpty(venname) && (!String.IsNullOrEmpty(vendorState)) && (!String.IsNullOrEmpty(vendorCity)))
                //                       strStateCityName = "org.State  like @vendorState and  org.City like @vendorCity";

                //                   else if (String.IsNullOrEmpty(vendorState) && (!String.IsNullOrEmpty(venname)) && (!String.IsNullOrEmpty(vendorCity)))
                //                       strStateCityName = "org.City like @vendorCity and org.Name like @venname";

                //                   else if (String.IsNullOrEmpty(vendorCity) && (!String.IsNullOrEmpty(vendorState)) && (!String.IsNullOrEmpty(venname)))
                //                       strStateCityName = " org.State  like @vendorState and org.Name like @venname";

                //                   else if (String.IsNullOrEmpty(venname) && String.IsNullOrEmpty(vendorState) && (!String.IsNullOrEmpty(vendorCity)))
                //                       strStateCityName = " org.City like @vendorCity";

                //                   else if (String.IsNullOrEmpty(venname) && String.IsNullOrEmpty(vendorCity) && (!String.IsNullOrEmpty(vendorState)))
                //                       strStateCityName = "org.State  like @vendorState";

                //                   else if (String.IsNullOrEmpty(vendorCity) && String.IsNullOrEmpty(vendorState) && (!String.IsNullOrEmpty(venname)))
                //                       strStateCityName = "org.Name like @venname";

                //                   if ((!String.IsNullOrEmpty(Country)) && (String.IsNullOrEmpty(strStateCityName)))
                //                   {
                //                       strStateCityName += " org.Country like @vencountry";
                //                   }
                //                   else if ((!String.IsNullOrEmpty(Country)) && (!String.IsNullOrEmpty(strStateCityName)))
                //                   {
                //                       strStateCityName += " and org.Country like @vencountry";
                //                   }
                //                   if (!String.IsNullOrEmpty(strStateCityName))
                //                   {
                //                       strStateCityName = "and(" + strStateCityName + ")";

                //                   }
                //                   var Area = "";
                //                   if (!string.IsNullOrEmpty(GeographicArea) && GeographicArea != "null")
                //                   {
                //                       var GeoList = GeographicArea.Split(',');
                //                       var regions = entityDB.Regions.Where(r => GeoList.Contains(r.RegionName)).Select(r => r.RegionId);
                //                       var RegionIds = string.Join(",", regions);
                //                       Area = "and org.GeographicArea in (select RegionName from Regions where RegionId in(" + RegionIds + "))";


                //                   }
                //                   //var GeoArea = "";
                //                   //if(!string.IsNullOrEmpty(GeographicArea))
                //                   //{ var GeoList = GeographicArea.Split(',');
                //                   //    var regions = entityDB.Regions.Where(r => GeoList.Contains(r.RegionName)).Select(r => r.RegionId);
                //                   //  var RegionIds=  string.Join(",", regions);
                //                   //    var myList = new List<string> { GeographicArea};
                //                   //    GeoArea = "and q.QuestionText='In which geographic area(s) do you normally work? (Ctrl-click for multiple selections)' and pu.UserInput in(select RegionName from Regions where RegionId in(" + RegionIds+") )";
                //                   //}
                //                   //Suma 04/10/2013===>    

                //                   //For StatusIdList
                //                   var strStatusSql = @"case 
                //   when psiteStatuses.cou is null or psiteStatuses.cou = 0 then ps.PrequalificationStatusName
                //else dbo.ufnGetPQStatus(p.PrequalificationId, @clientIdForStatus, @userid, ps.PrequalificationStatusName) end in(select PrequalificationStatusName from PrequalificationStatus where PrequalificationStatusId in( select * from dbo.ufn_ParseIntList(@statusIdList))) and";
                //                   if (String.IsNullOrEmpty(statusIdList)) strStatusSql = " ";

                //                   // Kiran on 04/10/2015
                //                   var questionnaireSearch = "";
                //                   if (clientTemplateid != null)
                //                       questionnaireSearch = "and c.ClientTemplateId=@clientTemplateid";
                //                   // Ends<<<

                //                   // sumanth on 08/12/2015
                //                   var strClientRefidSql = "";
                //                   if (!String.IsNullOrEmpty(clientrefid))
                //                   {

                //                       long clientid = 0;
                //                       long vendorid = 0;
                //                       var clientrefdetails = entityDB.ClientVendorReferenceIds.FirstOrDefault(rec => rec.ClientVendorReferenceId == clientrefid);
                //                       if (clientrefdetails != null)
                //                       {
                //                           clientid = clientrefdetails.ClientId;
                //                           vendorid = clientrefdetails.VendorId;

                //                       }
                //                       strClientRefidSql = " and p.VendorId in (select VendorId from ClientVendorReferenceIds where ClientId=@varclientId and ClientVendorReferenceId like '%" + @clientrefid + "%')";

                //                       //strClientRefidSql = " and p.ClientId=" + clientid + " and p.VendorId=" + vendorid;
                //                   }

                //                   var strClientContractSql = "";
                //                   if (!string.IsNullOrEmpty(clientContractId) && clientContractId.ToLower() != "none")
                //                   {
                //                       strClientContractSql =
                //                           " and p.VendorId in (select VendorId from ClientVendorReferenceIds where ClientId=@varclientId  and ClientContractId='" +
                //                           clientContractId + "')";

                //                   }

                //                   // ends <<<
                //                   //For Select statement
                //                   var strMainsql = "select distinct org.Name VendorName," +

                //                                      "p.PrequalificationStart,p.PrequalificationFinish,p.VendorId ,p.PrequalificationId,p.PrequalificationStatusId,t.TemplateCode,p.ClientId, ps.PrequalificationStatusName as Status from LatestPrequalification AS p " +//Suma on 2/12/2013  //Suma on 4/12/2013,c.TemplateID
                //                                      "left join (select count(*) cou,PrequalificationId from PrequalificationSites where PQSiteStatus is not null group by PrequalificationId) psiteStatuses on psiteStatuses.PrequalificationId = p.PrequalificationId " +
                //                                      "INNER JOIN PrequalificationStatus AS ps " +
                //                                      " ON ps.PrequalificationStatusId = p.PrequalificationStatusId " +
                //                                      " inner join ClientTemplates c on p.ClientTemplateId = c.ClientTemplateID" +
                //                                      " inner join Templates t on t.TemplateID=c.TemplateID " +
                //                                      " INNER JOIN  Organizations AS org ON org.OrganizationID = p.VendorId ";
                //                   //"Left JOIN PrequalificationSites as psite ON p.prequalificationId=psite.prequalificationid " +
                //                   //Removed this as introduced LatestPQ
                //                   //" INNER JOIN (select lp.VendorId, MAX(PrequalificationStart) as prequalStart from LatestPrequalification lp where (ClientId=@clientId or PrequalificationId in(select PQId from PrequalificationClient where ','+ClientIds+',' like @p0))" + questionnaireSearch + // Kiran on 04/10/2015
                //                   //" group by(lp.VendorId)) latest ON (p.PrequalificationStart = latest.prequalStart and p.VendorId = latest.VendorId)"; // Kiran on 11/27/2014


                //                   // " LEFT JOIN (select pre.VendorId, MAX(pre.PrequalificationStart)as latestPrequalificationStart from Prequalification pre group by pre.VendorId) latest ON p.PrequalificationStart = latest.latestPrequalificationStart and p.VendorId = latest.VendorId "; // Kiran on 04/30/2014
                //                   //" INNER JOIN ClientTemplates AS c ON c.ClientID = org.OrganizationID ";//Suma on 2/12/2013  //Suma on 4/12/2013

                //                   if (resubOnly)
                //                   {
                //                       strMainsql += "INNER JOIN (select PrequalificationId,count(*)  from PrequalificationStatusChangeLog where PrequalificationStatusId = 5 group by PrequalificationId having count(*) > 1) resubbed on resubbed.PrequalificationId = p.PrequalificationId ";
                //                   }

                //                   //For Innerjoin of PrequalificationReportindData
                //                   var strPrequalificationReportingDatasql = "INNER JOIN  PrequalificationReportingData AS prd ON (prd.PrequalificationId = p.PrequalificationId";

                //                   var biddingsql = "prd.ReportingType = 0 and prd.ReportingDataId in (select * from dbo.ufn_ParseIntList(@biddingIdList))";
                //                   var proficiencysql = "prd.ReportingType = 1 and prd.ReportingDataId in (select * from dbo.ufn_ParseIntList(@proficiencyIdList))";
                //                   var claycobiddingsql = "prd.ReportingType = 3 and prd.ReportingDataId in (select * from dbo.ufn_ParseIntList(@claycoBiddingIdList))";
                //                   var productCategoriesSql = "prd.ReportingType = 4 and prd.ReportingDataId in (select * from dbo.ufn_ParseIntList(@productCategoryList))";


                //                   var ProfBidsql = "";



                //                   if (String.IsNullOrEmpty(claycoBiddingIdList) && String.IsNullOrEmpty(biddingIdList) && (String.IsNullOrEmpty(proficiencyIdList)) && String.IsNullOrEmpty(productCategoryList))
                //                   {
                //                       //Suma on 10/15/2013 >>>
                //                       strPrequalificationReportingDatasql = " ";
                //                       ProfBidsql = " ";
                //                       //<<<Ends
                //                   }
                //                   else if (!String.IsNullOrEmpty(biddingIdList) && !String.IsNullOrEmpty(proficiencyIdList) && !String.IsNullOrEmpty(claycoBiddingIdList))
                //                   {
                //                       ProfBidsql = "and(" + biddingsql + "or(" + proficiencysql + ") or (" + claycobiddingsql + ")))";
                //                   }
                //                   else if (!String.IsNullOrEmpty(biddingIdList) && !String.IsNullOrEmpty(proficiencyIdList) && String.IsNullOrEmpty(claycoBiddingIdList))
                //                   {
                //                       ProfBidsql = "and(" + biddingsql + "or(" + proficiencysql + ")))";
                //                   }
                //                   else if (!String.IsNullOrEmpty(biddingIdList) && String.IsNullOrEmpty(proficiencyIdList) && !String.IsNullOrEmpty(claycoBiddingIdList))
                //                   {
                //                       ProfBidsql = "and(" + biddingsql + "or(" + claycobiddingsql + ")))";
                //                   }
                //                   else if (String.IsNullOrEmpty(biddingIdList) && !String.IsNullOrEmpty(proficiencyIdList) && String.IsNullOrEmpty(claycoBiddingIdList))
                //                   {
                //                       ProfBidsql = "and(" + proficiencysql + "))";
                //                   }
                //                   else if (!String.IsNullOrEmpty(biddingIdList) && String.IsNullOrEmpty(proficiencyIdList) && String.IsNullOrEmpty(claycoBiddingIdList))
                //                   {
                //                       ProfBidsql = "and(" + biddingsql + "))";
                //                   }
                //                   else if (String.IsNullOrEmpty(biddingIdList) && String.IsNullOrEmpty(proficiencyIdList) && !String.IsNullOrEmpty(claycoBiddingIdList))
                //                   {
                //                       ProfBidsql = " and (" + claycobiddingsql + "))";
                //                   }

                //                   if (!String.IsNullOrEmpty(productCategoryList))
                //                       ProfBidsql = " and (" + productCategoriesSql + "))";

                //                   //For Subsite
                //                   var strsubSitesql = "Left JOIN PrequalificationSites AS psite ON (p.PrequalificationId = psite.PrequalificationId ";
                //                   //if (Subsite != null)
                //                   //{
                //                   //    strsubSitesql += "and psite.ClientBusinessUnitSiteId = @Subsite ";
                //                   //}
                //                   //if (Site != null)
                //                   //{
                //                   //    strsubSitesql += "and psite.ClientBusinessUnitId = @Site ";
                //                   //}
                //                   strsubSitesql += ")";
                //                   var siteCondition = "";
                //                   var subsite = Subsite.Split(',');
                //                   if (!subsite.Contains("Any") && !subsite.Contains(""))
                //                   {

                //                       siteCondition += "and psite.ClientBusinessUnitSiteId in( " + Subsite + ") ";

                //                   }
                //                   var site = Site.Split(',');
                //                   if (!site.Contains("Any") && !site.Contains(""))
                //                   {

                //                       siteCondition += "and psite.ClientBusinessUnitId in(" + Site + ") ";

                //                   }
                //                   var clientCondition = "(p.ClientId = @clientId or p.PrequalificationId in(select PQId from PrequalificationClient where ','+ClientIds+',' like @p0))";

                //                   if (SSession.Role == OrganizationType.SuperClient)
                //                   {
                //                       var IsSuperClient = entityDB.SubClients.FirstOrDefault(r => r.SuperClientId == clientId);
                //                       if (IsSuperClient != null)
                //                       {
                //                           clientCondition = "(p.clientid=@clientId)";
                //                       }
                //                       else { clientCondition = "(p.PrequalificationId in(select PQId from PrequalificationClient where ',' + ClientIds + ',' like @p0))"; }
                //                   }

                //                   //if (Subsite == null && Site == null) strsubSitesql = "";

                //                   //For sortType
                //                   var sortName = sortType;

                //                   if (String.IsNullOrEmpty(sortType) || (sortType == "Name")) sortName = "org.Name";
                //                   if (sortType == "Status") sortName = "Status";
                //                   if (sortType == "Status Period") sortName = "p.PrequalificationStart";

                //                   var VendorSearchSQL = "";

                //                   var userSites = "";
                //                   var userSitesRec = entityDB.SystemUsersBUSites.FirstOrDefault(r => r.SystemUserId == SSession.UserId);
                //                   if (userSitesRec != null)
                //                       userSites = userSitesRec.BUSiteId;
                //                   if (period == "searchAll")
                //                   {
                //                       VendorSearchSQL = " " + strMainsql + " " +
                //                                         " " + strStateCityName + " " +
                //                                         " " + strClientRefidSql + " " +
                //                                         " " + strClientContractSql + " " +
                //                                         " " + strPrequalificationReportingDatasql + " " +
                //                                         " " + ProfBidsql + " " +
                //                                         " " + strsubSitesql + " " +


                //                                          " where( t.IsHide=0 and " + strStatusSql + clientCondition + " and ps.ShowInDashBoard= 1 " + questionnaireSearch +

                //                                         (string.IsNullOrEmpty(userSites) ? "" : " and psite.ClientBusinessUnitSiteId in (" + userSites + " )")
                //                                         + siteCondition + Area + ") order by " + sortName + " ";

                //                   } //End of SearchAll

                //                   //Search for a Particular Prequalification StatusPeriod
                //                   else if (period == "searchParticular")
                //                   {
                //                       var baseDate = (DateTime.Now.AddDays(-25)).ToString("MM/dd/yyyy");
                //                       var currentDate = DateTime.Now.ToString("MM/dd/yyyy");
                //                       VendorSearchSQL = " " + strMainsql + " " +
                //                                         " " + strStateCityName + " " +
                //                                         " " + strClientRefidSql + " " +
                //                                         " " + strClientContractSql + " " +
                //                                         " " + strPrequalificationReportingDatasql + " " +
                //                                         " " + ProfBidsql + " " +
                //                                         " " + strsubSitesql + " " +

                //                                          " where( t.IsHide=0 and " + strStatusSql + clientCondition + Area +
                //                                         " and(p.PrequalificationStart >= " + baseDate + "and ps.ShowInDashBoard = 1 ) " +
                //                                      (string.IsNullOrEmpty(userSites) ? "" : " and psite.ClientBusinessUnitSiteId in (" + userSites + ")") + questionnaireSearch + siteCondition + " )order by " + sortName + " ";

                //                   }
                //                   long clientIdForStatus = 0;
                //                   if (SSession.Role == OrganizationType.Client)
                //                   {
                //                       clientIdForStatus = SSession.OrganizationId;
                //                   }
                //                   //entityDB = new EFDbContext();
                //                   var resultList = entityDB.Database.SqlQuery<LocalSortingForNameStatusAndStatusPeriod>(VendorSearchSQL + "  OFFSET " + ((request.Page - 1) * request.PageSize) + " ROWS FETCH NEXT " + request.PageSize + " ROWS ONLY", new SqlParameter("@Subsite", Subsite.ToStringNullSafe()),
                //                       new SqlParameter("@Site", Site.ToStringNullSafe()),
                //                       new SqlParameter("@vendorCity", "%" + vendorCity.ToStringNullSafe().Trim() + "%"),
                //                       new SqlParameter("@vendorState", "%" + vendorState.ToStringNullSafe() + "%"),
                //                       new SqlParameter("@biddingIdList", biddingIdList.ToStringNullSafe()),
                //                       new SqlParameter("@statusIdList", statusIdList.ToStringNullSafe()),
                //                       new SqlParameter("@proficiencyIdList", proficiencyIdList.ToStringNullSafe()),
                //                       new SqlParameter("@venname", "%" + venname.ToStringNullSafe() + "%"),
                //                         new SqlParameter("@vencountry", "%" + Country.ToStringNullSafe() + "%"),
                //                       new SqlParameter("@p0", "%," + clientId.ToLongNullSafe() + ",%"),
                //                       new SqlParameter("@clientId", clientId.ToStringNullSafe()),
                //                       new SqlParameter("@clientTemplateid", clientTemplateid.ToStringNullSafe()),
                //                       new SqlParameter("@clientrefid", clientrefid.ToStringNullSafe()),
                //                       new SqlParameter("@claycoBiddingIdList", claycoBiddingIdList.ToStringNullSafe()),
                //                       new SqlParameter("@varclientId", varclientId.ToStringNullSafe()),
                //                       new SqlParameter("@clientContractId", clientContractId.ToStringNullSafe()),
                //                       new SqlParameter("@clientIdForStatus", clientIdForStatus.ToStringNullSafe()), new SqlParameter("@userid", SSession.UserId.ToStringNullSafe()),
                //                       new SqlParameter("@productCategoryList", productCategoryList.ToStringNullSafe())
                //                       );
                //                   var resultListCount = entityDB.Database.SqlQuery<int>(("select count(*) from(" + VendorSearchSQL + ") as p").Replace("order by " + sortName, ""), new SqlParameter("@Subsite", Subsite.ToStringNullSafe()),
                //                       new SqlParameter("@Site", Site.ToStringNullSafe()),
                //                       new SqlParameter("@vendorCity", "%" + vendorCity.ToStringNullSafe().Trim() + "%"),
                //                       new SqlParameter("@vendorState", "%" + vendorState.ToStringNullSafe() + "%"),
                //                       new SqlParameter("@biddingIdList", biddingIdList.ToStringNullSafe()),
                //                       new SqlParameter("@statusIdList", statusIdList.ToStringNullSafe()),
                //                       new SqlParameter("@proficiencyIdList", proficiencyIdList.ToStringNullSafe()),
                //                       new SqlParameter("@venname", "%" + venname.ToStringNullSafe() + "%"),
                //                       new SqlParameter("@vencountry", "%" + Country.ToStringNullSafe() + "%"),
                //                       new SqlParameter("@p0", "%," + clientId.ToLongNullSafe() + ",%"),
                //                       new SqlParameter("@clientId", clientId.ToStringNullSafe()),
                //                       new SqlParameter("@clientTemplateid", clientTemplateid.ToStringNullSafe()),
                //                       new SqlParameter("@clientrefid", clientrefid.ToStringNullSafe()),
                //                       new SqlParameter("@claycoBiddingIdList", claycoBiddingIdList.ToStringNullSafe()),
                //                       new SqlParameter("@varclientId", varclientId.ToStringNullSafe()),
                //                       new SqlParameter("@clientContractId", clientContractId.ToStringNullSafe()),
                //                       new SqlParameter("@clientIdForStatus", clientIdForStatus.ToStringNullSafe()), new SqlParameter("@userid", SSession.UserId.ToStringNullSafe()),
                //                       new SqlParameter("@productCategoryList", productCategoryList.ToStringNullSafe())
                //                       );
                //                   var Data = resultList.ToList();
                //                   Data.ForEach(r => { r.TemplateCode = r.TemplateCode ?? ""; r.Status = GetPQStatusForBanner(r.PrequalificationId, r.Status); r.HasPermission = HasAnalyticsPermission; });

                DataSourceResponse resultData = GetVendorsForAdvancedSearch(request, Subsite, vendorCity, Country, vendorState, biddingIdList, statusIdList, proficiencyIdList, sortType, clientId, clientTemplateid, Site, claycoBiddingIdList, clientContractId, productCategoryList, GeographicArea, resubOnly, HasAnalyticsPermission, venname, period, clientrefid, varclientId);
                //var result = new
                //{
                //    Data = Data,
                //    Total = resultListCount.First()
                //};

                //var result = resultList.ToDataSourceResult(request);

                return Json(resultData, JsonRequestBehavior.AllowGet);

                //var result = command.ExecuteReader();

                ////Suma on 9/7/2013====>Includes else ,{ and }
                //if (result == null)
                //{
                //    ViewBag.VBcount = 0;
                //}
                ////>>>Ends
                //else
                //{
                //    while (result.Read())
                //    {
                //        LocalSortingForNameStatusAndStatusPeriod PrequalificationDetails = new LocalSortingForNameStatusAndStatusPeriod();
                //        PrequalificationDetails.VendorName = result[0].ToString();
                //        var clientid = (long)result[8];
                //        if (SSession.Role != Role.Client && clientid == -1)
                //        {
                //            clientid = 0;
                //        }
                //        if (SSession.Role == Role.Client)
                //        {
                //            clientid = varclientId;
                //        }
                //        PrequalificationDetails.Status = _prequalification.GetPQStatusForBanner((long)result[5], result[1].ToString(), clientid, SSession.UserId).Value;
                //        //PrequalificationDetails.PrequalificationStatusName = result[1].ToString();
                //        PrequalificationDetails.PrequalificationStart = (DateTime)result[2];
                //        PrequalificationDetails.PrequalificationFinish = (DateTime)result[3];
                //        PrequalificationDetails.VendorId = (long)result[4];
                //        // sampletable.TemplateId = (Guid)result[5]; //Suma on 2/12/2013
                //        PrequalificationDetails.PrequalificationId = (long)result[5];//Suma on 2/12/2013   //Suma on 4/12/2013 Changed sample and sample1 table names
                //        PrequalificationDetails.PrequalificationStatusId = (long)result[6];  // Suresh 12/27/2013
                //        PrequalificationDetails.TemplateCode = result[7].ToString();
                //        PrequalificationAdvancedDetails.Add(PrequalificationDetails);
                //    }
                //    ViewBag.VBPrequalificationlist = PrequalificationAdvancedDetails.ToList();
                //    ViewBag.VBcount = PrequalificationAdvancedDetails.Count();
                //    ViewBag.VBAdvanced = 1;
                //}
                //return PartialView();
                //}//end of SearchAll
            }//END OF Advanced VendorSearch
            return PartialView();
            //}
        }

        private DataSourceResponse GetVendorsForAdvancedSearch(DataSourceRequest request, string Subsite, string vendorCity, string Country, string vendorState, string biddingIdList, string statusIdList, string proficiencyIdList, string sortType, long clientId, Guid? clientTemplateid, string Site, string claycoBiddingIdList, string clientContractId, string productCategoryList, string GeographicArea, bool resubOnly, bool HasAnalyticsPermission, string venname, string period, string clientrefid, long varclientId)
        {
            //For Vendor city State and Name 
            return _vendor.GetVendorsForAdvancedSearch(request, Subsite, vendorCity, Country, vendorState, biddingIdList, statusIdList, proficiencyIdList, sortType, clientId, clientTemplateid, Site, claycoBiddingIdList, clientContractId, productCategoryList, GeographicArea, resubOnly, HasAnalyticsPermission, venname, period, clientrefid, varclientId, SSession.Role, SSession.OrganizationId, SSession.UserId);

            //return vendorState;
        }


        // Suma 25/05/2013 -->
        // Search results Partial view --->
        [HttpPost]
        [SessionExpireForView]

        private static IQueryable<LocalSortingForNameStatusAndStatusPeriod> SortingBasedOnNameStatus(string sortType, IQueryable<LocalSortingForNameStatusAndStatusPeriod> varPrequalificationlist)
        {
            if (sortType == "Name")
            {
                varPrequalificationlist = varPrequalificationlist.OrderBy(rec => rec.VendorName);
            }
            else if (sortType == "Status")
            {
                varPrequalificationlist = varPrequalificationlist.OrderBy(rec => rec.PrequalificationStatusId);
            }
            else if (sortType == "Status Period")
            {
                varPrequalificationlist = varPrequalificationlist.OrderBy(rec => rec.PrequalificationStart);
            }
            else
            {
                varPrequalificationlist = varPrequalificationlist.OrderBy(rec => rec.VendorName);
            }
            return varPrequalificationlist;
        }

        private IQueryable<LocalSortingForNameStatusAndStatusPeriod> GetGroupedPrequalifications(long clientId, EFDbContext entityDB)
        {
            //using (var entityDB = new EFDbContext())
            {
                var ERIPqs = entityDB.PrequalificationClient.SqlQuery("select * from PrequalificationClient where ','+ClientIds+',' like @p0", "%," + clientId + ",%")
                .Select(r => r.PQId).ToList();

                var prequalificationsGroupedByVendor = entityDB.LatestPrequalification.Where(rec => rec.ClientId == clientId || ERIPqs.Contains(rec.PrequalificationId));
                if (SSession.Role == OrganizationType.Admin)
                {
                    if (_sysUser.IsERIUser(SSession.UserId))
                    {
                        prequalificationsGroupedByVendor = entityDB.LatestPrequalification.Where(rec => (rec.ClientId == clientId || ERIPqs.Contains(rec.PrequalificationId))
                        && rec.ClientTemplates.Templates.IsERI == true);

                    }
                }
                if (SSession.Role == OrganizationType.Client || SSession.Role == OrganizationType.SuperClient)
                {


                    var sitesRec = entityDB.SystemUsersBUSites.FirstOrDefault(r => r.SystemUserId == SSession.UserId);
                    if (sitesRec != null && !string.IsNullOrEmpty(sitesRec.BUSiteId))
                    {

                        var busites = sitesRec.BUSiteId;
                        var userSites = busites.Split(',').Select(long.Parse).ToList();
                        //var pqids = entityDB.PrequalificationSites.Where(r => userSites.Contains(r.ClientBusinessUnitSiteId.Value)).Select(r => r.PrequalificationId).ToList();

                        prequalificationsGroupedByVendor = entityDB.LatestPrequalification.Where(r => r.PrequalificationSites.Any(r1 => userSites.Contains(r1.ClientBusinessUnitSiteId.Value)));


                    }
                    if (_sysUser.IsERIUser(SSession.UserId))
                    {
                        prequalificationsGroupedByVendor = entityDB.SubClients.FirstOrDefault(r => r.SuperClientId == clientId) == null ?
                        entityDB.LatestPrequalification.Where(rec => ERIPqs.Contains(rec.PrequalificationId)) : entityDB.LatestPrequalification.Where(rec => rec.ClientId == clientId);


                    }

                }
                prequalificationsGroupedByVendor = prequalificationsGroupedByVendor.Where(r => r.ClientTemplates.Templates.IsHide == false);
                var listOfPrequalifications = prequalificationsGroupedByVendor.Select(latestPrequalification => new LocalSortingForNameStatusAndStatusPeriod()
                {

                    VendorName = latestPrequalification.Vendor.Name,

                    //  var status=latestPrequalification.PrequalificationStatus.PrequalificationStatusName


                    Status = latestPrequalification.PrequalificationStatus.PrequalificationStatusName,
                    PrequalificationStatusId = latestPrequalification.PrequalificationStatusId,
                    VendorId = latestPrequalification.VendorId,
                    PrequalificationId = latestPrequalification.PrequalificationId,
                    PrequalificationStart = latestPrequalification.PrequalificationStart,
                    PrequalificationFinish = latestPrequalification.PrequalificationFinish,
                    ShowInDashBoard = latestPrequalification.PrequalificationStatus.ShowInDashBoard, // Kiran on 6/7/2014
                    ClientId = clientId,
                    TemplateCode = (latestPrequalification.ClientTemplates != null ? latestPrequalification.ClientTemplates.Templates.TemplateCode : "") ?? "",

                });

                return listOfPrequalifications;
            }
        }

        //===========================SUMA on 6-28-2013 for Admin deletes vendor=====>>>
        //Deletion of a Vendor from database should be done in the following order:Document,Prequalification,
        //SystemUsersOrganisation,Organisations,Contact,SystemusersLogs,Systemusers. 
        [SessionExpire]

        public ActionResult VendorDeleteConfirm(long selectedVendorId)
        {
            var entityDB = new EFDbContext();
            {
                Guid SelectedVendorUserId = new Guid();
                // Boolean deleteFlag =false;
                //ViewBag.VBdeleteFlag = false;
                var PrequalificationdeleteVendorRec = entityDB.Prequalification.Where(m => m.VendorId == selectedVendorId).ToList();


                foreach (var rec in PrequalificationdeleteVendorRec)
                {

                    if (rec.PrequalificationStatus.PrequalificationStatusId < 3)
                    {
                        entityDB.Entry(rec).State = EntityState.Deleted;
                        entityDB.SaveChanges();
                    }
                }

                var DocumentdeleteRec = entityDB.Document.First(m => m.OrganizationId == selectedVendorId);
                entityDB.Entry(DocumentdeleteRec).State = EntityState.Deleted;
                entityDB.SaveChanges();


                var SysUserOrgDeleteRec = entityDB.SystemUsersOrganizations.Where(m => m.OrganizationId == selectedVendorId).ToList();
                foreach (var rec in SysUserOrgDeleteRec)
                {
                    SelectedVendorUserId = rec.UserId;
                }
                foreach (var rec in SysUserOrgDeleteRec)
                {
                    SelectedVendorUserId = rec.UserId;
                    entityDB.Entry(rec).State = EntityState.Deleted;
                    entityDB.SaveChanges();
                }


                // var SysUserOrgID = entityDB.SystemUsersOrganizations.Where(m => m.OrganizationId == selectedVendorId).ToList();

                var SysUserLogDeleteRec = entityDB.SystemUserLogs.Where(m => m.SystemUserId == SelectedVendorUserId);
                foreach (var rec in SysUserLogDeleteRec)
                {
                    entityDB.Entry(rec).State = EntityState.Deleted;
                    entityDB.SaveChanges();
                }

                var SysUserDeleteRec = entityDB.SystemUsers.First(m => m.UserId == SelectedVendorUserId);
                entityDB.Entry(SysUserDeleteRec).State = EntityState.Deleted;
                entityDB.SaveChanges();

                var OrgdeleteRec = entityDB.Organizations.First(m => m.OrganizationID == selectedVendorId);
                entityDB.Entry(OrgdeleteRec).State = EntityState.Deleted;
                entityDB.SaveChanges();

                var ContactDeleteRec = entityDB.Contact.First(m => m.UserId == SelectedVendorUserId);
                entityDB.Entry(ContactDeleteRec).State = EntityState.Deleted;
                entityDB.SaveChanges();

                //entityDB.SaveChanges();
                //}
                return View("VendorSearchResultsPartialView");
            }
        }
        //Vendor Search Delete Ends<<<<
        public PartialViewResult VendorDetailERIQSections(long pqid)
        {
            using (var entityDB = new EFDbContext())
            {
                var finalstatus = _prequalification.Finalstatus();
                ViewBag.finalstatus = finalstatus;
                //var qAndA = new List<QuestionAndAnswers>();

                var clientId = 0L;
                if (SSession.Role.Equals(OrganizationType.Client))
                    clientId = SSession.OrganizationId;


                //var defaultStatus = entityDB.Prequalification.Find(pqid).PrequalificationStatus.PrequalificationStatusName;
                var defaultStatus = _prequalification.GetPQStatusName(pqid);
                ViewBag.StatusId = _prequalification.GetPQStatusForBanner(pqid, defaultStatus, clientId, SSession.UserId).Key;

                List<QuestionAndAnswers> qAndA = _prequalification.GetVendorDetailERIQSections(pqid);

                //var templateid = entityDB.Prequalification.Find(pqid).ClientTemplates.TemplateID;
                //var Questions = entityDB.VendorDetailQuestions.Where(r => r.TemplateId == templateid && r.Type == 1).Select(r => r.Questions).OrderBy(r => r.QuestionID).Select(r => new
                //{
                //    QText = r.QuestionText,
                //    QCids = r.QuestionColumnDetails.Select(r1 => r1.QuestionColumnId)

                //}).ToList();

                //foreach (var question in Questions)
                //{
                //    var a = new QuestionAndAnswers();
                //    a.QuestionName = question.QText;
                //    a.Answers = new List<string>();
                //    var max = entityDB.Questions.Where(r => r.QuestionText == a.QuestionName).Select(r => r.QuestionID);
                //    var MaxValue = entityDB.VendorDetailQuestions.FirstOrDefault(r => max.Contains(r.QuestionId));
                //    a.Value = MaxValue.Value;
                //    a.IsMax = MaxValue.IsMax;
                //    var QCols = entityDB.PrequalificationUserInput.Where(r => r.PreQualificationId == pqid && question.QCids.Contains(r.QuestionColumnId)).Take(3).Select(r => new
                //    {
                //        r.UserInput,
                //        r.QuestionColumnDetails.ColumnValues,
                //        r.QuestionColumnDetails.TableReference,
                //        r.QuestionColumnDetails.TableReferenceField,
                //        r.QuestionColumnDetails.TableRefFieldValueDisplay,
                //        r.QuestionColumnDetails.QuestionControlTypeId
                //    }).ToList();
                //    foreach (var Qcol in QCols)
                //    {
                //        if (string.IsNullOrEmpty(Qcol.TableReference))
                //        {
                //            if (Qcol.QuestionControlTypeId == 8)
                //            {
                //                if (Qcol.UserInput.Equals("true", StringComparison.CurrentCultureIgnoreCase))
                //                    a.Answers.Add(Qcol.ColumnValues);
                //            }
                //            else
                //                a.Answers.Add(Qcol.UserInput);
                //        }
                //        else
                //        {
                //            var helper = new PrequalificationHelper();
                //            a.Answers.Add(helper.GetUserInputAnswer(Qcol.TableReference, Qcol.TableReferenceField, Qcol.TableRefFieldValueDisplay, Qcol.UserInput));
                //        }
                //    }

                //    qAndA.Add(a);
                return PartialView(qAndA);
            }

        }


        //==========================SUMA On 6-25-2013 for vendorDetails============>>>>
        [SessionExpire]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "PREQUALIFICATIONREQUIREMENTS", sideMenuName = "", ViewName = "VendorDetails")]
        //[HeaderAndSidebar(headerName = "Vendors", sideMenuName = "VendorSearch", ViewName = "VendorDetails")]
        public ActionResult VendorDetails(long SelectedVendorId, long PreQualificationId, long TemplateSectionID, int? mode)
        {
            //using (var entityDB = new EFDbContext())
            //{
            var pqDetails = _prequalification.GetPQDetails(PreQualificationId);
            ViewBag.userreview = _prequalification.GetVendorReviewAverageSpecificToClient(SelectedVendorId, pqDetails.ClientId);
            Guid userId = SSession.UserId;
            try
            {
                ViewBag.VendorAllContacts = pqDetails.vendorContacts;
                ViewBag.SubSectionName = pqDetails.subSectionNames;
            }
            catch { }
            ViewBag.IsEvaluationExpired = _prequalification.CheckEvalationExpiration(userId, SelectedVendorId, pqDetails.ClientId);
            ViewBag.VBVendorDetails = pqDetails.vendorOrgDetails;//.ToList();
            var templateid = pqDetails.TemplateID;
            ViewBag.ISSuperClient = pqDetails.ClientOrganizationType;
            var RestrictedCommentsSec = _sysUser.GetPQcomments(PreQualificationId);
            //ViewBag.HasFlag = pqDetails.HasFlag;
            ViewBag.preComments = RestrictedCommentsSec?.Comments;
            ViewBag.PqCommentsHasFlag = RestrictedCommentsSec?.IsChecked;
            var Templates = _prequalification.GetPQTemplate(PreQualificationId);
            ViewBag.HasCommentsSecPermisstion = Templates.HideCommentsSec;
            ViewBag.ClientVendorRefid = pqDetails.ClientVendorRefId;
            ViewBag.HaspredefineCommentsEditPermission = _sysUser.IsUserHasPermission(SSession.UserId, 1012);
            try
            {
                ViewBag.VBContactList = _organization.GetSuperuser(SelectedVendorId);
            }
            catch (Exception e) { }

            ViewBag.VBPrequalSubmitDetails = pqDetails.pqSubmittedDetails;
            try
            {
                if (pqDetails.pqUserIdLoggedInDetails != null && pqDetails.pqSubmittedDetails != null && pqDetails.pqSubmittedDetails.Count == 0)
                {
                    ViewBag.VBPrequalSubmissionDetailsFromPrequalification = pqDetails.pqUserIdLoggedInDetails;
                }
            }
            catch (Exception ee) { }
            ViewBag.ShowScorecard = pqDetails.ShowScorecard;
            ViewBag.recommended = pqDetails.RecommendedCount;
            ViewBag.notrecommended = pqDetails.NotRecommendedCount;
            ViewBag.Showlocations = Templates.HasVendorDetailLocation;
            ViewBag.PrequalificationId = PreQualificationId;
            long originalTemplateSectionId = TemplateSectionID;
            var templateId = pqDetails.TemplateID;
            var clientID = pqDetails.ClientId;

            try
            {
                if (TemplateSectionID == -1)//If TemplateSectionID=-1 then 1st one is selected one
                    TemplateSectionID = pqDetails.DefaultTemplateSectionId;
            }
            catch (Exception ee)
            {
            }

            if (SSession.Role.ToString().Equals(LocalConstants.Vendor))//If vendor login
            {
                if (pqDetails.PaymentReceived == null || pqDetails.PaymentReceived == false)//means vendor not payment
                {
                    var templateSecType = _prequalification.GetTemplateSectionType(TemplateSectionID);
                    if (templateSecType == 0 || templateSecType == 3)//if user try to open isprofile or payment
                    {
                        //Do nothing
                    }
                    else if (pqDetails.TemplateHasPaymentSection)//Means it has payment section
                    {
                        TemplateSectionID = pqDetails.PaymentSectionId;
                        ViewBag.PaymentError = true;
                    }
                }
                if (pqDetails.PrequalificationStatusId == 5)//means he submited prequalification
                {
                    mode = 1;//He can view sections
                }
            }
            else if (SSession.Role.Equals(LocalConstants.Client) || SSession.Role.Equals("Super Client"))
            {
                mode = 1;//He can view sections
            }

            //To Get list of PreQualification with respet to present prequalification client and vendor
            //Rajesh On 8-31-2013 >>>
            ViewBag.mode = mode;
            ViewBag.templateId = templateId;
            //Ends <<<

            ViewBag.TemplateCode = pqDetails.TemplateCode;
            ViewBag.VendorId = SelectedVendorId;
            ViewBag.ClientId = pqDetails.ClientId;
            ViewBag.ClientName = pqDetails.ClientName;
            ViewBag.VendorName = pqDetails.VendorName;
            ViewBag.Period = pqDetails.PrequalificationStart.ToString("MM/dd/yy") + "-" + pqDetails.PrequalificationFinish.ToString("MM/dd/yy");
            ViewBag.LogoPath = "";
            ViewBag.isEdit = pqDetails.PrequalificationStatusId == 10;
            ViewBag.LogoPath = ConfigurationManager.AppSettings["LogoUploadPath"] + "/";//Rajesh on 08/09/2013

            ViewBag.PrequalStartDate = pqDetails.PrequalificationCreate.ToString("MM/dd/yyyy"); // Kiran on 3/28/2014
            ViewBag.eritemplate = pqDetails.IsERI;
            ViewBag.StatusId = pqDetails.PrequalificationStatusId;
            if (@ViewBag.StatusId.ToString() == "10")
            {
                StatusBannerColor previousStatusBannerColor = _prequalification.GetPQPreviousStatusAndBannerColor(PreQualificationId, pqDetails.PrequalificationStatusId);
                ViewBag.VBPreviousStatusColour = previousStatusBannerColor.BannerColor;
                ViewBag.VBPreviousStatusName = previousStatusBannerColor.StatusName;
            }

            ViewBag.StatusName = GetPQStatusForBanner(PreQualificationId, pqDetails.PrequalificationStatusName);
            string statusName = ViewBag.StatusName;
            bool siteStatus = statusName == "Multiple";
            ViewBag.VBIsSiteHasMultiStatus = siteStatus;
            ViewBag.VBStatusColor = _prequalification.GetPQStatusBannerColor(pqDetails.PrequalificationStatusId, SSession.Role, mode, statusName, "");

            var invitationDetails = _prequalification.GetPQInviteDetails(PreQualificationId);

            ViewBag.InvitationSentBy = GetInvitationSentBy(invitationDetails);
            ViewBag.InvitationSentByUser = invitationDetails.InvitationSentByUser;

            var invitationFromOptions = _prequalification.GetInvitationFromOptions(pqDetails.ClientId);
            List<SelectListItem> selectLists = new List<SelectListItem>();
            invitationFromOptions.ForEach(s => selectLists.Add(new SelectListItem() { Text = s.Key, Value = s.Value }));

            ViewBag.InvitationFromOptions = selectLists;

            List<SelectListItem> dropDown_Prequalification = GetVendorPQsSpecificToClient(pqDetails.ClientId, SelectedVendorId, PreQualificationId);
            ViewBag.VBPreQualifications = dropDown_Prequalification;



            List<LocalTemplatesSideMenuModel> sideMenus = GetSideMenus(PreQualificationId, ref TemplateSectionID, mode, userId, templateId);
            ViewBag.sideMenu = sideMenus;
            ViewBag.TemplateSectionID = TemplateSectionID;

            return View();
        }

        public List<LocalTemplatesSideMenuModel> GetSideMenus(long PreQualificationId, ref long TemplateSectionID, int? mode, Guid userId, Guid templateId)
        {
            return _prequalification.GetPQSideMenusForVendorDetails(PreQualificationId, ref TemplateSectionID, mode, userId, templateId, SSession.Role);
        }

        private List<SelectListItem> GetVendorPQsSpecificToClient(long clientId, long vendorId, long pqId)
        {
            List<VendorPQsSpecificToClient> vendorPqsSpecificToClient = _prequalification.GetVendorPqsSpecificToClient(clientId, vendorId);

            var dropDown_Prequalification = new List<SelectListItem>();
            SelectListItem latestPreQualification = new SelectListItem();
            latestPreQualification.Text = vendorPqsSpecificToClient.FirstOrDefault().ClientName + " " + vendorPqsSpecificToClient.FirstOrDefault().PrequalificationStart.ToString("MM/dd/yyyy");
            latestPreQualification.Value = vendorPqsSpecificToClient.FirstOrDefault().PrequalificationId + "";
            dropDown_Prequalification.Add(latestPreQualification);


            if (!SSession.Role.ToString().Equals(LocalConstants.Vendor))
            {
                dropDown_Prequalification = (from preQualifications in vendorPqsSpecificToClient
                                             select new SelectListItem
                                             {
                                                 Text = preQualifications.ClientName + " " + preQualifications.PrequalificationStart.ToString("MM/dd/yyyy"),
                                                 Value = preQualifications.PrequalificationId + "",
                                                 Selected = (pqId == preQualifications.PrequalificationId)
                                             }).ToList();
            }

            if (SSession.Role.ToString().Equals(LocalConstants.Client) || SSession.Role.ToString().Equals(LocalConstants.SuperClient)) // Kiran on 4/30/2014            
            {
                List<VendorPQsSpecificToClient> clientAccessiblePqs = _clientBusiness.GetClientUserAccessiblePqs(clientId, vendorId, SSession.UserId);
                if (clientAccessiblePqs != null && clientAccessiblePqs.Count() != 0)
                {
                    dropDown_Prequalification = (from preQualifications in clientAccessiblePqs
                                                 select new SelectListItem
                                                 {
                                                     Text = preQualifications.ClientName + " " + preQualifications.PrequalificationStart.ToString("MM/dd/yyyy"),
                                                     Value = preQualifications.PrequalificationId + "",
                                                     Selected = (pqId == preQualifications.PrequalificationId)
                                                 }).ToList();
                }

            }
            if (SSession.Role.ToString().Equals(LocalConstants.Admin)) // Kiran on 4/30/2014            
            {
                if (vendorPqsSpecificToClient != null && (vendorPqsSpecificToClient.FirstOrDefault().PrequalificationFinish.Date.Ticks >= DateTime.Now.Date.Ticks && vendorPqsSpecificToClient.FirstOrDefault().PrequalificationFinish.AddDays(-30).Date.Ticks <= DateTime.Now.Date.Ticks) && (vendorPqsSpecificToClient.FirstOrDefault().PrequalificationStatusId == 4 || vendorPqsSpecificToClient.FirstOrDefault().PrequalificationStatusId == 8 || vendorPqsSpecificToClient.FirstOrDefault().PrequalificationStatusId == 9 || vendorPqsSpecificToClient.FirstOrDefault().PrequalificationStatusId == 13 || vendorPqsSpecificToClient.FirstOrDefault().PrequalificationStatusId == 23 || vendorPqsSpecificToClient.FirstOrDefault().PrequalificationStatusId == 24 || vendorPqsSpecificToClient.FirstOrDefault().PrequalificationStatusId == 25 || vendorPqsSpecificToClient.FirstOrDefault().PrequalificationStatusId == 26))
                {
                    SelectListItem pre = new SelectListItem();
                    pre.Text = "Renewal";
                    pre.Value = "-1";
                    dropDown_Prequalification.Add(pre);
                }
            }

            return dropDown_Prequalification;
        }

        private string GetInvitationSentBy(LocalVendorInviteDetails invitationDetails)
        {
            if (invitationDetails.InvitationSentBy != null)
            {
                if (invitationDetails.InvitationSentBy.Trim().Equals(Resources.Resources.Common_label_None))
                    invitationDetails.InvitationSentBy = Resources.Resources.PQBanner_Label_WithoutInvitation;
                else if (invitationDetails.InvitationSentBy.Trim().Equals(Resources.Resources.Common_label_FirstVerify))
                    invitationDetails.InvitationSentBy = Resources.Resources.PQBanner_Label_InvitationFromFirstVerify;
                else if (invitationDetails.InvitationSentBy.Trim().Equals(Resources.Resources.Common_label_ClientUser))
                    invitationDetails.InvitationSentBy = Resources.Resources.PQBanner_Label_InvitationFrom;
            }
            return invitationDetails.InvitationSentBy;
        }

        // Ends <<<<
        public string GetPQStatusForBanner(long pqId, string defaultStatus)
        {
            var clientId = 0L;
            if (SSession.Role.Equals(OrganizationType.Client))
                clientId = SSession.OrganizationId;
            //  if (SSession.Role.Equals(OrganizationType.SuperClient)&& SSession.OrganizationId!= SubClientId) clientId = SubClientId;
            return _prequalification.GetPQStatusForBanner(pqId, defaultStatus, clientId, SSession.UserId).Value;

        }
        public ActionResult GetPQVendorDetails(long pqId)
        {
            var helper = new PrequalificationHelper();
            return Json(helper.PQVendorDetails(pqId));

        }

        //private string GetAnswer(long VendorReviewInputId, long Section3QuestionId)
        //{
        //    using (var entityDB = new EFDbContext())
        //    {
        //        var x = entityDB.VendorReviewInputSection3Ans.FirstOrDefault(rec1 => rec1.Section3QuestionId == Section3QuestionId && rec1.VendorReviewInputId == VendorReviewInputId);
        //        if (x != null)
        //            return x.Answer;
        //    }
        //    return null;
        //}

        [HttpGet]
        [SessionExpire]
        [HeaderAndSidebar(headerName = "PREQUALIFICATIONREQUIREMENTS", sideMenuName = "", ViewName = "ScoreCard")]
        public ActionResult ScoreCard(long PreQualificationId, long SelectedVendorId, long? buid, long? VendorReviewInputId, int viewSource = 0)
        {
            var entityDB = new EFDbContext();
            {
                ViewBag.ViewSource = viewSource;

                var organizationId = Convert.ToInt64(Session["currentOrgId"]);

                var loggedinUserGuid = (Guid)Session["UserId"];
                ViewBag.IsEdit = false;
                LocalScoreCard scoreCard = _clientBusiness.GetVendorReviewInput(PreQualificationId, SelectedVendorId,
                    buid, VendorReviewInputId, SSession.OrganizationId, SSession.UserId);
                var userEmails = from clientUsers in scoreCard.ClientUsers.ToList()
                                 select new SelectListItem
                                 {
                                     Text = clientUsers.Key,
                                     Value = clientUsers.Value.ToString(),
                                     Selected = clientUsers.Value.ToString() == loggedinUserGuid.ToString(),
                                 };

                List<SelectListItem> totalUsers = new List<SelectListItem>();
                totalUsers.AddRange(userEmails);
                if (VendorReviewInputId != null)
                {
                    ViewBag.IsEdit = true;
                    totalUsers.ForEach(rec => rec.Selected = false);
                    totalUsers.FirstOrDefault(rec => rec.Value.ToString() == scoreCard.EvaluatedBy.ToString()).Selected = true;
                }
                ViewBag.EvaluatedBy = totalUsers.OrderBy(rec => rec.Text).ToList();

                var preQualificationDetails = entityDB.Prequalification.FirstOrDefault(m => m.PrequalificationId == PreQualificationId);
                // For Side Bar
                if (preQualificationDetails != null)

                {
                    ViewBag.PqStatusId = preQualificationDetails.PrequalificationStatusId;

                    ViewBag.VendorName = preQualificationDetails.Vendor.Name;
                    ViewBag.ClientName = preQualificationDetails.Client.Name;
                    ViewBag.PrequalificationCreate = preQualificationDetails.PrequalificationCreate;
                    ViewBag.PqStatusName = preQualificationDetails.PrequalificationStatus.PrequalificationStatusName;
                    if (preQualificationDetails.PrequalificationStatusId.ToString() == "10")
                    {
                        StatusBannerColor previousStatusBannerColor =
                            _prequalification.GetPQPreviousStatusAndBannerColor(PreQualificationId,
                                preQualificationDetails.PrequalificationStatusId);
                        ViewBag.VBPreviousStatusColour = previousStatusBannerColor.BannerColor;
                        ViewBag.VBPreviousStatusName = previousStatusBannerColor.StatusName;
                    }

                    StatusBannerColor buStatusBannerColor =
                        _prequalification.GetBuStatusBannerColor(PreQualificationId, buid, SSession.Role);
                    if (buStatusBannerColor != null)
                    {
                        ViewBag.VBStatusColor = buStatusBannerColor.BannerColor;
                        ViewBag.BUstatus = buStatusBannerColor.StatusName;
                    }

                    ViewBag.Period = preQualificationDetails.PrequalificationStart.ToString("MM/dd/yy") + "-" +
                                     preQualificationDetails.PrequalificationFinish.ToString("MM/dd/yy");
                }

                return View(scoreCard);
            }
        }

        [HttpPost]
        [HeaderAndSidebar(headerName = "PREQUALIFICATIONREQUIREMENTS", sideMenuName = "", ViewName = "ScoreCard")]
        public ActionResult ScoreCard(LocalScoreCard form)
        {
            var loggedinUserGuid = (Guid)Session["UserId"];
            var organizationId = Convert.ToInt64(Session["currentOrgId"]);
            _clientBusiness.PostVendorReviewInput(form, SSession.OrganizationId, SSession.UserId);

            return RedirectToAction("VendorDetails", new RouteValueDictionary(
                        new { controller = "AdminVendors", action = "VendorDetails", SelectedVendorId = form.SelectedVendorId, PreQualificationId = form.PreQualificationId, buId = form.buid, TemplateSectionID = -1, mode = 0 }));

        }
        [HttpGet]
        [HeaderAndSidebar(headerName = "PREQUALIFICATIONREQUIREMENTS", sideMenuName = "", ViewName = "ScoreCard")]
        public ActionResult ReviewsList(long PreQualificationId, long? SelectedVendorId, long? buid)
        {
            var entityDB = new EFDbContext();
            {
                var organizationId = Convert.ToInt64(Session["currentOrgId"]);
                ContractorEvaluations vendorReviews = new ContractorEvaluations();

                vendorReviews.VendorReviews = _clientBusiness.GetContractorEvaluations(PreQualificationId, SSession.OrganizationId, SSession.Role);
                vendorReviews.PqId = PreQualificationId;
                vendorReviews.VendorId = SelectedVendorId;
                vendorReviews.BuId = buid;
                var UserID = (Guid)Session["UserId"];
                // For Side Bar
                var preQualificationDetails = entityDB.Prequalification.FirstOrDefault(m => m.PrequalificationId == PreQualificationId);
                ViewBag.IsEvaluationExpired = _prequalification.CheckEvalationExpiration(UserID, SelectedVendorId.Value, preQualificationDetails.ClientId);

                if (preQualificationDetails != null)
                {
                    ViewBag.PqStatusId = preQualificationDetails.PrequalificationStatusId;
                    ViewBag.VendorName = preQualificationDetails.Vendor.Name;
                    ViewBag.ClientName = preQualificationDetails.Client.Name;
                    ViewBag.PrequalificationCreate = preQualificationDetails.PrequalificationCreate;
                    ViewBag.PqStatusName = preQualificationDetails.PrequalificationStatus.PrequalificationStatusName;
                    if (preQualificationDetails.PrequalificationStatusId.ToString() == "10")
                    {
                        StatusBannerColor previousStatusBannerColor =
                            _prequalification.GetPQPreviousStatusAndBannerColor(PreQualificationId,
                                preQualificationDetails.PrequalificationStatusId);
                        ViewBag.VBPreviousStatusColour = previousStatusBannerColor.BannerColor;
                        ViewBag.VBPreviousStatusName = previousStatusBannerColor.StatusName;
                    }

                    StatusBannerColor buStatusBannerColor =
                        _prequalification.GetBuStatusBannerColor(PreQualificationId, buid, SSession.Role);
                    if (buStatusBannerColor != null)
                    {
                        ViewBag.VBStatusColor = buStatusBannerColor.BannerColor;
                        ViewBag.BUstatus = buStatusBannerColor.StatusName;
                    }

                    ViewBag.Period = preQualificationDetails.PrequalificationStart.ToString("MM/dd/yy") + "-" +
                                     preQualificationDetails.PrequalificationFinish.ToString("MM/dd/yy");
                }

                return View(vendorReviews);
            }
        }

        public string DeleteReview(long ReviewID)
        {
            var entityDB = new EFDbContext();
            {
                try
                {
                    var reviewdata = entityDB.VendorReviewInput.Find(ReviewID);
                    foreach (var sectiondata in reviewdata.VendorReviewInputSection3Ans.ToList())
                    {
                        entityDB.VendorReviewInputSection3Ans.Remove(sectiondata);
                    }
                    foreach (var reviewdataanswers in reviewdata.VendorReviewInputAnswers.ToList())
                    {
                        entityDB.VendorReviewInputAnswers.Remove(reviewdataanswers);
                    }
                    entityDB.VendorReviewInput.Remove(reviewdata);
                    entityDB.SaveChanges();
                }
                catch (Exception ee)
                {
                    return null;
                }
                return "DeleteReviewSuccess";
            }
        }


        public ActionResult StatusHistory(long PrequalificationId)
        {
            //var statusHistory = entityDB.PrequalificationStatusChangeLog.Where(rec => rec.PrequalificationId == PrequalificationId).OrderByDescending(rec => rec.StatusChangeDate).ToList();
            ViewBag.VBStatusHistory = _prequalification.StatusHistory(PrequalificationId);
            return View();
        }

        /* Redeveloped the entire functionality by Kiran on 9/3/2014 for easy maintenance */
        [SessionExpireForView]
        public ActionResult VendorCommentsPartialView(long prequalificationID)
        {
            var entityDB = new EFDbContext();
            {
                //var clientId = entityDB.Prequalification.FirstOrDefault(m => m.PrequalificationId == prequalificationID).ClientId;

                //var varOrgNotificationSent = entityDB.OrganizationsNotificationsSent.Where(m => (m.SetupType == 1 || m.SetupType == 2 || m.SetupType == 5 || m.SetupType == 6) && m.ClientId == clientId).Select(m => m.NotificationId).ToList();
                var varOrgNotificationSent = _prequalification.GetClientNotificationTypes(prequalificationID);
                ViewBag.VBNotifications = varOrgNotificationSent;
                //ViewBag.VBprequalificationID = prequalificationID;
                // sumanth on  10/19/2015 start for FVOS-95
                //IList<FVGen3.Domain.Entities.OrganizationsNotificationsEmailLogs> notifications = new List<FVGen3.Domain.Entities.OrganizationsNotificationsEmailLogs>();
                //var notifications =

                //    entityDB.OrganizationsNotificationsEmailLogs.Where(
                //            rec =>
                //                rec.PrequalificationId == prequalificationID &&
                //                varOrgNotificationSent.Contains(rec.NotificationId)).ToList()

                //        .Select(
                //            r =>
                //                new PrequalificationCommentsLocal()
                //                {
                //                    EmailSentDateTime = r.EmailSentDateTime,
                //                    Name =
                //                        r.SystemUsers.Contacts.Any()
                //                            ? r.SystemUsers.Contacts.FirstOrDefault().FirstName + ' ' +
                //                              r.SystemUsers.Contacts.FirstOrDefault().LastName
                //                            : r.SystemUsers.Email,
                //                    Comments = r.Comments,
                //                    SetupType = r.OrganizationsNotificationsSent.SetupType,
                //                    NotificationType = r.NotificationType,
                //                    PrequalificationId = r.PrequalificationId,
                //                    EmailRecordId = r.EmailRecordId,
                //                    UserEmail = r.SystemUsers.Email,
                //                    UserContact = r.SystemUsers.Contacts.Any()
                //                            ? r.SystemUsers.Contacts.FirstOrDefault().PhoneNumber
                //                            : String.Empty
                //                }).ToList();


                //var expiredInvitaions =
                //    entityDB.VendorInviteDetails.Where(r => r.PQId == prequalificationID)
                //        .ToList()
                //        .Select(r => new PrequalificationCommentsLocal()
                //        {
                //            EmailSentDateTime = r.InvitationDate,
                //            Name = r.SystemUsers.Contacts.Any()
                //                ? r.SystemUsers.Contacts.FirstOrDefault().LastName + ' ' +
                //                  r.SystemUsers.Contacts.FirstOrDefault().FirstName
                //                : r.SystemUsers.Email,
                //            Comments = "Invitation sent to renew prequalification.",
                //            SetupType = -1,
                //            NotificationType = -1,
                //            PrequalificationId = r.PQId,
                //            EmailRecordId = -1,
                //            UserEmail = r.SystemUsers.Email,
                //            UserContact = r.SystemUsers.Contacts.Any()
                //                    ? r.SystemUsers.Contacts.FirstOrDefault().PhoneNumber
                //                    : String.Empty
                //        }).GroupBy(r => new { r.PrequalificationId, r.EmailSentDateTime.Date }).Select(r => r.FirstOrDefault()).ToList();

                //var AgreementLogs =
                //    entityDB.AgreementSubSectionLog.Where(r => r.PQId == prequalificationID)
                //        .ToList()
                //        .Select(r => new PrequalificationCommentsLocal()
                //        {
                //            EmailSentDateTime = r.SentDate,
                //            Name = r.SystemUsers.Contacts.Any()
                //                ? r.SystemUsers.Contacts.FirstOrDefault().LastName + ' ' +
                //                  r.SystemUsers.Contacts.FirstOrDefault().FirstName
                //                : r.SystemUsers.Email,
                //            Comments = r.AgreementSubSectionConfiguration.EmailTemplates.CommentText,
                //            SetupType = -1,
                //            NotificationType = -1,
                //            PrequalificationId = r.PQId,
                //            EmailRecordId = -1,
                //            UserEmail = r.SystemUsers.Email,
                //            UserContact = r.SystemUsers.Contacts.Any()
                //                    ? r.SystemUsers.Contacts.FirstOrDefault().PhoneNumber
                //                    : String.Empty
                //        }).ToList();
                //var VendorEmailLog =
                //    entityDB.VendorEmailLog.Where(r => r.PQId == prequalificationID && r.EmailStatus)
                //        .ToList()
                //        .Select(r => new PrequalificationCommentsLocal()
                //        {
                //            EmailSentDateTime = r.SendDate,
                //            Name = r.SystemUsers.Contacts.Any()
                //                ? r.SystemUsers.Contacts.FirstOrDefault().LastName + ' ' +
                //                  r.SystemUsers.Contacts.FirstOrDefault().FirstName
                //                : r.SystemUsers.Email,
                //            Comments = "Email message sent by client " + r.Prequalification.Client.Name,
                //            SetupType = -1,
                //            NotificationType = -1,
                //            PrequalificationId = r.PQId,
                //            EmailRecordId = -1,
                //            UserEmail = r.SystemUsers.Email,
                //            UserContact = r.SystemUsers.Contacts.Any()
                //                ? r.SystemUsers.Contacts.FirstOrDefault().PhoneNumber
                //                : String.Empty
                //        }).ToList();
                IEnumerable<PrequalificationCommentsViewModel> pqComments = _prequalification.GetPQComments(prequalificationID);
                //ViewBag.VBNotifications = notifications.Union(VendorEmailLog).Union(expiredInvitaions).Union(AgreementLogs).OrderByDescending(r => r.EmailSentDateTime);
                ViewBag.VBNotifications = pqComments;
                // Ends <<<
                return PartialView("VendorCommentsPartialView");
            }
        }

        /* Redeveloped the entire functionality by Kiran on 9/3/2014 for easy maintenance */
        [SessionExpireForView(pageType = "popupview")]
        [HttpGet]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "PREQUALIFICATIONREQUIREMENTS", sideMenuName = "", ViewName = "VendorAddComments")]
        public ActionResult VendorAddComments(long prequalificationId, long EmailRecordId)
        {
            using (var entityDB = new EFDbContext())
            {
                LocalComments varComments = new LocalComments();
                varComments.prequalificationId = prequalificationId;
                varComments.EmailRecordId = EmailRecordId;
                if (EmailRecordId != -1)
                {
                    //varComments.comment = entityDB.OrganizationsNotificationsEmailLogs.Find(EmailRecordId).Comments;
                    varComments.comment = _prequalification.GetNotificationComments(EmailRecordId);
                }
                return View(varComments);
            }
        }

        /* Redeveloped the entire functionality by Kiran on 9/3/2014 for easy maintenance */
        [SessionExpireForView(pageType = "popupview")]
        [HttpPost]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "PREQUALIFICATIONREQUIREMENTS", sideMenuName = "", ViewName = "VendorAddComments")]
        public ActionResult VendorAddComments(LocalComments local)
        {
            var entityDB = new EFDbContext();
            {
                Guid guidRecordId = (Guid)Session["UserId"];
                //var PrequalificationDetails = entityDB.Prequalification.FirstOrDefault(m => m.PrequalificationId == local.prequalificationId);

                //var MaxNotificationNo = 0;
                //try
                //{
                //    MaxNotificationNo = entityDB.OrganizationsNotificationsEmailLogs.Where(record => record.PrequalificationId == PrequalificationDetails.PrequalificationId && record.NotificationType == 1).Max(record => record.NotificationNo);
                //}
                //catch (Exception e) { }

                if (ModelState.IsValid)
                {
                    _prequalification.AddorUpdateComment(local, guidRecordId);
                    //long NotificationId = 0;
                    //var commentNotificationSentRecord = entityDB.OrganizationsNotificationsSent.FirstOrDefault(record => record.SetupType == 2 && record.ClientId == PrequalificationDetails.ClientId);
                    //if (commentNotificationSentRecord == null)
                    //{
                    //    OrganizationsNotificationsSent addNotificationForComment = new OrganizationsNotificationsSent();
                    //    addNotificationForComment.ClientId = PrequalificationDetails.ClientId;
                    //    addNotificationForComment.SetupType = 2;
                    //    entityDB.Entry(addNotificationForComment).State = EntityState.Added;
                    //    entityDB.SaveChanges();
                    //    NotificationId = entityDB.OrganizationsNotificationsSent.FirstOrDefault(record => record.SetupType == 2 && record.ClientId == PrequalificationDetails.ClientId).NotificationId;
                    //}
                    //else
                    //{
                    //    NotificationId = commentNotificationSentRecord.NotificationId;
                    //}
                    //if (local.EmailRecordId == -1)
                    //{
                    //    OrganizationsNotificationsEmailLogs addComment = new OrganizationsNotificationsEmailLogs();

                    //    addComment.NotificationId = NotificationId;
                    //    addComment.NotificationType = 1;
                    //    addComment.NotificationNo = MaxNotificationNo + 1;
                    //    addComment.VendorId = PrequalificationDetails.VendorId;
                    //    addComment.PrequalificationId = PrequalificationDetails.PrequalificationId;
                    //    addComment.EmailSentDateTime = DateTime.Now;
                    //    addComment.ExpiringDate = PrequalificationDetails.PrequalificationFinish;
                    //    addComment.Comments = local.comment;
                    //    addComment.MailSentBy = guidRecordId;
                    //    entityDB.Entry(addComment).State = EntityState.Added;
                    //    entityDB.SaveChanges();
                    //    return View("CloseVendorAddComments");
                    //}
                    //else
                    //{
                    //    var currentCommentRecord = entityDB.OrganizationsNotificationsEmailLogs.Find(local.EmailRecordId);
                    //    currentCommentRecord.Comments = local.comment;


                    //    currentCommentRecord.CommentsUpdateDateTime = DateTime.Now;
                    //    // Ends<<<
                    //    entityDB.Entry(currentCommentRecord).State = EntityState.Modified;
                    //    entityDB.SaveChanges();
                    //    return View("CloseVendorAddComments");
                    //}
                    return View("CloseVendorAddComments");
                }
                return View(local);
            }
        }

        public ActionResult CloseVendorAddComments()
        {
            //ViewBag.selectedVendorId = Session["VendorId"];
            return View();
        }
        //>>>>Ends


        public string deleteComment(long emailRecordId)
        {
            using (var entityDB = new EFDbContext())
            {
                _prequalification.DeleteComment(emailRecordId);
                return "commentDeleted";
            }
        }

        [HttpGet]
        [SessionExpire]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "Reports", sideMenuName = "", ViewName = "CustomReports")]
        public ActionResult CustomReports()
        {
            var entityDB = new EFDbContext();
            {
                var Countries = _organization.GetCountries().Select(r => new SelectListItem()
                {
                    Text = r.Value,
                    Value = r.Value
                }).ToList();

                Countries.Insert(0, (new SelectListItem
                {
                    Text = "Any",
                    Value = ""
                }));
                ViewBag.Countries = Countries;
                var Regions = entityDB.Regions.Select(r => new SelectListItem() { Text = r.RegionName, Value = r.RegionName }).ToList();
                Regions.Insert(0, (new SelectListItem
                {
                    Text = "Any",
                    Value = ""
                }));
                ViewBag.Regions = Regions;
                LocalSearchVendors searchVendors = new LocalSearchVendors();
                searchVendors.StatusList = new List<bool>();
                searchVendors.BiddingList = new List<bool>();



                var RoleName = SSession.Role;
                var varorgId = SSession.OrganizationId;
                if (RoleName.Equals("Client") || RoleName.Equals(OrganizationType.SuperClient))
                {
                    var varOrgList = from org in entityDB.Organizations.Where(records => records.OrganizationID == varorgId).ToList()
                                     select new SelectListItem
                                     {
                                         Text = org.Name,
                                         Value = org.OrganizationID.ToString()
                                     };
                    // var orgs = varOrgList.ToList();
                    ViewBag.VBSubClients = varOrgList;
                    ViewBag.VBSubClients_Default = varOrgList;

                    ViewBag.totalOrglist = varOrgList;
                    // ViewBag.VBOrganizationsList = ViewBag.VBOrglist;
                    ViewBag.VBLoginUserOrg = varOrgList;
                    if (RoleName.Equals(OrganizationType.SuperClient))
                    {
                        ViewBag.VBLoginUserOrg = varOrgList;
                        varorgId = -1;
                        varOrgList = _clientBusiness.GetSubClients(varorgId, true).Select(r => new SelectListItem() { Value = r.Key, Text = r.Value });

                        searchVendors.Name = varorgId;
                        var SubClientsList = _clientBusiness.GetSubClients(varorgId).Select(r => new SelectListItem() { Value = r.Key, Text = r.Value });
                        ViewBag.VBSubClients_Default = SubClientsList;
                        ViewBag.VBSubClients = SubClientsList;


                    }
                    var VBOrglist = varOrgList;
                    ViewBag.VBOrglist = VBOrglist;
                    ViewBag.ShowVendorRatingReport = entityDB.VendorReviewQuestions.Where(rec => rec.ClientID == varorgId).Count() > 0;
                    var States = (from states in entityDB.States.OrderBy(r => r.StateName).ToList()
                                  select new SelectListItem
                                  {
                                      Text = states.StateName,
                                      Value = states.StateName
                                  }).ToList();
                    States.Insert(0, new SelectListItem() { Text = "Any", Value = "" });
                    ViewBag.States = States;
                    var ooLocations = entityDB.ClientBusinessUnitSites.Join(entityDB.ClientBusinessUnits, b => b.ClientBusinessUnitId,
                        s => s.ClientBusinessUnitId, (b, s) => new { b, s }).
                        Where(w => w.s.ClientId == varorgId).ToList()
                            .Select(s => new SelectListItem
                            {
                                Text = s.b.SiteName,
                                Value = s.b.ClientBusinessUnitSiteId.ToString()
                            }).ToList();
                    //ooLocations.Insert(0, new SelectListItem(){Text="Select One", Value="-1"});
                    ooLocations.Insert(0, new SelectListItem() { Text = "All", Value = "-1" });

                    ViewBag.OOlocations = ooLocations;
                }

                else if (RoleName.Equals("Admin"))
                {
                    SelectListItem defaultItem = new SelectListItem();
                    defaultItem.Text = "Any";
                    defaultItem.Value = "";
                    var varOrgList = from org in entityDB.Organizations.Where(records => records.OrganizationType == "Client" && records.ShowInApplication == true).OrderBy(rec => rec.Name).ToList()
                                     select new SelectListItem
                                     {
                                         Text = org.Name,
                                         Value = org.OrganizationID.ToString()
                                     };

                    ViewBag.VBOrglist = varOrgList;

                    // Mani on 6/1/2015 for Score card report
                    var totalOrgList = from org in entityDB.Organizations.Where(records => records.OrganizationType == "Client" && records.ShowInApplication == true).OrderBy(rec => rec.Name).ToList()
                                       select new SelectListItem
                                       {
                                           Text = org.Name,
                                           Value = org.OrganizationID.ToString()
                                       };

                    ViewBag.totalOrglist = totalOrgList;
                    ViewBag.ShowVendorRatingReport = true;
                    ViewBag.VBLoginUserOrg = totalOrgList;
                    var orgsListWithDefaultItem = totalOrgList.ToList();
                    ViewBag.VBSubClients = varOrgList;
                    orgsListWithDefaultItem.Insert(0, defaultItem);
                    ViewBag.VBSubClients_Default = orgsListWithDefaultItem;

                    var eriTemplates = (from template in entityDB.Templates.Where(row => row.IsERI == true).ToList()
                                        select new SelectListItem
                                        {
                                            Text = template.TemplateName,
                                            Value = template.TemplateID.ToString()
                                        }).ToList();

                    eriTemplates.Insert(0, new SelectListItem() { Text = "All", Value = null });

                    ViewBag.eriTemplates = eriTemplates;
                    var States = (from states in entityDB.States.OrderBy(r => r.StateName).ToList()
                                  select new SelectListItem
                                  {
                                      Text = states.StateName,
                                      Value = states.StateName
                                  }).ToList();
                    States.Insert(0, new SelectListItem() { Text = "Any", Value = "" });
                    ViewBag.States = States;

                }
                var varStatusname = entityDB.PrequalificationStatus.ToList().Where(rec => rec.ShowInDashBoard == true).OrderBy(rec => rec.PrequalificationStatusName); // Kiran on 01/13/2015


                //Status CheckBoxes
                searchVendors.PrequalificationStatusList = varStatusname.ToList();
                foreach (var i in searchVendors.PrequalificationStatusList)
                {
                    searchVendors.StatusList.Add(false);
                }

                //for (int i = 0; i < 13; i++)
                //{
                //    searchVendors.BiddingList.Add(false);
                //}
                var biddingName = entityDB.BiddingInterests.Where(rec => rec.Status == 1).ToList(); // sumanth on 10/31/2015 for FVOS-108 to filter out inactive bidding interests
                searchVendors.BiddingInterestList = biddingName.ToList();

                foreach (var i in searchVendors.BiddingInterestList)
                {
                    searchVendors.BiddingList.Add(false);
                }



                ExpiringDocumentAndCOIReport docReport = new ExpiringDocumentAndCOIReport();
                docReport.DoctypeList = new List<bool>();

                var docType = entityDB.DocumentType.Where(m => m.DocumentExpires);
                if (RoleName.Equals("Client"))
                {
                    docType = from crt in entityDB.ClientTemplateReportingData
                              join ct in entityDB.ClientTemplates on crt.ClientTemplateId equals ct.ClientTemplateID
                              join dt in entityDB.DocumentType on crt.ReportingTypeId equals dt.DocumentTypeId
                              where crt.ReportingType == 2 && ct.ClientID == varorgId && crt.DocumentExpires != null && crt.DocumentExpires == true
                              select dt;

                }
                docReport.DocumentTypeList = docType.Distinct().OrderBy(rec => rec.DocumentTypeName).ToList();
                foreach (var i in docReport.DocumentTypeList)
                {
                    docReport.DoctypeList.Add(false);
                }


                ViewBag.DocReports = docReport;



                return View(searchVendors);
            }
        }



        [SessionExpire]
        public JsonResult ExpiredDocument(string doctypeList)
        {
            List<long> DoctypeList = new List<long>();
            var RoleName = Session["RoleName"];
            var varorgId = (long)Session["currentOrgId"];
            var currentDate = DateTime.Now.ToString("MM/dd/yyyy");
            var reportValues = "";
            string finalStatuses = "8,9,13,24,25,26";

            if (RoleName.Equals("Admin"))
            {
                reportValues = "Select DocType.DocumentTypeName,client.Name AS Client,vendor.Name AS Vendor, " +
                                "pstatus.PrequalificationStatusName AS Status,convert(varchar,orgLogs.EmailSentDateTime,110) AS EmailSentDate, " +
                                "case when orgLogs.NotificationNo = 1 then 'First' when orgLogs.NotificationNo = 2 then 'Second' when orgLogs.NotificationNo = 3 then 'Third' end as NotificationNo," +
                                " convert(varchar,Document.Expires,110) AS Expires  from Document " +
                                " INNER JOIN DocumentType AS DocType ON Document.DocumentTypeId = DocType.DocumentTypeId " +
                                " INNER JOIN Organizations AS org ON Document.OrganizationId = org.OrganizationID " +
                                " INNER JOIN OrganizationsNotificationsEmailLogs AS orgLogs " +
                                " ON orgLogs.DocumentId = Document.DocumentId " +
                                //" INNER JOIN Organizations AS client ON Document.OrganizationId = client.OrganizationID " +
                                " INNER JOIN Organizations AS vendor ON orgLogs.VendorId = vendor.OrganizationID " +
                                " INNER JOIN Prequalification AS p ON orgLogs.PrequalificationId = p.PrequalificationId " +
                                " INNER JOIN PrequalificationStatus AS pstatus  " +
                                " ON p.PrequalificationStatusId = pstatus.PrequalificationStatusId  " +
                                " INNER JOIN OrganizationsNotificationsSent AS s " +
                                " ON s.NotificationId =orgLogs.NotificationId  " +
                                " INNER JOIN Organizations AS client ON s.ClientId = client.OrganizationID " +
                                " where(  " +
                                " Document.ReferenceType = 'Prequalification'  " +
                                " and Document.DocumentTypeId in(" + doctypeList + ") and p.PrequalificationStatusId in (" + finalStatuses + ") and Document.DocumentStatus != 'Deleted' and (orgLogs.RecordStatus != 'Archived' or orgLogs.RecordStatus is null) ) order by orgLogs.EmailSentDateTime desc"; // Kiran on 1/6/2015 for displaying LIFO(Last In First Out) notifications // Kiran on 04/06/2015 to display notifications only for final statuses
            }

            else if (RoleName.Equals("Client"))
            {
                reportValues = "Select DocType.DocumentTypeName,client.Name AS Client,vendor.Name AS Vendor, " +
                                "pstatus.PrequalificationStatusName AS Status,convert(varchar,orgLogs.EmailSentDateTime,110) AS EmailSentDate, " +
                                "case when orgLogs.NotificationNo = 1 then 'First' when orgLogs.NotificationNo = 2 then 'Second' when orgLogs.NotificationNo = 3 then 'Third' end as NotificationNo," +
                                " convert(varchar,Document.Expires,110) AS Expires from Document " +
                                " INNER JOIN DocumentType AS DocType ON Document.DocumentTypeId = DocType.DocumentTypeId " +
                                " INNER JOIN Organizations AS org ON Document.OrganizationId = org.OrganizationID " +
                                " INNER JOIN OrganizationsNotificationsEmailLogs AS orgLogs " +
                                " ON orgLogs.DocumentId = Document.DocumentId " +
                                // " INNER JOIN Organizations AS client ON Document.OrganizationId = client.OrganizationID " +
                                " INNER JOIN Organizations AS vendor ON orgLogs.VendorId = vendor.OrganizationID " +
                                " INNER JOIN Prequalification AS p ON orgLogs.PrequalificationId = p.PrequalificationId " +
                                " INNER JOIN PrequalificationStatus AS pstatus  " +
                                " ON p.PrequalificationStatusId = pstatus.PrequalificationStatusId  " +
                                " INNER JOIN OrganizationsNotificationsSent AS s " +
                                " ON s.NotificationId =orgLogs.NotificationId  " +
                                " INNER JOIN Organizations AS client ON s.ClientId = client.OrganizationID " +
                                " where(  " +
                                " Document.ReferenceType = 'Prequalification'  " +
                                " and s.ClientId =  " + varorgId + "  " +
                                " and Document.DocumentTypeId in(" + doctypeList + ") and p.PrequalificationStatusId in (" + finalStatuses + ") and Document.DocumentStatus != 'Deleted' and (orgLogs.RecordStatus != 'Archived' or orgLogs.RecordStatus is null) ) order by orgLogs.EmailSentDateTime desc"; // Kiran on 1/6/2015 for displaying LIFO(Last In First Out) notifications // Kiran on 04/06/2015 to display notifications only for final statuses
            }


            return Json(reportValues, JsonRequestBehavior.AllowGet);

        }
        //=====Suma on 10/15/2013===Ends==>>>>>

        // Kiran on 2/5/2014
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "EmployeeQuiz", sideMenuName = "EmployeeQuizzes", ViewName = "EmployeesListForSeclectedClientAndVendor")]
        [SessionExpire]
        [HttpGet]
        public ActionResult EmployeesListForSeclectedClientAndVendor()
        {
            ClientsList();
            return View();
        }
        //Rajesh on 12/2/2014
        [HeaderAndSidebar(headerName = "EmployeeQuiz", sideMenuName = "EmployeeQuizzes", ViewName = "EmployeesListForSeclectedClientAndVendor")]
        [SessionExpire]
        [HttpGet]
        public ActionResult EditEmployeeEmails(long VendorOrgId)
        {
            var entityDB = new EFDbContext();
            {
                var employee_RoleId = EmployeeRole.EmployeeRoleId;
                //  var employee_RoleId = entityDB.SystemRoles.FirstOrDefault(record => record.RoleName == "Employee Training").RoleId;
                List<SystemUsersOrganizations> employee_sysUserOrgRoles = null;
                employee_sysUserOrgRoles = entityDB.SystemUsersOrganizationsRoles.Where(record => employee_RoleId.Contains(record.SysRoleId)).Select(rec => rec.SystemUsersOrganizations).Where(rec => rec.OrganizationId == VendorOrgId).Where(rec => rec.SystemUsers.UserStatus != false).ToList();
                ViewBag.VBEmployeeSysUserOrgs = employee_sysUserOrgRoles.OrderBy(rec => rec.SystemUsers.Contacts[0].LastName).ToList(); // Kiran on 01/31/2015 to sort the employees order by name.
                return View("../Employee/EditEmployeeEmails", new { VendorOrgId = VendorOrgId });
            }
        }
        //Ends<<<

        /*
            Created Date:  2/5/2014   Created By: Kiran Talluri
            Purpose: To display List of Clients
        */
        private void ClientsList()
        {
            var entityDB = new EFDbContext();
            {
                var clientsList = from orgs in entityDB.Organizations.Where(orgs => orgs.OrganizationType == "Client" && orgs.ShowInApplication == true).OrderBy(rec => rec.Name).ToList()

                                  select new SelectListItem
                                  {
                                      Text = orgs.Name,
                                      Value = orgs.OrganizationID.ToString()
                                  };
                SelectListItem defaultItem = new SelectListItem();
                defaultItem = new SelectListItem();
                defaultItem.Text = "";
                defaultItem.Value = "-1";
                var clientsList_defaultItem = clientsList.ToList();
                clientsList_defaultItem.Insert(0, defaultItem);
                ViewBag.VBclientsList = clientsList_defaultItem;
            }
        }

        public ActionResult getPrequalifiedVendors(long selectedClientId)
        {
            var entityDB = new EFDbContext();
            {
                var ERIClients = entityDB.Database.SqlQuery<long>("select PQId from PrequalificationClient where ','+ClientIds+',' like @p", new SqlParameter("@p", "%," + selectedClientId + ",%")).ToList();
                var PrequalifiedVendors = from a in
                                              (from prequalifications in entityDB.Prequalification.Where(rec => rec.ClientId == selectedClientId || ERIClients.Contains(rec.PrequalificationId))
                                               select prequalifications.Vendor).Distinct().OrderBy(r => r.Name).ToList()
                                          select new SelectListItem
                                          {
                                              Text = a.Name,
                                              Value = a.OrganizationID.ToString()
                                          }; // Kiran on 4/4/2014
                                             //Ends<<<
                return Json(PrequalifiedVendors);
            }
        }


        [OutputCache(Duration = 0)]
        public ActionResult PartialEmployeesListForAdminView(string firstName, string lastName, int searchType, string email, long? vendorId, long? clientId)
        {
            var entityDB = new EFDbContext();
            {
                ViewBag.VendorId = vendorId;//Rajesh on 12/2/2014
                ViewBag.clientId = clientId;
                ViewBag.searchType = searchType;
                if (searchType == 1)
                {
                    var result = entityDB.SystemUsersOrganizations.Where(
                        rec => rec.SystemUsersOrganizationsRoles.Any(
                            r => EmployeeRole.EmployeeRoleId.Contains(r.SysRoleId)));

                    if (!string.IsNullOrEmpty(firstName))
                        result = result.Where(rec => rec.SystemUsers.Contacts.Any(r => r.FirstName.Contains(firstName)));
                    if (!string.IsNullOrEmpty(lastName))
                        result = result.Where(rec => rec.SystemUsers.Contacts.Any(r => r.LastName.Contains(lastName)));
                    if (!string.IsNullOrEmpty(email))
                        result = result.Where(rec => rec.SystemUsers.Email.Contains(email));

                    ViewBag.VBEmployeeSysUserOrgs = result.ToList();
                }
                else
                    ViewBag.VBEmployeeSysUserOrgs = entityDB.SystemUsersOrganizationsQuizzes.Where(rec => rec.SystemUsersOrganizations.OrganizationId == vendorId).Select(rec => rec.SystemUsersOrganizations).Distinct().OrderBy(rec => rec.SystemUsers.Contacts.FirstOrDefault().LastName).ToList();


                return PartialView();
            }
        }

        [SessionExpire]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "EmployeeQuiz", sideMenuName = "EmployeeQuizzes", ViewName = "EmployeeTrainingsForAdminView")]
        public ActionResult EmployeeTrainingsForAdminView(Guid userId, long? clientId)
        {
            var entityDB = new EFDbContext();
            {
                LocalVendorEmployeeQuizzes employee = new LocalVendorEmployeeQuizzes();
                var employeeContactDetails = entityDB.Contact.FirstOrDefault(record => record.UserId == userId);
                employee.EmployeeName = employeeContactDetails.LastName + ", " + employeeContactDetails.FirstName;
                employee.UserId = userId;
                // Kiran on 1/31/2014
                var currentEmpSystemUsersRecord = entityDB.SystemUsers.Find(userId);
                ViewBag.VBEmpPassword = Utilities.EncryptionDecryption.Decryptdata(currentEmpSystemUsersRecord.Password);
                ViewBag.VBEmpUserName = currentEmpSystemUsersRecord.UserName;
                ViewBag.clientId = clientId;
                //Ends<<<
                return View(employee);
            }
        }

        [HttpGet]
        [SessionExpireForView]
        [OutputCache(Duration = 0)]
        public ActionResult PartialEmployeeQuizListForAdmin(Guid userId, int? type, int? showMessage, long? PQId, long? clientId)//if type= null or 0 Admin/vendor 1-trainee, if showmessage=0, do not display any message else display message
        {
            //var entityDB = new EFDbContext();
            //{
            // kiran on 03/13/2015
            var trainingCategories = empQuizCategoriesRepository.GetTrainingCategories();

            ViewBag.VBLoggedInUserIsVendorHasNoAccess = false;
            if (@Session["RoleName"].ToString().Equals("Vendor"))
            {
                trainingCategories = trainingCategories.Where(rec => rec.AccessType == 2).ToList();
            }
            if (trainingCategories == null || trainingCategories.Count() == 0)
            {
                ViewBag.VBLoggedInUserIsVendorHasNoAccess = true;
            }
            // kiran on 03/13/2015 Ends<<<
            ViewBag.showMessage = showMessage;

            //List<LocalEmployeeGuideDocumentsGroupedByClient> employeeGuidesGroupedByClient = new List<LocalEmployeeGuideDocumentsGroupedByClient>();
            //var userSysUserOrg = entityDB.SystemUsersOrganizations.FirstOrDefault(record => record.UserId == userId); // Kiran on 02/03/2015

            //var currentUserSysUserOrgId = entityDB.SystemUsersOrganizations.FirstOrDefault(record => record.UserId == userId).SysUserOrganizationId;
            //var currentUserSysUserOrgQuiz = entityDB.SystemUsersOrganizationsQuizzes.Where(record => record.SysUserOrgId == currentUserSysUserOrgId && (record.QuizRemoved == false || record.QuizRemoved == null)).ToList(); // Kiran on 11/18/2014
            //long clientID = -1;
            var clients = new List<long>();
            if (PQId != null)
            {
                //var pq = _prequalification.GetPrequalification((long)PQId);
                VendorDetailsViewModel pqDetails = _prequalification.GetPQCommonData((long)PQId);
                clients = new List<long> { pqDetails.ClientId };
                if (pqDetails.ClientOrganizationType == OrganizationType.SuperClient)
                {
                    try { clients = _prequalification.GetPrequalificationClient((long)PQId).ClientIdsList.ToList(); } catch { }
                }
                //currentUserSysUserOrgQuiz = currentUserSysUserOrgQuiz.Where(rec => rec.ClientTemplates != null && clients.Contains(rec.ClientTemplates.ClientID)).ToList();
            }
            var EmpQuizs = _employee.EmployeeQuizs(userId, type, (Guid)Session["UserId"], clients);
            ViewBag.Clients = EmpQuizs.ClientQuizzes.DistinctBy(r => r.ClientID).OrderBy(r => r.ClientName).Select(r => r.ClientName);

            //if (clientId != null)
            //{
            //    currentUserSysUserOrgQuiz = currentUserSysUserOrgQuiz.Where(rec => rec.ClientTemplates != null && rec.ClientTemplates.ClientID == clientId).ToList();
            //}

            //var sysUserOrgQuizGroupByClient = currentUserSysUserOrgQuiz.GroupBy(record => record.ClientTemplates.Organizations.Name);
            //ViewBag.VBsysUserOrgQuizGroupByClient = sysUserOrgQuizGroupByClient.ToList();

            //foreach (var client in sysUserOrgQuizGroupByClient)
            //{
            //    LocalEmployeeGuideDocumentsGroupedByClient employeeGuide = new LocalEmployeeGuideDocumentsGroupedByClient();
            //    employeeGuide.key = client.Key;
            //    employeeGuide.OrganizationId = client.FirstOrDefault().ClientId;
            //    clientID = client.FirstOrDefault().ClientId.Value;
            //    //employeeGuide.fileName = "";
            //    var vendorLatestPrequalification = entityDB.Prequalification.Where(rec => rec.ClientId == employeeGuide.OrganizationId && rec.VendorId == userSysUserOrg.OrganizationId).OrderByDescending(rec => rec.PrequalificationStart).ToList();


            //    if (vendorLatestPrequalification != null && vendorLatestPrequalification.Count() != 0 && vendorLatestPrequalification.FirstOrDefault().PrequalificationStatusId != 14 && vendorLatestPrequalification.FirstOrDefault().PrequalificationFinish > DateTime.Now) // Kiran on 02/05/2015
            //    {
            //        employeeGuide.qualified = true;
            //    }
            //    if (employeeGuide.qualified == null || employeeGuide.qualified == false)
            //        vendorLatestPrequalification = entityDB.Prequalification.Where(rec => rec.Client.OrganizationType == OrganizationType.SuperClient && rec.VendorId == userSysUserOrg.OrganizationId).OrderByDescending(rec => rec.PrequalificationStart).ToList();
            //    if (vendorLatestPrequalification != null && vendorLatestPrequalification.Count() != 0 && vendorLatestPrequalification.FirstOrDefault().PrequalificationStatusId != 14 && vendorLatestPrequalification.FirstOrDefault().PrequalificationFinish > DateTime.Now) // Kiran on 02/05/2015
            //    {
            //        employeeGuide.qualified = true;
            //    }
            //    employeeGuide.prequalifcation = vendorLatestPrequalification.FirstOrDefault();
            //    employeeGuidesGroupedByClient.Add(employeeGuide);
            //}
            //ViewBag.VBEmployeeGuidesGroupedByClient = employeeGuidesGroupedByClient;
            //// Ends<<<

            //// Kiran on 2/5/2014
            //var loggedinUserGuid = (Guid)Session["UserId"]; ;
            //var userSysOrgId = entityDB.SystemUsersOrganizations.FirstOrDefault(rec => rec.UserId == loggedinUserGuid).SysUserOrganizationId;
            //var employeeTrainingRoleId = entityDB.SystemRoles.FirstOrDefault(rec => rec.RoleName == "Employee Training").RoleId;
            //var userRoles = entityDB.SystemUsersOrganizationsRoles.FirstOrDefault(rec => rec.SysUserOrgId == userSysOrgId && rec.SysRoleId == employeeTrainingRoleId);

            //if (userRoles != null)
            //{
            //    ViewBag.Employee = true;
            //}

            //bool vendorIsExemptFromTrainingFee = false;
            //var props = entityDB.ClientVendorProperties.FirstOrDefault(r => r.ClientID == clientID && r.VendorID == userSysUserOrg.OrganizationId);
            //if (props != null)
            //{
            //    if (props.TrainingPaymentExempt != null && props.TrainingPaymentExempt == true)
            //    {
            //        vendorIsExemptFromTrainingFee = true;
            //    }

            //}
            //ViewBag.VendorIsTrainingExempt = vendorIsExemptFromTrainingFee;
            // Ends<<<
            ViewBag.userType = type;
            return PartialView(EmpQuizs);
            //}
        }
        public string DeleteTraining(long SysUserOrgQuizId)
        {
            var DeleteEmployee = _employee.DeleteTrainings(SysUserOrgQuizId);

            return "EmployeeDeleted";
        }
        // Kiran on 3/10/2014
        [SessionExpireForView(pageType = "partial")]
        [HttpGet]
        public string deleteQuiz(long quizId, Guid userId)
        {
            //using (var entityDB = new EFDbContext())
            //{
            //    var sysUserQuizRecord = entityDB.SystemUsersOrganizationsQuizzes.FirstOrDefault(record => record.SysUserOrgQuizId == quizId);
            //    entityDB.SystemUsersOrganizationsQuizzes.Remove(sysUserQuizRecord);
            //    entityDB.SaveChanges();
            //    return "quizDeleteSuccess";
            //}
            _employee.DeleteTrainings(quizId);
            return "quizDeleteSuccess";
        }

        // Ends<<<

        // Kiran on 11/1/2014 for Employee Trainings Report
        public ActionResult getClientBusinessUnits(long OrganizationId)
        {
            using (var entityDB = new EFDbContext())
            {
                var ClientBusinessUnits = (from busUnits in entityDB.ClientBusinessUnits.Where(busUnits => busUnits.ClientId == OrganizationId).OrderBy(rec => rec.BusinessUnitName).ToList() // sumanth on 10/19/2015 for FVOS-72 to display the BUs list in alphabetical order.                                      
                                           select new SelectListItem
                                           {
                                               Text = busUnits.BusinessUnitName,
                                               Value = busUnits.ClientBusinessUnitId.ToString()
                                           }).ToList();
                return Json(ClientBusinessUnits);
            }
        }
        public ActionResult clientUsers(long OrganizationId)
        {
            var entityDB = new EFDbContext();
            {
                var clientUserIds = entityDB.SystemUsersOrganizations.Where(rec => rec.OrganizationId == OrganizationId).Select(rec => rec.UserId).ToList();


                var contactUsers = from users in entityDB.Contact.Where(rec => clientUserIds.Contains(rec.UserId)).ToList()
                                   select new SelectListItem
                                   {
                                       Text = users.LastName + ", " + users.FirstName,
                                       Value = users.UserId.ToString()

                                   };

                var contactUserIds = contactUsers.Where(rec => clientUserIds.Contains(Guid.Parse(rec.Value))).Select(rec => Guid.Parse(rec.Value)).ToList();
                var userEmails = from emails in entityDB.SystemUsers.Where(rec => clientUserIds.Contains(rec.UserId) && !contactUserIds.Contains(rec.UserId)).ToList()
                                 select new SelectListItem
                                 {
                                     Text = emails.Email,
                                     Value = emails.UserId.ToString()

                                 };

                List<SelectListItem> totalUsers = new List<SelectListItem>();
                totalUsers.AddRange(contactUsers);
                totalUsers.AddRange(userEmails);
                return Json(totalUsers);
            }
        }
        // Mani on 4/10/2015
        [HttpPost]
        public JsonResult getTemplatesList(long clientid)
        {
            var entityDB = new EFDbContext();
            {
                var templateId = entityDB.ClientTemplates.Where(rec => rec.ClientID == clientid).Select(rec => rec.TemplateID).ToList();
                var templatesList = from templates in entityDB.Templates.Where(rec => (templateId.Contains(rec.TemplateID)) && rec.TemplateType == 0).OrderBy(o => o.TemplateName).ToList()
                                    select new SelectListItem
                                    {
                                        Text = templates.TemplateName,
                                        Value = templates.ClientTemplates.Count > 1 ?
                                        templates.ClientTemplates.FirstOrDefault(r => r.Organizations.OrganizationType == OrganizationType.SuperClient).ClientTemplateID.ToStringNullSafe() :
                                        templates.ClientTemplates.FirstOrDefault().ClientTemplateID.ToStringNullSafe()
                                    };

                if (SSession.Role == OrganizationType.Admin)
                {
                    if (_sysUser.IsERIUser(SSession.UserId))
                    {
                        templateId = entityDB.ClientTemplates.Where(rec => rec.ClientID == clientid).Select(rec => rec.TemplateID).ToList();
                        templatesList = from templates in entityDB.Templates.Where(rec => (templateId.Contains(rec.TemplateID) && rec.IsERI == true) && rec.TemplateType == 0).OrderBy(o => o.TemplateName).ToList()
                                        select new SelectListItem
                                        {
                                            Text = templates.TemplateName,
                                            Value = templates.ClientTemplates.Count > 1 ?
                                            templates.ClientTemplates.FirstOrDefault(r => r.Organizations.OrganizationType == OrganizationType.SuperClient).ClientTemplateID.ToStringNullSafe() :
                                            templates.ClientTemplates.FirstOrDefault().ClientTemplateID.ToStringNullSafe()
                                        };

                    }
                }

                return Json(templatesList, JsonRequestBehavior.AllowGet);
            }

        }


        // Kiran on 12/01/215 for FV-215
        public JsonResult getClientTrainingTemplates(long clientId)
        {
            using (var entityDB = new EFDbContext())
            {
                var clientTrainingTemplates = from clientTemplates in entityDB.ClientTemplates.Where(rec => rec.ClientID == clientId && rec.Templates.TemplateType == 1).ToList()
                                              select new SelectListItem
                                              {
                                                  Text = clientTemplates.Templates.TemplateName,
                                                  Value = clientTemplates.ClientTemplateID.ToString()
                                              };
                clientTrainingTemplates = clientTrainingTemplates.OrderBy(rec => rec.Text).ToList();
                return Json(clientTrainingTemplates, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetEmployees(long vendorID)
        {
            using (var entityDB = new EFDbContext())
            {
                var employee_RoleId = EmployeeRole.EmployeeRoleId;
                //  var employee_RoleId = entityDB.SystemRoles.FirstOrDefault(record => record.RoleName == "Employee Training").RoleId;
                var employee_sysUserOrgs = entityDB.SystemUsersOrganizationsRoles.Where(record => employee_RoleId.Contains(record.SysRoleId))
                    .Select(rec => rec.SystemUsersOrganizations).Where(rec => rec.OrganizationId == vendorID);

                var employees = employee_sysUserOrgs.Join(entityDB.SystemUsers, suo => suo.UserId,
                                        su => su.UserId,
                                        (suo, su) => new { su })
                                    .Join(entityDB.Contact, su => su.su.UserId,
                                        c => c.UserId,
                                        (su, c) => new { su, c }).ToList();
                List<SelectListItem> results = new List<SelectListItem>();
                employees.ForEach(a => results.Add(new SelectListItem
                {
                    Text = a.c.LastName + ", " + a.c.FirstName,
                    Value = a.c.UserId.ToString()
                }));
                return Json(results.OrderBy(rec => rec.Text).ToList(), JsonRequestBehavior.AllowGet);
            }
        }


        public JsonResult GetVendors(long locationID)
        {
            using (var entityDB = new EFDbContext())
            {
                var vendors = (from ps in entityDB.PrequalificationSites.Join(entityDB.Organizations, ps => ps.VendorId, o => o.OrganizationID, (ps, o) => new { ps, o })
                              .Where(w => w.ps.ClientBusinessUnitSiteId == locationID).ToList()
                               select new SelectListItem
                               {
                                   Text = ps.o.Name,
                                   Value = ps.o.OrganizationID.ToString()
                               }).ToList();
                vendors = vendors.OrderBy(rec => rec.Text).DistinctBy(x => x.Value).ToList();
                vendors.Insert(0, new SelectListItem() { Text = "All", Value = "-1" });
                return Json(vendors, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetVisitors(long locationID)
        {
            using (var entityDB = new EFDbContext())
            {
                var visitors = (from vis in entityDB.Visitors.Where(r => r.ClientBusinessUnitSiteID == locationID).ToList()
                                select new SelectListItem
                                {
                                    Text = vis.LastName + "," + vis.FirstName,
                                    Value = vis.VisitorID.ToString()
                                }).ToList();
                visitors.Insert(0, new SelectListItem() { Text = "All", Value = "-1" });
                return Json(visitors, JsonRequestBehavior.AllowGet);
            }
        }

        // Kiran on 12/01/215 for FV-215 Ends<<
        [SessionExpire]
        [HeaderAndSidebar(headerName = "MyAccount", sideMenuName = "ContractorEvaluations", ViewName = "MyContractorEvaluations")]
        public ActionResult MyContractorEvaluations()
        {
            var entityDB = new EFDbContext();
            {

                Guid guidUserId = (Guid)Session["UserId"];
                var vendorReviews = entityDB.VendorReviewInput.Where(rec => rec.InputUserId == guidUserId).OrderByDescending(rec => rec.VendorReviewInputId).ToList();

                ViewBag.Comments = vendorReviews;
                return View();
            }
        }
        public PartialViewResult VendorDetailQSections(long pqid)
        {
            using (var entityDB = new EFDbContext())
            {
                List<QuestionAndAnswers> qAndA = _prequalification.GetVendorDetailsQSections(pqid);
                //var qAndA = new List<QuestionAndAnswers>();
                //var templateid = entityDB.Prequalification.Find(pqid).ClientTemplates.TemplateID;
                //var Questions = entityDB.VendorDetailQuestions.Where(r => r.TemplateId == templateid && r.Type == 0).Select(r => r.Questions).OrderBy(r => r.QuestionID).Select(r => new
                //{
                //    QText = r.QuestionText,
                //    QCids = r.QuestionColumnDetails.Select(r1 => r1.QuestionColumnId)

                //}).ToList();
                //foreach (var question in Questions)
                //{
                //    var a = new QuestionAndAnswers();
                //    a.QuestionName = question.QText;
                //    a.Answers = new List<string>();
                //    var QCols = entityDB.PrequalificationUserInput.Where(r => r.PreQualificationId == pqid && question.QCids.Contains(r.QuestionColumnId)).Select(r => new { r.UserInput, r.QuestionColumnDetails.ColumnValues, r.QuestionColumnDetails.TableReference, r.QuestionColumnDetails.TableReferenceField, r.QuestionColumnDetails.TableRefFieldValueDisplay, r.QuestionColumnDetails.QuestionControlTypeId }).ToList();
                //    foreach (var Qcol in QCols)
                //    {
                //        if (string.IsNullOrEmpty(Qcol.TableReference))
                //        {
                //            if (Qcol.QuestionControlTypeId == 8)
                //            {
                //                if (Qcol.UserInput.Equals("true", StringComparison.CurrentCultureIgnoreCase))
                //                    a.Answers.Add(Qcol.ColumnValues);
                //            }
                //            else
                //                a.Answers.Add(Qcol.UserInput);
                //        }
                //        else
                //        {
                //            var helper = new PrequalificationHelper();
                //            a.Answers.Add(helper.GetUserInputAnswer(Qcol.TableReference, Qcol.TableReferenceField, Qcol.TableRefFieldValueDisplay, Qcol.UserInput));
                //        }
                //    }
                //    //a.Answers = .Select(r => r.UserInput).ToList();
                //    qAndA.Add(a);
                return PartialView(qAndA);
            }
        }
        public bool CheckIsERIClient(long clientId)
        {
            return _clientBusiness.CheckClientIsInERIClients(clientId);
        }

        public bool? CheckIsERIClientBasedOnEmailTemplate(int emailTemplateId)
        {
            long? clientId = _emailTemplate.GetEmailTemplateClientId(emailTemplateId);
            if (clientId != null)
                return _clientBusiness.CheckClientIsInERIClients((long)clientId);
            else
                return null;
        }
        public string SubClients(long clientId)
        {
            using (var entityDB = new EFDbContext())
            {
                var Client = entityDB.SubClients.FirstOrDefault(r => r.ClientId == clientId);
                if (Client != null)
                    return "true";

                else return "false";
            }
        }
        public JsonResult GetGetCountryLanguages(string country)
        {
            var languages = _organization.GetCountryLanguages(country);
            List<SelectListItem> selectLists = new List<SelectListItem>();
            selectLists.Add(new SelectListItem() { Text = "Select", Value = "" });
            languages.ForEach(s => selectLists.Add(new SelectListItem() { Text = s.Key, Value = s.Value }));
            return Json(languages.ToArray(), JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddComments(long PqId)
        {
            return Json(_sysUser.GetPQcomments(PqId), JsonRequestBehavior.AllowGet);
        }
        public string UpdateComments(long PqId, string Comments, bool IsChecked)
        {
            _sysUser.UpdatePqComments(PqId, Comments, SSession.UserId, IsChecked);
            return "Success";
        }
        public JsonResult GetInvitationData(Guid InvitationId)
        {
            var invitation = _vendor.GetInviteInfo(InvitationId);
            return Json(invitation.Comments, JsonRequestBehavior.AllowGet);
        }
        public ActionResult SendInvitation(LocalinviteVendor modelInviteVendor)
        {

            _vendor.UpdateInviteComments(modelInviteVendor, SSession.UserId);
            var InviteVen = _vendor.GetInviteInfo(modelInviteVendor.InvitationId);

            if (InviteVen.InvitationType == null)
                return Json(_vendor.SendNewInvitation(modelInviteVendor.InvitationId, (Guid)Session["UserId"]), JsonRequestBehavior.AllowGet);
            else
                return Json(_vendor.SendOutsideInvitation(modelInviteVendor.InvitationId, (Guid)Session["UserId"]), JsonRequestBehavior.AllowGet);

            //  string strMessage = ""; // Kiran on 4/10/2014
            // strMessage = SendMail.sendMail(MailInfo.toAddress, MailInfo.body, MailInfo.subject, MailInfo.bcc, MailInfo.cc);
            // return Json(strMessage, JsonRequestBehavior.AllowGet);
            // return Json("", JsonRequestBehavior.AllowGet);

        }

    }
}






