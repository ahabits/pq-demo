﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FVGen3.Domain.Concrete;
using FVGen3.Domain.Abstract;
using FVGen3.Domain.Entities;

namespace FVGen3.WebUI.Controllers
{
    public class InfusionSoftInvitationsController : Controller
    {
        //
        // GET: /InfusionSoftInvitations/
         EFDbContext entityDB;

        public InfusionSoftInvitationsController(ISystemUsersRepository repositorySystemUsers) 
        {
            entityDB = new EFDbContext();
        }
        //[HttpPost]
        public ActionResult ImportInvitations(string VendorEmail, string VendorCompanyName, string VendorFirstName, string VendorLastName, long? ClientId, DateTime? InvitationDate)//Rajesh on 2/21/2014
        {
            ViewBag.VBerror = false;
            if (ClientId == null || InvitationDate == null)
            {
                ViewBag.VBerror = true;
                return View();
            }
            try
            {
                //Rajesh on 2/12/2014
                if (entityDB.VendorInviteDetails.FirstOrDefault(rec => rec.InvitationSentFrom == 1 && rec.VendorEmail == VendorEmail && rec.ClientId == ClientId) != null)
                    return View();
                //Ends<<
                var vInvitation = new VendorInviteDetails();
                vInvitation.VendorEmail = VendorEmail == null ? "" : VendorEmail;
                vInvitation.VendorCompanyName = VendorCompanyName == null ? "" : VendorCompanyName;
                vInvitation.InvitationSentFrom = 1;
                vInvitation.VendorFirstName = VendorFirstName == null ? "" : VendorFirstName;
                vInvitation.VendorLastName = VendorLastName == null ? "" : VendorLastName;
                vInvitation.ClientId = (long)ClientId;
                var clientCode = entityDB.Organizations.Find(ClientId).SingleClientCode;//Rajesh on 2/21/2014
                vInvitation.ClientCode = clientCode;//Rajesh on 2/21/2014
                vInvitation.InvitationDate = (InvitationDate == null) ? DateTime.Now : (DateTime)InvitationDate;
                entityDB.VendorInviteDetails.Add(vInvitation);
                entityDB.SaveChanges();
            }
            catch
            {
                ViewBag.VBerror = true;
            }
            return View();
        }

    }
}
