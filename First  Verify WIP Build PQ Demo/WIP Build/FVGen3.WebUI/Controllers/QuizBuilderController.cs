﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FVGen3.WebUI.Annotations;
using FVGen3.Domain.Entities;
using FVGen3.Domain.Concrete;
using System.Data.Entity;
using System.IO;
using System.Configuration;

namespace FVGen3.WebUI.Controllers
{
    public class QuizBuilderController : BaseController
    {
        EFDbContext entityDB;
        public QuizBuilderController()
        {
            entityDB = new EFDbContext();
        }
        //
        // GET: /QuizBuilder/
        [SessionExpire]
        [HeaderAndSidebar(headerName = "QuestionnaireTool", sideMenuName = "QuizBuilder", ViewName = "AddOrEditQuiz")]
        public ActionResult AddOrEditQuiz()
        {
            ViewBag.Quizes = entityDB.Templates.Where(rec=>rec.TemplateType==1 && rec.TemplateStatus==1).ToList();
            LocalQuizTemplate form = new LocalQuizTemplate();
            return View(form);


        }

        [SessionExpire]
        [HttpPost]
        [HeaderAndSidebar(headerName = "QuestionnaireTool", sideMenuName = "QuizBuilder", ViewName = "AddOrEditQuiz")]
        public ActionResult AddOrEditQuiz(LocalQuizTemplate form)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.Quizes = entityDB.Templates.Where(rec => rec.TemplateType == 1 && rec.TemplateStatus == 1).ToList();
                return View(form);
            }
            Templates template;
            if (form.TemplateId == null)
            {
                template = new Templates();
                template.TemplateName = form.TemplateName;
                template.TemplateNotes = form.TemplateNote;
                template.DefaultTemplate = true;
                template.TemplateStatus = 1;
                template.TemplateType = 1;
                template.TemplateSections = new List<TemplateSections>();
                TemplateSections VideoSec = new TemplateSections();
                VideoSec.TemplateID = template.TemplateID;
                VideoSec.DisplayOrder = 1;
                VideoSec.Visible = true;
                VideoSec.SectionName = "Video";
                
                VideoSec.VisibleToClient = false;
                VideoSec.TemplateSectionType = 21;
                template.TemplateSections.Add(VideoSec);
                
                VideoSec.TemplateSubSections = new List<TemplateSubSections>();

                TemplateSubSections subSec1 = new TemplateSubSections();
                subSec1.TemplateSectionID = VideoSec.TemplateSectionID;
                subSec1.SubSectionType = 2;
                subSec1.SubSectionName = "Training Videos";
                subSec1.DisplayOrder = 1;
                subSec1.Visible = true;
                subSec1.SubHeader = false;
                subSec1.NoOfColumns = 0;
                subSec1.SubSectionTypeCondition = "QuizVideos";
                VideoSec.TemplateSubSections.Add(subSec1);  
  

                
                TemplateSections QuesSec = new TemplateSections();
                QuesSec.TemplateID = template.TemplateID;
                QuesSec.DisplayOrder = 1;
                QuesSec.Visible = true;
                QuesSec.SectionName = "Questions";
                QuesSec.TemplateSectionType = 1;
                QuesSec.VisibleToClient = false;
                QuesSec.TemplateSubSections = new List<TemplateSubSections>();
                template.TemplateSections.Add(QuesSec);

                TemplateSubSections subSec2 = new TemplateSubSections();
                subSec2.TemplateSectionID = QuesSec.TemplateSectionID;
                subSec2.SubSectionType = 0;
                subSec2.SubSectionName = "Qualify Training";
                subSec2.DisplayOrder = 1;
                subSec2.Visible = true;
                subSec2.SubHeader = false;
                subSec2.SectionNotes = "Please choose the correct answer";
                subSec2.NoOfColumns = 0;

                QuesSec.TemplateSubSections.Add(subSec2);    
                entityDB.Templates.Add(template);

            }
            else
            {
                template = entityDB.Templates.Find(form.TemplateId);
                template.TemplateName = form.TemplateName;
                template.TemplateNotes = form.TemplateNote;
                entityDB.Entry(template).State = EntityState.Modified;
            }
            entityDB.SaveChanges();
           
            ViewBag.Quizes = entityDB.Templates.Where(rec => rec.TemplateType == 1 && rec.TemplateStatus == 1).ToList();
            return Redirect("AddOrEditQuiz");
        }

        [SessionExpire]
        [HeaderAndSidebar(headerName = "QuestionnaireTool", sideMenuName = "QuizBuilder", ViewName = "AssignQuizes")]
        public ActionResult AssignQuizes()
        {
            ViewBag.ClientTemplates = entityDB.ClientTemplates.Where(rec => rec.Templates.TemplateType == 1).ToList();
            ViewBag.documents = entityDB.Document.ToList();
            return View();
        }

        [SessionExpire]
        [HeaderAndSidebar(headerName = "QuestionnaireTool", sideMenuName = "QuizBuilder", ViewName = "AddOrEditAssignQuizes")]
        public ActionResult AddOrEditAssignQuizes(long ClientId)
        {
            //Rajesh on 12/18/2013>>>
            var clientIds = entityDB.ClientTemplates.Where(rec => rec.Templates.TemplateType == 1).Select(rec => rec.ClientID).ToList();
                             


            ViewBag.Clients = (from client in entityDB.Organizations.Where(rec => rec.OrganizationType == "Client" && !clientIds.Contains(rec.OrganizationID)).ToList()
                               select new SelectListItem()
                               {
                                   Text = client.Name,
                                   Value = client.OrganizationID + "",
                                   Selected = client.OrganizationID == ClientId
                               }).ToList();

            if (ClientId != -1)//Means He is in editing mode(Show only that client)
            {
                ViewBag.Clients = (from client in entityDB.Organizations.Where(rec => rec.OrganizationType == "Client" && rec.OrganizationID == ClientId).ToList()
                                   select new SelectListItem()
                                   {
                                       Text = client.Name,
                                       Value = client.OrganizationID + "",
                                       Selected = client.OrganizationID == ClientId
                                   }).ToList();
            }
            //Ends<<<
            LocalAssignQuiz LocQuiz = new LocalAssignQuiz();
            //LocQuiz.templateInfo = (from templates in entityDB.Templates.Where(rec => rec.TemplateType == 1 && rec.TemplateStatus == 1).ToList()
            //                        select new LocalAssignQuiz.TemplateInfo()
            //                        {
            //                            IsReadOnly = entityDB.ClientTemplates.FirstOrDefault(rec => rec.ClientID == ClientId && rec.TemplateID == templates.TemplateID) != null &&
            //                                         entityDB.ClientTemplates.FirstOrDefault(rec => rec.ClientID == ClientId && rec.TemplateID == templates.TemplateID).EmployeeQuizzes != null &&
            //                                         entityDB.ClientTemplates.FirstOrDefault(rec => rec.ClientID == ClientId && rec.TemplateID == templates.TemplateID).EmployeeQuizzes.FirstOrDefault(rec => rec.EmployeeQuizUserInput != null && rec.EmployeeQuizUserInput.Count() > 0) != null,
            //                            IsChecked = entityDB.ClientTemplates.FirstOrDefault(rec => rec.ClientID == ClientId && rec.TemplateID == templates.TemplateID) != null,
            //                            TemplateId = templates.TemplateID,
            //                            TemplateName = templates.TemplateName,
            //                            FileName = entityDB.ClientTemplates.FirstOrDefault(rec => rec.ClientID == ClientId && rec.TemplateID == templates.TemplateID) == null ? "" : entityDB.ClientTemplates.FirstOrDefault(rec => rec.ClientID == ClientId && rec.TemplateID == templates.TemplateID).QuizVideoURL,
            //                            QuizPassRate = entityDB.ClientTemplates.FirstOrDefault(rec => rec.ClientID == ClientId && rec.TemplateID == templates.TemplateID) == null ? "" : entityDB.ClientTemplates.FirstOrDefault(rec => rec.ClientID == ClientId && rec.TemplateID == templates.TemplateID).QuizPassRate.ToString() // Kiran on 2/21/2014
            //                        }).ToList();

            return View(LocQuiz);
        }
        [SessionExpire]
        [HeaderAndSidebar(headerName = "QuestionnaireTool", sideMenuName = "QuizBuilder", ViewName = "AddOrEditAssignQuizes")]
        [HttpPost]
        public ActionResult AddOrEditAssignQuizes(LocalAssignQuiz Form)
        {

            if (Form.templateInfo==null || Form.templateInfo.Where(rec => rec.IsChecked == true).Count() == 0)
            {
                ViewBag.Clients = (from client in entityDB.Organizations.Where(rec => rec.OrganizationType == "Client").ToList()
                                   select new SelectListItem()
                                   {
                                       Text = client.Name,
                                       Value = client.OrganizationID + ""
                                   }).ToList();
                ModelState.AddModelError("", "Please select at least one Training");
                return View(Form);
            }
            foreach (var info in Form.templateInfo)
            {
                var clientId = Convert.ToInt64(Form.ClientId);
                //Kiran on 2/21/2014
                decimal passRate = Convert.ToDecimal(info.QuizPassRate);
                if (info.IsChecked)
                {
                    if (info.FileName == "" || info.FileName == null)
                    {
                        ViewBag.Clients = (from client in entityDB.Organizations.Where(rec => rec.OrganizationType == "Client").ToList()
                                           select new SelectListItem()
                                           {
                                               Text = client.Name,
                                               Value = client.OrganizationID + ""
                                           }).ToList();
                        ModelState.AddModelError("", "Please enter Video URL");
                        return View(Form);
                    }
                    if (passRate == 0)
                    {
                        ViewBag.Clients = (from client in entityDB.Organizations.Where(rec => rec.OrganizationType == "Client").ToList()
                                           select new SelectListItem()
                                           {
                                               Text = client.Name,
                                               Value = client.OrganizationID + ""
                                           }).ToList();
                        ModelState.AddModelError("", "Please enter Pass Rate");
                        return View(Form);
                    }
                }
                if (passRate > 100)
                {
                    ViewBag.Clients = (from client in entityDB.Organizations.Where(rec => rec.OrganizationType == "Client").ToList()
                                       select new SelectListItem()
                                       {
                                           Text = client.Name,
                                           Value = client.OrganizationID + ""
                                       }).ToList();
                    ModelState.AddModelError("", "Pass Rate should not be more than 100");
                    return View(Form);
                }
                //Ends<<<
                ClientTemplates cTemplate = entityDB.ClientTemplates.FirstOrDefault(rec => rec.TemplateID == info.TemplateId && rec.ClientID == clientId);
                if (cTemplate == null && info.IsChecked)
                {
                    cTemplate = new ClientTemplates();
                    cTemplate.ClientID = clientId;
                    cTemplate.TemplateID = info.TemplateId;
                    cTemplate.DisplayOrder = 0;
                    cTemplate.Visible = true;
                    //cTemplate.QuizVideoURL = info.FileName;
                    //cTemplate.QuizPassRate = Convert.ToDecimal(info.QuizPassRate); // Kiran on 2/21/2014
                    entityDB.ClientTemplates.Add(cTemplate);

                    foreach (var clientBU in entityDB.ClientBusinessUnits.Where(rec => rec.ClientId == clientId))
                    {

                        ClientTemplatesForBU CTBU = new ClientTemplatesForBU();
                        CTBU.ClientTemplateId = cTemplate.ClientTemplateID;
                        CTBU.ClientBusinessUnitId = clientBU.ClientBusinessUnitId;
                        entityDB.ClientTemplatesForBU.Add(CTBU);
                    }

                }
                else if (cTemplate != null)
                {
                    if (info.IsChecked)
                    {
                        //cTemplate.QuizVideoURL = info.FileName;
                        //cTemplate.QuizPassRate = Convert.ToDecimal(info.QuizPassRate); // Kiran on 2/21/2014
                        entityDB.Entry(cTemplate).State = EntityState.Modified;
                    }
                    else//He wants to delete client template
                    {
                        foreach (var clientBu in cTemplate.ClientTemplatesForBU.ToList())
                        {
                            entityDB.ClientTemplatesForBU.Remove(clientBu);
                        }
                        entityDB.ClientTemplates.Remove(cTemplate);
                    }
                }

                entityDB.SaveChanges();
            }
            //entityDB.SaveChanges();//Rajesh on 12/18/2013
            return Redirect("../TemplateTool/PopUpCloseView");
        }

        [SessionExpire]
        [HeaderAndSidebar(headerName = "QuestionnaireTool", sideMenuName = "QuizBuilder", ViewName = "Questionslist")]
        
        public ActionResult Questionslist(Guid TemplateId)
        {
            ViewBag.TemplateId = TemplateId;//Rajesh on 8/12/2014
            Templates template=entityDB.Templates.Find(TemplateId);
            var SubSection = template.TemplateSections.FirstOrDefault(rec => rec.TemplateSectionType != 21).TemplateSubSections.FirstOrDefault(rec => rec.SubSectionTypeCondition != "QuizVideos");
            


            //To Change Side Menu Selection>>>
            if (SubSection.TemplateSections.Templates.DefaultTemplate == null || SubSection.TemplateSections.Templates.DefaultTemplate == false)
            {
                List<LocalHeaderMenuModel> headerMenu = (List<LocalHeaderMenuModel>)ViewBag.headerMenus;
                headerMenu.FirstOrDefault(record => record.menuName == "QuestionnaireTool").sideMenus.ForEach(record => record.isChecked = false);
                headerMenu.FirstOrDefault(record => record.menuName == "QuestionnaireTool").sideMenus.FirstOrDefault(record => record.menuName == "CustomTemplateTool").isChecked = true;
                ViewBag.headerMenus = headerMenu;
            }

            //Ends<<<
            ViewBag.TemplateStatus = SubSection.TemplateSections.Templates.TemplateStatus;


            ViewBag.TemplateType = SubSection.TemplateSections.Templates.DefaultTemplate == true ? 1 : 0;//To Know Template Type is standard or Custom

            ViewBag.SectionId = SubSection.TemplateSectionID;
            ViewBag.TemplateName = SubSection.TemplateSections.Templates.TemplateName;
            ViewBag.SectionName = SubSection.TemplateSections.SectionName;
            ViewBag.SubSectionName = SubSection.SubSectionName;

            ViewBag.SubSectionType = SubSection.SubSectionType;
            ViewBag.Condition = SubSection.SubSectionTypeCondition;

            ViewBag.SubSections = (from subsections in SubSection.TemplateSections.TemplateSubSections.ToList()
                                   select new SelectListItem
                                   {
                                       Text = subsections.SubSectionName,
                                       Value = subsections.SubSectionID + "",
                                       Selected = SubSection.SubSectionID == subsections.SubSectionID
                                   }).ToList();

            ViewBag.SubSectionId = SubSection.SubSectionID;
            ViewBag.QuestionsBankList = entityDB.QuestionsBank.Where(rec => rec.QuestionReference == 2 && rec.QuestionStatus == 1 && rec.UsedFor==1).ToList();

            ViewBag.QuestionsList = entityDB.Questions.Where(rec => rec.SubSectionId == SubSection.SubSectionID).OrderBy(rec => rec.DisplayOrder).ToList();//Rajesh on 11/25/2013
            return View();
        }


        [HttpGet]
        public string DeleteTemplate(Guid TemplateId)
        {
            var template = entityDB.Templates.Find(TemplateId);
            try
            {
                if (template.ClientTemplates != null)
                {
                    foreach (var Ctemplate in template.ClientTemplates.ToList())
                    {
                        foreach (var CTData in Ctemplate.ClientTemplateReportingData.ToList())
                            entityDB.Entry(CTData).State = EntityState.Deleted;

                        entityDB.Entry(Ctemplate).State = EntityState.Deleted;
                    }
                }
                if (template.TemplateSections != null)
                    foreach (var Tsection in template.TemplateSections.ToList())
                    {
                        if (Tsection.TemplateSectionsHeaderFooter != null)
                            foreach (var HandF in Tsection.TemplateSectionsHeaderFooter.ToList())
                            {
                                entityDB.Entry(HandF).State = EntityState.Deleted;
                            }
                        if (Tsection.TemplateSubSections != null)
                            foreach (var Ssection in Tsection.TemplateSubSections.ToList())
                            {

                                if (Ssection.Questions != null)
                                    foreach (var question in Ssection.Questions.ToList())//Delete Questions
                                    {
                                        if (question.QuestionColumnDetails != null)
                                            foreach (var Qcolumn in question.QuestionColumnDetails.ToList())
                                                entityDB.Entry(Qcolumn).State = EntityState.Deleted;
                                        if (question.QuestionsDependants != null)
                                        {
                                            foreach (var Qdependent in question.QuestionsDependants.ToList())
                                                entityDB.Entry(Qdependent).State = EntityState.Deleted;
                                        }
                                        entityDB.Entry(question).State = EntityState.Deleted;
                                    }
                                entityDB.Entry(Ssection).State = EntityState.Deleted;
                            }
                        entityDB.Entry(Tsection).State = EntityState.Deleted;
                    }
                entityDB.Entry(template).State = EntityState.Deleted;
                entityDB.SaveChanges();
                return "OK";
            }catch(Exception ee)
            {
                return "Quiz cannot be deleted. This quiz is already in use";
            }
        }

        public string ChangeQuestionsOrder(string QuestionIds, long templateSubSectionId)
        {
            string result = "Ok" + QuestionIds + "\n";
            try
            {
                int orderNumber = 0;
                var templateSubSec = entityDB.TemplateSubSections.Find(templateSubSectionId);
                if (templateSubSec != null)
                    foreach (var ids in QuestionIds.Split('|'))
                    {
                        try
                        {
                            long secId = Convert.ToInt64(ids);
                            result += secId;
                            Questions Question = templateSubSec.Questions.ToList().FirstOrDefault(rec => rec.QuestionID == secId);
                            if (Question == null)
                                continue;
                            orderNumber++;
                            Question.DisplayOrder = orderNumber;
                            entityDB.Entry(Question).State = EntityState.Modified;
                        }
                        catch (Exception ee) { }
                    }
                entityDB.SaveChanges();
            }
            catch (Exception ee) { result += ee.Message; }
            return result;
        }

        [HttpPost]
        public string AddOrEditQuestion(string QuestionText, long SubSectionId, int NumberOfColumns, long QuestionBankId, long QuestionId)
        {
            Questions question;
            long questionId = -1;
            if (QuestionId == -1)
            {
                question = new Questions();
                question.QuestionText = QuestionText;
                question.SubSectionId = SubSectionId;
                question.DisplayOrder = 0;
                question.Visible = true;
                question.NumberOfColumns = NumberOfColumns;
                question.IsMandatory = true;
                question.IsBold = true;
                question.ResponseRequired = true;
                question.DisplayResponse = true;
                question.HasDependantQuestions = false;
                question.QuestionBankId = QuestionBankId;
                entityDB.Questions.Add(question);
                questionId = question.QuestionID;
            }
            else
            {
                question = entityDB.Questions.Find(QuestionId);

                if (NumberOfColumns != question.NumberOfColumns && question.QuestionColumnDetails != null)
                {
                    foreach (var QColumn in question.QuestionColumnDetails.ToList())
                        entityDB.Entry(QColumn).State = EntityState.Deleted;
                }
                question.NumberOfColumns = NumberOfColumns;
                questionId = question.QuestionID;

                entityDB.Entry(question).State = EntityState.Modified;
            }

            entityDB.SaveChanges();
            return "" + question.QuestionID;
        }

        [HttpPost]
        public string DeleteQuestions(string questionBankIds, long subSectionId)
        {
            if (questionBankIds == "1")//Delete Blank Questions
            {
                foreach (var question in entityDB.Questions.Where(rec => rec.QuestionBankId == 1 && rec.SubSectionId == subSectionId).ToList())
                {
                    //entityDB.Entry(question).State = EntityState.Deleted;
                    if (question.QuestionColumnDetails != null)
                        foreach (var Qcolumn in question.QuestionColumnDetails.ToList())
                            entityDB.Entry(Qcolumn).State = EntityState.Deleted;

                    if (question.EmployeeQuizAnswers != null)
                        foreach (var Qcolumn in question.EmployeeQuizAnswers.ToList())
                            entityDB.Entry(Qcolumn).State = EntityState.Deleted;

                    if (question.QuestionsDependants != null)
                    {
                        foreach (var Qdependent in question.QuestionsDependants.ToList())
                            entityDB.Entry(Qdependent).State = EntityState.Deleted;
                    }
                    entityDB.Entry(question).State = EntityState.Deleted;
                }
                entityDB.SaveChanges();
                return "Ok";
            }

            foreach (var QBId in questionBankIds.Split(','))
            {

                try
                {
                    long QBankId = Convert.ToInt64(QBId);
                    foreach (var question in entityDB.Questions.Where(rec => rec.QuestionBankId == QBankId && rec.SubSectionId == subSectionId).ToList())
                    {
                        if (question.QuestionColumnDetails != null)
                            foreach (var Qcolumn in question.QuestionColumnDetails.ToList())
                                entityDB.Entry(Qcolumn).State = EntityState.Deleted;
                        if (question.EmployeeQuizAnswers != null)
                            foreach (var Qcolumn in question.EmployeeQuizAnswers.ToList())
                                entityDB.Entry(Qcolumn).State = EntityState.Deleted;
                        if (question.QuestionsDependants != null)
                        {
                            foreach (var Qdependent in question.QuestionsDependants.ToList())
                                entityDB.Entry(Qdependent).State = EntityState.Deleted;
                        }
                        entityDB.Entry(question).State = EntityState.Deleted;
                    }
                }
                catch (Exception ee) { }
            }
            entityDB.SaveChanges();
            return "OK";
        }

        [HttpPost]
        public string AddQuestionBank(string QuestionString)
        {
            var currentQuestionRecord = entityDB.QuestionsBank.FirstOrDefault(record => record.QuestionString.ToUpper() == QuestionString.ToUpper() && record.QuestionReference == 2 && record.UsedFor == 1); // Kiran on 7/3/2014
            if (currentQuestionRecord != null)
            {
                ViewBag.isQuestionExits = true;
                return "Already Exist";
            }
            QuestionsBank Question = new QuestionsBank();
            Question.QuestionString = QuestionString;
            Question.QuestionReference = 2;
            entityDB.QuestionsBank.Add(Question);
            Question.UsedFor = 1;
            Question.QuestionStatus = 1;
            entityDB.SaveChanges();

            return "Ok";
        }


        //Rajesh on 10/25/2013
        [SessionExpireForView]
        [HeaderAndSidebar(headerName = "QuestionnaireTool", sideMenuName = "QuizBuilder", ViewName = "AddOrEditQuestionColumnDetails")]
        public ActionResult AddOrEditQuestionColumnDetails(long QuestionId)
        {
            ViewBag.QuestionControlTypes = (from controls in entityDB.QuestionControlType.Where(rec => rec.Visible == true&&rec.QuestionTypeID==7).ToList()
                                            select new SelectListItem
                                            {
                                                Text = controls.QuestionTypeDisplayName,
                                                Value = controls.QuestionTypeID + ""
                                            }).ToList();
            var Question = entityDB.Questions.Find(QuestionId);
            ViewBag.Question = Question;
            // Kiran on 9/11/2014
            var questionColumnDetailsExists = entityDB.QuestionColumnDetails.FirstOrDefault(rec => rec.QuestionId == QuestionId);
            LocalQuestionColumnDetails details = new LocalQuestionColumnDetails();
            if (questionColumnDetailsExists != null)
            {
                details.isEdit = true;
                var employeeQuizUserInputExists = entityDB.EmployeeQuizUserInput.FirstOrDefault(rec => rec.QuestionColumnId == questionColumnDetailsExists.QuestionColumnId);
                if (employeeQuizUserInputExists != null)
                {
                    details.isQuesionColumnIdexistsInQuizUserInput = true;
                }
            }
            else
            {
                details.isEdit = false;
            }
            // Ends<<<
            details.QuestionId = QuestionId;
            details.IsBold = (bool)Question.IsBold;
            details.IsMandatory = (bool)Question.IsMandatory;
            details.IsRepeat = Question.QuestionColumnDetails.Count() == 0;
            details.ResponseRequired = Question.ResponseRequired;
            details.Visible = !Question.Visible;
            details.ColumnDetails = new List<ColumnDetails>();
            details.HasDependent = (bool)Question.HasDependantQuestions;
            if (Question.EmployeeQuizAnswers != null && Question.EmployeeQuizAnswers.Count() > 0)
                details.QuestionAnswer = Question.EmployeeQuizAnswers[0].CorrectAnswer;
            if (Question.QuestionColumnDetails.Count() == 0)
            {
                for (int i = 0; i < Question.NumberOfColumns; i++)
                {
                    details.ColumnDetails.Add(new ColumnDetails());
                }
            }
            else
            {
                foreach (var column in Question.QuestionColumnDetails)
                {
                    ColumnDetails c = new ColumnDetails();
                    c.ControlType = column.QuestionControlTypeId + "";
                    c.DisplayType = column.DisplayType;
                    c.HasTableReference = !string.IsNullOrWhiteSpace(column.TableReference);
                    c.RefTableName = column.TableReference;
                    c.Value = column.ColumnValues;
                    details.ColumnDetails.Add(c);
                }
            }
            return View(details);
        }
        [HttpPost]
        [SessionExpireForView]
        [HeaderAndSidebar(headerName = "QuestionnaireTool", sideMenuName = "QuizBuilder", ViewName = "AddOrEditQuestionColumnDetails")]
        public ActionResult AddOrEditQuestionColumnDetails(LocalQuestionColumnDetails form)
        {
            ViewBag.QuestionControlTypes = (from controls in entityDB.QuestionControlType.Where(rec => rec.Visible == true && rec.QuestionTypeID==7).ToList()
                                            select new SelectListItem
                                            {
                                                Text = controls.QuestionTypeDisplayName,
                                                Value = controls.QuestionTypeID + ""
                                            }).ToList();

            var Question = entityDB.Questions.Find(form.QuestionId);

            var errorMessage = "";

            ViewBag.Question = Question;

            Question.IsBold = form.IsBold;
            Question.IsMandatory = form.IsMandatory;
            Question.Visible = !form.Visible;
            Question.ResponseRequired = form.ResponseRequired;
            Question.HasDependantQuestions = form.HasDependent;

            if (string.IsNullOrEmpty(form.QuestionAnswer))
            {
                ModelState.AddModelError("", "Please enter Question Answer and Value");
                return View(form);
            }
            if (Question.QuestionColumnDetails == null || !(Question.QuestionColumnDetails.Where(rec => rec.PrequalificationUserInputs.Count() > 0).Count() > 0))
            {
                EmployeeQuizAnswers QuestionAns=entityDB.EmployeeQuizAnswers.FirstOrDefault(rec=>rec.QuestionId==Question.QuestionID);
                if (QuestionAns == null)
                {
                    QuestionAns = new EmployeeQuizAnswers();
                    QuestionAns.CorrectAnswer = form.QuestionAnswer;
                    QuestionAns.QuestionId = Question.QuestionID;
                    entityDB.EmployeeQuizAnswers.Add(QuestionAns);
                }
                else
                {
                    QuestionAns.CorrectAnswer = form.QuestionAnswer;
                    entityDB.Entry(QuestionAns).State = EntityState.Modified;
                }

                // Kiran on 9/11/2014
                //foreach (var column in Question.QuestionColumnDetails.ToList())
                //{
                //    entityDB.Entry(column).State = EntityState.Deleted;
                //}

                if (form.isEdit == false)
                {
                    if (form.ColumnDetails != null)
                        for (int i = 0; i < form.ColumnDetails.Count(); i++)
                        {
                            if (form.IsRepeat)
                            {
                                if (form.ColumnDetails[0].ControlType == "7" || form.ColumnDetails[0].ControlType == "10")//Radio or MultiSelect
                                {
                                    if (string.IsNullOrEmpty(form.ColumnDetails[0].Value))
                                    {
                                        errorMessage = "Column 1 ";
                                    }
                                }
                                else if (!form.ColumnDetails[0].HasTableReference && form.ColumnDetails[0].ControlType == "9")
                                {
                                    if (string.IsNullOrEmpty(form.ColumnDetails[0].Value))
                                    {
                                        errorMessage = "Column 1 ";
                                    }
                                }
                                QuestionColumnDetails details = new QuestionColumnDetails();
                                details.ColumnNo = i + 1;
                                details.ColumnValues = form.ColumnDetails[0].Value;
                                details.DisplayType = Convert.ToInt32(form.ColumnDetails[0].DisplayType);
                                details.QuestionControlTypeId = Convert.ToInt32(form.ColumnDetails[0].ControlType);
                                details.QuestionId = Question.QuestionID;
                                if (form.ColumnDetails[0].HasTableReference)
                                {
                                    details.ColumnValues = "";
                                    details.TableReference = form.ColumnDetails[0].RefTableName;
                                    if (form.ColumnDetails[0].RefTableName == "BiddingInterests" || form.ColumnDetails[0].RefTableName == "BiddingInterests")
                                    {
                                        details.TableReferenceField = "BiddingInterestId";
                                        details.TableRefFieldValueDisplay = "BiddingInterestName";
                                    }

                                }
                                Question.QuestionColumnDetails.Add(details);
                            }
                            else
                            {

                                if (form.ColumnDetails[i].ControlType == "7" || form.ColumnDetails[i].ControlType == "10")//Radio or MultiSelect
                                {
                                    if (string.IsNullOrEmpty(form.ColumnDetails[0].Value))
                                    {
                                        errorMessage += " Column " + (i + 1) + ",";
                                    }
                                }
                                else if (!form.ColumnDetails[i].HasTableReference && form.ColumnDetails[i].ControlType == "9")
                                {
                                    if (string.IsNullOrEmpty(form.ColumnDetails[0].Value))
                                    {
                                        errorMessage += " Column " + (i + 1) + ",";
                                    }
                                }

                                QuestionColumnDetails details = new QuestionColumnDetails();
                                details.ColumnNo = i + 1;
                                details.ColumnValues = form.ColumnDetails[i].Value;
                                details.DisplayType = Convert.ToInt32(form.ColumnDetails[i].DisplayType);
                                details.QuestionControlTypeId = Convert.ToInt32(form.ColumnDetails[i].ControlType);
                                details.QuestionId = Question.QuestionID;
                                if (form.ColumnDetails[i].HasTableReference)
                                {
                                    details.TableReference = form.ColumnDetails[0].RefTableName;
                                    if (form.ColumnDetails[i].RefTableName == "BiddingInterests" || form.ColumnDetails[i].RefTableName == "ClaycoBiddingInterests")
                                    {
                                        details.TableReferenceField = "BiddingInterestId";
                                        details.TableRefFieldValueDisplay = "BiddingInterestName";
                                    }

                                }
                                Question.QuestionColumnDetails.Add(details);
                            }

                        }
                }
                else
                {
                    var questionColumnDetails = entityDB.QuestionColumnDetails.FirstOrDefault(rec => rec.QuestionId == form.QuestionId);
                    if (form.isQuesionColumnIdexistsInQuizUserInput != null && form.isQuesionColumnIdexistsInQuizUserInput != true)
                    {
                        var questionRecord = entityDB.Questions.Find(form.QuestionId);
                        questionRecord.IsBold = form.IsBold;
                        questionRecord.Visible = !form.Visible;
                        questionRecord.IsMandatory = form.IsMandatory;
                        entityDB.Entry(questionRecord).State = EntityState.Modified;
                        entityDB.SaveChanges();
                    }
                    else
                    {
                        if (form.ColumnDetails != null)
                            for (int i = 0; i < form.ColumnDetails.Count(); i++)
                            {
                                if (form.IsRepeat)
                                {
                                    if (form.ColumnDetails[0].ControlType == "7" || form.ColumnDetails[0].ControlType == "10")//Radio or MultiSelect
                                    {
                                        if (string.IsNullOrEmpty(form.ColumnDetails[0].Value))
                                        {
                                            errorMessage = "Column 1 ";
                                        }
                                    }
                                    else if (!form.ColumnDetails[0].HasTableReference && form.ColumnDetails[0].ControlType == "9")
                                    {
                                        if (string.IsNullOrEmpty(form.ColumnDetails[0].Value))
                                        {
                                            errorMessage = "Column 1 ";
                                        }
                                    }
                                    questionColumnDetails.ColumnNo = i + 1;
                                    questionColumnDetails.ColumnValues = form.ColumnDetails[0].Value;
                                    questionColumnDetails.DisplayType = Convert.ToInt32(form.ColumnDetails[0].DisplayType);
                                    questionColumnDetails.QuestionControlTypeId = Convert.ToInt32(form.ColumnDetails[0].ControlType);
                                    questionColumnDetails.QuestionId = Question.QuestionID;
                                    if (form.ColumnDetails[0].HasTableReference)
                                    {
                                        questionColumnDetails.ColumnValues = "";
                                        questionColumnDetails.TableReference = form.ColumnDetails[0].RefTableName;
                                        if (form.ColumnDetails[0].RefTableName == "BiddingInterests" || form.ColumnDetails[0].RefTableName == "ClaycoBiddingInterests")
                                        {
                                            questionColumnDetails.TableReferenceField = "BiddingInterestId";
                                            questionColumnDetails.TableRefFieldValueDisplay = "BiddingInterestName";
                                        }

                                    }
                                    entityDB.Entry(questionColumnDetails).State = EntityState.Modified;
                                }
                                else
                                {

                                    if (form.ColumnDetails[i].ControlType == "7" || form.ColumnDetails[i].ControlType == "10")//Radio or MultiSelect
                                    {
                                        if (string.IsNullOrEmpty(form.ColumnDetails[0].Value))
                                        {
                                            errorMessage += " Column " + (i + 1) + ",";
                                        }
                                    }
                                    else if (!form.ColumnDetails[i].HasTableReference && form.ColumnDetails[i].ControlType == "9")
                                    {
                                        if (string.IsNullOrEmpty(form.ColumnDetails[0].Value))
                                        {
                                            errorMessage += " Column " + (i + 1) + ",";
                                        }
                                    }

                                    questionColumnDetails.ColumnNo = i + 1;
                                    questionColumnDetails.ColumnValues = form.ColumnDetails[i].Value;
                                    questionColumnDetails.DisplayType = Convert.ToInt32(form.ColumnDetails[i].DisplayType);
                                    questionColumnDetails.QuestionControlTypeId = Convert.ToInt32(form.ColumnDetails[i].ControlType);
                                    questionColumnDetails.QuestionId = Question.QuestionID;
                                    if (form.ColumnDetails[i].HasTableReference)
                                    {
                                        questionColumnDetails.TableReference = form.ColumnDetails[0].RefTableName;
                                        if (form.ColumnDetails[i].RefTableName == "BiddingInterests" || form.ColumnDetails[i].RefTableName == "ClaycoBiddingInterests")
                                        {
                                            questionColumnDetails.TableReferenceField = "BiddingInterestId";
                                            questionColumnDetails.TableRefFieldValueDisplay = "BiddingInterestName";
                                        }

                                    }
                                    entityDB.Entry(questionColumnDetails).State = EntityState.Modified;
                                }

                            }
                    }
                    // Ends<<<<
                }
            }
            if (errorMessage.Equals(""))
            {
                entityDB.SaveChanges();
                return Redirect("../TemplateTool/PopUpCloseView");
            }
            else
            {

                ModelState.AddModelError("", "Please enter Question Answer and Value");
                return View(form);
            }
        }

        // Kiran on 2/20/2014
        [SessionExpireForView]
        [HttpGet]
        [HeaderAndSidebar(headerName = "QuestionnaireTool", sideMenuName = "QuizBuilder", ViewName = "AddAlternativeVideos")]
        public ActionResult AddAlternativeVideos(Guid templateId)
        {
            LocalAlternativeVideos addalternatevideo = new LocalAlternativeVideos();
            addalternatevideo.TemplateName = entityDB.Templates.FirstOrDefault(rec => rec.TemplateID == templateId).TemplateName;
            return View(addalternatevideo);
        }

        [SessionExpireForView]
        [HttpPost]
        [HeaderAndSidebar(headerName = "QuestionnaireTool", sideMenuName = "QuizBuilder", ViewName = "AddAlternativeVideos")]
        public ActionResult AddAlternativeVideos(HttpPostedFileBase file, LocalAlternativeVideos fileData)
        {
            var templateSectionId = entityDB.TemplateSections.FirstOrDefault(rec => rec.TemplateID == fileData.TemplateId && rec.TemplateSectionType == 21).TemplateSectionID;
            var templateSectionHeaderFooterRecordForCurrentTraining = entityDB.TemplateSectionsHeaderFooter.FirstOrDefault(rec => rec.TemplateSectionId == templateSectionId);
            if (templateSectionHeaderFooterRecordForCurrentTraining == null)
            {
                TemplateSectionsHeaderFooter addCurrentTrainingRecord = new TemplateSectionsHeaderFooter();
                addCurrentTrainingRecord.TemplateSectionId = templateSectionId;
                addCurrentTrainingRecord.PositionType = 1;
                addCurrentTrainingRecord.PositionTitle = "Alternate Video Formats";
                addCurrentTrainingRecord.AttachmentExist = true;

                entityDB.TemplateSectionsHeaderFooter.Add(addCurrentTrainingRecord);
                entityDB.SaveChanges();
            }

            Document addTraining = new Document();

            var documentTypeIdOfQuizVideos = entityDB.DocumentType.FirstOrDefault(rec => rec.DocumentTypeName == "Quiz Videos").DocumentTypeId;
            var templateSectionsHeaderFooterId = entityDB.TemplateSectionsHeaderFooter.FirstOrDefault(rec => rec.TemplateSectionId == templateSectionId).HeaderFooterId;

            addTraining.DocumentTypeId = documentTypeIdOfQuizVideos;
            addTraining.OrganizationId = Convert.ToInt64(Session["currentOrgId"]);
            addTraining.SystemUserAsUploader = (Guid)Session["UserId"];
            addTraining.ReferenceType = "Section Footer";
            addTraining.ReferenceRecordID = templateSectionsHeaderFooterId;
            if (file != null && file.ContentLength > 0)
            {
                var extension = Path.GetExtension(file.FileName);
                addTraining.DocumentName = fileData.FileName + extension;
            }

            addTraining.Uploaded = DateTime.Now;
            entityDB.Document.Add(addTraining);
            entityDB.SaveChanges();

            if (file != null && file.ContentLength > 0)
            {
                //SAVE file
                var fileName = Path.GetFileName(file.FileName);
                var extension = Path.GetExtension(file.FileName);

                Directory.CreateDirectory(Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["QuizVideos"])));
                var path = Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["QuizVideos"]), fileName + extension);
                file.SaveAs(path);

                string uniqueId = addTraining.DocumentId.ToString();
                string oldFilePath = Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["QuizVideos"]), fileName + extension);
                string newFilePath = Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["QuizVideos"]), uniqueId + extension);
                System.IO.File.Move(oldFilePath, newFilePath);
            }
            return Redirect("../TemplateTool/PopUpCloseView");
        }

        public string DeleteClientTraining(Guid documentId, string docName)
        {
            var documentRecord = entityDB.Document.FirstOrDefault(rec => rec.DocumentId == documentId);
            entityDB.Document.Remove(documentRecord);
            entityDB.SaveChanges();

            string documentPhysicalName = documentId.ToString();
            var dotIndex = docName.LastIndexOf('.');
            var extension = docName.Substring(dotIndex);
            var path = Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["QuizVideos"]), documentPhysicalName + extension);
            if (path != null)
            {
                System.IO.File.Delete(path);
            }
            return "TrainingDeletionSuccess";
        }        
    }
}
