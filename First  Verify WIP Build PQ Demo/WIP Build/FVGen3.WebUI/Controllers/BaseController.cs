﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Threading;
using System.Web.Routing;
using FVGen3.WebUI.Annotations;
using FVGen3.WebUI.Models;
using PayPal.Api;
using System.Collections.Generic;
using PayPal.Sample;
using System.Configuration;
using FVGen3.Domain.Entities;
using System.Web.Script.Serialization;
using FVGen3.BusinessLogic;
using FVGen3.WebUI.Utilities.ErrorLogs;
using FVGen3.BusinessLogic.Interfaces;
namespace FVGen3.WebUI.Controllers
{
    [URLEncoder]
    public class BaseController : Controller
    {
       public IUtilityBusiness logs =new UtilityBusiness();
      //  public BaseController(IUtilityBusinessClass Utility)
      //{
          
      //    this.Utility = Utility; 
      //  }
        //private GeneralLogs Logs;
        private SessionModel session;
        public SessionModel SSession { get { return session; } }
        //public GeneralLogs Logs { get { return Logs; } }
        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            if (requestContext.HttpContext.Session != null && requestContext.HttpContext.Session["UserId"] != null)
            {
                session = new SessionModel
                {
                    UserId = new Guid(requestContext.HttpContext.Session["UserId"].ToString()),
                    Role = requestContext.HttpContext.Session["RoleName"].ToString(),
                    OrganizationId = (long)requestContext.HttpContext.Session["currentOrgId"],
                    HeaderMenus = (List<LocalHeaderMenuModel>)requestContext.HttpContext.Session["headerMenus"]
            };
            }
        }
        public SessionModel GetSession()
        {
            return session;
        }
        //public GeneralLogs GetLogs()
        //{
        //    return Logs;
        //}
        public new RedirectToRouteResult RedirectToAction(string action)
        {
            return base.RedirectToAction(action);
        }
        protected override void ExecuteCore()
        {
            string cultureName = null;
            // Attempt to read the culture cookie from Request
            HttpCookie cultureCookie = Request.Cookies["_culture"];
            if (cultureCookie != null)
                cultureName = cultureCookie.Value;
            else
                cultureName = Request.UserLanguages[0]; // obtain it from HTTP header AcceptLanguages

            // Validate culture name
            cultureName = CultureHelper.CultureHelper.GetImplementedCulture(cultureName); // This is safe


            // Modify current thread's cultures            
            Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(cultureName);
            Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;

            base.ExecuteCore();
        }
        protected override void OnException(ExceptionContext filterContext)
        {
            var HideException = ConfigurationManager.AppSettings["HideException"] + "";
            if (!string.IsNullOrEmpty(HideException) && HideException.ToLower().Equals("true"))
            {
                return;
            }
            Exception ex = filterContext.Exception;
            filterContext.ExceptionHandled = true;


            var controller = (BaseController)filterContext.Controller;
            //  filterContext.Result = controller.RedirectToAction("../SystemUsers/ExceptionHandling", model);
            try
            {
                var params1 = Request.QueryString.ToString();
                var pb = new SystemUserBusiness();
                var errorModelWithSession = new ErrorLogUtility(System.Web.HttpContext.Current.Request)
                                            .GetErrorModel(ex, Session["UserId"]);
                var ErrorId = pb.ErrorHandler(errorModelWithSession, params1);
                //var model = new HandleErrorInfo(filterContext.Exception, "SystemUsers", "ExceptionHandling");
                //TempData["alertMessage"] = "Whatever you want to alert the user with ";
                //TempData["ErrorId"] = ErrorId;

                filterContext.Result = new RedirectToRouteResult(
                                   new RouteValueDictionary
                                   {
                                       { "action", "ExceptionHandling" },
                                       { "controller", "SystemUsers" },
                                       {"ErrorId",ErrorId }
                                   });
            }
            catch { }
        }
        public string getPaymentURL(LocalPaypal paypal,string returnURL,string returnParams)
        {
            var apiContext = PayPal.Sample.Configuration.GetAPIContext();

            string payerId = Request.Params["PayerID"];
            var url = "";
            if (string.IsNullOrEmpty(payerId))
            {
                // ###Items
                // Items within a transaction.
                var itemList = new ItemList()
                {
                    items = new List<Item>()
                    {
                        new Item()
                        {
                            name = paypal.item_name,
                            currency = paypal.currency_code,
                            price =Math.Round(Convert.ToDecimal(paypal.amount), 2).ToString() ,
                            quantity = "1",
                           
                        }
                    }
                };

                // ###Payer
                // A resource representing a Payer that funds a payment
                // Payment Method
                // as `paypal`
                var payer = new Payer() { payment_method = "paypal" };

                // ###Redirect URLS
                // These URLs will determine how the user is redirected from PayPal once they have either approved or canceled the payment.
                //var baseURI = Request.Url.Scheme + "://" + Request.Url.Authority + "/PaymentWithPayPal.aspx?";
                var guid = Convert.ToString((new Random()).Next(100000));
                //var redirectUrl = baseURI + "guid=" + guid;
                paypal.@return = returnURL+ QueryStringModule.Encrypt(returnParams + "&guid=" + guid); //to give the access for next sections, passing only preq Template price(0 group)
                var redirUrls = new RedirectUrls()
                {
                    cancel_url = paypal.cancel_return,
                    return_url = paypal.@return,
                };

                // ###Details
                // Let's you specify details of a payment amount.
                var details = new Details()
                {
                    tax = "0",
                    shipping = "0",
                    subtotal = Math.Round(Convert.ToDecimal(paypal.amount), 2).ToString()
                };

                // ###Amount
                // Let's you specify a payment amount.
                var amount = new Amount()
                {
                    currency = paypal.currency_code,
                    total = Math.Round(Convert.ToDecimal(paypal.amount), 2).ToString(), // Total must be equal to sum of shipping, tax and subtotal.
                    details = details
                };

                // ###Transaction
                // A transaction defines the contract of a
                // payment - what is the payment for and who
                // is fulfilling it. 
                var transactionList = new List<Transaction>();

                // The Payment creation API requires a list of
                // Transaction; add the created `Transaction`
                // to a List
                transactionList.Add(new Transaction()
                {
                    description = "Transaction description.",
                    invoice_number = PayPal.Sample.Common.GetRandomInvoiceNumber(),
                    amount = amount,
                    item_list = itemList,
                    notify_url = paypal.notify_url,
                    custom = paypal.custom
                });

                // ###Payment
                // A Payment Resource; create one using
                // the above types and intent as `sale` or `authorize`
                var payment = new Payment()
                {
                    intent = "sale",
                    payer = payer,
                    transactions = transactionList,
                    redirect_urls = redirUrls,

                };
                //this.flow.AddNewRequest("Paypal Payment", payment);
                var json = new JavaScriptSerializer().Serialize(payment);
                // Create a payment using a valid APIContext
                var createdPayment = payment.Create(apiContext);

                // ^ Ignore workflow code segment
                //#region Track Workflow
                //flow.RecordResponse(createdPayment);
                //#endregion
                Session.Add(guid, createdPayment.id);
                // Using the `links` provided by the `createdPayment` object, we can give the user the option to redirect to PayPal to approve the payment.

                var links = createdPayment.links.GetEnumerator();
                while (links.MoveNext())
                {
                    var link = links.Current;
                    if (link.rel.ToLower().Trim().Equals("approval_url"))
                    {
                        url = link.href;
                        //this.flow.RecordRedirectUrl("Redirect to PayPal to approve the payment...", link.href);
                    }
                }
            }
            return url;
        }
    }
}
