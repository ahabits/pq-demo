﻿/*
Page Created Date:  06/11/2013
Created By: Siva Bommisetty
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FVGen3.Domain.Abstract;
using FVGen3.Domain.Entities;
using FVGen3.Domain.Concrete;
using FVGen3.WebUI.Annotations;
using System.Web.Routing;
using System.IO;
using System.Configuration;
using System.Web.Helpers;
using FVGen3.WebUI.Controllers;
using FVGen3.WebUI.Utilities;
using System.Data.Entity;
using System.Text.RegularExpressions;
using AhaApps.Libraries.Extensions;
using FVGen3.BusinessLogic.Interfaces;
using FVGen3.WebUI.Enums;
using Resources;
using FVGen3.WebUI.Constants;
using System.Data.Objects.SqlClient;
using FVGen3.Common;
using FVGen3.DataLayer.DTO;

namespace FVGen3.WebUI.Views.Companies
{
    public class CompaniesController : BaseController
    {
        //
        // GET: /Companies/
        //public ActionResult Index()
        //{
        //    return View();
        //}

        //Constructor to initialize efdbcontext
        EFDbContext entityDB;
        private IPrequalificationBusiness _prequalification;
        private IClientBusiness _Client;
        private IOrganizationBusiness _orgBusiness;
        private ISystemUserBusiness _sysBusiness;
        public CompaniesController(IPrequalificationBusiness prequalification, IClientBusiness _Client, IOrganizationBusiness _org, ISystemUserBusiness _sysBusiness)
        {
            entityDB = new EFDbContext();
            _prequalification = prequalification;
            this._Client = _Client;
            _orgBusiness = _org;
            this._sysBusiness = _sysBusiness;
        }


        //to retrieve templates,section,and subsections
        //takes newDoc form as argument
        //returns templates, sections, subsections as Viewbag elements
        public void getTemplatesAndSectionsAndSubSecs(LocalAddNewDocument form)
        {
            if (form.TemplateID != null)
            {
                ViewBag.Templates = GetTemplateDetails(form.OrganizationID, new Guid(form.TemplateID));
            }

            if (ViewBag.Templates != null && ViewBag.Templates.Count != 0)
            {
                ViewBag.TemplateSections = getSectionDetails(new Guid(form.TemplateID), Convert.ToInt64(form.TemplateSectionID));
            }

            //if (ViewBag.TemplateSections != null && ViewBag.TemplateSections.Count != 0)
            //{
            //    ViewBag.TemplateSubSections = getSubSectionDetails(form.TemplateSectionID, Convert.ToInt64(form.HeadFootSubSecId));
            //}
        }

        //to display addnewdoc view
        //takes orgId as argument
        //retrieves and assigns templates, sections, subsections as veiwbag elements
        //returns documentVeiw to be displayed to the user.
        [SessionExpireForView]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "MyAccount", sideMenuName = "Companies", ViewName = "AddNewDocument")]
        public ActionResult AddNewDocument(long OrganizationID)
        {
            LocalAddNewDocument document = new LocalAddNewDocument();
            document.OrganizationID = OrganizationID;
            document.HeadOrFootOrSubSec = "-1";
            document.IntroOrUnit = "1";
         
            ViewBag.Templates = GetTemplateDetails(OrganizationID, new Guid());

            if (ViewBag.Templates != null && ViewBag.Templates.Count != 0)
            {
                ////getSectionDetails(new Guid(ViewBag.Templates[0].Value), -1);
                Guid templateId = new Guid(ViewBag.Templates[0].Value);
                var section = entityDB.TemplateSections.FirstOrDefault(m => m.TemplateID == templateId && m.TemplateSectionType == 0); // 0 means Intro
                document.TemplateSectionID = section != null ? section.TemplateSectionID.ToString() : "-1";

                ViewBag.BusinessUnits = getBusinessUnits(OrganizationID, 0);
                
            }
            else
            {
                document.TemplateSectionID = "-1";
            }

            bool docTypeNotExist = false;
            List<DocumentType> docType = entityDB.DocumentType.Where(m => m.DocumentTypeName == "Client Uploads").ToList();
            if (docType.Count() == 0)
                docTypeNotExist = true;

            ViewBag.docTypeNotExist = docTypeNotExist;

            return View(document);

        }
        public ActionResult GetPqSections(Guid templateid,long ClientId)
        { var TemplateSectionID = entityDB.TemplateSections.FirstOrDefault(m => m.TemplateID == templateid && m.TemplateSectionType == 31).TemplateSectionID;
           // int positionType = Convert.ToInt16(headOrFootOrSubSec) - 1;
          //  var templateProfiles = from templateProf in entityDB.ClientTemplateProfiles.Where(templateProf => templateProf.TemplateSectionId == TemplateSectionID && templateProf.PositionType == positionType && templateProf.PositionTitle != null && templateProf.PositionTitle != "" && templateProf.ClientId == ClientId).ToList()
                                   //select new SelectListItem
                                   //{
                                   //    Text = templateProf.PositionTitle,
                                   //    Value = templateProf.ClientTemplateProfileId.ToString(),
                                      
                                   //};
           // var profiles = templateProfiles.ToList();

            //SelectListItem otherProfile = new SelectListItem();
            //otherProfile.Text = @Resources.Resources.Companies_Others;
            //otherProfile.Value = "-1";
            //profiles.Insert(profiles.Count, otherProfile);

            return Json("profiles");
            //var subSections = subSecs.ToList();
            //return RedirectToAction("getHeadersOrFootersOrSubSections", new RouteValueDictionary(
            //       new { controller = "Companies", action = "getHeadersOrFootersOrSubSections", templateSecId = TemplateSectionID, HeadFootOrSubSecId=1, headOrFootOrSubSec=1, sectionType=31, clientId = ClientId }));
        
            
        }

        public long getIntroductionSections(Guid templateId)
        {
            long introSecId;
            TemplateSections section = entityDB.TemplateSections.FirstOrDefault(m => m.TemplateID == templateId && m.TemplateSectionType == 0); // 0 means Intro
            if (section != null)
            {
                introSecId = section.TemplateSectionID;
                ViewBag.TemplateSections = section;
            }
            else
                introSecId = -1;

            return introSecId;
        }
        public ActionResult GetClientBusinessUnitSites(long ClientId)
        {

            var BusinessUnitSites = _Client.GetClientBusinessUnitSites(ClientId);



            return Json(BusinessUnitSites, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetTemplates(long PQId)
        {
            return Json(_prequalification.GetPQTemplates(PQId), JsonRequestBehavior.AllowGet);
        }
        //to save newdoc to DB
        //takes docFile and newDoc Form as arguments
        //saves file and record into documenttable and updates templatesectionheaderfooter/subsection table
        //returns successView if successed OR formView in-case of failure
        [SessionExpireForView]
        [HttpPost]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "MyAccount", sideMenuName = "Companies", ViewName = "AddNewDocument")]
        public ActionResult AddNewDocument(HttpPostedFileBase file, LocalAddNewDocument form)
        {
            bool isAddSuccess = false;

            Document doc = new Document();

            ClientBusinessUnits businessUnit;
            if (form.IntroOrUnit.Equals("2"))
            {
                if (!form.ClientBusinessUnitId.Equals("-1")) //observe the !
                {
                    businessUnit = entityDB.ClientBusinessUnits.Find(Convert.ToInt64(form.ClientBusinessUnitId));
                }
                else
                {
                    businessUnit = new ClientBusinessUnits();

                    getBusinessUnits(form.OrganizationID,0);
                    ModelState.AddModelError("ClientBusinessUnitId", "*");
                }
               

                doc.OrganizationId = form.OrganizationID;
               
                   

                    doc.ReferenceType = @Resources.Resources.Common_BusinessUnit;
                
                    doc.ReferenceRecordID = businessUnit.ClientBusinessUnitId;

                   // doc.ReferenceType = @Resources.Resources.Doc_ClientBusinessUnitSites;
               
                    doc.Uploaded = DateTime.Now;
                doc.Expires = DateTime.Now;
                doc.SystemUserAsUploader = (Guid)Session["UserId"];

                if (file != null && file.ContentLength > 0)
                {
                    var extension = Path.GetExtension(file.FileName);
                    doc.DocumentName = form.fileTitle + extension;
                }

                DocumentType typeOfDoc = entityDB.DocumentType.FirstOrDefault(m => m.DocumentTypeName == @Resources.Resources.Companies_ClientDocs);
                if (typeOfDoc != null)
                {
                    typeOfDoc.Documents.Add(doc);

                    entityDB.Entry(typeOfDoc).State = EntityState.Modified;
                    entityDB.SaveChanges();

                    isAddSuccess = true;
                }

            }
            else if (form.IntroOrUnit == "3")
            {
                ClientBusinessUnitSites businessUnitSite;
                if (!form.ClientBusinessUnitId.Equals("-1")) //observe the !
                {
                    businessUnitSite = entityDB.ClientBusinessUnitSites.Find(form.ClientBusinessUnitsiteId);
                }
                else
                {
                    businessUnitSite = new ClientBusinessUnitSites();

                    getBusinessUnits(form.OrganizationID, 0);
                    ModelState.AddModelError("BusinessUnitsiteId", "*");
                }
                doc.OrganizationId = form.OrganizationID;

                doc.ReferenceRecordID = form.ClientBusinessUnitsiteId;

                doc.ReferenceType = @Resources.Resources.Doc_ClientBusinessUnitSites;

                doc.Uploaded = DateTime.Now;
                doc.Expires = DateTime.Now;
                doc.SystemUserAsUploader = (Guid)Session["UserId"];

                if (file != null && file.ContentLength > 0)
                {
                    var extension = Path.GetExtension(file.FileName);
                    doc.DocumentName = form.fileTitle + extension;
                }

                DocumentType typeOfDoc = entityDB.DocumentType.FirstOrDefault(m => m.DocumentTypeName == @Resources.Resources.Companies_ClientDocs);
                if (typeOfDoc != null)
                {
                    typeOfDoc.Documents.Add(doc);

                    entityDB.Entry(typeOfDoc).State = EntityState.Modified;
                    entityDB.SaveChanges();

                    isAddSuccess = true;
                }
            }
            else //Headers or Footers
            {
                ClientTemplateProfiles newProfile;
                if (!form.HeadFootSubSecId.Equals("-1"))
                {
                    newProfile = entityDB.ClientTemplateProfiles.Find(Convert.ToInt64(form.HeadFootSubSecId));
                    if (newProfile != null)
                    {
                        newProfile.AttachmentExist = true;

                        entityDB.Entry(newProfile).State = EntityState.Modified;
                    }
                }
                else
                {
                    newProfile = new ClientTemplateProfiles();

                    newProfile.PositionTitle = form.headerOrFooterTitle;
                    newProfile.PositionContent = "";
                    newProfile.AttachmentExist = true;
                    newProfile.AttachmentType = 0;
                    newProfile.ClientId = form.OrganizationID;
                    newProfile.ContentType = 0;  // Suresh on 19th Dec 2013
                    if (form.IntroOrUnit == "4") {
                        newProfile.TemplateSectionId = entityDB.TemplateSections.FirstOrDefault(r => r.TemplateID == new Guid(form.TemplateID) && r.TemplateSectionType == 31).TemplateSectionID;
                    }
                    else { newProfile.TemplateSectionId = Convert.ToInt64(form.TemplateSectionID); }

                    if (form.HeadOrFootOrSubSec.Equals("1"))
                        newProfile.PositionType = 0;
                    if (form.HeadOrFootOrSubSec.Equals("2"))
                        newProfile.PositionType = 1;

                    entityDB.ClientTemplateProfiles.Add(newProfile);
                }

                entityDB.SaveChanges();

                doc.ReferenceRecordID = newProfile.ClientTemplateProfileId;
                if (form.HeadOrFootOrSubSec.Equals("1") || form.HeadOrFootOrSubSec.Equals("2"))
                    doc.ReferenceType = "Client Profile";



                doc.OrganizationId = form.OrganizationID;

                if (file != null && file.ContentLength > 0)
                {
                    var extension = Path.GetExtension(file.FileName);
                    doc.DocumentName = form.fileTitle + extension;
                }

                doc.Uploaded = DateTime.Now;
                doc.Expires = DateTime.Now;
                doc.SystemUserAsUploader = (Guid)Session["UserId"];
                doc.EmailSent = false;
                doc.OrderBy = form.displayOrder;
                DocumentType typeOfDoc = entityDB.DocumentType.FirstOrDefault(m => m.DocumentTypeName == @Resources.Resources.Companies_ClientDocs);
                if (typeOfDoc != null)
                {
                    typeOfDoc.Documents.Add(doc);

                    entityDB.Entry(typeOfDoc).State = EntityState.Modified;
                    entityDB.SaveChanges();

                    isAddSuccess = true;
                }
            }

            if (isAddSuccess)
            {
                if (file != null && file.ContentLength > 0)
                {
                    //SAVE file
                    var fileName = Path.GetFileName(file.FileName);
                    var extension = Path.GetExtension(file.FileName);

                    Directory.CreateDirectory(Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["ClientUploadsPath"])));
                    var path = Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["ClientUploadsPath"]), fileName + extension);
                    file.SaveAs(path);

                    string uniqueId = doc.DocumentId.ToString();
                    string oldFilePath = Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["ClientUploadsPath"]), fileName + extension);
                    string newFilePath = Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["ClientUploadsPath"]), uniqueId + extension);
                    System.IO.File.Move(oldFilePath, newFilePath);

                    return View("AddNewDocSuccess", doc);
                }
                else
                {
                    ModelState.AddModelError("file", "*");
                }
            }
            //}
            //else
            //{
            //getTemplatesAndSectionsAndSubSecs(form);
            //}

            return View(form);
        }

        public ActionResult EditFinancialMonthAndDate(long OrganizationID)
        {
            LocalAddNewCompany form = new LocalAddNewCompany();
            form.OrganizationID = OrganizationID;
            var org = entityDB.Organizations.Find(OrganizationID);
            form.FinancialMonth = org.FinancialMonth;
            form.FinancialDate = org.FinancialDate;
            return View(form);
        }
        [HttpPost]
        public ActionResult EditFinancialMonthAndDate(long OrganizationID, LocalAddNewCompany form)
        {
            var HasOrganization = entityDB.Organizations.Find(form.OrganizationID);
            if (HasOrganization != null)
            {
                HasOrganization.FinancialMonth = form.FinancialMonth;
                HasOrganization.FinancialDate = form.FinancialDate;
                entityDB.Entry(HasOrganization).State = EntityState.Modified;
                var HasFinancialData = entityDB.FinancialAnalyticsModel.Where(r => r.VendorId == form.OrganizationID).ToList();
                if (HasFinancialData != null)
                {

                    foreach (var finance in HasFinancialData)
                    {

                        var financial = new DateTime(finance.DateOfFinancialStatements.Value.Year, form.FinancialMonth.Value, form.FinancialDate.Value);
                        finance.DateOfFinancialStatements = financial;
                        entityDB.Entry(finance).State = EntityState.Modified;
                    }
                }
                var HasFinancialLog = entityDB.FinancialAnalyticsLog.Where(r => r.VendorId == form.OrganizationID).ToList();
                if (HasFinancialLog != null)
                {

                    foreach (var financelog in HasFinancialLog)
                    {
                        //var year = finance.DateOfFinancialStatements.Value.Year
                        var FinanicalDate = new DateTime(financelog.DateOfFinancialStatements.Value.Year, form.FinancialMonth.Value, form.FinancialDate.Value);
                        financelog.DateOfFinancialStatements = FinanicalDate;
                        entityDB.Entry(financelog).State = EntityState.Modified;
                    }
                }
                entityDB.SaveChanges();
            }
            return Redirect("~/QuizTemplateBuilder/PopUpCloseView");
        }

        //to delete document in ClientEdit View
        //takes 6 args which are necessary to delete a document
        //check and deletes doc from fileSystem and doc table, and check and updates headerfooter/subsection table with attachExist = false, if no.of docs == 0
        //returns success/failure message
        [SessionExpireForView]
        [HttpPost]
        public string deleteDocument(Guid documentId, string refType, long refRecordId, long orgId, string idAsFileName, string userEnteredFilename)
        {
            Document document = entityDB.Document.Find(documentId);
            if (document != null)
            {
                var dotIndex = userEnteredFilename.LastIndexOf('.');
                var extension = userEnteredFilename.Substring(dotIndex);
                var path = Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["ClientUploadsPath"]), idAsFileName + extension);
                if (System.IO.File.Exists(path))
                {
                    System.IO.File.Delete(path);

                    entityDB.Document.Remove(document);
                    entityDB.SaveChanges();

                    //update SubSection
                    if (refType.Equals(@Resources.Resources.Companies_SubSec))
                    {
                        List<Document> docsInTheSameSubSection = entityDB.Document.Where(m => m.ReferenceRecordID == refRecordId && m.ReferenceType == refType).ToList();//Rajesh on 8/12/2014
                        if (docsInTheSameSubSection.Count == 0)
                        {
                            TemplateSubSections subSec = entityDB.TemplateSubSections.Find(refRecordId);

                            //on Nov 5th
                            //if (subSec.Questions.Count == 0)
                            //{
                            //entityDB.TemplateSubSections.Remove(subSec);
                            //}
                            //else
                            //{
                            subSec.AttachmentExist = false;
                            entityDB.Entry(subSec).State = EntityState.Modified;
                            //}
                            entityDB.SaveChanges();
                        }
                    }
                    else //update HeaderFooter..
                    {
                        List<Document> docsWithSameHeadOrFoot = entityDB.Document.Where(m => m.ReferenceRecordID == refRecordId && m.ReferenceType == refType).ToList();
                        if (docsWithSameHeadOrFoot.Count == 0)
                        {
                            if (refType.Equals("Client Profile"))
                            {
                                ClientTemplateProfiles profile = entityDB.ClientTemplateProfiles.Find(refRecordId);

                                //on Nov 5th
                                if (profile.PositionContent.Equals("") || profile.PositionContent == null)
                                {
                                    entityDB.ClientTemplateProfiles.Remove(profile);
                                }
                                else
                                {
                                    profile.AttachmentExist = false;
                                    entityDB.Entry(profile).State = EntityState.Modified;

                                }
                            }
                            else if (refType.Equals("Section Footer") || refType.Equals("Section Header"))
                            {
                                TemplateSectionsHeaderFooter secHeadFoot = entityDB.TemplateSectionsHeaderFooter.Find(refRecordId);

                                //on Nov 5th
                                //if (secHeadFoot.PostionContent.Equals("") || secHeadFoot.PostionContent == null)
                                //{
                                //    entityDB.TemplateSectionsHeaderFooter.Remove(secHeadFoot);
                                //}
                                //else
                                //{
                                secHeadFoot.AttachmentExist = false;
                                entityDB.Entry(secHeadFoot).State = EntityState.Modified;
                                //}
                            }

                            entityDB.SaveChanges();
                        }
                    }

                    return "DeleteSuccess";
                }

                return "DeleteFailed";
            }
            return "DeleteFailed";
        }
       
        public List<SelectListItem> getBusinessUnits(long clientIdLocal, long businessUnitIdLocal)
        {
            var clientOrg = entityDB.Organizations.Find(clientIdLocal);
            var SubClients =new List<long>();
            if (clientOrg.OrganizationType == OrganizationType.SuperClient)
            {
                 SubClients = _Client.GetSubClients(clientIdLocal).Select(r => long.Parse(r.Key)).ToList();
            }
            // Kiran on 10/29/2014
            var units = from bunits in entityDB.ClientBusinessUnits.Where(m => (m.ClientId == clientIdLocal || SubClients.Contains(m.ClientId)) && m.ClientId == clientIdLocal).OrderBy(m => m.BusinessUnitName).ToList()
                        select new SelectListItem
                        {
                            Text = bunits.BusinessUnitName,
                            Value = bunits.ClientBusinessUnitId.ToString(),
                            Selected = bunits.ClientBusinessUnitId == businessUnitIdLocal,

                        };
            var businessUnits = units.ToList();
            return businessUnits;
        }


        ///////////////////////////////////////////////////////////////////////////
        //to download document
        //idAsFilename is the name of the file
        //filename is the name-of-file-as-entered-by-user, and is used to get file-extension
        //clientId is id of the client to which this doc belongs to.
        [SessionExpire]
        public ActionResult DownloadDoc(string idAsFileName, string userEnteredFilename, long clientId)
        {
            try
            {
                var dotIndex = userEnteredFilename.LastIndexOf('.');
                var extension = userEnteredFilename.Substring(dotIndex);

                // Kiran on 12/16/2014
                string invalidChars = Regex.Escape(new string(System.IO.Path.GetInvalidFileNameChars()));
                string invalidReStr = string.Format(@"[{0}]+", invalidChars);
                userEnteredFilename = Regex.Replace(userEnteredFilename, invalidReStr, "_").Replace(";", "").Replace(",", "");
                // Ends<<<

                //var filePath = HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["ClientUploadsPath"]) + "/" + idAsFileName + extension;
                var filePath = Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["ClientUploadsPath"]), idAsFileName + extension);
                if (System.IO.File.Exists(filePath))
                {
                    var fs = System.IO.File.OpenRead(filePath);
                    //return File(fs, "file", userEnteredFilename);
                    Response.AppendHeader("Content-Disposition", "inline; filename=" + userEnteredFilename + ".pdf");
                    return File(fs, "application/pdf");
                }
                else
                {
                    return View("DocDownloadFailed");
                }
            }
            catch
            {
                throw new HttpException(404, @Resources.Resources.EmailTemplatesCouldNotFound + userEnteredFilename);
            }
        }
        public ActionResult DownloadPQDoc(string idAsFileName, string userEnteredFilename)
        {
            try
            {
                var dotIndex = userEnteredFilename.LastIndexOf('.');
                var extension = userEnteredFilename.Substring(dotIndex);

                // Kiran on 12/16/2014
                string invalidChars = Regex.Escape(new string(System.IO.Path.GetInvalidFileNameChars()));
                string invalidReStr = string.Format(@"[{0}]+", invalidChars);
                userEnteredFilename = Regex.Replace(userEnteredFilename, invalidReStr, "_").Replace(";", "").Replace(",", "");
                // Ends<<<

                //var filePath = HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["ClientUploadsPath"]) + "/" + idAsFileName + extension;
                var filePath = Path.Combine(ConfigurationSettings.AppSettings["PQDocumentsPath"], idAsFileName + extension);
                if (System.IO.File.Exists(filePath))
                {
                    var fs = System.IO.File.OpenRead(filePath);
                    //return File(fs, "file", userEnteredFilename);
                    Response.AppendHeader("Content-Disposition", "inline; filename=" + userEnteredFilename + ".pdf");
                    return File(fs, "application/pdf");
                }
                else
                {
                    return View("DocDownloadFailed");
                }
            }
            catch
            {
                throw new HttpException(404, @Resources.Resources.EmailTemplatesCouldNotFound + userEnteredFilename);
            }
        }
        [SessionExpire]
        public ActionResult DownloadAlternateFormat(string idAsFileName, string userEnteredFilename, long clientId)
        {
            try
            {
                var dotIndex = userEnteredFilename.LastIndexOf('.');
                var extension = userEnteredFilename.Substring(dotIndex);

                // Kiran on 12/16/2014
                string invalidChars = Regex.Escape(new string(System.IO.Path.GetInvalidFileNameChars()));
                string invalidReStr = string.Format(@"[{0}]+", invalidChars);
                userEnteredFilename = Regex.Replace(userEnteredFilename, invalidReStr, "_").Replace(";", "").Replace(",", "");
                // Ends<<<

                //var filePath = HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["ClientUploadsPath"]) + "/" + idAsFileName + extension;
                var filePath = Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["TrainingVideosPath"]), idAsFileName + extension);
                if (System.IO.File.Exists(filePath))
                {
                    var fs = System.IO.File.OpenRead(filePath);
                    //return File(fs, "file", userEnteredFilename);
                    Response.AppendHeader("Content-Disposition", "inline; filename=" + userEnteredFilename + ".pdf");
                    return File(fs, "application/pdf");
                }
                else
                {
                    return View("DocDownloadFailed");
                }
            }
            catch
            {
                throw new HttpException(404, @Resources.Resources.EmailTemplatesCouldNotFound + userEnteredFilename);
            }
        }




        //to get List of templates
        //takes templateid to be selected by default in drop-down list, and orgid as parameters
        //returns list of templates
        public List<SelectListItem> GetTemplateDetails(long orgId, Guid templateId)
        {
            OrganizationData clientOrg = _orgBusiness.GetOrganization(orgId);
           
            bool includeEri = clientOrg.OrganizationType == OrganizationType.SuperClient ? true:false;
            var clientTempsTemp = (from clientTemp in entityDB.ClientTemplates.Where(m => m.ClientID == orgId && m.Templates.TemplateType == 0 && m.Templates.IsERI == includeEri
                                   && m.Templates.TemplateStatus == 1)
                                   select new 
                                   {
                                       Text = clientTemp.Templates.TemplateName,
                                       Value = clientTemp.Templates.TemplateID,
                                       
                                   }).Distinct().ToList().Select(r=>new SelectListItem() {
                                       Text=r.Text,
                                       Value=r.Value.ToString(),
                                       Selected = r.Value == templateId
                                   }).ToList();

            return clientTempsTemp;

        }

        //to get List of sections
        //takes templateSectionId to be selected by default in drop-down list, and templateId as parameters
        //returns list of templatesections
        //Siva on 21st Sep
        public List<SelectListItem> getSectionDetails(Guid templateId, long templateSectionId)
        {
            //var sects = from secs in entityDB.TemplateSections.Where(m => m.TemplateSectionType != 3 || m.TemplateSectionType == null).OrderBy(m => m.DisplayOrder).ToList()
            //            where secs.TemplateID == templateId
            //            select new SelectListItem
            //            {
            //                Text = secs.SectionName,
            //                Value = secs.TemplateSectionID.ToString() + "," + secs.TemplateSectionType,
            //                Selected = secs.TemplateSectionID == templateSectionId,

            //            };

            var sects = from secs in entityDB.TemplateSections.Where(m => (m.TemplateSectionType != 3 || m.TemplateSectionType == null) && m.TemplateID == templateId).OrderBy(m => m.DisplayOrder).ToList()
                        select new SelectListItem
                        {
                            Text = secs.SectionName,
                            Value = secs.TemplateSectionID.ToString() + "," + secs.TemplateSectionType,
                            Selected = secs.TemplateSectionID == templateSectionId,

                        };
            var tempsecs = sects.ToList();
            return tempsecs;
        }
        //End on 23rd Aug

        //to get List of subsections
        //takes templateSectionId to be selected by default in drop-down list, and templateId as parameters
        //returns list of subsections
        public List<SelectListItem> getSubSectionDetails(long templateSectionId, long templateSubSectionId)
        {
            var subSections = from secs in entityDB.TemplateSubSections.Where(rec => rec.TemplateSectionID == templateSectionId).ToList()
                              select new SelectListItem
                              {
                                  Text = secs.SubSectionName,
                                  Value = secs.SubSectionID.ToString(),
                                  Selected = secs.SubSectionID == templateSubSectionId
                              };
            var tempsubSections = subSections.ToList();
            return tempsubSections;
        }

        //to get sections
        //takes templateSectionId as parameter
        //returns list of templatesections
        public ActionResult getSections(Guid templateId)
        {

            var tempsecs = getSectionDetails(templateId, -1);
            if (tempsecs.Count == 0)
            {
                return Json(new List<SelectListItem>());
            }
            return Json(tempsecs);
        }

        //to get subsections
        //takes templateSectionId as parameter
        //returns list of subsections as json object
        public ActionResult getSubSections(long templateSecId)
        {
            var tempsubSections = getSubSectionDetails(templateSecId, -1);
            return Json(tempsubSections);
        }

        //to get header/footer/subsections 
        //takes HeadFootOrSubSecId to be selected by default in drop-down list as parameter, templateid and headorfoot for retrieving head/footer/subsections
        //returns list of headers/footers/subsections as json object
        public ActionResult getHeadersOrFootersOrSubSections(long templateSecId, int HeadFootOrSubSecId, int headOrFootOrSubSec, string sectionType, long clientId)
        {
            if (sectionType=="31")
            {
                var templateid = entityDB.TemplateSections.FirstOrDefault(r => r.TemplateSectionID == templateSecId).TemplateID;
                templateSecId = entityDB.TemplateSections.FirstOrDefault(r => r.TemplateID == templateid && r.TemplateSectionType==31).TemplateSectionID;
             
            }
            if (headOrFootOrSubSec == 3) //Means SubSections
            {
                var subSecs = from subsec in entityDB.TemplateSubSections.Where(subsec => subsec.TemplateSectionID == templateSecId && subsec.SubSectionName != null && subsec.SubSectionName != "").ToList()
                              select new SelectListItem
                              {
                                  Text = subsec.SubSectionName,
                                  Value = subsec.SubSectionID.ToString(),
                                  Selected = subsec.SubSectionID == HeadFootOrSubSecId
                              };
                var subSections = subSecs.ToList();

                //on Nov 5th
                //SelectListItem otherSubSec = new SelectListItem();
                //otherSubSec.Text = @Resources.Resources.Companies_Others;
                //otherSubSec.Value = "-1";
                //subSections.Insert(subSections.Count, otherSubSec);

                return Json(subSections);
            }
            else
            {
                if (sectionType.Equals("0")|| sectionType.Equals("31")) //Means Introduction section (Company Profile)
                {
                    int positionType = Convert.ToInt16(headOrFootOrSubSec) - 1;
                    var templateProfiles = from templateProf in entityDB.ClientTemplateProfiles.Where(templateProf => templateProf.TemplateSectionId == templateSecId && templateProf.PositionType == positionType && templateProf.PositionTitle != null && templateProf.PositionTitle != "" && templateProf.ClientId == clientId).ToList()
                                           select new SelectListItem
                                           {
                                               Text = templateProf.PositionTitle,
                                               Value = templateProf.ClientTemplateProfileId.ToString(),
                                               Selected = templateProf.ClientTemplateProfileId == HeadFootOrSubSecId
                                           };
                    var profiles = templateProfiles.ToList();

                    SelectListItem otherProfile = new SelectListItem();
                    otherProfile.Text = @Resources.Resources.Companies_Others;
                    otherProfile.Value = "-1";
                    profiles.Insert(profiles.Count, otherProfile);

                    return Json(profiles);
                }
                else //Means other sections
                {
                    int headOrFootType = Convert.ToInt16(headOrFootOrSubSec) - 1;
                    var headFoots = from headerOrFooter in entityDB.TemplateSectionsHeaderFooter.Where(headerOrFooter => headerOrFooter.TemplateSectionId == templateSecId && headerOrFooter.PositionType == headOrFootType && headerOrFooter.PositionTitle != null && headerOrFooter.PositionTitle != "").ToList()
                                    select new SelectListItem
                                    {
                                        Text = headerOrFooter.PositionTitle,
                                        Value = headerOrFooter.HeaderFooterId.ToString(),
                                        Selected = headerOrFooter.HeaderFooterId == HeadFootOrSubSecId
                                    };
                    var headersOrFooters = headFoots.ToList();

                    //on Nov 5th
                    //SelectListItem otherHeadOrFoot = new SelectListItem();
                    //otherHeadOrFoot.Text = @Resources.Resources.Companies_Others;
                    //otherHeadOrFoot.Value = "-1";
                    //headersOrFooters.Insert(headersOrFooters.Count, otherHeadOrFoot);

                    return Json(headersOrFooters);
                }
            }
        }
        public JsonResult GetTemplateData(long ClientId)
        {
          var  MultipleInvitationCode = entityDB.MultipleInvitationCode.Where(r => r.ClientId == ClientId).ToList();
            return Json(MultipleInvitationCode, JsonRequestBehavior.AllowGet);
        }

        public string Updatekey(long organizationid)
        {
            var key = FVGen3.Common.EncryptionDecryption.GetUniqueKey(10);
            var Regeneratekey = _Client.UpdateKey(organizationid, key);


            return "success";
        }
        public string Templatekey(long organizationid,Guid TemplateId)
        {
            var key = FVGen3.Common.EncryptionDecryption.GetUniqueKey(12);
            var Regeneratekey = _Client.UpdateTemplateKey(organizationid, key, TemplateId);


            return "success";
        }
      
        public string deleteTemplateKey(long organizationid,Guid TemplateId)
        {
           // var Template = new Guid(TemplateId);
            return _Client.DeleteTemplateKey(organizationid, TemplateId);
        }
        ////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////
        // CLIENT
        //to display editclient view
        //takes orgid, and isSuccess as parameters
        //isSuccess is for displaying/hiding saveSuccess message
        //returns addnewcompany veiw.
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "MyAccount", sideMenuName = "Companies", ViewName = "EditClient")]
        [SessionExpire(lastPageUrl = "~/SystemUsers/DashBoard")]
        public ActionResult EditClient(long OrganizationID, bool isSuccess)
        {
            ViewBag.IsSubClient = _orgBusiness.IsSubClient(OrganizationID);
         
            ViewBag.OrganizationID = OrganizationID;
            ViewBag.Countries = _orgBusiness.GetCountries().Select(r => new SelectListItem() { Text = r.Value, Value = r.Value }).ToList();
            LocalAddNewCompany newClient;
          
            OrganizationData newOrganization = _orgBusiness.GetOrganization(OrganizationID);

            newClient = new LocalAddNewCompany();
            newClient.OrganizationID = OrganizationID;
            newClient.OrganizationType = newOrganization.OrganizationType;
            
            newClient.Name = newOrganization.Name;
            newClient.Address1 = newOrganization.Address1;
            newClient.Address2 = newOrganization.Address2;
            newClient.Address3 = newOrganization.Address3;
            newClient.City = newOrganization.City;
            newClient.State = newOrganization.State;

            newClient.Zip = newOrganization.Zip;
            newClient.Country = newOrganization.Country;
            newClient.PhoneNumber = newOrganization.PhoneNumber;
            newClient.ExtNumber = newOrganization.ExtNumber;
            newClient.FaxNumber = newOrganization.FaxNumber;
            newClient.WebsiteURL = newOrganization.WebsiteURL;
            newClient.FederalIDNumber = newOrganization.FederalIDNumber;
            newClient.isSuccess = isSuccess;
            newClient.CountryType = newOrganization.CountryType;
            newClient.SingleClientCode = newOrganization.SingleClientCode;
            newClient.ClientSecrte = newOrganization.ClientSecret;

            newClient.ContractorEvaluationExpirationPeriod = newOrganization.ContractorEvaluationExpirationPeriod;
            newClient.OrgRepresentativeName = newOrganization.OrgRepresentativeName;
            newClient.OrgRepresentativeEmail = newOrganization.OrgRepresentativeEmail;
            newClient.OrgInfusionSoftContactId = newOrganization.OrgInfusionSoftContactId;
            
            if (newOrganization.IsClientInviteOnly != null)
                newClient.AcceptVendorsViaInvite = (bool)newOrganization.IsClientInviteOnly;
            else
                newClient.AcceptVendorsViaInvite = false;
            
            if (newOrganization.TrainingRequired != null)
                newClient.TrainingRequired = (bool)newOrganization.TrainingRequired;
            else
                newClient.TrainingRequired = false;
            
            if (newOrganization.TrainingFeeCap != null)
            {
                newClient.TrainingFeeCap = newOrganization.TrainingFeeCap.Value.ToString("0.##");
            }
            else
            {
                newClient.TrainingFeeCap = "0.00";
            }

            newClient.TrainingCertificate = (newOrganization.TrainingCertificate.HasValue && newOrganization.TrainingCertificate.Value);

            if (newOrganization.OrganizationType.Equals(OrganizationType.SuperClient))
            {
               
                var subClients = _Client.GetSubClients(OrganizationID,false).Select(rec => rec.Key).ToList();
                var restrictedClient = _Client.GetRestrictedClients(OrganizationID);
                var clientsInfo = _Client.GetClients().Where(r => !restrictedClient.Contains(long.Parse(r.Key)));
                newClient.clients =  clientsInfo.Select(clients=> new SelectListItem
                                     {
                                         Text = clients.Value,
                                         Value = clients.Key.ToString(),
                                         Selected = subClients.Contains(clients.Key.ToString())
                                     }).ToList();
             
                var clientIds = clientsInfo.Select(r => long.Parse(r.Key));
                
                newClient.DisabledClients = _Client.GetClientsFromClientTemplates(clientIds.ToList());
                //if (disabledClients != null) ViewBag.disabledClients = disabledClients;

            }

            ViewBag.Templates = GetTemplateDetails(OrganizationID, new Guid());
            ViewBag.States = _orgBusiness.GetCountryStates(newClient.Country).Select(r => new SelectListItem() { Text = r.Value, Value = r.Value }).ToList();


            //  List< MultipleInvitationCode> MultipleCode= new List<MultipleInvitationCode>();
           

        
            return View(newClient);
        }

        //to save changes in editClient view
        //takes addnewclient form as argument
        //saves and returns addnewclient form with isSuccess = true
        //Updated on 23rd July
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "MyAccount", sideMenuName = "Companies", ViewName = "EditClient")]
        [SessionExpire(lastPageUrl = "~/SystemUsers/DashBoard")]
        [HttpPost]
        public ActionResult EditClient(HttpPostedFileBase ImageUrl, LocalAddNewCompany form) //Added ImageUrl on 23rd July
        {
            ViewBag.IsSubClient = _orgBusiness.IsSubClient(form.OrganizationID);
            ModelState.Remove("PrincipalCompanyOfficerName");//This Field is used for vendor so we no need to check for validation
            ModelState.Remove("CopyAnswers");
            ModelState.Remove("Language");
            ModelState.Remove("GeographicArea");
            ModelState.Remove("TaxID");//Rajesh on 8/12/2014
            ViewBag.Countries = _orgBusiness.GetCountries().Select(r => new SelectListItem() { Text = r.Value, Value = r.Value }).ToList();
            ViewBag.States = _orgBusiness.GetCountryStates(form.Country).Select(r => new SelectListItem() { Text = r.Value, Value = r.Value }).ToList();
            // kiran on 03/10/2015 for handling logo validations in IE
           
            List<LocalImages> images = new List<LocalImages>();
            if (Request.Files != null && Request.Files.Count > 0)
            {
                for (int i = 0; i < Request.Files.Count; i++)
                {
                    string file = Request.Files.AllKeys[i];
                    if (Request.Files[i].ContentLength == 0) continue;

                    var image = WebImage.GetImageFromRequest(file);
                    LocalImages localImage = new LocalImages();
                    localImage.Name = file;
                    localImage.Image = image;
                    images.Add(localImage);

                    if (image != null)
                    {
                        var extension = Path.GetExtension(image.FileName);
                        if (!extension.Equals(".png") && i == 0)
                        {
                            ModelState.AddModelError("Logo", "Only PNG files can be uploaded");
                        }
                        if (!extension.Equals(".png") && i == 1)
                        {
                            ModelState.AddModelError("Photo", "Only PNG files can be uploaded");
                        }
                        var fileSize = image.GetBytes().Length / 1024;

                        if (fileSize > 30 && i == 0)
                        {
                            ModelState.AddModelError("Logo", "File size can not be more than 30KB, please choose another file");
                        }
                        if (fileSize > 30 && i == 1)
                        {
                            ModelState.AddModelError("Photo", "File size can not be more than 30KB, please choose another file");
                        }
                        if ((image.Width > 250 || image.Height > 250) && i == 0)
                        {
                            ModelState.AddModelError("Logo", "Logo dimensions can not be more than 250 X 250 pixels");
                        }
                    }
                }
            }
           
            // kiran on 03/10/2015 for handling logo validations in IE Ends<<<

            // If valid, save to database
            if (ModelState.IsValid)
            {
                OrganizationData newOrganization;
                if (false)
                {
                    newOrganization = new OrganizationData();
                }
                else
                {
                    newOrganization = _orgBusiness.GetOrganization(form.OrganizationID);
                }

                newOrganization.Name = form.Name;
                newOrganization.Address1 = form.Address1;
                newOrganization.Address2 = form.Address2;
                newOrganization.Address3 = form.Address3; 
                newOrganization.City = form.City;
                newOrganization.State = form.State;
                newOrganization.CountryType = form.CountryType;
                newOrganization.Zip = form.Zip;
                newOrganization.Country = form.Country;
                newOrganization.PhoneNumber = form.PhoneNumber;
                newOrganization.ExtNumber = form.ExtNumber; 
                newOrganization.FaxNumber = form.FaxNumber;
                newOrganization.WebsiteURL = form.WebsiteURL;
                newOrganization.FederalIDNumber = form.FederalIDNumber;
                newOrganization.IsClientInviteOnly = form.AcceptVendorsViaInvite;  
                newOrganization.TrainingRequired = form.TrainingRequired; 
                newOrganization.TrainingFeeCap = form.TrainingFeeCap.ToDecimalNullSafe(0.00m); 
                newOrganization.UpdateDateTime = DateTime.Now; 
                newOrganization.TrainingCertificate = form.TrainingCertificate;

                newOrganization.ContractorEvaluationExpirationPeriod = form.ContractorEvaluationExpirationPeriod;
                newOrganization.OrgRepresentativeName = form.OrgRepresentativeName;
                newOrganization.OrgRepresentativeEmail = form.OrgRepresentativeEmail;
                newOrganization.OrgInfusionSoftContactId = form.OrgInfusionSoftContactId;


                long newOrgId = _orgBusiness.PostOrganization(newOrganization, form.OrganizationID);
                if (newOrganization.OrganizationType.Equals(OrganizationType.SuperClient))
                {
                    foreach (var subClient in form.clients.Where(r => r.Selected == true))
                    {
                        long subClientId = Convert.ToInt64(subClient.Value);
                        _orgBusiness.PostSubClient(form.OrganizationID, subClientId);
                    }

                    foreach (var subClient in form.clients.Where(r => r.Selected == false))
                    {
                        long subClientId = Convert.ToInt64(subClient.Value);
                        _orgBusiness.PostSubClient(form.OrganizationID, subClientId);
                    }
                }

                //entityDB.SaveChanges();

                form.OrganizationID = newOrgId;
                
                foreach (var image in images)
                {
                    if (image != null)
                    {
                        if (image.Image.Width > 500)
                        {
                            image.Image.Resize(500, ((500 * image.Image.Height) / image.Image.Width));
                        }

                        var extension = Path.GetExtension(image.Image.FileName);
                        string logoId = form.OrganizationID.ToString();

                        if (image.Name.Equals("Logo"))
                        {
                            Directory.CreateDirectory(Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["LogoUploadPath"])));
                            var path = Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["LogoUploadPath"]), logoId + extension);
                            image.Image.Save(path);
                        }

                        else
                        {
                            Directory.CreateDirectory(Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["PhotoUploadPath"])));
                            var path = Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["PhotoUploadPath"]), logoId + extension);
                            image.Image.Save(path);
                        }
                    }
                }
                // Kiran on 03/11/2015 Ends<<<

                return RedirectToAction("EditClient", new RouteValueDictionary(
                    new { controller = "Companies", action = "EditClient", OrganizationID = form.OrganizationID, isSuccess = true }));

            }

            form.isSuccess = false;
            return View(form);
        }

        ////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////
        // VENDOR

        //to display addnewcompany view
        //takes orgid, isSuccess as parameters
        //returns addNewCompanyView
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "MyAccount", sideMenuName = "Companies", ViewName = "EditVendor")]
        [SessionExpire(lastPageUrl = "~/Companies/SearchCompany")]
        public ActionResult EditVendor(long OrganizationID, bool isSuccess)
        {

            ViewBag.HasFinancialData = _orgBusiness.IsVendorHasFinancialData(OrganizationID);
            ViewBag.appIdNotAvailable = false;
            ViewBag.isTaxIdExists = false; 
            ViewBag.taxIdCountError = false; 
            ViewBag.Countries = _orgBusiness.GetCountries().Select(r => new SelectListItem() { Text = r.Value, Value = r.Value }).ToList();
            var regions = _orgBusiness.GetRegions().Select(r => new SelectListItem() { Text = r.Value, Value = r.Value }).ToList();
            ViewBag.Regions = regions;
           LocalAddNewCompany newCompany;
            if (OrganizationID == -1)
            {
                newCompany = new LocalAddNewCompany();
                newCompany.OrganizationID = OrganizationID;

                //this is for checking vendor orgType radiobutton by deafult, in case of AddNew
                newCompany.OrganizationType = @Resources.Resources.Companies_Vendor;
                newCompany.AcceptVendorsViaInvite = false;
            }
            else
            {
                OrganizationData newOrganization = _orgBusiness.GetOrganization(OrganizationID);

                newCompany = new LocalAddNewCompany();
                newCompany.OrganizationID = OrganizationID;
                newCompany.OrganizationType = newOrganization.OrganizationType;
                newCompany.Name = newOrganization.Name;
                newCompany.PrincipalCompanyOfficerName = newOrganization.PrincipalCompanyOfficerName;
                newCompany.TaxID = newOrganization.TaxID;
                newCompany.CopyAnswers = newOrganization.CopyAnswers.ToString();
                if (newCompany.CopyAnswers == "") { newCompany.CopyAnswers = "0"; }
                newCompany.Address1 = newOrganization.Address1;
                newCompany.Address2 = newOrganization.Address2;
                newCompany.Address3 = newOrganization.Address3;
                newCompany.City = newOrganization.City;
                newCompany.State = newOrganization.State;
                newCompany.FinancialMonth = newOrganization.FinancialMonth;
                newCompany.FinancialDate = newOrganization.FinancialDate;
                newCompany.Zip = newOrganization.Zip;
                newCompany.Country = newOrganization.Country;
                newCompany.PhoneNumber = newOrganization.PhoneNumber;
                newCompany.ExtNumber = newOrganization.ExtNumber;
                newCompany.FaxNumber = newOrganization.FaxNumber;
                newCompany.WebsiteURL = newOrganization.WebsiteURL;
                newCompany.FederalIDNumber = newOrganization.FederalIDNumber;
                newCompany.CountryType = newOrganization.CountryType;
                newCompany.Language = newOrganization.Language;

                newCompany.GeographicArea = new List<string>();
                if (newOrganization.GeographicArea != null)
                {
                    foreach (var geoArea in newOrganization.GeographicArea.Split(','))
                    {
                        newCompany.GeographicArea.Add(geoArea);
                    }
                }
             
                newCompany.OrgRepresentativeName = newOrganization.OrgRepresentativeName;
                newCompany.OrgRepresentativeEmail = newOrganization.OrgRepresentativeEmail;
                newCompany.OrgInfusionSoftContactId = newOrganization.OrgInfusionSoftContactId;
             
                if (newOrganization.IsClientInviteOnly != null)
                    newCompany.AcceptVendorsViaInvite = (bool)newOrganization.IsClientInviteOnly;
                else
                    newCompany.AcceptVendorsViaInvite = false;

                newCompany.VendorArchivedDetails = _orgBusiness.GetArchivedArchiveOrganizationDetails(OrganizationID).OrderByDescending(r => r.VersionNo).ToList();
                ViewBag.States = _orgBusiness.GetCountryStates(newCompany.Country).Select(r => new SelectListItem() { Text = r.Value, Value = r.Value }).ToList();
                ViewBag.Languages = _orgBusiness.GetCountryLanguages(newCompany.Country).Select(r => new SelectListItem() { Text = r.Key, Value = r.Value }).ToList();
                ViewBag.Regions = _orgBusiness.GetRegions().Select(r => new SelectListItem() { Text = r.Value, Value = r.Value, Selected = newCompany.GeographicArea.Contains(r.Value) ? true : false }).ToList();
                //ViewBag.Regions = entityDB.Regions.Select(r => new SelectListItem() { Text = r.RegionName, Value = r.RegionName,Selected=  newCompany.GeographicArea.Contains(r.RegionName)?true :false }).ToList();
               
                Session["SelectedVendorId"] = newCompany.OrganizationID;
            }

            newCompany.isSuccess = isSuccess;
            return View(newCompany);
        }

        //to Edit Vendor
        //takes addnewdoc form as parameter
        //saves and returns editClientVeiw or editVendor(nothing but addNewCompnayView) based on selection in client/vendor radio button
        //Updated on 23rd July
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "MyAccount", sideMenuName = "Companies", ViewName = "EditVendor")]
        [SessionExpire(lastPageUrl = "~/SystemUsers/DashBoard")]
        [HttpPost]
        public ActionResult EditVendor(List<HttpPostedFileBase> ImageUrls, LocalAddNewCompany form) //Added ImageUrl on 23rd July
        {
            ViewBag.HasFinancialData = _orgBusiness.IsVendorHasFinancialData(form.OrganizationID);
            //var Vendor = entityDB.FinancialAnalyticsModel.FirstOrDefault(r => r.VendorId == form.OrganizationID);
            //ViewBag.Vendor = Vendor;
            ModelState.Remove("GeographicArea");
            ModelState.Remove("ContractorEvaluationExpirationPeriod");
            ViewBag.appIdNotAvailable = false;
            ViewBag.isTaxIdExists = false;
            ViewBag.Regions = _orgBusiness.GetRegions().Select(r => new SelectListItem() { Text = r.Value, Value = r.Value }).ToList();
            ViewBag.taxIdCountError = false;
            
            ViewBag.States = _orgBusiness.GetCountryStates(form.Country).Select(r => new SelectListItem() { Text = r.Value, Value = r.Value }).ToList();
            ViewBag.Languages = _orgBusiness.GetCountryLanguages(form.Country).Select(r => new SelectListItem() { Text = r.Key, Value = r.Value }).ToList();
            form.VendorArchivedDetails = _orgBusiness.GetArchivedArchiveOrganizationDetails(form.OrganizationID).OrderByDescending(r => r.VersionNo).ToList();
            ViewBag.Countries = _orgBusiness.GetCountries().Select(r => new SelectListItem() { Text = r.Value, Value = r.Value }).ToList();
            
            if (isRegistrationValid(form))
            {
                //SystemApplicationId appId = entityDB.SystemApplicationId.FirstOrDefault();
                //string organizationWithTaxId = _orgBusiness.CheckAnyOrganizationHasTaxId(form.TaxID,form.OrganizationID);
                //if (appId == null)
                //{
                //    ViewBag.appIdNotAvailable = true;
                //    return View(form);
                //}

                if (form.CountryType == (int)StateType.USA)
                {
                    var taxIDCount = form.TaxID.Replace("-", "").Count();
                    if (taxIDCount == 0 || taxIDCount != 9)
                    {
                        ViewBag.taxIdCountError = true;
                        return View(form);
                    }
                }

                OrganizationData newOrganization;
                if (form.OrganizationID == -1)
                {
                    newOrganization = new OrganizationData();
                    newOrganization.OrganizationType = form.OrganizationType;
                }
                else
                {
                    newOrganization = _orgBusiness.GetOrganization(form.OrganizationID);
                }

                if (newOrganization.OrganizationType.Equals("Vendor"))
                    newOrganization.IsClientInviteOnly = false;
                else
                    newOrganization.IsClientInviteOnly = form.AcceptVendorsViaInvite;
                
                var pathForCSI = HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["WebResponseFiles"]);
                if (newOrganization.OrganizationType == "Vendor")
                {
                    if (newOrganization.Name != form.Name || newOrganization.PrincipalCompanyOfficerName != form.PrincipalCompanyOfficerName)
                    {
                        try
                        {
                            CSIWebServiceInitiateRequest.CSIWebService(newOrganization.OrganizationID, form.Name, form.PrincipalCompanyOfficerName, pathForCSI); // Kiran on 8/8/2014 
                        }
                        catch
                        {
                            // ignored
                        }
                    }

                    if (newOrganization.Name != form.Name || newOrganization.PrincipalCompanyOfficerName != form.PrincipalCompanyOfficerName || newOrganization.OrgRepresentativeEmail != form.OrgRepresentativeEmail || newOrganization.OrgRepresentativeName != form.OrgRepresentativeName || newOrganization.Address1 != form.Address1 || newOrganization.Address2 != form.Address2)
                    {
                        string adminSuperUserEmail = _sysBusiness.GetAdminSuperUserEmail();

                        string body = System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Views/Companies/EmailFormatForvendorInformation.htm"));
                        string subject = form.Name + " changed its company information";
                        body = body.Replace("_Vendor", form.Name);
                        var items = "";
                        if (newOrganization.Name.Trim() != form.Name.Trim())
                        {
                            items += "<li style='width:100%;float:left;padding:0.5% 0;'>Legal Name of Business</li>";
                        }
                        if (newOrganization.PrincipalCompanyOfficerName!=null && newOrganization.PrincipalCompanyOfficerName.Trim() != form.PrincipalCompanyOfficerName.Trim())
                        {
                            items += "<li style='width:100%;float:left;padding:0.5% 0;'>Principal Officer Name</li>";
                        }
                        if (!string.IsNullOrEmpty(newOrganization.OrgRepresentativeEmail) && newOrganization.OrgRepresentativeEmail.Trim() != form.OrgRepresentativeEmail.Trim())
                        {
                            items += "<li style='width:100%;float:left;padding:0.5% 0;'>Representative Email</li>";
                        }
                        if (!string.IsNullOrEmpty(newOrganization.OrgRepresentativeName) && newOrganization.OrgRepresentativeName.Trim() != form.OrgRepresentativeName.Trim())
                        {
                            items += "<li style='width:100%;float:left;padding:0.5% 0;'>Representative Name</li>";
                        }
                        if (!string.IsNullOrEmpty(newOrganization.Address1) && newOrganization.Address1.Trim() != form.Address1.Trim())
                        {
                            items += "<li style='width:100%;float:left;padding:0.5% 0;'>Address1</li>";
                        }
                        if (!string.IsNullOrEmpty(newOrganization.Address2)  && newOrganization.Address2.Trim() != form.Address2?.Trim())
                        {
                            items += "<li style='width:100%;float:left;padding:0.5% 0;'>Address2</li>";
                        }
                        
                        if (!string.IsNullOrEmpty(newOrganization.Address3) && newOrganization.Address3.Trim() != form.Address3?.Trim())
                        {
                            items += "<li style='width:100%;float:left;padding:0.5% 0;'>Address3</li>";
                        }
                        
                        body = body.Replace("_items", items);
                        Common.SendMail.sendMail(adminSuperUserEmail, body, subject);

                        _sysBusiness.AddArchiveVendorData(null, SSession.UserId, newOrganization);
                    }
                }
                //Ends <<<

                newOrganization.Name = form.Name;
                newOrganization.PrincipalCompanyOfficerName = form.PrincipalCompanyOfficerName; 
                newOrganization.TaxID = form.TaxID;
                newOrganization.CountryType = form.CountryType;
                newOrganization.CopyAnswers = Convert.ToInt32(form.CopyAnswers);
                newOrganization.Address1 = form.Address1;
                newOrganization.Address2 = form.Address2;
                newOrganization.Address3 = form.Address3;
                newOrganization.City = form.City;
                newOrganization.State = form.State;
                newOrganization.Language = Convert.ToInt64(form.Language);
                newOrganization.FinancialMonth = form.FinancialMonth;
                newOrganization.FinancialDate = form.FinancialDate;
                newOrganization.Zip = form.Zip;
                newOrganization.Country = form.Country;
                newOrganization.PhoneNumber = form.PhoneNumber;
                newOrganization.ExtNumber = form.ExtNumber;
                newOrganization.FaxNumber = form.FaxNumber;
                newOrganization.WebsiteURL = form.WebsiteURL;
                
                newOrganization.IsClientInviteOnly = form.AcceptVendorsViaInvite; 
                
                if (form.GeographicArea!=null)
                {
                    newOrganization.GeographicArea = string.Join(",", form.GeographicArea);
                }
                newOrganization.OrgRepresentativeName = form.OrgRepresentativeName;
                newOrganization.OrgRepresentativeEmail = form.OrgRepresentativeEmail;
                newOrganization.OrgInfusionSoftContactId = form.OrgInfusionSoftContactId;
                
                long newOrgId = _orgBusiness.PostOrganization(newOrganization,form.OrganizationID);

                form.OrganizationID = newOrgId;

                if (Request.Files != null && Request.Files.Count > 0)
                {
                    for (int i = 0; i < Request.Files.Count; i++)
                    {
                        string file = Request.Files.AllKeys[i];
                        if (Request.Files[i].ContentLength == 0) continue;

                        var image = WebImage.GetImageFromRequest(file);
                        if (image != null)
                        {
                            if (image.Width > 500)
                            {
                                image.Resize(500, ((500 * image.Height) / image.Width));
                            }


                            var extension = Path.GetExtension(image.FileName);
                            string logoId = form.OrganizationID.ToString();

                            if (i == 0)
                            {
                                Directory.CreateDirectory(Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["LogoUploadPath"])));
                                var path = Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["LogoUploadPath"]), logoId + extension);
                                image.Save(path);
                            }

                            else
                            {
                                Directory.CreateDirectory(Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["PhotoUploadPath"])));
                                var path = Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["PhotoUploadPath"]), logoId + extension);
                                image.Save(path);
                            }

                        }
                    }
                }
                //end

                if (newOrganization.OrganizationType.Equals("Client"))
                {
                    return RedirectToAction("EditClient", new RouteValueDictionary(
                    new { controller = "Companies", action = "EditClient", OrganizationID = form.OrganizationID, isSuccess = true }));
                }
                if (newOrganization.OrganizationType.Equals("Vendor"))
                {
                    return RedirectToAction("EditVendor", new RouteValueDictionary(
                    new { controller = "Companies", action = "EditVendor", OrganizationID = form.OrganizationID, isSuccess = true }));
                }
            }

            form.isSuccess = false;
            return View(form);
        }


        //to get list of docs
        //takes orgid as paramter
        //retrieve and assigns docslist as viewbag element
        //returns partialview of docslist.
        [SessionExpireForView]
        [OutputCache(Duration = 0)]
        public ActionResult DocumentsListPartial(long orgId)
        {
            RetrieveDocuments(orgId);
            return PartialView();
        }


        //to retrieve and assigns list of docs as veiwbag elements
        //this method used 2 times
        public void RetrieveDocuments(long orgId)
        {
            List<LocalDocument> localDocList = new List<LocalDocument>();

            List<ClientTemplates> clientTemps = entityDB.ClientTemplates.Where(m => m.ClientID == orgId).ToList();

            foreach (var clientTemp in clientTemps)
            {
                LocalDocument localDoc = new LocalDocument();

                List<Document> docs = new List<Document>();

                foreach (var sec in clientTemp.Templates.TemplateSections.ToList())
                {
                    if (sec.TemplateSectionType == 0)
                    {
                        foreach (var clientTempProfile in sec.ClientTemplateProfiles.Where(m => m.ClientId == orgId))
                        {
                            if (clientTempProfile.AttachmentExist == true)
                            {
                                List<Document> docsLocal = entityDB.Document.Where(m => m.ReferenceRecordID == clientTempProfile.ClientTemplateProfileId && (m.ReferenceType == "Client Profile")).ToList();
                                foreach (var doc in docsLocal)
                                {
                                    docs.Add(doc);
                                }
                            }
                        }
                    }
                }
                    foreach (var sec in clientTemp.Templates.TemplateSections.ToList())
                    {
                        if (sec.TemplateSectionType == 31)
                        {
                            foreach (var clientTempProfile in sec.ClientTemplateProfiles.Where(m => m.ClientId == orgId))
                            {
                                if (clientTempProfile.AttachmentExist == true)
                                {
                                    List<Document> docsLocal = entityDB.Document.Where(m => m.ReferenceRecordID == clientTempProfile.ClientTemplateProfileId && (m.ReferenceType == "Client Profile")).ToList();
                                    foreach (var doc in docsLocal)
                                    {
                                        docs.Add(doc);
                                    }
                                }
                            }
                        }
                        //else
                        //{
                        //    foreach (var headerFooter in sec.TemplateSectionsHeaderFooter)
                        //    {
                        //        if (headerFooter.AttachmentExist == true)
                        //        {
                        //            List<Document> docsLocal = entityDB.Document.Where(m => m.ReferenceRecordID == headerFooter.HeaderFooterId && (m.ReferenceType == @Resources.Resources.Companies_SecHeader || m.ReferenceType == @Resources.Resources.Companies_SecFooter) && m.OrganizationId == orgId).ToList();
                        //            foreach (var doc in docsLocal)
                        //            {
                        //                docs.Add(doc);
                        //            }
                        //        }
                        //    }

                        //    foreach (var subsec in sec.TemplateSubSections)
                        //    {
                        //        if (subsec.AttachmentExist == true)
                        //        {
                        //            List<Document> docsLocal = entityDB.Document.Where(m => m.ReferenceRecordID == subsec.SubSectionID && m.ReferenceType == @Resources.Resources.Companies_SubSec && m.OrganizationId == orgId).ToList();
                        //            foreach (var doc in docsLocal)
                        //            {
                        //                docs.Add(doc);
                        //            }
                        //        }
                        //    }
                        //}
                    }

                if (docs.Count > 0)
                {
                    localDoc.documents = docs;
                    localDoc.Category = clientTemp.Templates.TemplateName;
                    localDoc.TemplateOrBUnit = 1; //means Template
                    localDocList.Add(localDoc);
                }

            }

            List<ClientBusinessUnits> bUnits = entityDB.ClientBusinessUnits.Where(m => m.ClientId == orgId).ToList();
            foreach (var unit in bUnits)
            {
                LocalDocument localDoc = new LocalDocument();

                List<Document> docs = new List<Document>();

                List<Document> docsLocal = entityDB.Document.Where(m => m.ReferenceRecordID == unit.ClientBusinessUnitId && (m.ReferenceType == "Business Unit")).ToList();
                foreach (var doc in docsLocal)
                {
                    docs.Add(doc);
                }

                if (docs.Count > 0)
                {
                    localDoc.documents = docs;
                    localDoc.Category = unit.BusinessUnitName;
                    localDoc.TemplateOrBUnit = 2; //means BUnit
                    localDocList.Add(localDoc);
                }
            }

            ViewBag.localDocumentsList = localDocList;
       


        List<ClientBusinessUnitSites> bUnitsites = entityDB.ClientBusinessUnits.Where(m => m.ClientId == orgId).SelectMany(r=>r.ClientBusinessUnitSites).ToList();
            foreach (var unit in bUnitsites)
            {
                LocalDocument localDoc = new LocalDocument();

        List<Document> docs = new List<Document>();

        List<Document> docsLocal = entityDB.Document.Where(m => m.ReferenceRecordID == unit.ClientBusinessUnitSiteId && (m.ReferenceType == "ClientBusinessUnitSites")).ToList();
                foreach (var doc in docsLocal)
                {
                    docs.Add(doc);
                }

                if (docs.Count > 0)
                {
                    localDoc.documents = docs;
                    localDoc.Category = unit.SiteName;
                    localDoc.TemplateOrBUnit = 3; //means BUnitsite
                    localDocList.Add(localDoc);
                }
            }

            ViewBag.localDocumentsList = localDocList;
        }


        ////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////
        // VENDOR

        //to display addnewcompany view
        //takes orgid, isSuccess as parameters
        //returns addNewCompanyView
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "MyAccount", sideMenuName = "Companies", ViewName = "AddNewCompany")]
        [SessionExpire(lastPageUrl = "~/Companies/SearchCompany")]
        public ActionResult AddNewCompany(long OrganizationID, bool isSuccess)
        {
              ViewBag.Templates = GetTemplateDetails(LocalConstants.ClientID, new Guid());
            ViewBag.appIdNotAvailable = false;
            ViewBag.isTaxIdExists = false; 
            ViewBag.taxIdCountError = false; 
            ViewBag.Countries = _orgBusiness.GetCountries().Select(r => new SelectListItem() { Text = r.Value, Value = r.Value }).ToList();
            var regions = _orgBusiness.GetRegions().Select(r => new SelectListItem() { Text = r.Value, Value = r.Value }).ToList();
            ViewBag.Regions = regions;
            LocalAddNewCompany newCompany;
            if (OrganizationID == -5)
            {
                newCompany = new LocalAddNewCompany();
                newCompany.OrganizationID = OrganizationID;

                //this is for checking vendor orgType radiobutton by deafult, in case of AddNew
                newCompany.OrganizationType = @Resources.Resources.Companies_Vendor;
                newCompany.AcceptVendorsViaInvite = false; 
            }
            else
            {
                OrganizationData newOrganization = _orgBusiness.GetOrganization(OrganizationID);

                newCompany = new LocalAddNewCompany();
                newCompany.OrganizationID = OrganizationID;
                newCompany.OrganizationType = newOrganization.OrganizationType;
                newCompany.Name = newOrganization.Name;
                newCompany.PrincipalCompanyOfficerName = newOrganization.PrincipalCompanyOfficerName;
                newCompany.TaxID = newOrganization.TaxID;
                
                newCompany.CopyAnswers = newOrganization.CopyAnswers.ToString();
                newCompany.Address1 = newOrganization.Address1;
                newCompany.Address2 = newOrganization.Address2;
                newCompany.Address3 = newOrganization.Address3;
                newCompany.City = newOrganization.City;
                newCompany.State = newOrganization.State;
                newCompany.Zip = newOrganization.Zip;
                newCompany.Country = newOrganization.Country;
                newCompany.PhoneNumber = newOrganization.PhoneNumber;
                newCompany.ExtNumber = newOrganization.ExtNumber;
                newCompany.FaxNumber = newOrganization.FaxNumber;
                newCompany.WebsiteURL = newOrganization.WebsiteURL;
                newCompany.FederalIDNumber = newOrganization.FederalIDNumber;

                newCompany.OrgRepresentativeName = newOrganization.OrgRepresentativeName;
                newCompany.OrgRepresentativeEmail = newOrganization.OrgRepresentativeEmail;
                newCompany.OrgInfusionSoftContactId = newOrganization.OrgInfusionSoftContactId;
                
                if (newOrganization.IsClientInviteOnly != null)
                    newCompany.AcceptVendorsViaInvite = (bool)newOrganization.IsClientInviteOnly;
                else
                    newCompany.AcceptVendorsViaInvite = false;

                Session["SelectedVendorId"] = newCompany.OrganizationID;
            }
            var restrictedClients = _Client.GetRestrictedClients(OrganizationID);
            newCompany.clients = (from clients in _Client.GetClients().Where(r => !restrictedClients.Contains(long.Parse(r.Key)))
                                  select new SelectListItem
                                    {
                                        Text = clients.Value,
                                        Value = clients.Key.ToString(),
                                    }).ToList();

            newCompany.isSuccess = isSuccess;
            return View(newCompany);
        }
        [SessionExpireForView]
        public string unlockPrequalification(long prequalId, bool commitStatus)
        {
            Prequalification prequal = entityDB.Prequalification.Find(prequalId);

            if (prequal != null)
            {
                prequal.locked = false;
                prequal.UnlockedBy = (Guid)Session["UserId"];
                prequal.UnlockedDate = DateTime.Now;
                entityDB.SaveChanges();
                return "PrequalUnlockSuccess";
            }
            return "Invalid Prequalification ID.";
        }

        //to delete prequalification
        //takes prequalId as parameter
        //returns success/failure message
        [SessionExpireForView]
        public string deletePrequalification(long prequalId, bool commitStatus)
        {
            Prequalification prequal = entityDB.Prequalification.Find(prequalId);

            ////Added on 10th Aug 2013
            if (prequal != null)
            {
                List<Document> prequalDocs = entityDB.Document.Where(rec => rec.ReferenceType == "PreQualification" && rec.ReferenceRecordID == prequalId).ToList();
                //if (prequal.PrequalificationStatusId <= 4 && prequalDocs.Count == 0)
                {
                    //try
                    {
                        foreach (var StatusLog in prequal.PrequalificationStatusChangeLog.ToList())
                            entityDB.PrequalificationStatusChangeLog.Remove(StatusLog);
                        //if (prequal.CSIWebResponseDetails != null)
                        //    foreach (var CSI in prequal.CSIWebResponseDetails.ToList())
                        //        entityDB.CSIWebResponseDetails.Remove(CSI);
                        if (prequal.OrganizationsNotificationsEmailLogs != null)
                            foreach (var ONEmailLogs in prequal.OrganizationsNotificationsEmailLogs.ToList())
                                entityDB.OrganizationsNotificationsEmailLogs.Remove(ONEmailLogs);
                        if (prequal.PrequalificationComments != null)
                            foreach (var Comments in prequal.PrequalificationComments.ToList())
                                entityDB.PrequalificationComments.Remove(Comments);
                        if (prequal.PrequalificationCompletedSections != null)
                            foreach (var PCompletedSection in prequal.PrequalificationCompletedSections.ToList())
                                entityDB.PrequalificationCompletedSections.Remove(PCompletedSection);
                        if (prequal.PrequalificationReportingData != null)
                            foreach (var ReportingData in prequal.PrequalificationReportingData.ToList())
                                entityDB.PrequalificationReportingData.Remove(ReportingData);
                        if (prequal.PrequalificationSites != null)
                            foreach (var PreSites in prequal.PrequalificationSites.ToList())
                                entityDB.PrequalificationSites.Remove(PreSites);
                        if (prequal.PrequalificationUserInputs != null)
                            foreach (var UserInput in prequal.PrequalificationUserInputs.ToList())
                                entityDB.PrequalificationUserInput.Remove(UserInput);
                        if (prequal.PrequalificationClient.Id != 0)
                            entityDB.PrequalificationClient.Remove(prequal.PrequalificationClient);

                        if (prequal.VendorDetails != null)
                            foreach (var vendorContact in prequal.VendorDetails.ToList())
                                entityDB.VendorDetails.Remove(vendorContact);
                        if (prequal.PrequalificationPayments != null)
                            foreach (var prequalPayment in prequal.PrequalificationPayments.ToList())
                                entityDB.PrequalificationPayments.Remove(prequalPayment);
                        //sandhya on 08/12/2015
                        if (prequal.PrequalificationTrainingAnnualFees != null)
                            foreach (var TrainingPayment in prequal.PrequalificationTrainingAnnualFees.ToList())
                                entityDB.PrequalificationTrainingAnnualFees.Remove(TrainingPayment);
                        if (prequalDocs != null)
                            foreach (var UserInput in prequalDocs.ToList())
                                entityDB.Document.Remove(UserInput);
                        //Document's deletion from its physical location
                        foreach (var document in prequalDocs)
                        {
                            var dotIndex = document.DocumentName.LastIndexOf('.');
                            var extension = document.DocumentName.Substring(dotIndex);
                            var path = Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["ClientUploadsPath"]), document.DocumentId + extension);
                            if (path != null && System.IO.File.Exists(path))
                            {
                                System.IO.File.Delete(path);
                            }
                        }

                        //Siva on 29th Mar 2014
                        //Rajesh on 4/5/2014
                        if (prequal.PrequalificationEMRStatsYears != null)
                            foreach (var prequalEmrStatYear in prequal.PrequalificationEMRStatsYears.ToList())
                            {
                                if (prequalEmrStatYear.PrequalificationEMRStatsValues != null)
                                {
                                    foreach (var values in prequalEmrStatYear.PrequalificationEMRStatsValues.ToList())
                                    {
                                        entityDB.PrequalificationEMRStatsValues.Remove(values);
                                    }
                                }
                                entityDB.PrequalificationEMRStatsYears.Remove(prequalEmrStatYear);
                            }
                        //Ends<<<

                        if (prequal.PrequalificationNotifications != null)
                            foreach (var prequalNotification in prequal.PrequalificationNotifications.ToList())
                                entityDB.PrequalificationNotifications.Remove(prequalNotification);
                        //End of Siva on 29th Mar 2014

                        if (prequal.PrequalificationSectionsToDisplay != null)
                            foreach (var section in prequal.PrequalificationSectionsToDisplay.ToList())
                                entityDB.PrequalificationSectionsToDisplay.Remove(section);

                        //Siva on 2nd April
                        if (prequal.PrequalificationStatusId >= 5)
                        {
                            var currentDate = DateTime.Now;
                            var orgPrequalOpenPeriods = entityDB.OrganizationsPrequalificationOpenPeriod.FirstOrDefault(rec => rec.VendorId == prequal.VendorId && rec.PrequalificationPeriodStart <= currentDate && rec.PrequalificationPeriodClose >= currentDate);
                            if (orgPrequalOpenPeriods != null)
                            {
                                orgPrequalOpenPeriods.TotalPaymentRecieved -= (prequal.TotalPaymentReceivedAmount == null ? 0 : (decimal)prequal.TotalPaymentReceivedAmount);
                                if (orgPrequalOpenPeriods.TotalPaymentRecieved < 0)
                                    orgPrequalOpenPeriods.TotalPaymentRecieved = 0;

                                entityDB.Entry(orgPrequalOpenPeriods).State = EntityState.Modified;
                            }
                        }

                        if (prequal.FeeCapLog != null)
                            foreach (var feecap in prequal.FeeCapLog.ToList())
                                entityDB.FeeCapLog.Remove(feecap);
                        if (prequal.PrequalificationSubsection.Any())
                            foreach (var pqSubSec in prequal.PrequalificationSubsection.ToList())
                            {
                                foreach (var pqQues in pqSubSec.PrequalificationQuestion.ToList())
                                {
                                    entityDB.PrequalificationQuestion.Remove(pqQues);
                                }
                                entityDB.PrequalificationSubsection.Remove(pqSubSec);
                            }
                        entityDB.Prequalification.Remove(prequal);

                        if (commitStatus)
                            entityDB.SaveChanges();

                        return "PrequalDeleteSuccess";
                    }
                    //catch
                    //{
                    //    return "UserAddedData";
                    //}
                }
                //else
                //{
                //    return "UserAddedData";
                //}






                //List<PrequalificationSites> prequalSitesList = entityDB.PrequalificationSites.Where(m => m.PrequalificationId == prequal.PrequalificationId).ToList();
                //List<OrganizationsNotificationsEmailLogs> emailLogs = entityDB.OrganizationsNotificationsEmailLogs.Where(m => m.PrequalificationId == prequalId).ToList();
                //List<PrequalificationReportingData> prequalReportDatas = entityDB.PrequalificationReportingData.Where(m => m.PrequalificationId == prequal.PrequalificationId).ToList();
                //List<PrequalificationUserInput> prequalUserInputs = entityDB.PrequalificationUserInput.Where(m => m.PreQualificationId == prequal.PrequalificationId).ToList();
                //List<PrequalificationCompletedSections> prequalCompleteSecs = entityDB.PrequalificationCompletedSections.Where(m => m.PrequalificationId == prequal.PrequalificationId).ToList();
                //List<Document> prequalDocs = entityDB.Document.Where(rec => rec.ReferenceType == "PreQualification" && rec.ReferenceRecordID == prequalId).ToList();

                //if (prequalSitesList.Count == 0 && emailLogs.Count == 0 && prequalReportDatas.Count == 0 && prequalUserInputs.Count == 0 && prequalCompleteSecs.Count == 0 && prequalDocs.Count == 0)
                //{
                //    entityDB.Prequalification.Remove(prequal);
                //    entityDB.SaveChanges();

                //    return "PrequalDeleteSuccess";
                //}

                //foreach (var prequalSite in perqualSitesList)
                //{
                //    entityDB.PrequalificationSites.Remove(prequalSite);
                //}

                //foreach (var emaillog in emailLogs)
                //{
                //    entityDB.OrganizationsNotificationsEmailLogs.Remove(emaillog);
                //}

                //foreach (var prequalReportData in prequalReportDatas)
                //{
                //    entityDB.PrequalificationReportingData.Remove(prequalReportData);
                //}

                //foreach (var prequalUserInput in prequalUserInputs)
                //{
                //    entityDB.PrequalificationUserInput.Remove(prequalUserInput);
                //}

                //foreach (var CompleteSection in prequalCompleteSecs)
                //{
                //    entityDB.PrequalificationCompletedSections.Remove(CompleteSection);
                //}

                //foreach (var document in prequalDocs)
                //{
                //    var dotIndex = document.DocumentName.LastIndexOf('.');
                //    var extension = document.DocumentName.Substring(dotIndex);
                //    var path = Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["ClientUploadsPath"]), document.DocumentId + extension);
                //    if (path != null)
                //    {
                //        System.IO.File.Delete(path);
                //    }

                //    entityDB.Document.Remove(document);
                //}

            }
            ////End of 10th Aug 2013

            return "DeletePrequalFailed";
        }




        //to get list of prequalifications in EditVendor view
        //takes vendorid as parameter
        //assigns prequallist as viewbag element and returns prequalList partialView
        [SessionExpireForView]
        [OutputCache(Duration = 0)]
        public ActionResult PrequalificationsListPartial(long vendorId)
        {
            List<Prequalification> prequals = entityDB.Prequalification.Where(m => m.VendorId == vendorId).ToList();

            ViewBag.prequalificationsList = prequals;

            return PartialView();
        }
        public bool isRegistrationValid(LocalAddNewCompany locVenReg_form)
        {
            if (locVenReg_form.CountryType != (int)StateType.USA)
            {
                var taxerror = ModelState["TaxID"].Errors.FirstOrDefault(rec => rec.ErrorMessage == Resources.Resources.Common_InvalidTaxID);
                ModelState["TaxID"].Errors.Remove(taxerror);
            }

            return ModelState.IsValid;
        }
        //to add a new company(Client or vendor)
        //takes addnewdoc form as parameter
        //saves and returns editClientVeiw or editVendor(nothing but addNewCompnayView) based on selection in client/vendor radio button
        //Updated on 23rd July
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "MyAccount", sideMenuName = "Companies", ViewName = "AddNewCompany")]
        [SessionExpire(lastPageUrl = "~/SystemUsers/DashBoard")]
        [HttpPost]
        public ActionResult AddNewCompany(List<HttpPostedFileBase> ImageUrls, LocalAddNewCompany form) //Added ImageUrl on 23rd July
        {
            ViewBag.Templates = GetTemplateDetails(LocalConstants.ClientID, new Guid());
            ViewBag.Countries = _orgBusiness.GetCountries().Select(r => new SelectListItem() { Text = r.Value, Value = r.Value }).ToList();
            ViewBag.appIdNotAvailable = false;
            ViewBag.isTaxIdExists = false; 
            ViewBag.taxIdCountError = false; 
            var regions = _orgBusiness.GetRegions().Select(r => new SelectListItem() { Text = r.Value, Value = r.Value }).ToList();
            ViewBag.Regions = regions;
            if (form.OrganizationType == @OrganizationType.Client || form.OrganizationType == OrganizationType.SuperClient)
            {
                ModelState.Remove("GeographicArea");
                ModelState.Remove("PrincipalCompanyOfficerName");
                ModelState.Remove("CopyAnswers");
                ModelState.Remove("TaxType");
                ModelState.Remove("Language");
                ModelState.Remove("TaxID");
            }
            if (form.OrganizationType == @OrganizationType.Vendor || form.OrganizationType == OrganizationType.Client)
            {
                ModelState.Remove("clients");
            }
               List<LocalImages> images = new List<LocalImages>();
            if (Request.Files != null && Request.Files.Count > 0)
            {
                for (int j = 0; j < Request.Files.Count; j++)
                {
                    string file = Request.Files.AllKeys[j];
                    if (Request.Files[j].ContentLength == 0) continue;

                    var image = WebImage.GetImageFromRequest(file);
                    LocalImages localImage = new LocalImages();
                    localImage.Name = file;
                    localImage.Image = image;
                    images.Add(localImage);

                    if (image != null)
                    {
                        var extension = Path.GetExtension(image.FileName);
                        if (!extension.Equals(".png") && j == 0)
                        {
                            ModelState.AddModelError("Logo", "Only PNG files can be uploaded");
                        }
                        if (!extension.Equals(".png") && j == 1)
                        {
                            ModelState.AddModelError("Photo", "Only PNG files can be uploaded");
                        }
                        var fileSize = image.GetBytes().Length / 1024;

                        if (fileSize > 30 && j == 0)
                        {
                            ModelState.AddModelError("Logo", "File size can not be more than 30KB, please choose another file");
                        }
                        if (fileSize > 30 && j == 1)
                        {
                            ModelState.AddModelError("Photo", "File size can not be more than 30KB, please choose another file");
                        }
                        if ((image.Width > 250 || image.Height > 250) && j == 0)
                        {
                            ModelState.AddModelError("Logo", "Logo size can not be more than 250 X 250 pixels");
                        }
                    }
                }
            }
            // kiran on 03/10/2015 for handling logo validations in IE Ends<<<

            // If valid, save to database
            if (isRegistrationValid(form))
            {
                //SystemApplicationId appId = entityDB.SystemApplicationId.FirstOrDefault();
                string organizationWithTaxId = _orgBusiness.CheckAnyOrganizationHasTaxId(form.TaxID,null);
                //if (appId == null)
                //{
                //    ViewBag.appIdNotAvailable = true;
                //    return View(form);
                //}

                if (form.OrganizationType == "Vendor" && form.CountryType == (int)StateType.USA)
                {
                    var taxIDCount = form.TaxID.Replace("-", "").Count();
                    //if (form.TaxType != 2)
                    if (taxIDCount == 0 || taxIDCount != 9)
                    {
                        ViewBag.taxIdCountError = true;
                        return View(form);
                    }
                    
                    if (!string.IsNullOrEmpty(organizationWithTaxId))
                    {
                        ViewBag.isTaxIdExists = true;
                        ViewBag.VBTaxIdOrg = organizationWithTaxId;
                        return View(form);
                    }
                }
                OrganizationData newOrganization;
                if (form.OrganizationID == -5)
                {
                    newOrganization = new OrganizationData();;
                    newOrganization.OrganizationType = form.OrganizationType;
                }
                else
                {
                    newOrganization = _orgBusiness.GetOrganization(form.OrganizationID);
                }

                // Siva - 10th Aug 2013
                if (newOrganization.OrganizationType.Equals(@OrganizationType.Vendor))
                {
                    newOrganization.IsClientInviteOnly = null;
                    newOrganization.TrainingRequired = null; 
                    newOrganization.TrainingFeeCap = null;
                    newOrganization.TrainingCertificate = null;
                    newOrganization.PrincipalCompanyOfficerName = form.PrincipalCompanyOfficerName; 
                    newOrganization.CopyAnswers = Convert.ToInt32(form.CopyAnswers); 
                    newOrganization.TaxID = form.TaxID; 
                    newOrganization.GeographicArea = string.Join(",", form.GeographicArea);
                }
                else
                {
                    newOrganization.SingleClientCode = FVGen3.Common.EncryptionDecryption.GetUniqueKey();
                    newOrganization.IsClientInviteOnly = form.AcceptVendorsViaInvite;
                    newOrganization.ShowInApplication = true;
                    newOrganization.TrainingRequired = form.TrainingRequired; 
                    newOrganization.TrainingFeeCap = form.TrainingFeeCap.ToDecimalNullSafe(0.00m); 
                    newOrganization.TrainingCertificate = form.TrainingCertificate;
                                                     
                }
                //End

                newOrganization.Name = form.Name;
                newOrganization.CountryType = form.CountryType;
                newOrganization.Address1 = form.Address1;
                newOrganization.Address2 = form.Address2;
                newOrganization.Address3 = form.Address3;
                newOrganization.City = form.City;
                newOrganization.State = form.State;
                newOrganization.Zip = form.Zip;
                newOrganization.Country = form.Country;
                newOrganization.Language = Convert.ToInt64(form.Language);
                newOrganization.PhoneNumber = form.PhoneNumber;
                newOrganization.ExtNumber = form.ExtNumber;
                newOrganization.FaxNumber = form.FaxNumber;
                newOrganization.WebsiteURL = form.WebsiteURL;
                newOrganization.FederalIDNumber = form.FederalIDNumber;

                newOrganization.OrgRepresentativeName = form.OrgRepresentativeName;
                newOrganization.OrgRepresentativeEmail = form.OrgRepresentativeEmail;
                newOrganization.OrgInfusionSoftContactId = form.OrgInfusionSoftContactId;
                
                long newOrgId = _orgBusiness.PostOrganization(newOrganization,form.OrganizationID);
                if (newOrganization.OrganizationType.Equals(OrganizationType.SuperClient))
                {
                    foreach (var subClient in form.clients.Where(r => r.Selected == true))
                    {
                        _orgBusiness.PostSubClient(newOrgId, Convert.ToInt64(subClient.Value));
                    }
                    _Client.PostClientBusAndSites(form.OrganizationID, form.Name);
                }
                var pathForCSI = HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["WebResponseFiles"]);
                try
                {
                    var data = Utilities.CSIWebServiceInitiateRequest.CSIWebService(newOrganization.OrganizationID, newOrganization.Name, newOrganization.PrincipalCompanyOfficerName, pathForCSI); // Kiran on 8/8/2014
                }
                catch { }

               form.OrganizationID = newOrgId;

                foreach (var image in images)
                {
                    if (image != null)
                    {
                        if (image.Image.Width > 500)
                        {
                            image.Image.Resize(500, ((500 * image.Image.Height) / image.Image.Width));
                        }

                        var extension = Path.GetExtension(image.Image.FileName);
                        string logoId = form.OrganizationID.ToString();

                        if (image.Name.Equals("Logo"))
                        {
                            Directory.CreateDirectory(Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["LogoUploadPath"])));
                            var path = Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["LogoUploadPath"]), logoId + extension);
                            image.Image.Save(path);
                        }

                        else
                        {
                            Directory.CreateDirectory(Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["PhotoUploadPath"])));
                            var path = Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["PhotoUploadPath"]), logoId + extension);
                            image.Image.Save(path);
                        }
                    }
                }
             
                if (newOrganization.OrganizationType.Equals(@OrganizationType.Client) || (newOrganization.OrganizationType.Equals(@OrganizationType.SuperClient)))
                {
                    return RedirectToAction("EditClient", new RouteValueDictionary(
                    new { controller = "Companies", action = "EditClient", OrganizationID = form.OrganizationID, isSuccess = true }));
                }
                if (newOrganization.OrganizationType.Equals(@OrganizationType.Vendor))
                {
                    return RedirectToAction("EditVendor", new RouteValueDictionary(
                    new { controller = "Companies", action = "EditVendor", OrganizationID = form.OrganizationID, isSuccess = true }));
                }
            }

            form.isSuccess = false;
            return View(form);
        }

        //TO CROP IMAGE..
        public void EditImage(EditorInputModel editor, long OrganizationID, bool isLogo)
        {
            var image = new WebImage("~" + editor.ImageUrl);
            var height = (double)image.Height;
            var width = (double)image.Width;

            // No resizing - taken care of in JS, Siva on 2nd Dec>>>> 

            //if (image.Width > 100 && image.Height > 100)
            //{
            //    image.Resize(100, 100, true, false);
            //}
            //else if (image.Width > 100)
            //{
            //    image.Resize(100, image.Height, true, false);
            //}
            //else if (image.Height > 100)
            //{
            //    image.Resize(image.Width, 100, true, false);
            //}

            //SAVE file
            var extension = Path.GetExtension(image.FileName);
            string uniqueId = OrganizationID.ToString();
            if (isLogo)
            {
                Directory.CreateDirectory(Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["LogoUploadPath"])));
                var path = Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["LogoUploadPath"]), uniqueId + extension);
                image.Save(path);
            }
            else
            {
                Directory.CreateDirectory(Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["PhotoUploadPath"])));
                var path = Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["PhotoUploadPath"]), uniqueId + extension);
                image.Save(path);
            }

        }


        //to delete a company
        //work in progress
        //need to delete all information regarding the company
        [SessionExpireForView]
        [HttpGet]
        public string deleteCompany(long OrganizationID)
        {
            List<Prequalification> prequals = entityDB.Prequalification.Where(m => m.VendorId == OrganizationID).ToList();

            //Siva on 29th Mar 2014
            //if (prequals.Count > 0)
            //{
            //    return "PrequalificationsExist";
            //}
            foreach (var prequalification in prequals)
            {
                bool commitStatus = false;
                deletePrequalification(prequalification.PrequalificationId, commitStatus);

                //entityDB.Prequalification.Remove(prequalification);
            }
            //End of Siva on 29th Mar 2014

            Organizations organization = entityDB.Organizations.Find(OrganizationID);

            List<SystemUsersOrganizations> sysUserOrgs = entityDB.SystemUsersOrganizations.Where(m => m.OrganizationId == OrganizationID).ToList();
            foreach (var sysuserorg in sysUserOrgs)
            {
                SystemUsers sysUser = sysuserorg.SystemUsers;
                List<Contact> contactsLocal = sysUser.Contacts.ToList();
                foreach (var contactLocal in contactsLocal)
                {
                    entityDB.Contact.Remove(contactLocal);
                }

                List<SystemUserLogs> userLogs = entityDB.SystemUserLogs.Where(m => m.SystemUserId == sysUser.UserId).ToList();
                foreach (var userLog in userLogs)
                {
                    entityDB.SystemUserLogs.Remove(userLog);
                }

                List<Document> docs = entityDB.Document.Where(m => m.OrganizationId == OrganizationID).ToList();
                foreach (var document in docs)
                {
                    entityDB.Document.Remove(document);
                }
                List<DocumentLogs> docLogs = entityDB.DocumentLogs.Where(m => m.OrganizationId == OrganizationID).ToList();
                foreach (var documentlog in docLogs)
                {
                    entityDB.DocumentLogs.Remove(documentlog);
                }
                //remove sysUserOrgRoles,
                List<SystemUsersOrganizationsRoles> orgRoles = entityDB.SystemUsersOrganizationsRoles.Where(m => m.SysUserOrgId == sysuserorg.SysUserOrganizationId).ToList();
                foreach (var orgRole in orgRoles)
                {
                    entityDB.SystemUsersOrganizationsRoles.Remove(orgRole);
                }

                // kiran on 12/18/2014
                foreach (var orgQuiz in sysuserorg.SystemUsersOrganizationsQuizzes.ToList())
                {
                    foreach (var empQuizUserInput in orgQuiz.EmployeeQuizUserInput.ToList())
                    {
                        entityDB.EmployeeQuizUserInput.Remove(empQuizUserInput);
                    }
                    foreach (var quizCompetedSec in orgQuiz.QuizzesCompletedSections.ToList())
                    {
                        entityDB.QuizzesCompletedSections.Remove(quizCompetedSec);
                    }

                    entityDB.SystemUsersOrganizationsQuizzes.Remove(orgQuiz);
                }
                // Ends<<<


                entityDB.SystemUsersOrganizations.Remove(sysuserorg);
                entityDB.SystemUsers.Remove(sysUser);

            }

            //Siva on 29th Mar 2014 

            // Kiran on 8/8/2014
            foreach (var CSI in organization.CSIWebResponseDetails.ToList())
            {
                entityDB.CSIWebResponseDetails.Remove(CSI);
            }
            // Ends<<<

            foreach (var bunit in organization.ClientBusinessUnits.ToList())
            {
                entityDB.ClientBusinessUnits.Remove(bunit);
            }
            foreach (var clientTemplateProfile in organization.ClientTemplateProfiles.ToList())
            {
                entityDB.ClientTemplateProfiles.Remove(clientTemplateProfile);
            }
            foreach (var clientTemplate in organization.ClientTemplates.ToList())
            {
                entityDB.ClientTemplates.Remove(clientTemplate);
            }
            foreach (var emailSetupNotification in organization.OrganizationsEmailSetupNotifications.ToList())
            {
                entityDB.OrganizationsEmailSetupNotifications.Remove(emailSetupNotification);
            }
            // Kiran on 8/22/2014
            if (organization.OrganizationsNotificationsSentVendorList != null)
            {
                foreach (var emaillog in organization.OrganizationsNotificationsSentVendorList.ToList())
                {
                    entityDB.OrganizationsNotificationsEmailLogs.Remove(emaillog);
                }
            }
            // Ends<<<
            foreach (var notificationSentClient in organization.OrganizationsNotificationsSentClientList.ToList())
            {
                entityDB.OrganizationsNotificationsSent.Remove(notificationSentClient);
            }
            foreach (var templateSubSec in organization.TemplateSubSections.ToList())
            {
                entityDB.TemplateSubSections.Remove(templateSubSec);
            }
            foreach (var prequalOpenPeriod in organization.OrganizationsPrequalificationOpenPeriod.ToList())
            {
                entityDB.OrganizationsPrequalificationOpenPeriod.Remove(prequalOpenPeriod);
            }
            //End of Siva on 29th Mar 2014

            // Kiran on 8/8/2014
            string fileName = organization.Name + "_" + OrganizationID;
            Directory.CreateDirectory(Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["WebResponseFiles"])));
            var path = Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["WebResponseFiles"]), fileName + ".xml");
            if (System.IO.File.Exists(path))
            {
                System.IO.File.Delete(path);
            }
            foreach (var archivedOrgIfo in organization.ArchiveOrganizationDetails.ToList())
            {
                entityDB.ArchiveOrganizationDetails.Remove(archivedOrgIfo);
            }
            foreach (var clientVendor in entityDB.ClientVendorReferenceIds.Where(rec => rec.VendorId == OrganizationID))
            {
                entityDB.ClientVendorReferenceIds.Remove(clientVendor);
            }
            // Ends<<<

            entityDB.Organizations.Remove(organization);
            entityDB.SaveChanges();

            return "DeleteComapnySuccess";
        }
        //public string deleteCompany(long OrganizationID)
        //{
        //    List<Prequalification> prequals = entityDB.Prequalification.Where(m => m.VendorId == OrganizationID).ToList();
        //    if (prequals.Count > 0)
        //    {
        //        return "PrequalificationsExist";
        //    }

        //    foreach (var prequalification in prequals)
        //    {
        //        entityDB.Prequalification.Remove(prequalification);
        //    }

        //    Organizations organization = entityDB.Organizations.Find(OrganizationID);

        //    List<SystemUsersOrganizations> sysUserOrgs = entityDB.SystemUsersOrganizations.Where(m => m.OrganizationId == OrganizationID).ToList();
        //    foreach (var sysuserorg in sysUserOrgs)
        //    {
        //        SystemUsers sysUser = sysuserorg.SystemUsers;
        //        List<Contact> contactsLocal = sysUser.Contacts.ToList();
        //        foreach (var contactLocal in contactsLocal)
        //        {
        //            entityDB.Contact.Remove(contactLocal);
        //        }

        //        List<SystemUserLogs> userLogs = entityDB.SystemUserLogs.Where(m => m.SystemUserId == sysUser.UserId).ToList();
        //        foreach (var userLog in userLogs)
        //        {
        //            entityDB.SystemUserLogs.Remove(userLog);
        //        }

        //        List<Document> docs = entityDB.Document.Where(m => m.OrganizationId == OrganizationID).ToList();
        //        foreach (var document in docs)
        //        {
        //            entityDB.Document.Remove(document);
        //        }

        //        //remove sysUserOrgRoles,
        //        List<SystemUsersOrganizationsRoles> orgRoles = entityDB.SystemUsersOrganizationsRoles.Where(m => m.SysUserOrgId == sysuserorg.SysUserOrganizationId).ToList();
        //        foreach (var orgRole in orgRoles)
        //        {
        //            entityDB.SystemUsersOrganizationsRoles.Remove(orgRole);
        //        }


        //        entityDB.SystemUsersOrganizations.Remove(sysuserorg);
        //        entityDB.SystemUsers.Remove(sysUser);

        //    }

        //    entityDB.Organizations.Remove(organization);
        //    entityDB.SaveChanges();

        //    return "DeleteComapnySuccess";
        //}

        //to display searchcompanyview
        //returns searchCompany view
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "MyAccount", sideMenuName = "Companies", ViewName = "SearchCompany")]
        [SessionExpire]
        public ActionResult SearchCompany()
        {
            ViewBag.isSuccess = false;
            return View();
        }

        //to retrieve and return company list base search criteria
        [SessionExpireForView]
        [OutputCache(Duration = 0)]
        public ActionResult GetCompanies(string companyName, string typeClient, string typeVendor, string typeAdmin)
        {
            ViewBag.companiesList = _orgBusiness.GetOrganizations(companyName, typeClient, typeVendor, typeAdmin);
            return PartialView("SearchCompanyResultsPartial");
        }


        ///////////////////////////
        /////////////////////////////
        /// Add New HeaderFooter ////

        //to display view to add introduction content
        //takes orgId as argument
        //retrieves and assigns templates as veiwbag elements
        //returns AddNewHeaderFooterView to be displayed to the user.
        // We are keeping the CSHTML name same hence AddNewHeaderFooter continues. The purpose has been pointed to ClientTemplatesProfiles
        [SessionExpireForView]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "MyAccount", sideMenuName = "Companies", ViewName = "AddNewHeaderFooter")]
        public ActionResult AddNewHeaderFooter(long OrganizationID, long sectionId, long headerFooterId, Guid templateId)
        {
            LocalAddNewClientTemplateProfile templateProfile = new LocalAddNewClientTemplateProfile();
            templateProfile.OrganizationID = OrganizationID;

            ViewBag.Templates = GetTemplateDetails(OrganizationID, templateId);

            if (headerFooterId == -1)
            {
                templateProfile.HeaderFooterId = headerFooterId;
                templateProfile.TemplateSectionId = sectionId;
                templateProfile.PositionType = 0; //to select header radion button by default
            }
            else
            {
                ClientTemplateProfiles existingProfile = entityDB.ClientTemplateProfiles.Find(headerFooterId);
                templateProfile.TemplateSectionId = existingProfile.TemplateSectionId;

                TemplateSections section = entityDB.TemplateSections.Find(existingProfile.TemplateSectionId);

                templateProfile.TemplateSectionName = section.SectionName;
                templateProfile.PositionType = existingProfile.PositionType;
                templateProfile.PositionTitle = existingProfile.PositionTitle;
                templateProfile.PostionContent = existingProfile.PositionContent;
                templateProfile.HeaderFooterId = headerFooterId;

            }

            return View(templateProfile);

        }

        [HttpPost]
        [SessionExpireForView]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "MyAccount", sideMenuName = "Companies", ViewName = "AddNewHeaderFooter")]
        public ActionResult AddNewHeaderFooter(LocalAddNewClientTemplateProfile form)
        {
            if (ModelState.IsValid)
            {
                TemplateSections section = new TemplateSections();
                if (form.TemplateSectionId == -1)
                {
                    section.TemplateID = form.TemplateId;
                    section.DisplayOrder = -1;
                    section.Visible = true;
                    section.SectionName = form.TemplateSectionName;
                    section.TemplateSectionType = 0;

                    //Rajesh on 2/11/2014
                    TemplateSectionsPermission permission = new TemplateSectionsPermission();
                    permission.VisibleTo = 3;
                    section.TemplateSectionsPermission = new List<TemplateSectionsPermission>();
                    section.TemplateSectionsPermission.Add(permission);
                    //Ends<<

                    entityDB.TemplateSections.Add(section);
                }
                else
                {
                    section = entityDB.TemplateSections.Find(form.TemplateSectionId);
                    section.TemplateSectionType = 0;
                    section.SectionName = form.TemplateSectionName;

                    entityDB.Entry(section).State = EntityState.Modified;
                }

                ClientTemplateProfiles templateProfile = new ClientTemplateProfiles();
                if (form.HeaderFooterId == -1)
                {
                    templateProfile.TemplateSectionId = section.TemplateSectionID;
                    templateProfile.PositionType = form.PositionType;
                    templateProfile.PositionTitle = form.PositionTitle;
                    templateProfile.PositionContent = HttpUtility.HtmlDecode(form.PostionContent);
                    templateProfile.AttachmentExist = false;
                    templateProfile.ClientId = form.OrganizationID;
                    templateProfile.ContentType = 0; //Dec 11th

                    entityDB.ClientTemplateProfiles.Add(templateProfile);
                }
                else
                {
                    templateProfile = entityDB.ClientTemplateProfiles.Find(form.HeaderFooterId);
                    templateProfile.PositionTitle = form.PositionTitle;
                    templateProfile.PositionContent = HttpUtility.HtmlDecode(form.PostionContent);

                    entityDB.Entry(templateProfile).State = EntityState.Modified;
                }

                entityDB.SaveChanges();

                return View("AddNewHeaderFooterSuccess", templateProfile);
            }
            else
            {
                ViewBag.Templates = GetTemplateDetails(form.OrganizationID, form.TemplateId);

                return View(form);
            }
        }


        [SessionExpireForView]
        [HttpPost]
        public string deleteHeaderFooter(long headerFooterId)
        {
            ClientTemplateProfiles profile = entityDB.ClientTemplateProfiles.Find(headerFooterId);
            if (profile != null)
            {
                entityDB.ClientTemplateProfiles.Remove(profile);
                entityDB.SaveChanges();

                return "DeleteHeaderFooterSucceded";
            }

            return "DeleteHeaderFooterFailed";
        }


        [SessionExpireForView]
        [HttpGet]
        [OutputCache(Duration = 0)]
        public ActionResult HeaderFootersListPartial(Guid templateId, long clientId)
        {
            List<LocalIntroContentTabModel> headerFootersOfTemplates = new List<LocalIntroContentTabModel>();
            Templates template = entityDB.Templates.Find(templateId);
            foreach (var section in template.TemplateSections.ToList())
            {
                if (section.TemplateSectionType == 0)
                {
                    List<ClientTemplateProfiles> clientProfiles = section.ClientTemplateProfiles.Where(m => m.ClientId == clientId).OrderBy(m => m.PositionType).ToList();

                    foreach (var profile in clientProfiles)
                    {
                        LocalIntroContentTabModel introcontent = new LocalIntroContentTabModel();
                        introcontent.HeaderFooterId = profile.ClientTemplateProfileId;
                        introcontent.PositionTitle = profile.PositionTitle;
                        introcontent.PostionContent = profile.PositionContent;
                        introcontent.PositionType = profile.PositionType;
                        introcontent.AttachmentExist = profile.AttachmentExist;
                        introcontent.TemplateSectionId = profile.TemplateSectionId;
                        introcontent.TemplateSections = profile.TemplateSections;
                        //introcontent.DisplayImages = profile.DisplayImages;

                        headerFootersOfTemplates.Add(introcontent);

                        if ((bool)profile.AttachmentExist)
                        {
                            List<Document> IntroDocs = entityDB.Document.Where(row => row.ReferenceType == "Client Profile" && row.ReferenceRecordID == profile.ClientTemplateProfileId).ToList();
                            introcontent.documents = IntroDocs;
                        }
                    }
                }
            }

            ViewBag.headerFootersOfTemplates = headerFootersOfTemplates;

            return PartialView();
        }


        //to get sections
        //takes templateSectionId and sectionid as parameters
        //returns list of templatesections
        public ActionResult getSectionsForHeaderFooter(Guid templateId, long sectionId)
        {
            if (sectionId == -1)
            {
                var sects = from secs in entityDB.TemplateSections.Where(secs => secs.TemplateID == templateId && secs.TemplateSectionType == 0).ToList()
                            select new SelectListItem
                            {
                                Text = secs.SectionName,
                                Value = secs.TemplateSectionID.ToString()
                            };
                var tempsecs = sects.ToList();
                if (tempsecs.Count == 0)
                {
                    SelectListItem defaultItem = new SelectListItem();
                    defaultItem.Text = "";
                    defaultItem.Value = "-1";
                    tempsecs.Insert(0, defaultItem);
                }

                return Json(tempsecs);
            }
            else
            {
                var sects = from secs in entityDB.TemplateSections.Where(secs => secs.TemplateID == templateId && secs.TemplateSectionID == sectionId && secs.TemplateSectionType == 0).ToList()
                            select new SelectListItem
                            {
                                Text = secs.SectionName,
                                Value = secs.TemplateSectionID.ToString()
                            };
                var tempsecs = sects.ToList();

                return Json(tempsecs);
            }


        }
        //to get List of sections
        //takes templateSectionId to be selected by default in drop-down list, and templateId as parameters
        //returns list of templatesections
        public List<SelectListItem> getSectionDetailsForHeaderFooter(Guid templateId, long templateSectionId)
        {
            var sects = from secs in entityDB.TemplateSections.Where(secs => secs.TemplateID == templateId && secs.TemplateSectionType == 0).ToList()

                        select new SelectListItem
                        {
                            Text = secs.SectionName,
                            Value = secs.TemplateSectionID.ToString(),
                            Selected = secs.TemplateSectionID == templateSectionId
                        };
            var tempsecs = sects.ToList();
            return tempsecs;
        }


        ///////////////////////////////
        // siva on Dec 10th
        //Prequalification Requirements
        [SessionExpireForView]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "MyAccount", sideMenuName = "Companies", ViewName = "AddNewPrequalRequirement")]
        public ActionResult AddNewPrequalRequirement(long OrganizationID, Guid templateId, long clientTemplateProfileId)
        {
            LocalAddNewPrequalRequirement prequalRequirement = new LocalAddNewPrequalRequirement();
            prequalRequirement.OrganizationID = OrganizationID;

            Templates template = entityDB.Templates.Find(templateId);
            prequalRequirement.TemplateID = templateId;
            prequalRequirement.TemplateName = template.TemplateName;
            prequalRequirement.ClientTemplateProfileId = clientTemplateProfileId;

            if (clientTemplateProfileId > 0)
            {
                ClientTemplateProfiles profile = entityDB.ClientTemplateProfiles.Find(clientTemplateProfileId);
                prequalRequirement.PrequalRequirementTitle = profile.PositionTitle;
                prequalRequirement.PrequalRequirementContent = profile.PositionContent;
            }

            return View(prequalRequirement);

        }

        [HttpPost]
        [SessionExpireForView]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "MyAccount", sideMenuName = "Companies", ViewName = "AddNewPrequalRequirement")]
        public ActionResult AddNewPrequalRequirement(LocalAddNewPrequalRequirement form)
        {
            if (ModelState.IsValid)
            {
                ClientTemplateProfiles newProfile;
                if (form.ClientTemplateProfileId > 0)
                {
                    newProfile = entityDB.ClientTemplateProfiles.Find(form.ClientTemplateProfileId);
                    newProfile.PositionTitle = form.PrequalRequirementTitle;
                    newProfile.PositionContent = HttpUtility.HtmlDecode(form.PrequalRequirementContent);

                    entityDB.Entry(newProfile).State = EntityState.Modified;
                }
                else
                {
                    newProfile = new ClientTemplateProfiles();

                    TemplateSections sectionWithType31 = entityDB.TemplateSections.FirstOrDefault(rec => rec.TemplateSectionType == 31 && rec.TemplateID == form.TemplateID);
                    if (sectionWithType31 == null)
                    {
                        sectionWithType31 = new TemplateSections();
                        sectionWithType31.TemplateID = form.TemplateID;
                        sectionWithType31.SectionName = @Resources.Resources.Companies_PrequalificationRequirements;
                        sectionWithType31.TemplateSectionType = 31;
                        sectionWithType31.Visible = true;
                        sectionWithType31.VisibleToClient = true;
                        sectionWithType31.DisplayOrder = 0;

                        //Rajesh on 2/11/2014
                        TemplateSectionsPermission permission = new TemplateSectionsPermission();
                        permission.VisibleTo = 3;
                        sectionWithType31.TemplateSectionsPermission = new List<TemplateSectionsPermission>();
                        sectionWithType31.TemplateSectionsPermission.Add(permission);
                        //Ends<<

                        entityDB.TemplateSections.Add(sectionWithType31);
                        entityDB.SaveChanges();
                    }

                    newProfile.PositionTitle = form.PrequalRequirementTitle;
                    newProfile.PositionContent = HttpUtility.HtmlDecode(form.PrequalRequirementContent);
                    newProfile.AttachmentExist = false;
                    newProfile.AttachmentType = 0;
                    newProfile.ClientId = form.OrganizationID;
                    newProfile.TemplateSectionId = sectionWithType31.TemplateSectionID;
                    newProfile.PositionType = 0; //Always add them to Header
                    newProfile.ContentType = 1;

                    entityDB.ClientTemplateProfiles.Add(newProfile);
                }

                entityDB.SaveChanges();

                return View("AddNewPrequalRequirementSuccess", newProfile);
            }
            else
            {
                return View(form);
            }

        }

        [SessionExpireForView]
        [HttpPost]
        public string deletePrequalRequirement(long templateProfileId)
        {
            ClientTemplateProfiles profile = entityDB.ClientTemplateProfiles.Find(templateProfileId);
            if (profile != null)
            {
                entityDB.ClientTemplateProfiles.Remove(profile);
                entityDB.SaveChanges();

                return "deletePrequalRequirementSucceded";
            }

            return "deletePrequalRequirementFailed";
        }

        [SessionExpireForView]
        [HttpGet]
        [OutputCache(Duration = 0)]
        public ActionResult PrequalRequirementsListPartial(Guid templateId, long clientId)
        {
            List<LocalIntroContentTabModel> PrequalRequirementsList = new List<LocalIntroContentTabModel>();
            Templates template = entityDB.Templates.Find(templateId);
            foreach (var section in template.TemplateSections.ToList())
            {
                if (section.TemplateSectionType == 31)
                {
                    List<ClientTemplateProfiles> clientProfiles = section.ClientTemplateProfiles.Where(m => m.ClientId == clientId && m.ContentType == 1).OrderBy(m => m.PositionType).ToList();

                    foreach (var profile in clientProfiles)
                    {
                        LocalIntroContentTabModel introcontent = new LocalIntroContentTabModel();
                        introcontent.HeaderFooterId = profile.ClientTemplateProfileId;
                        introcontent.PositionTitle = profile.PositionTitle;
                        introcontent.PostionContent = profile.PositionContent;
                        introcontent.PositionType = profile.PositionType;
                        introcontent.AttachmentExist = profile.AttachmentExist;
                        introcontent.TemplateSectionId = profile.TemplateSectionId;
                        introcontent.TemplateSections = profile.TemplateSections;
                        //introcontent.DisplayImages = profile.DisplayImages;

                        PrequalRequirementsList.Add(introcontent);

                        if ((bool)profile.AttachmentExist)
                        {
                            List<Document> IntroDocs = entityDB.Document.Where(row => row.ReferenceType == "Client Profile" && row.ReferenceRecordID == profile.ClientTemplateProfileId).ToList();
                            introcontent.documents = IntroDocs;
                        }
                    }
                }
            }

            ViewBag.PrequalRequirementsList = PrequalRequirementsList;

            return PartialView();
        }

        public SelectListItem[] GetStateSelectListItems(bool includeCanada = false)
        {
            var helper = new FVGen3.BusinessLogic.DropdDownDataSupplier();
            var states = helper.GetUnitedStatesTableData(-1, false, includeCanada);
            List<SelectListItem> selectLists = new List<SelectListItem>();
            //'<option>' + datasite + '</option>'
            states.ForEach(s => selectLists.Add(new SelectListItem() { Text = s.Key, Value = s.Value }));
            return selectLists.ToArray();
        }
     
         [HttpPost]
        public string BulkVendorInsertion(FormCollection form, HttpPostedFileBase file)
        {
            string result = new StreamReader(file.InputStream).ReadToEnd();
            var TemplateId = new Guid(form["Template"]);
            var Data = _orgBusiness.BulkVendorInsertion(result, TemplateId, SSession.UserId);
            return Data;


        }
        [HttpPost]
        public string BulkVendorLocationInsertion(FormCollection form, HttpPostedFileBase file)
        {
            string result = new StreamReader(file.InputStream).ReadToEnd();
            var TemplateId = new Guid(form["Template"]);
            var Data = _orgBusiness.BulkLocationInsertion(result, TemplateId, SSession.UserId);
            return Data;


        }
    }
}
