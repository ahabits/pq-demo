﻿/*
Page Created Date:  29/04/2013
Created By: Rajesh Pendela
Purpose:
Version: 1.0
****************************************************
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FVGen3.Domain.Abstract;
using FVGen3.Domain.Entities;

namespace FVGen3.WebUI.Controllers
{
    public class HomeController : Controller
    {
        private IContactsRepository repository;

        public HomeController(IContactsRepository contactRepository) 
        {
            this.repository = contactRepository;
        }

        public ViewResult List()
        {
            IQueryable<Contact> c = repository.Contact;
            return View(c);
        }

        public ActionResult Index()
        {
            return View();
        }

    }
}
