﻿/*
Page Created Date:  28/05/2013
Created By: Rajesh Pendela
Purpose:All operations of prequalification will be controlled here
Version: 1.0
****************************************************
Version: 2.0
Date: 07/25/2013
Changes by : Rajesh Pendela
Changes: Pop up view of Dynamic subsection for Prequalification sites
*****************************************************
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FVGen3.Domain.Abstract;
using FVGen3.Domain.Concrete;
using FVGen3.Domain.Entities;
using FVGen3.WebUI.Annotations;
using System.Data;
using System.Data.Objects;
using System.Data.Common;
using System.Data.EntityClient;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using FVGen3.WebUI.Utilities;
using System.Net.Mail;
using iTextSharp.text.pdf;
using System.Xml;
using System.Net;
using System.Text;
using FVGen3.BusinessLogic;
using FVGen3.DataLayer.DTO;
using AhaApps.Libraries.Extensions;
namespace FVGen3.WebUI.Controllers
{
    public class PrequalificationController : BaseController
    {

        //
        // GET: /Prequalification/
        EFDbContext entityDB;
        public PrequalificationController(ISystemUsersRepository repositorySystemUsers)
        {
            entityDB = new EFDbContext();
        }

        [SessionExpire(lastPageUrl = "../SystemUsers/Dashboard")]
        [HeaderAndSidebar(headerName = "PREQUALIFICATIONREQUIREMENTS", sideMenuName = "", ViewName = "TemplatesList")]
        public ActionResult TemplatesList(long PreQualificationId, long? oldPreQualificationId)//Rajesh on 9/21/2013
        {

            if (PreQualificationId == -1)//Means He clicked Renewal
            {
                //PreQualificationId = AddNewPrequalification(PreQualificationId);
                //Rajesh on 9/21/2013
                long tempPreQualificationId = AddNewPrequalification(oldPreQualificationId);
                if (tempPreQualificationId != -1)
                    PreQualificationId = tempPreQualificationId;
                //Ends<<<


                //To add Prequalification Logs
                Prequalification preQualificationDetails_loc = entityDB.Prequalification.Find(PreQualificationId);
                PrequalificationStatusChangeLog preQualificationLog = new PrequalificationStatusChangeLog();
                preQualificationLog.PrequalificationId = PreQualificationId;
                preQualificationLog.PrequalificationStatusId = 15;
                preQualificationLog.StatusChangeDate = DateTime.Now;
                preQualificationLog.StatusChangedByUser = new Guid(Session["UserId"].ToString());
                preQualificationLog.PrequalificationStart = preQualificationDetails_loc.PrequalificationStart;
                preQualificationLog.PrequalificationFinish = preQualificationDetails_loc.PrequalificationFinish;
                entityDB.PrequalificationStatusChangeLog.Add(preQualificationLog);
                entityDB.SaveChanges();
                //>>>Ends

                entityDB = new EFDbContext();
            }
            //This session value was created for Template sections list

            Prequalification preQualificationDetails = entityDB.Prequalification.Find(PreQualificationId);

            ViewBag.CopyAnswers = (preQualificationDetails.Vendor.CopyAnswers == null) ? 0 : (int)preQualificationDetails.Vendor.CopyAnswers;

            //////////////////

            long[] prequalificationStatus = { 1, 2 };
            int status = Array.IndexOf(prequalificationStatus, preQualificationDetails.PrequalificationStatusId);

            if (status == -1 && preQualificationDetails.ClientTemplates != null)
            {
                return Redirect("../Prequalification/TemplateSectionsList?templateId=" + preQualificationDetails.ClientTemplates.TemplateID + "&TemplateSectionID=-1&PreQualificationId=" + PreQualificationId);
            }



            if (preQualificationDetails.PrequalificationStatusId != 2)
            {
                //Rajesh DT:-8/7/2013>>>>
                preQualificationDetails.PrequalificationStart = DateTime.Now;
                DateTime FinishDateTime = new DateTime();
                var preStatus = entityDB.PrequalificationStatus.FirstOrDefault(rec => rec.PrequalificationStatusId == 2);
                if (preStatus != null)
                {
                    if (preStatus.PeriodLengthUnit.ToLower().Equals("d"))
                        FinishDateTime = DateTime.Now.AddDays(preStatus.PeriodLength);
                    else if (preStatus.PeriodLengthUnit.ToLower().Equals("m"))
                        FinishDateTime = DateTime.Now.AddMonths(preStatus.PeriodLength);
                    else if (preStatus.PeriodLengthUnit.ToLower().Equals("y"))
                        FinishDateTime = DateTime.Now.AddYears(preStatus.PeriodLength);
                }
                preQualificationDetails.PrequalificationFinish = FinishDateTime;
                // Ends <<<<<



                //To add Prequalification Logs
                PrequalificationStatusChangeLog preQualificationLog = new PrequalificationStatusChangeLog();
                preQualificationLog.PrequalificationId = preQualificationDetails.PrequalificationId;
                preQualificationLog.PrequalificationStatusId = 2;
                preQualificationLog.StatusChangeDate = DateTime.Now;
                preQualificationLog.StatusChangedByUser = new Guid(Session["UserId"].ToString());
                preQualificationLog.PrequalificationStart = preQualificationDetails.PrequalificationStart;
                preQualificationLog.PrequalificationFinish = preQualificationDetails.PrequalificationFinish;
                entityDB.PrequalificationStatusChangeLog.Add(preQualificationLog);
                //>>>Ends
            }

            preQualificationDetails.PrequalificationStatusId = 2;
            ViewBag.PreQualificationId = PreQualificationId;
            entityDB.Entry(preQualificationDetails).State = EntityState.Modified;
            entityDB.SaveChanges();
            ViewBag.templatesList = entityDB.ClientTemplates.Where(records => records.ClientID == preQualificationDetails.ClientId && records.Templates.TemplateType == 0 && records.Templates.TemplateStatus == 1).OrderBy(rec => rec.Templates.TemplateSequence).ToList(); // Kiran on 03/13/2015 to sort the templates by template name
            return View();
        }

        private long AddNewPrequalification(long? oldPreQualificationId)//Rajesh on 9/21/2013
        {

            if (oldPreQualificationId == null || oldPreQualificationId == -1)//Rajesh on 9/21/2013
                return -1;//Rajesh on 9/21/2013

            var oldPrequalification = entityDB.Prequalification.Find(oldPreQualificationId);

            Prequalification p = new Prequalification();
            p.ClientId = oldPrequalification.ClientId;
            p.ClientTemplateId = oldPrequalification.ClientTemplateId;
            p.PrequalificationCreate = DateTime.Now;
            p.locked = oldPrequalification.ClientTemplates.Templates.LockedByDefault;

            DateTime StartDateTime = new DateTime();
            var preStatus = entityDB.PrequalificationStatus.FirstOrDefault(rec => rec.PrequalificationStatusId == 15);
            if (preStatus != null)
            {
                if (preStatus.PeriodLengthUnit.ToLower().Equals("d"))
                    StartDateTime = oldPrequalification.PrequalificationFinish.AddDays(-preStatus.PeriodLength);
                else if (preStatus.PeriodLengthUnit.ToLower().Equals("m"))
                    StartDateTime = oldPrequalification.PrequalificationFinish.AddMonths(-preStatus.PeriodLength);
                else if (preStatus.PeriodLengthUnit.ToLower().Equals("y"))
                    StartDateTime = oldPrequalification.PrequalificationFinish.AddYears(-preStatus.PeriodLength);
            }
            p.PrequalificationFinish = oldPrequalification.PrequalificationFinish;
            p.PrequalificationStart = StartDateTime; p.PrequalificationStatusId = 15;
            p.PrequalificationStatusId = 15;

            //Ends<<<
            p.PrequalificationUserInputs = new List<PrequalificationUserInput>();
            p.UserIdAsCompleter = oldPrequalification.UserIdAsCompleter;
            p.VendorId = oldPrequalification.VendorId;
            entityDB.Prequalification.Add(p);
            //entityDB.SaveChanges();

            if (oldPrequalification.Vendor.CopyAnswers != null && oldPrequalification.Vendor.CopyAnswers == 2)
            {

                //To Get oldprequalification EMRYears
                //var EMRYearsIDs = (from EMRYears in oldPrequalification.PrequalificationEMRStatsYears.ToList()
                //                   select EMRYears.PrequalEMRStatsYearId).ToList();
                var EMRYearsIDs = oldPrequalification.PrequalificationEMRStatsYears.Select(rec => rec.PrequalEMRStatsYearId).ToList();
                //var ColumnIds = (from EMRValues in entityDB.PrequalificationEMRStatsValues.Where(rec => EMRYearsIDs.Contains(rec.PrequalEMRStatsYearId)).ToList()
                //                 select EMRValues.QuestionColumnId).ToList();
                var ColumnIds = entityDB.PrequalificationEMRStatsValues.Where(rec => EMRYearsIDs.Contains(rec.PrequalEMRStatsYearId)).Select(rec => rec.QuestionColumnId).ToList();
                foreach (var UserInput in oldPrequalification.PrequalificationUserInputs.Where(rec => !ColumnIds.Contains(rec.QuestionColumnId)).ToList())
                {
                    PrequalificationUserInput inputVal = new PrequalificationUserInput();
                    inputVal.PreQualificationId = p.PrequalificationId;
                    inputVal.QuestionColumnId = UserInput.QuestionColumnId;
                    inputVal.UserInput = UserInput.UserInput;
                    entityDB.PrequalificationUserInput.Add(inputVal);
                }

                foreach (var reportingData in oldPrequalification.PrequalificationReportingData.ToList())
                {
                    var ReportingData_Loc = new PrequalificationReportingData();
                    ReportingData_Loc.PrequalificationId = p.PrequalificationId;
                    ReportingData_Loc.ClientId = p.ClientId;
                    ReportingData_Loc.VendorId = p.VendorId;
                    ReportingData_Loc.ReportingType = reportingData.ReportingType;
                    ReportingData_Loc.ReportingDataId = reportingData.ReportingDataId;
                    entityDB.PrequalificationReportingData.Add(ReportingData_Loc);
                }
                foreach (var sites in oldPrequalification.PrequalificationSites.ToList())
                {
                    PrequalificationSites site = new PrequalificationSites();
                    site.PrequalificationId = p.PrequalificationId;
                    site.VendorId = sites.VendorId;
                    site.ClientBULocation = sites.ClientBULocation;
                    site.ClientBusinessUnitId = sites.ClientBusinessUnitId;
                    site.ClientBusinessUnitSiteId = sites.ClientBusinessUnitSiteId;
                    site.ClientId = sites.ClientId;
                    site.Comment = sites.Comment;
                    entityDB.PrequalificationSites.Add(site);
                }
                //Adding Documents


            }

            entityDB.SaveChanges();
            var QuestionWith1000 = new Questions();
            //Update EMRYears using questionId 1000
            var templateSections = oldPrequalification.ClientTemplates.Templates.TemplateSections.Where(rec => rec.TemplateSectionType != 34 && !rec.SectionName.EndsWith("approval", StringComparison.CurrentCultureIgnoreCase)).ToList();
            foreach (var TemplateSection in templateSections)
            {
                var YearWiseEMR = TemplateSection.TemplateSubSections.FirstOrDefault(rec => rec.SubSectionTypeCondition == "YearWiseStats");
                if (YearWiseEMR != null)
                {
                    var Question = YearWiseEMR.Questions.FirstOrDefault(rec => rec.QuestionBankId == 1000);
                    QuestionWith1000 = Question;
                    var CurrentYear = DateTime.Now.Year;
                    foreach (var QColumnDetails in Question.QuestionColumnDetails.OrderBy(rec => rec.ColumnNo).ToList())
                    {
                        PrequalificationUserInput PUserInput = entityDB.PrequalificationUserInput.FirstOrDefault(rec => rec.PreQualificationId == p.PrequalificationId && rec.QuestionColumnId == QColumnDetails.QuestionColumnId);
                        if (PUserInput != null)
                        {
                            PUserInput.UserInput = --CurrentYear + "";
                            entityDB.Entry(PUserInput).State = EntityState.Modified;
                        }
                        else
                        {
                            PUserInput = new PrequalificationUserInput();
                            PUserInput.PreQualificationId = p.PrequalificationId;
                            PUserInput.QuestionColumnId = QColumnDetails.QuestionColumnId;
                            PUserInput.UserInput = --CurrentYear + "";
                            entityDB.PrequalificationUserInput.Add(PUserInput);
                        }


                    }
                }
            }

            entityDB.SaveChanges();
            if (QuestionWith1000.QuestionColumnDetails != null)
                foreach (var QcolumnDetails in QuestionWith1000.QuestionColumnDetails.ToList())
                {
                    var QCValue = entityDB.PrequalificationUserInput.FirstOrDefault(rec => rec.PreQualificationId == p.PrequalificationId && rec.QuestionColumnId == QcolumnDetails.QuestionColumnId).UserInput;
                    if (QCValue != null)
                    {
                        var EMRYear = entityDB.PrequalificationEMRStatsYears.FirstOrDefault(rec => rec.PrequalificationId == oldPrequalification.PrequalificationId && rec.EMRStatsYear == QCValue);
                        if (EMRYear != null)
                        {

                            foreach (var EMRValues in EMRYear.PrequalificationEMRStatsValues.ToList())
                            {
                                var QuestionColumnDetails = EMRValues.Questions.QuestionColumnDetails.FirstOrDefault(rec => rec.ColumnNo == QcolumnDetails.ColumnNo);
                                PrequalificationUserInput PUserInput = entityDB.PrequalificationUserInput.FirstOrDefault(rec => rec.PreQualificationId == p.PrequalificationId && rec.QuestionColumnId == QuestionColumnDetails.QuestionColumnId);
                                if (PUserInput != null)
                                {
                                    PUserInput.UserInput = EMRValues.QuestionColumnIdValue + "";
                                    entityDB.Entry(PUserInput).State = EntityState.Modified;
                                }
                                else
                                {
                                    PUserInput = new PrequalificationUserInput();
                                    PUserInput.PreQualificationId = p.PrequalificationId;
                                    PUserInput.QuestionColumnId = QuestionColumnDetails.QuestionColumnId;
                                    PUserInput.UserInput = EMRValues.QuestionColumnIdValue + "";
                                    entityDB.PrequalificationUserInput.Add(PUserInput);
                                }
                            }
                        }
                    }
                }
            entityDB.SaveChanges();


            return p.PrequalificationId;
        }

        [SessionExpire(lastPageUrl = "../SystemUsers/Dashboard")]
        public ActionResult StartTemplate(Guid ClientTemplateId, long PrequalificationId, bool? copyAnswer, long? client, string selectedDocs)
        {

            var CTemplate = entityDB.ClientTemplates.Find(ClientTemplateId);
            Prequalification preQualificationDetails = entityDB.Prequalification.Find(PrequalificationId);
            preQualificationDetails.ClientTemplateId = ClientTemplateId;
            preQualificationDetails.locked = CTemplate.Templates.LockedByDefault;
            //Mani on 8/19/2015
            decimal? clientTemplateBUGroupPrice = 0.0M;
            try
            {
                clientTemplateBUGroupPrice = entityDB.ClientTemplatesBUGroupPrice.FirstOrDefault(rec => rec.ClientTemplateID == ClientTemplateId && rec.GroupPriceType == 0 && rec.GroupingFor == 0).GroupPrice;
            }
            catch { }
            // Ends<<<
            preQualificationDetails.TotalInvoiceAmount = clientTemplateBUGroupPrice; // Kiran on 02/24/2015



            if (preQualificationDetails.PrequalificationStatusId != 3)
            {
                //Rajesh DT:-8/7/2013>>>>
                preQualificationDetails.PrequalificationStart = DateTime.Now;
                DateTime FinishDateTime = new DateTime();
                var preStatus = entityDB.PrequalificationStatus.FirstOrDefault(rec => rec.PrequalificationStatusId == 3);
                if (preStatus != null)
                {
                    if (preStatus.PeriodLengthUnit.ToLower().Equals("d"))
                        FinishDateTime = DateTime.Now.AddDays(preStatus.PeriodLength);
                    else if (preStatus.PeriodLengthUnit.ToLower().Equals("m"))
                        FinishDateTime = DateTime.Now.AddMonths(preStatus.PeriodLength);
                    else if (preStatus.PeriodLengthUnit.ToLower().Equals("y"))
                        FinishDateTime = DateTime.Now.AddYears(preStatus.PeriodLength);
                }
                preQualificationDetails.PrequalificationFinish = FinishDateTime;
                //<<<<<

                //To add Prequalification Logs
                PrequalificationStatusChangeLog preQualificationLog = new PrequalificationStatusChangeLog();
                preQualificationLog.PrequalificationId = preQualificationDetails.PrequalificationId;
                preQualificationLog.PrequalificationStatusId = 3;
                preQualificationLog.StatusChangeDate = DateTime.Now;
                preQualificationLog.StatusChangedByUser = new Guid(Session["UserId"].ToString());
                preQualificationLog.PrequalificationStart = preQualificationDetails.PrequalificationStart;
                preQualificationLog.PrequalificationFinish = preQualificationDetails.PrequalificationFinish;
                entityDB.PrequalificationStatusChangeLog.Add(preQualificationLog);
                //>>>Ends


                foreach (var TSections in CTemplate.Templates.TemplateSections.ToList())
                {
                    var TSubSection = TSections.TemplateSubSections.FirstOrDefault(rec => rec.SubSectionTypeCondition == "YearWiseStats");
                    if (TSubSection != null)
                    {
                        var YearQuestion = TSubSection.Questions.FirstOrDefault(rec => rec.QuestionBankId == 1000);
                        if (YearQuestion != null)
                        {
                            var CurrentYear = DateTime.Now.Year;
                            foreach (var QColumnDetails in YearQuestion.QuestionColumnDetails.OrderBy(rec => rec.ColumnNo).ToList())
                            {
                                PrequalificationUserInput PUserInput = new PrequalificationUserInput();
                                PUserInput.PreQualificationId = preQualificationDetails.PrequalificationId;
                                PUserInput.QuestionColumnId = QColumnDetails.QuestionColumnId;
                                PUserInput.UserInput = --CurrentYear + "";
                                entityDB.PrequalificationUserInput.Add(PUserInput);
                            }
                        }
                    }
                }

            }
            preQualificationDetails.PrequalificationStatusId = 3;

            entityDB.Entry(preQualificationDetails).State = EntityState.Modified;



            if (copyAnswer != null && copyAnswer == true)
            {
                var clientId = Convert.ToInt64(client);
                var selectedPreQualificationId = entityDB.Prequalification.FirstOrDefault(rec => rec.ClientId == clientId && rec.VendorId == preQualificationDetails.VendorId).PrequalificationId;

                //var templateSupportedBiddings = (from reporting in CTemplate.ClientTemplateReportingData.Where(rec => rec.ReportingType == 0)
                //                                 select reporting.ReportingTypeId).ToList();
                //var templateSupportedProficiency = (from reporting in CTemplate.ClientTemplateReportingData.Where(rec => rec.ReportingType == 1)
                //                                    select reporting.ReportingTypeId).ToList();
                var templateSupportedBiddings = CTemplate.ClientTemplateReportingData.Where(rec => rec.ReportingType == 0).Select(rec => rec.ReportingTypeId);

                var templateSupportedProficiency = CTemplate.ClientTemplateReportingData.Where(rec => rec.ReportingType == 1).Select(rec => rec.ReportingTypeId).ToList();

                //Copy prequalification user input
                foreach (var UserInput in entityDB.PrequalificationUserInput.Where(rec => rec.PreQualificationId == selectedPreQualificationId).ToList())
                {//rec.TemplateSectionType != 34 && !rec.SectionName.EndsWith("approval",StringComparison.CurrentCultureIgnoreCase
                    var templateSection = UserInput.QuestionColumnDetails.Questions.TemplateSubSections.TemplateSections;
                    if (templateSection.TemplateSectionType == 34 ||
                        templateSection.SectionName.EndsWith("approval", StringComparison.CurrentCultureIgnoreCase))
                        continue;
                    if (templateSection.TemplateSectionType == 6 || templateSection.TemplateSectionType == 8)//Bidding
                    {
                        if (UserInput.QuestionColumnDetails.Questions.QuestionsBank.QuestionReference == 0 && templateSupportedBiddings.Contains((long)UserInput.QuestionColumnDetails.Questions.QuestionsBank.QuestionReferenceId))
                        {
                            PrequalificationUserInput inputValue = new PrequalificationUserInput();
                            inputValue.PreQualificationId = preQualificationDetails.PrequalificationId;
                            inputValue.QuestionColumnId = UserInput.QuestionColumnId;
                            inputValue.UserInput = UserInput.UserInput;
                            entityDB.PrequalificationUserInput.Add(inputValue);


                            //Adding Reportin data
                            var newReport = new PrequalificationReportingData();
                            newReport.PrequalificationId = preQualificationDetails.PrequalificationId;
                            newReport.ClientId = clientId;
                            newReport.VendorId = preQualificationDetails.VendorId;
                            newReport.ReportingType = 0;
                            newReport.ReportingDataId = UserInput.QuestionColumnDetails.Questions.QuestionsBank.QuestionReferenceId;
                            entityDB.PrequalificationReportingData.Add(newReport);
                        }
                        else
                            continue;
                    }
                    else if (templateSection.TemplateSectionType == 5)//Proficiency
                    {
                        if (UserInput.QuestionColumnDetails.Questions.QuestionsBank.QuestionReference == 1 && templateSupportedProficiency.Contains((long)UserInput.QuestionColumnDetails.Questions.QuestionsBank.QuestionReferenceId))
                        {
                            PrequalificationUserInput inputValue = new PrequalificationUserInput();
                            inputValue.PreQualificationId = preQualificationDetails.PrequalificationId;
                            inputValue.QuestionColumnId = UserInput.QuestionColumnId;
                            inputValue.UserInput = UserInput.UserInput;
                            entityDB.PrequalificationUserInput.Add(inputValue);


                            //Adding Reportin data
                            var newReport = new PrequalificationReportingData();
                            newReport.PrequalificationId = preQualificationDetails.PrequalificationId;
                            newReport.ClientId = clientId;
                            newReport.VendorId = preQualificationDetails.VendorId;
                            newReport.ReportingType = 1;
                            newReport.ReportingDataId = UserInput.QuestionColumnDetails.Questions.QuestionsBank.QuestionReferenceId;
                            entityDB.PrequalificationReportingData.Add(newReport);
                        }
                        else
                            continue;

                    }
                    else
                    {
                        PrequalificationUserInput inputVal = new PrequalificationUserInput();
                        inputVal.PreQualificationId = preQualificationDetails.PrequalificationId;
                        inputVal.QuestionColumnId = UserInput.QuestionColumnId;
                        inputVal.UserInput = UserInput.UserInput;
                        entityDB.PrequalificationUserInput.Add(inputVal);
                    }
                }

                //Adding Documents
                if (selectedDocs != null)
                {
                    foreach (var doc in selectedDocs.Split(','))
                    {
                        try
                        {
                            Guid id = new Guid();
                            var docId = new Guid(doc.Replace("Doc_", ""));
                            var sourceDoc = entityDB.Document.Find(docId);
                            var destinationDoc = new Document();
                            destinationDoc.DocumentTypeId = sourceDoc.DocumentTypeId;
                            destinationDoc.OrganizationId = sourceDoc.OrganizationId;
                            destinationDoc.SystemUserAsUploader = sourceDoc.SystemUserAsUploader;
                            destinationDoc.ReferenceType = sourceDoc.ReferenceType;
                            destinationDoc.ReferenceRecordID = preQualificationDetails.PrequalificationId;//This Only Changed
                            destinationDoc.DocumentName = sourceDoc.DocumentName;
                            destinationDoc.Uploaded = sourceDoc.Uploaded;
                            destinationDoc.Expires = sourceDoc.Expires;
                            destinationDoc.DocumentId = id;
                            entityDB.Document.Add(destinationDoc);


                            var dotIndex = sourceDoc.DocumentName.LastIndexOf('.');
                            var extension = sourceDoc.DocumentName.Substring(dotIndex);
                            var path = Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["ClientUploadsPath"]), sourceDoc.DocumentId + extension);
                            var dest = Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["ClientUploadsPath"]), id + extension);

                            System.IO.File.Copy(path, dest, true);
                        }
                        catch { }
                    }

                }

                //Adding Sites

                //foreach (var site in entityDB.PrequalificationSites.Where(rec => rec.PrequalificationId == selectedPreQualificationId).ToList())
                //{
                //    var newSite = new PrequalificationSites();
                //    newSite.PrequalificationId = preQualificationDetails.PrequalificationId;
                //    newSite.ClientId = clientId;
                //    newSite.VendorId = preQualificationDetails.VendorId;
                //    newSite.ClientBULocation = site.ClientBULocation;
                //    newSite.ClientBusinessUnitId = site.ClientBusinessUnitId;
                //    newSite.ClientBusinessUnitSiteId = site.ClientBusinessUnitSiteId;
                //    newSite.Comment = site.Comment;
                //    entityDB.PrequalificationSites.Add(newSite);
                //}



            }
            entityDB.SaveChanges();


            //Check to see if this  is locked. If so display message and redirect to dashboard.
            if (preQualificationDetails.locked != null && preQualificationDetails.locked == true)
            {
                SendUnlockRequestNotification(preQualificationDetails.PrequalificationId, preQualificationDetails);
                return Redirect("../SystemUsers/Dashboard");
            }
            return Redirect("../Prequalification/TemplateSectionsList?templateId=" + preQualificationDetails.ClientTemplates.TemplateID + "&TemplateSectionID=-1&PreQualificationId=" + PrequalificationId);
        }



        private void SendUnlockRequestNotification(long PreQualificationId, Prequalification currentPreQualification)
        {
            //To Send Notifications for particular sections
            //Rajesh on 2/17/2014

            //if (Session["RoleName"].ToString().Equals("Vendor") || Session["RoleName"].ToString().Equals("Client"))
            {


                string adminEmails = currentPreQualification.ClientTemplates.Templates.unlockNotificationAdminEmails.ToStringNullSafe();

                if (!String.IsNullOrWhiteSpace(adminEmails))
                {
                    var template = entityDB.EmailTemplates.Find(currentPreQualification.ClientTemplates.Templates.EmaiTemplateId);
                    if (template == null) return;


                    var subject = template.EmailSubject;
                    var body = template.EmailBody;
                    var Comment = template.CommentText;
                    var clientEmail = "";
                    var strMessage = "";
                    var emailsToSent = ""; // Kiran on 9/23/2014
                    var clientSysUserOrgs = currentPreQualification.Client.SystemUsersOrganizations;
                    foreach (var clientUserOrg in clientSysUserOrgs)
                    {
                        var clientOrgRoles = clientUserOrg.SystemUsersOrganizationsRoles;
                        foreach (var clientOrgRole in clientOrgRoles)
                        {
                            if (clientOrgRole.SystemRoles.RoleName.Equals("Super User"))
                            {
                                clientEmail = clientUserOrg.SystemUsers.Email;
                            }
                        }
                    }
                    subject = subject.Replace("[Date]", DateTime.Now + "");
                    subject = subject.Replace("[VendorName]", currentPreQualification.Vendor.Name);
                    subject = subject.Replace("[ClientName]", currentPreQualification.Client.Name);
                    subject = subject.Replace("[CustomerServicePhone]", currentPreQualification.Client.PhoneNumber);
                    subject = subject.Replace("[CustomerServiceEmail]", clientEmail);

                    body = body.Replace("[Date]", DateTime.Now + "");
                    body = body.Replace("[VendorName]", currentPreQualification.Vendor.Name);
                    body = body.Replace("[ClientName]", currentPreQualification.Client.Name);
                    body = body.Replace("[CustomerServicePhone]", currentPreQualification.Client.PhoneNumber);
                    body = body.Replace("[CustomerServiceEmail]", clientEmail);

                    strMessage = SendMail.sendMail(adminEmails, body, subject);


                }
            }
            //Ends<<<
        }

        //Sub Section Id is for present selected Id
        [SessionExpire]
        [HeaderAndSidebar(headerName = "PREQUALIFICATIONREQUIREMENTS", sideMenuName = "", ViewName = "TemplateSectionsList")]
        public ActionResult TemplateSectionsList(Guid templateId, long TemplateSectionID, long PreQualificationId, int? mode)//null or 0 :-edit mode 1-View Mode
        {

            //Rajesh on 2/13/2014
            //To get all templates sections permission with visible to vendor/client/admin
            //var allVisibleTemplateSections = (from templateSecPermission in entityDB.TemplateSectionsPermission.Where(rec => rec.VisibleTo == 3 || rec.VisibleTo == 1).ToList()
            //                                  select templateSecPermission.TemplateSectionID).ToList();
            long originalTemplateSectionId = TemplateSectionID;
            Prequalification preQualificationDetails = entityDB.Prequalification.Find(PreQualificationId);

            Guid userId = new Guid(Session["UserId"].ToString());
            //var userhasRestrictedSectionAccess = FVGen3.BusinessLogic.RolesHelper.IsUserInRole(userId, 1001, "read", preQualificationDetails.ClientId, preQualificationDetails.VendorId);
            ViewBag.RestrictedSectionEdit = FVGen3.BusinessLogic.RolesHelper.IsUserInRole(userId, 1001, "read", preQualificationDetails.ClientId, preQualificationDetails.VendorId);

            var allVisibleTemplateSections = entityDB.TemplateSectionsPermission.Where(rec => rec.VisibleTo == 3 || rec.VisibleTo == 1).Select(rec => rec.TemplateSectionID).ToList();
            //if (userhasRestrictedSectionAccess)
            if (Session["RoleName"].ToString().Equals("Client") || Session["RoleName"].ToString().Equals("Admin"))
            {
                allVisibleTemplateSections.AddRange(entityDB.TemplateSectionsPermission.Where(rec => rec.VisibleTo == 4).Select(rec => rec.TemplateSectionID).ToList());
            }
            else//Vendor have the access of access level 2 section.
            {
                allVisibleTemplateSections.AddRange(entityDB.TemplateSectionsPermission.Where(rec => rec.VisibleTo == LocalConstants.SectionPermissionType.RESTRICTED_ACCESS_2).Select(rec => rec.TemplateSectionID).ToList());
            }
            //Ends<<<





            //If any other user try to enter any other url we check that user belogs to this vendor or not

            var loginUserId_loc = new Guid(Session["UserId"].ToString());
            var loginOrgDetails = entityDB.SystemUsersOrganizations.FirstOrDefault(rec => rec.UserId == loginUserId_loc);

            if (preQualificationDetails.VendorId != loginOrgDetails.OrganizationId && Session["RoleName"].ToString() == "Vendor")
            {
                return Redirect("../SystemUsers/DashBoard");
            }
            if (preQualificationDetails.ClientId != loginOrgDetails.OrganizationId && Session["RoleName"].ToString() == "Client")
            {
                return Redirect("../SystemUsers/DashBoard");
            }
            //Ends<<<




            try
            {
                ViewBag.AdminVenUserId = preQualificationDetails.Vendor.SystemUsersOrganizations.FirstOrDefault(rec => rec.PrimaryUser == true).SystemUsers.UserId;
            }
            catch { }
            try { ViewBag.AdminVenContactId = preQualificationDetails.Vendor.SystemUsersOrganizations.FirstOrDefault(rec => rec.PrimaryUser == true).SystemUsers.Contacts.FirstOrDefault().ContactId; }
            catch { }
            //Templates selectedTemplate = entityDB.Templates.Find(templateId);
            Templates selectedTemplate = preQualificationDetails.ClientTemplates.Templates;
            try
            {
                if (TemplateSectionID == -1)//If TemplateSectionID=-1 then 1st one is selected one
                {
                    TemplateSectionID = selectedTemplate.TemplateSections.Where(rec => rec.Visible == true).ToList().OrderBy(rec => rec.DisplayOrder).FirstOrDefault().TemplateSectionID;
                    //Rajesh on 2/13/2014
                    if (Session["RoleName"].ToString().Equals("Vendor"))//If Vendor Login
                    {
                        TemplateSectionID = selectedTemplate.TemplateSections.Where(rec => rec.Visible == true && allVisibleTemplateSections.Contains(rec.TemplateSectionID)).ToList().OrderBy(rec => rec.DisplayOrder).FirstOrDefault().TemplateSectionID;
                    }
                    //Ends<<<
                    //var completedSections = (from completedSec in entityDB.PrequalificationCompletedSections.Where(rec => rec.PrequalificationId == preQualificationDetails.PrequalificationId).ToList()
                    //                         select completedSec.TemplateSectionId).ToList();

                    var completedSections = entityDB.PrequalificationCompletedSections.Where(rec => rec.PrequalificationId == preQualificationDetails.PrequalificationId).Select(rec => rec.TemplateSectionId).ToList();


                    //To get In completed sections
                    TemplateSections TemplateSectionsList = selectedTemplate.TemplateSections.Where(rec => rec.Visible == true).OrderBy(rec => rec.DisplayOrder).FirstOrDefault(rec => !completedSections.Contains(rec.TemplateSectionID));
                    //Rajesh on 2/13/2014
                    if (Session["RoleName"].ToString().Equals("Vendor"))//If Vendor Login
                    {
                        TemplateSectionsList = selectedTemplate.TemplateSections.Where(rec => rec.Visible == true && allVisibleTemplateSections.Contains(rec.TemplateSectionID)).OrderBy(rec => rec.DisplayOrder).FirstOrDefault(rec => !completedSections.Contains(rec.TemplateSectionID));
                    }
                    //Ends<<<
                    if (TemplateSectionsList != null)
                    {
                        //Rajesh on 1/21/2014
                        if (TemplateSectionsList.TemplateSectionType == 35)
                        {
                            TemplateSectionsList = selectedTemplate.TemplateSections.Where(rec => rec.Visible == true).OrderBy(rec => rec.DisplayOrder).FirstOrDefault(rec => !completedSections.Contains(rec.TemplateSectionID) && rec.TemplateSectionType != 35);
                            //Rajesh on 2/13/2014
                            if (Session["RoleName"].ToString().Equals("Vendor"))//If Vendor Login
                            {
                                TemplateSectionsList = selectedTemplate.TemplateSections.Where(rec => rec.Visible == true && allVisibleTemplateSections.Contains(rec.TemplateSectionID)).OrderBy(rec => rec.DisplayOrder).FirstOrDefault(rec => !completedSections.Contains(rec.TemplateSectionID) && rec.TemplateSectionType != 35);
                            }
                            //Ends<<
                        }
                        //Ends<<<

                        if (preQualificationDetails.RequestForMoreInfo == null || preQualificationDetails.RequestForMoreInfo == false)
                        {
                            if (TemplateSectionsList.TemplateSectionType == 34)
                            {
                                TemplateSectionsList = selectedTemplate.TemplateSections.Where(rec => rec.Visible == true).OrderBy(rec => rec.DisplayOrder).FirstOrDefault(rec => !completedSections.Contains(rec.TemplateSectionID) && rec.TemplateSectionType != 34);
                                //Rajesh on 2/13/2014
                                if (Session["RoleName"].ToString().Equals("Vendor"))//If Vendor Login
                                {
                                    TemplateSectionsList = selectedTemplate.TemplateSections.Where(rec => rec.Visible == true && allVisibleTemplateSections.Contains(rec.TemplateSectionID)).OrderBy(rec => rec.DisplayOrder).FirstOrDefault(rec => !completedSections.Contains(rec.TemplateSectionID) && rec.TemplateSectionType != 34);
                                }
                                //Ends<<
                            }
                        }
                        if (TemplateSectionsList != null)
                            TemplateSectionID = TemplateSectionsList.TemplateSectionID;
                    }
                }
            }
            catch (Exception ee)
            {
            }

            //Session["RoleName"]
            if (Session["RoleName"].ToString().Equals("Vendor"))//If vendor login
            {
                if (preQualificationDetails.PaymentReceived == null || preQualificationDetails.PaymentReceived == false)//means vendor not payment
                {
                    var templateSecType = entityDB.TemplateSections.Find(TemplateSectionID).TemplateSectionType;
                    if (templateSecType == 0 || templateSecType == 3 || templateSecType == 31 || templateSecType == 32)//if user try to open isprofile or payment
                    {
                        //Do nothing
                    }
                    else if (entityDB.Templates.Find(templateId).TemplateSections.FirstOrDefault(rec => rec.TemplateSectionType == 3) != null)//Means it has payment section
                    {
                        TemplateSectionID = entityDB.Templates.Find(templateId).TemplateSections.FirstOrDefault(rec => rec.TemplateSectionType == 3).TemplateSectionID;
                        ViewBag.PaymentError = true;
                    }
                }
                if (preQualificationDetails.PrequalificationStatusId >= 5 && preQualificationDetails.PrequalificationStatusId != 7 && preQualificationDetails.PrequalificationStatusId != 15)//means he submited prequalification//Rajesh on 3/24/2014
                {
                    mode = 1;//He can view sections
                }
                // Kiran on 03/09/2015
                if (preQualificationDetails.PrequalificationStatusId == 4)
                    mode = 1;
                // Kiran on 03/09/2015 Ends<<<

                // Rajesh on 01/20/2015 to open Exception/Probation section forever.
                var tSection =
                    entityDB.TemplateSections.FirstOrDefault(rec => rec.TemplateSectionID == TemplateSectionID);
                if (Session["RoleName"].ToString().Equals("Vendor") && tSection.TemplateSectionType == 34)
                {
                    if (
                        !entityDB.PrequalificationCompletedSections.Any(
                            rec =>
                                rec.PrequalificationId == PreQualificationId &&
                                rec.TemplateSectionId == tSection.TemplateSectionID))
                    {
                        mode = 0; // Vendor can edit Exception/Probation Section forever.    
                    }
                    //else
                    //{
                    //    mMode = mode;
                    //}
                }
                // Ends<<<
            }
            else if (Session["RoleName"].ToString().Equals("Client"))
            {
                mode = 1;//He can view sections

                var permissiontype = entityDB.TemplateSections.FirstOrDefault(rec => rec.TemplateSectionID == TemplateSectionID).TemplateSectionsPermission.FirstOrDefault().SectionsPermissionId;
                var userhasRestrictedSectionAccess = FVGen3.BusinessLogic.RolesHelper.IsUserInRole(userId, 10001, "Restricted Section Access", preQualificationDetails.ClientId, preQualificationDetails.VendorId);
                if (permissiontype == 4 && userhasRestrictedSectionAccess)
                {
                    mode = 0;
                }
                //Check against TemplateSectionID to find Viewtype if 4 and user has permission make mode = 0

            }



            //To Get list of PreQualification with respet to present prequalification client and vendor
            //Rajesh On 8-31-2013 >>>
            @ViewBag.mode = mode;
            @ViewBag.templateId = templateId;
            //Ends <<<
            //Rajesh on 2/21/2014

            //var VendorEmails = (from vEmails in preQualificationDetails.Vendor.SystemUsersOrganizations.ToList()
            //                    select vEmails.SystemUsers.UserName).ToList();
            var VendorEmails = preQualificationDetails.Vendor.SystemUsersOrganizations.Select(rec => rec.SystemUsers.UserName).ToList();
            var InvitationDetails = entityDB.VendorInviteDetails.FirstOrDefault(rec => VendorEmails.Contains(rec.VendorEmail) && rec.ClientId == preQualificationDetails.ClientId);
            if (InvitationDetails == null)
                ViewBag.InvitationSentBy = "Without Invitation";
            else if (InvitationDetails.InvitationSentFrom == 0)
                try
                {
                    // Kiran on 3/27/2014
                    ViewBag.InvitationSentBy = "Invitation from ";
                    ViewBag.InvitationSentByUser = InvitationDetails.SystemUsers.Contacts.FirstOrDefault().LastName + ", " + InvitationDetails.SystemUsers.Contacts.FirstOrDefault().FirstName;
                    // Ends <<<
                }
                catch { }
            else
                ViewBag.InvitationSentBy = "Invitation from InfusionSoft";
            //Ends<<<


            ViewBag.TemplateCode = selectedTemplate.TemplateCode;
            ViewBag.VendorId = preQualificationDetails.VendorId;
            ViewBag.ClientId = preQualificationDetails.ClientId;
            ViewBag.ClientName = preQualificationDetails.Client.Name;
            ViewBag.VendorName = preQualificationDetails.Vendor.Name;
            ViewBag.Period = preQualificationDetails.PrequalificationStart.ToString("MM/dd/yy") + "-" + preQualificationDetails.PrequalificationFinish.ToString("MM/dd/yy");
            ViewBag.LogoPath = "";
            ViewBag.isEdit = preQualificationDetails.PrequalificationStatusId == 10;
            ViewBag.LogoPath = ConfigurationSettings.AppSettings["LogoUploadPath"].ToString() + "/";//Rajesh on 08/09/2013
            ViewBag.StatusName = preQualificationDetails.PrequalificationStatus.PrequalificationStatusName; // Kiran on 3/28/2014
            ViewBag.PrequalStartDate = preQualificationDetails.PrequalificationCreate.ToString("MM/dd/yyyy"); // Kiran on 3/28/2014

            // Kiran on 3/28/2014
            ViewBag.StatusId = preQualificationDetails.PrequalificationStatusId;

            if (@ViewBag.StatusId.ToString() == "10")
            {
                var previousPrequalStatusRecords = entityDB.PrequalificationStatusChangeLog.Where(rec => rec.PrequalificationId == preQualificationDetails.PrequalificationId && rec.PrequalificationStatusId != preQualificationDetails.PrequalificationStatusId).OrderByDescending(record => record.PrequalStatusLogId).ToList();
                if (previousPrequalStatusRecords != null)
                {
                    var prequalificationStatusChangeRec = previousPrequalStatusRecords.FirstOrDefault();
                    var previousPrequalStatusId = prequalificationStatusChangeRec.PrequalificationStatusId;
                    var preQualificationStatusRec = entityDB.PrequalificationStatus.Find(previousPrequalStatusId);
                    ViewBag.VBPreviousStatusColour = entityDB.PrequalificationStatus.FirstOrDefault(rec => rec.PrequalificationStatusId == previousPrequalStatusId).BannerColor;
                    ViewBag.VBPreviousStatusName = entityDB.PrequalificationStatus.FirstOrDefault(rec => rec.PrequalificationStatusId == previousPrequalStatusId).PrequalificationStatusName;

                    // Commented on 02/11/2015 to display the current status along with previous status(Ex:- Prequalified-Expired) & current period instead of previous status period.
                    //ViewBag.VBPreviousPeriod = prequalificationStatusChangeRec.PrequalificationStart.ToString("MM/dd/yyyy") + " - " + prequalificationStatusChangeRec.PrequalificationFinish.ToString("MM/dd/yyyy");
                }
            }
            // Ends<<<

            // Kiran on 3/15/2014
            if (@Session["RoleName"].ToString() == "Admin" || (@Session["RoleName"].ToString() == "Client" && mode == 1) || (@Session["RoleName"].ToString() == "Vendor" && mode == 1)) // Kiran on 3/25/2014            
            {
                ViewBag.VBStatusColor = preQualificationDetails.PrequalificationStatus.BannerColor;
            }
            else if ((mode == null || mode == 0 && @Session["RoleName"].ToString() == "Client") || (mode == null || mode == 0 && @Session["RoleName"].ToString() == "Vendor"))
            {
                ViewBag.VBStatusColor = LocalConstants.PQ_DEFAULT_BG_COLOR;
            }
            // Ends<<<


            //Rajesh on 1/3/2014
            try
            {//Don't change order
                ViewBag.FinalizedUserEmail = preQualificationDetails.LoggedInUserForPreQualification.Email;
                ViewBag.FinalizedDateTime = preQualificationDetails.PrequalificationSubmit;
                ViewBag.FinalizedUserName = preQualificationDetails.LoggedInUserForPreQualification.UserName;

            }
            catch (Exception ee) { }
            //Ends<<<

            var PreQualificationsList = entityDB.Prequalification.Where(rec => rec.ClientId == preQualificationDetails.ClientId && rec.VendorId == preQualificationDetails.VendorId).OrderByDescending(rec => rec.PrequalificationStart).ToList();
            var dropDown_Prequalification = new List<SelectListItem>();
            SelectListItem latestPreQualification = new SelectListItem();
            latestPreQualification.Text = PreQualificationsList.FirstOrDefault().Client.Name + " " + PreQualificationsList.FirstOrDefault().PrequalificationStart.ToString("MM/dd/yyyy");
            latestPreQualification.Value = PreQualificationsList.FirstOrDefault().PrequalificationId + "";
            dropDown_Prequalification.Add(latestPreQualification);

            if (Session["RoleName"].ToString().Equals("Admin") || Session["RoleName"].ToString().Equals("Client")) // Kiran on 4/30/2014            
            {
                dropDown_Prequalification = (from preQualifications in entityDB.Prequalification.Where(rec => rec.ClientId == preQualificationDetails.ClientId && rec.VendorId == preQualificationDetails.VendorId).OrderByDescending(rec => rec.PrequalificationStart).ToList()
                                             select new SelectListItem
                                             {
                                                 Text = preQualificationDetails.Client.Name + " " + preQualifications.PrequalificationStart.ToString("MM/dd/yyyy"),
                                                 Value = preQualifications.PrequalificationId + "",
                                                 Selected = (PreQualificationId == preQualifications.PrequalificationId)
                                             }).ToList();
            }
            if (Session["RoleName"].ToString().Equals("Admin")) // Kiran on 4/30/2014            
            {
                if (PreQualificationsList != null && (PreQualificationsList.FirstOrDefault().PrequalificationFinish.Date.Ticks >= DateTime.Now.Date.Ticks && PreQualificationsList.FirstOrDefault().PrequalificationFinish.AddDays(-30).Date.Ticks <= DateTime.Now.Date.Ticks) && (PreQualificationsList.FirstOrDefault().PrequalificationStatusId == 4 || PreQualificationsList.FirstOrDefault().PrequalificationStatusId == 8 || PreQualificationsList.FirstOrDefault().PrequalificationStatusId == 9 || PreQualificationsList.FirstOrDefault().PrequalificationStatusId == 13 || PreQualificationsList.FirstOrDefault().PrequalificationStatusId == 23 || PreQualificationsList.FirstOrDefault().PrequalificationStatusId == 24 || PreQualificationsList.FirstOrDefault().PrequalificationStatusId == 25 || PreQualificationsList.FirstOrDefault().PrequalificationStatusId == 26))//Rajesh on 3/25/2014 // Kiran on 02/04/2015 for opening the renewal option for different statuses as per the email dated 02/03/2015.
                {
                    SelectListItem pre = new SelectListItem();
                    pre.Text = "Renewal";
                    pre.Value = "-1";
                    dropDown_Prequalification.Add(pre);
                }
            }
            ViewBag.VBPreQualifications = dropDown_Prequalification;

            // for clientreference id. sumanth on 25/08/2015

            var ClientRefId = "";
            try
            {
                ClientRefId = entityDB.ClientVendorReferenceIds.FirstOrDefault(rec => rec.ClientId == preQualificationDetails.ClientId && rec.VendorId == preQualificationDetails.VendorId).ClientVendorReferenceId;
            }
            catch
            {
                ClientRefId = "";
            }
            ViewBag.VBClientVendorRefID = ClientRefId;
            //Creating side Menus
            List<LocalTemplatesSideMenuModel> sideMenus = new List<LocalTemplatesSideMenuModel>();

            var sideMenuSections = selectedTemplate.TemplateSections.Where(rec => rec.Visible == true).OrderBy(x => x.DisplayOrder).ToList();
            if (Session["RoleName"].ToString().Equals("Client"))
            {
                long[] prequalificationStatus = { 3, 15 };//Rajesh on 1/21/2014
                int status = Array.IndexOf(prequalificationStatus, preQualificationDetails.PrequalificationStatusId);
                if (status != -1)
                    sideMenuSections = selectedTemplate.TemplateSections.Where(rec => rec.VisibleToClient == true && rec.Visible == true).OrderBy(x => x.DisplayOrder).ToList();
                // Kiran on 4/25/2014
                var allVisibleTemplateSectionsForClient = entityDB.TemplateSectionsPermission.Where(rec => rec.VisibleTo == 3 || rec.VisibleTo == 2).Select(rec => rec.TemplateSectionID).ToList();

                //Guid userId = new Guid(Session["UserId"].ToString());
                //var userhasRestrictedSectionAccess = FVGen3.BusinessLogic.RolesHelper.IsUserInRole(userId, "Restricted Section Access", preQualificationDetails.ClientId, preQualificationDetails.VendorId);

                //if (userhasRestrictedSectionAccess)
                //{
                allVisibleTemplateSectionsForClient.AddRange(entityDB.TemplateSectionsPermission.Where(rec => rec.VisibleTo == 4).Select(rec => rec.TemplateSectionID).ToList());
                //}

                //Rajesh on 12-08-2016 for restricted access 2>>
                var hasAccess2 = FVGen3.BusinessLogic.RolesHelper.IsUserInRole(userId, LocalConstants.RESTRICTED_ACCESS_2_VIEW_ID, "read", preQualificationDetails.ClientId, preQualificationDetails.VendorId) || Session["RoleName"].Equals("Super Admin");
                if (hasAccess2)
                    allVisibleTemplateSectionsForClient.AddRange(entityDB.TemplateSectionsPermission.Where(rec => rec.VisibleTo == LocalConstants.SectionPermissionType.RESTRICTED_ACCESS_2).Select(rec => rec.TemplateSectionID).ToList());

                //<<
                sideMenuSections = sideMenuSections.Where(rec => allVisibleTemplateSectionsForClient.Contains(rec.TemplateSectionID)).ToList();
                // Ends<<<
                try
                {
                    if (sideMenuSections.FirstOrDefault(rec => rec.TemplateSectionID == TemplateSectionID) == null)
                        TemplateSectionID = sideMenuSections.FirstOrDefault().TemplateSectionID;
                }
                catch
                {
                    TemplateSectionID = -1;
                }
            }

            //Rajesh on 2/13/2014

            if (Session["RoleName"].ToString().Equals("Vendor"))//If Vendor Login
            {

                sideMenuSections = sideMenuSections.Where(rec => allVisibleTemplateSections.Contains(rec.TemplateSectionID)).ToList();
            }
            //Ends<<<

            var initial = true;
            // Kiran on 6/24/2014
            if (@Session["RoleName"].ToString() != "Vendor")
            {
                LocalTemplatesSideMenuModel vendorDetailsMenu = new LocalTemplatesSideMenuModel("Vendor Details", "VendorDetails", "AdminVendors", false, "SelectedVendorId=" + preQualificationDetails.VendorId + "&PreQualificationId=" + PreQualificationId + "&TemplateSectionID=-1" + "&mode=" + mode);
                vendorDetailsMenu.isVisible = true;
                vendorDetailsMenu.isSectionCompleted = null;
                sideMenus.Add(vendorDetailsMenu);
                // Ends<<<
            }
            foreach (TemplateSections section in sideMenuSections)
            {
                if (initial && @Session["RoleName"].ToString() == "Admin" && mode != 1)//Rajesh On 8-31-2013 
                {
                    LocalTemplatesSideMenuModel menu1 = new LocalTemplatesSideMenuModel("Change Status", "ChangeStatus", "Prequalification", false, "templateId=" + section.TemplateID + "&PreQualificationId=" + PreQualificationId + "&mode=" + mode);
                    menu1.isVisible = true;
                    menu1.isSectionCompleted = null;
                    sideMenus.Add(menu1);
                    initial = false;
                }

                LocalTemplatesSideMenuModel menu = new LocalTemplatesSideMenuModel(section.SectionName, "TemplateSectionsList", "Prequalification", TemplateSectionID == section.TemplateSectionID, "templateId=" + section.TemplateID + "&TemplateSectionID=" + section.TemplateSectionID + "&PreQualificationId=" + PreQualificationId + "&mode=" + mode);
                //menu.actionName = "TemplateSectionsList";
                //menu.controllerName = "PrequalificationController";
                //menu.isChecked = TemplateSectionID==section.TemplateSectionID;
                //menu.menuName = section.SectionName;
                //menu.param="templateId="+section.TemplateID+"&TemplateSectionID="+section.TemplateSectionID;
                //Rajesh on 10-01-2013
                if (@Session["RoleName"].ToString() == "Vendor" && (preQualificationDetails.PaymentReceived == null || preQualificationDetails.PaymentReceived == false))
                {
                    int[] userPermissionSections = { 0, 3, 31, 32 };//isprofile and payment
                    if (Array.IndexOf(userPermissionSections, section.TemplateSectionType) == -1)
                        menu.isVendorNoPaymentNoAccess = false;
                }
                menu.isSectionCompleted = (entityDB.PrequalificationCompletedSections.FirstOrDefault(rec => rec.PrequalificationId == PreQualificationId && rec.TemplateSectionId == section.TemplateSectionID) != null);//Rajesh on 8/26/2013
                //Rajesh on 1/11/2014
                menu.isVisible = true;
                if (section.TemplateSectionType == 34)
                {
                    if (preQualificationDetails.RequestForMoreInfo == null || preQualificationDetails.RequestForMoreInfo == false)
                        menu.isVisible = false;
                }
                //Ends<<<
                //Rajesh on 1/21/2014
                if (section.TemplateSectionType == 35 && Session["RoleName"].ToString().Equals("Vendor"))//If Vendor Login Don't Client Data menu
                {
                    //Do nothing
                }
                else
                    sideMenus.Add(menu);
                //Ends<<<


            }
            if (@Session["RoleName"].ToString() == "Admin" || @Session["RoleName"].ToString() == "Client")
            {
                var menu2 = new LocalTemplatesSideMenuModel("Employees", "Employees", "Prequalification", false, "templateId=" + templateId + "&PreQualificationId=" + PreQualificationId);
                menu2.isVisible = true;
                menu2.isSectionCompleted = null;
                initial = false;
                //sideMenus.Add(menu2);
                if (@Session["RoleName"].ToString() == "Admin" && mode != 1)
                {
                    sideMenus.Insert(2, menu2);
                }
                else
                {
                    sideMenus.Insert(1, menu2);
                }

            }
            //<a href="TemplateSectionsList?templateId=@v.TemplateID&TemplateSectionID=@v.TemplateSectionID"> @v.SectionName</a><br />
            ViewBag.sideMenu = sideMenus;
            ViewBag.TemplateSectionID = TemplateSectionID;
            ViewBag.VBDocumentsList = entityDB.Document;
            ViewBag.PaymentRecieved = preQualificationDetails.PaymentReceived;  // Suresh on 19th Dec 2013

            getCommand();
            var templatePriceNotInUse = paymentSection(preQualificationDetails.VendorId, preQualificationDetails.ClientTemplates.ClientTemplateID, preQualificationDetails.PrequalificationId);// Mani on 8/18/2015
            var paidTemplatePrice = entityDB.PrequalificationPayments.FirstOrDefault(rec => rec.PrequalificationId == PreQualificationId && rec.PaymentCategory == 0);
            if (paidTemplatePrice == null)
                ViewBag.preqprice = entityDB.ClientTemplatesBUGroupPrice.FirstOrDefault(rec => rec.ClientTemplateID == preQualificationDetails.ClientTemplateId && rec.GroupingFor == 0 && rec.GroupPriceType == 0).GroupPrice;
            else if (paidTemplatePrice != null && paidTemplatePrice.PaymentReceivedAmount == null)
                ViewBag.preqprice = entityDB.ClientTemplatesBUGroupPrice.FirstOrDefault(rec => rec.ClientTemplateID == preQualificationDetails.ClientTemplateId && rec.GroupingFor == 0 && rec.GroupPriceType == 0).GroupPrice;
            else
                ViewBag.preqprice = paidTemplatePrice.PaymentReceivedAmount;
            ViewBag.TemplatePrice = getTemplatePrice(preQualificationDetails.VendorId, preQualificationDetails.ClientTemplates.ClientTemplateID, preQualificationDetails.PrequalificationId);

            ViewBag.PreQualificationId = PreQualificationId;

            //Rajesh on 12/30/2013
            // Kiran on 02/12/2015 to stop displaying the employees & vendor users that are disabled under the guidance of rajesh
            var empTrainingRoleId = entityDB.SystemRoles.FirstOrDefault(rec => rec.RoleName.Trim() == "Employee Training").RoleId;

            ViewBag.Vendors = (from vendors in preQualificationDetails.Vendor.SystemUsersOrganizations.Where(rec => rec.SystemUsers.UserStatus == true && rec.SystemUsersOrganizationsRoles.Where(rec1 => rec1.SysRoleId == empTrainingRoleId).Count() == 0)
                               select new SelectListItem
                               {
                                   Text = vendors.SystemUsers.Contacts != null && vendors.SystemUsers.Contacts.Count != 0 ? vendors.SystemUsers.Contacts.FirstOrDefault().FirstName.Trim() + " " + vendors.SystemUsers.Contacts.FirstOrDefault().LastName.Trim() : vendors.SystemUsers.Email + " - No Contact details available",
                                   Value = vendors.SystemUsers.UserId + "",
                                   Selected = (preQualificationDetails.UserIdAsCompleter == vendors.SystemUsers.UserId)

                               }).ToList();
            //Ends<<<

            //Rajesh on 4/7/2014 For To know the section permission 
            var showSaveAndContinue = true;
            if (Session["RoleName"].ToString().Equals("Client"))
            {
                var section = entityDB.TemplateSections.Find(TemplateSectionID);
                showSaveAndContinue = false;
                if (section != null && section.TemplateSectionsPermission != null)
                    if ((section.TemplateSectionsPermission.FirstOrDefault().VisibleTo == 2)//|| section.TemplateSectionsPermission.FirstOrDefault().VisibleTo == 4)//If it is client data entry//Rajesh on 3/21/2014
                        || (section.TemplateSectionsPermission.FirstOrDefault().VisibleTo == 4 && ViewBag.RestrictedSectionEdit == true))
                    {
                        showSaveAndContinue = true;
                    }
                    else
                        foreach (var subSec in section.TemplateSubSections.ToList())
                        {
                            //If It is limited access permission then check this user has edit permission
                            var SubSecPermission = subSec.TemplateSubSectionsPermissions.FirstOrDefault(rec => rec.PermissionFor == "Client");
                            if (SubSecPermission != null && SubSecPermission.PermissionType == 1)
                            {
                                var loginUserId = new Guid(Session["UserId"].ToString());
                                try
                                {
                                    if (subSec.TemplateSubSectionsPermissions.FirstOrDefault(rec => rec.PermissionFor == "Client").TemplateSubSectionsUserPermissions.FirstOrDefault(rec => rec.UserId == loginUserId).EditAccess)
                                    {
                                        showSaveAndContinue = true;

                                        //break;//If atleast 1 sub section is editable we can send notification
                                    }
                                }
                                catch { }
                            }
                        }
            }
            ViewBag.showSaveAndContinue = showSaveAndContinue;
            ViewBag.VBPrequalification = preQualificationDetails; // Kiran on 11/9/2015

            return View(selectedTemplate);
        }



        //Rajesh on DT:8-29-2013
        [SessionExpire]
        [HeaderAndSidebar(headerName = "PREQUALIFICATIONREQUIREMENTS", sideMenuName = "", ViewName = "TemplateSectionsList")]//Rajesh on 11/3/2014 Done DB Changes Also
        public ActionResult ChangeStatus(Guid templateId, long PreQualificationId)
        {
            Prequalification preQualificationDetails = entityDB.Prequalification.Find(PreQualificationId);
            Templates selectedTemplate = entityDB.Templates.Find(templateId);
            SetupQuestionnaire(templateId, PreQualificationId, "ChangeStatus");

            //For Change Status
            ViewBag.CurrentStatus = preQualificationDetails.PrequalificationStatus.PrequalificationStatusName;
            ViewBag.currentStatusPeriod = preQualificationDetails.PrequalificationStart.ToString("MM/dd/yyyy");


            LocalChangeStatus status = new LocalChangeStatus();
            status.templateId = templateId;
            status.PrequalificationId = PreQualificationId;
            status.NotCompletedSection = selectedTemplate.TemplateSections.Where(rec => rec.Visible == true).OrderBy(x => x.DisplayOrder).ToList();
            //SelectListItem item=new SelectListItem();
            //item.Value="-1";
            //item.Text="";
            //TemplatesList_dropDown.Add(item);

            status.NotCompletedSection_Status = new List<bool>();
            for (int i = 0; i < status.NotCompletedSection.Count; i++)
            {
                status.NotCompletedSection_Status.Add(false);
            }

            //Rajesh on 11/05/2013>>>
            //status.NewPayment = preQualificationDetails.OverirdeAmount == null ? 0 : (decimal)preQualificationDetails.OverirdeAmount;
            //status.isOverridePayment = preQualificationDetails.OverridePayment == null ? false : (bool)preQualificationDetails.OverridePayment;
            status.NewPayment = 100;
            status.RequestForMoreInfo = preQualificationDetails.RequestForMoreInfo == null ? false : (bool)preQualificationDetails.RequestForMoreInfo;
            //Ends<<


            var PreQualificationStatuses = new List<SelectListItem>();
            SelectListItem item = new SelectListItem();
            item.Value = "-1";
            item.Text = "";
            PreQualificationStatuses.Add(item);

            //Rajesh on 9/25/2014
            PreQualificationStatuses.AddRange((from a in entityDB.PrequalificationStatus.Where(rec => rec.PrequalificationStatusId > 2).ToList().Where(rec => rec.ShowInDashBoard == true).OrderBy(rec => rec.PrequalificationStatusName) // Kiran on 01/13/2015
                                               select new SelectListItem()
                                               {
                                                   Text = a.PrequalificationStatusName,
                                                   Value = a.PrequalificationStatusId + ""
                                               }).ToList());
            ViewBag.PreQualificationStatuses = PreQualificationStatuses;

            // Kiran on 12/03/2013
            var adminOrgId = entityDB.Organizations.FirstOrDefault(record => record.OrganizationType == "Admin").OrganizationID;

            // Kiran on 12/8/2013
            var UseridAsSigner = entityDB.Prequalification.FirstOrDefault(record => record.PrequalificationId == PreQualificationId).UserIdAsSigner;

            var UserIds = entityDB.SystemUsersOrganizations.Where(rec => rec.OrganizationId == adminOrgId).Select(rec => rec.UserId).ToList();

            var adminUsers = (from systemUsers in entityDB.SystemUsers.Where(rec => UserIds.Contains(rec.UserId)).ToList()
                              select new SelectListItem
                              {
                                  Text = systemUsers.Contacts.FirstOrDefault() != null ? systemUsers.Contacts[0].LastName + ", " + systemUsers.Contacts[0].FirstName : systemUsers.Email,
                                  Value = systemUsers.UserId.ToString(),
                                  Selected = UseridAsSigner == systemUsers.UserId
                              }).ToList();

            // Kiran on 03/18/2015 Ends<<<

            // Ends<<<

            SelectListItem defaultItem = new SelectListItem();
            defaultItem = new SelectListItem();
            defaultItem.Text = "";
            defaultItem.Value = "";
            var adminUsersWithDefaultValue = adminUsers.ToList();
            adminUsersWithDefaultValue.Insert(0, defaultItem);

            ViewBag.VBAdminUsers = adminUsersWithDefaultValue;
            // Ends<<<
            return View(status);
        }
        //Rajesh on 11/3/2014
        private void SetupQuestionnaire(Guid templateId, long PreQualificationId, string ScreenName)
        {
            //Rajesh On 8-31-2013 >>>
            @ViewBag.mode = 0;
            @ViewBag.templateId = templateId;
            //Ends <<<

            Prequalification preQualificationDetails = entityDB.Prequalification.Find(PreQualificationId);
            ViewBag.TemplateCode = preQualificationDetails.ClientTemplates.Templates.TemplateCode;
            ViewBag.VendorId = preQualificationDetails.VendorId;
            ViewBag.ClientId = preQualificationDetails.ClientId;
            ViewBag.ClientName = preQualificationDetails.Client.Name;
            ViewBag.VendorName = preQualificationDetails.Vendor.Name;
            ViewBag.Period = preQualificationDetails.PrequalificationStart.ToString("MM/dd/yy") + "-" + preQualificationDetails.PrequalificationFinish.ToString("MM/dd/yy");
            ViewBag.LogoPath = "";
            ViewBag.isEdit = preQualificationDetails.PrequalificationStatusId == 10;
            ViewBag.LogoPath = ConfigurationSettings.AppSettings["LogoUploadPath"].ToString() + "/";//Rajesh on 08/09/2013
            ViewBag.StatusName = preQualificationDetails.PrequalificationStatus.PrequalificationStatusName; // Kiran on 3/28/2014
            ViewBag.PrequalStartDate = preQualificationDetails.PrequalificationCreate.ToString("MM/dd/yyyy"); // Kiran on 3/28/2014

            //Rajesh on 9/25/2014
            ViewBag.PrequalificationStartDate = preQualificationDetails.PrequalificationStart.ToString("MM/dd/yyyy");
            ViewBag.currentDate = DateTime.Now.ToString("MM/dd/yyyy");
            //Ends<<

            // Kiran on 3/28/2014
            ViewBag.StatusId = preQualificationDetails.PrequalificationStatusId;

            if (@ViewBag.StatusId.ToString() == "10")
            {
                var previousPrequalStatusRecords = entityDB.PrequalificationStatusChangeLog.Where(rec => rec.PrequalificationId == preQualificationDetails.PrequalificationId && rec.PrequalificationStatusId != preQualificationDetails.PrequalificationStatusId).OrderByDescending(record => record.PrequalStatusLogId).ToList();
                if (previousPrequalStatusRecords != null)
                {
                    var prequalificationStatusChangeRec = previousPrequalStatusRecords.FirstOrDefault();
                    var previousPrequalStatusId = prequalificationStatusChangeRec.PrequalificationStatusId;
                    var preQualificationStatusRec = entityDB.PrequalificationStatus.Find(previousPrequalStatusId);
                    ViewBag.VBPreviousStatusColour = entityDB.PrequalificationStatus.FirstOrDefault(rec => rec.PrequalificationStatusId == previousPrequalStatusId).BannerColor;
                    ViewBag.VBPreviousStatusName = entityDB.PrequalificationStatus.FirstOrDefault(rec => rec.PrequalificationStatusId == previousPrequalStatusId).PrequalificationStatusName;

                    //ViewBag.VBPreviousPeriod = startDate + " - " + prequalificationStatusChangeRec.StatusChangeDate.Date;
                    // Commented on 02/11/2015 to display the current status along with previous status(Ex:- Prequalified-Expired) & current period instead of previous status period.
                    //ViewBag.VBPreviousPeriod = prequalificationStatusChangeRec.PrequalificationStart.ToString("MM/dd/yyyy") + " - " + prequalificationStatusChangeRec.PrequalificationFinish.ToString("MM/dd/yyyy");
                }
            }
            // Ends<<<

            // Kiran on 3/15/2014
            if (@Session["RoleName"].ToString() == "Admin" || (@Session["RoleName"].ToString() == "Client" && ViewBag.mode == 1) || (@Session["RoleName"].ToString() == "Vendor" && ViewBag.mode == 1)) // Kiran on 3/25/2014            
            {
                ViewBag.VBStatusColor = preQualificationDetails.PrequalificationStatus.BannerColor;
            }
            else if ((ViewBag.mode == null || ViewBag.mode == 0 && @Session["RoleName"].ToString() == "Client") || (ViewBag.mode == null || ViewBag.mode == 0 && @Session["RoleName"].ToString() == "Vendor"))
            {
                ViewBag.VBStatusColor = LocalConstants.PQ_DEFAULT_BG_COLOR;
            }
            // Ends<<<
            //Rajesh on 2/21/2014

            //var VendorEmails = (from vEmails in preQualificationDetails.Vendor.SystemUsersOrganizations.ToList()
            //                    select vEmails.SystemUsers.UserName).ToList();
            var VendorEmails = preQualificationDetails.Vendor.SystemUsersOrganizations.Select(rec => rec.SystemUsers.UserName).ToList();

            var InvitationDetails = entityDB.VendorInviteDetails.FirstOrDefault(rec => VendorEmails.Contains(rec.VendorEmail) && rec.ClientId == preQualificationDetails.ClientId);
            if (InvitationDetails == null)
                ViewBag.InvitationSentBy = "Without Invitation";
            else if (InvitationDetails.InvitationSentFrom == 0)
                try
                {
                    // Kiran on 3/27/2014
                    ViewBag.InvitationSentBy = "Invitation from ";
                    ViewBag.InvitationSentByUser = InvitationDetails.SystemUsers.Contacts.FirstOrDefault().LastName + ", " + InvitationDetails.SystemUsers.Contacts.FirstOrDefault().FirstName;
                    // Ends <<<
                }
                catch { }
            else
                ViewBag.InvitationSentBy = "Invitation from InfusionSoft";
            //Ends<<<


            var PreQualificationsList = entityDB.Prequalification.Where(rec => rec.ClientId == preQualificationDetails.ClientId && rec.VendorId == preQualificationDetails.VendorId).OrderByDescending(rec => rec.PrequalificationStart).ToList();
            var dropDown_Prequalification = new List<SelectListItem>();
            SelectListItem latestPreQualification = new SelectListItem();
            latestPreQualification.Text = PreQualificationsList.FirstOrDefault().Client.Name + " " + PreQualificationsList.FirstOrDefault().PrequalificationStart.ToString("MM/dd/yyyy");
            latestPreQualification.Value = PreQualificationsList.FirstOrDefault().PrequalificationId + "";
            dropDown_Prequalification.Add(latestPreQualification);


            if (Session["RoleName"].ToString().Equals("Admin"))
            {
                dropDown_Prequalification = (from preQualifications in entityDB.Prequalification.Where(rec => rec.ClientId == preQualificationDetails.ClientId && rec.VendorId == preQualificationDetails.VendorId).OrderByDescending(rec => rec.PrequalificationStart).ToList()
                                             select new SelectListItem
                                             {
                                                 Text = preQualificationDetails.Client.Name + " " + preQualifications.PrequalificationStart.ToString("MM/dd/yyyy"),
                                                 Value = preQualifications.PrequalificationId + "",
                                                 Selected = (PreQualificationId == preQualifications.PrequalificationId)
                                             }).ToList();
            }
            if (PreQualificationsList != null && (PreQualificationsList.FirstOrDefault().PrequalificationFinish.Date.Ticks >= DateTime.Now.Date.Ticks && PreQualificationsList.FirstOrDefault().PrequalificationFinish.AddDays(-30).Date.Ticks <= DateTime.Now.Date.Ticks) && (PreQualificationsList.FirstOrDefault().PrequalificationStatusId == 4 || PreQualificationsList.FirstOrDefault().PrequalificationStatusId == 8 || PreQualificationsList.FirstOrDefault().PrequalificationStatusId == 9 || PreQualificationsList.FirstOrDefault().PrequalificationStatusId == 13 || PreQualificationsList.FirstOrDefault().PrequalificationStatusId == 23 || PreQualificationsList.FirstOrDefault().PrequalificationStatusId == 24 || PreQualificationsList.FirstOrDefault().PrequalificationStatusId == 25 || PreQualificationsList.FirstOrDefault().PrequalificationStatusId == 26))//Rajesh on 3/25/2014 // Kiran on 02/04/2015 for opening the renewal option for different statuses as per the email dated 02/03/2015.
            {
                SelectListItem pre = new SelectListItem();
                pre.Text = "Renewal";
                pre.Value = "-1";
                dropDown_Prequalification.Add(pre);
            }
            ViewBag.VBPreQualifications = dropDown_Prequalification;
            var selectedTemplate = entityDB.Templates.Find(templateId);

            //Creating side Menus
            List<LocalTemplatesSideMenuModel> sideMenus = new List<LocalTemplatesSideMenuModel>();


            var initial = true;
            // Kiran on 6/24/2014
            if (@Session["RoleName"].ToString() != "Vendor")
            {
                LocalTemplatesSideMenuModel vendorDetailsMenu = new LocalTemplatesSideMenuModel("Vendor Details", "VendorDetails", "AdminVendors", false, "SelectedVendorId=" + preQualificationDetails.VendorId + "&PreQualificationId=" + PreQualificationId + "&TemplateSectionID=-1" + "&mode=" + 0);
                vendorDetailsMenu.isVisible = true;
                vendorDetailsMenu.isSectionCompleted = null;
                sideMenus.Add(vendorDetailsMenu);
                // Ends<<<
            }
            foreach (TemplateSections section in selectedTemplate.TemplateSections.Where(rec => rec.Visible == true).OrderBy(x => x.DisplayOrder).ToList())
            {
                if (initial && @Session["RoleName"].ToString() == "Admin")
                {
                    LocalTemplatesSideMenuModel menu1 = new LocalTemplatesSideMenuModel("Change Status", "ChangeStatus", "Prequalification", "ChangeStatus".Equals(ScreenName), "templateId=" + section.TemplateID + "&PreQualificationId=" + PreQualificationId);
                    menu1.isVisible = true;
                    menu1.isSectionCompleted = null;
                    initial = false;
                    sideMenus.Add(menu1);
                }
                LocalTemplatesSideMenuModel menu = new LocalTemplatesSideMenuModel(section.SectionName, "TemplateSectionsList", "Prequalification", false, "templateId=" + section.TemplateID + "&TemplateSectionID=" + section.TemplateSectionID + "&PreQualificationId=" + PreQualificationId);

                menu.isSectionCompleted = (entityDB.PrequalificationCompletedSections.FirstOrDefault(rec => rec.PrequalificationId == PreQualificationId && rec.TemplateSectionId == section.TemplateSectionID) != null);//Rajesh on 8/26/2013
                //Rajesh on 1/11/2014
                menu.isVisible = true;
                if (section.TemplateSectionType == 34)
                {
                    if (preQualificationDetails.RequestForMoreInfo == null || preQualificationDetails.RequestForMoreInfo == false)
                        menu.isVisible = false;
                }
                //Ends<<<
                //Rajesh on 1/21/2014
                if (section.TemplateSectionType == 35 && Session["RoleName"].ToString().Equals("Vendor"))//If Vendor Login Don't Client Data menu
                {
                    //Do nothing
                }
                else
                    sideMenus.Add(menu);
                //Ends<<<
            }
            if (@Session["RoleName"].ToString() == "Admin" || @Session["RoleName"].ToString() == "Client")
            {
                var menu2 = new LocalTemplatesSideMenuModel("Employees", "Employees", "Prequalification", "Employees".Equals(ScreenName), "templateId=" + templateId + "&PreQualificationId=" + PreQualificationId);
                menu2.isVisible = true;
                menu2.isSectionCompleted = null;
                initial = false;
                //sideMenus.Add(menu2);
                if (@Session["RoleName"].ToString() == "Admin")
                {
                    sideMenus.Insert(2, menu2);
                }
                else
                {
                    sideMenus.Insert(1, menu2);
                }
            }
            //<a href="TemplateSectionsList?templateId=@v.TemplateID&TemplateSectionID=@v.TemplateSectionID"> @v.SectionName</a><br />
            ViewBag.sideMenu = sideMenus;

            ViewBag.VBDocumentsList = entityDB.Document;

            getCommand();
            ViewBag.TemplatePrice = getTemplatePrice(preQualificationDetails.VendorId, preQualificationDetails.ClientTemplates.ClientTemplateID, preQualificationDetails.PrequalificationId);
            ViewBag.PreQualificationId = PreQualificationId;
        }

        [SessionExpire]
        [HeaderAndSidebar(headerName = "PREQUALIFICATIONREQUIREMENTS", sideMenuName = "", ViewName = "Employees")]
        public ActionResult Employees(Guid templateId, long PreQualificationId)
        {
            SetupQuestionnaire(templateId, PreQualificationId, "Employees");

            var Prequalification = entityDB.Prequalification.Find(PreQualificationId);
            //To get Employess
            var employee_RoleId = entityDB.SystemRoles.FirstOrDefault(record => record.RoleName == "Employee Training").RoleId;

            //var employee_sysUserOrgRoles = entityDB.SystemUsersOrganizationsRoles.Where(record => record.SysRoleId == employee_RoleId).ToList();

            //var employee_sysUserOrgs = new List<SystemUsersOrganizations>();

            //foreach (var employee_sysUserOrgRole in employee_sysUserOrgRoles)
            //{
            //    var employee_sysUserOrg = employee_sysUserOrgRole.SystemUsersOrganizations;
            //    // Kiran on 2/10/2014
            //    var employee_sysUserOrgQuizzes = entityDB.SystemUsersOrganizationsQuizzes.Where(record => record.SysUserOrgId == employee_sysUserOrg.SysUserOrganizationId).ToList();
            //    foreach (var employee_quiz in employee_sysUserOrgQuizzes)
            //    {
            //        if (employee_sysUserOrg.OrganizationId == Prequalification.VendorId && employee_quiz.ClientTemplates.ClientID == Prequalification.ClientId)
            //        {
            //            employee_sysUserOrgs.Add(employee_sysUserOrg);
            //        }
            //    }
            //    // Ends<<<
            //}
            //ViewBag.VBEmployeeSysUserOrgs = employee_sysUserOrgs.Distinct().ToList(); // kiran on 9/11/2014

            var employee_sysUserOrgs = entityDB.SystemUsersOrganizationsRoles.Where(record => record.SysRoleId == employee_RoleId).Select(rec => rec.SystemUsersOrganizations).Where(rec => rec.OrganizationId == Prequalification.VendorId).ToList();


            ViewBag.VendorId = Prequalification.VendorId;//Rajesh on 12/2/2014
            ViewBag.VBEmployeeSysUserOrgs = employee_sysUserOrgs.OrderBy(rec => rec.SystemUsers.Contacts[0].LastName).ToList(); // kiran on 9/11/2014 // Kiran on 02/03/2015 for sorting the employees by lastname.

            return View();
        }

        public ActionResult EmployeeDetails(Guid userId)
        {
            LocalVendorEmployeeQuizzes employee = new LocalVendorEmployeeQuizzes();
            var employeeContactDetails = entityDB.Contact.FirstOrDefault(record => record.UserId == userId);
            employee.EmployeeName = employeeContactDetails.LastName + ", " + employeeContactDetails.FirstName; // Kiran on 02/30/2015
            employee.UserId = userId;
            // Kiran on 1/31/2014
            var currentEmpSystemUsersRecord = entityDB.SystemUsers.Find(userId);
            ViewBag.VBEmpPassword = Utilities.EncryptionDecryption.Decryptdata(currentEmpSystemUsersRecord.Password);
            ViewBag.VBEmpUserName = currentEmpSystemUsersRecord.UserName;
            //Ends<<<
            return View(employee);
        }

        /* Kiran on 11/5/2014
           To assign the On Site Training Status to employee(Fo client user only) */
        //[HttpPost]
        //public string AssignOnSiteTrainingToEmployee(string SystemUserOrgQuizIds)
        //{
        //    if (SystemUserOrgQuizIds != "")
        //    {
        //        foreach (var employee in SystemUserOrgQuizIds.Split(','))
        //        {
        //            if (employee != "")
        //            {
        //                try
        //                {
        //                    string[] empSysUserOrgQuiz = employee.Split('|');
        //                    var empSysUserOrgQuizId = Convert.ToInt64(empSysUserOrgQuiz[0].ToString().Replace("id_", ""));
        //                    var empQuizOnSiteTrainingStatus = empSysUserOrgQuiz[1].ToString().ToLower();
        //                    var empQuizOnSitetrainingCheckListrecord = entityDB.SystemUsersOrganizationsEmpQuizChecklist.FirstOrDefault(rec => rec.CheckListStatusID == 2 && rec.SysUserOrgQuizId == empSysUserOrgQuizId);
        //                    if (empQuizOnSiteTrainingStatus == "true")
        //                    {
        //                        if (empQuizOnSitetrainingCheckListrecord == null)
        //                        {
        //                            SystemUsersOrganizationsEmpQuizChecklist assignOnSiteTraining = new SystemUsersOrganizationsEmpQuizChecklist();
        //                            assignOnSiteTraining.SysUserOrgQuizId = empSysUserOrgQuizId;
        //                            assignOnSiteTraining.CheckListStatusID = 2;
        //                            entityDB.SystemUsersOrganizationsEmpQuizChecklist.Add(assignOnSiteTraining);
        //                        }
        //                    }
        //                    else
        //                    {
        //                        if (empQuizOnSitetrainingCheckListrecord != null)
        //                        {
        //                            entityDB.SystemUsersOrganizationsEmpQuizChecklist.Remove(empQuizOnSitetrainingCheckListrecord);
        //                        }
        //                    }
        //                }
        //                catch { }
        //            }
        //        }
        //        entityDB.SaveChanges();
        //        return "SaveSuccess";
        //    }
        //    return "";
        //}
        // Ends<<<


        //Rajesh on DT:8-29-2013
        //[SessionExpire]
        //[HeaderAndSidebar(headerName = "PREQUALIFICATIONREQUIREMENTS", sideMenuName = "", ViewName = "TemplateSectionsList")]
        //public ActionResult ChangeStatus(Guid templateId, long PreQualificationId)
        //{
        //    //Rajesh On 8-31-2013 >>>
        //    @ViewBag.mode = 0;
        //    @ViewBag.templateId = templateId;
        //    //Ends <<<

        //    Prequalification preQualificationDetails = entityDB.Prequalification.Find(PreQualificationId);
        //    ViewBag.TemplateCode = preQualificationDetails.ClientTemplates.Templates.TemplateCode;
        //    ViewBag.VendorId = preQualificationDetails.VendorId;
        //    ViewBag.ClientId = preQualificationDetails.ClientId;
        //    ViewBag.ClientName = preQualificationDetails.Client.Name;
        //    ViewBag.VendorName = preQualificationDetails.Vendor.Name;
        //    ViewBag.Period = preQualificationDetails.PrequalificationStart.ToString("MM/dd/yyyy") + "-" + preQualificationDetails.PrequalificationFinish.ToString("MM/dd/yyyy");
        //    ViewBag.LogoPath = "";
        //    ViewBag.isEdit = preQualificationDetails.PrequalificationStatusId == 10;
        //    ViewBag.LogoPath = ConfigurationSettings.AppSettings["LogoUploadPath"].ToString() + "/";//Rajesh on 08/09/2013
        //    ViewBag.StatusName = preQualificationDetails.PrequalificationStatus.PrequalificationStatusName; // Kiran on 3/28/2014
        //    ViewBag.PrequalStartDate = preQualificationDetails.PrequalificationCreate.ToString("MM/dd/yyyy"); // Kiran on 3/28/2014

        //    //Rajesh on 9/25/2014
        //    ViewBag.PrequalificationStartDate = preQualificationDetails.PrequalificationStart.ToString("MM/dd/yyyy");
        //    ViewBag.currentDate = DateTime.Now.ToString("MM/dd/yyyy");
        //    //Ends<<

        //    // Kiran on 3/28/2014
        //    ViewBag.StatusId = preQualificationDetails.PrequalificationStatusId; 
        //    if (@ViewBag.StatusId.ToString() == "10")
        //    {
        //        var previousPrequalStatusRecords = entityDB.PrequalificationStatusChangeLog.Where(rec => rec.PrequalificationId == preQualificationDetails.PrequalificationId && rec.PrequalificationStatusId != preQualificationDetails.PrequalificationStatusId).OrderByDescending(record => record.PrequalStatusLogId).ToList();
        //        if (previousPrequalStatusRecords != null)
        //        {
        //            var prequalificationStatusChangeRec = previousPrequalStatusRecords.FirstOrDefault();
        //            var previousPrequalStatusId = prequalificationStatusChangeRec.PrequalificationStatusId;
        //            var preQualificationStatusRec = entityDB.PrequalificationStatus.Find(previousPrequalStatusId);
        //            ViewBag.VBPreviousStatusColour = entityDB.PrequalificationStatus.FirstOrDefault(rec => rec.PrequalificationStatusId == previousPrequalStatusId).BannerColor;
        //            ViewBag.VBPreviousStatusName = entityDB.PrequalificationStatus.FirstOrDefault(rec => rec.PrequalificationStatusId == previousPrequalStatusId).PrequalificationStatusName;

        //            //var startDate = prequalificationStatusChangeRec.StatusChangeDate.Date;
        //            //if (preQualificationStatusRec.PeriodLengthUnit.Equals("D"))
        //            //{
        //            //    startDate = startDate.AddDays(-preQualificationStatusRec.PeriodLength);
        //            //}
        //            //else if (preQualificationStatusRec.PeriodLengthUnit.Equals("M"))
        //            //{
        //            //    startDate = startDate.AddMonths(-preQualificationStatusRec.PeriodLength);
        //            //}
        //            //else if (preQualificationStatusRec.PeriodLengthUnit.Equals("Y"))
        //            //{
        //            //    startDate = startDate.AddYears(-preQualificationStatusRec.PeriodLength);
        //            //}
        //            //ViewBag.VBPreviousPeriod = startDate + " - " + prequalificationStatusChangeRec.StatusChangeDate.Date;
        //            ViewBag.VBPreviousPeriod = prequalificationStatusChangeRec.PrequalificationStart.ToString("MM/dd/yyyy") + " - " + prequalificationStatusChangeRec.PrequalificationFinish.ToString("MM/dd/yyyy");
        //        }
        //    }
        //    // Ends<<<

        //    // Kiran on 3/15/2014
        //    if (@Session["RoleName"].ToString() == "Admin" || (@Session["RoleName"].ToString() == "Client" && ViewBag.mode == 1) || (@Session["RoleName"].ToString() == "Vendor" && ViewBag.mode == 1)) // Kiran on 3/25/2014            
        //    {
        //        ViewBag.VBStatusColor = preQualificationDetails.PrequalificationStatus.BannerColor;
        //    }
        //    else if ((ViewBag.mode == null || ViewBag.mode == 0 && @Session["RoleName"].ToString() == "Client") || (ViewBag.mode == null || ViewBag.mode == 0 && @Session["RoleName"].ToString() == "Vendor"))
        //    {
        //        ViewBag.VBStatusColor = "#e5e6e8";
        //    }
        //    // Ends<<<
        //    //Rajesh on 2/21/2014

        //    var VendorEmails = (from vEmails in preQualificationDetails.Vendor.SystemUsersOrganizations.ToList()
        //                        select vEmails.SystemUsers.UserName).ToList();

        //    var InvitationDetails = entityDB.VendorInviteDetails.FirstOrDefault(rec => VendorEmails.Contains(rec.VendorEmail) && rec.ClientId == preQualificationDetails.ClientId);
        //    if (InvitationDetails == null)
        //        ViewBag.InvitationSentBy = "Without Invitation";
        //    else if (InvitationDetails.InvitationSentFrom == 0)
        //        try
        //        {
        //            // Kiran on 3/27/2014
        //            ViewBag.InvitationSentBy = "Invitation from ";
        //            ViewBag.InvitationSentByUser = InvitationDetails.SystemUsers.Contacts.FirstOrDefault().FirstName + " " + InvitationDetails.SystemUsers.Contacts.FirstOrDefault().LastName;
        //            // Ends <<<
        //        }
        //        catch { }
        //    else
        //        ViewBag.InvitationSentBy = "Invitation from FV";
        //    //Ends<<<


        //    var PreQualificationsList = entityDB.Prequalification.Where(rec => rec.ClientId == preQualificationDetails.ClientId && rec.VendorId == preQualificationDetails.VendorId).OrderByDescending(rec => rec.PrequalificationStart).ToList();
        //    var dropDown_Prequalification = new List<SelectListItem>();
        //    SelectListItem latestPreQualification = new SelectListItem();
        //    latestPreQualification.Text = PreQualificationsList.FirstOrDefault().Client.Name + " " + PreQualificationsList.FirstOrDefault().PrequalificationStart.ToString("MM/dd/yyyy");
        //    latestPreQualification.Value = PreQualificationsList.FirstOrDefault().PrequalificationId + "";
        //    dropDown_Prequalification.Add(latestPreQualification);


        //    if (Session["RoleName"].ToString().Equals("Admin"))
        //    {
        //        dropDown_Prequalification = (from preQualifications in entityDB.Prequalification.Where(rec => rec.ClientId == preQualificationDetails.ClientId && rec.VendorId == preQualificationDetails.VendorId).OrderByDescending(rec => rec.PrequalificationStart).ToList()
        //                                     select new SelectListItem
        //                                     {
        //                                         Text = preQualificationDetails.Client.Name + " " + preQualifications.PrequalificationStart.ToString("MM/dd/yyyy"),
        //                                         Value = preQualifications.PrequalificationId + "",
        //                                         Selected = (PreQualificationId == preQualifications.PrequalificationId)
        //                                     }).ToList();
        //    }
        //    if (PreQualificationsList != null && PreQualificationsList.FirstOrDefault().PrequalificationFinish.Ticks <= DateTime.Now.AddDays(30).Ticks && PreQualificationsList.FirstOrDefault().PrequalificationStatusId == 9)
        //    {
        //        SelectListItem pre = new SelectListItem();
        //        pre.Text = "Renewal";
        //        pre.Value = "-1";
        //        dropDown_Prequalification.Add(pre);
        //    }
        //    ViewBag.VBPreQualifications = dropDown_Prequalification;
        //    Templates selectedTemplate = entityDB.Templates.Find(templateId);

        //    //Creating side Menus
        //    List<LocalTemplatesSideMenuModel> sideMenus = new List<LocalTemplatesSideMenuModel>();


        //    var initial = true;
        //    // Kiran on 6/24/2014
        //    if (@Session["RoleName"].ToString() != "Vendor")
        //    {
        //        LocalTemplatesSideMenuModel vendorDetailsMenu = new LocalTemplatesSideMenuModel("Vendor Details", "VendorDetails", "AdminVendors", false, "SelectedVendorId=" + preQualificationDetails.VendorId + "&PreQualificationId=" + PreQualificationId + "&TemplateSectionID=-1" + "&mode=" + 0);
        //        vendorDetailsMenu.isVisible = true;
        //        vendorDetailsMenu.isSectionCompleted = null;
        //        sideMenus.Add(vendorDetailsMenu);
        //        // Ends<<<
        //    }
        //    foreach (TemplateSections section in selectedTemplate.TemplateSections.Where(rec => rec.Visible == true).OrderBy(x => x.DisplayOrder).ToList())
        //    {
        //        if (initial && @Session["RoleName"].ToString() == "Admin")
        //        {
        //            LocalTemplatesSideMenuModel menu1 = new LocalTemplatesSideMenuModel("Change Status", "ChangeStatus", "Prequalification", true, "templateId=" + section.TemplateID + "&PreQualificationId=" + PreQualificationId);
        //            menu1.isVisible = true;
        //            menu1.isSectionCompleted = null;
        //            sideMenus.Add(menu1);
        //            initial = false;
        //        }
        //        LocalTemplatesSideMenuModel menu = new LocalTemplatesSideMenuModel(section.SectionName, "TemplateSectionsList", "Prequalification", false, "templateId=" + section.TemplateID + "&TemplateSectionID=" + section.TemplateSectionID + "&PreQualificationId=" + PreQualificationId);

        //        menu.isSectionCompleted = (entityDB.PrequalificationCompletedSections.FirstOrDefault(rec => rec.PrequalificationId == PreQualificationId && rec.TemplateSectionId == section.TemplateSectionID) != null);//Rajesh on 8/26/2013
        //        //Rajesh on 1/11/2014
        //        menu.isVisible = true;
        //        if (section.TemplateSectionType == 34)
        //        {
        //            if (preQualificationDetails.RequestForMoreInfo == null || preQualificationDetails.RequestForMoreInfo == false)
        //                menu.isVisible = false;
        //        }
        //        //Ends<<<
        //        //Rajesh on 1/21/2014
        //        if (section.TemplateSectionType == 35 && Session["RoleName"].ToString().Equals("Vendor"))//If Vendor Login Don't Client Data menu
        //        {
        //            //Do nothing
        //        }
        //        else
        //            sideMenus.Add(menu);
        //        //Ends<<<
        //    }

        //    //<a href="TemplateSectionsList?templateId=@v.TemplateID&TemplateSectionID=@v.TemplateSectionID"> @v.SectionName</a><br />
        //    ViewBag.sideMenu = sideMenus;

        //    ViewBag.VBDocumentsList = entityDB.Document.ToList();

        //    getCommand();
        //    ViewBag.TemplatePrice = getTemplatePrice(preQualificationDetails.VendorId, preQualificationDetails.ClientTemplates.TemplateID,preQualificationDetails.PrequalificationId);
        //    ViewBag.PreQualificationId = PreQualificationId;

        //    //For Change Status
        //    ViewBag.CurrentStatus = preQualificationDetails.PrequalificationStatus.PrequalificationStatusName;
        //    ViewBag.currentStatusPeriod = preQualificationDetails.PrequalificationStart.ToString("MM/dd/yyyy");


        //    LocalChangeStatus status = new LocalChangeStatus();
        //    status.templateId = templateId;
        //    status.PrequalificationId = PreQualificationId;
        //    status.NotCompletedSection = selectedTemplate.TemplateSections.Where(rec => rec.Visible==true).OrderBy(x => x.DisplayOrder).ToList();
        //    //SelectListItem item=new SelectListItem();
        //    //item.Value="-1";
        //    //item.Text="";
        //    //TemplatesList_dropDown.Add(item);

        //    status.NotCompletedSection_Status = new List<bool>();
        //    for (int i = 0; i < status.NotCompletedSection.Count; i++)
        //    {
        //        status.NotCompletedSection_Status.Add(false);
        //    }

        //    //Rajesh on 11/05/2013>>>
        //    status.NewPayment = preQualificationDetails.OverirdeAmount == null ? 0 : (decimal)preQualificationDetails.OverirdeAmount;
        //    status.isOverridePayment = preQualificationDetails.OverridePayment == null ? false : (bool)preQualificationDetails.OverridePayment;
        //    status.RequestForMoreInfo = preQualificationDetails.RequestForMoreInfo==null?false:(bool)preQualificationDetails.RequestForMoreInfo;
        //    //Ends<<


        //    var PreQualificationStatuses = new List<SelectListItem>();
        //    SelectListItem item = new SelectListItem();
        //    item.Value = "-1";
        //    item.Text = "";
        //    PreQualificationStatuses.Add(item);

        //    //Rajesh on 9/25/2014
        //    PreQualificationStatuses.AddRange((from a in entityDB.PrequalificationStatus.Where(rec => rec.PrequalificationStatusId > 5).ToList().Where(rec => rec.ShowInDashBoard == true)
        //                                       select new SelectListItem()
        //                                       {
        //                                           Text = a.PrequalificationStatusName,
        //                                           Value = a.PrequalificationStatusId + ""
        //                                       }).ToList());
        //    ViewBag.PreQualificationStatuses = PreQualificationStatuses;

        //    // Kiran on 12/03/2013
        //    var adminOrgId = entityDB.Organizations.FirstOrDefault(record => record.OrganizationType == "Admin").OrganizationID;

        //    // Kiran on 12/8/2013
        //    var UseridAsSigner = entityDB.Prequalification.FirstOrDefault(record => record.PrequalificationId == PreQualificationId).UserIdAsSigner;

        //    var adminUsers = (from sysUserOrg in entityDB.SystemUsersOrganizations.ToList()
        //                      from contact in entityDB.Contact.ToList()
        //                      where sysUserOrg.OrganizationId == adminOrgId && contact.UserId == sysUserOrg.UserId
        //                      select new SelectListItem
        //                      {
        //                          Text = contact.FirstName + " " + contact.LastName,
        //                          Value = contact.UserId.ToString(),
        //                          Selected = UseridAsSigner == contact.UserId
        //                      }).ToList();
        //    // Ends<<<

        //    SelectListItem defaultItem = new SelectListItem();
        //    defaultItem = new SelectListItem();
        //    defaultItem.Text = "";
        //    defaultItem.Value = "";
        //    var adminUsersWithDefaultValue = adminUsers.ToList();
        //    adminUsersWithDefaultValue.Insert(0, defaultItem);

        //    ViewBag.VBAdminUsers = adminUsersWithDefaultValue;
        //    // Ends<<<
        //    return View(status);
        //}

        // Kiran on 11/20/2013
        public JsonResult GetPeriodLengthAndUnit(int newStatus)
        {
            var currentStatusPeriodUnit = from periodUnit in entityDB.PrequalificationStatus.Where(record => record.PrequalificationStatusId == newStatus).ToList()
                                          select new SelectListItem
                                          {
                                              Text = periodUnit.PeriodLengthUnit,
                                              Value = periodUnit.PeriodLength + ""
                                          };

            return Json(currentStatusPeriodUnit);
        }
        // Ends<<<


        [HttpPost]
        [SessionExpire]
        [HeaderAndSidebar(headerName = "PREQUALIFICATIONREQUIREMENTS", sideMenuName = "", ViewName = "TemplateSectionsList")]
        public ActionResult ChangeStatus(LocalChangeStatus form)
        {
            //Rajesh On 8-31-2013 >>>
            @ViewBag.mode = 0;
            @ViewBag.templateId = form.templateId;

            // Kiran on 12/03/2013
            var adminOrgId = entityDB.Organizations.FirstOrDefault(record => record.OrganizationType == "Admin").OrganizationID;

            var UserIds = entityDB.SystemUsersOrganizations.Where(rec => rec.OrganizationId == adminOrgId).Select(rec => rec.UserId).ToList();

            var adminUsers = (from systemUsers in entityDB.SystemUsers.Where(rec => UserIds.Contains(rec.UserId)).ToList()
                              select new SelectListItem
                              {
                                  Text = systemUsers.Contacts.FirstOrDefault() != null ? systemUsers.Contacts[0].LastName + ", " + systemUsers.Contacts[0].FirstName : systemUsers.Email,
                                  Value = systemUsers.UserId.ToString(),

                              }).ToList();
            // Kiran on 03/18/2015 Ends<<<

            SelectListItem defaultItem = new SelectListItem();
            defaultItem = new SelectListItem();
            defaultItem.Text = "";
            defaultItem.Value = "";
            var adminUsersWithDefaultValue = adminUsers.ToList();
            adminUsersWithDefaultValue.Insert(0, defaultItem);

            ViewBag.VBAdminUsers = adminUsersWithDefaultValue;
            // Ends<<<

            // Kiran on 12/11/2013
            Prequalification preQualificationDetails_loc = entityDB.Prequalification.Find(form.PrequalificationId);
            ViewBag.StatusName = preQualificationDetails_loc.PrequalificationStatus.PrequalificationStatusName; // Kiran on 3/28/2014
            ViewBag.PrequalStartDate = preQualificationDetails_loc.PrequalificationCreate.ToString("MM/dd/yyyy"); // Kiran on 3/28/2014
            // Kiran on 3/15/2014
            if (@Session["RoleName"].ToString() == "Admin" || (@Session["RoleName"].ToString() == "Client" && ViewBag.mode == 1) || (@Session["RoleName"].ToString() == "Vendor" && ViewBag.mode == 1)) // Kiran on 3/25/2014            
            {
                ViewBag.VBStatusColor = preQualificationDetails_loc.PrequalificationStatus.BannerColor;
            }
            else if ((ViewBag.mode == null || ViewBag.mode == 0 && @Session["RoleName"].ToString() == "Client") || (ViewBag.mode == null || ViewBag.mode == 0 && @Session["RoleName"].ToString() == "Vendor"))
            {
                ViewBag.VBStatusColor = LocalConstants.PQ_DEFAULT_BG_COLOR;
            }
            // Ends<<<

            // Kiran on 3/28/2014
            ViewBag.StatusId = preQualificationDetails_loc.PrequalificationStatusId;
            if (@ViewBag.StatusId.ToString() == "10")
            {
                var previousPrequalStatusRecords = entityDB.PrequalificationStatusChangeLog.Where(rec => rec.PrequalificationId == preQualificationDetails_loc.PrequalificationId && rec.PrequalificationStatusId != preQualificationDetails_loc.PrequalificationStatusId).OrderByDescending(record => record.PrequalStatusLogId).ToList();
                if (previousPrequalStatusRecords != null)
                {
                    var prequalificationStatusChangeRec = previousPrequalStatusRecords.FirstOrDefault();
                    var previousPrequalStatusId = prequalificationStatusChangeRec.PrequalificationStatusId;
                    var preQualificationStatusRec = entityDB.PrequalificationStatus.Find(previousPrequalStatusId);
                    ViewBag.VBPreviousStatusColour = entityDB.PrequalificationStatus.FirstOrDefault(rec => rec.PrequalificationStatusId == previousPrequalStatusId).BannerColor;
                    ViewBag.VBPreviousStatusName = entityDB.PrequalificationStatus.FirstOrDefault(rec => rec.PrequalificationStatusId == previousPrequalStatusId).PrequalificationStatusName;

                    // Commented on 02/11/2015 to display the current status along with previous status(Ex:- Prequalified-Expired) & current period instead of previous status period.
                    //ViewBag.VBPreviousPeriod = prequalificationStatusChangeRec.PrequalificationStart.ToString("MM/dd/yyyy") + " - " + prequalificationStatusChangeRec.PrequalificationFinish.ToString("MM/dd/yyyy");
                }
            }
            //preQualificationDetails_loc.RequestForMoreInfo = form.RequestForMoreInfo; // Kiran 01/11/2014

            if (form.isStatusChanged == false)
            {
                ModelState.AddModelError("", "Please confirm status change");
            }
            else if (form.NewStatus != "16" && form.NewStatus != "10") // Kiran on 02/04/2015
            {
                if (form.adminUserId == "-1" || form.adminUserId == null)
                {
                    ModelState.AddModelError("adminUserId", "Please select, to whom the vendor is assigned");
                }
                else
                {
                    preQualificationDetails_loc.UserIdAsSigner = new Guid(form.adminUserId);
                    preQualificationDetails_loc.RequestForMoreInfo = form.RequestForMoreInfo; // Rajesh on 10/1/2014
                    entityDB.Entry(preQualificationDetails_loc).State = EntityState.Modified;
                    entityDB.SaveChanges();
                }
            }
            // Ends<<<

            if (form.NewStatus != "-1" && form.NewStatus != "16" && form.NewStatus != "10")//Rajesh on 10/1/2014 // Kiran on 02/04/2015
            {
                if (form.NewPeriodStart == null || form.NewPeriodStart == "")
                {
                    ModelState.AddModelError("NewPeriodStart", "Please select New Period Start Date");
                }
                if (form.NewStatusChanged == null || form.NewStatusChanged == "")
                {
                    ModelState.AddModelError("NewStatusChanged", "Please select New Status Change Date");
                }
            }
            //Ends <<<
            //if (ModelState.IsValid)
            //{
            //    //Mani on 16-07-2015 SO-140
            //    var prequalificationStatus = new string[] { "9", "13", "24", "25", "26" };
            //    var NewStatus = Convert.ToString(form.NewStatus);
            //    var templatePrice = entityDB.TemplatePrices.FirstOrDefault(rec => rec.TemplateID == preQualificationDetails_loc.ClientTemplates.TemplateID).TemplatePrice;
            //    var preqOpenperiod = entityDB.OrganizationsPrequalificationOpenPeriod.Where(rec => rec.VendorId == preQualificationDetails_loc.VendorId).OrderByDescending(rec => rec.PrequalificationPeriodClose).FirstOrDefault();
            //    if (form.NewPeriodStart != null && preqOpenperiod != null && prequalificationStatus.Contains(NewStatus) && preQualificationDetails_loc.PaymentReceivedAmount != null)
            //    {
            //        DateTime newperiod = DateTime.ParseExact(form.NewPeriodStart, "MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);
            //        if (newperiod > preqOpenperiod.PrequalificationPeriodClose && templatePrice != preQualificationDetails_loc.PaymentReceivedAmount)
            //        {
            //            ModelState.AddModelError("", "Prequalification discount is not applicable as the annual period ends.Hence status can't be changed");
            //        }
            //    }
            //}
            //Ends >>>
            if (ModelState.IsValid)
            {
                if (!form.NewStatus.Equals("-1"))//Rajesh on 11/5/2013
                {
                    if (!(preQualificationDetails_loc.PrequalificationStatusId == Convert.ToInt64(form.NewStatus) && preQualificationDetails_loc.PrequalificationStatusId == 7))//Rajesh on 9/25/2014
                    {
                        preQualificationDetails_loc.PrequalificationStatusId = Convert.ToInt64(form.NewStatus);
                        if (Convert.ToInt64(form.NewStatus) != 16 && Convert.ToInt64(form.NewStatus) != 10)//Rajesh on 10/1/2014 If it is client review we need to place logs rec only // Kiran on 02/04/2015
                        {
                            preQualificationDetails_loc.RequestForMoreInfo = form.RequestForMoreInfo; // Rajesh on 10/1/2014
                            preQualificationDetails_loc.PrequalificationStart = Convert.ToDateTime(form.NewPeriodStart);
                            preQualificationDetails_loc.UserIdAsSigner = new Guid(form.adminUserId); // Kiran on 12/03/2013
                            preQualificationDetails_loc.RequestForMoreInfo = form.RequestForMoreInfo; // Kiran 01/11/2014

                            // Kiran on 11/22/2013
                            if (form.PeriodLengthUnit.ToLower().Equals("d"))
                                preQualificationDetails_loc.PrequalificationFinish = Convert.ToDateTime(form.NewPeriodStart).AddDays(form.PeriodLength);
                            else if (form.PeriodLengthUnit.ToLower().Equals("m"))
                                preQualificationDetails_loc.PrequalificationFinish = Convert.ToDateTime(form.NewPeriodStart).AddMonths(form.PeriodLength);
                            else if (form.PeriodLengthUnit.ToLower().Equals("y"))
                                preQualificationDetails_loc.PrequalificationFinish = Convert.ToDateTime(form.NewPeriodStart).AddYears(form.PeriodLength);
                        }
                        PrequalificationStatusChangeLog newStatusChangeLog = new PrequalificationStatusChangeLog();
                        newStatusChangeLog.PrequalificationId = form.PrequalificationId;
                        newStatusChangeLog.PrequalificationStatusId = Convert.ToInt64(form.NewStatus);// Kiran on 12/11/2013
                        if (Convert.ToInt64(form.NewStatus) != 16 && Convert.ToInt64(form.NewStatus) != 10)//Rajesh on 10/1/2014 If it is client review we need to place logs rec only  // Kiran on 02/04/2015
                            newStatusChangeLog.StatusChangeDate = Convert.ToDateTime(form.NewStatusChanged).Add(TimeSpan.Parse(DateTime.Now.ToString("HH:MM:ss")));
                        else
                            newStatusChangeLog.StatusChangeDate = DateTime.Now;
                        newStatusChangeLog.StatusChangedByUser = (Guid)Session["UserId"];
                        newStatusChangeLog.PrequalificationStart = preQualificationDetails_loc.PrequalificationStart;
                        newStatusChangeLog.PrequalificationFinish = preQualificationDetails_loc.PrequalificationFinish;
                        entityDB.PrequalificationStatusChangeLog.Add(newStatusChangeLog);
                        entityDB.SaveChanges();
                        // Ends<<<

                        entityDB.Entry(preQualificationDetails_loc).State = EntityState.Modified;
                    }
                }

                //To override payment
                //Rajesh on 11/5/2013
                //if (form.isOverridePayment)
                //{
                //    preQualificationDetails_loc.OverirdeAmount = form.NewPayment;
                //    preQualificationDetails_loc.OverridePayment = form.isOverridePayment;
                //    preQualificationDetails_loc.PaymentReceived = true;
                //    entityDB.Entry(preQualificationDetails_loc).State = EntityState.Modified;

                //    try
                //    {
                //        var sectionId=preQualificationDetails_loc.ClientTemplates.Templates.TemplateSections.FirstOrDefault(rec1 => rec1.TemplateSectionType == 3).TemplateSectionID;
                //        PrequalificationCompletedSections sectionCompleted = entityDB.PrequalificationCompletedSections.FirstOrDefault(rec => rec.PrequalificationId == form.PrequalificationId && rec.TemplateSectionId == sectionId);

                //        if (sectionCompleted == null)
                //        {
                //            sectionCompleted = new PrequalificationCompletedSections();
                //            sectionCompleted.PrequalificationId = form.PrequalificationId;
                //            sectionCompleted.TemplateSectionId = preQualificationDetails_loc.ClientTemplates.Templates.TemplateSections.FirstOrDefault(rec1 => rec1.TemplateSectionType == 3).TemplateSectionID;
                //            entityDB.PrequalificationCompletedSections.Add(sectionCompleted);

                //        }

                //    }
                //    catch (Exception ee) { }
                //}
                //else
                //{
                //    preQualificationDetails_loc.OverirdeAmount = 0;
                //    preQualificationDetails_loc.OverridePayment = false;
                //    preQualificationDetails_loc.PaymentReceived = false;

                //    entityDB.Entry(preQualificationDetails_loc).State = EntityState.Modified;

                //    try
                //    {
                //        var sectionId=preQualificationDetails_loc.ClientTemplates.Templates.TemplateSections.FirstOrDefault(rec1 => rec1.TemplateSectionType == 3).TemplateSectionID;
                //        PrequalificationCompletedSections sectionCompleted = entityDB.PrequalificationCompletedSections.FirstOrDefault(rec => rec.PrequalificationId == form.PrequalificationId && rec.TemplateSectionId == sectionId);
                //        entityDB.PrequalificationCompletedSections.Remove(sectionCompleted);


                //    }
                //    catch (Exception ee) { }
                //}
                ////Ends<<

                //To Change PrequalificationCompletedSections
                if (form.NewStatus == "7")
                {
                    for (var i = 0; i < form.NotCompletedSection.Count; i++)
                    {
                        if (!form.NotCompletedSection_Status[i]) continue;
                        var sectionId = form.NotCompletedSection[i].TemplateSectionID;
                        var pendingSection = entityDB.PrequalificationCompletedSections.FirstOrDefault(rec => rec.PrequalificationId == form.PrequalificationId && rec.TemplateSectionId == sectionId);
                        if (pendingSection != null)
                            entityDB.PrequalificationCompletedSections.Remove(pendingSection);
                    }
                    //To Make finalize and submit as a non completd section if any section is marked as incomplete
                    if (form.NotCompletedSection.Count > 0)
                    {
                        long SectionForFinalizeAndSubmit = -1;
                        try
                        {
                            SectionForFinalizeAndSubmit = preQualificationDetails_loc.ClientTemplates.Templates.TemplateSections.FirstOrDefault(rec => rec.TemplateSectionType == 99).TemplateSectionID;
                        }
                        catch { }

                        var pendingSection = entityDB.PrequalificationCompletedSections.FirstOrDefault(rec => rec.PrequalificationId == form.PrequalificationId && rec.TemplateSectionId == SectionForFinalizeAndSubmit);
                        if (pendingSection != null)
                            entityDB.PrequalificationCompletedSections.Remove(pendingSection);
                    }
                }
                entityDB.SaveChanges();

                // Kiran on 11/5/2014
                var empTrainingRoleId = entityDB.SystemRoles.FirstOrDefault(rec => rec.RoleName == "Employee Training").RoleId;
                // kiran on 11/8/2014

                // Kiran on 12/24/2014
                //if ((preQualificationDetails_loc.PrequalificationStatusId == 10 && form.NewStatus == "10") || (preQualificationDetails_loc.PrequalificationStatusId == 15 && form.NewStatus == "15"))
                //{
                //    var empUserOrgRoles = entityDB.SystemUsersOrganizationsRoles.Where(rec => rec.SysRoleId == empTrainingRoleId && rec.SystemUsersOrganizations.OrganizationId == preQualificationDetails_loc.VendorId).ToList();
                //    if (empUserOrgRoles != null)
                //    {
                //        foreach (var userOrgRole in empUserOrgRoles)
                //        {
                //            var empSysUserOrgQuizzes = userOrgRole.SystemUsersOrganizations.SystemUsersOrganizationsQuizzes.ToList();
                //            if (empSysUserOrgQuizzes != null)
                //            {
                //                foreach (var empQuiz in empSysUserOrgQuizzes)
                //                {
                //                    empQuiz.QuizRemoved = true;
                //                    entityDB.Entry(empQuiz).State = EntityState.Modified;
                //                    entityDB.SaveChanges();
                //                }
                //            }
                //        }
                //    }       
                //}
                // Ends<<<

                //var emailTemplateFormatForEmployeeInvitation = entityDB.EmailTemplates.FirstOrDefault(rec => rec.EmailUsedFor == 6);
                //var subject = emailTemplateFormatForEmployeeInvitation.EmailSubject;
                //var body = emailTemplateFormatForEmployeeInvitation.EmailBody;
                // Ends<<<

                //if ((preQualificationDetails_loc.PrequalificationStatusId != 14 && form.NewStatus != "14")) // Kiran on 02/05/2015 given the provision to assign trainings under exception/probation related statuses, prequalified status // kiran on 02/07/2015 to assign trainings to employee under any status except N/A as per the email 02/07/2015.
                ////if (preQualificationDetails_loc.PrequalificationStatusId == 9 && form.NewStatus == "9")
                //{
                //    if (preQualificationDetails_loc.Client.TrainingRequired == true)
                //    {
                //        var clientQuizTemplates = entityDB.ClientTemplates.Where(rec => rec.ClientID == preQualificationDetails_loc.ClientId).ToList();
                //        foreach (var clientQuizTemplate in clientQuizTemplates)
                //        {
                //            if (clientQuizTemplate.Templates.TemplateType == 1 && clientQuizTemplate.Templates.TemplateStatus == 1)
                //            {
                //                if (preQualificationDetails_loc.PrequalificationSites != null)
                //                {
                //                    var prequalSites = entityDB.PrequalificationSites.Where(rec => rec.PrequalificationId == preQualificationDetails_loc.PrequalificationId).Select(rec => rec.ClientBusinessUnitId).ToList();

                //                    // Kiran on 12/18/2014
                //                    var quizClientTemplateBus = entityDB.ClientTemplatesForBU.Where(rec => prequalSites.Contains(rec.ClientBusinessUnitId) && rec.ClientTemplateId == clientQuizTemplate.ClientTemplateID).ToList();
                //                    var empUserOrgRoles = entityDB.SystemUsersOrganizationsRoles.Where(rec => rec.SysRoleId == empTrainingRoleId && rec.SystemUsersOrganizations.OrganizationId == preQualificationDetails_loc.VendorId).ToList();
                //                    //foreach (var empUserOrgRole in empUserOrgRoles)
                //                    //{
                //                    //    var employeeQuiz = empUserOrgRole.SystemUsersOrganizations.SystemUsersOrganizationsQuizzes.FirstOrDefault(rec => rec.ClientTemplateId == clientQuizTemplate.ClientTemplateID);
                //                    //    if (employeeQuiz != null)
                //                    //    {
                //                    //        try
                //                    //        {
                //                    //            entityDB.SystemUsersOrganizationsQuizzes.Remove(employeeQuiz);
                //                    //            entityDB.SaveChanges(); // Kiran on 12/5/2014
                //                    //        }
                //                    //        catch
                //                    //        {
                //                    //            employeeQuiz.QuizRemoved = true;
                //                    //            entityDB.Entry(employeeQuiz).State = EntityState.Modified;
                //                    //            entityDB.SaveChanges(); // Kiran on 12/5/2014
                //                    //        }
                //                    //    }
                //                    //}

                //                    if (quizClientTemplateBus != null && quizClientTemplateBus.Count != 0) // Kiran on 11/8/2014
                //                    {
                //                        foreach (var userOrgRole in empUserOrgRoles)
                //                        {
                //                            var empQuiz = userOrgRole.SystemUsersOrganizations.SystemUsersOrganizationsQuizzes.FirstOrDefault(rec => rec.ClientTemplateId == clientQuizTemplate.ClientTemplateID);
                //                            if (empQuiz == null && userOrgRole.SystemUsersOrganizations.SystemUsers.UserStatus == true) // Kiran on 01/22/2015
                //                            {
                //                                SystemUsersOrganizationsQuizzes addQuizForEmployee = new SystemUsersOrganizationsQuizzes();
                //                                addQuizForEmployee.SysUserOrgId = userOrgRole.SystemUsersOrganizations.SysUserOrganizationId;
                //                                addQuizForEmployee.ClientTemplateId = clientQuizTemplate.ClientTemplateID;
                //                                addQuizForEmployee.QuizRemoved = false;
                //                                // Kiran on 12/18/2014
                //                                addQuizForEmployee.VendorId = preQualificationDetails_loc.VendorId;
                //                                addQuizForEmployee.ClientId = preQualificationDetails_loc.ClientId;
                //                                // Ends<<<
                //                                entityDB.SystemUsersOrganizationsQuizzes.Add(addQuizForEmployee);
                //                                entityDB.SaveChanges(); // Kiran on 12/5/2014
                //                            }
                //                            else
                //                            { 
                //                                empQuiz.QuizRemoved = false;
                //                                entityDB.SaveChanges();
                //                            }
                //                        }
                //                    }
                //                    // Ends<<<
                //                }
                //            }
                //        }
                //        // kiran on 11/8/2014
                //        if (clientQuizTemplates != null)
                //        {
                //            var vendorSuperuserContact = entityDB.SystemUsersOrganizations.FirstOrDefault(rec => rec.PrimaryUser == true).SystemUsers.Contacts[0];
                //            var vendorUserName = vendorSuperuserContact.FirstName + " " + vendorSuperuserContact.LastName;

                //            var empUserOrgRoles = entityDB.SystemUsersOrganizationsRoles.Where(rec => rec.SysRoleId == empTrainingRoleId && rec.SystemUsersOrganizations.OrganizationId == preQualificationDetails_loc.VendorId).ToList();
                //            foreach (var employeeRole in empUserOrgRoles)
                //            {
                //                // Kiran on 01/22/2015
                //                if (employeeRole.SystemUsersOrganizations.SystemUsers.UserStatus == true)
                //                {
                //                    subject = subject.Replace("[ClientName]", preQualificationDetails_loc.Client.Name);
                //                    body = body.Replace("[CLIENTNAME]", preQualificationDetails_loc.Client.Name);
                //                    body = body.Replace("[VENDORNAME]", preQualificationDetails_loc.Vendor.Name);
                //                    body = body.Replace("[VendorUserName]", vendorUserName);
                //                    body = body.Replace("[Date]", DateTime.Now + "");
                //                    var empContact = employeeRole.SystemUsersOrganizations.SystemUsers.Contacts[0];
                //                    body = body.Replace("[EmployeeName]", empContact.FirstName + " " + empContact.LastName);
                //                    body = body.Replace("[Email]", employeeRole.SystemUsersOrganizations.SystemUsers.Email);
                //                    body = body.Replace("[Password]", Utilities.EncryptionDecryption.Decryptdata(employeeRole.SystemUsersOrganizations.SystemUsers.Password));
                //                    body = body + "</br>" + emailTemplateFormatForEmployeeInvitation.CommentText;

                //                    var message = Utilities.SendMail.sendMail(employeeRole.SystemUsersOrganizations.SystemUsers.Email, body, subject);
                //                }
                //                // Ends<<<
                //            }
                //            // Ends<<<
                //        }
                //    }
                //}
                //entityDB.SaveChanges();
                // Ends <<<
                return Redirect("../Prequalification/TemplateSectionsList?templateId=" + preQualificationDetails_loc.ClientTemplates.TemplateID + "&TemplateSectionID=-1&PreQualificationId=" + form.PrequalificationId);

                //return Redirect("../Prequalification/TemplateSectionsList?templateId=" + preQualificationDetails_loc.ClientTemplates.TemplateID + "&TemplateSectionID=-1&PreQualificationId=" + form.PrequalificationId);


            }


            Prequalification preQualificationDetails = entityDB.Prequalification.Find(form.PrequalificationId);
            ViewBag.TemplateCode = preQualificationDetails.ClientTemplates.Templates.TemplateCode;
            ViewBag.VendorId = preQualificationDetails.VendorId;
            ViewBag.ClientId = preQualificationDetails.ClientId;
            ViewBag.ClientName = preQualificationDetails.Client.Name;
            ViewBag.VendorName = preQualificationDetails.Vendor.Name;
            ViewBag.Period = preQualificationDetails.PrequalificationStart.ToString("MM/dd/yy") + "-" + preQualificationDetails.PrequalificationFinish.ToString("MM/dd/yy");
            ViewBag.LogoPath = "";
            ViewBag.isEdit = preQualificationDetails.PrequalificationStatusId == 10;
            ViewBag.LogoPath = ConfigurationSettings.AppSettings["LogoUploadPath"].ToString() + "/";//Rajesh on 08/09/2013



            var PreQualificationsList = entityDB.Prequalification.Where(rec => rec.ClientId == preQualificationDetails.ClientId && rec.VendorId == preQualificationDetails.VendorId).OrderByDescending(rec => rec.PrequalificationStart).ToList();
            var dropDown_Prequalification = new List<SelectListItem>();
            SelectListItem latestPreQualification = new SelectListItem();
            latestPreQualification.Text = PreQualificationsList.FirstOrDefault().Client.Name + " " + PreQualificationsList.FirstOrDefault().PrequalificationStart.ToString("MM/dd/yyyy");
            latestPreQualification.Value = PreQualificationsList.FirstOrDefault().PrequalificationId + "";
            dropDown_Prequalification.Add(latestPreQualification);

            //Rajesh on 9/25/2014
            ViewBag.PrequalificationStartDate = preQualificationDetails.PrequalificationStart.ToString("MM/dd/yyyy");
            ViewBag.currentDate = DateTime.Now.ToString("MM/dd/yyyy");
            //Ends<<

            if (Session["RoleName"].ToString().Equals("Admin"))
            {
                dropDown_Prequalification = (from preQualifications in entityDB.Prequalification.Where(rec => rec.ClientId == preQualificationDetails.ClientId && rec.VendorId == preQualificationDetails.VendorId).OrderByDescending(rec => rec.PrequalificationStart).ToList()
                                             select new SelectListItem
                                             {
                                                 Text = preQualificationDetails.Client.Name + " " + preQualifications.PrequalificationStart.ToString("MM/dd/yyyy"),
                                                 Value = preQualifications.PrequalificationId + "",
                                                 Selected = (form.PrequalificationId == preQualifications.PrequalificationId)
                                             }).ToList();
            }
            if (PreQualificationsList != null && (PreQualificationsList.FirstOrDefault().PrequalificationFinish.Date.Ticks >= DateTime.Now.Date.Ticks && PreQualificationsList.FirstOrDefault().PrequalificationFinish.AddDays(-30).Date.Ticks <= DateTime.Now.Date.Ticks) && (PreQualificationsList.FirstOrDefault().PrequalificationStatusId == 4 || PreQualificationsList.FirstOrDefault().PrequalificationStatusId == 8 || PreQualificationsList.FirstOrDefault().PrequalificationStatusId == 9 || PreQualificationsList.FirstOrDefault().PrequalificationStatusId == 13 || PreQualificationsList.FirstOrDefault().PrequalificationStatusId == 23 || PreQualificationsList.FirstOrDefault().PrequalificationStatusId == 24 || PreQualificationsList.FirstOrDefault().PrequalificationStatusId == 25 || PreQualificationsList.FirstOrDefault().PrequalificationStatusId == 26))//Rajesh on 3/25/2014 // Kiran on 02/04/2015 for opening the renewal option for different statuses as per the email dated 02/03/2015.
            {
                SelectListItem pre = new SelectListItem();
                pre.Text = "Renewal";
                pre.Value = "-1";
                dropDown_Prequalification.Add(pre);
            }
            ViewBag.VBPreQualifications = dropDown_Prequalification;
            Templates selectedTemplate = entityDB.Templates.Find(form.templateId);

            //Creating side Menus
            List<LocalTemplatesSideMenuModel> sideMenus = new List<LocalTemplatesSideMenuModel>();


            var initial = true;
            // Kiran on 6/24/2014
            if (@Session["RoleName"].ToString() != "Vendor")
            {
                LocalTemplatesSideMenuModel vendorDetailsMenu = new LocalTemplatesSideMenuModel("Vendor Details", "VendorDetails", "AdminVendors", false, "SelectedVendorId=" + preQualificationDetails.VendorId + "&PreQualificationId=" + form.PrequalificationId + "&TemplateSectionID=-1" + "&mode=" + 0);
                vendorDetailsMenu.isVisible = true;
                vendorDetailsMenu.isSectionCompleted = null;
                sideMenus.Add(vendorDetailsMenu);
                // Ends<<<
            }
            foreach (TemplateSections section in selectedTemplate.TemplateSections.Where(rec => rec.Visible == true).OrderBy(x => x.DisplayOrder).ToList())
            {
                if (initial && @Session["RoleName"].ToString() == "Admin")
                {
                    LocalTemplatesSideMenuModel menu1 = new LocalTemplatesSideMenuModel("Change Status", "ChangeStatus", "Prequalification", true, "templateId=" + section.TemplateID + "&PreQualificationId=" + form.PrequalificationId);
                    menu1.isVisible = true;
                    menu1.isSectionCompleted = null;
                    sideMenus.Add(menu1);
                    initial = false;
                }
                LocalTemplatesSideMenuModel menu = new LocalTemplatesSideMenuModel(section.SectionName, "TemplateSectionsList", "Prequalification", false, "templateId=" + section.TemplateID + "&TemplateSectionID=" + section.TemplateSectionID + "&PreQualificationId=" + form.PrequalificationId);

                menu.isSectionCompleted = (entityDB.PrequalificationCompletedSections.FirstOrDefault(rec => rec.PrequalificationId == form.PrequalificationId && rec.TemplateSectionId == section.TemplateSectionID) != null);//Rajesh on 8/26/2013
                //Rajesh on 1/11/2014
                menu.isVisible = true;
                if (section.TemplateSectionType == 34)
                {
                    if (preQualificationDetails.RequestForMoreInfo == null || preQualificationDetails.RequestForMoreInfo == false)
                        menu.isVisible = false;
                }
                //Ends<<<

                //Rajesh on 1/21/2014
                if (section.TemplateSectionType == 35 && Session["RoleName"].ToString().Equals("Vendor"))//If Vendor Login Don't Client Data menu
                {
                    //Do nothing
                }
                else
                    sideMenus.Add(menu);
                //Ends<<<


            }
            //Rajesh on 11/3/2014 do the same for admin vendors
            if (@Session["RoleName"].ToString() == "Admin" || @Session["RoleName"].ToString() == "Client")
            {
                var menu2 = new LocalTemplatesSideMenuModel("Employees", "Employees", "Prequalification", false, "templateId=" + form.templateId + "&PreQualificationId=" + form.PrequalificationId);
                menu2.isVisible = true;
                menu2.isSectionCompleted = null;
                initial = false;
                //sideMenus.Add(menu2);
                if (@Session["RoleName"].ToString() == "Admin")
                {
                    sideMenus.Insert(2, menu2);
                }
                else
                {
                    sideMenus.Insert(1, menu2);
                }
            }
            //<a href="TemplateSectionsList?templateId=@v.TemplateID&TemplateSectionID=@v.TemplateSectionID"> @v.SectionName</a><br />
            ViewBag.sideMenu = sideMenus;

            ViewBag.VBDocumentsList = entityDB.Document;

            getCommand();
            ViewBag.TemplatePrice = getTemplatePrice(preQualificationDetails.VendorId, preQualificationDetails.ClientTemplates.ClientTemplateID, preQualificationDetails.PrequalificationId);
            ViewBag.PreQualificationId = form.PrequalificationId;

            //For Change Status
            ViewBag.CurrentStatus = preQualificationDetails.PrequalificationStatus.PrequalificationStatusName;
            ViewBag.currentStatusPeriod = preQualificationDetails.PrequalificationStart.ToString("MM/dd/yyyy");


            LocalChangeStatus status = new LocalChangeStatus();
            status.templateId = form.templateId;
            status.PrequalificationId = form.PrequalificationId;

            var selectedStatus = form.NewStatus; // Kiran on 12/9/2013

            status.NotCompletedSection = selectedTemplate.TemplateSections.Where(rec => rec.Visible == true).OrderBy(x => x.DisplayOrder).ToList();
            //SelectListItem item=new SelectListItem();
            //item.Value="-1";
            //item.Text="";
            //TemplatesList_dropDown.Add(item);

            status.NotCompletedSection_Status = new List<bool>();
            for (int i = 0; i < status.NotCompletedSection.Count; i++)
            {
                status.NotCompletedSection_Status.Add(false);
            }



            var PreQualificationStatuses = new List<SelectListItem>();
            SelectListItem item = new SelectListItem();
            item.Value = "-1";
            item.Text = "";
            PreQualificationStatuses.Add(item);

            // Kiran on 12/9/2013
            PreQualificationStatuses.AddRange((from a in entityDB.PrequalificationStatus.Where(rec => rec.PrequalificationStatusId > 2).ToList().Where(rec => rec.ShowInDashBoard == true).OrderBy(rec => rec.PrequalificationStatusName) // Kiran on 01/13/2015
                                               select new SelectListItem()
                                               {
                                                   Text = a.PrequalificationStatusName,
                                                   Value = a.PrequalificationStatusId + "",
                                               }).ToList());
            // Ends<<<

            ViewBag.PreQualificationStatuses = PreQualificationStatuses;

            // Kiran on 03/20/2015
            var VendorEmails = preQualificationDetails.Vendor.SystemUsersOrganizations.Select(rec => rec.SystemUsers.UserName).ToList();

            var InvitationDetails = entityDB.VendorInviteDetails.FirstOrDefault(rec => VendorEmails.Contains(rec.VendorEmail) && rec.ClientId == preQualificationDetails.ClientId);
            if (InvitationDetails == null)
                ViewBag.InvitationSentBy = "Without Invitation";
            else if (InvitationDetails.InvitationSentFrom == 0)
                try
                {
                    // Kiran on 3/27/2014
                    ViewBag.InvitationSentBy = "Invitation from ";
                    ViewBag.InvitationSentByUser = InvitationDetails.SystemUsers.Contacts.FirstOrDefault().LastName + ", " + InvitationDetails.SystemUsers.Contacts.FirstOrDefault().FirstName;
                    // Ends <<<
                }
                catch { }
            else
                ViewBag.InvitationSentBy = "Invitation from InfusionSoft";


            return View(status);
        }



        //DV:RP DT:6/21/2013

        //Purpose:  To Open Dynamic Popup View
        [SessionExpireForView]
        public ActionResult DynamicPopUpView(long subSectionId, long PrequalificationId)
        {
            ViewBag.PrequalificationId = PrequalificationId;
            TemplateSubSections subsection = entityDB.TemplateSubSections.Find(subSectionId);
            if (subsection.SubSectionTypeCondition.Equals("SitePrequalification"))
            {
                return Redirect("PrequalificationSites" + QueryStringModule.Encrypt("preQualificationSiteId=-1&subSectionId=" + subSectionId + "&preQualificationId=" + PrequalificationId));
            }
            else if (subsection.SubSectionTypeCondition.Equals("SupportingDocument"))
            {
                return Redirect("SupportingDocument" + QueryStringModule.Encrypt("subSectionId=" + subSectionId + "&PrequalificationId=" + PrequalificationId));
            }
            else if (subsection.SubSectionTypeCondition.Equals("PrequalComments"))
            {
                return Redirect("AddComments" + QueryStringModule.Encrypt("subSectionId=" + subSectionId + "&PrequalificationId=" + PrequalificationId));
            }
            return Redirect("PrequalificationSites" + QueryStringModule.Encrypt("preQualificationSiteId=-1&subSectionId=" + subSectionId + "&preQualificationId=" + PrequalificationId));
        }
        public ActionResult AddComments(long subSectionId, long PrequalificationId)
        {
            LocalPrequalificationComments form = new LocalPrequalificationComments();
            form.PrequalificationId = PrequalificationId;
            form.SubSectionID = subSectionId;
            form.TemplateSectionID = entityDB.TemplateSubSections.Find(subSectionId).TemplateSectionID;
            return View(form);
        }
        [HttpPost]
        public ActionResult AddComments(LocalPrequalificationComments form)//Comments can only insertion
        {
            PrequalificationComments comments = new PrequalificationComments();
            comments.Comments = form.Comments;
            comments.CommentsBy = (Guid)Session["UserId"];
            comments.CommentsDate = DateTime.Now;
            comments.CommentsTime = DateTime.Now.TimeOfDay;
            comments.PrequalificationId = form.PrequalificationId;
            comments.SubSectionID = form.SubSectionID;
            comments.TemplateSectionID = form.TemplateSectionID;
            entityDB.PrequalificationComments.Add(comments);
            entityDB.SaveChanges();
            return Redirect("PopUpCloseView?subSectionId=" + form.SubSectionID);
        }
        [SessionExpireForView]
        public ActionResult SupportingDocument(Guid? DocumentId, long subSectionId, int? mode, long PrequalificationId)
        {
            var currentPreQualification = entityDB.Prequalification.Find(PrequalificationId);
            LocalSupportedDocument form = new LocalSupportedDocument();
            var documentTypeIds = entityDB.ClientTemplateReportingData.Where(rec => rec.ReportingType == 2 && rec.ClientTemplateId == currentPreQualification.ClientTemplateId).Select(rec => rec.ReportingTypeId).ToList();

            ViewBag.DocumentTypes = (from DocType in entityDB.DocumentType.ToList().Where(rec => rec.PrequalificationUseOnly == true && documentTypeIds.Contains(rec.DocumentTypeId))
                                     select new SelectListItem
                                     {
                                         Text = DocType.DocumentTypeName,
                                         Value = DocType.DocumentTypeId + ""
                                     });
            form.subSectionId = subSectionId;
            form.PrequalificationId = PrequalificationId;
            form.Mode = mode;//1-means edit Mode
            if (DocumentId != null)
            {
                Document d = entityDB.Document.Find(DocumentId);
                form.DocumentTypeId = d.DocumentTypeId + "";
                form.DocumentId = d.DocumentId;
                try
                {
                    form.Expires = d.Expires.ToString();
                }
                catch { }
            }
            return View(form);
        }
        public bool? isDocumentExpire(long preQualification, long docTypeId)
        {
            var currentPreQualification = entityDB.Prequalification.Find(preQualification);
            try
            {
                return entityDB.ClientTemplateReportingData.FirstOrDefault(rec => rec.ReportingType == 2 && rec.ReportingTypeId == docTypeId && rec.ClientTemplateId == currentPreQualification.ClientTemplateId).DocumentExpires;
            }
            catch { }
            return false;
        }
        [HttpPost]
        [SessionExpireForView]
        public ActionResult SupportingDocument(HttpPostedFileBase file, LocalSupportedDocument form)
        {
            if (form.Mode != null && form.Mode == 1)
                ModelState["file"].Errors.Clear();
            ViewBag.DocumentTypes = (from DocType in entityDB.DocumentType.ToList().Where(rec => rec.PrequalificationUseOnly == true)
                                     select new SelectListItem
                                     {
                                         Text = DocType.DocumentTypeName,
                                         Value = DocType.DocumentTypeId + ""
                                     });

            //if (ModelState.IsValid)
            {
                if (form.Mode != null && form.Mode == 1)//If it is edit mode
                {
                }
                else if (!Path.GetExtension(file.FileName).ToLower().Equals(".pdf"))
                {
                    ModelState.AddModelError("", "Invalid File format,Please select PDF ");
                    return View(form);
                }
                //sumanth on 10/26/2015 for FVOS-86 the exipiring date must be greater than current date.
                if (form.Expires != null)
                {
                    DateTime docExpires = Convert.ToDateTime(form.Expires);
                    if (docExpires < DateTime.Now)
                    {
                        ModelState.AddModelError("", "Document expiration date must not be less than current date.");
                        return View(form);
                    }
                    //sumanth ends on 10/26/2015.
                }
                Document doc = new Document();

                //if (form.DocumentId != null)//If it is edit or replace mode
                //    doc = entityDB.Document.Find(form.DocumentId);

                //Kiran on 8/12/2014
                if (form.DocumentId != null)//If it is edit or replace mode
                {
                    doc = entityDB.Document.Find(form.DocumentId);
                    var documentNotifications = entityDB.OrganizationsNotificationsEmailLogs.Where(rec => rec.DocumentId == form.DocumentId).ToList();
                    if (documentNotifications != null)
                    {
                        foreach (var notification in documentNotifications)
                        {
                            notification.RecordStatus = "Archived";
                            entityDB.Entry(notification).State = EntityState.Modified;
                            entityDB.SaveChanges();
                        }
                    }
                    if (doc.DocumentTypeId != Convert.ToInt64(form.DocumentTypeId))
                    {
                        var templateSectionId = entityDB.TemplateSubSections.FirstOrDefault(rec => rec.SubSectionID == form.subSectionId).TemplateSectionID;
                        var prequalCompletedSectionRecord = entityDB.PrequalificationCompletedSections.FirstOrDefault(rec => rec.TemplateSectionId == templateSectionId && rec.PrequalificationId == form.PrequalificationId);
                        if (prequalCompletedSectionRecord != null)
                        {
                            entityDB.PrequalificationCompletedSections.Remove(prequalCompletedSectionRecord);
                            entityDB.SaveChanges();
                        }
                    }
                }
                // Ends<<<
                var vendorId = entityDB.Prequalification.Find(form.PrequalificationId).VendorId;
                doc.ShowOnDashboard = true;// for SMI-226 to display document on dashboard when the document is updated
                doc.OrganizationId = vendorId;//Rajesh (long)Session["currentOrgId"];
                doc.ReferenceRecordID = form.PrequalificationId;
                doc.ReferenceType = "PreQualification";
                // Kiran on 8/25/2014
                if (form.DocumentId == null)
                {
                    doc.Uploaded = DateTime.Now;
                    doc.SystemUserAsUploader = (Guid)Session["UserId"];
                }
                //Ends<<<
                //doc.Uploaded = DateTime.Now;
                if (form.Expires != null)
                    doc.Expires = Convert.ToDateTime(form.Expires);
                else
                    doc.Expires = null;
                //doc.SystemUserAsUploader = (Guid)Session["UserId"];
                doc.DocumentTypeId = Convert.ToInt64(form.DocumentTypeId);

                // Kiran on 8/25/2014
                doc.UpdatedOn = DateTime.Now;
                doc.UpdatedBy = (Guid)Session["UserId"];
                // Ends<<<

                if (form.Mode != null && form.Mode == 1) ;//means Edit Mode
                else
                    if (file != null && file.ContentLength > 0)
                    {
                        var extension = Path.GetExtension(file.FileName);
                        doc.DocumentName = Path.GetFileName(file.FileName); // Siva on 02/16/2015
                    }
                if (form.DocumentId == null)
                {
                    entityDB.Document.Add(doc);
                }
                else
                {
                    entityDB.Entry(doc).State = EntityState.Modified;
                }
                entityDB.SaveChanges();
                if (form.Mode != null && form.Mode == 1) ;
                else
                {
                    if (file != null && file.ContentLength > 0)
                    {
                        //SAVE file
                        var fileName = Path.GetFileName(file.FileName);
                        var extension = Path.GetExtension(file.FileName);

                        Directory.CreateDirectory(Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["ClientUploadsPath"])));
                        var path = Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["ClientUploadsPath"]), fileName + extension);
                        file.SaveAs(path);

                        string uniqueId = doc.DocumentId.ToString();
                        string oldFilePath = Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["ClientUploadsPath"]), fileName + extension);
                        string newFilePath = Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["ClientUploadsPath"]), uniqueId + extension);
                        System.IO.File.Delete(newFilePath);
                        System.IO.File.Move(oldFilePath, newFilePath);
                    }
                    else
                    {
                        ModelState.AddModelError("file", "*");
                    }
                }
                return Redirect("PopUpCloseView?subSectionId=" + form.subSectionId);
            }
            return View(form);
        }

        //Dev:RP DT:7/24/2013
        //Desc:-To Delete Document
        [HttpPost]
        [SessionExpire]
        public ActionResult RemoveDocument(Guid documentId)
        {
            Document document = entityDB.Document.Find(documentId);
            // Kiran on 01/28/2015
            try
            {
                if (document != null)
                {
                    var dotIndex = document.DocumentName.LastIndexOf('.');
                    var extension = document.DocumentName.Substring(dotIndex);
                    var path = Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["ClientUploadsPath"]), document.DocumentId + extension);
                    if (path != null)
                    {
                        System.IO.File.Delete(path);
                        entityDB.Document.Remove(document);
                        entityDB.SaveChanges();
                    }
                }
            }
            catch
            {
                document.DocumentStatus = "Deleted";
                entityDB.Entry(document).State = EntityState.Modified;
                entityDB.SaveChanges();
            }
            // Ends<<<
            return Json("Ok");
        }
        //<<< RemoveDocument Ends

        //[HttpGet]
        //[SessionExpireForView]
        //public ActionResult PrequalificationSites(long preQualificationSiteId, long subSectionId, long preQualificationId)
        //{
        //    LocalAddPrequalificationSite locPreQualificationSite = new LocalAddPrequalificationSite();
        //    PrequalificationSites preSite;

        //    long clientId = entityDB.Prequalification.Find(preQualificationId).ClientId;

        //    ViewBag.BussinessUnits = from busUnits in entityDB.ClientBusinessUnits.Where(rec => rec.ClientId == clientId).ToList()
        //                             select new SelectListItem
        //                             {
        //                                 Text = busUnits.BusinessUnitName,
        //                                 Value = busUnits.ClientBusinessUnitId.ToString(),
        //                             };

        //    if (preQualificationSiteId == -1)
        //    {
        //        preSite = new PrequalificationSites();
        //        locPreQualificationSite.siteId = -1;
        //    }
        //    else
        //    {
        //        preSite = entityDB.PrequalificationSites.Find(preQualificationSiteId);
        //        //locPreQualificationSite.BusinessUnitLocation = preSite.ClientBULocation;
        //        locPreQualificationSite.BusinessUnit = (long)preSite.ClientBusinessUnitId;
        //        locPreQualificationSite.ClientBusinessUnitSite = (long)preSite.ClientBusinessUnitSiteId;
        //        locPreQualificationSite.siteId = preSite.PrequalificationSitesId;
        //        locPreQualificationSite.Comments = preSite.Comment;


        //        var busUnit = entityDB.ClientBusinessUnits.FirstOrDefault(record => record.ClientBusinessUnitId == preSite.ClientBusinessUnitId);

        //        locPreQualificationSite.BusinessUnitSites = (from busUnitSites in busUnit.ClientBusinessUnitSites.ToList()
        //                                                     select new SelectListItem
        //                                                     {
        //                                                         Text = busUnitSites.SiteName,
        //                                                         Value = busUnitSites.ClientBusinessUnitSiteId.ToString(),
        //                                                         Selected = (locPreQualificationSite.ClientBusinessUnitSite == busUnitSites.ClientBusinessUnitSiteId),
        //                                                     }).ToList();

        //    }
        //    locPreQualificationSite.subSectionId = subSectionId;
        //    locPreQualificationSite.preQualificationId = preQualificationId;
        //    ViewBag.preQualificationId = preQualificationId;
        //    return View(locPreQualificationSite);
        //}


        //[HttpPost]
        //[SessionExpireForView]
        //public ActionResult PrequalificationSites(LocalAddPrequalificationSite locQualificationSite)
        //{
        //    //To Check prequalificationsite exist with locQualificationSite Details
        //    long busUnitId = Convert.ToInt64(locQualificationSite.BusinessUnit);
        //    long busUnitSiteId = Convert.ToInt64(locQualificationSite.ClientBusinessUnitSite);

        //    long clientId = entityDB.Prequalification.Find(locQualificationSite.preQualificationId).ClientId;
        //    // Kiran on 3/19/2014

        //    ViewBag.preQualificationId = locQualificationSite.preQualificationId;
        //    // Ver 2.0 <---
        //    var selectedPrequalunitOrSite = entityDB.PrequalificationSites.FirstOrDefault(rec => rec.ClientBusinessUnitId == locQualificationSite.BusinessUnit && rec.ClientBusinessUnitSiteId == locQualificationSite.ClientBusinessUnitSite && rec.PrequalificationSitesId != locQualificationSite.siteId && rec.PrequalificationId == locQualificationSite.preQualificationId);
        //    long selectedBusUnitId = Convert.ToInt64(locQualificationSite.BusinessUnit);
        //    long selectedBusUnitSitedId = Convert.ToInt64(locQualificationSite.ClientBusinessUnitSite);

        //    ViewBag.BussinessUnits = from busUnits in entityDB.ClientBusinessUnits.Where(rec => rec.ClientId == clientId).ToList()
        //                             select new SelectListItem
        //                             {
        //                                 Text = busUnits.BusinessUnitName,
        //                                 Value = busUnits.ClientBusinessUnitId.ToString(),
        //                             };

        //    if (selectedPrequalunitOrSite != null)
        //    {
        //        var busUnit = entityDB.ClientBusinessUnits.FirstOrDefault(record => record.ClientBusinessUnitId == busUnitId);

        //        var curretUserOrgBusUnitSites = from busUnitSites in busUnit.ClientBusinessUnitSites.ToList()
        //                                        select new SelectListItem
        //                                        {
        //                                            Text = busUnitSites.SiteName,
        //                                            Value = busUnitSites.ClientBusinessUnitSiteId.ToString(),
        //                                        };

        //        ViewBag.BussinessUnitSites = curretUserOrgBusUnitSites;
        //        //ModelState.AddModelError("", @Resources.Resources.PreQualificationSite_Error_siteExist);
        //        ViewBag.VBBuexists = true;
        //        return View(locQualificationSite);
        //    }
        //    // Ends<<<

        //    //if (ModelState.IsValid) // Kiran on 3/13/2014
        //    //{
        //    PrequalificationSites preSite;
        //    if (locQualificationSite.siteId == -1)//New Site
        //        preSite = new PrequalificationSites();
        //    else//Existing Site Need to Update
        //        preSite = entityDB.PrequalificationSites.Find(locQualificationSite.siteId);
        //    //preSite.ClientBULocation = locQualificationSite.BusinessUnitLocation;
        //    preSite.ClientBusinessUnitId = Convert.ToInt64(locQualificationSite.BusinessUnit);
        //    preSite.ClientBusinessUnitSiteId = Convert.ToInt64(locQualificationSite.ClientBusinessUnitSite);
        //    preSite.PrequalificationId = locQualificationSite.preQualificationId;
        //    preSite.Comment = locQualificationSite.Comments;

        //    //Rajesh on 09/09/2013

        //    var preQualification = entityDB.Prequalification.Find(locQualificationSite.preQualificationId);
        //    preSite.ClientId = preQualification.ClientId;
        //    preSite.VendorId = preQualification.VendorId;
        //    //Ends<<<

        //    if (locQualificationSite.siteId == -1)
        //        entityDB.PrequalificationSites.Add(preSite);
        //    else
        //        entityDB.Entry(preSite).State = EntityState.Modified;
        //    entityDB.SaveChanges();
        //    return Redirect("PopUpCloseView?subSectionId=" + locQualificationSite.subSectionId);

        //    //}           

        //    //return View(locQualificationSite);
        //}

        // Developed by Sandhya on 04/08/2015
        [HttpGet]
        [SessionExpireForView]
        public ActionResult PrequalificationSites(long preQualificationSiteId, long subSectionId, long preQualificationId)
        {
            LocalAddPrequalificationSite locPreQualificationSite = new LocalAddPrequalificationSite();
            PrequalificationSites preSite;

            @ViewBag.PrequalificationsiteId = preQualificationSiteId;
            long clientId = entityDB.Prequalification.Find(preQualificationId).ClientId;

            ViewBag.BussinessUnits = from busUnits in entityDB.ClientBusinessUnits.Where(rec => rec.ClientId == clientId).OrderBy(rec => rec.BusinessUnitName).ToList()
                                     select new SelectListItem
                                     {
                                         Text = busUnits.BusinessUnitName,
                                         Value = busUnits.ClientBusinessUnitId.ToString(),
                                     };

            if (preQualificationSiteId == -1)
            {
                preSite = new PrequalificationSites();
                locPreQualificationSite.siteId = -1;
            }
            else
            {
                preSite = entityDB.PrequalificationSites.Find(preQualificationSiteId);
                //locPreQualificationSite.BusinessUnitLocation = preSite.ClientBULocation;
                locPreQualificationSite.BusinessUnit = (long)preSite.ClientBusinessUnitId;
                locPreQualificationSite.ClientBusinessUnitSite = (long)preSite.ClientBusinessUnitSiteId;
                locPreQualificationSite.siteId = preSite.PrequalificationSitesId;
                locPreQualificationSite.Comments = preSite.Comment;


                var busUnit = entityDB.ClientBusinessUnits.FirstOrDefault(record => record.ClientBusinessUnitId == preSite.ClientBusinessUnitId);

                locPreQualificationSite.BusinessUnitSites = (from busUnitSites in busUnit.ClientBusinessUnitSites.ToList()
                                                             select new SelectListItem
                                                             {
                                                                 Text = busUnitSites.SiteName,
                                                                 Value = busUnitSites.ClientBusinessUnitSiteId.ToString(),
                                                                 Selected = (locPreQualificationSite.ClientBusinessUnitSite == busUnitSites.ClientBusinessUnitSiteId),
                                                             }).ToList();

            }
            // var selectedlist = entityDB.PrequalificationSites.Where(rec => rec.PrequalificationId == locPreQualificationSite.preQualificationId);
            var selecbool = entityDB.PrequalificationSites.Where(rec => rec.PrequalificationSitesId == locPreQualificationSite.siteId);
            locPreQualificationSite.subSectionId = subSectionId;
            locPreQualificationSite.preQualificationId = preQualificationId;
            ViewBag.preQualificationId = preQualificationId;
            return View(locPreQualificationSite);
        }

        [HttpPost]
        [SessionExpireForView]
        public ActionResult PrequalificationSites(LocalAddPrequalificationSite locQualificationSite)
        {
            ViewBag.Vslist = locQualificationSite.ClientBusinessUnitSites.Count();
            //To Check prequalificationsite exist with locQualificationSite Details
            long busUnitId = Convert.ToInt64(locQualificationSite.BusinessUnit);
            long busUnitSiteId = Convert.ToInt64(locQualificationSite.ClientBusinessUnitSite);

            long clientId = entityDB.Prequalification.Find(locQualificationSite.preQualificationId).ClientId;

            ViewBag.preQualificationId = locQualificationSite.preQualificationId;
            // Ver 2.0 <---
            var selectedPrequalunitOrSite = entityDB.PrequalificationSites.FirstOrDefault(rec => rec.ClientBusinessUnitId == locQualificationSite.BusinessUnit && rec.ClientBusinessUnitSiteId == locQualificationSite.ClientBusinessUnitSite && rec.PrequalificationSitesId != locQualificationSite.siteId && rec.PrequalificationId == locQualificationSite.preQualificationId);
            long selectedBusUnitId = Convert.ToInt64(locQualificationSite.BusinessUnit);
            long selectedBusUnitSitedId = Convert.ToInt64(locQualificationSite.ClientBusinessUnitSite);

            ViewBag.BussinessUnits = from busUnits in entityDB.ClientBusinessUnits.Where(rec => rec.ClientId == clientId).ToList()
                                     select new SelectListItem
                                     {
                                         Text = busUnits.BusinessUnitName,
                                         Value = busUnits.ClientBusinessUnitId.ToString(),
                                     };

            if (selectedPrequalunitOrSite != null)
            {
                var busUnit = entityDB.ClientBusinessUnits.FirstOrDefault(record => record.ClientBusinessUnitId == busUnitId);

                var curretUserOrgBusUnitSites = from busUnitSites in busUnit.ClientBusinessUnitSites.ToList()
                                                select new SelectListItem
                                                {
                                                    Text = busUnitSites.SiteName,
                                                    Value = busUnitSites.ClientBusinessUnitSiteId.ToString(),
                                                };

                ViewBag.BussinessUnitSites = curretUserOrgBusUnitSites;
                //ModelState.AddModelError("", @Resources.Resources.PreQualificationSite_Error_siteExist);
                ViewBag.VBBuexists = true;
                return View(locQualificationSite);
            }

            foreach (var prequalificationSite in locQualificationSite.ClientBusinessUnitSites)
            {
                var prequalSite = entityDB.PrequalificationSites.FirstOrDefault(rec => rec.PrequalificationId == locQualificationSite.preQualificationId && rec.ClientBusinessUnitSiteId == prequalificationSite.BusinessUnitSiteId && rec.ClientBusinessUnitId == locQualificationSite.BusinessUnit);
                if (prequalSite == null)
                {
                    PrequalificationSites preSite = new PrequalificationSites();
                    if (prequalificationSite.isChecked && prequalificationSite.BusinessUnitSiteId != 999)
                    {
                        preSite.ClientBusinessUnitSiteId = Convert.ToInt64(prequalificationSite.BusinessUnitSiteId);

                        preSite.ClientBusinessUnitId = Convert.ToInt64(locQualificationSite.BusinessUnit);
                        preSite.PrequalificationId = locQualificationSite.preQualificationId;
                        preSite.Comment = locQualificationSite.Comments;

                        var preQualification = entityDB.Prequalification.Find(locQualificationSite.preQualificationId);
                        preSite.ClientId = preQualification.ClientId;
                        preSite.VendorId = preQualification.VendorId;
                        preSite.PrequalificationStatusId = preQualification.PrequalificationStatusId;

                        entityDB.PrequalificationSites.Add(preSite);

                        entityDB.SaveChanges();
                    }
                }
            }
            // Siva 20th Feb
            var locationsSec = entityDB.PrequalificationCompletedSections.FirstOrDefault(rec => rec.PrequalificationId == locQualificationSite.preQualificationId && rec.TemplateSections.TemplateSectionType == 7);
            if (locationsSec != null)
                entityDB.PrequalificationCompletedSections.Remove(locationsSec);
            // Siva 20th Feb

            entityDB.SaveChanges();
            return Redirect("PopUpCloseView?subSectionId=" + locQualificationSite.subSectionId);

            //}           

            //return View(locQualificationSite);
        }

        // Ends<<<

        [HttpGet]
        [SessionExpireForView]
        public ActionResult EditPrequalificationSite(long preQualificationSiteId, long subSectionId, long preQualificationId)
        {
            LocalAddPrequalificationSite locPreQualificationSite = new LocalAddPrequalificationSite();
            PrequalificationSites preSite;

            long clientId = entityDB.Prequalification.Find(preQualificationId).ClientId;

            ViewBag.BussinessUnits = from busUnits in entityDB.ClientBusinessUnits.Where(rec => rec.ClientId == clientId).ToList()
                                     select new SelectListItem
                                     {
                                         Text = busUnits.BusinessUnitName,
                                         Value = busUnits.ClientBusinessUnitId.ToString(),
                                     };

            if (preQualificationSiteId == -1)
            {
                preSite = new PrequalificationSites();
                locPreQualificationSite.siteId = -1;
            }
            else
            {
                preSite = entityDB.PrequalificationSites.Find(preQualificationSiteId);
                //locPreQualificationSite.BusinessUnitLocation = preSite.ClientBULocation;
                locPreQualificationSite.BusinessUnit = (long)preSite.ClientBusinessUnitId;
                locPreQualificationSite.ClientBusinessUnitSite = (long)preSite.ClientBusinessUnitSiteId;
                locPreQualificationSite.siteId = preSite.PrequalificationSitesId;
                locPreQualificationSite.Comments = preSite.Comment;


                var busUnit = entityDB.ClientBusinessUnits.FirstOrDefault(record => record.ClientBusinessUnitId == preSite.ClientBusinessUnitId);

                locPreQualificationSite.BusinessUnitSites = (from busUnitSites in busUnit.ClientBusinessUnitSites.ToList()
                                                             select new SelectListItem
                                                             {
                                                                 Text = busUnitSites.SiteName,
                                                                 Value = busUnitSites.ClientBusinessUnitSiteId.ToString(),
                                                                 Selected = (locPreQualificationSite.ClientBusinessUnitSite == busUnitSites.ClientBusinessUnitSiteId),
                                                             }).ToList();

            }
            locPreQualificationSite.subSectionId = subSectionId;
            locPreQualificationSite.preQualificationId = preQualificationId;
            ViewBag.preQualificationId = preQualificationId;
            return View(locPreQualificationSite);
        }

        [HttpPost]
        [SessionExpireForView]
        public ActionResult EditPrequalificationSite(LocalAddPrequalificationSite locQualificationSite)
        {
            //To Check prequalificationsite exist with locQualificationSite Details
            long busUnitId = Convert.ToInt64(locQualificationSite.BusinessUnit);
            long busUnitSiteId = Convert.ToInt64(locQualificationSite.ClientBusinessUnitSite);

            long clientId = entityDB.Prequalification.Find(locQualificationSite.preQualificationId).ClientId;
            // Kiran on 3/19/2014

            ViewBag.preQualificationId = locQualificationSite.preQualificationId;
            // Ver 2.0 <---
            var selectedPrequalunitOrSite = entityDB.PrequalificationSites.FirstOrDefault(rec => rec.ClientBusinessUnitId == locQualificationSite.BusinessUnit && rec.ClientBusinessUnitSiteId == locQualificationSite.ClientBusinessUnitSite && rec.PrequalificationSitesId != locQualificationSite.siteId && rec.PrequalificationId == locQualificationSite.preQualificationId);
            long selectedBusUnitId = Convert.ToInt64(locQualificationSite.BusinessUnit);
            long selectedBusUnitSitedId = Convert.ToInt64(locQualificationSite.ClientBusinessUnitSite);

            ViewBag.BussinessUnits = from busUnits in entityDB.ClientBusinessUnits.Where(rec => rec.ClientId == clientId).ToList()
                                     select new SelectListItem
                                     {
                                         Text = busUnits.BusinessUnitName,
                                         Value = busUnits.ClientBusinessUnitId.ToString(),
                                     };

            if (selectedPrequalunitOrSite != null)
            {
                var busUnit = entityDB.ClientBusinessUnits.FirstOrDefault(record => record.ClientBusinessUnitId == busUnitId);

                var curretUserOrgBusUnitSites = from busUnitSites in busUnit.ClientBusinessUnitSites.ToList()
                                                select new SelectListItem
                                                {
                                                    Text = busUnitSites.SiteName,
                                                    Value = busUnitSites.ClientBusinessUnitSiteId.ToString(),
                                                };

                ViewBag.BussinessUnitSites = curretUserOrgBusUnitSites;
                //ModelState.AddModelError("", @Resources.Resources.PreQualificationSite_Error_siteExist);
                ViewBag.VBBuexists = true;
                return View(locQualificationSite);
            }
            // Ends<<<

            //if (ModelState.IsValid) // Kiran on 3/13/2014
            //{
            PrequalificationSites preSite;
            if (locQualificationSite.siteId == -1)//New Site
                preSite = new PrequalificationSites();
            else//Existing Site Need to Update
                preSite = entityDB.PrequalificationSites.Find(locQualificationSite.siteId);
            //preSite.ClientBULocation = locQualificationSite.BusinessUnitLocation;
            preSite.ClientBusinessUnitId = Convert.ToInt64(locQualificationSite.BusinessUnit);
            preSite.ClientBusinessUnitSiteId = Convert.ToInt64(locQualificationSite.ClientBusinessUnitSite);
            preSite.PrequalificationId = locQualificationSite.preQualificationId;
            preSite.Comment = locQualificationSite.Comments;

            //Rajesh on 09/09/2013

            var preQualification = entityDB.Prequalification.Find(locQualificationSite.preQualificationId);
            preSite.ClientId = preQualification.ClientId;
            preSite.VendorId = preQualification.VendorId;
            //Ends<<<

            if (locQualificationSite.siteId == -1)
                entityDB.PrequalificationSites.Add(preSite);
            else
                entityDB.Entry(preSite).State = EntityState.Modified;
            entityDB.SaveChanges();
            return Redirect("PopUpCloseView?subSectionId=" + locQualificationSite.subSectionId);
        }

        //Dev:RP DT:7/24/2013
        //Desc:-To get dynamic content of a sub section
        [OutputCache(Duration = 0)]
        public ActionResult GetDynamicSubSectionContent(long subSectionId, long preQualificationID, long? mode)
        {
            ViewBag.mode = mode;
            ViewBag.subSectionId = subSectionId;
            ViewBag.preQualificationID = preQualificationID;
            TemplateSubSections subsection = entityDB.TemplateSubSections.Find(subSectionId);
            var currentPrequalification = entityDB.Prequalification.Find(preQualificationID); // Kiran on 01/31/2015
            if (subsection.SubSectionTypeCondition.Equals("SitePrequalification"))
            {
                var sitesInfo = entityDB.PrequalificationSites.Where(rec => rec.PrequalificationId == preQualificationID).GroupBy(rec => rec.ClientBusinessUnitId).ToList();
                ViewBag.siteInfo = sitesInfo;
                ViewBag.Documents = entityDB.Document.Where(rec => rec.ReferenceType == "Business Unit").ToList();
                ViewBag.PrequalificationStatus = entityDB.Prequalification.FirstOrDefault(rec => rec.PrequalificationId == preQualificationID).PrequalificationStatusId; // Kiran on 3/12/2014
                return PartialView("PrequalificationSitesPartialView");
            }

            if (subsection.SubSectionTypeCondition.Equals("SupportingDocument"))
            {
                Guid userId = new Guid(Session["UserId"].ToString());
                ViewBag.RestrictedAccess = FVGen3.BusinessLogic.RolesHelper.IsUserInRole(userId, 1000, "read", currentPrequalification.ClientId, currentPrequalification.VendorId) || Session["RoleName"].Equals("Super Admin");
                //|| Session["RoleName"].Equals("Vendor")


                var documentTypeIds = entityDB.ClientTemplateReportingData.Where(rec => rec.ReportingType == 2 && rec.ClientTemplateId == currentPrequalification.ClientTemplateId).Select(rec => rec.ReportingTypeId).ToList();

                if (Session["RoleName"].Equals("Admin") || Session["RoleName"].Equals("Vendor"))
                {
                    ViewBag.Documents = entityDB.Document.Where(rec => rec.ReferenceType == "PreQualification" && rec.ReferenceRecordID == preQualificationID && (rec.DocumentStatus != "Deleted" || rec.DocumentStatus == null) && documentTypeIds.Contains(rec.DocumentTypeId)).GroupBy(rec => rec.DocumentTypeId).ToList(); // Kiran on 01/28/2015 for displaying only approved/rejected documents // Kiran on 01/31/2015 to display the documents that are applicable for this client template.
                }
                else
                {
                    ViewBag.Documents = entityDB.Document.Where(rec => rec.ReferenceType == "PreQualification" && rec.ReferenceRecordID == preQualificationID && rec.DocumentStatus == "Approved" && documentTypeIds.Contains(rec.DocumentTypeId)).GroupBy(rec => rec.DocumentTypeId).ToList(); // Kiran on 01/31/2015 to display the documents that are applicable for this client template.
                }
                return PartialView("SupportedDocumentsPartialView");
            }
            //Rajesh on 1/7/2014
            if (subsection.SubSectionTypeCondition.Equals("PrequalComments"))
            {
                ViewBag.Comments = entityDB.PrequalificationComments.Where(rec => rec.PrequalificationId == preQualificationID && rec.SubSectionID == subSectionId).ToList();
                return PartialView("CommentsPartialView");
            }
            //<<<Ends
            return Json("");
        }

        [OutputCache(Duration = 0)]
        public ActionResult PrequalificationSitesPartialView()
        {
            return PartialView();
        }

        [OutputCache(Duration = 0)]
        public ActionResult SupportedDocumentsPartialView()
        {
            return PartialView();
        }

        [OutputCache(Duration = 0)]
        public ActionResult CommentsPartialView()
        {
            return PartialView();
        }

        //Dev:RP DT:7/24/2013
        //Desc:-To Close Dynamic popup it may be PrequalificationSites or any..
        public ActionResult PopUpCloseView(long subSectionId)
        {
            ViewBag.subSectionId = subSectionId;
            return View();
        }

        //DV:RP DT:6/21/2013   10-02-2013
        //Purpose:  To submit form(TemplateSection)
        [HttpPost]
        //Type >>> null or 0= Save And Continue 1-Only Save
        // Code added on 7/1/2014 for Client Signature, it works only when the section is created using the template tool
        // Does not work in copy template, it has to be recreated.
        public String SubmitFormData(String fieldsWithValues, long SectionId, long PreQualificationId, int? type, decimal? paymentAmt)//This filedsWithValues contains QuestionColumnId ^ value ^ Insertion type(Insertion table)|QuestionColumnId ^ value Insertion type(Insertion table) etc..
        {
            cntr = 0;
            if (Session["UserId"] == null)
            {
                return "Session Has Expired";
            }
            String result = "Success";

            bool isBiddingDeleted = false;
            var currentPreQualification = entityDB.Prequalification.Find(PreQualificationId);

            // Kiran on 8/5/2014
            if (Session["RoleName"].ToString().Equals("Vendor") || Session["RoleName"].ToString().Equals("Admin"))//If Vendor Login
            {
                if (entityDB.TemplateSections.Find(SectionId).TemplateSectionType == 4)
                {
                    var templateId = entityDB.TemplateSections.Find(SectionId).Templates.TemplateID;
                    var mandatoryDocumentsOfCurrentTemplate = entityDB.ClientTemplates.FirstOrDefault(record => record.TemplateID == templateId).ClientTemplateReportingData.Where(record => record.DocIsMandatory == true && record.ReportingType == 2);
                    var vendorUploadedDocumentIds = entityDB.Document.Where(documents => documents.ReferenceType == "PreQualification" && documents.ReferenceRecordID == currentPreQualification.PrequalificationId && (documents.DocumentStatus != "Deleted" || documents.DocumentStatus == null)).Select(rec => rec.DocumentTypeId).ToList();

                    var errorString = "";

                    foreach (var mandatoryDocument in mandatoryDocumentsOfCurrentTemplate)
                    {
                        if (vendorUploadedDocumentIds.Contains(mandatoryDocument.ReportingTypeId))
                        {
                            continue;
                        }
                        else
                        {
                            var mandatoryDocName = entityDB.DocumentType.FirstOrDefault(record => record.DocumentTypeId == mandatoryDocument.ReportingTypeId).DocumentTypeName;
                            errorString = errorString + "\n " + mandatoryDocName;
                        }
                    }
                    if (errorString != "")
                    {
                        return "Following documents have to be uploaded to proceed further:" + "\n" + errorString;
                    }
                }

                // Kiran on 12/6/2014
                var prequalificationSites = entityDB.PrequalificationSites.Where(rec => rec.PrequalificationId == PreQualificationId).ToList();
                if (entityDB.TemplateSections.Find(SectionId).TemplateSectionType == 7)
                {
                    if (prequalificationSites == null || prequalificationSites.Count() == 0)
                        return "Please add a business unit location";
                    // Mani on 8/19/2015
                    else
                    {
                        //update paymentid for after added sites
                        var paidBuIds = entityDB.PrequalificationSites.Where(rec => rec.PrequalificationId == PreQualificationId && rec.PrequalificationPaymentsId != null).Select(rec => rec.ClientBusinessUnitId).Distinct().ToList();
                        var addedSites = entityDB.PrequalificationSites.Where(rec => rec.PrequalificationId == PreQualificationId && (rec.PrequalificationPaymentsId == null || rec.PrequalificationPayments.PaymentCategory == 0)).ToList();
                        foreach (var site in addedSites)
                        {
                            if (paidBuIds.Contains(site.ClientBusinessUnitId))
                            {
                                var paymentid = entityDB.PrequalificationSites.FirstOrDefault(rec => rec.ClientBusinessUnitId == site.ClientBusinessUnitId && rec.PrequalificationId == PreQualificationId && rec.PrequalificationPaymentsId != null).PrequalificationPaymentsId;
                                site.PrequalificationPaymentsId = paymentid;
                                entityDB.Entry(site).State = EntityState.Modified;
                                entityDB.SaveChanges();
                            }
                        }
                        //Ends>>>
                        var sitesToPay = entityDB.PrequalificationSites.Where(rec => rec.PrequalificationId == PreQualificationId && (rec.PrequalificationPaymentsId == null || rec.PrequalificationPayments.PaymentCategory == 0)).ToList();
                        List<ClientTemplatesBUGroupPrice> pricesToBePaid = new List<ClientTemplatesBUGroupPrice>();
                        //Kiran on 24th Feb 2015
                        var bunitIds = prequalificationSites.Select(rec => rec.ClientBusinessUnitId).Distinct().ToList();

                        //var templatesForBU = entityDB.ClientTemplatesForBU.Where(rec => bunitIds.Contains(rec.ClientBusinessUnitId) && rec.ClientTemplateId == currentPreQualification.ClientTemplateId && rec.ClientTemplatesBUGroupPrice.GroupingFor == 0).Select(rec => rec.ClientTemplateBUGroupId).ToList(); // Kiran on 06/16/2015
                        if (sitesToPay.Count() != 0)
                        {
                            var buIds = sitesToPay.Select(rec => rec.ClientBusinessUnitId).Distinct().ToList();
                            var templatesForBuToPay = entityDB.ClientTemplatesForBU.Where(rec => buIds.Contains(rec.ClientBusinessUnitId) && rec.ClientTemplateId == currentPreQualification.ClientTemplateId && rec.ClientTemplatesBUGroupPrice.GroupingFor == 0 && rec.ClientTemplatesBUGroupPrice.GroupPrice != 0).Select(rec => rec.ClientTemplateBUGroupId).ToList(); // Kiran on 06/16/2015 // sumanth on 10/19/2015 for FVOS-92
                            pricesToBePaid = entityDB.ClientTemplatesBUGroupPrice.Where(rec => templatesForBuToPay.Contains(rec.ClientTemplateBUGroupId) && rec.GroupingFor == 0 && rec.GroupPriceType > 0).ToList();
                        }
                        // for annual payment
                        //var preqsitesBus = entityDB.PrequalificationSites.Where(rec => rec.PrequalificationId == PreQualificationId).Select(rec => rec.ClientBusinessUnitId).ToList();
                        var annualFeeGroups = entityDB.PrequalificationTrainingAnnualFees.Where(rec => rec.PrequalificationId == PreQualificationId).Select(rec => rec.ClientTemplateBUGroupId).ToList();
                        var clientTemplatesForBu = entityDB.ClientTemplatesForBU.Where(rec => rec.ClientTemplateId == currentPreQualification.ClientTemplateId && bunitIds.Contains(rec.ClientBusinessUnitId) && rec.ClientTemplatesBUGroupPrice.GroupingFor == 1).ToList();
                        var groupIds = clientTemplatesForBu.Select(rec => rec.ClientTemplateBUGroupId).Distinct().ToList();
                        var templateBuId = clientTemplatesForBu.Select(rec => rec.ClientTemplateForBUId).Distinct().ToList();
                        var annualBuPricingGroups = entityDB.ClientTemplatesBUGroupPrice.Where(rec => rec.ClientTemplateID == currentPreQualification.ClientTemplateId && rec.GroupingFor == 1 && rec.GroupPrice != 0 && groupIds.Contains(rec.ClientTemplateBUGroupId)).ToList();
                        var toPayForAnuual = entityDB.ClientTemplatesBUGroupPrice.Where(rec => rec.ClientTemplateID == currentPreQualification.ClientTemplateId && rec.GroupingFor == 1 && rec.GroupPrice != 0 && groupIds.Contains(rec.ClientTemplateBUGroupId) && !annualFeeGroups.Contains(rec.ClientTemplateBUGroupId)).ToList();

                        // Making payments section as complete or open the section
                        var paymentSection = entityDB.PrequalificationCompletedSections.FirstOrDefault(rec => rec.PrequalificationId == PreQualificationId && rec.TemplateSections.TemplateSectionType == 3);
                        if (pricesToBePaid != null && pricesToBePaid.Count() > 0 || (toPayForAnuual != null && toPayForAnuual.Count() > 0))
                        {
                            if (paymentSection != null)
                            {
                                entityDB.PrequalificationCompletedSections.Remove(paymentSection);
                            }
                        }
                        else if (paymentSection == null && currentPreQualification.PaymentReceived == true)
                        {
                            PrequalificationCompletedSections newPaymentSec = new PrequalificationCompletedSections();
                            newPaymentSec.TemplateSectionId = currentPreQualification.ClientTemplates.Templates.TemplateSections.FirstOrDefault(rec => rec.TemplateSectionType == 3).TemplateSectionID;
                            newPaymentSec.PrequalificationId = PreQualificationId;
                            entityDB.PrequalificationCompletedSections.Add(newPaymentSec);
                            currentPreQualification.PaymentReceived = true;
                        }
                        entityDB.SaveChanges();

                        // after annual payent if he add bu in the paid group insert records as payment completed.
                        foreach (var templateBu in clientTemplatesForBu)
                        {
                            if (entityDB.PrequalificationTrainingAnnualFees.FirstOrDefault(rec => rec.ClientTemplateForBUId == templateBu.ClientTemplateForBUId && rec.PrequalificationId == PreQualificationId) == null)
                            {
                                var preqAnnualFee = entityDB.PrequalificationTrainingAnnualFees.FirstOrDefault(rec => rec.ClientTemplateBUGroupId == templateBu.ClientTemplateBUGroupId && rec.PrequalificationId == PreQualificationId);
                                if (preqAnnualFee != null)
                                {
                                    PrequalificationTrainingAnnualFees annualFeeRec = new PrequalificationTrainingAnnualFees();
                                    annualFeeRec.AnnualFeeAmount = 0;
                                    annualFeeRec.AnnualFeeReceivedAmount = 0;
                                    annualFeeRec.ClientTemplateBUGroupId = preqAnnualFee.ClientTemplateBUGroupId;
                                    annualFeeRec.ClientTemplateForBUId = templateBu.ClientTemplateForBUId;
                                    annualFeeRec.FeeReceivedDate = DateTime.Now;
                                    annualFeeRec.PrequalificationId = preqAnnualFee.PrequalificationId;
                                    annualFeeRec.FeeTransactionId = preqAnnualFee.FeeTransactionId;
                                    annualFeeRec.FeeTransactionMetaData = preqAnnualFee.FeeTransactionMetaData;
                                    annualFeeRec.SkipPayment = preqAnnualFee.SkipPayment;
                                    entityDB.PrequalificationTrainingAnnualFees.Add(annualFeeRec);
                                    entityDB.SaveChanges();
                                }
                            }
                        }
                        // Ends >>>

                        // Kiran on 02/24/2015
                        decimal? totalAmountToPay = 0.0M;
                        //var totalInvoiceAmountToPayForPrequalSites = entityDB.ClientTemplatesBUGroupPrice.Where(rec => templatesForBU.Contains(rec.ClientTemplateBUGroupId) && rec.GroupingFor == 0 && rec.GroupPriceType != 0).ToList();
                        var defaultPriceForSelectedTemplate = entityDB.ClientTemplatesBUGroupPrice.FirstOrDefault(rec => rec.ClientTemplateID == currentPreQualification.ClientTemplateId && rec.GroupPriceType == 0 && rec.GroupingFor == 0).GroupPrice;
                        var prequalSitesBusinessUnitsClientTemplatesForBU = entityDB.ClientTemplatesForBU.Where(rec => bunitIds.Contains(rec.ClientBusinessUnitId) && rec.ClientTemplateId == currentPreQualification.ClientTemplateId && rec.ClientTemplatesBUGroupPrice.GroupingFor == 0 && rec.ClientTemplatesBUGroupPrice.GroupPriceType != 0).ToList();
                        if (prequalSitesBusinessUnitsClientTemplatesForBU != null && prequalSitesBusinessUnitsClientTemplatesForBU.Count() > 0)
                        {

                            foreach (var businessUnitTemplateBU in prequalSitesBusinessUnitsClientTemplatesForBU)
                            {
                                if (businessUnitTemplateBU.ClientTemplatesBUGroupPrice.GroupPriceType > 0)
                                    totalAmountToPay += businessUnitTemplateBU.ClientTemplatesBUGroupPrice.GroupPrice;
                            }
                        }
                        totalAmountToPay += defaultPriceForSelectedTemplate;



                        if (totalAmountToPay != currentPreQualification.TotalInvoiceAmount || annualBuPricingGroups.Count() > 0)
                        {
                            currentPreQualification.TotalInvoiceAmount = totalAmountToPay;
                            currentPreQualification.TotalAnnualFeeAmount = (decimal)annualBuPricingGroups.Sum(rec => rec.GroupPrice);
                            entityDB.Entry(currentPreQualification).State = EntityState.Modified;
                            //var paymentSection = entityDB.PrequalificationCompletedSections.FirstOrDefault(rec => rec.PrequalificationId == PreQualificationId && rec.TemplateSections.TemplateSectionType == 3);
                            //entityDB.PrequalificationCompletedSections.Remove(paymentSection);
                            entityDB.SaveChanges();

                        }
                        // Ends<<<
                    }
                }
                // Ends<<<
            }
            // Ends<<<

            var currentSection = entityDB.TemplateSections.Find(SectionId); // Rajesh on 7/1/2014
            //Rajesh on 12/30/2013
            if (entityDB.TemplateSections.Find(SectionId).TemplateSectionType == 99 && type != 3)
            {
                currentPreQualification.UserIdAsCompleter = new Guid(fieldsWithValues);
                currentPreQualification.UserIdLoggedIn = (Guid)Session["UserId"];
                entityDB.Entry(currentPreQualification).State = EntityState.Modified;

                entityDB.SaveChanges();
            }
            else
            {//Ends<<<
                try
                {
                    List<string> keywordList = new List<string>(); // Sandhya on 04/11/2015
                    bool isFirstInsertion_ForMPA = true; // Rajesh on 7/1/2014

                    var preUserInputs = entityDB.PrequalificationUserInput.Where(rec => rec.PreQualificationId == PreQualificationId).ToList();
                    foreach (var record in fieldsWithValues.Split('|'))
                    {

                        // Insertion into PrequalicationUserInput table >>>
                        string[] questionID_Val = record.Split('^');//0-QuestionColumn Id,1-question Val,2-Insertion Type(Inserted table name),3-QuestionReferenceId
                        if (questionID_Val.Length == 4)
                        {
                            bool isInsert = false;

                            long QuestionColumnId = Convert.ToInt64(questionID_Val[0]);
                            var QuestionColumn = entityDB.QuestionColumnDetails.Find(QuestionColumnId);

                            //sandhya 04/11/2015
                            var reportForKeyword = entityDB.Questions.Find(QuestionColumn.QuestionId).ReportForKeyword;
                            if (!string.IsNullOrEmpty(reportForKeyword) && !keywordList.Contains(reportForKeyword))
                            {
                                keywordList.Add(reportForKeyword);
                            }
                            // Ends<<<

                            PrequalificationUserInput preUserInput_NewRecord;
                            //To get existing record
                            //RAB trying to make this more efficient by reducitn calls
                            //preUserInput_NewRecord = entityDB.PrequalificationUserInput.FirstOrDefault(rec => rec.PreQualificationId == PreQualificationId && rec.QuestionColumnId == QuestionColumnId);
                            preUserInput_NewRecord = preUserInputs.FirstOrDefault(rec => rec.QuestionColumnId == QuestionColumnId);

                            //if no record found
                            if (preUserInput_NewRecord == null)
                            {
                                isInsert = true;
                                preUserInput_NewRecord = new PrequalificationUserInput();
                            }

                            preUserInput_NewRecord.PreQualificationId = PreQualificationId;
                            preUserInput_NewRecord.QuestionColumnId = QuestionColumnId;
                            preUserInput_NewRecord.UserInput = questionID_Val[1].Trim();
                            // Rajesh on 7/1/2014





                            if (isInsert)
                            {

                                if (String.IsNullOrWhiteSpace(questionID_Val[1]))
                                    continue;
                                entityDB.PrequalificationUserInput.Add(preUserInput_NewRecord);
                                // Ends<<<
                            }
                            else
                                entityDB.Entry(preUserInput_NewRecord).State = EntityState.Modified;
                            // Insertion Ends <<<
                            //================================

                            //In addition to user input we are inserting data into related tables viz. YearWiseStats, PrequalificationReportingData etc.
                            if (questionID_Val[2].Equals("YearWiseStatsInsertion"))
                            {
                                if (QuestionColumn.Questions.QuestionBankId == 1000)//If It is EMR Years Related Question
                                {
                                    PrequalificationEMRStatsYears EMRYears = entityDB.PrequalificationEMRStatsYears.FirstOrDefault(rec => rec.PrequalificationId == PreQualificationId && rec.ColumnNo == QuestionColumn.ColumnNo);
                                    if (EMRYears == null)
                                    {
                                        EMRYears = new PrequalificationEMRStatsYears();
                                        EMRYears.PrequalificationId = PreQualificationId;
                                        EMRYears.ColumnNo = QuestionColumn.ColumnNo;
                                        EMRYears.EMRStatsYear = questionID_Val[1].Trim();
                                        entityDB.PrequalificationEMRStatsYears.Add(EMRYears);
                                    }
                                    else
                                    {
                                        EMRYears.EMRStatsYear = questionID_Val[1].Trim();
                                        entityDB.Entry(EMRYears).State = EntityState.Modified;
                                    }
                                    entityDB.SaveChanges();//We are doing this partial save bcz of childs exist
                                }
                                else
                                {
                                    PrequalificationEMRStatsYears EMRYears = entityDB.PrequalificationEMRStatsYears.FirstOrDefault(rec => rec.PrequalificationId == PreQualificationId && rec.ColumnNo == QuestionColumn.ColumnNo);
                                    if (EMRYears == null || QuestionColumn.Questions.NumberOfColumns != 3)
                                    {
                                        //Do Nothing
                                    }
                                    else
                                    {
                                        PrequalificationEMRStatsValues EMRValues = entityDB.PrequalificationEMRStatsValues.FirstOrDefault(rec => rec.QuestionColumnId == QuestionColumn.QuestionColumnId && rec.PrequalEMRStatsYearId == EMRYears.PrequalEMRStatsYearId);
                                        if (EMRValues == null)
                                        {
                                            EMRValues = new PrequalificationEMRStatsValues();
                                            EMRValues.PrequalEMRStatsYearId = EMRYears.PrequalEMRStatsYearId;
                                            EMRValues.QuestionId = QuestionColumn.Questions.QuestionID;
                                            EMRValues.QuestionColumnId = QuestionColumn.QuestionColumnId;
                                            EMRValues.QuestionColumnIdValue = questionID_Val[1].Trim();
                                            entityDB.PrequalificationEMRStatsValues.Add(EMRValues);
                                        }
                                        else
                                        {
                                            EMRValues.QuestionColumnIdValue = questionID_Val[1].Trim();
                                            entityDB.Entry(EMRValues).State = EntityState.Modified;
                                        }
                                    }
                                }
                            }
                            else if (questionID_Val[2].Equals("BiddingManualInsertion") || questionID_Val[2].Equals("BiddingInsertion") || questionID_Val[2].Equals("ProficiencyInsertion") || questionID_Val[2].Equals("ClaycoBiddingInsertion"))
                            {
                                if (!isBiddingDeleted)
                                {
                                    var reportingType = 0;
                                    if (questionID_Val[2].Equals("ProficiencyInsertion"))
                                        reportingType = 1;
                                    if (questionID_Val[2].Equals("ClaycoBiddingInsertion"))
                                        reportingType = 3;
                                    var ReportingDatalist = entityDB.PrequalificationReportingData.Where(rec => rec.PrequalificationId == PreQualificationId && rec.ReportingType == reportingType).ToList();
                                    foreach (var reportData in ReportingDatalist)
                                    {
                                        entityDB.PrequalificationReportingData.Remove(reportData);
                                    }
                                    entityDB.SaveChanges();
                                    isBiddingDeleted = true;
                                }
                                if (String.IsNullOrWhiteSpace(questionID_Val[1]))
                                    continue;
                                else
                                {
                                    foreach (var ids in questionID_Val[1].Split(','))
                                    {
                                        PrequalificationReportingData data = new PrequalificationReportingData();
                                        data.PrequalificationId = PreQualificationId;
                                        data.ClientId = currentPreQualification.ClientId;
                                        data.VendorId = currentPreQualification.VendorId;
                                        if (questionID_Val[2].Equals("ProficiencyInsertion"))
                                            data.ReportingType = 1;
                                        else if (questionID_Val[2].Equals("ClaycoBiddingInsertion"))
                                            data.ReportingType = 3;
                                        else
                                            data.ReportingType = 0;//Bidding intrest

                                        //ids is user input---incase of BiddingManualInsertion we get dropdown value
                                        //in case of BiddingInsertion/Proficiency insertion we get Yes,no,Not applicable
                                        if (questionID_Val[2].Equals("BiddingManualInsertion") || questionID_Val[2].Equals("ClaycoBiddingInsertion"))//It contains only dropdowns
                                        {
                                            long RId = Convert.ToInt64(ids);
                                            if (entityDB.PrequalificationReportingData.FirstOrDefault(rec => rec.PrequalificationId == PreQualificationId && rec.ReportingType == 0 && rec.ReportingDataId == RId) != null)
                                                continue;
                                            data.ReportingDataId = RId;
                                        }
                                        else//It contains only Radio Buttons
                                        {
                                            if (!ids.ToLower().Trim().Equals("yes"))
                                                continue;
                                            data.ReportingDataId = Convert.ToInt64(questionID_Val[3]);
                                        }

                                        entityDB.PrequalificationReportingData.Add(data);
                                    }
                                }
                            }

                            entityDB.SaveChanges();
                        }

                    }
                    var clientTemplateID = entityDB.Prequalification.Where(rec => rec.PrequalificationId == PreQualificationId).FirstOrDefault().ClientTemplateId;
                    var templateID = entityDB.ClientTemplates.Where(rec => rec.ClientTemplateID == clientTemplateID.Value).FirstOrDefault().TemplateID;
                    //Sandhya 04/11/2015---Start
                    foreach (var key in keywordList) // Sandhya on 04/21/2015 Modified while updating section
                    {
                        if (key.ToLower().Equals("vendordetails"))
                        {
                            var GetVendordetails = entityDB.VendorDetails.Where(rec => rec.PrequalificationId == PreQualificationId).ToList();
                            if (GetVendordetails != null)
                            {
                                foreach (var Onerecord in GetVendordetails)
                                {
                                    entityDB.VendorDetails.Remove(Onerecord);
                                    entityDB.SaveChanges();
                                }
                            }
                        }

                    }

                    foreach (var key in keywordList)
                    {
                        //var questionclo = entityDB.QuestionColumnDetails.Where(rec => rec.Questions.ReportForKeyword == key).GroupBy(rec => rec.Questions.GroupAsOneRecord).ToList();
                        //var questionclo = entityDB.QuestionColumnDetails
                        //    .Where(rec => rec.Questions.ReportForKeyword == key 
                        //        && rec.Questions.TemplateSubSections.TemplateSections.TemplateID == templateID
                        //        && rec.PrequalificationUserInputs.Any(pqi => pqi.PreQualificationId == PreQualificationId )).GroupBy(rec => rec.Questions.GroupAsOneRecord).ToList();

                        var helper = new PrequalificationHelper();
                        var questionclo = helper.GetQuestionColumnData(PreQualificationId, key);

                        //var questions = entityDB.Questions.Where(rec => rec.ReportForKeyword == key).ToList();
                        List<string> myCollection = new List<string>();
                        foreach (var questionsrecord in questionclo)
                        {
                            //sumanth on 08/12/2015
                            if (key.ToLower().Equals("vendordetails"))
                            {
                                VendorDetails vendor = new VendorDetails();

                                foreach (var questions in questionsrecord.Questions)
                                {
                                    long prequalificationId = 0;
                                    //var questionColumID = questions.QuestionColumnId;
                                    //var questionText = questions.Questions.QuestionText.Replace(":", "").Replace(" ", "").Trim();
                                    //var userInput = "";
                                    //if (questions.PrequalificationUserInputs != null && questions.PrequalificationUserInputs.Count() > 0)
                                    //{
                                    //    userInput = questions.PrequalificationUserInputs[0].UserInput;
                                    //    prequalificationId = questions.PrequalificationUserInputs[0].PreQualificationId;
                                    //}
                                    var questionText = questions.QuestionText.Replace(":", "").Replace(" ", "").Trim();
                                    var userInput = questions.PrequalificationUserInput;

                                    //if (prequalificationId == PreQualificationId)
                                    //{
                                    if (questionText.Equals("FullName"))
                                        vendor.FullName = userInput;
                                    else if (questionText.Equals("Title"))
                                        vendor.Title = userInput;
                                    else if (questionText.Equals("EmailAddress"))
                                        vendor.EmailAddress = userInput;
                                    else if (questionText.Equals("TelephoneNumber"))
                                        vendor.TelephoneNumber = userInput;
                                    //}
                                    vendor.GroupNumber = Convert.ToInt64(questionsrecord.Key); // Sandhya on 05/12/2015
                                }
                                vendor.PrequalificationId = PreQualificationId;

                                if (!string.IsNullOrEmpty(vendor.FullName) || !string.IsNullOrEmpty(vendor.Title) || !string.IsNullOrEmpty(vendor.EmailAddress) || !string.IsNullOrEmpty(vendor.TelephoneNumber))
                                    entityDB.VendorDetails.Add(vendor);
                                entityDB.SaveChanges();
                            }
                            else if (key.ToLower().Equals("clientvendorid"))
                            {
                                // sumanth on 08/12/0215

                                foreach (var questions in questionsrecord.Questions)
                                {
                                    ClientVendorReferenceIds ClientVendorRefId = new ClientVendorReferenceIds();

                                    long prequalificationId = 0;
                                    //var questionColumID = questions.QuestionColumnId;
                                    //var questionText = questions.Questions.QuestionText.Replace(":", "").Replace(" ", "").Trim();
                                    //var userInput = "";
                                    //if (questions.PrequalificationUserInputs != null && questions.PrequalificationUserInputs.Count() > 0)
                                    //{
                                    //    userInput = questions.PrequalificationUserInputs[0].UserInput;
                                    //    prequalificationId = questions.PrequalificationUserInputs[0].PreQualificationId;
                                    //}
                                    var questionText = questions.QuestionText.Replace(":", "").Replace(" ", "").Trim();
                                    var userInput = questions.PrequalificationUserInput;

                                    ClientVendorRefId = entityDB.ClientVendorReferenceIds.FirstOrDefault(rec => rec.ClientId == currentPreQualification.ClientId && rec.VendorId == currentPreQualification.VendorId);


                                    if (ClientVendorRefId == null)
                                    {
                                        ClientVendorReferenceIds ClientVendorRefId1 = new ClientVendorReferenceIds();
                                        //if (prequalificationId == PreQualificationId)
                                        //{

                                        ClientVendorRefId1.ClientId = currentPreQualification.ClientId;
                                        ClientVendorRefId1.VendorId = currentPreQualification.VendorId;
                                        ClientVendorRefId1.ClientVendorReferenceId = userInput;
                                        //}
                                        if (!string.IsNullOrEmpty(ClientVendorRefId1.ClientVendorReferenceId))
                                            entityDB.ClientVendorReferenceIds.Add(ClientVendorRefId1);
                                        entityDB.SaveChanges();

                                    }
                                    else
                                    {
                                        ClientVendorRefId.ClientVendorReferenceId = userInput;
                                        entityDB.Entry(ClientVendorRefId).State = EntityState.Modified;
                                        entityDB.SaveChanges();
                                    }




                                }
                            }
                        }
                    }
                    //Sandhya 04/11/2015---End

                    //To Insert MPA Client Sign

                    if (isFirstInsertion_ForMPA)
                    {
                        isFirstInsertion_ForMPA = false;
                        if (currentSection.TemplateSectionType == 33)//If it is MPA
                        {
                            List<long> QuestionColumnIds = new List<long>();
                            foreach (var subSec in currentSection.TemplateSubSections.ToList())
                            {
                                foreach (var questions in subSec.Questions.ToList())
                                {
                                    foreach (var qColumns in questions.QuestionColumnDetails.ToList())
                                        QuestionColumnIds.Add(qColumns.QuestionColumnId);
                                }
                            }

                            var clientSigns = entityDB.ClientUserSignature.Where(rec => QuestionColumnIds.Contains(rec.QuestionColumnId));
                            foreach (var clientUserSign in clientSigns.ToList())
                            {
                                var controlType = clientUserSign.QuestionColumnDetails.QuestionControlTypeId;
                                bool isInsert = true;
                                var preUserInput = entityDB.PrequalificationUserInput.FirstOrDefault(Rec => Rec.QuestionColumnId == clientUserSign.QuestionColumnId && Rec.PreQualificationId == currentPreQualification.PrequalificationId);
                                if (preUserInput == null)
                                    preUserInput = new PrequalificationUserInput();
                                else
                                    isInsert = false;

                                String getRefVal = "";
                                if (clientUserSign.GetValueQuestionColumId != null)
                                {
                                    try
                                    {
                                        getRefVal = entityDB.PrequalificationUserInput.FirstOrDefault(rec => rec.PreQualificationId == currentPreQualification.PrequalificationId && rec.QuestionColumnId == clientUserSign.GetValueQuestionColumId).UserInput;
                                    }
                                    catch { }
                                }
                                preUserInput.PreQualificationId = currentPreQualification.PrequalificationId;
                                preUserInput.QuestionColumnId = clientUserSign.QuestionColumnId;
                                preUserInput.UserInput = clientUserSign.QuestionColumnIdValue;
                                if (!getRefVal.Equals(""))
                                    preUserInput.UserInput = getRefVal;
                                if (isInsert)
                                    entityDB.PrequalificationUserInput.Add(preUserInput);
                                else
                                    entityDB.Entry(preUserInput).State = EntityState.Modified;
                            }
                            entityDB.SaveChanges();
                        }
                    }

                    //Ends<<<

                    if (type != 1)//If it is not an intrem(Only )Save
                        SendSubSectionNotification(SectionId, PreQualificationId, currentPreQualification);

                    var Tsection = entityDB.TemplateSections.Find(SectionId);
                    //var SubSection = Tsection.TemplateSubSections.FirstOrDefault(rec => rec.SubSectionTypeCondition == "CSIWebServiceResponse");
                    //if ( SubSection!= null)
                    //{
                    //    // Removed if condition on 07/31/2014
                    //    //if (entityDB.CSIWebResponseDetails.FirstOrDefault(rec => rec.PrequalificationId == PreQualificationId && SubSection.SubSectionID==rec.ResponseNeededForID) == null)
                    //        CSIWebServiceInitiateRequest(currentPreQualification,SubSection.SubSectionID);
                    //}


                }
                catch (Exception ee)
                {
                    result = "Error : " + ee.Message;
                }
            }
            //Rajesh on 11/23/2013
            if (type != null && type == 1)
                return "Saved Successfully";
            //Ends
            //Rajesh on 08/26/2013
            PrequalificationCompletedSections sectionCompleted = entityDB.PrequalificationCompletedSections.FirstOrDefault(rec => rec.PrequalificationId == PreQualificationId && rec.TemplateSectionId == SectionId);
            //var templateSection = entityDB.TemplateSections.Find(SectionId);
            //if (templateSection.TemplateSectionType == 3)
            //{
            //    var prequalification = entityDB.Prequalification.Find(PreQualificationId);
            //    // kiran on 3/17/2014
            //    if (prequalification.PaymentReceived != true)
            //    {
            //        //prequalification.PaymentReceivedAmount = paymentAmt;
            //        //prequalification.PaymentReceivedDate = DateTime.Now;
            //        prequalification.PaymentReceived = true;//In Here we are doing skip payment
            //        entityDB.Entry(prequalification).State = EntityState.Modified;
            //        entityDB.SaveChanges();
            //    }
            //    // Ends<<<
            //    prequalification.PaymentReceived = true;
            //    entityDB.Entry(prequalification).State = EntityState.Modified;
            //    entityDB.SaveChanges();
            //}

            //Ends <<<<<


            // Rajesh on 07/27/2013
            string strNextSectionURL = "";

            Prequalification preQualificationDetails = entityDB.Prequalification.Find(PreQualificationId);
            List<TemplateSections> tempSections = preQualificationDetails.ClientTemplates.Templates.TemplateSections.Where(rec => rec.Visible == true).OrderBy(rec => rec.DisplayOrder).ToList();

            //Rajesh on 1/21/2014
            if (Session["RoleName"].ToString().Equals("Client"))
            {
                long[] prequalificationStatus = { 3, 15 };
                int status = Array.IndexOf(prequalificationStatus, preQualificationDetails.PrequalificationStatusId);
                if (status != -1)
                    tempSections = preQualificationDetails.ClientTemplates.Templates.TemplateSections.Where(rec => rec.Visible == true && rec.VisibleToClient == true).OrderBy(rec => rec.DisplayOrder).ToList();

            }
            //Ends<<<
            //Rajesh on 2/13/2014
            if (Session["RoleName"].ToString().Equals("Vendor"))//If Vendor Login
            {

                //To get all templates sections permission with visible to vendor client admin
                //var allVisibleTemplateSections = (from templateSecPermission in entityDB.TemplateSectionsPermission.Where(rec => rec.VisibleTo == 3 || rec.VisibleTo == 1).ToList()
                //                                  select templateSecPermission.TemplateSectionID).ToList();



                var allVisibleTemplateSections = entityDB.TemplateSectionsPermission.Where(rec => rec.VisibleTo == 3 || rec.VisibleTo == 1 || rec.VisibleTo == 5).Select(rec => rec.TemplateSectionID).ToList();
                Guid userId = new Guid(Session["UserId"].ToString());
                //var userhasRestrictedSectionAccess = FVGen3.BusinessLogic.RolesHelper.IsUserInRole(userId, 1001, "read", preQualificationDetails.ClientId, preQualificationDetails.VendorId);
                ViewBag.RestrictedSectionEdit = FVGen3.BusinessLogic.RolesHelper.IsUserInRole(userId, 1001, "read", preQualificationDetails.ClientId, preQualificationDetails.VendorId);
                //if (userhasRestrictedSectionAccess)
                //{
                //    allVisibleTemplateSections.AddRange(entityDB.TemplateSectionsPermission.Where(rec => rec.VisibleTo == 4).Select(rec => rec.TemplateSectionID).ToList());
                //}

                tempSections = preQualificationDetails.ClientTemplates.Templates.TemplateSections.Where(rec => rec.Visible == true && allVisibleTemplateSections.Contains(rec.TemplateSectionID)).OrderBy(rec => rec.DisplayOrder).ToList();
            }
            //Ends<<<

            long longSecid = SectionId;
            try
            {
                //Rajesh on 1/21/2014
                if (tempSections.FirstOrDefault(rec => rec.TemplateSectionID == SectionId).TemplateSectionType == 99)//If It is Finalize and Submit
                {
                    NextURL(PreQualificationId, ref result, ref strNextSectionURL, preQualificationDetails, paymentAmt, SectionId);
                    sectionCompleted = MarkSectionAsCompleted(SectionId, PreQualificationId, result, sectionCompleted, tempSections, type);
                    return result;
                }
                //Ends<<

                longSecid = tempSections[tempSections.IndexOf(tempSections.FirstOrDefault(rec => rec.TemplateSectionID == SectionId)) + 1].TemplateSectionID;
                if (tempSections.FirstOrDefault(rec => rec.TemplateSectionID == longSecid).TemplateSectionType == 34)
                {
                    if (preQualificationDetails.RequestForMoreInfo == true)
                    {
                        //Do Nothing
                    }
                    else//To get next Section
                        longSecid = tempSections[tempSections.IndexOf(tempSections.FirstOrDefault(rec => rec.TemplateSectionID == longSecid)) + 1].TemplateSectionID;
                }
            }
            catch (Exception ee)
            { //Means it is last page submission
                //Rajesh on 1/21/2014
                NextURL(PreQualificationId, ref result, ref strNextSectionURL, preQualificationDetails, paymentAmt, SectionId);
                sectionCompleted = MarkSectionAsCompleted(SectionId, PreQualificationId, result, sectionCompleted, tempSections, type);
                // Ends <<<<

                return result;
            }
            strNextSectionURL = "../Prequalification/TemplateSectionsList?templateId=" + preQualificationDetails.ClientTemplates.TemplateID + "&TemplateSectionID=" + longSecid + "&PreQualificationId=" + PreQualificationId;
            if (result.Equals("Success"))
                result = strNextSectionURL;
            // Ends <<<<

            sectionCompleted = MarkSectionAsCompleted(SectionId, PreQualificationId, result, sectionCompleted, tempSections, type);
            return result;
        }

        public ActionResult GetPrequalificationPayments(long vendorId)
        {
            var helper = new PrequalificationHelper();
            var payments = helper.GetPrequalificationPayment(vendorId);
            return Json(payments);
        }
        public ActionResult GetQuizPayments(long vendorId)
        {
            var helper = new PrequalificationHelper();
            var payments = helper.GetQuizPayments(vendorId);
            return Json(payments);
        }
        private void SendSubSectionNotification(long SectionId, long PreQualificationId, Prequalification currentPreQualification)
        {
            //To Send Notifications for particular sections
            //Rajesh on 2/17/2014

            if (Session["RoleName"].ToString().Equals("Vendor") || Session["RoleName"].ToString().Equals("Client"))
            {
                var EditableSubSectionIds = new List<long>();
                var flag_SendNotification = true;
                var section = entityDB.TemplateSections.Find(SectionId);
                if (Session["RoleName"].ToString().Equals("Client"))
                {
                    flag_SendNotification = false;
                    if (section.TemplateSectionsPermission.FirstOrDefault().VisibleTo == 2)//If it is client data entry//Rajesh on 3/21/2014
                    {
                        flag_SendNotification = true;
                    }
                    else
                        foreach (var subSec in section.TemplateSubSections.ToList())
                        {
                            //If It is limited access permission then check this user has edit permission
                            try
                            {
                                if (subSec.TemplateSubSectionsPermissions.FirstOrDefault(rec => rec.PermissionFor == "Client").PermissionType == 1)
                                {
                                    var loginUserId = new Guid(Session["UserId"].ToString());
                                    if (subSec.TemplateSubSectionsPermissions.FirstOrDefault(rec => rec.PermissionFor == "Client").TemplateSubSectionsUserPermissions.FirstOrDefault(rec => rec.UserId == loginUserId).EditAccess)
                                    {
                                        flag_SendNotification = true;
                                        EditableSubSectionIds.Add(subSec.SubSectionID);
                                        //break;//If atleast 1 sub section is editable we can send notification
                                    }
                                }
                            }
                            catch { }
                        }
                }

                var SubSections = section.TemplateSubSections.ToList();
                if (Session["RoleName"].ToString().Equals("Client") && section.TemplateSectionsPermission.FirstOrDefault().VisibleTo != 2)//Rajesh on 3/21/2014
                    SubSections = section.TemplateSubSections.Where(rec => EditableSubSectionIds.Contains(rec.SubSectionID)).ToList();

                if (flag_SendNotification)
                    foreach (var subSec in SubSections)
                    {
                        var sendNotification = true;
                        if (subSec.NotificationExist == true)
                        {

                            //foreach (var Question in subSec.Questions.Where(rec => rec.IsMandatory == true).ToList())
                            //{
                            //    if (sendNotification)
                            //        foreach (var QColumn in Question.QuestionColumnDetails.ToList())
                            //        {
                            //            if (entityDB.PrequalificationUserInput.FirstOrDefault(rec => rec.PreQualificationId == PreQualificationId && rec.QuestionColumnId == QColumn.QuestionColumnId) == null)
                            //            {
                            //                sendNotification = false;
                            //                break;
                            //            }
                            //        }
                            //}
                            if (sendNotification == false)
                                break;



                            var notification = subSec.TemplateSubSectionsNotifications.FirstOrDefault();


                            if (notification != null)
                                if (entityDB.PrequalificationNotifications.FirstOrDefault(rec => rec.PrequalificationId == PreQualificationId && rec.TemplateSubSectionNotificationId == notification.TemplateSubSectionNotificationId) == null)
                                {

                                    var template = entityDB.EmailTemplates.Find(notification.EmaiTemplateId);

                                    var subject = template.EmailSubject;
                                    var body = template.EmailBody;
                                    var Comment = template.CommentText;
                                    var clientEmail = "";
                                    var strMessage = "";
                                    var emailsToSent = ""; // Kiran on 9/23/2014
                                    var clientSysUserOrgs = currentPreQualification.Client.SystemUsersOrganizations;
                                    foreach (var clientUserOrg in clientSysUserOrgs)
                                    {
                                        var clientOrgRoles = clientUserOrg.SystemUsersOrganizationsRoles;
                                        foreach (var clientOrgRole in clientOrgRoles)
                                        {
                                            if (clientOrgRole.SystemRoles.RoleName.Equals("Super User"))
                                            {
                                                clientEmail = clientUserOrg.SystemUsers.Email;
                                            }
                                        }
                                    }
                                    subject = subject.Replace("[Date]", DateTime.Now + "");
                                    subject = subject.Replace("[VendorName]", currentPreQualification.Vendor.Name);
                                    subject = subject.Replace("[ClientName]", currentPreQualification.Client.Name);
                                    subject = subject.Replace("[CustomerServicePhone]", currentPreQualification.Client.PhoneNumber);
                                    subject = subject.Replace("[CustomerServiceEmail]", clientEmail);

                                    body = body.Replace("[Date]", DateTime.Now + "");
                                    body = body.Replace("[VendorName]", currentPreQualification.Vendor.Name);
                                    body = body.Replace("[ClientName]", currentPreQualification.Client.Name);
                                    body = body.Replace("[CustomerServicePhone]", currentPreQualification.Client.PhoneNumber);
                                    body = body.Replace("[CustomerServiceEmail]", clientEmail);
                                    // Kiran on 9/23/2014
                                    var subSectionNotificationsToSent = subSec.TemplateSubSectionsNotifications.Where(rec => rec.NotifyUserDisabled != true).ToList();
                                    if (subSectionNotificationsToSent != null)
                                    {

                                        foreach (var notificationToSent in subSectionNotificationsToSent)
                                        {
                                            emailsToSent = emailsToSent + notificationToSent.AdminOrClientUser.Email + ",";
                                        }
                                        emailsToSent = emailsToSent.Substring(0, emailsToSent.Length - 1);
                                    }
                                    //if (notification.AdminUserId != null)
                                    //strMessage = SendMail.sendMail(emailsToSent, body + "<br><br>" + Comment, subject);
                                    strMessage = SendMail.sendMail(emailsToSent, body, subject);
                                    //if (notification.ClientUserId != null)
                                    //    strMessage = SendMail.sendMail(notification.ClientUser.Email, body + "<br><br>" + Comment, subject);
                                    if (strMessage.ToString() == "Success")
                                    {
                                        //Rajesh on 1/8/2015
                                        if (!String.IsNullOrEmpty(Comment))
                                        {
                                            OrganizationsNotificationsSent NotificationSent = entityDB.OrganizationsNotificationsSent.FirstOrDefault(rec => rec.ClientId == currentPreQualification.ClientId && rec.SetupType == 2);
                                            if (NotificationSent == null)
                                            {
                                                NotificationSent = new OrganizationsNotificationsSent();
                                                NotificationSent.ClientId = currentPreQualification.ClientId;
                                                NotificationSent.SetupType = 2;
                                                entityDB.OrganizationsNotificationsSent.Add(NotificationSent);
                                            }
                                            var logs = new OrganizationsNotificationsEmailLogs();
                                            logs.Comments = Comment;
                                            logs.EmailSentDateTime = DateTime.Now;
                                            logs.MailSentBy = new Guid(Session["UserId"].ToString());
                                            logs.NotificationId = NotificationSent.NotificationId;
                                            logs.NotificationNo = 1;
                                            logs.NotificationStatus = 0;
                                            logs.NotificationType = 0;
                                            logs.PrequalificationId = currentPreQualification.PrequalificationId;
                                            logs.VendorId = currentPreQualification.VendorId;
                                            entityDB.OrganizationsNotificationsEmailLogs.Add(logs);
                                            entityDB.SaveChanges();
                                        }
                                        //Ends<<<
                                        foreach (var notificationSent in subSectionNotificationsToSent)
                                        {
                                            PrequalificationNotifications PreNotifications = new PrequalificationNotifications();
                                            PreNotifications.PrequalificationId = PreQualificationId;
                                            PreNotifications.TemplateSubSectionNotificationId = notificationSent.TemplateSubSectionNotificationId;
                                            PreNotifications.NotificationDateTime = DateTime.Now;
                                            entityDB.PrequalificationNotifications.Add(PreNotifications);
                                            entityDB.SaveChanges();//Rajesh on 3/4/2014
                                        }
                                    }
                                }
                        }
                    }
            }
            //Ends<<<
        }

        private PrequalificationCompletedSections MarkSectionAsCompleted(long SectionId, long PreQualificationId, String result, PrequalificationCompletedSections sectionCompleted, List<TemplateSections> tempSections, int? type)
        {
            //Rajesh on 4/7/2014
            if (type == 3)
                return sectionCompleted;
            //Ends<<<
            if (sectionCompleted == null)
            {
                var firstOrDefault = tempSections.FirstOrDefault(rec => rec.TemplateSectionID == SectionId);
                if (firstOrDefault != null && (firstOrDefault.TemplateSectionType == 99 && result == "" || result == "preqOpenPeriodEnds"))// changes by Mani on 8/6/2015 
                {
                    //Do nothing there is some incomplete scction in this prequalification
                }
                else
                {
                    sectionCompleted = new PrequalificationCompletedSections();
                    sectionCompleted.PrequalificationId = PreQualificationId;
                    sectionCompleted.TemplateSectionId = SectionId;
                    entityDB.PrequalificationCompletedSections.Add(sectionCompleted);
                    entityDB.SaveChanges();
                }
            }
            //Rajesh on 9/25/2014
            //initially our system is like this.After prequalification completed by vendor it's directly send to admin for processing means finalize& complete satatus is 5.but now we have intrime step.
            //After finalize and submit prequalification status will be client review(16) if client completes all sections then this prequalification status will be 5(Submited to admin)
            var prequalification_loc = entityDB.Prequalification.Find(PreQualificationId);
            if (Session["RoleName"].ToString().Equals("Client") && prequalification_loc.PrequalificationStatusId == 16)
            {
                var flag_ClientSecCompleted = true;

                foreach (TemplateSections section in prequalification_loc.ClientTemplates.Templates.TemplateSections.Where(rec => rec.Visible == true).OrderBy(x => x.DisplayOrder).ToList())
                {
                    var completedSection = entityDB.PrequalificationCompletedSections.FirstOrDefault(rec => rec.PrequalificationId == PreQualificationId && rec.TemplateSectionId == section.TemplateSectionID);//Rajesh on 2/24/2014
                    if (completedSection == null)//Rajesh on 2/24/2014
                    {
                        //Rajesh on 3/6/2014
                        if (section.TemplateSectionsPermission.FirstOrDefault().VisibleTo == 2)//Client Data Section No need to check this section for completion
                        {
                            flag_ClientSecCompleted = false;//Rajesh on 9/25/2014
                        }
                    }
                }

                if (flag_ClientSecCompleted)
                {
                    prequalification_loc.PrequalificationStatusId = 5;
                    entityDB.Entry(prequalification_loc).State = EntityState.Modified;
                    entityDB.SaveChanges();
                }
            }
            return sectionCompleted;
        }

        //private void CSIWebServiceInitiateRequest(Prequalification currentPreQualification,long SubSecId)
        //{

        //    WATCHDOGWebReference.SearchService myService = new WATCHDOGWebReference.SearchService();
        //    myService.Url = ConfigurationSettings.AppSettings["ATTUSWebServiceURL"].ToString();
        //    myService.Timeout = 30000;

        //    XmlDocument myXmlRequest = new XmlDocument();
        //    XmlNode docNode = myXmlRequest.CreateXmlDeclaration("1.0", "UTF-8", null);
        //    myXmlRequest.AppendChild(docNode);

        //    XmlNode ATTUSWATCHDOGREQUEST = myXmlRequest.CreateElement("", "ATTUSWATCHDOGREQUEST", "");
        //    myXmlRequest.AppendChild(ATTUSWATCHDOGREQUEST);

        //    XmlNode BILLINGCODE = myXmlRequest.CreateElement("", "BILLINGCODE", "");

        //    XmlAttribute partnerIdAttribute = myXmlRequest.CreateAttribute("partnerid");
        //    partnerIdAttribute.Value = "91b53925-dddf-44dd-a84f-baf6ce3c6aee";
        //    BILLINGCODE.Attributes.Append(partnerIdAttribute);

        //    XmlAttribute billingIdAttribute = myXmlRequest.CreateAttribute("billingid");
        //    billingIdAttribute.Value = "91b53925-dddf-44dd-a84f-baf6ce3c6aee";
        //    BILLINGCODE.Attributes.Append(billingIdAttribute);

        //    ATTUSWATCHDOGREQUEST.AppendChild(BILLINGCODE);

        //    XmlNode LISTS = myXmlRequest.CreateElement("", "LISTS", "");

        //    XmlNode LIST = myXmlRequest.CreateElement("", "LIST", "");

        //    XmlAttribute codeAttribute = myXmlRequest.CreateAttribute("code");
        //    codeAttribute.Value = "OFAC";
        //    LIST.Attributes.Append(codeAttribute);

        //    LISTS.AppendChild(LIST);
        //    ATTUSWATCHDOGREQUEST.AppendChild(LISTS);

        //    XmlNode QUERY = myXmlRequest.CreateElement("", "QUERY", "");

        //    XmlNode ITEM = myXmlRequest.CreateElement("", "ITEM", "");

        //    XmlAttribute allentitiesAttribute = myXmlRequest.CreateAttribute("allentities");
        //    allentitiesAttribute.Value = currentPreQualification.Vendor.PrincipalCompanyOfficerName;
        //    ITEM.Attributes.Append(allentitiesAttribute);

        //    XmlAttribute corpnameAttribute = myXmlRequest.CreateAttribute("corpname");
        //    corpnameAttribute.Value = currentPreQualification.Vendor.Name;
        //    ITEM.Attributes.Append(corpnameAttribute);

        //    QUERY.AppendChild(ITEM);
        //    ATTUSWATCHDOGREQUEST.AppendChild(QUERY);

        //    XmlNode VERSION = myXmlRequest.CreateElement("", "VERSION", "");
        //    VERSION.InnerText = "2.0";

        //    ATTUSWATCHDOGREQUEST.AppendChild(VERSION);

        //    XmlNode myXmlResponse = myService.ProcessNameListMessage(myXmlRequest);

        //    // Kiran on 3/5/2014
        //    XmlDocument responseDoc = new XmlDocument();
        //    responseDoc.LoadXml(myXmlResponse.OuterXml.ToString());

        //    string fileName = currentPreQualification.Vendor.Name + "_" + currentPreQualification.PrequalificationId;


        //    Directory.CreateDirectory(Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["WebResponseFiles"])));
        //    var path = Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["WebResponseFiles"]), fileName + ".xml");
        //    // Kiran on 7/31/2014
        //    if (path != null)
        //    {
        //        System.IO.File.Delete(path);
        //    }
        //    responseDoc.Save(path);
        //    // Ends<<<

        //    XmlNodeList parentNodes = myXmlResponse.ChildNodes;

        //    // Kiran on 7/31/2014
        //    var existingCSIRecordsForCurrentPrequalification = entityDB.CSIWebResponseDetails.Where(rec => rec.PrequalificationId == currentPreQualification.PrequalificationId);
        //    if (existingCSIRecordsForCurrentPrequalification != null)
        //    {
        //        foreach (var existingCSIRecord in existingCSIRecordsForCurrentPrequalification.ToList())
        //        {
        //            entityDB.CSIWebResponseDetails.Remove(existingCSIRecord);
        //            entityDB.SaveChanges();
        //        }
        //    }
        //    // Ends<<<

        //    foreach (XmlNode parentNode in parentNodes)
        //    {
        //        if (parentNode.Name == "SEARCHCRITERIA")
        //        {
        //            XmlNodeList searchCriteriaNodes = parentNode.ChildNodes;
        //            foreach (XmlNode searchCriteriaNode in searchCriteriaNodes)
        //            {
        //                if (searchCriteriaNode.Name == "ENTITIES")
        //                {
        //                    XmlNodeList entitiesNodes = searchCriteriaNode.ChildNodes;
        //                    if (entitiesNodes != null)
        //                    {
        //                        foreach (XmlNode entitiesNode in entitiesNodes)
        //                        {
        //                            if (entitiesNode.Name == "ENTITY")
        //                            {
        //                                XmlNodeList entityElementNodes = entitiesNode.ChildNodes;
        //                                var entityNodeAttributes = entitiesNode.Attributes;

        //                                foreach (XmlNode node in entityElementNodes)
        //                                {
        //                                    CSIWebResponseDetails csiRecord = new CSIWebResponseDetails();
        //                                    csiRecord.PrequalificationId = currentPreQualification.PrequalificationId;
        //                                    csiRecord.ResponseNeededForID = SubSecId;
        //                                    csiRecord.RequestInvokedFrom = 0;
        //                                    csiRecord.EntityID = Convert.ToInt64(entityNodeAttributes[0].Value);
        //                                    csiRecord.EntityNumber = Convert.ToInt64(entityNodeAttributes[1].Value);
        //                                    if (node.ChildNodes.Count != 0)
        //                                    {
        //                                        XmlNodeList childNodesOfEntityInnerNodes = node.ChildNodes;
        //                                        if (childNodesOfEntityInnerNodes.Count > 1)
        //                                        {
        //                                            foreach (XmlNode childNode in childNodesOfEntityInnerNodes)
        //                                            {
        //                                                if (childNode.Name == "ALIAS")
        //                                                {
        //                                                    csiRecord.ElementNodeName = childNode.Name;
        //                                                    csiRecord.ElementNodeValue = childNode.Attributes[0].Value;
        //                                                }
        //                                                else
        //                                                {
        //                                                    csiRecord.ElementNodeName = childNode.Name;
        //                                                    var addressAttributes = childNode.Attributes;
        //                                                    csiRecord.ElementNodeValue = addressAttributes[0].Value + "," + addressAttributes[1].Value + "," + addressAttributes[2].Value + "," + addressAttributes[4].Value + "," + addressAttributes[5].Value;
        //                                                }
        //                                                entityDB.CSIWebResponseDetails.Add(csiRecord);
        //                                                entityDB.SaveChanges();
        //                                            }
        //                                        }
        //                                        else
        //                                        {
        //                                            foreach (XmlNode childNode in childNodesOfEntityInnerNodes)
        //                                            {
        //                                                if (childNode.Name == "ALIAS")
        //                                                {
        //                                                    csiRecord.ElementNodeName = childNode.Name;
        //                                                    csiRecord.ElementNodeValue = childNode.Attributes[0].Value;
        //                                                }
        //                                                else
        //                                                {
        //                                                    csiRecord.ElementNodeName = childNode.ParentNode.Name;
        //                                                    csiRecord.ElementNodeValue = childNode.Value;
        //                                                }
        //                                                entityDB.CSIWebResponseDetails.Add(csiRecord);
        //                                                entityDB.SaveChanges();
        //                                            }
        //                                        }
        //                                    }
        //                                    else
        //                                    {
        //                                        csiRecord.ElementNodeName = node.Name;
        //                                        csiRecord.ElementNodeValue = node.Value;
        //                                        entityDB.CSIWebResponseDetails.Add(csiRecord);
        //                                        entityDB.SaveChanges();
        //                                    }

        //                                }
        //                            }
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //    }
        //}
        //Rajesh on 1/21/2014
        private void NextURL(long PreQualificationId, ref String result, ref string strNextSectionURL, Prequalification preQualificationDetails, decimal? paymentAmt, long SectionId)
        {
            Prequalification prequalification_loc = entityDB.Prequalification.Find(PreQualificationId);
            bool flag_Completed = true;
            bool flag_ClientSecCompleted = true;//Rajesh on 9/25/2014
            foreach (TemplateSections section in prequalification_loc.ClientTemplates.Templates.TemplateSections.Where(rec => rec.Visible == true).OrderBy(x => x.DisplayOrder).ToList())
            {
                var completedSection = entityDB.PrequalificationCompletedSections.FirstOrDefault(rec => rec.PrequalificationId == PreQualificationId && rec.TemplateSectionId == section.TemplateSectionID);//Rajesh on 2/24/2014

                if (completedSection == null)//Rajesh on 2/24/2014
                {
                    //Rajesh on 3/6/2014
                    if (section.TemplateSectionsPermission.FirstOrDefault().VisibleTo == 2 || section.TemplateSectionsPermission.FirstOrDefault().VisibleTo == 4)//Client Data Section No need to check this section for completion
                    {
                        flag_ClientSecCompleted = false;//Rajesh on 9/25/2014
                    }
                    //Rajesh on 3/20/2014
                    else if (section.TemplateSectionID == SectionId)//Menas if we check same section at that time it well be not completed section so we need to skip checking
                    {
                    }
                    //Ends<<
                    // Ends <<<
                    else if (section.TemplateSectionType == 34)
                    {
                        if (preQualificationDetails.RequestForMoreInfo == true)
                        {
                            flag_Completed = false;
                            break;
                        }
                    }
                    else if (section.TemplateSectionType != 35)//Rajesh on 1/21/2014
                    {
                        flag_Completed = false;
                        break;
                    }

                }
                // Rajesh on 01/20/2015 to open the Exception/Probation section forever.
                //else
                //{
                //    if (section.TemplateSectionType == 34)
                //    {
                //        if (preQualificationDetails.RequestForMoreInfo == true)
                //        {
                //            flag_Completed = false;
                //            break;
                //        }
                //    }

                //}
                // Ends<<<
            }
            // Mani on 7/14/2015 for so-140
            // if (flag_Completed)//All Sections are completed
            //{
            //    var CurrentDate = DateTime.Now;
            //    var preqOpenperiod = entityDB.OrganizationsPrequalificationOpenPeriod.Where(rec => rec.VendorId == prequalification_loc.VendorId).OrderByDescending(rec => rec.PrequalificationPeriodClose).FirstOrDefault();
            //    var templatePrice = entityDB.TemplatePrices.FirstOrDefault(rec => rec.TemplateID == prequalification_loc.ClientTemplates.TemplateID).TemplatePrice;
            //    if (preqOpenperiod != null && preqOpenperiod.PrequalificationPeriodClose < CurrentDate && templatePrice !=prequalification_loc.PaymentReceivedAmount)
            //    {
            //        var paymentSection = entityDB.PrequalificationCompletedSections.FirstOrDefault(rec => rec.PrequalificationId == PreQualificationId && rec.TemplateSections.TemplateSectionType == 3);
            //        entityDB.PrequalificationCompletedSections.Remove(paymentSection);
            //        // Modify preq data
            //        prequalification_loc.PaymentReceived = false;
            //        entityDB.Entry(prequalification_loc).State = EntityState.Modified;
            //        entityDB.SaveChanges();
            //        result = "preqOpenPeriodEnds";
            //        flag_Completed = false;

            //    }
            // }
            // Ends >>
            //Rajesh on 1/21/2014
            if (Session["RoleName"].ToString().Equals("Client"))
            {
                flag_Completed = false;
            }
            //Ends<<<
            if (flag_Completed)//All Sections are completed
            {
                //if (prequalification_loc.PrequalificationStatusId != 5)
                //Rajesh on 9/25/2014
                var PresentPrequalificationstatus = 5;
                //if (!flag_ClientSecCompleted)
                //    PresentPrequalificationstatus = 16;
                //Ends<<
                if (prequalification_loc.PrequalificationStatusId != 5 || prequalification_loc.PrequalificationStatusId != 16)//16 means client review
                {
                    //To add Prequalification Logs
                    PrequalificationStatusChangeLog preQualificationLog = new PrequalificationStatusChangeLog();
                    preQualificationLog.PrequalificationId = prequalification_loc.PrequalificationId;
                    //preQualificationLog.PrequalificationStatusId = 5;//prequalification_loc.PrequalificationStatusId;
                    preQualificationLog.PrequalificationStatusId = PresentPrequalificationstatus;//rajesh on 9/25/2014
                    preQualificationLog.StatusChangeDate = DateTime.Now;
                    preQualificationLog.StatusChangedByUser = new Guid(Session["UserId"].ToString());
                    preQualificationLog.PrequalificationStart = preQualificationDetails.PrequalificationStart;
                    preQualificationLog.PrequalificationFinish = preQualificationDetails.PrequalificationFinish;
                    entityDB.PrequalificationStatusChangeLog.Add(preQualificationLog);
                    //>>>Ends
                }

                //Rajesh on 3/17/2014
                if (prequalification_loc.PrequalificationStatusId == 3)//Rajesh on 9/26/2014 // Kiran on 12/05/2015 for 500^ Validation(Separated the logic for Pending, Pending-Renewal Statuses)
                {
                    var recievedPreqPrice = entityDB.PrequalificationPayments.FirstOrDefault(rec => rec.PrequalificationId == PreQualificationId && rec.PaymentCategory == 0).PaymentReceivedAmount;// Mani on 8/10/2015
                    var CurrentDate = DateTime.Now;
                    var OPeriod = entityDB.OrganizationsPrequalificationOpenPeriod.FirstOrDefault(rec => rec.VendorId == prequalification_loc.VendorId && rec.PrequalificationPeriodStart <= CurrentDate && rec.PrequalificationPeriodClose >= CurrentDate);
                    if (OPeriod == null)
                    {
                        OPeriod = new OrganizationsPrequalificationOpenPeriod();
                        OPeriod.VendorId = prequalification_loc.VendorId;
                        OPeriod.PrequalificationPeriodStart = DateTime.Now;
                        OPeriod.PrequalificationPeriodClose = DateTime.Now.AddYears(1);
                        OPeriod.PeriodStatus = 0;
                        OPeriod.TotalPaymentRecieved = paymentAmt == null ? 0 : (decimal)recievedPreqPrice;
                        entityDB.OrganizationsPrequalificationOpenPeriod.Add(OPeriod);
                    }
                    else
                    {
                        OPeriod.TotalPaymentRecieved += (paymentAmt == null ? 0 : (decimal)recievedPreqPrice);
                        entityDB.Entry(OPeriod).State = EntityState.Modified;
                    }


                    //To Close all OrganizationsPrequalificationOpenPeriod

                    foreach (var openPeriod in entityDB.OrganizationsPrequalificationOpenPeriod.Where(rec => rec.PrequalificationPeriodClose < CurrentDate && rec.PeriodStatus == 0).ToList())
                    {
                        openPeriod.PeriodStatus = 1;
                        entityDB.Entry(openPeriod).State = EntityState.Modified;
                    }
                    entityDB.SaveChanges();
                }
                //Ends<<<

                // Kiran on 12/05/2015 for 500^ validation
                if (prequalification_loc.PrequalificationStatusId == 15)
                {
                    var CurrentDate = DateTime.Now;
                    var templateRecievedAmount = entityDB.PrequalificationPayments.FirstOrDefault(rec => rec.PrequalificationId == PreQualificationId && rec.PaymentCategory == 0).PaymentReceivedAmount;// Mani on 8/10/2015
                    var OPeriod = entityDB.OrganizationsPrequalificationOpenPeriod.FirstOrDefault(rec => rec.VendorId == prequalification_loc.VendorId && rec.PrequalificationPeriodStart <= CurrentDate && rec.PrequalificationPeriodClose >= CurrentDate);
                    OrganizationsPrequalificationOpenPeriod openPeriodOfUpcomingYear = null;
                    DateTime? upcomingYearPeriodStartDate = null;
                    DateTime? upcomingYearPeriodEndDate = null;
                    try
                    {
                        openPeriodOfUpcomingYear = entityDB.OrganizationsPrequalificationOpenPeriod.FirstOrDefault(rec => rec.VendorId == prequalification_loc.VendorId && rec.PrequalificationPeriodStart > OPeriod.PrequalificationPeriodClose);
                    }
                    catch (Exception e) { }
                    // Kiran on 12/21/2015 for FV-241
                    if (OPeriod != null)
                    {
                        upcomingYearPeriodStartDate = OPeriod.PrequalificationPeriodClose.AddDays(1);
                        upcomingYearPeriodEndDate = OPeriod.PrequalificationPeriodClose.AddYears(1);
                    }
                    // Kiran on 12/21/2015 for FV-241 ends<<<

                    if (OPeriod == null) // It means user making payment after the Pending-Renewal period & vendor annualopenperiod expires
                    {
                        OPeriod = new OrganizationsPrequalificationOpenPeriod();
                        OPeriod.VendorId = prequalification_loc.VendorId;
                        OPeriod.PrequalificationPeriodStart = DateTime.Now;
                        OPeriod.PrequalificationPeriodClose = DateTime.Now.AddYears(1);
                        OPeriod.PeriodStatus = 0;
                        OPeriod.TotalPaymentRecieved = paymentAmt == null ? 0 : (decimal)templateRecievedAmount;
                        entityDB.OrganizationsPrequalificationOpenPeriod.Add(OPeriod);
                    }
                    //else if (OPeriod != null && OPeriod.PrequalificationPeriodClose > CurrentDate && openPeriodOfUpcomingYear == null) // It means user making payment during Renewal Open Period
                    else if (OPeriod != null && prequalification_loc.PrequalificationFinish > OPeriod.PrequalificationPeriodClose)
                    {
                        if (openPeriodOfUpcomingYear == null)
                        {
                            openPeriodOfUpcomingYear = new OrganizationsPrequalificationOpenPeriod();
                            openPeriodOfUpcomingYear.VendorId = prequalification_loc.VendorId;
                            openPeriodOfUpcomingYear.PrequalificationPeriodStart = Convert.ToDateTime(upcomingYearPeriodStartDate);
                            openPeriodOfUpcomingYear.PrequalificationPeriodClose = Convert.ToDateTime(upcomingYearPeriodEndDate);
                            openPeriodOfUpcomingYear.PeriodStatus = 0;
                            openPeriodOfUpcomingYear.TotalPaymentRecieved = paymentAmt == null ? 0 : (decimal)templateRecievedAmount;
                            entityDB.OrganizationsPrequalificationOpenPeriod.Add(openPeriodOfUpcomingYear);
                        }
                        else
                        {
                            openPeriodOfUpcomingYear.TotalPaymentRecieved += (paymentAmt == null ? 0 : (decimal)templateRecievedAmount);
                            entityDB.Entry(OPeriod).State = EntityState.Modified;
                        }
                    }
                    else //if (OPeriod.PrequalificationPeriodClose > CurrentDate.AddDays(60) && openPeriodOfUpcomingYear == null)
                    {
                        OPeriod.TotalPaymentRecieved += (paymentAmt == null ? 0 : (decimal)templateRecievedAmount);
                        entityDB.Entry(OPeriod).State = EntityState.Modified;
                    }
                    //else
                    //{
                    //    openPeriodOfUpcomingYear.TotalPaymentRecieved += (paymentAmt == null ? 0 : (decimal)templateRecievedAmount);
                    //    entityDB.Entry(OPeriod).State = EntityState.Modified;
                    //}

                    //To Close all OrganizationsPrequalificationOpenPeriod

                    foreach (var openPeriod in entityDB.OrganizationsPrequalificationOpenPeriod.Where(rec => rec.PrequalificationPeriodClose < CurrentDate && rec.PeriodStatus == 0 && rec.VendorId == prequalification_loc.VendorId).ToList())
                    {
                        openPeriod.PeriodStatus = 1;
                        entityDB.Entry(openPeriod).State = EntityState.Modified;
                    }
                    entityDB.SaveChanges();
                }
                // Kiran on 12/05/2015 for SMI-277 ends

                //prequalification_loc.PrequalificationStatusId = 5;//Submitted for Admin Review
                //Rajesh on 1/6/2015
                DateTime StartDate = DateTime.Now, EndDate = DateTime.Now;
                if (prequalification_loc.PrequalificationStatusId == 3)
                {
                    StartDate = DateTime.Now;
                }
                else if (prequalification_loc.PrequalificationStatusId == 15)
                {
                    StartDate = prequalification_loc.PrequalificationFinish.AddDays(1);
                }
                if (prequalification_loc.PrequalificationStatusId == 3 || prequalification_loc.PrequalificationStatusId == 15)//6 or 16 omit period...
                {
                    var preStatus = entityDB.PrequalificationStatus.FirstOrDefault(rec => rec.PrequalificationStatusId == 5);
                    if (preStatus != null)
                    {
                        if (preStatus.PeriodLengthUnit.ToLower().Equals("d"))
                            EndDate = StartDate.AddDays(preStatus.PeriodLength);
                        else if (preStatus.PeriodLengthUnit.ToLower().Equals("m"))
                            EndDate = StartDate.AddMonths(preStatus.PeriodLength);
                        else if (preStatus.PeriodLengthUnit.ToLower().Equals("y"))
                            EndDate = StartDate.AddYears(preStatus.PeriodLength);
                    }
                    prequalification_loc.PrequalificationStart = StartDate;
                    prequalification_loc.PrequalificationFinish = EndDate;
                }
                //Ends<<<
                prequalification_loc.PrequalificationStatusId = PresentPrequalificationstatus;//Rajesh on 9/25/2014
                prequalification_loc.PrequalificationSubmit = DateTime.Now;
                prequalification_loc.UserIdAsCompleter = (Guid)Session["UserId"];
                entityDB.Entry(prequalification_loc).State = EntityState.Modified;
                entityDB.SaveChanges();


                strNextSectionURL = "../SystemUsers/DashBoard";
            }


            if (result.Equals("Success"))
                result = strNextSectionURL;
        }
        //Ends<<<
        public void getCommand()
        {
            SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["EFDbContext"].ConnectionString);
            connection.Open();
            ViewBag.command = connection.CreateCommand();

        }

        // Rajesh on 08/25/2013
        // Tiered Pricing for Payment
        //private decimal getTemplatePrice(long vendorId, Guid? templateId, long preQualificationId)
        //{
        //    var currentPrequalification = entityDB.Prequalification.Find(preQualificationId);
        //    if (currentPrequalification.PaymentReceivedAmount != null && currentPrequalification.PaymentReceived!=false)
        //    {
        //        ViewBag.TemplatePrice=currentPrequalification.PaymentReceivedAmount;
        //        //return (decimal)currentPrequalification.PaymentReceivedAmount;
        //        return 0;
        //    }

        //    decimal price = 0;
        //    var templatePrices = entityDB.TemplatePrices.FirstOrDefault(rec => rec.TemplateID == templateId);
        //    if (templatePrices == null)
        //        return 0;
        //     else
        //        ViewBag.TemplatePrice = templatePrices.TemplatePrice;
        //    var CurrentDate = DateTime.Now;

        //    var PreopenPeriod = entityDB.OrganizationsPrequalificationOpenPeriod.FirstOrDefault(rec => rec.VendorId == currentPrequalification.VendorId && rec.PrequalificationPeriodStart <= CurrentDate && rec.PrequalificationPeriodClose >= CurrentDate);
        //    if (PreopenPeriod == null && currentPrequalification.PaymentReceived == null)
        //        price = templatePrices.TemplatePrice;
        //    //else if (PreopenPeriod == null && currentPrequalification.PaymentReceived == false && currentPrequalification.PaymentReceivedAmount != null && templatePrices.TemplatePrice != currentPrequalification.PaymentReceivedAmount)
        //    //{
        //    //    var BalancePayment = templatePrices.TemplatePrice - currentPrequalification.PaymentReceivedAmount;
        //    //    price = (decimal)BalancePayment;
        //    //}
        //    else if (PreopenPeriod == null && currentPrequalification.PaymentReceived != null)
        //    {
        //        price = 0;
        //    }
        //    else
        //    {
        //        if (500 - PreopenPeriod.TotalPaymentRecieved >= templatePrices.TemplatePrice)
        //        {
        //            price = templatePrices.TemplatePrice;
        //        }
        //        else
        //        {
        //            price = 500 - PreopenPeriod.TotalPaymentRecieved;
        //            ViewBag.DiscountPrice = templatePrices.TemplatePrice - price;
        //        }
        //    }
        //    if (price <= 0)
        //    {
        //        ViewBag.DiscountPrice = templatePrices.TemplatePrice;
        //        price = 0;
        //    }




        //    //This commented code for Discount Price

        //    //var currentPrequalification = entityDB.Prequalification.Find(preQualificationId);
        //    //if (currentPrequalification.OverridePayment != null && currentPrequalification.OverridePayment == true)
        //    //    if (currentPrequalification.OverirdeAmount != null && currentPrequalification.OverirdeAmount > 0)
        //    //        return (decimal)currentPrequalification.OverirdeAmount;

        //    //decimal price = 0;



        //    //var allPrequalifications = entityDB.Prequalification.Where(rec => rec.VendorId == vendorId && (rec.PrequalificationStatusId == 9 || rec.PrequalificationStatusId == 10));



        //    //int numberOfTemplates = (from a in allPrequalifications.ToList()
        //    //                         select a.ClientTemplates.TemplateID).Count();

        //    //var templatePrices = entityDB.TemplatePrices.FirstOrDefault(rec => rec.TemplateID == templateId);

        //    //if (numberOfTemplates == 0)//means this is first payment
        //    //{
        //    //    if (templatePrices != null)
        //    //        price = templatePrices.TemplatePrice;
        //    //}
        //    //else
        //    //{
        //    //    if (templatePrices != null)
        //    //    price = templatePrices.TemplatePrice;//rajesh on 2/25/2014
        //    //    int discountType = (from a in allPrequalifications.ToList()//If user has different type of templates then type=multiple else same...
        //    //                        select a.ClientTemplates.TemplateID).Distinct().Count() > 1 ? 1 : 0;
        //    //    if (templatePrices != null)
        //    //    {
        //    //        if (templatePrices.TemplateDiscountPrices == null || templatePrices.TemplateDiscountPrices.Count == 0)
        //    //            price = templatePrices.TemplatePrice;
        //    //        else
        //    //            //Rajesh on 2/25/2014
        //    //            try
        //    //            {
        //    //                price = templatePrices.TemplateDiscountPrices.OrderBy(rec => rec.MinNoOfPrequalification).LastOrDefault(rec => rec.MinNoOfPrequalification <= numberOfTemplates + 1 && rec.DiscountType == discountType).DiscountedPrice;//+1 means we are caliculating for next template payment
        //    //            }
        //    //            catch { }
        //    //        //Ends<<<

        //    //    }
        //    // }
        //    ///Ends Discount price code<<
        //    return price;
        //}
        private decimal getTemplatePrice(long vendorId, Guid? clientTemplateId, long preQualificationId)
        {

            var currentPrequalification = entityDB.Prequalification.Find(preQualificationId);
            //if (currentPrequalification.TotalPaymentReceivedAmount != null)
            //{
            //    return (decimal)currentPrequalification.TotalPaymentReceivedAmount;
            //}
            ViewBag.VBBUGroupPrice = entityDB.ClientTemplatesBUGroupPrice.Where(rec => rec.ClientTemplateID == currentPrequalification.ClientTemplateId && rec.GroupingFor == 0).GroupBy(rec => rec.GroupPriceType).ToList();
            decimal price = 0;

            //Siva 20th Feb 2015
            //var templatePrices = entityDB.TemplatePrices.FirstOrDefault(rec => rec.TemplateID == templateId);
            var templatePrices = entityDB.ClientTemplatesBUGroupPrice.FirstOrDefault(rec => rec.ClientTemplateID == clientTemplateId && rec.GroupPriceType == 0 && rec.GroupingFor == 0);
            if (templatePrices == null)
                return 0;
            var CurrentDate = DateTime.Now;

            //var PreopenPeriod = entityDB.OrganizationsPrequalificationOpenPeriod.FirstOrDefault(rec => rec.VendorId == currentPrequalification.VendorId && rec.PrequalificationPeriodStart <= CurrentDate && rec.PrequalificationPeriodClose >= CurrentDate);

            // Kiran on 11/24/2015 for 500^ Validation
            OrganizationsPrequalificationOpenPeriod PreopenPeriod = entityDB.OrganizationsPrequalificationOpenPeriod.FirstOrDefault(rec => rec.VendorId == currentPrequalification.VendorId && rec.PrequalificationPeriodStart <= CurrentDate && rec.PrequalificationPeriodClose >= CurrentDate); ;
            if (currentPrequalification.PrequalificationStatusId != 15)
                PreopenPeriod = PreopenPeriod;
            else if (currentPrequalification.PrequalificationStatusId == 15)
            {
                if (PreopenPeriod != null && PreopenPeriod.PrequalificationPeriodClose >= CurrentDate.AddDays(60))
                    PreopenPeriod = PreopenPeriod;
                else
                    try
                    {
                        PreopenPeriod = entityDB.OrganizationsPrequalificationOpenPeriod.FirstOrDefault(rec => rec.VendorId == vendorId && rec.PrequalificationPeriodStart > PreopenPeriod.PrequalificationPeriodClose);
                    }
                    catch
                    {
                        PreopenPeriod = null;
                    }
            }
            // Kiran on 11/24/2015 for 500^ Validation Ends<<<
            if (PreopenPeriod == null && currentPrequalification.PaymentReceived == null)
                price = Convert.ToDecimal(templatePrices.GroupPrice);
            //else if (PreopenPeriod == null && currentPrequalification.PaymentReceived == false && currentPrequalification.TotalPaymentReceivedAmount != null && currentPrequalification.TotalInvoiceAmount != currentPrequalification.TotalPaymentReceivedAmount)
            //{
            //    var BalancePayment = currentPrequalification.TotalInvoiceAmount - currentPrequalification.TotalPaymentReceivedAmount;
            //    price = (decimal)BalancePayment;
            //}

            else if (PreopenPeriod == null && currentPrequalification.PaymentReceived == false)
                price = Convert.ToDecimal(templatePrices.GroupPrice);

            else if (PreopenPeriod == null && currentPrequalification.PaymentReceived != null)
            {
                price = 0;
            }
            else
            {
                long annualPeriodMaximumAmount = Convert.ToInt64(ConfigurationManager.AppSettings["PrequalificationAnnualPeriodAmount"]);
                if (annualPeriodMaximumAmount - PreopenPeriod.TotalPaymentRecieved >= templatePrices.GroupPrice && currentPrequalification.PaymentReceived == null)
                {
                    price = Convert.ToDecimal(templatePrices.GroupPrice);
                }
                else if (annualPeriodMaximumAmount - PreopenPeriod.TotalPaymentRecieved <= templatePrices.GroupPrice && currentPrequalification.PaymentReceived == null)
                {
                    price = annualPeriodMaximumAmount - PreopenPeriod.TotalPaymentRecieved;
                    ViewBag.DiscountPrice = templatePrices.GroupPrice - price;
                }
                else
                {
                    price = 0;
                }
            }
            if (price <= 0)
            {
                ViewBag.DiscountPrice = Convert.ToDecimal(templatePrices.GroupPrice);
                price = 0;
            }
            return price;
        }
        private decimal paymentSection(long vendorId, Guid? clientTemplateId, long preQualificationId)
        {
            //var paidBuid = entityDB.PrequalificationSites.Where(rec => rec.PrequalificationId == preQualificationId && rec.PrequalificationPaymentsId != null).Select(rec => rec.ClientBusinessUnitId).Distinct().ToList();
            //var bunitIds = entityDB.PrequalificationSites.Where(rec => rec.PrequalificationId == preQualificationId && rec.PrequalificationPaymentsId == null && !paidBuid.Contains(rec.ClientBusinessUnitId)).Select(rec => rec.ClientBusinessUnitId).ToList();
            //bunitIds = bunitIds.Distinct().ToList();
            //paid 0 fee groups for display
            ViewBag.preqBus = entityDB.PrequalificationSites.Where(rec => rec.PrequalificationPayments.PaymentCategory == 0 && rec.PrequalificationId == preQualificationId).Select(rec => rec.ClientBusinessUnits).Distinct().ToList();

            // additional fee cal and list of bu's
            var preqsitebus = entityDB.PrequalificationSites.Where(rec => rec.PrequalificationId == preQualificationId).Select(rec => rec.ClientBusinessUnitId).Distinct().ToList();
            var additionalFeeBu = entityDB.ClientTemplatesBUGroupPrice.Where(rec => rec.ClientTemplateID == clientTemplateId && rec.GroupingFor == 0 && rec.GroupPrice != 0 && rec.GroupPriceType != 0).Select(rec => rec.ClientTemplateBUGroupId).ToList();

            var paidAdditonalBu = entityDB.PrequalificationPayments.Where(rec => rec.PrequalificationId == preQualificationId && rec.PaymentCategory == 1).Select(rec => rec.PrequalificationPaymentsId).ToList();
            ViewBag.paidAdditonalBu = entityDB.PrequalificationSites.Where(rec => paidAdditonalBu.Contains((long)rec.PrequalificationPaymentsId) && rec.PrequalificationId == preQualificationId).GroupBy(rec => rec.PrequalificationPaymentsId).ToList();// for paid additional bu's in view
            if (paidAdditonalBu.Count != ViewBag.paidAdditonalBu.Count)//paid in additional but move to one fee to show the payment in additional
            {
                var paidAdditionalPaymentIds = entityDB.PrequalificationSites.Where(rec => paidAdditonalBu.Contains((long)rec.PrequalificationPaymentsId) && rec.PrequalificationId == preQualificationId).Select(rec => rec.PrequalificationPaymentsId).Distinct().ToList();
                ViewBag.unusedPaymentids = entityDB.PrequalificationPayments.Where(rec => !paidAdditionalPaymentIds.Contains(rec.PrequalificationPaymentsId) && rec.PrequalificationId == preQualificationId && rec.PaymentCategory == 1).Distinct().ToList();
            }


            //ViewBag.additonalBu = entityDB.ClientTemplatesForBU.Where(rec => bunitIds.Contains(rec.ClientBusinessUnitId) && additionalFeeBu.Contains((long)rec.ClientTemplateBUGroupId)).ToList();
            //decimal? additionalPayment = entityDB.ClientTemplatesForBU.Where(rec => bunitIds.Contains(rec.ClientBusinessUnitId) && rec.ClientTemplateId == clientTemplateId && rec.ClientTemplatesBUGroupPrice.GroupPrice != 0 && rec.ClientTemplatesBUGroupPrice.GroupingFor == 0 && rec.ClientTemplatesBUGroupPrice.GroupPriceType!=0).Sum(rec => rec.ClientTemplatesBUGroupPrice.GroupPrice);
            //if (additionalPayment == null)
            //    additionalPayment = 0;
            //ViewBag.AdditonalFee = additionalPayment;
            //ViewBag.paidBuIds = entityDB.PrequalificationSites.Where(rec => rec.PrequalificationId == preQualificationId && rec.PrequalificationPaymentsId!=null).Select(rec=>rec.ClientBusinessUnitId).ToList();

            //var additionalBus = entityDB.ClientTemplatesForBU.Where(rec => preqsitebus.Contains(rec.ClientBusinessUnitId) && additionalFeeBu.Contains((long)rec.ClientTemplateBUGroupId)).Select(rec => rec.ClientBusinessUnitId).ToList();
            //ViewBag.paidBus = entityDB.PrequalificationSites.Where(rec => rec.PrequalificationId == preQualificationId && rec.PrequalificationPaymentsId != null && additionalBus.Contains(rec.ClientBusinessUnitId)).GroupBy(rec => rec.PrequalificationPaymentsId).ToList();
            ViewBag.unPaidBus = entityDB.ClientTemplatesForBU.Where(rec => preqsitebus.Contains(rec.ClientBusinessUnitId) && additionalFeeBu.Contains((long)rec.ClientTemplateBUGroupId)).ToList();//for unpaid bu's in view
            var additionalBus = entityDB.ClientTemplatesForBU.Where(rec => preqsitebus.Contains(rec.ClientBusinessUnitId) && additionalFeeBu.Contains((long)rec.ClientTemplateBUGroupId)).Select(rec => rec.ClientBusinessUnitId).ToList();
            ViewBag.paidBus = entityDB.PrequalificationSites.Where(rec => rec.PrequalificationId == preQualificationId && additionalBus.Contains(rec.ClientBusinessUnitId) && (rec.PrequalificationPaymentsId == null || rec.PrequalificationPayments.PaymentCategory == 0)).Select(rec => rec.ClientBusinessUnitId).Distinct().ToList();//To display one fee bu,to be paid in additional group

            decimal totalAdditionalfee = 0;
            foreach (var BusinessUnit in (List<ClientTemplatesForBU>)ViewBag.unPaidBus)
            {
                if (ViewBag.paidBus.Contains(BusinessUnit.ClientBusinessUnitId))
                {
                    totalAdditionalfee += (decimal)BusinessUnit.ClientTemplatesBUGroupPrice.GroupPrice;
                }
            }
            ViewBag.AdditonalFee = totalAdditionalfee;
            // Annual fee cal and list of bu's
            //var annualFeeTemplateBuIds = entityDB.PrequalificationTrainingAnnualFees.Where(rec => rec.PrequalificationId == preQualificationId).Select(rec => rec.ClientTemplateForBUId).ToList();
            var paidGroupIds = entityDB.PrequalificationTrainingAnnualFees.Where(rec => rec.PrequalificationId == preQualificationId).Select(rec => rec.ClientTemplateBUGroupId).ToList();
            //var annualFeeGroups = entityDB.ClientTemplatesBUGroupPrice.Where(rec => rec.ClientTemplateID == clientTemplateId && rec.GroupingFor == 1 && rec.GroupPrice != 0 && !paidGroupIds.Contains(rec.ClientTemplateBUGroupId)).Distinct().ToList();

            //var annualFeeGroupIds = annualFeeGroups.Select(rec => rec.ClientTemplateBUGroupId).Distinct().ToList();
            // Prequalification start date cal and calculating the difference of months on payment make time 
            // changed by sumanth on 30/06/2015 start
            int preqstartmonth = 0;
            int todaymonth = 0;

            var currentPrequalification = entityDB.Prequalification.FirstOrDefault(rec => rec.PrequalificationId == preQualificationId);
            long preqStatusid = currentPrequalification.PrequalificationStatusId;
            if (preqStatusid != 3 && preqStatusid != 15)
            {
                preqstartmonth = currentPrequalification.PrequalificationStart.Month;
                todaymonth = int.Parse(DateTime.Today.Month.ToString());
            }
            //sumanth on 30/06/2015 ends

            // Kiran on 08/26/2015
            if ((todaymonth < preqstartmonth) || (todaymonth == currentPrequalification.PrequalificationFinish.Month && DateTime.Now.Year == currentPrequalification.PrequalificationFinish.Year))
            {
                ViewBag.monthdiff = (currentPrequalification.PrequalificationFinish.Month - todaymonth) + 1;
            }
            else
            {
                ViewBag.monthdiff = 12 - (todaymonth - preqstartmonth);
            }
            // Ends<<<

            //var groupIds = annualFeeGroups.Select(record => record.ClientTemplateBUGroupId).ToList();
            //var finalBus = entityDB.ClientTemplatesForBU.Where(rec => groupIds.Contains((long)rec.ClientTemplateBUGroupId) && preqsitebus.Contains(rec.ClientBusinessUnitId) && !annualFeeTemplateBuIds.Contains(rec.ClientTemplateForBUId) && !annualFeeGroupIds.Contains((long)rec.ClientTemplateBUGroupId)).ToList();
            //ViewBag.preqsiteBus = entityDB.ClientTemplatesForBU.Where(rec => groupIds.Contains((long)rec.ClientTemplateBUGroupId) && preqsitebus.Contains(rec.ClientBusinessUnitId)).ToList(); //for hiding annual fee group if no preqsites
            //var ClientTemplatesBUGroupPrice = finalBus.Select(rec => rec.ClientTemplatesBUGroupPrice).ToList();
            //var localGroupPrice = ClientTemplatesBUGroupPrice.Distinct().ToList();
            var test = entityDB.ClientTemplatesForBU.Where(rec => preqsitebus.Contains(rec.ClientBusinessUnitId) && !paidGroupIds.Contains((long)rec.ClientTemplateBUGroupId) && rec.ClientTemplateId == clientTemplateId && rec.ClientTemplatesBUGroupPrice.GroupingFor == 1 && rec.ClientTemplatesBUGroupPrice.GroupPrice != 0).ToList();
            ViewBag.annualfeeBus = test.GroupBy(rec => rec.ClientTemplateBUGroupId).ToList();
            var paygroup = test.Select(rec => rec.ClientTemplatesBUGroupPrice).Distinct().ToList();
            ViewBag.annualPaidBus = entityDB.PrequalificationTrainingAnnualFees.Where(rec => rec.PrequalificationId == preQualificationId).GroupBy(rec => rec.ClientTemplateBUGroupId).ToList();
            //var preqBuGroupIds = entityDB.ClientTemplatesForBU.Where(rec => preqsitebus.Contains(rec.ClientBusinessUnitId)).Select(rec => rec.ClientTemplateBUGroupId).ToList();
            // var paygroup = entityDB.ClientTemplatesBUGroupPrice.Where(rec => preqBuGroupIds.Contains(rec.ClientTemplateBUGroupId) && annualFeeGroupIds.Contains(rec.ClientTemplateBUGroupId)).Distinct().ToList();
            //ViewBag.annualfeeBus = paygroup.Select(rec => rec.ClientTemplatesForBU).Distinct().ToList();//To show all the bu's
            // annual fee change by month changed by on 19/06/2015 by sumanth start
            decimal totalannualfee = 0;
            for (int i = 0; i < paygroup.Count; i++)
            {
                totalannualfee = (paygroup[i].GroupPrice.Value * ViewBag.monthdiff / 12) + totalannualfee;
            }
            ViewBag.totalAnnualFee = Math.Round(totalannualfee, 2, MidpointRounding.AwayFromZero);// to show annual bu amount
            // end
            ViewBag.VBprequalAnnualFeeGroupIds = entityDB.PrequalificationTrainingAnnualFees.Where(rec => rec.PrequalificationId == preQualificationId).Select(rec => rec.ClientTemplateBUGroupId).Distinct().ToList();
            ViewBag.VBprequalAnnualFeePaidGroups = entityDB.PrequalificationTrainingAnnualFees.Where(rec => rec.PrequalificationId == preQualificationId).Distinct().ToList();// for paid bu's  // Kiran on 06/24/2015 for payment issue after making payment
            // Ends<<<
            ViewBag.VBPrequalSitesBusinessUnits = preqsitebus;
            // prequalification price cal
            decimal price = 0;
            //var currentPrequalification = entityDB.Prequalification.Find(preQualificationId);
            if (currentPrequalification.TotalPaymentReceivedAmount == null)
            {
                var templatePrices = entityDB.ClientTemplatesBUGroupPrice.FirstOrDefault(rec => rec.ClientTemplateID == clientTemplateId && rec.GroupPriceType == 0 && rec.GroupingFor == 0).GroupPrice;
                if (templatePrices != null)
                    price = (decimal)templatePrices;
            }

            return price;
        }
        //////////////////////
        //////////////////////
        // Siva on 29th Aug //
        //////////////////////

        [SessionExpire(lastPageUrl = "../SystemUsers/Dashboard")]
        public ActionResult EmailModelTemplatesList(long prequalificationId)
        {
            ViewBag.prequalificationId = prequalificationId;
            return View(entityDB.EmailTemplates.Where(m => m.IsDocumentSpecific == false && m.EmailUsedFor == 0).OrderBy(m => m.Name).ToList());
        }

        [SessionExpireForView]
        public ActionResult EmailCompose(int templateId, long prequalificationId)
        {
            var vendorSuperUserEmail = "";
            EmailTemplates template = entityDB.EmailTemplates.Find(templateId);

            var prequalification = entityDB.Prequalification.Find(prequalificationId);
            // Kiran on 9/5/2014
            var selectedPrequalificationVendorSuperUserEmail = "";
            try
            {
                selectedPrequalificationVendorSuperUserEmail = entityDB.SystemUsersOrganizations.FirstOrDefault(record => record.OrganizationId == prequalification.VendorId && record.PrimaryUser == true).SystemUsers.Email;
            }
            catch (Exception e) { }
            //var vendorSysUserOrgs = prequalification.Vendor.SystemUsersOrganizations;
            //foreach (var vendorUserOrg in vendorSysUserOrgs)
            //{
            //    var vendorOrgRoles = vendorUserOrg.SystemUsersOrganizationsRoles;
            //    foreach (var vendorOrgRole in vendorOrgRoles)
            //    {
            //        if (vendorOrgRole.SystemRoles.RoleName.Equals("Super User"))
            //        {
            //            vendorSuperUserEmail = vendorUserOrg.SystemUsers.Email;
            //        }
            //    }
            //}

            // Ends<<<
            var clientEmail = "";
            var clientSysUserOrgs = prequalification.Client.SystemUsersOrganizations;
            foreach (var clientUserOrg in clientSysUserOrgs)
            {
                var clientOrgRoles = clientUserOrg.SystemUsersOrganizationsRoles;
                foreach (var clientOrgRole in clientOrgRoles)
                {
                    if (clientOrgRole.SystemRoles.RoleName.Equals("Super User"))
                    {
                        clientEmail = clientUserOrg.SystemUsers.Email;
                    }
                }
            }

            // Mani on 7/23/2015 for fv-171
            //selectedPrequalificationVendorSuperUserEmail = selectedPrequalificationVendorSuperUserEmail + ",";

            //foreach (var vendorContacts in entityDB.VendorDetails.Where(rec => rec.PrequalificationId == prequalificationId).Select(rec => rec.EmailAddress).ToList())
            //{
            //    selectedPrequalificationVendorSuperUserEmail += vendorContacts + ",";
            //}
            //selectedPrequalificationVendorSuperUserEmail = selectedPrequalificationVendorSuperUserEmail.Substring(0, selectedPrequalificationVendorSuperUserEmail.Length - 1);
            // change by sumanth on 10/06/2015 for SMI-239 starts <<
            try
            {
                var vendorDetailsContacts = entityDB.VendorDetails.Where(rec => rec.PrequalificationId == prequalification.PrequalificationId && rec.EmailAddress != null).Select(rec => rec.EmailAddress).ToList();
                if (vendorDetailsContacts.Count() != 0)
                {
                    foreach (var contact in vendorDetailsContacts)
                    {
                        var Toaddress = ""; ;
                        try
                        {
                            Toaddress = selectedPrequalificationVendorSuperUserEmail.ToString();
                        }
                        catch { Toaddress = ""; }
                        if ((!selectedPrequalificationVendorSuperUserEmail.Contains(contact) && !Toaddress.ToString().Equals(contact)) || Toaddress.ToString().Equals(""))
                        {
                            if (selectedPrequalificationVendorSuperUserEmail == "")
                                selectedPrequalificationVendorSuperUserEmail = contact;
                            else if (contact != null)
                                selectedPrequalificationVendorSuperUserEmail = selectedPrequalificationVendorSuperUserEmail + "," + contact;
                            else
                                selectedPrequalificationVendorSuperUserEmail = selectedPrequalificationVendorSuperUserEmail + contact + ",";
                        }
                    }

                }

            }
            catch
            { }
            // sumanth ends on 10/06/2015 <<<
            // Ends >>>

            LocalEmailComposeModel newEmailForm = new LocalEmailComposeModel();
            newEmailForm.ToAddress = selectedPrequalificationVendorSuperUserEmail; // Kiran on 9/5/2014
            //newEmailForm.CCAddress = cc;
            newEmailForm.Subject = template.EmailSubject;
            newEmailForm.Body = template.EmailBody;
            newEmailForm.EmailComment = template.CommentText;

            // Kiran on 8/26/2014
            newEmailForm.SendCopyToMySelf = true;
            var loggedInUserId = (Guid)Session["UserId"];
            newEmailForm.BCCAddress = entityDB.SystemUsers.FirstOrDefault(rec => rec.UserId == loggedInUserId).Email;
            // Ends<<

            newEmailForm.Subject = newEmailForm.Subject.Replace("[Date]", DateTime.Now + "");
            newEmailForm.Subject = newEmailForm.Subject.Replace("[VendorName]", prequalification.Vendor.Name);
            newEmailForm.Subject = newEmailForm.Subject.Replace("[ClientName]", prequalification.Client.Name);
            newEmailForm.Subject = newEmailForm.Subject.Replace("[CustomerServicePhone]", prequalification.Client.PhoneNumber);
            newEmailForm.Subject = newEmailForm.Subject.Replace("[CustomerServiceEmail]", clientEmail);


            newEmailForm.Body = newEmailForm.Body.Replace("[Date]", DateTime.Now + "");
            newEmailForm.Body = newEmailForm.Body.Replace("[VendorName]", prequalification.Vendor.Name);
            newEmailForm.Body = newEmailForm.Body.Replace("[ClientName]", prequalification.Client.Name);
            newEmailForm.Body = newEmailForm.Body.Replace("[CustomerServicePhone]", prequalification.Client.PhoneNumber);
            newEmailForm.Body = newEmailForm.Body.Replace("[CustomerServiceEmail]", clientEmail);

            //Siva on 2nd Apr 2014
            ViewBag.errorMessage = "";

            //Rajesh on 12/6/2014
            var AttachmentsList = (from attachment in template.EmailTemplateAttachments.ToList()
                                   select new LocalAttachmetList
                                   {
                                       AttachmentName = attachment.FileName,
                                       AttachmentId = attachment.AttachmentId,
                                       isActive = 1
                                   }).ToList();
            newEmailForm.AttachmentsList = AttachmentsList;
            //Ends<<<
            return View(newEmailForm);
        }

        [SessionExpireForView]
        [HttpPost]
        public ActionResult EmailCompose(LocalEmailComposeModel form, List<HttpPostedFileBase> attachments)
        {
            string bccMail = "";
            string ccMail = "";
            string toAddress = ""; // Kiran on 12/6/2014

            var bodyString = HttpUtility.HtmlDecode(form.Body);
            if (form.SendCopyToMySelf)
            {
                var userId = new Guid(Session["UserId"].ToString());
                var sysUser = entityDB.SystemUsers.Find(userId);
                //bccMail = sysUser.Email;
            }

            // Kiran on 9/15/2014
            if (!string.IsNullOrEmpty(form.CCAddress))
            {
                if (!form.CCAddress.Trim().Equals("")) //observe the !
                {
                    ccMail = form.CCAddress;
                    string ccEmails = "";
                    foreach (var email in ccMail.Split(','))
                    {
                        if (email == "")
                        {
                            ccEmails = ccEmails + email.Replace(",", "");
                        }
                        else
                        {
                            ccEmails = ccEmails + email.Replace(",", "") + ",";
                        }
                    }
                    ccMail = ccEmails.Substring(0, ccEmails.Length - 1);
                }
            }
            // Ends<<<

            // Kiran on 12/6/2014
            if (!string.IsNullOrEmpty(form.ToAddress))
            {
                if (!form.ToAddress.Trim().Equals("")) //observe the !
                {
                    toAddress = form.ToAddress;
                    string toAddressEmails = "";
                    foreach (var email in toAddress.Split(','))
                    {
                        if (email == "")
                        {
                            toAddressEmails = toAddressEmails + email.Replace(",", "");
                        }
                        else
                        {
                            toAddressEmails = toAddressEmails + email.Replace(",", "") + ",";
                        }
                    }
                    toAddress = toAddressEmails.Substring(0, toAddressEmails.Length - 1);
                }
            }
            // Ends<<<

            List<Attachment> mailAttachments = new List<Attachment>();
            if (attachments != null && attachments.Count() > 0)
            {
                foreach (var file in attachments)
                {
                    if (file != null)
                    {
                        Attachment messageAttachment = new Attachment(file.InputStream, Path.GetFileName(file.FileName));
                        mailAttachments.Add(messageAttachment);
                    }
                }
            }
            if (form.AttachmentsList != null)
                foreach (var attachment in form.AttachmentsList.Where(rec => rec.isActive == 1))
                {
                    var locAttach = entityDB.EmailTemplateAttachments.Find(attachment.AttachmentId);
                    var path = Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["EmailTemplateAttachmentsPath"]), locAttach.TemplateID + "" + locAttach.AttachmentId + locAttach.FileName);
                    var f = new FileStream(path, FileMode.Open, FileAccess.Read);
                    if (System.IO.File.Exists(path))
                    {
                        Attachment messageAttachment = new Attachment(f, locAttach.FileName);
                        messageAttachment.Name = locAttach.FileName;
                        mailAttachments.Add(messageAttachment);
                    }
                }
            //body
            var formattedBody = bodyString.Replace("\r", "");
            //formattedBody = formattedBody.Replace("\n", "<br/>");
            string emailBody = System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Views/Prequalification/PrequalEmailFormat.htm"));
            emailBody = emailBody.Replace("ReplaceString", formattedBody);


            var emailStatus = SendMail.sendMail(form.ToAddress, emailBody, form.Subject, form.BCCAddress, ccMail, mailAttachments);

            //Siva on Apr 7th
            if (emailStatus.Equals("Success"))
            {
                long notificationId = 0;
                Prequalification prequal = entityDB.Prequalification.Find(form.PrequalificationId);
                List<OrganizationsNotificationsSent> orgNotesSent = entityDB.OrganizationsNotificationsSent.Where(rec => rec.ClientId == prequal.ClientId && rec.SetupType == 2).ToList();
                if (orgNotesSent.Count == 0)
                {
                    OrganizationsNotificationsSent newOrgNoteSent = new OrganizationsNotificationsSent();
                    newOrgNoteSent.ClientId = prequal.ClientId;
                    newOrgNoteSent.SetupType = 2;
                    newOrgNoteSent.SetupTypeId = null;
                    entityDB.OrganizationsNotificationsSent.Add(newOrgNoteSent);
                    entityDB.SaveChanges();

                    notificationId = newOrgNoteSent.NotificationId;
                }
                else
                {
                    notificationId = orgNotesSent[0].NotificationId;
                }

                var MaxNotificationNo = 0;
                try
                {
                    MaxNotificationNo = entityDB.OrganizationsNotificationsEmailLogs.Where(record => record.PrequalificationId == prequal.PrequalificationId && record.NotificationType == 1).Max(record => record.NotificationNo);
                }
                catch (Exception e) { }

                OrganizationsNotificationsEmailLogs notifiLogs = new OrganizationsNotificationsEmailLogs();
                notifiLogs.NotificationId = notificationId;
                notifiLogs.NotificationType = 0; // Kiran on 9/3/2014
                notifiLogs.NotificationNo = MaxNotificationNo + 1;
                notifiLogs.VendorId = prequal.VendorId;
                notifiLogs.PrequalificationId = prequal.PrequalificationId;
                notifiLogs.EmailSentDateTime = DateTime.Now;
                notifiLogs.MailSentBy = (Guid)Session["UserId"];
                notifiLogs.ReadByAdmin = 0;
                notifiLogs.ReadByClient = 0;
                notifiLogs.ReadByVendor = 0;
                notifiLogs.EmailTemplateId = form.TemplateId;
                notifiLogs.EmailSentTo = form.ToAddress;
                if (form.EmailComment != null && form.EmailComment.Trim() != "")
                    notifiLogs.Comments = form.EmailComment;
                entityDB.OrganizationsNotificationsEmailLogs.Add(notifiLogs);

                entityDB.SaveChanges();

                return View("EmailSendingSuccesPage");
            }
            else
            {
                ViewBag.errorMessage = "Mail Sending Failed";
                return View(form);
            }
            //End of Siva on Apr 7th
        }

        [HttpPost]
        public ActionResult GetTemplatesClients(Guid ClientTemplateId, long preQualificationId)
        {
            var vendorId = entityDB.Prequalification.Find(preQualificationId).VendorId;
            var templateId = entityDB.ClientTemplates.Find(ClientTemplateId).TemplateID;
            var clientTemplates = entityDB.ClientTemplates.ToList().Where(rec => rec.TemplateID == templateId);


            List<SelectListItem> data = new List<SelectListItem>();
            foreach (var clientTemplate in clientTemplates.ToList())
            {
                foreach (var Pre in clientTemplate.Prequalification.Where(rec => rec.VendorId == vendorId && rec.PrequalificationStatusId >= 5))//To get all clients with present template and prequalification status >5
                {
                    SelectListItem item = new SelectListItem { Text = Pre.Client.Name, Value = Pre.ClientId + "" };
                    data.Add(item);
                }
            }

            return Json(data);

        }
        [HttpPost]
        public string ChangeDocStatus(Guid DocId, string status)
        {
            Document d = entityDB.Document.Find(DocId);
            // sumanth on 10/20/2015 for FV-220 starts
            d.StatusChangedBy = (Guid)Session["UserId"]; // sumanth on 10/20/2015 for FV-220 to display the user details that changed status of document
            d.StatusChangeDate = DateTime.Now; // sumanth on 10/20/2015 FV-220 to display the user details that changed status of document
            try
            {

                ViewBag.VBcontactDetails = " by " + entityDB.Contact.FirstOrDefault(rec => rec.SystemUsers.UserId == d.StatusChangedBy).LastName + " " + entityDB.Contact.FirstOrDefault(rec => rec.SystemUsers.UserId == d.StatusChangedBy).FirstName;
            }
            catch { ViewBag.VBcontactDetails = ""; }
            // sumanth ends on 10/20/2015
            d.DocumentStatus = status;
            d.ShowOnDashboard = true;//  sumanth on 10/08/2015 for SMI-226 to display document on dashboard when the document is updated
            entityDB.Entry(d).State = EntityState.Modified;
            entityDB.SaveChanges();
            return ViewBag.VBcontactDetails; // sumanth on 10/20/2015 for FV-220
        }
        [HttpPost]
        public ActionResult GetClientDocuments(long clientId, long preQualificationId, Guid ClientTemplateId)
        {
            var templateSupportedDocTypes = entityDB.ClientTemplates.Find(ClientTemplateId).ClientTemplateReportingData.Where(rec => rec.ReportingType == 2).Select(rec => rec.ReportingTypeId);

            var preQualification = entityDB.Prequalification.Find(preQualificationId);
            var vendorId = preQualification.VendorId;
            var PreId = entityDB.Prequalification.FirstOrDefault(rec => rec.ClientId == clientId && rec.VendorId == vendorId).PrequalificationId;

            List<SelectListItem> data = new List<SelectListItem>();
            foreach (var docs in entityDB.Document.Where(rec => rec.ReferenceType == "PreQualification" && rec.ReferenceRecordID == PreId && templateSupportedDocTypes.Contains(rec.DocumentTypeId)).ToList())
            {
                SelectListItem item = new SelectListItem { Text = docs.DocumentName.Replace(".pdf", ""), Value = docs.DocumentId + "" };
                data.Add(item);
            }
            return Json(data);
        }

        // Kiran on 12/14/2013
        //public ActionResult PostToPaypal(string TemplatePrice, string Client, string Vendor, long VendorId, Guid templateId, long TemplateSectionID, long PreQualificationId, int? mode)
        //{
        //    LocalPaypal paypal = new LocalPaypal();
        //    paypal.cmd = "_xclick";
        //    paypal.business = ConfigurationManager.AppSettings["BusinessAccountKey"];
        //    bool useSandbox = Convert.ToBoolean(ConfigurationManager.AppSettings["UseSandbox"]);
        //    if (useSandbox)
        //        ViewBag.actionURL = "https://www.sandbox.paypal.com/cgi-bin/webscr";
        //    else
        //        ViewBag.actionURL = "https://www.paypal.com/cgi-bin/webscr";

        //    paypal.cancel_return = ConfigurationManager.AppSettings["CancelURL"].ToString() + templateId + "&TemplateSectionID=" + TemplateSectionID + "&PreQualificationId=" + PreQualificationId + "&mode=" + mode;
        //    paypal.@return = ConfigurationManager.AppSettings["ReturnURL"].ToString() + templateId + "&TemplateSectionID=" + TemplateSectionID + "&PreQualificationId=" + PreQualificationId + "&mode=" + mode + "&paymentAmt=" + TemplatePrice; //Kiran 3/17/2014 
        //    paypal.notify_url = ConfigurationManager.AppSettings["NotifyURL"].ToString();
        //    paypal.currency_code = ConfigurationManager.AppSettings["CurrencyCode"];

        //    paypal.item_name = HttpUtility.HtmlDecode(HttpUtility.UrlDecode(Client + " Vendor Prequalification: " + Vendor + " (" + PreQualificationId + ")"));
        //    paypal.amount = TemplatePrice;
        //    paypal.preqID = PreQualificationId; // Kiran on 7/31/2014
        //    return View(paypal);
        //}
        // Ends<<<
        // Mani on 8/18/2015
        public ActionResult PostToPaypal(string TemplatePrice, string Client, string Vendor, long VendorId, Guid templateId, long TemplateSectionID, long PreQualificationId, int? mode, string skipType, string totalAmount)
        {
            cntr = 0;
            var currentPrequalification = entityDB.Prequalification.Find(PreQualificationId);
            LocalPaypal paypal = new LocalPaypal();
            paypal.cmd = "_xclick";
            paypal.business = ConfigurationManager.AppSettings["BusinessAccountKey"];
            bool useSandbox = Convert.ToBoolean(ConfigurationManager.AppSettings["UseSandbox"]);
            if (useSandbox)
                ViewBag.actionURL = "https://www.sandbox.paypal.com/cgi-bin/webscr";
            else
                ViewBag.actionURL = "https://www.paypal.com/cgi-bin/webscr";


            // additional payment cal from db
            var preqBunitIds = entityDB.PrequalificationSites.Where(rec => rec.PrequalificationId == PreQualificationId && (rec.PrequalificationPaymentsId == null || rec.PrequalificationPayments.PaymentCategory == 0)).Select(rec => rec.ClientBusinessUnitId).ToList();
            preqBunitIds = preqBunitIds.Distinct().ToList();
            var clientTemplatesforBu = entityDB.ClientTemplatesForBU.Where(rec => preqBunitIds.Contains(rec.ClientBusinessUnitId) && rec.ClientTemplateId == currentPrequalification.ClientTemplateId && rec.ClientTemplatesBUGroupPrice.GroupPrice != 0 && rec.ClientTemplatesBUGroupPrice.GroupingFor == 0 && rec.ClientTemplatesBUGroupPrice.GroupPriceType != 0).ToList();
            decimal? additionalPayment = clientTemplatesforBu.Sum(rec => rec.ClientTemplatesBUGroupPrice.GroupPrice);
            if (additionalPayment == null)
                additionalPayment = 0;
            var additionBuIds = "";

            foreach (var templateBu in clientTemplatesforBu)
            {
                additionBuIds += templateBu.ClientBusinessUnitId.ToString() + ",";
            }
            if (additionBuIds != "")
                additionBuIds = additionBuIds.Substring(0, additionBuIds.Length - 1);
            // Annual payment cal from db //changed by sumanth on 23/06/2015 starts
            var annualFeeGroups = entityDB.ClientTemplatesBUGroupPrice.Where(rec => rec.ClientTemplateID == currentPrequalification.ClientTemplateId && rec.GroupingFor == 1 && rec.GroupPrice != 0).ToList();
            ViewBag.annualfeeBus = annualFeeGroups.Select(rec => rec.ClientTemplatesForBU).ToList();
            var allPreqSites = entityDB.PrequalificationSites.Where(rec => rec.PrequalificationId == PreQualificationId).Select(rec => rec.ClientBusinessUnitId).ToList();

            var groupIds = annualFeeGroups.Select(record => record.ClientTemplateBUGroupId).ToList();
            var annualFeeTemplateBuIds = entityDB.PrequalificationTrainingAnnualFees.Where(rec => rec.PrequalificationId == PreQualificationId).Select(rec => rec.ClientTemplateForBUId).ToList();
            var annualfeeGroupIds = entityDB.PrequalificationTrainingAnnualFees.Where(rec => rec.PrequalificationId == PreQualificationId).Select(rec => rec.ClientTemplateBUGroupId).Distinct().ToList();
            var clienttemplatesBuGroups = entityDB.ClientTemplatesForBU.Where(rec => groupIds.Contains((long)rec.ClientTemplateBUGroupId) && rec.ClientTemplateId == currentPrequalification.ClientTemplateId && allPreqSites.Contains(rec.ClientBusinessUnitId) && !annualfeeGroupIds.Contains((long)rec.ClientTemplateBUGroupId) && rec.ClientTemplatesBUGroupPrice.GroupingFor == 1).Select(rec => rec.ClientTemplatesBUGroupPrice).ToList();
            var distinctGroups = clienttemplatesBuGroups.Distinct().ToList();
            //changed by sumanth on 30/06/2015
            int preqstartmonth = 0;
            int todaymonth = 0;
            long preqStatusid = currentPrequalification.PrequalificationStatusId;
            if (preqStatusid != 3 && preqStatusid != 15)
            {
                preqstartmonth = currentPrequalification.PrequalificationStart.Month;
                todaymonth = int.Parse(DateTime.Today.Month.ToString());
            }
            // Kiran on 08/26/2015
            if ((todaymonth < preqstartmonth) || (todaymonth == currentPrequalification.PrequalificationFinish.Month && DateTime.Now.Year == currentPrequalification.PrequalificationFinish.Year))
            {
                ViewBag.monthdiff = (currentPrequalification.PrequalificationFinish.Month - todaymonth) + 1;
            }
            else
            {
                ViewBag.monthdiff = 12 - (todaymonth - preqstartmonth);
            }
            // Ends<<<
            decimal totalannualfee = 0;

            for (int i = 0; i < distinctGroups.Count; i++)
            {
                totalannualfee = (distinctGroups[i].GroupPrice.Value * ViewBag.monthdiff / 12) + totalannualfee;

            }
            ViewBag.totalAnnualFee = Math.Round(totalannualfee, 2, MidpointRounding.AwayFromZero);
            var AnnualFee = ViewBag.totalAnnualFee;
            // end >>
            var AnnualClientTemplatesBuIds = entityDB.ClientTemplatesForBU.Where(rec => rec.ClientTemplatesBUGroupPrice.GroupingFor == 1 && rec.ClientTemplatesBUGroupPrice.GroupPrice != 0 && rec.ClientTemplateId == currentPrequalification.ClientTemplateId && allPreqSites.Contains(rec.ClientBusinessUnitId) && !annualfeeGroupIds.Contains((long)rec.ClientTemplateBUGroupId)).Distinct().ToList();

            var annualTemplateBuIds = "";
            foreach (var templateBu in AnnualClientTemplatesBuIds)
            {
                annualTemplateBuIds += templateBu.ClientTemplateForBUId.ToString() + ",";
            }
            if (annualTemplateBuIds != "")
                annualTemplateBuIds = annualTemplateBuIds.Substring(0, annualTemplateBuIds.Length - 1);
            // Preq payment cal from db
            decimal tempPrice = 0;
            //if (currentPrequalification.TotalPaymentReceivedAmount == null )
            //{
            //    decimal? templatePrices = entityDB.ClientTemplatesBUGroupPrice.FirstOrDefault(rec => rec.ClientTemplateID == currentPrequalification.ClientTemplateId && rec.GroupPriceType == 0 && rec.GroupingFor == 0).GroupPrice;
            //    if (templatePrices != null)
            //        tempPrice = (decimal)templatePrices;
            //}
            tempPrice = getTemplatePrice(VendorId, currentPrequalification.ClientTemplates.ClientTemplateID, PreQualificationId);
            // Ends >>
            // updating the 0 fee group bu's with prequalification payment.
            var bu0GroupIds = entityDB.ClientTemplatesBUGroupPrice.Where(rec => rec.ClientTemplateID == currentPrequalification.ClientTemplateId && rec.GroupingFor == 0 && rec.GroupPrice != 0 && rec.GroupPriceType == 0).Select(rec => rec.ClientTemplateBUGroupId).ToList();
            var Group0BuIds = entityDB.ClientTemplatesForBU.Where(rec => bu0GroupIds.Contains((long)rec.ClientTemplateBUGroupId)).Select(rec => rec.ClientBusinessUnitId).ToList();
            var UnPaymentSites0group = entityDB.PrequalificationSites.Where(rec => Group0BuIds.Contains(rec.ClientBusinessUnitId) && rec.PrequalificationId == PreQualificationId && (rec.PrequalificationPaymentsId == null || rec.PrequalificationPayments.PaymentCategory == 1)).ToList();
            if (!skipType.Contains('1') && currentPrequalification.PaymentReceived == true && UnPaymentSites0group.Count != 0)
            {
                foreach (var site in UnPaymentSites0group)
                {
                    var paymentRec = entityDB.PrequalificationPayments.FirstOrDefault(rec => rec.PrequalificationId == PreQualificationId && rec.PaymentCategory == 0);
                    if (paymentRec != null)
                    {
                        site.PrequalificationPaymentsId = entityDB.PrequalificationPayments.FirstOrDefault(rec => rec.PrequalificationId == PreQualificationId && rec.PaymentCategory == 0).PrequalificationPaymentsId;
                        entityDB.Entry(site).State = EntityState.Modified;
                        entityDB.SaveChanges();
                    }
                }
            }
            //Ends >>>
            var finalstring = "";
            if (skipType.Contains('1'))
            {
                PrequalificationPayments skipPayment = entityDB.PrequalificationPayments.FirstOrDefault(rec => rec.PaymentCategory == 0 && rec.PrequalificationId == PreQualificationId);
                if (skipPayment == null)
                {
                    skipPayment = new PrequalificationPayments();
                    skipPayment.SkipPayment = true;
                    skipPayment.PrequalificationId = PreQualificationId;
                    skipPayment.PaymentReceivedDate = DateTime.Now;
                    skipPayment.PaymentCategory = 0;
                    skipPayment.PaymentReceivedAmount = tempPrice;
                    skipPayment.InvoiceAmount = entityDB.ClientTemplatesBUGroupPrice.FirstOrDefault(rec => rec.ClientTemplateID == currentPrequalification.ClientTemplateId && rec.GroupPriceType == 0 && rec.GroupingFor == 0).GroupPrice;
                    entityDB.PrequalificationPayments.Add(skipPayment);


                    currentPrequalification.TotalPaymentReceivedAmount = tempPrice; // Kiran on 02/24/2015
                    //prequalification.PaymentReceivedDate = DateTime.Now;                                                                                                                                                                  

                }
                else
                {
                    currentPrequalification.TotalPaymentReceivedAmount += tempPrice;
                    entityDB.Entry(skipPayment).State = EntityState.Modified;
                }

                // for 0 group bu's update in preq.sites
                foreach (var site in UnPaymentSites0group)
                {
                    site.PrequalificationPaymentsId = skipPayment.PrequalificationPaymentsId;
                    entityDB.Entry(site).State = EntityState.Modified;
                    entityDB.SaveChanges();
                }
                //Ends>>
                currentPrequalification.PaymentReceived = true;//In Here we are doing skip payment
                entityDB.Entry(currentPrequalification).State = EntityState.Modified;
                entityDB.SaveChanges();

                tempPrice = 0;
            }
            if (skipType.Contains('2'))
            {

                PrequalificationPayments skipPayment = new PrequalificationPayments();
                skipPayment.SkipPayment = true;
                skipPayment.PrequalificationId = PreQualificationId;
                skipPayment.PaymentReceivedDate = DateTime.Now;
                skipPayment.PaymentCategory = 1;
                skipPayment.InvoiceAmount = additionalPayment;
                skipPayment.PaymentReceivedAmount = additionalPayment;
                entityDB.PrequalificationPayments.Add(skipPayment);
                entityDB.SaveChanges();

                // Adding templatebu's to sites and update payments for additional bu's
                var buGroupIds = entityDB.ClientTemplatesBUGroupPrice.Where(rec => rec.ClientTemplateID == currentPrequalification.ClientTemplateId && rec.GroupingFor == 0 && rec.GroupPrice != 0 && rec.GroupPriceType != 0).Select(rec => rec.ClientTemplateBUGroupId).ToList();
                var buIds = entityDB.ClientTemplatesForBU.Where(rec => buGroupIds.Contains((long)rec.ClientTemplateBUGroupId)).Select(rec => rec.ClientBusinessUnitId).ToList();
                var UnPaymentSites = entityDB.PrequalificationSites.Where(rec => buIds.Contains(rec.ClientBusinessUnitId) && rec.PrequalificationId == PreQualificationId && (rec.PrequalificationPaymentsId == null || rec.PrequalificationPayments.PaymentCategory == 0)).ToList();

                foreach (var site in UnPaymentSites)
                {
                    site.PrequalificationPaymentsId = skipPayment.PrequalificationPaymentsId;
                    entityDB.Entry(site).State = EntityState.Modified;
                    entityDB.SaveChanges();
                }
                // Modification in prequalifcation table
                var previousAmount = currentPrequalification.TotalPaymentReceivedAmount;
                currentPrequalification.TotalPaymentReceivedAmount = previousAmount + additionalPayment; //Adding additional amount to the preq table
                currentPrequalification.PaymentReceived = true;//In Here we are doing skip payment
                entityDB.Entry(currentPrequalification).State = EntityState.Modified;
                entityDB.SaveChanges();
                additionalPayment = 0;// for skip payment make the cal fee as 0
            }
            if (skipType.Contains('3'))
            {

                foreach (var TemplateBu in AnnualClientTemplatesBuIds)
                {
                    // Sumanth on 06/24/2015
                    var clientTemplateBUGroupPrice = entityDB.ClientTemplatesBUGroupPrice.Find(TemplateBu.ClientTemplateBUGroupId).GroupPrice;
                    decimal preqannualfee = Math.Round(clientTemplateBUGroupPrice * ViewBag.monthdiff / 12, 2, MidpointRounding.AwayFromZero);
                    // Ends<<<

                    PrequalificationTrainingAnnualFees annualFee = new PrequalificationTrainingAnnualFees();
                    annualFee.PrequalificationId = PreQualificationId;
                    annualFee.SkipPayment = true;
                    annualFee.ClientTemplateBUGroupId = (long)TemplateBu.ClientTemplateBUGroupId;
                    annualFee.ClientTemplateForBUId = TemplateBu.ClientTemplateForBUId;
                    annualFee.FeeReceivedDate = DateTime.Now;

                    if (entityDB.PrequalificationTrainingAnnualFees.FirstOrDefault(rec => rec.PrequalificationId == PreQualificationId && rec.ClientTemplateBUGroupId == TemplateBu.ClientTemplateBUGroupId) == null)
                    {
                        annualFee.AnnualFeeAmount = preqannualfee;
                        annualFee.AnnualFeeReceivedAmount = preqannualfee;
                    }
                    else
                    {
                        annualFee.AnnualFeeAmount = 0;
                        annualFee.AnnualFeeReceivedAmount = 0;
                    }
                    entityDB.PrequalificationTrainingAnnualFees.Add(annualFee);
                    entityDB.SaveChanges();
                }
                //currentPrequalification.TotalAnnualFeeAmount += AnnualFee;
                if (currentPrequalification.TotalAnnualFeeReceived == null)
                    currentPrequalification.TotalAnnualFeeReceived = AnnualFee;
                else
                    currentPrequalification.TotalAnnualFeeReceived += AnnualFee;
                entityDB.Entry(currentPrequalification).State = EntityState.Modified;
                entityDB.SaveChanges();
                AnnualFee = 0;
            }
            decimal? totalPayment = tempPrice + additionalPayment + AnnualFee;
            decimal amountToPay = 0;
            var stringAmount = totalAmount.TrimStart('$');
            amountToPay = Convert.ToDecimal(stringAmount);
            var paymentSection = entityDB.PrequalificationCompletedSections.FirstOrDefault(rec => rec.PrequalificationId == PreQualificationId && rec.TemplateSections.TemplateSectionType == 3);
            if (amountToPay == 0)
            {
                if (paymentSection == null)
                {
                    PrequalificationCompletedSections section = new PrequalificationCompletedSections();
                    section.PrequalificationId = PreQualificationId;
                    section.TemplateSectionId = TemplateSectionID;
                    entityDB.PrequalificationCompletedSections.Add(section);
                }
                //sandhya on 08/04/2015
                if (currentPrequalification.TotalPaymentReceivedAmount == null)
                {
                    currentPrequalification.TotalPaymentReceivedAmount = tempPrice;
                    currentPrequalification.PaymentReceived = true;
                    entityDB.Entry(currentPrequalification).State = EntityState.Modified;
                }
                //for 500^ having to pay is 0
                PrequalificationPayments paymentLog = entityDB.PrequalificationPayments.FirstOrDefault(rec => rec.PaymentCategory == 0 && rec.PrequalificationId == PreQualificationId);
                if (paymentLog == null)
                {
                    paymentLog = new PrequalificationPayments();
                    paymentLog.PrequalificationId = PreQualificationId;
                    paymentLog.PaymentReceivedDate = DateTime.Now;
                    paymentLog.PaymentCategory = 0;
                    paymentLog.PaymentReceivedAmount = tempPrice;
                    paymentLog.InvoiceAmount = entityDB.ClientTemplatesBUGroupPrice.FirstOrDefault(rec => rec.ClientTemplateID == currentPrequalification.ClientTemplateId && rec.GroupPriceType == 0 && rec.GroupingFor == 0).GroupPrice;
                    entityDB.PrequalificationPayments.Add(paymentLog);
                }
                //Ends>>
                entityDB.SaveChanges();

                //var displayOrder = entityDB.TemplateSections.FirstOrDefault(rec => rec.TemplateID == templateId && rec.TemplateSectionID == TemplateSectionID).DisplayOrder;
                //var nextSection = entityDB.TemplateSections.FirstOrDefault(rec => rec.TemplateID == templateId && rec.DisplayOrder == displayOrder + 1).TemplateSectionID;
                //var nextSection = entityDB.TemplateSections.FirstOrDefault(rec => rec.TemplateID == templateId && rec.DisplayOrder > displayOrder + 1 && rec.Visible == true).TemplateSectionID;
                var nextSection = nextSectionId(PreQualificationId, TemplateSectionID);
                String strNextSectionURL = "../Prequalification/TemplateSectionsList?templateId=" + templateId + "&TemplateSectionID=" + nextSection + "&PreQualificationId=" + PreQualificationId;
                return Redirect(strNextSectionURL);
            }
            var discountPaymentRec = entityDB.PrequalificationPayments.FirstOrDefault(rec => rec.PaymentCategory == 0 && rec.PrequalificationId == PreQualificationId);
            paypal.custom = "";
            if ((!skipType.Contains('1') && tempPrice != 0) || (discountPaymentRec == null && tempPrice == 0))
                paypal.custom += "preq:" + tempPrice + "|";

            if (!skipType.Contains('2') && additionalPayment != 0 && additionBuIds != "")
                paypal.custom += "additional:" + additionBuIds + ";" + additionalPayment + "|";

            if (!skipType.Contains('3') && AnnualFee != 0 && annualTemplateBuIds != "")
                paypal.custom += "annual:" + annualTemplateBuIds + ";" + AnnualFee;
            var paymentString = paypal.custom;

            paypal.cancel_return = ConfigurationManager.AppSettings["CancelURL"].ToString() + templateId + "&TemplateSectionID=" + TemplateSectionID + "&PreQualificationId=" + PreQualificationId + "&mode=" + mode;
            paypal.@return = ConfigurationManager.AppSettings["ReturnURL"].ToString() + templateId + "&TemplateSectionID=" + TemplateSectionID + "&PreQualificationId=" + PreQualificationId + "&mode=" + mode + "&paymentAmt=" + tempPrice + "&payString=" + paymentString; //to give the access for next sections, passing only preq Template price(0 group)
            paypal.notify_url = ConfigurationManager.AppSettings["NotifyURL"].ToString();
            paypal.currency_code = ConfigurationManager.AppSettings["CurrencyCode"];

            paypal.item_name = HttpUtility.HtmlDecode(HttpUtility.UrlDecode(Client + " Total Payment: " + Vendor + " (" + PreQualificationId + ")"));
            paypal.amount = totalPayment.ToString();

            paypal.preqID = PreQualificationId; // Kiran on 7/31/2014




            //paypal.custom = "preq:" + tempPrice + "|additional:" + additionBuIds + ";" + additionalPayment + "|annual:" + annualBuIds + ";" + AnnualFee;
            return View(paypal);

        }

        private long nextSectionId(long prequalificationId, long currentSectionId)
        {
            var prequalification = entityDB.Prequalification.Find(prequalificationId);
            var tempSections =
                prequalification.ClientTemplates.Templates.TemplateSections.Where(rec => rec.Visible)
                    .OrderBy(rec => rec.DisplayOrder);

            //Rajesh on 1/21/2014
            if (Session["RoleName"].ToString().Equals("Client"))
            {
                long[] prequalificationStatus = { 3, 15 };
                int status = Array.IndexOf(prequalificationStatus, prequalification.PrequalificationStatusId);
                if (status != -1)
                    tempSections = prequalification.ClientTemplates.Templates.TemplateSections.Where(rec => rec.Visible && rec.VisibleToClient == true).OrderBy(rec => rec.DisplayOrder);

            }
            //Ends<<<
            //Rajesh on 2/13/2014
            if (Session["RoleName"].ToString().Equals("Vendor"))//If Vendor Login
            {

                var allVisibleTemplateSections = entityDB.TemplateSectionsPermission.Where(rec => rec.VisibleTo == 3 || rec.VisibleTo == 1 || rec.VisibleTo == 5).Select(rec => rec.TemplateSectionID).ToList();
                Guid userId = new Guid(Session["UserId"].ToString());
                //var userhasRestrictedSectionAccess = FVGen3.BusinessLogic.RolesHelper.IsUserInRole(userId, 1001, "read", preQualificationDetails.ClientId, preQualificationDetails.VendorId);
                ViewBag.RestrictedSectionEdit = FVGen3.BusinessLogic.RolesHelper.IsUserInRole(userId, 1001, "read", prequalification.ClientId, prequalification.VendorId);
                tempSections = prequalification.ClientTemplates.Templates.TemplateSections.Where(rec => rec.Visible == true && allVisibleTemplateSections.Contains(rec.TemplateSectionID)).OrderBy(rec => rec.DisplayOrder);
            }
            //var longSecid = tempSections[tempSections.IndexOf(tempSections.FirstOrDefault(rec => rec.TemplateSectionID == currentSectionId)) + 1].TemplateSectionID;
            var sections = tempSections.Select(rec => rec.TemplateSectionID).ToList();
            //Assuming this is not a last record.bcz this logic for payment next section.
            var longSecid = sections[sections.IndexOf(sections.FirstOrDefault(rec => rec == currentSectionId)) + 1];
            return longSecid;
        }
        // Kiran on 07/16/2014
        [HttpPost]
        public ActionResult NotifyURLForMerchant()
        {
            byte[] param = Request.BinaryRead(Request.ContentLength);
            string strRequest = Encoding.ASCII.GetString(param);

            // append PayPal verification code to end of string
            strRequest += "&cmd=_notify-validate";

            // create an HttpRequest channel to perform handshake with PayPal
            // Kiran on 7/30/2014
            bool useSandbox = Convert.ToBoolean(ConfigurationManager.AppSettings["UseSandbox"]);
            HttpWebRequest req;
            if (useSandbox)
                req = (HttpWebRequest)WebRequest.Create(@"https://www.sandbox.paypal.com/cgi-bin/webscr");
            else
                req = (HttpWebRequest)WebRequest.Create(@"https://www.paypal.com/cgi-bin/webscr");
            // Ends<<<
            req.Method = "POST";
            req.ContentType = "application/x-www-form-urlencoded";
            req.ContentLength = strRequest.Length;

            StreamWriter streamOut = new StreamWriter(req.GetRequestStream(), Encoding.ASCII);
            streamOut.Write(strRequest);
            streamOut.Close();

            // receive response from PayPal
            StreamReader streamIn = new StreamReader(req.GetResponse().GetResponseStream());
            string strResponse = streamIn.ReadToEnd();
            streamIn.Close();

            string txn_id = Request.Form["txn_id"];
            // if PayPal response is successful / verified
            if (strResponse.Equals("VERIFIED"))
            {
                // paypal has verified the data, it is safe for us to perform processing now

                // extract the form fields expected: buyer and seller email, payment status, amount
                string payerEmail = Request.Form["payer_email"];
                string paymentStatus = Request.Form["payment_status"];
                string receiverEmail = Request.Form["receiver_email"];
                string metaData = Request.Form["mc_gross"]; // Kiran on 7/30/2014
                string transactionId = Request.Form["txn_id"]; // Kiran on 7/30/2014
                string custom = Request.Form["custom"];
                long preqId = Convert.ToInt64(Request.Form["preqID"]);
                string paymentAmount = Request.Form["mc_gross"];
                string itemName = HttpUtility.HtmlDecode(Request.Form["item_name"]);
                int index = itemName.IndexOf("(");
                string paymentDate = Request.Form["payment_date"]; // Kiran on 12/07/2015
                //int itemNameLength = itemName.Length;
                //long prequalificationId = Convert.ToInt64(itemName.Substring(index + 1, itemNameLength - 1));
                long prequalificationId = Convert.ToInt64(itemName.Substring(index + 1).Replace(")", ""));//Rajesh on 8/12/2014
                //if (paymentStatus.Equals("Completed"))
                //{
                //    // Kiran on 7/30/2014
                //    var prequalRecord = entityDB.Prequalification.FirstOrDefault(rec => rec.PrequalificationId == prequalificationId);
                //    //prequalRecord.PaymentTransactionId = transactionId;
                //    //prequalRecord.TransactionMetaData = strRequest;//metaData;//Rajesh on 8/12/2014
                //    if (prequalRecord.PaymentReceived == false || prequalRecord.PaymentReceived == null)
                //    {
                //        prequalRecord.PaymentReceived = true;
                //        prequalRecord.TotalPaymentReceivedAmount = Convert.ToDecimal(paymentAmount);
                //        //prequalRecord.PaymentReceivedDate = DateTime.Now;
                //    }
                //    entityDB.Entry(prequalRecord).State = System.Data.EntityState.Modified;
                //    entityDB.SaveChanges();
                //    // Ends<<<
                //}
                if (paymentStatus.Equals("Pending") || paymentStatus.Equals("Completed"))
                {

                    if (!string.IsNullOrEmpty(custom))
                    {
                        var prequalRecord = entityDB.Prequalification.FirstOrDefault(rec => rec.PrequalificationId == prequalificationId);

                        foreach (var sections in custom.Split('|'))
                        {
                            if (sections.Contains("preq"))
                            {
                                var prequal = sections.Split(':');

                                // Kiran on 02/24/2015
                                PrequalificationPayments paymentLog = entityDB.PrequalificationPayments.FirstOrDefault(rec => rec.PrequalificationId == prequalificationId && rec.PaymentCategory == 0);

                                // Kiran on 10/16/2015 to handle the payment log issue when the user closes the browser after making payment
                                if (paymentLog == null)
                                {
                                    paymentLog = new PrequalificationPayments();
                                    paymentLog.InvoiceAmount = entityDB.ClientTemplatesBUGroupPrice.FirstOrDefault(rec => rec.ClientTemplateID == prequalRecord.ClientTemplateId && rec.GroupPriceType == 0 && rec.GroupingFor == 0).GroupPrice;
                                    paymentLog.PaymentReceivedAmount = Convert.ToDecimal(prequal[1]);
                                    paymentLog.PaymentReceivedDate = DateTime.Now;
                                    paymentLog.PaymentCategory = 0;
                                    paymentLog.PaymentTransactionId = transactionId;
                                    paymentLog.PrequalificationId = prequalificationId;
                                    paymentLog.TransactionMetaData = strRequest;
                                    prequalRecord.PaymentReceived = true;
                                    prequalRecord.TotalPaymentReceivedAmount = Convert.ToDecimal(prequal[1]);
                                    entityDB.Entry(paymentLog).State = EntityState.Added;
                                    // for 0 group bu's update in preq.sites
                                    var buGroupIds = entityDB.ClientTemplatesBUGroupPrice.Where(rec => rec.ClientTemplateID == prequalRecord.ClientTemplateId && rec.GroupingFor == 0 && rec.GroupPrice != 0 && rec.GroupPriceType == 0).Select(rec => rec.ClientTemplateBUGroupId).ToList();
                                    var buIds = entityDB.ClientTemplatesForBU.Where(rec => buGroupIds.Contains((long)rec.ClientTemplateBUGroupId)).Select(rec => rec.ClientBusinessUnitId).ToList();
                                    var UnPaymentSites = entityDB.PrequalificationSites.Where(rec => buIds.Contains(rec.ClientBusinessUnitId) && rec.PrequalificationId == prequalificationId && rec.PrequalificationPaymentsId == null).ToList();

                                    foreach (var site in UnPaymentSites)
                                    {
                                        site.PrequalificationPaymentsId = paymentLog.PrequalificationPaymentsId;
                                        entityDB.Entry(site).State = EntityState.Modified;
                                        entityDB.SaveChanges();
                                    }
                                    entityDB.Entry(prequalRecord).State = EntityState.Modified;
                                    entityDB.SaveChanges();
                                }
                                else
                                {
                                    paymentLog.PaymentTransactionId = transactionId;
                                    paymentLog.TransactionMetaData = strRequest;

                                    entityDB.Entry(paymentLog).State = System.Data.EntityState.Modified;

                                    //prequalRecord.TotalPaymentReceivedAmount = Convert.ToDecimal(prequal[1]);
                                    //entityDB.Entry(prequalRecord).State = System.Data.EntityState.Modified;
                                    entityDB.SaveChanges();
                                }
                                //  Kiran on 10/16/2015 Ends<<<
                            }
                            else if (sections.Contains("additional"))
                            {
                                var additionalsplit = sections.Split(':');
                                var busplit = additionalsplit[1].Split(';');

                                var buid = busplit[0].Split(',')[0];
                                long preqSitesBuId = Convert.ToInt64(buid);
                                var prequalificationPaymentId = entityDB.PrequalificationSites.FirstOrDefault(rec => rec.ClientBusinessUnitId == preqSitesBuId && rec.PrequalificationId == prequalificationId).PrequalificationPaymentsId;

                                PrequalificationPayments paymentLog = entityDB.PrequalificationPayments.FirstOrDefault(rec => rec.PrequalificationId == prequalificationId && rec.PrequalificationPaymentsId == prequalificationPaymentId && rec.PaymentCategory == 1);

                                // Kiran on 10/16/2015 to handle the payment log issue when the user closes the browser after making payment
                                if (paymentLog == null)
                                {
                                    paymentLog = new PrequalificationPayments();
                                    paymentLog.InvoiceAmount = Convert.ToDecimal(busplit[1]);
                                    paymentLog.PaymentReceivedAmount = Convert.ToDecimal(busplit[1]);
                                    paymentLog.PaymentReceivedDate = DateTime.Now;
                                    paymentLog.PaymentCategory = 1;
                                    paymentLog.PaymentTransactionId = transactionId;
                                    paymentLog.PrequalificationId = prequalificationId;
                                    paymentLog.TransactionMetaData = strRequest;

                                    entityDB.Entry(paymentLog).State = EntityState.Added;

                                    entityDB.SaveChanges();

                                    foreach (var businessUniId in busplit[0].Split(','))
                                    {
                                        if (!string.IsNullOrEmpty(businessUniId))
                                        {
                                            long preqSiteBuId = Convert.ToInt64(buid);
                                            var prequalificationSites = entityDB.PrequalificationSites.Where(rec => rec.ClientBusinessUnitId == preqSiteBuId && rec.PrequalificationId == prequalificationId).ToList();
                                            foreach (var prequalSite in prequalificationSites)
                                            {
                                                prequalSite.PrequalificationPaymentsId = paymentLog.PrequalificationPaymentsId;
                                                entityDB.Entry(prequalSite).State = EntityState.Modified;
                                                entityDB.SaveChanges();
                                            }
                                        }
                                    }
                                    prequalRecord.TotalPaymentReceivedAmount += Convert.ToDecimal(busplit[1]);
                                    entityDB.Entry(prequalRecord).State = EntityState.Modified;
                                    entityDB.SaveChanges();
                                }
                                else
                                {
                                    paymentLog.PaymentTransactionId = transactionId;
                                    paymentLog.TransactionMetaData = strRequest;
                                    entityDB.Entry(paymentLog).State = System.Data.EntityState.Modified;

                                    entityDB.SaveChanges();
                                }
                                // Kiran on 10/16/2015 Ends<<<
                            }
                            else if (sections.Contains("annual"))
                            {
                                // kiran on 10/16/2015 to handle issue when user closes the browser 
                                var annualsplit = sections.Split(':');
                                var busplit = annualsplit[1].Split(';');
                                int preqstartmonth = 0;
                                int todaymonth = 0;


                                //foreach (var templateBuid in busplit[0].Split(','))
                                //{
                                //    long localTemplateBuId = Convert.ToInt64(templateBuid);

                                //    PrequalificationTrainingAnnualFees annualFee = entityDB.PrequalificationTrainingAnnualFees.FirstOrDefault(rec => rec.ClientTemplateForBUId == localTemplateBuId && rec.PrequalificationId == prequalificationId);

                                //    annualFee.FeeTransactionId = transactionId;
                                //    annualFee.FeeTransactionMetaData = strRequest;
                                //    entityDB.Entry(annualFee).State = System.Data.EntityState.Modified;
                                //    entityDB.SaveChanges();
                                //}


                                // Sumanth on 06/24/2015

                                long preqStatusid = prequalRecord.PrequalificationStatusId;
                                if (preqStatusid != 3 && preqStatusid != 15 && preqStatusid != 4 && preqStatusid != 7 && preqStatusid != 10 && preqStatusid != 23) // Kiran on 09/11/2015
                                {
                                    preqstartmonth = entityDB.Prequalification.FirstOrDefault(rec => rec.PrequalificationId == prequalificationId).PrequalificationStart.Month;
                                    todaymonth = int.Parse(DateTime.Today.Month.ToString());
                                }                    // Ends<<<
                                // Kiran on 08/26/2015
                                if ((todaymonth < preqstartmonth) || (todaymonth == prequalRecord.PrequalificationFinish.Month && DateTime.Now.Year == prequalRecord.PrequalificationFinish.Year))
                                {
                                    ViewBag.monthdiff = (prequalRecord.PrequalificationFinish.Month - todaymonth) + 1;
                                }
                                else
                                {
                                    ViewBag.monthdiff = 12 - (todaymonth - preqstartmonth);
                                }
                                // Ends<<<
                                PrequalificationTrainingAnnualFees record = new PrequalificationTrainingAnnualFees();

                                foreach (var templateBuid in busplit[0].Split(','))
                                {
                                    long localTemplateBuId = Convert.ToInt64(templateBuid);
                                    var cliettemplateforBu = entityDB.ClientTemplatesForBU.FirstOrDefault(rec => rec.ClientTemplateForBUId == localTemplateBuId);
                                    // Sumanth on 06/24/2015
                                    var clientTemplateBUGroupPrice = entityDB.ClientTemplatesBUGroupPrice.Find(cliettemplateforBu.ClientTemplateBUGroupId).GroupPrice;
                                    decimal preqannualfee = Math.Round(clientTemplateBUGroupPrice * ViewBag.monthdiff / 12, 2, MidpointRounding.AwayFromZero);
                                    // Ends<<<
                                    record = entityDB.PrequalificationTrainingAnnualFees.FirstOrDefault(rec => rec.ClientTemplateBUGroupId == cliettemplateforBu.ClientTemplateBUGroupId && rec.PrequalificationId == prequalificationId && rec.ClientTemplateForBUId == localTemplateBuId);
                                    var recExist = entityDB.PrequalificationTrainingAnnualFees.FirstOrDefault(rec => rec.ClientTemplateBUGroupId == cliettemplateforBu.ClientTemplateBUGroupId && rec.PrequalificationId == prequalificationId);
                                    PrequalificationTrainingAnnualFees annualFee = entityDB.PrequalificationTrainingAnnualFees.FirstOrDefault(rec => rec.ClientTemplateForBUId == localTemplateBuId && rec.PrequalificationId == prequalificationId); // kiran on 09/28/2015 to handle issue when user closes the browser
                                    if (record == null)
                                    {
                                        annualFee = new PrequalificationTrainingAnnualFees();
                                        if (recExist == null)
                                        {
                                            // Sumanth on 06/24/2015
                                            annualFee.AnnualFeeAmount = preqannualfee;
                                            annualFee.AnnualFeeReceivedAmount = preqannualfee;
                                            // Ends<<<
                                        }
                                        else
                                        {
                                            annualFee.AnnualFeeAmount = 0;
                                            annualFee.AnnualFeeReceivedAmount = 0;
                                        }
                                        annualFee.PrequalificationId = prequalificationId;
                                        annualFee.ClientTemplateForBUId = cliettemplateforBu.ClientTemplateForBUId;
                                        annualFee.ClientTemplateBUGroupId = (long)cliettemplateforBu.ClientTemplateBUGroupId;
                                        annualFee.FeeReceivedDate = DateTime.Now;
                                        // Kiran on 12/07/2015
                                        annualFee.FeeTransactionId = transactionId;
                                        annualFee.FeeTransactionMetaData = strRequest;
                                        // Kiran on 12/07/2015 ends
                                        entityDB.PrequalificationTrainingAnnualFees.Add(annualFee);
                                        entityDB.SaveChanges();
                                    }
                                    else
                                    {
                                        annualFee.FeeTransactionId = transactionId;
                                        annualFee.FeeTransactionMetaData = strRequest;
                                        entityDB.Entry(annualFee).State = EntityState.Modified;
                                        entityDB.SaveChanges();
                                    }
                                }
                                if (record == null)
                                {
                                    if (prequalRecord.TotalAnnualFeeReceived != null)
                                        prequalRecord.TotalAnnualFeeReceived += Convert.ToDecimal(busplit[1]);
                                    else
                                        prequalRecord.TotalAnnualFeeReceived = Convert.ToDecimal(busplit[1]);
                                    entityDB.Entry(prequalRecord).State = EntityState.Modified;
                                    entityDB.SaveChanges();
                                }
                                // Kiran on 10/16/2015 Ends<<<
                            }
                        }

                        //Kiran on 12/07/2015 for FV-227
                        var VendorDetails = prequalRecord.Vendor.Name + " (" + prequalRecord.PrequalificationId + ") ";
                        string body = System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Views/Prequalification/EMailFormatOfPaymentReceived.htm"));
                        body = body.Replace("VendorDetails", VendorDetails);
                        body = body.Replace("TotalAmount", paymentAmount);
                        body = body.Replace("TxnId", transactionId);
                        body = body.Replace("PaidDate", paymentDate);
                        body = body.Replace("Client", prequalRecord.Client.Name);

                        string toEmail = ConfigurationManager.AppSettings["EmailForPaymentNotice"];

                        Utilities.SendMail.sendMail(toEmail, body, "" + prequalRecord.Vendor.Name + " - Payment Transaction Details");
                        //Kiran on 12/07/2015 for FV-227 ends

                        string fileName = txn_id;

                        Directory.CreateDirectory(Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["PaypalNotification"])));
                        var filePath = Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["PaypalNotification"]), fileName + ".txt");

                        System.IO.File.WriteAllText(filePath, strRequest + "\n" + strResponse);

                    }
                    // Ends<<<
                }

                // else
                else
                {
                    // payment not complete yet, may be undergoing additional verification or processing

                    // do nothing - wait for paypal to send another IPN when payment is complete
                }

            }

            return View();
        }
        // Ends<<<





        // kiran on 2/12/2014

        //public ActionResult UpdatePaymentDetails(Guid templateId, long TemplateSectionID, long PreQualificationId, int? mode, decimal paymentAmt)//3/17/2014
        //{
        //    string redirectionURL = "";
        //    var prequalificationRecord = entityDB.Prequalification.Find(PreQualificationId);
        //    prequalificationRecord.PaymentReceived = true;
        //    // Kiran on 3/17/2014 Changes by mani for fvos-39 on 8/6/2015
        //    if (prequalificationRecord.PaymentReceivedAmount == null)
        //        prequalificationRecord.PaymentReceivedAmount = paymentAmt;
        //    else
        //        prequalificationRecord.PaymentReceivedAmount += paymentAmt;
        //    prequalificationRecord.PaymentReceivedDate = DateTime.Now;
        //    // Ends<<<
        //    entityDB.Entry(prequalificationRecord).State = EntityState.Modified;
        //    entityDB.SaveChanges();
        //    redirectionURL = "TemplateSectionsList?templateId=" + templateId + "&TemplateSectionID=" + TemplateSectionID + "&PreQualificationId=" + PreQualificationId + "&mode=" + mode;
        //    return Redirect(redirectionURL);
        //}
        public static int cntr = 0; // this counter is used to handle paypal redirection to smi application payment page within and after 10 sec // sumanth 18/08/2015
        public ActionResult UpdatePaymentDetails(Guid templateId, long TemplateSectionID, long PreQualificationId, int? mode, decimal paymentAmt, string payString)//3/17/2014
        {

            string redirectionURL = "";
            if (cntr == 0)
            {
                cntr = 1;
                var prequalificationRecord = entityDB.Prequalification.Find(PreQualificationId);
                prequalificationRecord.PaymentReceived = true;
                // Kiran on 3/17/2014
                //prequalificationRecord.TotalPaymentReceivedAmount += paymentAmt;

                // updating the 0 fee group bu's with prequalification payment.
                var buGroupIds = entityDB.ClientTemplatesBUGroupPrice.Where(rec => rec.ClientTemplateID == prequalificationRecord.ClientTemplateId && rec.GroupingFor == 0 && rec.GroupPrice != 0 && rec.GroupPriceType == 0).Select(rec => rec.ClientTemplateBUGroupId).ToList();
                var buIds = entityDB.ClientTemplatesForBU.Where(rec => buGroupIds.Contains((long)rec.ClientTemplateBUGroupId)).Select(rec => rec.ClientBusinessUnitId).ToList();
                var UnPaymentSites = entityDB.PrequalificationSites.Where(rec => buIds.Contains(rec.ClientBusinessUnitId) && rec.PrequalificationId == PreQualificationId && rec.PrequalificationPaymentsId == null).ToList();
                PrequalificationPayments preqPaymentLog = entityDB.PrequalificationPayments.FirstOrDefault(rec => rec.PrequalificationId == PreQualificationId && rec.PaymentCategory == 0);
                if (preqPaymentLog != null)
                {
                    foreach (var site in UnPaymentSites)
                    {
                        site.PrequalificationPaymentsId = preqPaymentLog.PrequalificationPaymentsId;
                        entityDB.Entry(site).State = EntityState.Modified;
                        entityDB.SaveChanges();
                    }
                }
                //Ends>>

                // Mani on 6/22/2015
                foreach (var sections in payString.Split('|'))
                {
                    if (sections.Contains("preq"))
                    {
                        var prequal = sections.Split(':');

                        //if (prequalificationRecord.PaymentReceived == false || prequalificationRecord.PaymentReceived == null)
                        //{
                        prequalificationRecord.PaymentReceived = true;

                        //prequalRecord.PaymentReceivedDate = DateTime.Now;
                        //}

                        PrequalificationPayments paymentLog = entityDB.PrequalificationPayments.FirstOrDefault(rec => rec.PrequalificationId == PreQualificationId && rec.PaymentCategory == 0);
                        if (paymentLog == null)
                        {
                            paymentLog = new PrequalificationPayments();
                            paymentLog.InvoiceAmount = entityDB.ClientTemplatesBUGroupPrice.FirstOrDefault(rec => rec.ClientTemplateID == prequalificationRecord.ClientTemplateId && rec.GroupPriceType == 0 && rec.GroupingFor == 0).GroupPrice;
                            paymentLog.PaymentReceivedAmount = Convert.ToDecimal(prequal[1]);
                            paymentLog.PaymentReceivedDate = DateTime.Now;
                            paymentLog.PaymentCategory = 0;
                            //paymentLog.PaymentTransactionId = transactionId;
                            paymentLog.PrequalificationId = PreQualificationId;
                            //paymentLog.TransactionMetaData = strRequest;
                            prequalificationRecord.TotalPaymentReceivedAmount = Convert.ToDecimal(prequal[1]);
                            entityDB.Entry(paymentLog).State = EntityState.Added;
                            // for 0 group bu's update in preq.sites
                            foreach (var site in UnPaymentSites)
                            {
                                site.PrequalificationPaymentsId = paymentLog.PrequalificationPaymentsId;
                                entityDB.Entry(site).State = EntityState.Modified;
                                entityDB.SaveChanges();
                            }
                        }
                        else
                        {
                            paymentLog.PaymentReceivedAmount = Convert.ToDecimal(prequal[1]);
                            paymentLog.PaymentReceivedDate = DateTime.Now;
                            entityDB.Entry(paymentLog).State = System.Data.EntityState.Modified;
                            prequalificationRecord.TotalPaymentReceivedAmount = Convert.ToDecimal(prequal[1]);
                        }
                        entityDB.Entry(prequalificationRecord).State = System.Data.EntityState.Modified;
                        entityDB.SaveChanges();
                    }
                    else if (sections.Contains("additional"))
                    {
                        var additionalsplit = sections.Split(':');
                        var busplit = additionalsplit[1].Split(';');

                        PrequalificationPayments paymentLog = new PrequalificationPayments();
                        paymentLog.InvoiceAmount = Convert.ToDecimal(busplit[1]);
                        paymentLog.PaymentReceivedAmount = Convert.ToDecimal(busplit[1]);
                        paymentLog.PaymentReceivedDate = DateTime.Now;
                        paymentLog.PaymentCategory = 1;
                        //paymentLog.PaymentTransactionId = transactionId;
                        paymentLog.PrequalificationId = PreQualificationId;
                        //paymentLog.TransactionMetaData = strRequest;

                        entityDB.Entry(paymentLog).State = EntityState.Added;

                        entityDB.SaveChanges();

                        foreach (var buid in busplit[0].Split(','))
                        {
                            if (!string.IsNullOrEmpty(buid))
                            {
                                long preqSitesBuId = Convert.ToInt64(buid);
                                var prequalificationSites = entityDB.PrequalificationSites.Where(rec => rec.ClientBusinessUnitId == preqSitesBuId && rec.PrequalificationId == PreQualificationId).ToList();
                                foreach (var prequalSite in prequalificationSites)
                                {
                                    prequalSite.PrequalificationPaymentsId = paymentLog.PrequalificationPaymentsId;
                                    entityDB.Entry(prequalSite).State = EntityState.Modified;
                                    entityDB.SaveChanges();
                                }
                            }
                        }
                        prequalificationRecord.TotalPaymentReceivedAmount += Convert.ToDecimal(busplit[1]);
                        entityDB.Entry(prequalificationRecord).State = EntityState.Modified;
                        entityDB.SaveChanges();
                    }
                    else if (sections.Contains("annual"))
                    {
                        // Sumanth on 06/24/2015
                        var annualsplit = sections.Split(':');
                        var busplit = annualsplit[1].Split(';');
                        int preqstartmonth = 0;
                        int todaymonth = 0;

                        long preqStatusid = prequalificationRecord.PrequalificationStatusId;
                        if (preqStatusid != 3 && preqStatusid != 15)
                        {
                            preqstartmonth = prequalificationRecord.PrequalificationStart.Month;
                            todaymonth = int.Parse(DateTime.Today.Month.ToString());
                        }
                        // Ends<<<

                        // Kiran on 08/26/2015
                        if ((todaymonth < preqstartmonth) || (todaymonth == prequalificationRecord.PrequalificationFinish.Month && DateTime.Now.Year == prequalificationRecord.PrequalificationFinish.Year))
                        {
                            ViewBag.monthdiff = (prequalificationRecord.PrequalificationFinish.Month - todaymonth) + 1;
                        }
                        else
                        {
                            ViewBag.monthdiff = 12 - (todaymonth - preqstartmonth);
                        }
                        // Ends<<<
                        PrequalificationTrainingAnnualFees record = new PrequalificationTrainingAnnualFees();

                        foreach (var templateBuid in busplit[0].Split(','))
                        {
                            long localTemplateBuId = Convert.ToInt64(templateBuid);
                            var cliettemplateforBu = entityDB.ClientTemplatesForBU.FirstOrDefault(rec => rec.ClientTemplateForBUId == localTemplateBuId);
                            // Sumanth on 06/24/2015
                            var clientTemplateBUGroupPrice = entityDB.ClientTemplatesBUGroupPrice.Find(cliettemplateforBu.ClientTemplateBUGroupId).GroupPrice;
                            decimal preqannualfee = Math.Round(clientTemplateBUGroupPrice * ViewBag.monthdiff / 12, 2, MidpointRounding.AwayFromZero);
                            // Ends<<<
                            record = entityDB.PrequalificationTrainingAnnualFees.FirstOrDefault(rec => rec.ClientTemplateBUGroupId == cliettemplateforBu.ClientTemplateBUGroupId && rec.PrequalificationId == PreQualificationId && rec.ClientTemplateForBUId == localTemplateBuId);
                            var recExist = entityDB.PrequalificationTrainingAnnualFees.FirstOrDefault(rec => rec.ClientTemplateBUGroupId == cliettemplateforBu.ClientTemplateBUGroupId && rec.PrequalificationId == PreQualificationId);
                            if (record == null)
                            {
                                PrequalificationTrainingAnnualFees annualFee = new PrequalificationTrainingAnnualFees();
                                if (recExist == null)
                                {
                                    // Sumanth on 06/24/2015
                                    //annualFee.AnnualFeeAmount = Convert.ToDecimal(busplit[1]);
                                    //annualFee.AnnualFeeReceivedAmount = Convert.ToDecimal(busplit[1]);
                                    annualFee.AnnualFeeAmount = preqannualfee;
                                    annualFee.AnnualFeeReceivedAmount = preqannualfee;
                                    // Ends<<<
                                }
                                else
                                {
                                    annualFee.AnnualFeeAmount = 0;
                                    annualFee.AnnualFeeReceivedAmount = 0;
                                }
                                annualFee.PrequalificationId = PreQualificationId;
                                annualFee.ClientTemplateForBUId = cliettemplateforBu.ClientTemplateForBUId;
                                annualFee.ClientTemplateBUGroupId = (long)cliettemplateforBu.ClientTemplateBUGroupId;
                                annualFee.FeeReceivedDate = DateTime.Now;
                                //annualFee.FeeTransactionId = transactionId;
                                //annualFee.FeeTransactionMetaData = strRequest;
                                entityDB.PrequalificationTrainingAnnualFees.Add(annualFee);
                                entityDB.SaveChanges();
                            }
                        }
                        if (record == null)
                        {
                            //prequalificationRecord.TotalAnnualFeeAmount += Convert.ToDecimal(busplit[1]);
                            if (prequalificationRecord.TotalAnnualFeeReceived != null)
                                prequalificationRecord.TotalAnnualFeeReceived += Convert.ToDecimal(busplit[1]);
                            else
                                prequalificationRecord.TotalAnnualFeeReceived = Convert.ToDecimal(busplit[1]);
                            entityDB.Entry(prequalificationRecord).State = EntityState.Modified;
                            entityDB.SaveChanges();
                        }
                    }
                }

                //prequalificationRecord.PaymentReceivedDate = DateTime.Now;
                // Ends<<<
                entityDB.Entry(prequalificationRecord).State = EntityState.Modified;
                entityDB.SaveChanges();

            }
            redirectionURL = "TemplateSectionsList?templateId=" + templateId + "&TemplateSectionID=" + TemplateSectionID + "&PreQualificationId=" + PreQualificationId + "&mode=" + mode;
            return Redirect(redirectionURL);
        }
        // Ends<<<



        //Rajesh on 12/30/2013
        public string getVendorDetails(Guid userId)
        {
            var user = entityDB.SystemUsers.Find(userId);
            var phoneNumber = "";
            try
            {
                phoneNumber = user.Contacts.FirstOrDefault().PhoneNumber;
            }
            catch { }
            var cTitle = "";
            try
            {
                cTitle = user.Contacts.FirstOrDefault().ContactTitle;
            }
            catch { }
            var userDetails = phoneNumber + "|" + user.Email + "|" + cTitle;
            return userDetails;
        }
        //Ends<<<
        //Rajesh on 2/6/2014
        public ActionResult GeneratePDF(long TemplateSectionID, long PreQualificationId)
        {
            var htmlText = @"<html><body style='font-family:arial,sans-serief;font-size:14px;'>";
            var currentPrequalification = entityDB.Prequalification.Find(PreQualificationId);

            var currentTSection = entityDB.TemplateSections.Find(TemplateSectionID);
            var clientName = currentPrequalification.Client.Name;
            var vendorName = currentPrequalification.Vendor.Name;
            foreach (var Sec in currentTSection.TemplateSubSections.Where(rec => rec.SubSectionTypeCondition == "MPATitleHeader").ToList())
            {
                htmlText += "<br/>";
                htmlText += "<h2 style='float:left;font-size: 16px;font-weight: 700;margin: -0.2em 0px 0px;width:100%;'><u>" + Sec.SubSectionName + "</u></h2>";

                htmlText += "<p>" + Sec.SectionNotes.Replace("[_ClientName]", "“" + clientName + "”").Replace("[_VendorName]", "“" + vendorName + "”") + "</p>";
            }

            foreach (var Header in currentTSection.TemplateSectionsHeaderFooter.Where(rec => rec.PositionType == 0).ToList())
            {
                htmlText += "<br/>";
                htmlText += "<h2 style='float:left;font-size: 16px;font-weight: 700;margin: -0.2em 0px 0px;width:100%;'><u>" + Header.PositionTitle + "</u></h2>";

                htmlText += "<p>" + Header.PostionContent.Replace("[_ClientName]", "“" + clientName + "”").Replace("[_VendorName]", "“" + vendorName + "”") + "</p>";
            }

            foreach (var Sec in currentTSection.TemplateSubSections.Where(rec => rec.SubSectionTypeCondition != "MPATitleHeader").ToList())
            {
                htmlText += "<br/>";
                htmlText += "<h2 style='float:left;font-size: 16px;font-weight: 700;margin: -0.2em 0px 0px;width:100%;'><u>" + Sec.SubSectionName + "</u></h2>";

                htmlText += "<p>" + Sec.SectionNotes + "</p>";
                htmlText += "<br/><table style='width:50%;float:left;'>";

                foreach (var question in Sec.Questions.ToList())
                {
                    htmlText += "<tr><td><b>" + question.QuestionText + "</b></td>";
                    htmlText += "<td>";
                    try
                    {
                        htmlText += question.QuestionColumnDetails.FirstOrDefault().PrequalificationUserInputs.FirstOrDefault(rec => rec.PreQualificationId == PreQualificationId).UserInput;
                    }
                    catch { };
                    htmlText += "</td>";
                    htmlText += "</tr>";
                }
                htmlText += "</table>";
            }



            //foreach (var Footer in currentTSection.TemplateSectionsHeaderFooter.Where(rec => rec.PositionType == 1).ToList())
            //{
            //    htmlText += "<br/>";
            //    htmlText += "<h2 style='float:left;font-size: 16px;font-weight: 700;margin: -0.2em 0px 0px;width:100%;'>" + Footer.PositionTitle + "</h2>";

            //    htmlText += "<p>" + Footer.PostionContent + "</p>";
            //}

            htmlText += "</body></html>";
            iTextSharp.text.Document document = new iTextSharp.text.Document(iTextSharp.text.PageSize.A4);

            //Rajesh on 2/13/2014
            Directory.CreateDirectory(Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["MPADrafts"])));
            var path = Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["MPADrafts"]), "MPADraft" + PreQualificationId + ".pdf");
            //Ends<<<
            Stream f = new FileStream(path, FileMode.Create);
            PdfWriter.GetInstance(document, f);
            document.Open();

            //iTextSharp.text.Font font = new iTextSharp.text.Font(iTextSharp.text.Font.HELVETICA, 9, iTextSharp.text.Font.NORMAL);
            //iTextSharp.text.Chunk chunk = new iTextSharp.text.Chunk("RajeIsh", font);
            //document.Add(chunk);

            iTextSharp.text.html.simpleparser.HTMLWorker hw =
                         new iTextSharp.text.html.simpleparser.HTMLWorker(document);
            hw.Parse(new StringReader(htmlText));
            document.Close();

            var fs = System.IO.File.OpenRead(path);
            //return File(fs, "file", userEnteredFilename);
            Response.AppendHeader("Content-Disposition", "inline; filename=Sample.pdf");
            return File(fs, "application/pdf");
        }
        //Ends<<<

        // Kiran on 3/12/2014 for prequalification site deletion
        public string deletePrequalSite(long prequalSiteId, long prequalId)
        {
            var prequalSiteRecord = entityDB.PrequalificationSites.FirstOrDefault(rec => rec.PrequalificationSitesId == prequalSiteId && rec.PrequalificationId == prequalId);
            var annaulFeeRec = entityDB.PrequalificationTrainingAnnualFees.FirstOrDefault(rec => rec.ClientTemplatesForBU.ClientBusinessUnitId == prequalSiteRecord.ClientBusinessUnitId && rec.PrequalificationId == prequalId);//Mani on 8/5/2015
            //// Kiran on 07/22/2015
            //if (prequalSiteRecord.PrequalificationPaymentsId == null && annaulFeeRec == null)
            //{
            //    entityDB.PrequalificationSites.Remove(prequalSiteRecord);
            //    entityDB.SaveChanges();
            //    return "SiteDeleteSuccess";
            //}
            //else
            //    return "CantDeletePaymentDone";
            //entityDB.PrequalificationSites.Remove(prequalSiteRecord);
            //entityDB.SaveChanges();

            // sumanth on 10/19/2015 for FVOS-92 to delete the sites in prequalification starts
            if (annaulFeeRec == null)
            {
                PrequalificationPayments paymentTransaction = null;
                if (prequalSiteRecord.PrequalificationPaymentsId != null)
                {
                    paymentTransaction = entityDB.PrequalificationPayments.FirstOrDefault(rec => rec.PrequalificationPaymentsId == prequalSiteRecord.PrequalificationPaymentsId);
                    if (paymentTransaction.PaymentCategory == 0)
                    {
                        prequalSiteRecord.PrequalificationPaymentsId = null;
                        entityDB.Entry(prequalSiteRecord).State = EntityState.Modified;
                    }
                }
                if (prequalSiteRecord.PrequalificationPaymentsId == null)
                    entityDB.PrequalificationSites.Remove(prequalSiteRecord);
                else if (prequalSiteRecord.PrequalificationPaymentsId != null && paymentTransaction.PaymentCategory == 1)
                    return "AdditionalPaymentDone";
            }
            else
            {
                return "TrainingPaymentDone";
            }
            // Sumanth ends on 10/19/2015 Ends<<<
            var locationsSec = entityDB.PrequalificationCompletedSections.FirstOrDefault(rec => rec.PrequalificationId == prequalId && rec.TemplateSections.TemplateSectionType == 7);
            if (locationsSec != null)
                entityDB.PrequalificationCompletedSections.Remove(locationsSec);


            entityDB.SaveChanges();
            return "SiteDeleteSuccess";


        }

        // Ends<<
    }

}
