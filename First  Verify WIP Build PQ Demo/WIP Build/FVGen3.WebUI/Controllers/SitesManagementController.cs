﻿/*
Page Created Date:  07/06/2013
Created By: Kiran Talluri
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FVGen3.Domain.Abstract;
using FVGen3.Domain.Entities;
using FVGen3.Domain.Concrete;
using Ninject;
using FVGen3.WebUI.Annotations;
using System.Data.Entity;

namespace FVGen3.WebUI.Controllers
{
    public class SitesManagementController : BaseController
    {
        //
        // GET: /SitesManagement/

        EFDbContext entityDB = new EFDbContext();

        [HttpGet]
        [SessionExpire]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "MyAccount", sideMenuName = "Sitetypes", ViewName = "ManageSites")]
        public ActionResult ManageSites()
        {
            ClientsList();
            return View();
        }

        /*
            Created Date:  06/07/2013   Created By: Kiran Talluri
            Purpose: To display List of Clients
        */
        private void ClientsList()
        {
            var clientsList = from orgs in entityDB.Organizations.OrderBy(rec => rec.Name).Where(orgs => orgs.OrganizationType == "Client" && orgs.ShowInApplication == true).ToList()                              
                              select new SelectListItem
                               {
                                   Text = orgs.Name,
                                   Value = orgs.OrganizationID.ToString()
                               };
            SelectListItem defaultItem = new SelectListItem();
            defaultItem = new SelectListItem();
            defaultItem.Text = "";
            defaultItem.Value = "-1";
            var clientsList_defaultItem = clientsList.ToList();
            clientsList_defaultItem.Insert(0, defaultItem);
            ViewBag.VBclientsList = clientsList_defaultItem;
        }

        /*
            Created Date:  06/10/2013   Created By: Kiran Talluri
            Purpose: To display the default form to add business unit
        */

        [HttpGet]
        [SessionExpireForView]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "MyAccount", sideMenuName = "Sitetypes", ViewName = "AddClientBusinessUnit")]
        public ActionResult AddClientBusinessUnit(long orgId, long siteId)
        {
            LocalAddClientBusinessUnit local = new LocalAddClientBusinessUnit();
            local.ClientId = orgId;

            return View(local);
        }

        // =========== To add a Client Business Unit ============ //
        /*
            Created Date:  06/10/2013   Created By: Kiran Talluri
            Purpose: To add a Client Business Unit
        */

        [SessionExpireForView(pageType = "popupview")]
        [HttpPost]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "MyAccount", sideMenuName = "Sitetypes", ViewName = "AddClientBusinessUnit")]
        public ActionResult AddClientBusinessUnit(LocalAddClientBusinessUnit local)
        {
            if (ModelState.IsValid)
            {
                if (entityDB.ClientBusinessUnits.FirstOrDefault(rec => rec.BusinessUnitName.ToLower().Trim() == local.BusinessUnitName.ToLower().Trim() && rec.ClientId == local.ClientId) != null)
                {
                    ModelState.AddModelError("BusinessUnitName", "Unit already exist");
                    return View(local);
                }
                ClientBusinessUnits clBusUnit = new ClientBusinessUnits();

                clBusUnit.ClientId = local.ClientId;
                clBusUnit.BusinessUnitName = local.BusinessUnitName;
                clBusUnit.InsertDate = DateTime.Now.Date;

                entityDB.ClientBusinessUnits.Add(clBusUnit);


                var currentOrgClientTemplates = entityDB.ClientTemplates.Where(record => record.ClientID == local.ClientId).ToList();
                foreach (var clientTemplate in currentOrgClientTemplates)
                {
                    if (clientTemplate.Templates.TemplateType == 0 && clientTemplate.Templates.TemplateStatus == 1)
                    {
                        ClientTemplatesForBU currentClientTemplateBU = new ClientTemplatesForBU();
                        currentClientTemplateBU.ClientTemplateId = clientTemplate.ClientTemplateID;
                        currentClientTemplateBU.ClientBusinessUnitId = clBusUnit.ClientBusinessUnitId;
                        entityDB.ClientTemplatesForBU.Add(currentClientTemplateBU);
                    }
                }

                entityDB.SaveChanges();
                LocalManageSites form = new LocalManageSites();
                form.ClientId = local.ClientId;
                return RedirectToAction("CloseAddClientBusinessUnit", form);
            }

            //if (!ModelState.IsValid)

            //if (formValid)
            //{
            //    var unitLocation = "";
            //    if (local.BusinessUnitType == true)
            //    {
            //        unitLocation = local.BusinessUnitLocation;
            //    }
            //    else
            //    {
            //        if (local.BusinessUnitRegion == "-1")
            //        {
            //            unitLocation = local.BusinessUnitRegionTxt;
            //        }
            //        else
            //        {
            //            unitLocation = local.BusinessUnitRegion;
            //        }
            //    }

            //}


            // Kiran on 11/22/2013

            // Ends<<<
            return View(local);
        }
                   
        public ActionResult CloseAddClientBusinessUnit(LocalManageSites local)
        {
            return View(local);
        }

        /*
            Created Date:  06/10/2013   Created By: Kiran Talluri
            Purpose: To return the partial view
        */
        [SessionExpireForView(pageType = "partial")]
        [OutputCache(Duration = 0)]
        public ActionResult PartialManageSitesList(long ClientId)
        {
            ViewBag.notValidId = false;
            long clientId = ClientId;
            if (clientId == -1)
            {
                ViewBag.notValidId = true;
                return PartialView();
            }
            // Changed on 07/24/2013 to display BU by location
            var busUnits = entityDB.Organizations.Find(clientId).ClientBusinessUnits.ToList();
            // Ends <<<<

            ViewBag.VBclientBusUnitsList = busUnits.OrderBy(rec => rec.BusinessUnitName).ToList();// Phani On 25-05-2015 arrange bu's name in alphabatical order
            return PartialView();
        }

        /*
            Created Date:  06/11/2013   Created By: Kiran Talluri
            Purpose: To display the default values when adding the site for clientunit
        */

        [HttpGet]
        [SessionExpireForView]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "MyAccount", sideMenuName = "Sitetypes", ViewName = "AddClientBusinessUnitSite")]
        public ActionResult AddClientBusinessUnitSite(long clientId, long siteId, long unitId)
        {
            var clientSelectedBusinessUnit = entityDB.ClientBusinessUnits.FirstOrDefault(record => record.ClientBusinessUnitId == unitId).BusinessUnitName; // Added on 8/22/2013

            LocalAddClientBusinessUnitSite local = new LocalAddClientBusinessUnitSite();
            local.isSiteEdit = false;

            // ========= To display the list of Safety Representatives for that particular organization ========== //

            //Rajesh on 11/29/2014
            var UserIds = entityDB.SystemUsersOrganizations.Where(rec => rec.OrganizationId == clientId).Select(rec => rec.UserId).ToList();
            local.SafetyRepresentataives = (from systemUsers in entityDB.SystemUsers.Where(rec => UserIds.Contains(rec.UserId)).ToList()
                                            select new SelectListItem
                                            {
                                                Text = systemUsers.Contacts.FirstOrDefault() != null ? systemUsers.Contacts[0].LastName + ", " + systemUsers.Contacts[0].FirstName : systemUsers.Email,
                                                Value = systemUsers.UserId.ToString()
                                            }).ToList();
            local.SafetyRepresentataives = local.SafetyRepresentataives.OrderBy(rec => rec.Text).ToList();

            //Ends<<<
           
            // Kiran on 10/29/2014
            //local.BusinessUnitType = true;
            //var businessUnitRegions = (from regions in
            //                               (from busUnitSiteRegions in entityDB.ClientBusinessUnitSites.ToList()
            //                                where busUnitSiteRegions.BusinessUnitType == 1
            //                                select busUnitSiteRegions.BusinessUnitLocation).Distinct()
            //                           select new SelectListItem
            //                           {
            //                               Text = regions,
            //                               Value = regions
            //                           });

            //var businessUnitRegionsWithNewRegion = businessUnitRegions.ToList();
            //SelectListItem newRegion = new SelectListItem();
            //newRegion.Text = "Add New Region";
            //newRegion.Value = "-1";
            //businessUnitRegionsWithNewRegion.Add(newRegion);
            //ViewBag.VBBusinessUnitRegions = businessUnitRegionsWithNewRegion;

            //var regionsList = entityDB.ClientBusinessUnitSites.Where(record => record.BusinessUnitType == 1).ToList();
            //ViewBag.VBRegionsList = regionsList;

            // Ends<<<

            // ========== To display the retrieved site object to user while editing=========== //

            if (siteId != -1)
            {
                local.ClientId = clientId;
                local.isSiteEdit = true;
                local.ClientBusinessUnitSiteId = siteId;
                local.ClientBusinessUnitId = unitId;
                local.OldBusSiteId = siteId;
                local.ClientSelectedBusinessUnit = clientSelectedBusinessUnit; // Added on 8/22/2013

                var clientSelectedBusUnitSIte = entityDB.ClientBusinessUnitSites.FirstOrDefault(m => m.ClientBusinessUnitSiteId == siteId);

                local.BusinessUnitLocation = clientSelectedBusUnitSIte.BusinessUnitLocation;
                
                var siteRepresentatives = entityDB.ClientBusinessUnitSiteRepresentatives.Where(rec => rec.ClientBUSiteID == siteId).ToList();
                                          

                local.BusinessUnitSiteName = clientSelectedBusUnitSIte.SiteName;

                local.BusinessUnitSiteSafetyRepresentative = new List<string>();
                foreach (var representative in siteRepresentatives)
                {
                    local.BusinessUnitSiteSafetyRepresentative.Add(representative.SafetyRepresentative.ToString());
                }
                return View(local);
            }
            local.ClientId = clientId;
            local.ClientBusinessUnitId = unitId;
            local.ClientSelectedBusinessUnit = clientSelectedBusinessUnit; // Added on 8/22/2013

            return View(local);
        }

        /*
            Created Date:  06/11/2013   Created By: Kiran Talluri
            Purpose: To add the Business unit site for the particular Client unit
        */

        [SessionExpireForView(pageType = "popupview")]
        [HttpPost]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "MyAccount", sideMenuName = "Sitetypes", ViewName = "AddClientBusinessUnitSite")]
        public ActionResult AddClientBusinessUnitSite(LocalAddClientBusinessUnitSite local)
        {
            var clientSelectedBusinessUnit = entityDB.ClientBusinessUnits.FirstOrDefault(record => record.ClientBusinessUnitId == local.ClientBusinessUnitId).BusinessUnitName; // Added on 8/22/2013

            // ========= To display the list of Safety Representatives for that particular organization ========== //

            var UserIds = entityDB.SystemUsersOrganizations.Where(rec => rec.OrganizationId == local.ClientId).Select(rec => rec.UserId).ToList();
            local.SafetyRepresentataives = (from systemUsers in entityDB.SystemUsers.Where(rec => UserIds.Contains(rec.UserId)).ToList()
                                            select new SelectListItem
                                            {
                                                Text = systemUsers.Contacts.FirstOrDefault() != null ? systemUsers.Contacts[0].LastName + ", " + systemUsers.Contacts[0].FirstName : systemUsers.Email,
                                                Value = systemUsers.UserId.ToString()
                                            }).ToList();
            local.SafetyRepresentataives = local.SafetyRepresentataives.OrderBy(rec => rec.Text).ToList();
            
            if (ModelState.IsValid)
            {
                //var formValid = true;
                //if (local.BusinessUnitType == true && local.BusinessUnitLocation == "")
                //{
                //    formValid = false;
                //}
                //if (local.BusinessUnitType == false)
                //{
                //    if (local.BusinessUnitRegion == "-1" && string.IsNullOrEmpty(local.BusinessUnitRegionTxt))
                //        formValid = false;
                //}
                //if (!formValid)
                //{
                //    return View(local);
                //}

                if (local.isSiteEdit == false)
                {
                    var busUnitId = entityDB.ClientBusinessUnits.FirstOrDefault(m => m.ClientBusinessUnitId == local.ClientBusinessUnitId && m.ClientId == local.ClientId);

                    ClientBusinessUnitSites clBusUnitSite = new ClientBusinessUnitSites();
                    ClientBusinessUnitSiteRepresentatives clBusUnitSiteRep = new ClientBusinessUnitSiteRepresentatives();

                    clBusUnitSite.ClientBusinessUnitId = local.ClientBusinessUnitId;
                    clBusUnitSite.SiteName = local.BusinessUnitSiteName;
                    clBusUnitSite.InsertDate = DateTime.Now.Date;
                    clBusUnitSite.BusinessUnitType = 0;
                    clBusUnitSite.BusinessUnitLocation = local.BusinessUnitLocation;

                    //if (local.BusinessUnitType == true)
                    //{
                    //    clBusUnitSite.BusinessUnitType = 0;
                    //    clBusUnitSite.BusinessUnitLocation = local.BusinessUnitLocation;
                    //}
                    //else
                    //{
                    //    clBusUnitSite.BusinessUnitType = 1;
                    //    if (local.BusinessUnitRegion == "-1")
                    //    {
                    //        clBusUnitSite.BusinessUnitLocation = local.BusinessUnitRegionTxt;
                    //    }
                    //    else
                    //    {
                    //        clBusUnitSite.BusinessUnitLocation = local.BusinessUnitRegion;
                    //    }
                    //}

                    entityDB.ClientBusinessUnitSites.Add(clBusUnitSite);

                    // ========== To add the multiple safety representatives while adding site ========== //

                    foreach (var rep in local.BusinessUnitSiteSafetyRepresentative)
                    {
                        clBusUnitSiteRep.SafetyRepresentative = new Guid(rep);
                        entityDB.ClientBusinessUnitSiteRepresentatives.Add(clBusUnitSiteRep);
                        entityDB.SaveChanges();
                    }
                }
                // ========== To update the Business unit site while editing ========== //
                else
                {
                    var clientSelectedBusUnitSite = entityDB.ClientBusinessUnitSites.FirstOrDefault(m => m.ClientBusinessUnitSiteId == local.ClientBusinessUnitSiteId);
                    //var safetyRepresentatives = from representatives in entityDB.ClientBusinessUnitSiteRepresentatives
                    //                            where representatives.ClientBUSiteID == local.OldBusSiteId
                    //                            select representatives;
                    var safetyRepresentatives = entityDB.ClientBusinessUnitSiteRepresentatives.Where(rec => rec.ClientBUSiteID == local.OldBusSiteId).ToList();
                                                

                    clientSelectedBusUnitSite.ClientBusinessUnitSiteId = local.ClientBusinessUnitSiteId;
                    clientSelectedBusUnitSite.SiteName = local.BusinessUnitSiteName;
                    clientSelectedBusUnitSite.UpdateDate = DateTime.Now.Date;
                    clientSelectedBusUnitSite.BusinessUnitType = 0;
                    clientSelectedBusUnitSite.BusinessUnitLocation = local.BusinessUnitLocation;

                    //if (local.BusinessUnitType == true)
                    //{
                    //    clientSelectedBusUnitSite.BusinessUnitType = 0;
                    //    clientSelectedBusUnitSite.BusinessUnitLocation = local.BusinessUnitLocation;
                    //}
                    //else
                    //{
                    //    clientSelectedBusUnitSite.BusinessUnitType = 1;
                    //    if (local.BusinessUnitRegion == "-1")
                    //    {
                    //        clientSelectedBusUnitSite.BusinessUnitLocation = local.BusinessUnitRegionTxt;
                    //    }
                    //    else
                    //    {
                    //        clientSelectedBusUnitSite.BusinessUnitLocation = local.BusinessUnitRegion;
                    //    }
                    //}

                    entityDB.Entry(clientSelectedBusUnitSite).State = EntityState.Modified;

                    // =========== To remove the old Safety Representatives while updating ============= //

                    foreach (var representative in safetyRepresentatives)
                    {
                        entityDB.ClientBusinessUnitSiteRepresentatives.Remove(representative);
                    }
                    entityDB.SaveChanges();

                    ClientBusinessUnitSiteRepresentatives clBusUnitSiteRep = new ClientBusinessUnitSiteRepresentatives();
                    clBusUnitSiteRep.ClientBUSiteID = local.ClientBusinessUnitSiteId;

                    // ========== To update the multiple safety representatives while updating site ========== //
                    foreach (var rep in local.BusinessUnitSiteSafetyRepresentative)
                    {
                        clBusUnitSiteRep.SafetyRepresentative = new Guid(rep);
                        entityDB.ClientBusinessUnitSiteRepresentatives.Add(clBusUnitSiteRep);
                        entityDB.SaveChanges();
                    }
                }
                LocalManageSites form = new LocalManageSites();
                form.ClientId = local.ClientId;
                return RedirectToAction("CloseAddClientBusinessUnit", form);
            }
            return View(local);
        }

        /*
            Created Date:  06/11/2013   Created By: Kiran Talluri
            Purpose: To delete the selected client business unit site
        */

        [SessionExpireForView(pageType = "partial")]
        [HttpGet]
        public string deleteSite(long siteId)
        {
            LocalAddClientBusinessUnitSite local = new LocalAddClientBusinessUnitSite();
            local.ClientBusinessUnitSiteId = siteId;

            // Kiran on 3/22/2014
            var prequalSite = entityDB.PrequalificationSites.FirstOrDefault(rec => rec.ClientBusinessUnitSiteId == siteId);
            if (prequalSite == null)
            {
                //var siteRepresentatives = from representatives in entityDB.ClientBusinessUnitSiteRepresentatives
                //                          where representatives.ClientBUSiteID == local.ClientBusinessUnitSiteId
                //                          select representatives;
                var siteRepresentatives = entityDB.ClientBusinessUnitSiteRepresentatives.Where(rec => rec.ClientBUSiteID == local.ClientBusinessUnitSiteId);
                                          
                foreach (var representative in siteRepresentatives)
                {
                    entityDB.ClientBusinessUnitSiteRepresentatives.Remove(representative);
                }
                var site = entityDB.ClientBusinessUnitSites.FirstOrDefault(m => m.ClientBusinessUnitSiteId == local.ClientBusinessUnitSiteId);
                entityDB.ClientBusinessUnitSites.Remove(site);
                entityDB.SaveChanges();
                return "SiteDeleteSuccess";
            }

            return "PrequalSiteExists";
            // Ends<<< 
        }

        /*
            Created Date:  11/23/2013   Created By: Kiran Talluri
            Purpose: To delete the selected client business unit site
        */
        [SessionExpireForView(pageType = "partial")]
        [HttpGet]
        public string DeleteBusinessUnitAndSites(long busUnitId)
        {
            var prequalificationRecordExists = entityDB.PrequalificationSites.FirstOrDefault(record => record.ClientBusinessUnitId == busUnitId);
            if (prequalificationRecordExists == null)
            {
                var clientTemplatesForBURecords = entityDB.ClientTemplatesForBU.Where(record => record.ClientBusinessUnitId == busUnitId).ToList();
                if (clientTemplatesForBURecords != null)
                {
                    foreach (var buRecord in clientTemplatesForBURecords)
                    {
                        entityDB.ClientTemplatesForBU.Remove(buRecord);
                        //entityDB.SaveChanges();
                    }
                }

                var clientBusinessUnitRecord = entityDB.ClientBusinessUnits.FirstOrDefault(record => record.ClientBusinessUnitId == busUnitId);
                var clientBusinessUnitSiteRecords = entityDB.ClientBusinessUnitSites.Where(record => record.ClientBusinessUnitId == busUnitId).ToList();

                foreach (var unitSite in clientBusinessUnitSiteRecords)
                {
                    var clientUnitSiteRepRecord = entityDB.ClientBusinessUnitSiteRepresentatives.FirstOrDefault(record => record.ClientBUSiteID == unitSite.ClientBusinessUnitSiteId);
                    entityDB.ClientBusinessUnitSiteRepresentatives.Remove(clientUnitSiteRepRecord);
                    entityDB.ClientBusinessUnitSites.Remove(unitSite);
                    entityDB.SaveChanges();
                }
                entityDB.ClientBusinessUnits.Remove(clientBusinessUnitRecord);
                entityDB.SaveChanges();
                return "UnitDeleteSuccess";
            }
            else
                return "Prequalification exist";
        }
        // Ends<<<


        [HttpPost]
        public string getUserSessionValue()
        {
            if (Session["UserId"] == null)
            {
                return @Resources.Resources.JQuerySessionExpired;
            }
            else
            {
                return @Resources.Resources.JQuerySessionExist;
            }
        }
        public SelectListItem[] GetStateSelectListItems(bool includeCanada = false)
        {
            var helper = new FVGen3.BusinessLogic.DropdDownDataSupplier();
            //var states = helper.GetDynamicTableData("UnitedStates");
            var states = helper.GetUnitedStatesTableData(-1, false, includeCanada);
            List<SelectListItem> selectLists = new List<SelectListItem>();
            //'<option>' + datasite + '</option>'
            states.ForEach(s => selectLists.Add(new SelectListItem() { Text = s.Key, Value = s.Value }));
            return selectLists.ToArray();
        }


    }
}
