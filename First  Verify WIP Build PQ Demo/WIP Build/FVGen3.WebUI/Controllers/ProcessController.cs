﻿/*
Page Created Date:  07/05/2013
Created By: Kiran Talluri
Purpose: To establish the client-vendor relation, to activate the vendor & to redirect to respective forms
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FVGen3.Domain.Abstract;
using FVGen3.Domain.Entities;
using FVGen3.Domain.Concrete;
using FVGen3.WebUI.Annotations;
using System.Text.RegularExpressions;
using System.Configuration;
using System.Net.Mail;
using System.Text;
using System.Security.Cryptography;
using System.Data.Entity;
using FVGen3.BusinessLogic.Interfaces;
using Resources;
using FVGen3.WebUI.Constants;

namespace FVGen3.WebUI.Controllers
{
    public class ProcessController : BaseController
    {
        private IPrequalificationBusiness _prequalification; private IOrganizationBusiness _organization;
        public ProcessController(IPrequalificationBusiness _prequalification,IOrganizationBusiness _organization)
        {
            this._prequalification = _prequalification;
            this._organization = _organization;
        }
        //
        // GET: /Process/
        EFDbContext entityDB = new EFDbContext();
        public ActionResult Index()
        {
            return View();
        }

        /*
            Created Date:  06/03/2013   Created By: Kiran Talluri
            Purpose: To activate the vendor & to redirect to respective forms
        */
        [HttpGet]
        public ActionResult UserActivation(string userid, String InvitationId)
        {
            string userId = userid.ToString();
            Guid userId1 = new Guid(userId);
            Guid invitationId = InvitationId.Equals("") ? new Guid() : new Guid(InvitationId.ToString());

            HttpContext.Application["isconfirm"] = true;

            SystemUsers newUser = entityDB.SystemUsers.FirstOrDefault(m => m.UserId == userId1);
            newUser.UserStatus = true;

            // Rajesh on 08/23/2013
            // Assign Super User role to vendor when his registration is confirmed by clicking on email link
            //string roleId = entityDB.SystemRoles.FirstOrDefault(rec => rec.RoleName == "Super User" && rec.OrganizationType == "Vendor").RoleId;
            var sysUserOrgRecord = newUser.SystemUsersOrganizations.FirstOrDefault();

            entityDB.Entry(newUser).State = EntityState.Modified;
            entityDB.SaveChanges();

            ViewBag.id = userid;

            //RP DT:-7/15/2013 fixed mulitple inserts for a same invite >>>

            if (InvitationId == null || InvitationId == "")
            {
                return Redirect("~/SystemUsers/Login?messageType=1"); // 1 means Registration Successful
            }
            else
            {
                var currentInvitationIdRecord = entityDB.VendorInviteDetails.FirstOrDefault(record => record.InvitationId == invitationId);
                long clientId = currentInvitationIdRecord.ClientId;
                var clientOrg = _organization.GetOrganization(clientId);
                if (currentInvitationIdRecord.IgnoredInvitation != null && currentInvitationIdRecord.IgnoredInvitation == true)
                    return Redirect("~/SystemUsers/Login"); // 1 means Registration Successful


               
                long VendorOrganizationId = newUser.SystemUsersOrganizations.FirstOrDefault().OrganizationId;
                // >>>>>> Dt: 07/058/2013 Establishing client-vendor relation in PreQualification table

                Prequalification vendorClient_relation = new Prequalification();
                var iSERI = entityDB.SubClients.Any(r => r.ClientId == clientOrg.OrganizationID && r.IsActive==true);
                var clienttemplates = entityDB.ClientTemplates.Where(r => r.ClientID == currentInvitationIdRecord.ClientId && r.Templates.RiskLevel == currentInvitationIdRecord.RiskLevel);
                var clienttemplate = clienttemplates.FirstOrDefault();
                if (clienttemplates.Count() > 1)
                {
                    clienttemplate = clienttemplates.FirstOrDefault(r => r.Templates.IsERI == iSERI);
                }
                //var TemplateID = new Guid();
                //if (clienttemplate != null) TemplateID = clienttemplate.TemplateID;
                if (currentInvitationIdRecord.VendorValidated == null || currentInvitationIdRecord.VendorValidated == false)
                {
                    if (!iSERI)
                    {
                        long vendorId = entityDB.SystemUsersOrganizations.FirstOrDefault(record => record.UserId == userId1).Organizations.OrganizationID;
                        vendorClient_relation.VendorId = vendorId;
                        vendorClient_relation.PrequalificationStatusId = 1;
                        vendorClient_relation.ClientId = currentInvitationIdRecord.ClientId;
                        vendorClient_relation.PrequalificationStart = DateTime.Now;
                        vendorClient_relation.PrequalificationFinish = DateTime.Now;
                        vendorClient_relation.PrequalificationCreate = DateTime.Now;
                        vendorClient_relation.UserIdAsCompleter = userId1;//Rajesh on 7/27/2013
                        if (currentInvitationIdRecord.InvitationSentFrom == 0)
                        {
                            vendorClient_relation.InvitationFrom = LocalConstants.PQInvitationId;
                        }
                        else
                            vendorClient_relation.InvitationFrom = currentInvitationIdRecord.InvitationSentFrom;
                        vendorClient_relation.InvitationSentByUser = currentInvitationIdRecord.InvitationSentByUser;
                        vendorClient_relation.InvitationId =new Guid(InvitationId);


                        if (clienttemplate != null)
                        {
                            vendorClient_relation.PrequalificationStatusId = 3;
                            if (vendorClient_relation.PrequalificationStatusId == 3)
                            {
                                DateTime FinishDateTime = DateTime.Now;
                                var preStatus = entityDB.PrequalificationStatus.FirstOrDefault(rec => rec.PrequalificationStatusId == 3);
                                if (preStatus != null)
                                {
                                    if (preStatus.PeriodLengthUnit.ToLower().Equals("d"))
                                        FinishDateTime = DateTime.Now.AddDays(preStatus.PeriodLength);
                                    else if (preStatus.PeriodLengthUnit.ToLower().Equals("m"))
                                        FinishDateTime = DateTime.Now.AddMonths(preStatus.PeriodLength);
                                    else if (preStatus.PeriodLengthUnit.ToLower().Equals("y"))
                                        FinishDateTime = DateTime.Now.AddYears(preStatus.PeriodLength);
                                }
                                vendorClient_relation.PrequalificationFinish = FinishDateTime;
                            }

                            vendorClient_relation.ClientTemplateId = clienttemplate.ClientTemplateID;
                            vendorClient_relation.locked = clienttemplate.Templates.LockedByDefault;
                            vendorClient_relation.Comments = currentInvitationIdRecord.Comments;
                        }
                        entityDB.Prequalification.Add(vendorClient_relation);
                        entityDB.SaveChanges();
                        logs.Info("Pq Added Successfully ", "userId=" + userid+"&invitationId="+invitationId, "processcontroller/UserActivation");

                    
                  
                   
                    foreach (var location in currentInvitationIdRecord.VendorInviteLocations)
                    {
                        var pqSites = new PrequalificationSites();
                        pqSites.ClientId = currentInvitationIdRecord.ClientId;
                        pqSites.VendorId = currentInvitationIdRecord.VendorId;
                        pqSites.PrequalificationId = vendorClient_relation.PrequalificationId;
                        pqSites.ClientBULocation = location.ClientBusinessUnitSites.BusinessUnitLocation;
                        pqSites.ClientBusinessUnitId = location.ClientBusinessUnitSites.ClientBusinessUnitId;
                        pqSites.ClientBusinessUnitSiteId = location.ClientBusinessUnitSiteID;
                        entityDB.PrequalificationSites.Add(pqSites);
                    }
                    }
                    currentInvitationIdRecord.VendorValidated = true;
                    entityDB.Entry(currentInvitationIdRecord).State = EntityState.Modified;

                    if (!iSERI)
                    {
                        //To add Prequalification Logs
                        PrequalificationStatusChangeLog preQualificationLog = new PrequalificationStatusChangeLog();
                        preQualificationLog.PrequalificationId = vendorClient_relation.PrequalificationId;
                        preQualificationLog.PrequalificationStatusId = vendorClient_relation.PrequalificationStatusId;
                        preQualificationLog.StatusChangeDate = DateTime.Now;
                        preQualificationLog.StatusChangedByUser = new Guid(userid);
                        preQualificationLog.PrequalificationStart = vendorClient_relation.PrequalificationStart;
                        preQualificationLog.PrequalificationFinish = vendorClient_relation.PrequalificationFinish;
                        preQualificationLog.CreatedDate = DateTime.Now;
                        entityDB.PrequalificationStatusChangeLog.Add(preQualificationLog);


                        //>>>Ends
                    }
                    entityDB.SaveChanges();
                    _prequalification.ZeroPayments(vendorClient_relation.PrequalificationId);

                }
                else
                    vendorClient_relation = entityDB.Prequalification.FirstOrDefault(rec => rec.ClientId == currentInvitationIdRecord.ClientId && rec.VendorId == VendorOrganizationId);
                string lastpageurl = "";

                if (iSERI)
                    lastpageurl = "~/Vendor/ListOfPotentialClients";
                else
                {
                   

                     lastpageurl = "~/Prequalification/TemplatesList?PreQualificationId=" + vendorClient_relation.PrequalificationId + "&templateId=" + clienttemplate?.TemplateID;  
                }
                return Redirect("~/SystemUsers/Login?lastLoginPage=" + HttpUtility.UrlEncode(lastpageurl));
            }
            //    long vendorId = entityDB.SystemUsersOrganizations.FirstOrDefault(record => record.UserId == userId1).Organizations.OrganizationID;
            //    vendorClient_relation.VendorId = vendorId;
            //    vendorClient_relation.PrequalificationStatusId = 1;
            //    vendorClient_relation.ClientId = clientId;
            //    vendorClient_relation.PrequalificationStart = DateTime.Now;
            //    vendorClient_relation.PrequalificationFinish = DateTime.Now;
            //    vendorClient_relation.PrequalificationCreate = DateTime.Now;

            //    entityDB.Prequalification.Add(vendorClient_relation);
            //    entityDB.SaveChanges();

            //    // <<<<<<< Establishing client-vendor relation ends

            //    string lastpageurl = "~/Prequalification/TemplatesList?PreQualificationId=" + vendorClient_relation.PrequalificationId;
            //    return Redirect("~/SystemUsers/Login?lastLoginPage=" + lastpageurl);
            //}
            // Multiple inserts fix Ends <<<<

        }


        // Rajesh 07/08/2013
        // User when clicks on the invitation link >>>
        public ActionResult InviteVendor(Guid? InvitationId) // Allowed nulls to address the issue if no email exists in our database
        {
            string lastpageurl = "";
            if (InvitationId == null)
            {
                return Redirect("~/Vendor/VendorRegistration");
            }
            VendorInviteDetails current_InvitedVendorDetails = entityDB.VendorInviteDetails.Find(InvitationId);
            var iSERI = entityDB.SubClients.Any(r => r.ClientId == current_InvitedVendorDetails.ClientId && r.IsActive==true);

            var clienttemplates = entityDB.ClientTemplates.Where(r => r.ClientID == current_InvitedVendorDetails.ClientId && r.Templates.RiskLevel == current_InvitedVendorDetails.RiskLevel);
            var clienttemplate = clienttemplates.FirstOrDefault();
            if (clienttemplates.Count()>1)
            {
                clienttemplate = clienttemplates.FirstOrDefault(r => r.Templates.IsERI == iSERI);
            }
           
            var currentClient = _organization.GetOrganization(current_InvitedVendorDetails.ClientId);
            if (current_InvitedVendorDetails == null)
                return Redirect("~/Vendor/VendorRegistration");

            if (current_InvitedVendorDetails.IgnoredInvitation != null && current_InvitedVendorDetails.IgnoredInvitation == true)
                return Redirect("~/Vendor/VendorRegistration");


            //To find whether the user exist or not from SystemUsers
            SystemUsers VendorDetails = entityDB.SystemUsers.FirstOrDefault(rec => rec.UserName == current_InvitedVendorDetails.VendorEmail);

            if (VendorDetails == null)//User Not Exist
            {
                return Redirect("~/Vendor/VendorRegistration?InvitationId=" + InvitationId);
            }
            else
            {
                long VendorOrganizationId = VendorDetails.SystemUsersOrganizations.FirstOrDefault().OrganizationId;
                Prequalification vendorClient_relation = entityDB.Prequalification.FirstOrDefault(rec => rec.ClientId == current_InvitedVendorDetails.ClientId && rec.VendorId == VendorOrganizationId);
                if (!iSERI)
                {
                    if (vendorClient_relation == null)//current_InvitedVendorDetails.VendorValidated == null || current_InvitedVendorDetails.VendorValidated == false)
                    {
                        vendorClient_relation = new Prequalification();
                        long vendorId = entityDB.SystemUsersOrganizations.FirstOrDefault(record => record.UserId == VendorDetails.UserId).Organizations.OrganizationID;
                        vendorClient_relation.VendorId = vendorId;
                        vendorClient_relation.PrequalificationStatusId = 1;
                        vendorClient_relation.ClientId = current_InvitedVendorDetails.ClientId;
                        vendorClient_relation.PrequalificationStart = DateTime.Now;
                        vendorClient_relation.PrequalificationFinish = DateTime.Now;
                        vendorClient_relation.PrequalificationCreate = DateTime.Now;
                        if(current_InvitedVendorDetails.InvitationSentFrom==0)
                        {
                            vendorClient_relation.InvitationFrom = LocalConstants.PQInvitationId;
                        }
                        else
                            vendorClient_relation.InvitationFrom = current_InvitedVendorDetails.InvitationSentFrom;
                        vendorClient_relation.InvitationSentByUser = current_InvitedVendorDetails.InvitationSentByUser;
                        vendorClient_relation.InvitationId = InvitationId;
                        vendorClient_relation.UserIdAsCompleter = VendorDetails.UserId;//Rajesh on 7/27/2013
                        entityDB.Prequalification.Add(vendorClient_relation);
                        entityDB.SaveChanges();
                        logs.Info("Pq Added Successfully "+ vendorClient_relation.PrequalificationId, "InvitationId=" + InvitationId, "Processcontroller/inviteVendor");

                        _prequalification.ZeroPayments(vendorClient_relation.PrequalificationId);
                        //To add Prequalification Logs
                        //PrequalificationStatusChangeLog preQualificationLog = new PrequalificationStatusChangeLog();
                        //preQualificationLog.PrequalificationId = vendorClient_relation.PrequalificationId;
                        //preQualificationLog.PrequalificationStatusId = vendorClient_relation.PrequalificationStatusId;
                        //preQualificationLog.StatusChangeDate = DateTime.Now;
                        //preQualificationLog.StatusChangedByUser = new Guid(Session["UserId"].ToString());
                        //entityDB.PrequalificationStatusChangeLog.Add(preQualificationLog);
                        //>>>Ends
                     
                       
                        foreach (var location in current_InvitedVendorDetails.VendorInviteLocations)
                        {
                            var pqSites = new PrequalificationSites();
                            pqSites.ClientId = current_InvitedVendorDetails.ClientId;
                            pqSites.VendorId = current_InvitedVendorDetails.VendorId;
                            pqSites.PrequalificationId = vendorClient_relation.PrequalificationId;
                            pqSites.ClientBULocation = location.ClientBusinessUnitSites.BusinessUnitLocation;
                            pqSites.ClientBusinessUnitId = location.ClientBusinessUnitSites.ClientBusinessUnitId;
                            pqSites.ClientBusinessUnitSiteId = location.ClientBusinessUnitSiteID;
                            entityDB.PrequalificationSites.Add(pqSites);
                        }

                    }
                }
                else
                    vendorClient_relation = entityDB.Prequalification.FirstOrDefault(rec => rec.ClientId == current_InvitedVendorDetails.ClientId && rec.VendorId == VendorOrganizationId);
                if (clienttemplate != null&& vendorClient_relation.ClientTemplates==null)
                {
                    vendorClient_relation.PrequalificationStatusId = 3;
                    vendorClient_relation.ClientTemplateId = clienttemplate.ClientTemplateID;
                    vendorClient_relation.locked = clienttemplate.Templates.LockedByDefault;
                    vendorClient_relation.Comments = current_InvitedVendorDetails.Comments;

                    var preStatus = entityDB.PrequalificationStatus.FirstOrDefault(rec => rec.PrequalificationStatusId == 3);
                    if (preStatus != null)
                    {
                        if (preStatus.PeriodLengthUnit.ToLower().Equals("d"))
                            vendorClient_relation.PrequalificationFinish = DateTime.Now.AddDays(preStatus.PeriodLength);
                        else if (preStatus.PeriodLengthUnit.ToLower().Equals("m"))
                            vendorClient_relation.PrequalificationFinish = DateTime.Now.AddMonths(preStatus.PeriodLength);
                        else if (preStatus.PeriodLengthUnit.ToLower().Equals("y"))
                            vendorClient_relation.PrequalificationFinish = DateTime.Now.AddYears(preStatus.PeriodLength);
                    }

                }
                entityDB.Entry(vendorClient_relation).State = EntityState.Modified;
                current_InvitedVendorDetails.VendorValidated = true;
                entityDB.Entry(current_InvitedVendorDetails).State = EntityState.Modified;
                entityDB.SaveChanges();
                //Rajesh on 09/07/2013 >>>>
                HttpCookie cookie = Request.Cookies["Login"];
               
                if (currentClient.OrganizationType==OrganizationType.SuperClient && vendorClient_relation == null)
                {
                    lastpageurl = "~/Vendor/ListOfPotentialClients";

                    Session["SelectedVendorId"] = current_InvitedVendorDetails.VendorId;
                }
                else if(vendorClient_relation == null)
                {
                    lastpageurl = "~/Vendor/ListOfPotentialClients";
                    Session["SelectedVendorId"] = current_InvitedVendorDetails.VendorId;
                }
                else if (cookie == null)
                {
                    lastpageurl = "~/Prequalification/TemplatesList?PreQualificationId=" + vendorClient_relation.PrequalificationId + "&templateId=" +clienttemplate?.TemplateID;
                }
                else if (VendorDetails.UserName.Equals(cookie.Values["UserName"].ToString()) || cookie.Values["isLogin"].Equals("false"))
                    lastpageurl = "~/Prequalification/TemplatesList?PreQualificationId=" + vendorClient_relation.PrequalificationId + "&templateId=" + clienttemplate?.TemplateID;

                else
                {
                    Response.Cookies["Login"]["UserName"] = cookie.Values["UserName"];
                    //Response.Cookies["Login"].Expires = DateTime.Now.AddYears(1);
                    Response.Cookies["Login"]["Password"] = cookie.Values["Password"];
                    Response.Cookies["Login"]["isLogin"] = "false";

                    Response.Cookies["Login"].Expires = DateTime.Now.AddYears(1);
                }

                //Ends<<<

            }

            return Redirect("~/SystemUsers/Login?lastLoginPage=" + HttpUtility.UrlEncode(lastpageurl));

        }
        // User when clicks on invitation link Ends <<<
        public ActionResult FVInviteVendor(string Email, long clientId)
        {
            VendorInviteDetails current_InvitedVendorDetails = entityDB.VendorInviteDetails.FirstOrDefault(rec => rec.VendorEmail == Email && rec.ClientId == clientId);
            if (current_InvitedVendorDetails == null) // If email does not exists in our database & invited through InfusionSoft, then this situation comes into picture.
                return Redirect("InviteVendor");
            else
                return Redirect("InviteVendor?InvitationId=" + current_InvitedVendorDetails.InvitationId);

        }
    }
}
