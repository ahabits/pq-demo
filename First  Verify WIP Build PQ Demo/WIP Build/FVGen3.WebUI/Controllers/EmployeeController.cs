﻿/*
Page Created Date:  10/21/2014
Created By: Kiran Talluri
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FVGen3.Domain.Concrete;
using FVGen3.Domain.Entities;
using FVGen3.WebUI.Annotations;
using System.Net.Mail;
using System.Configuration;
using System.IO;
using System.Data.Entity;
using System.Text.RegularExpressions;
using FVGen3.Domain.Abstract;
using System.Text;
using System.Net;
using AhaApps.Libraries.Extensions;
using AhaApps.Libraries.Logging;
using SelectPdf;
using FVGen3.WebUI.Utilities;
using PayPal.Api;
using PayPal.Sample;
using PayPal.Sample.Utilities;
using FVGen3.BusinessLogic.Interfaces;
using Resources;
using FVGen3.WebUI.Constants;

namespace FVGen3.WebUI.Controllers
{
    public class EmployeeController : BaseController
    {
        //
        // GET: /Employee/

        EFDbContext entityDB = new EFDbContext();
        private IEmployeeBusiness _employee;
        private IQuizBusiness _quizBusiness;
        private ISystemUserBusiness _sysUserBusiness;
        private IOrganizationBusiness _organizationBusiness;

        private EFSystemUsersOrganizationsQuizzesCategoriesRepository sysUserOrgQuizCategoriesRepository;
        private EFEmployeeQuizCategoriesRepository empQuizCategoriesRepository;
        private RequestFlow flow = new RequestFlow();
        public EmployeeController(EFEmployeeQuizCategoriesRepository empQuizCategoriesRepository, IEmployeeBusiness _employee, IQuizBusiness quizBusiness, ISystemUserBusiness sysUserBusiness, IOrganizationBusiness orgBusiness)
        {
            this.empQuizCategoriesRepository = empQuizCategoriesRepository;
            this._employee = _employee;
            this._quizBusiness = quizBusiness;
            this._sysUserBusiness = sysUserBusiness;
            this._organizationBusiness = orgBusiness;
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        [SessionExpire]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "MyAccount", sideMenuName = "EmployeesForTraining", ViewName = "AddEmployees")]
        public ActionResult AddEmployees(bool? notified)
        {
            if (notified != null && notified == true)
            {
                ViewBag.alertTraining = "Training";
            }
            var employee_RoleId = EmployeeRole.EmployeeRoleId;
                      //  var employee_RoleId = entityDB.SystemRoles.FirstOrDefault(record => record.RoleName == "Employee Training").RoleId;
            long currentOrgId = (long)Session["currentOrgId"];
            //var employee_sysUserOrgRoles = entityDB.SystemUsersOrganizationsRoles.Where(record => record.SysRoleId == employee_RoleId).ToList();

            //var employee_sysUserOrgs = new List<SystemUsersOrganizations>();

            //foreach (var employee_sysUserOrgRole in employee_sysUserOrgRoles)
            //{
            //    var employee_sysUserOrg = employee_sysUserOrgRole.SystemUsersOrganizations;
            //    if (employee_sysUserOrg.OrganizationId == currentOrgId)
            //    {
            //        employee_sysUserOrgs.Add(employee_sysUserOrg);
            //    }
            //}
            var employee_sysUserOrgs = entityDB.SystemUsersOrganizationsRoles.Where(record => employee_RoleId.Contains(record.SysRoleId) )
                .Select(rec => rec.SystemUsersOrganizations).Where(rec => rec.OrganizationId == currentOrgId).ToList();
            // Kiran on 11/29/2014
            var IsNskVendor = entityDB.LatestPrequalification.Any(r => r.ClientId == 9834 && r.VendorId == currentOrgId);
            ViewBag.IsNskVendor = IsNskVendor;
            var filePath = Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["EmployerGuideToAddEmployees"]));
            var file = Directory.GetFiles(filePath).FirstOrDefault();

            if (file != null)
            {
                var fileName = Path.GetFileName(file);
                ViewBag.VBEmployerGuide = fileName;
            }
            // Ends<<<
            // Kiran on 01/20/2015
            //Rajesh on 1/6/2015
            //filePath = Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["EmployeeGuide"]));
            //file = Directory.GetFiles(filePath).FirstOrDefault();

            //if (file != null)
            //{
            //    var fileName = Path.GetFileName(file);
            //    ViewBag.VBEmployeeGuide = fileName;
            //}
            // Ends<<<
            ViewBag.VBEmployeeSysUserOrgs = employee_sysUserOrgs.OrderBy(rec => rec.SystemUsers.Contacts[0].LastName).ToList(); // Kiran on 02/03/2015 for sorting the employees by lastname.
            return View();
        }
        //Rajesh on 12/2/2014
        [HttpGet]
        public String EditUserEmail(Guid userId, String userName, String firstName, String lastName)
        {
            if (entityDB.SystemUsers.FirstOrDefault(rec => rec.UserId != userId && rec.UserName == userName.Trim()) == null)
            {
                var currentUser = entityDB.SystemUsers.Find(userId);
                //Check to see if email
                if (IsValidEmail(userName))
                {

                    currentUser.UserName = userName;
                    currentUser.Email = userName;
                    currentUser.LoweredEmail = userName.ToLower();
                }
                else
                {
                    currentUser.UserName = userName;
                    currentUser.Email = "";
                    currentUser.LoweredEmail = "";

                }
                if (currentUser.Contacts.Any() && @Session["RoleName"].ToString().Equals("Admin"))
                {
                    var contact = currentUser.Contacts.FirstOrDefault();
                    contact.FirstName = firstName;
                    contact.LastName = lastName;
                }
                entityDB.Entry(currentUser).State = EntityState.Modified;
                entityDB.SaveChanges();
            }
            else { return "UserExist"; }
            return "Ok";
        }
        public ActionResult EmployeePrivacyPolicy()
        {
            return PartialView();
        }
        public bool IsValidEmail(string strIn)
        {
            try
            {
                MailAddress m = new MailAddress(strIn);

                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        [SessionExpire]
        [HeaderAndSidebar(headerName = "MyAccount", sideMenuName = "EmployeesForTraining", ViewName = "AddEmployees")]
        public ActionResult EditEmployeeEmails(long VendorOrgId)
        {
            var employee_RoleId = EmployeeRole.EmployeeRoleId;
         //  var employee_RoleId = entityDB.SystemRoles.FirstOrDefault(record => record.RoleName == "Employee Training").RoleId;
            List<SystemUsersOrganizations> employee_sysUserOrgRoles = null;
            employee_sysUserOrgRoles = entityDB.SystemUsersOrganizationsRoles.Where(record => employee_RoleId.Contains(record.SysRoleId)).Select(rec => rec.SystemUsersOrganizations).Where(rec => rec.OrganizationId == VendorOrgId && rec.SystemUsers.UserStatus != false).ToList();
            ViewBag.VBEmployeeSysUserOrgs = employee_sysUserOrgRoles.OrderBy(rec => rec.SystemUsers.Contacts[0].LastName).ToList(); // Kiran on 01/31/2015 to sort the employees order by name.
            return View();
        }
        //Ends<<<

        [HttpPost]
        [SessionExpire]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "MyAccount", sideMenuName = "EmployeesForTraining", ViewName = "AddEmployees")]
        public ActionResult AddEmployees(LocalAddEmployees form)
        {
            if (form.Employees != null)
            {
                // kiran on 11/8/2014
                var emailTemplateFormatForEmployeeInvitation = entityDB.EmailTemplates.FirstOrDefault(rec => rec.EmailUsedFor == 6);
                var subject = emailTemplateFormatForEmployeeInvitation.EmailSubject;
                var body = emailTemplateFormatForEmployeeInvitation.EmailBody;
                // Ends<<<
                var orgType = Session["RoleName"].ToString();
                if (orgType.ToLower() == "vendor")
                {
                    foreach (var employee in form.Employees)
                    {
                        body = emailTemplateFormatForEmployeeInvitation.EmailBody; // Kiran on 11/11/2014
                        SystemApplicationId sysAppId = entityDB.SystemApplicationId.FirstOrDefault();

                        SystemUsersOrganizations currenrtUser_sysUserOrg = new SystemUsersOrganizations();

                        SystemUsersOrganizationsRoles currentUser_sysUserOrgRole = new SystemUsersOrganizationsRoles();

                        currenrtUser_sysUserOrg.OrganizationId = (long)Session["currentOrgId"];

                        currenrtUser_sysUserOrg.SystemUsers = new SystemUsers();

                        currenrtUser_sysUserOrg.SystemUsers.ApplicationId = sysAppId.ApplicationId;
                        currenrtUser_sysUserOrg.SystemUsers.UserName = employee.Email;
                        var fourLetterUniqueKeyForEmpployee = Utilities.EncryptionDecryption.GetUniquePasswordForEmployee();
                        currenrtUser_sysUserOrg.SystemUsers.Password = Utilities.EncryptionDecryption.EncryptData(fourLetterUniqueKeyForEmpployee);
                        if (employee.UserHasEmail)
                        {
                            currenrtUser_sysUserOrg.SystemUsers.Email = employee.Email;
                        }
                        currenrtUser_sysUserOrg.SystemUsers.CreateDate = DateTime.Now;
                        currenrtUser_sysUserOrg.SystemUsers.LastLoginDate = DateTime.Now;
                        currenrtUser_sysUserOrg.SystemUsers.UserStatus = true;

                        Contact currentUser_Contact = new Contact();

                        currentUser_Contact.FirstName = employee.FirstName;
                        currentUser_Contact.LastName = employee.LastName;
                        currentUser_Contact.ContactTitle = employee.Title;
                        currentUser_Contact.PhoneNumber = employee.PhoneNumber;
                        if (employee.DocumentType == 1)
                        {
                            currentUser_Contact.DocumentType = employee.DocumentType;
                            currentUser_Contact.DriversLicenseLast4Digits = employee.Last4Digits;
                        }
                        else if (employee.DocumentType == 2)
                        {
                            currentUser_Contact.DocumentType = employee.DocumentType;
                            currentUser_Contact.GreenCardLast4Digits = employee.Last4Digits;
                        }
                        else if (employee.DocumentType == 3)
                        {
                            currentUser_Contact.DocumentType = employee.DocumentType;
                            currentUser_Contact.SocialSecurityNumberLast4Digits = employee.Last4Digits;
                        }
                        else if (employee.DocumentType == 4)
                        {
                            currentUser_Contact.DocumentType = employee.DocumentType;
                            currentUser_Contact.PhoneNumberLast4Digits = employee.Last4Digits;
                        }

                            entityDB.SystemUsers.Add(currenrtUser_sysUserOrg.SystemUsers);
                        entityDB.Contact.Add(currentUser_Contact);
                        entityDB.SystemUsersOrganizations.Add(currenrtUser_sysUserOrg);

                        currentUser_sysUserOrgRole.SysUserOrgId = currenrtUser_sysUserOrg.SysUserOrganizationId;
                        currentUser_sysUserOrgRole.SysRoleId = EmployeeRole.VendorEmpRoleId;
                        //currentUser_sysUserOrgRole.SysRoleId = entityDB.SystemRoles.FirstOrDefault(record => record.RoleName == "Employee Training").RoleId;

                        entityDB.SystemUsersOrganizationsRoles.Add(currentUser_sysUserOrgRole);

                        // kiran on 10/29/2014
                        //var vendorOrgId = (long)Session["currentOrgId"];
                        ////var vendorPrequalifications = entityDB.Prequalification.Where(rec => rec.VendorId == vendorOrgId && rec.PrequalificationStatusId == 9).ToList();
                        //var vendorPrequalifications = entityDB.Prequalification.Where(rec => rec.VendorId == vendorOrgId && rec.PrequalificationStatusId != 14).ToList(); // Kiran on 02/05/2015 given the provision to assign trainings under exception/probation related statuses, prequalified status // kiran on 02/07/2015 to assign trainings to employee under any status except N/A as per the email 02/07/2015.
                        //bool mailSent = false; // Kiran on 01/13/2015

                        //foreach (var prequal in vendorPrequalifications)
                        //{
                        //    if (prequal.Client.TrainingRequired == true && prequal.PrequalificationFinish >= DateTime.Now)
                        //    {
                        //        var clientQuizTemplates = entityDB.ClientTemplates.Where(rec => rec.ClientID == prequal.Client.OrganizationID).ToList();
                        //        foreach (var clientQuizTemplate in clientQuizTemplates)
                        //        {
                        //            if (clientQuizTemplate.Templates.TemplateType == 1 && clientQuizTemplate.Templates.TemplateStatus == 1)
                        //            {
                        //                if (prequal.PrequalificationSites != null)
                        //                {
                        //                    var prequalSites = (from prequalificationSites in entityDB.PrequalificationSites.Where(rec => rec.PrequalificationId == prequal.PrequalificationId).ToList()

                        //                                        select prequalificationSites.ClientBusinessUnitId).ToList();

                        //                    var quizClientTemplateBus = entityDB.ClientTemplatesForBU.Where(rec => prequalSites.Contains(rec.ClientBusinessUnitId) && rec.ClientTemplateId == clientQuizTemplate.ClientTemplateID).ToList(); // Kiran on 11/18/2014

                        //                    if (quizClientTemplateBus != null && quizClientTemplateBus.Count != 0) // Kiran on 11/8/2014
                        //                    {
                        //                        SystemUsersOrganizationsQuizzes addQuizForEmployee = new SystemUsersOrganizationsQuizzes();
                        //                        addQuizForEmployee.SysUserOrgId = currenrtUser_sysUserOrg.SysUserOrganizationId;
                        //                        addQuizForEmployee.ClientTemplateId = clientQuizTemplate.ClientTemplateID;
                        //                        // Kiran on 12/18/2014
                        //                        addQuizForEmployee.VendorId = prequal.VendorId;
                        //                        addQuizForEmployee.ClientId = prequal.ClientId;
                        //                        // Ends<<<
                        //                        entityDB.SystemUsersOrganizationsQuizzes.Add(addQuizForEmployee);
                        //                    }
                        //                }
                        //            }
                        //        }

                        //        if (mailSent == false) // Kiran on 01/13/2015
                        //        {
                        //            // kiran on 11/8/2014
                        //            if (clientQuizTemplates != null)
                        //            {
                        //                // Kiran on 12/2/2014
                        //                body = emailTemplateFormatForEmployeeInvitation.EmailBody;
                        //                subject = emailTemplateFormatForEmployeeInvitation.EmailSubject;
                        //                // Ends<<<

                        //                Guid vendorLoggedInUserId = (Guid)Session["UserId"];
                        //                var vendorLoggedInUser = entityDB.SystemUsers.Find(vendorLoggedInUserId);
                        //                var vendorUserName = "";
                        //                try
                        //                {
                        //                    vendorUserName = vendorLoggedInUser.Contacts[0].FirstName + " " + vendorLoggedInUser.Contacts[0].LastName;
                        //                }
                        //                catch 
                        //                {
                        //                    vendorUserName = vendorLoggedInUser.Email;
                        //                }
                        //                subject = subject.Replace("[ClientName]", prequal.Client.Name);
                        //                body = body.Replace("[Date]", DateTime.Now + "");
                        //                body = body.Replace("[EmployeeName]", employee.FirstName + " " + employee.LastName);
                        //                body = body.Replace("[CLIENTNAME]", prequal.Client.Name);
                        //                body = body.Replace("[VENDORNAME]", prequal.Vendor.Name);
                        //                body = body.Replace("[VendorUserName]", vendorUserName);
                        //                body = body.Replace("[Email]", employee.Email);
                        //                body = body.Replace("[Password]", Utilities.EncryptionDecryption.Decryptdata(currenrtUser_sysUserOrg.SystemUsers.Password));
                        //                body = body + "</br>" + emailTemplateFormatForEmployeeInvitation.CommentText;
                        //                // 11/11/2014

                        //                List<Attachment> mailAttachments = new List<Attachment>();
                        //                if (emailTemplateFormatForEmployeeInvitation.EmailTemplateAttachments != null && emailTemplateFormatForEmployeeInvitation.EmailTemplateAttachments.Count() > 0)
                        //                {
                        //                    foreach (var file in emailTemplateFormatForEmployeeInvitation.EmailTemplateAttachments)
                        //                    {
                        //                        var filePath = Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["EmailTemplateAttachmentsPath"]), emailTemplateFormatForEmployeeInvitation.EmailTemplateID + file.AttachmentId.ToString() + file.FileName);

                        //                        var f = new FileStream(filePath, FileMode.Open, FileAccess.Read);
                        //                        if (filePath != null)
                        //                        {
                        //                            mailAttachments.Add(new Attachment(f, file.FileName));
                        //                        }
                        //                    }
                        //                }

                        //                var message = Utilities.SendMail.sendMail(employee.Email, body, subject, "", "", mailAttachments);

                        //                // Ends<<<
                        //            }
                        //        }
                        //        mailSent = true; // Kiran on 01/13/2015
                        //    }
                        //}
                        // Ends<<

                        entityDB.SaveChanges();

                    }
                }
            }
            return RedirectToAction("AddEmployees");
        }

        public String deleteEmployee(long sysUserOrgId)
        {
            return _employee.deleteEmployee(sysUserOrgId);
        }

        [SessionExpire]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "MyAccount", sideMenuName = "EmployeesForTraining", ViewName = "VendorEmployeeQuizzes")]
        public ActionResult VendorEmployeeQuizzes(Guid userId)
        {
            LocalVendorEmployeeQuizzes employee = new LocalVendorEmployeeQuizzes();
            var employeeContactDetails = entityDB.Contact.FirstOrDefault(record => record.UserId == userId);
            employee.EmployeeName = employeeContactDetails.LastName + ", " + employeeContactDetails.FirstName; // Kiran on 03/30/2015
            employee.UserId = userId;
            // Kiran on 1/31/2014
            var currentEmpSystemUsersRecord = entityDB.SystemUsers.Find(userId);
            ViewBag.VBEmpPassword = Utilities.EncryptionDecryption.Decryptdata(currentEmpSystemUsersRecord.Password);
            ViewBag.VBEmpUserName = currentEmpSystemUsersRecord.UserName;
            ViewBag.VendorID = entityDB.SystemUsersOrganizations.FirstOrDefault(rec => rec.UserId == userId).OrganizationId;
            //Ends<<<
            return View(employee);

        }

        [HttpGet]
        [SessionExpireForView]
        [OutputCache(Duration = 0)]
        public ActionResult PartialEmployeeQuizList(Guid userId, int? type)//if null or 0 Admin vendor 1-trainee
        {
           
            var trainingCategories = empQuizCategoriesRepository.GetTrainingCategories();
              ViewBag.orgs = entityDB.Organizations;
            ViewBag.VBLoggedInUserIsVendorHasNoAccess = false;
            if (@Session["RoleName"].ToString().Equals("Vendor"))
            {
                trainingCategories = trainingCategories.Where(rec => rec.AccessType == 2).ToList();
            }
            if (trainingCategories == null || trainingCategories.Count() == 0)
            {
                ViewBag.VBLoggedInUserIsVendorHasNoAccess = true;
            }
            // kiran on 03/13/2015 Ends<<<

            // Kiran on 01/20/2015
           var EmpQuizs = _employee.EmployeeQuizs(userId, type,(Guid)Session["UserId"], null);
            ViewBag.Clients = EmpQuizs.ClientQuizzes.DistinctBy(r => r.ClientID).OrderBy(r => r.ClientName).Select(r => r.ClientName);
           EmpQuizs.LocalEmployeeGuideDocumentsGroupedByClient.ForEach(r => r.FileNames = getFileNames(r.OrganizationId));
            //List<LocalEmployeeGuideDocumentsGroupedByClient> employeeGuidesGroupedByClient = new List<LocalEmployeeGuideDocumentsGroupedByClient>();
            //var userSysUserOrg = entityDB.SystemUsersOrganizations.FirstOrDefault(record => record.UserId == userId); // Kiran on 02/03/2015
            //var currentUserSysUserOrgId = entityDB.SystemUsersOrganizations.FirstOrDefault(record => record.UserId == userId).SysUserOrganizationId;
            //var currentUserSysUserOrgQuiz = entityDB.SystemUsersOrganizationsQuizzes.Where(record => record.SysUserOrgId == currentUserSysUserOrgId && (record.QuizRemoved == false || record.QuizRemoved == null)).ToList(); // Kiran on 11/18/2014

            //long clientID = -1;


            //var sysUserOrgQuizGroupByClient = currentUserSysUserOrgQuiz.GroupBy(record => record.ClientTemplates.Organizations.Name);
            //ViewBag.VBsysUserOrgQuizGroupByClient = sysUserOrgQuizGroupByClient.ToList();
            //foreach (var client in sysUserOrgQuizGroupByClient)
            //{
            //    LocalEmployeeGuideDocumentsGroupedByClient employeeGuide = new LocalEmployeeGuideDocumentsGroupedByClient();
            //    employeeGuide.key = client.Key;
            //    employeeGuide.OrganizationId = client.FirstOrDefault().ClientId;
            //    clientID = client.FirstOrDefault().ClientId.Value;
            //    employeeGuide.FileNames = getFileNames(employeeGuide.OrganizationId);
            //    // Kiran on 02/03/2015
            //    var vendorLatestPrequalification = entityDB.Prequalification.Where(rec => rec.ClientId == employeeGuide.OrganizationId && rec.VendorId == userSysUserOrg.OrganizationId).OrderByDescending(rec => rec.PrequalificationStart).ToList();

            //    employeeGuide.prequalifcation = vendorLatestPrequalification.FirstOrDefault();

            //    if (vendorLatestPrequalification.Any() && vendorLatestPrequalification.FirstOrDefault().PrequalificationStatusId != 14 && vendorLatestPrequalification.FirstOrDefault().PrequalificationFinish > DateTime.Now) // Kiran on 02/05/2015 // Kiran on 02/12/2015
            //    //if ((vendorLatestPrequalification.FirstOrDefault().PrequalificationStatusId == 9) && vendorLatestPrequalification.FirstOrDefault().PrequalificationFinish > DateTime.Now)
            //    {
            //        employeeGuide.qualified = true;
            //    }
            //    if (employeeGuide.qualified == null || employeeGuide.qualified == false)
            //    {
            //        vendorLatestPrequalification = entityDB.Prequalification.Where(rec => rec.Client.OrganizationType == OrganizationType.SuperClient && rec.VendorId == userSysUserOrg.OrganizationId).OrderByDescending(rec => rec.PrequalificationStart).ToList();
            //        if (vendorLatestPrequalification.Any())
            //        {
            //            employeeGuide.prequalifcation = vendorLatestPrequalification.FirstOrDefault();

            //            if (vendorLatestPrequalification.FirstOrDefault().PrequalificationStatusId != 14 && vendorLatestPrequalification.FirstOrDefault().PrequalificationFinish > DateTime.Now) // Kiran on 02/05/2015 // Kiran on 02/12/2015
            //                                                                                                                                                                                     //if ((vendorLatestPrequalification.FirstOrDefault().PrequalificationStatusId == 9) && vendorLatestPrequalification.FirstOrDefault().PrequalificationFinish > DateTime.Now)
            //            {
            //                employeeGuide.qualified = true;
            //            }
            //        }
            //    }

            //    employeeGuidesGroupedByClient.Add(employeeGuide);
            //}
            //ViewBag.VBEmployeeGuidesGroupedByClient = employeeGuidesGroupedByClient;
            //// Ends<<<

            //// Kiran on 2/5/2014
            //var loggedinUserGuid = (Guid)Session["UserId"];
            //var userSysOrgId = entityDB.SystemUsersOrganizations.FirstOrDefault(rec => rec.UserId == loggedinUserGuid).SysUserOrganizationId;
            //var employeeTrainingRoleId = entityDB.SystemRoles.FirstOrDefault(rec => rec.RoleName == "Employee Training").RoleId;
            //var userRoles = entityDB.SystemUsersOrganizationsRoles.FirstOrDefault(rec => rec.SysUserOrgId == userSysOrgId && rec.SysRoleId == employeeTrainingRoleId);

            //if (userRoles != null)
            //{
            //    ViewBag.Employee = true;
            //}

            //bool vendorIsExemptFromTrainingFee = false;
            //var props = entityDB.ClientVendorProperties.FirstOrDefault(r => r.ClientID == clientID && r.VendorID == userSysUserOrg.OrganizationId);
            //if (props != null)
            //{
            //    if (props.TrainingPaymentExempt != null && props.TrainingPaymentExempt == true)
            //    {
            //        vendorIsExemptFromTrainingFee = true;
            //    }

            //}
            //ViewBag.VendorIsTrainingExempt = vendorIsExemptFromTrainingFee;
            // Ends<<<
            ViewBag.userType = type;
            return PartialView(EmpQuizs);
        }
        //public string DeleteTraining(long SysUserOrgQuizId)
        //{
        //    var DeleteEmployee = _employee.DeleteTrainings(SysUserOrgQuizId);

        //    return "EmployeeDeleted";
        //}

        // Kiran on 01/20/2015
        public List<string> getFileNames(long? clientId)
        {
            List<string> fileNames = new List<string>();
            if (clientId != null)

                try
                {
                    var filePath = Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["EmployeeGuide"]), clientId + "");
                    var files = Directory.GetFiles(filePath);

                    if (files != null)
                    {
                        foreach (var file in files)
                            fileNames.Add(Path.GetFileName(file));

                    }
                }
                catch { }
            return fileNames;
        }
        // Ends<<<
        //        function GetServerReccomendation(nameBase) {
        [HttpGet]
        public string GetUsernameReccomendation(string nameBase)
        {
            var usernames = entityDB.SystemUsers.Where(rec => rec.UserName.ToLower().StartsWith(nameBase.ToLower())).Select(s => s.UserName).ToList();
            if (usernames == null)
            {
                return nameBase;
            }
            int x = 100;

            while (true)
            {
                if (usernames.Contains(nameBase + x.ToString()))
                {
                    x++;
                }
                else
                {
                    return nameBase + x.ToString();
                }
            }

        }

        //function CheckUserNameExists(username) {
        [HttpGet]
        public string CheckUserNameExists(string username)
        {
            var emailRecord = entityDB.SystemUsers.FirstOrDefault(rec => rec.UserName.ToLower() == username.ToLower());
            if (emailRecord != null)
            {
                return "Username Exists";
            }
            return "";
        }


        // Kiran on 11/3/2014 to stop duplicate email insertion
        [HttpGet]
        public string CheckEmailExists(string Email)
        {
            var emailRecord = entityDB.SystemUsers.FirstOrDefault(rec => rec.Email.ToLower() == Email.ToLower());
            if (emailRecord != null)
            {
                return "Email Exists";
            }
            return "";
        }
        /* kiran on 11/4/2014
                   To assign the checklists to employee */
        //[HttpGet]
        //[SessionExpire]
        //[HeaderAndSidebar(headerName = "PREQUALIFICATIONREQUIREMENTS", sideMenuName = "", ViewName = "AddEmployeeCheckList")] // Kiran on 11/5/2014
        //// Kiran on 12/18/2014
        //public ActionResult AddEmployeeCheckList(long SysUserOrgQuizId)
        //{
        //    LocalAddEmployeeCheckLists empCheckList = new LocalAddEmployeeCheckLists();
        //    empCheckList.checkLists = (from checkLists in entityDB.CheckListStatus.ToList()
        //                               select new CheckBoxesControlForEmpCheckLists
        //                               {
        //                                   id = checkLists.CheckListStatusID.ToString(),
        //                                   Text = checkLists.StatusDescription,
        //                                   Status = (entityDB.SystemUsersOrganizationsEmpQuizChecklist.FirstOrDefault(rec => rec.SysUserOrgQuizId == SysUserOrgQuizId && rec.CheckListStatusID == checkLists.CheckListStatusID) != null)
        //                               }).ToList();
        //    empCheckList.SysUserOrgQuizId = SysUserOrgQuizId;

        //    return View(empCheckList);
        //}

        //[HttpPost]
        //[SessionExpire]
        //[HeaderAndSidebar(headerName = "PREQUALIFICATIONREQUIREMENTS", sideMenuName = "", ViewName = "AddEmployeeCheckList")] // Kiran on 11/5/2014
        //public ActionResult AddEmployeeCheckList(LocalAddEmployeeCheckLists addEmployeeCheckLists)
        //{
        //    ViewBag.VBDisplayError = false;
        //    if (addEmployeeCheckLists.checkLists != null)
        //    {
        //        if (addEmployeeCheckLists.checkLists.Where(rec => rec.Status == true) == null || addEmployeeCheckLists.checkLists.Where(rec => rec.Status == true).Count() == 0)
        //        {
        //            ViewBag.VBDisplayError = true;
        //            return View(addEmployeeCheckLists);
        //        }
        //        else
        //        {
        //            var empExistingCheckLists = entityDB.SystemUsersOrganizationsEmpQuizChecklist.Where(rec => rec.SysUserOrgQuizId == addEmployeeCheckLists.SysUserOrgQuizId).ToList();
        //            foreach (var empCheckList in empExistingCheckLists)
        //            {
        //                entityDB.SystemUsersOrganizationsEmpQuizChecklist.Remove(empCheckList);
        //            }

        //            foreach (var checkList in addEmployeeCheckLists.checkLists.Where(rec => rec.Status == true))
        //            {
        //                long checkListId = Convert.ToInt64(checkList.id);
        //                SystemUsersOrganizationsEmpQuizChecklist addCheckListToEmp = new SystemUsersOrganizationsEmpQuizChecklist();
        //                addCheckListToEmp.CheckListStatusID = checkListId;
        //                addCheckListToEmp.SysUserOrgQuizId = addEmployeeCheckLists.SysUserOrgQuizId;
        //                entityDB.SystemUsersOrganizationsEmpQuizChecklist.Add(addCheckListToEmp);
        //            }
        //        }

        //        entityDB.SaveChanges();
        //        return Redirect("~/QuizTemplateBuilder/PopUpCloseView");
        //    }
        //    ViewBag.VBDisplayError = true;
        //    return View(addEmployeeCheckLists);
        //}
        // Ends<<<<



        //Rajesh on 2/17/2015 
        public ActionResult AddEmployeeCheckLists(long sysUserOrgId)
        {
            //var empQuizCategories = empQuizCategoriesRepository.GetEmployeeCategories();
            //// kiran on 03/13/2015
            //if (@Session["RoleName"].ToString().Equals("Vendor"))
            //{
            //    empQuizCategories = empQuizCategories.Where(rec => rec.AccessType == 2).ToList();
            //}
            //// kiran on 03/13/2015 Ends<<<
            //List<LocalAddCategories> trainingCategoriesList = new List<LocalAddCategories>();
            //foreach (var category in empQuizCategories)
            //{
            //    LocalAddCategories trainingCategory = new LocalAddCategories();
            //    trainingCategory.CategoryName = category.CategoryName;
            //    trainingCategory.SubCategoryControlType = category.SubCategoryControlType;
            //    trainingCategory.SubCategoriesValue = category.SubCategoriesValue;
            //    trainingCategory.SysUserOrgId = sysUserOrgId;
            //    trainingCategory.EmpQuizCategoryId = category.EmpQuizCategoryId;
            //    var category_Loc = entityDB.SystemUsersOrganizationsCategories.FirstOrDefault(rec => rec.SysUserOrganizationId == trainingCategory.SysUserOrgId && rec.EmpQuizCategoryId == trainingCategory.EmpQuizCategoryId);
            //    if (category_Loc != null)
            //    {
            //        trainingCategory.SubCategorySelectedValue = category_Loc.SubCategoryInputValue;
            //        trainingCategory.SubCategoryEnteredDate = category_Loc.SubCategoryEnteredDate;
            //    }
            //    trainingCategoriesList.Add(trainingCategory);
            //}
            List<LocalAddCategories> empCategories = _employee.GetEmployeeCategories(sysUserOrgId, SSession.Role);
            return View(empCategories);
        }

        [HttpPost]
        public ActionResult AddEmployeeCheckLists(List<LocalAddCategories> form)
        {
            //foreach (var Category in form.ToList())
            //{
            //    var category_Loc = entityDB.SystemUsersOrganizationsCategories.FirstOrDefault(rec => rec.SysUserOrganizationId == Category.SysUserOrgId && rec.EmpQuizCategoryId == Category.EmpQuizCategoryId);
            //    if (category_Loc == null)
            //    {
            //        if (!Category.SubCategorySelectedValue.Equals("N/A"))
            //        {
            //            category_Loc = new SystemUsersOrganizationsCategories();
            //            category_Loc.EmpQuizCategoryId = Category.EmpQuizCategoryId;
            //            category_Loc.SubCategoryEnteredDate = Category.SubCategoryEnteredDate;
            //            category_Loc.SubCategoryInputValue = Category.SubCategorySelectedValue;
            //            category_Loc.SysUserOrganizationId = Category.SysUserOrgId;
            //            category_Loc.CategoryAssignedBy = new Guid(Session["UserId"].ToString()); // Kiran on 03/13/2015
            //            entityDB.SystemUsersOrganizationsCategories.Add(category_Loc);
            //        }
            //    }
            //    else
            //    {
            //        if (!Category.SubCategorySelectedValue.Equals("N/A"))
            //        {
            //            category_Loc.SubCategoryInputValue = Category.SubCategorySelectedValue;
            //            category_Loc.SubCategoryEnteredDate = Category.SubCategoryEnteredDate;
            //            category_Loc.CategoryAssignedBy = new Guid(Session["UserId"].ToString()); // Kiran on 03/13/2015
            //            entityDB.Entry(category_Loc).State = EntityState.Modified;
            //        }
            //        else
            //        {
            //            entityDB.Entry(category_Loc).State = EntityState.Deleted;
            //        }
            //    }
            //    entityDB.SaveChanges();
            //}
            _employee.PostEmployeeCategories(form, SSession.UserId);
            return RedirectToAction("PopUpCloseView", "QuizTemplateBuilder");
        }
        //Ends<<< 

        /*
        Created Date:  02/16/2015   Created By: Kiran Talluri
        Purpose: To display the Employee Quiz CheckLists
        */
        [HttpGet]
        public ActionResult AddEmployeeQuizCheckLists(long sysUserOrgQuizId)
        {
            //List<EmployeeQuizCategories> trainingCategories = empQuizCategoriesRepository.GetTrainingCategories();
            //// kiran on 03/13/2015
            //if (@Session["RoleName"].ToString().Equals("Vendor"))
            //{
            //    trainingCategories = trainingCategories.Where(rec => rec.AccessType == 2).ToList();
            //}
            //// kiran on 03/13/2015 Ends<<<
            //List<LocalAddCategories> trainingCategoriesList = new List<LocalAddCategories>();
            //foreach (var category in trainingCategories)
            //{
            //    LocalAddCategories trainingCategory = new LocalAddCategories();
            //    trainingCategory.EmpQuizCategoryId = category.EmpQuizCategoryId;
            //    trainingCategory.CategoryName = category.CategoryName;
            //    trainingCategory.SubCategoryControlType = category.SubCategoryControlType;
            //    trainingCategory.SubCategoriesValue = category.SubCategoriesValue;
            //    trainingCategory.SysUserOrgQuizId = sysUserOrgQuizId;

            //    var sysUserOrgQuizCategory = entityDB.SystemUsersOrganizationsQuizzesCategories.FirstOrDefault(rec => rec.EmpQuizCategoryId == category.EmpQuizCategoryId && rec.SysUserOrgQuizId == sysUserOrgQuizId);
            //    if (sysUserOrgQuizCategory != null)
            //    {
            //        trainingCategory.SubCategorySelectedValue = sysUserOrgQuizCategory.SubCategoryInputValue;
            //        trainingCategory.SubCategoryEnteredDate = sysUserOrgQuizCategory.SubCategoryEnteredDate;
            //    }

            //    trainingCategoriesList.Add(trainingCategory);
            //}

            List<LocalAddCategories> trainingCategoriesList = _employee.GetTrainingCategories(sysUserOrgQuizId, SSession.Role);
            return View(trainingCategoriesList);
        }

        [HttpPost]
        public ActionResult AddEmployeeQuizCheckLists(List<LocalAddCategories> addQuizCategories)
        {
            //foreach (var category in addQuizCategories)
            //{
            //    var sysUserOrgQuizCategory = entityDB.SystemUsersOrganizationsQuizzesCategories.FirstOrDefault(rec => rec.EmpQuizCategoryId == category.EmpQuizCategoryId && rec.SysUserOrgQuizId == category.SysUserOrgQuizId);
            //    if (sysUserOrgQuizCategory == null)
            //    {
            //        if (!category.SubCategorySelectedValue.Equals("N/A"))
            //        {
            //            sysUserOrgQuizCategory = new SystemUsersOrganizationsQuizzesCategories();
            //            sysUserOrgQuizCategory.SubCategoryInputValue = category.SubCategorySelectedValue;
            //            sysUserOrgQuizCategory.SysUserOrgQuizId = category.SysUserOrgQuizId;
            //            sysUserOrgQuizCategory.SubCategoryEnteredDate = category.SubCategoryEnteredDate;
            //            sysUserOrgQuizCategory.EmpQuizCategoryId = category.EmpQuizCategoryId;
            //            sysUserOrgQuizCategory.CategoryAssignedBy = new Guid(Session["UserId"].ToString()); // Kiran on 03/13/2015
            //            entityDB.SystemUsersOrganizationsQuizzesCategories.Add(sysUserOrgQuizCategory);
            //        }
            //    }
            //    else
            //    {
            //        if (!category.SubCategorySelectedValue.Equals("N/A"))
            //        {
            //            sysUserOrgQuizCategory.SubCategoryInputValue = category.SubCategorySelectedValue;
            //            sysUserOrgQuizCategory.SubCategoryEnteredDate = category.SubCategoryEnteredDate;
            //            sysUserOrgQuizCategory.CategoryAssignedBy = new Guid(Session["UserId"].ToString()); // Kiran on 03/13/2015
            //            entityDB.Entry(sysUserOrgQuizCategory).State = EntityState.Modified;
            //        }
            //        else
            //        {
            //            entityDB.Entry(sysUserOrgQuizCategory).State = EntityState.Deleted;
            //        }
            //    }
            //    // Mani on 5/9/2015 
            //    if (!category.SubCategorySelectedValue.Equals("N/A") && category.EmpQuizCategoryId == 2)//for onsite training only
            //    {
            //        var sysUserOrgQuizRec = entityDB.SystemUsersOrganizationsQuizzes.Find(category.SysUserOrgQuizId);
            //        if (category.SubCategorySelectedValue.Equals("Pass"))//if category value is pass update rec with expiration date 
            //        {
            //            sysUserOrgQuizRec.QuizResult = true;
            //            DateTime submittedDate = (DateTime)category.SubCategoryEnteredDate;
            //            sysUserOrgQuizRec.QuizDateStart = submittedDate;
            //            sysUserOrgQuizRec.QuizDateSubmitted = submittedDate;
            //            sysUserOrgQuizRec.QuizExpirationDate = submittedDate.AddYears(1);

            //        }
            //        else
            //        {
            //            sysUserOrgQuizRec.QuizResult = false;
            //            DateTime submittedDate = (DateTime)category.SubCategoryEnteredDate;
            //            sysUserOrgQuizRec.QuizDateStart = submittedDate;
            //            sysUserOrgQuizRec.QuizDateSubmitted = submittedDate;
            //        }

            //        entityDB.Entry(sysUserOrgQuizRec).State = EntityState.Modified;
            //    }

            //    // Ends>>
            //    entityDB.SaveChanges();

            //}
            _employee.PostEmployeeQuizCategories(addQuizCategories, SSession.UserId);
            return Redirect("~/QuizTemplateBuilder/PopUpCloseView");
        }


        // kiran on 11/5/2014
        [SessionExpire]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "EmployeeQuiz", sideMenuName = "Quizzes", ViewName = "EmployeeQuizzes")]
        public ActionResult EmployeeQuizzes()
        {
            //Guid userId = new Guid(Session["UserId"].ToString());
            LocalVendorEmployeeQuizzes employee = new LocalVendorEmployeeQuizzes();
            employee.UserId = SSession.UserId;
            var userAcknowledgement = entityDB.UserAcknowledgement.FirstOrDefault(rec => rec.SubmittedBy == employee.UserId && rec.Acknowledgement == true);
            if (userAcknowledgement == null)
                return RedirectToAction("EmployeeAcknowledgement");
            else
                return View(employee);
        }

        // Kiran on 03/24/2015
        [HttpGet]
        [SessionExpireForView]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "MyAccount", sideMenuName = "EmployeesForTraining", ViewName = "AssignBulkTrainings")]
        public ActionResult AssignBulkTrainings(long VendorId, Guid? empid)
        {
            ViewBag.empid = empid;
            ViewBag.trainingids = null;
            var trainingids = new List<string>();
            if (empid != null)
            {
                var enddate = DateTime.Now.AddDays(30);
                var emprentra = entityDB.SystemUsersOrganizationsQuizzes.Where(rec => rec.SysUserOrgId == (entityDB.SystemUsersOrganizations.FirstOrDefault(r => r.UserId == empid).SysUserOrganizationId) && (DateTime)rec.QuizExpirationDate <= enddate).ToList();
                trainingids = emprentra.Select(rec => rec.ClientTemplateId.ToString()).ToList();
                ViewBag.trainingids = trainingids;
            }

            LocalAssignBulkTrainings assignBulkTrainings = new LocalAssignBulkTrainings();
            assignBulkTrainings.VendorId = VendorId;
            var employee_RoleId = EmployeeRole.EmployeeRoleId;
            // var employee_RoleId = entityDB.SystemRoles.FirstOrDefault(record => record.RoleName == "Employee Training").RoleId;
            var employee_sysUserOrgs = entityDB.SystemUsersOrganizationsRoles.Where(record =>employee_RoleId.Contains(record.SysRoleId)).Select(rec => rec.SystemUsersOrganizations).Where(rec => rec.OrganizationId == VendorId).ToList();
            if (empid != null) employee_sysUserOrgs= employee_sysUserOrgs.Where(r => r.UserId == empid).ToList();
             var employeeSysUserOrgs = new List<EmployeeSystemUsersOrganizations>();
            foreach (var employee in employee_sysUserOrgs)
            {
                EmployeeSystemUsersOrganizations addEmpSysUserOrg = new EmployeeSystemUsersOrganizations();
                
                    addEmpSysUserOrg.UserId = employee.UserId;
                addEmpSysUserOrg.EmployeeName = employee.SystemUsers.Contacts[0].LastName + " " + employee.SystemUsers.Contacts[0].FirstName;
                addEmpSysUserOrg.SysUserOrgId = employee.SysUserOrganizationId;
                addEmpSysUserOrg.empUserStatus = (bool)employee.SystemUsers.UserStatus; // Kiran on 06/11/2015
                employeeSysUserOrgs.Add(addEmpSysUserOrg);
            }
             assignBulkTrainings.employeeSysUserOrgs = employeeSysUserOrgs.OrderBy(rec => rec.EmployeeName).ToList(); 
            
            assignBulkTrainings.trainings = new List<CheckBoxesControlForEmployeeQuizzes>();
            //To get the latest prequalification of each client
            //var vendorPrequalifications = entityDB.Prequalification.SqlQuery(@"select p.* from (select e.*,ROW_NUMBER() over(partition by e.ClientId order by e.PrequalificationStart desc) 
            //    as seq from Prequalification e  where e.VendorId=" + VendorId + @") e 
            //    join Prequalification p on e.PrequalificationId=p.PrequalificationId where  p.VendorId=" + VendorId + @" and seq=1").Where(rec => rec.PrequalificationStatusId != 14 && rec.PrequalificationStatusId != 10).ToList();
            var vendorPrequalifications = entityDB.LatestPrequalification.Where(r => r.VendorId == VendorId && r.PrequalificationStatusId != 14 && r.PrequalificationStatusId != 10).ToList();
            foreach (var prequal in vendorPrequalifications)
            {
                //if (prequal.Client.TrainingRequired == true && prequal.PrequalificationFinish >= DateTime.Now)
                {
                    var clientIds = new List<long>() { prequal.ClientId };
                    if (prequal.Client.OrganizationType ==OrganizationType.SuperClient)
                    {
                        var pqClients = entityDB.PrequalificationClient.FirstOrDefault(r => r.PQId == prequal.PrequalificationId);
                        if (pqClients != null)
                            clientIds = pqClients.ClientIdsList.ToList();
                    }
                    var clientQuizTemplates = entityDB.ClientTemplates.Where(rec => clientIds.Contains(rec.ClientID)).ToList();
                    foreach (var clientQuizTemplate in clientQuizTemplates)
                    {
                        if (clientQuizTemplate.Templates.TemplateType == 1 && clientQuizTemplate.Templates.TemplateStatus == 1 && clientQuizTemplate.Templates.IsHide == false)
                        {
                            if (prequal.PrequalificationSites != null)
                            {
                                var prequalBUs = (from prequalificationSites in entityDB.PrequalificationSites.Where(rec => rec.PrequalificationId == prequal.PrequalificationId).ToList()
                                                  select prequalificationSites.ClientBusinessUnitId).ToList();

                                var quizClientTemplateBus = entityDB.ClientTemplatesForBU.Where(rec => prequalBUs.Contains(rec.ClientBusinessUnitId) && rec.ClientTemplateId == clientQuizTemplate.ClientTemplateID).ToList(); // Kiran on 11/18/2014

                                var prequalSites = (from prequalificationSites in entityDB.PrequalificationSites.Where(rec => rec.PrequalificationId == prequal.PrequalificationId).ToList()
                                                    select prequalificationSites.ClientBusinessUnitSiteId).ToList();

                                var prequalSiteQuizSites = entityDB.ClientTemplatesForBU.Where(rec => prequalBUs.Contains(rec.ClientBusinessUnitId) && rec.ClientTemplateId == clientQuizTemplate.ClientTemplateID).SelectMany(rec => rec.ClientTemplatesForBUSites).Any(r => prequalSites.Contains(r.ClientBusinessUnitSiteId));

                                //if (quizClientTemplateBus != null && quizClientTemplateBus.Count != 0 && assignBulkTrainings.trainings.FirstOrDefault(rec => rec.id == clientQuizTemplate.ClientTemplateID.ToString()) == null)
                                if (prequalSiteQuizSites && assignBulkTrainings.trainings.FirstOrDefault(rec => rec.id == clientQuizTemplate.ClientTemplateID.ToString()) == null)
                                {
                                    CheckBoxesControlForEmployeeQuizzes assignTraining = new CheckBoxesControlForEmployeeQuizzes();
                                    assignTraining.id = clientQuizTemplate.ClientTemplateID.ToString();
                                    assignTraining.Text = clientQuizTemplate.Templates.TemplateName;
                                    assignTraining.clientOrgName = clientQuizTemplate.Organizations.Name;
                                    assignBulkTrainings.trainings.Add(assignTraining);
                                }
                            }
                        }
                    }
                }
            }
            assignBulkTrainings.trainings = assignBulkTrainings.trainings.OrderBy(rec => rec.clientOrgName).ToList();
            if (empid != null) { assignBulkTrainings.trainings = assignBulkTrainings.trainings.Where(r => trainingids.Contains(r.id)).ToList(); }

            return View(assignBulkTrainings);
        }

        [HttpPost]
        [SessionExpireForView]
        //[HeaderAndSidebar(headerName = "PREQUALIFICATIONREQUIREMENTS", sideMenuName = "", ViewName = "AssignBulkTrainings")]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "MyAccount", sideMenuName = "EmployeesForTraining", ViewName = "AssignBulkTrainings")]
        public ActionResult AssignBulkTrainings(LocalAssignBulkTrainings assignBulkTrainings)
        {
            string trainingsList = "";
            List<SystemUsersOrganizationsQuizzes> empAssignedQuizzes = new List<SystemUsersOrganizationsQuizzes>();
            foreach (var employee in assignBulkTrainings.employeeSysUserOrgs.ToList())
            {
                //var body = emailTemplateFormatForEmployeeInvitation.EmailBody;
                //var employeeSystemUserDetails = entityDB.SystemUsersOrganizations.FirstOrDefault(rec => rec.SysUserOrganizationId == employee.SysUserOrgId).SystemUsers;

                if (employee.empStatus)
                {
                    foreach (var training in assignBulkTrainings.trainings.ToList())
                    {
                        Guid clientTemplateId = new Guid(training.id);
                        var empSysUserOrgQuiz = entityDB.SystemUsersOrganizationsQuizzes.FirstOrDefault(rec => rec.ClientTemplateId == clientTemplateId && rec.SysUserOrgId == employee.SysUserOrgId);

                        if (training.status == true) //Insertion Block
                        {
                            if (empSysUserOrgQuiz == null)
                            {
                                var assignTrainingToEmployee = new SystemUsersOrganizationsQuizzes();
                                assignTrainingToEmployee.ClientTemplateId = clientTemplateId;
                                assignTrainingToEmployee.VendorId = assignBulkTrainings.VendorId;
                                assignTrainingToEmployee.ClientId = entityDB.ClientTemplates.Find(clientTemplateId).ClientID;
                                assignTrainingToEmployee.SysUserOrgId = employee.SysUserOrgId;
                                entityDB.SystemUsersOrganizationsQuizzes.Add(assignTrainingToEmployee);
                                entityDB.SaveChanges();
                                empAssignedQuizzes.Add(assignTrainingToEmployee);
                            }
                            else
                            {
                                if (!empSysUserOrgQuiz.QuizPayments.Any(r => r.IsOpen != null && r.IsOpen.Value) && empSysUserOrgQuiz.QuizExpirationDate != null && empSysUserOrgQuiz.QuizExpirationDate <= DateTime.Now.AddDays(30))
                                {
                                    empSysUserOrgQuiz.PreviousLastQuizTakenStatus = empSysUserOrgQuiz.QuizResult;
                                    empSysUserOrgQuiz.PreviousLastQuizSubmittedDate =
                                        empSysUserOrgQuiz.QuizDateSubmitted;
                                    empSysUserOrgQuiz.QuizDateStart = null;
                                    empSysUserOrgQuiz.QuizDateSubmitted = null;
                                    empSysUserOrgQuiz.QuizResult = null;
                                    empSysUserOrgQuiz.QuizExpirationDate = null;

                                }
                                empSysUserOrgQuiz.QuizRemoved = false;
                                entityDB.Entry(empSysUserOrgQuiz).State = EntityState.Modified;
                                entityDB.SaveChanges();
                                empAssignedQuizzes.Add(empSysUserOrgQuiz);
                            }
                        }

                    }
                }
            }
            TempData["empAssignedQuizzes"] = empAssignedQuizzes.ToList();
            return RedirectToAction("TrainingFeePaymentForAssignedTrainings");
        }


        [HttpGet]
        [HeaderAndSidebar(headerName = "MyAccount", sideMenuName = "EmployeesForTraining", ViewName = "TrainingFeePaymentForAssignedTrainings")]
        public ActionResult TrainingFeePaymentForAssignedTrainings()
        {
            entityDB = new EFDbContext();
            var totalAmountDue = 0.0M;
            List<SystemUsersOrganizationsQuizzes> empAssignedQuizzes = new List<SystemUsersOrganizationsQuizzes>();
            if (TempData["empAssignedQuizzes"] != null)
            {
                empAssignedQuizzes = (List<SystemUsersOrganizationsQuizzes>)TempData["empAssignedQuizzes"];
                TempData.Keep("empAssignedQuizzes");
            }
            LocalGetTrainingDetails getTrainingDetails = new LocalGetTrainingDetails();
            List<long> QuizIds = new List<long>();
            getTrainingDetails.employeeAdditionAlFeeTrainings = new List<CheckBoxControlForEmployeeTrainings>();
            long currentOrgId = (long)Session["currentOrgId"];
            foreach (SystemUsersOrganizationsQuizzes employeeTraining in empAssignedQuizzes)
            {
                CheckBoxControlForEmployeeTrainings addEmployeeTraining = new CheckBoxControlForEmployeeTrainings();
                var clientId = employeeTraining.ClientTemplates.ClientID;
                addEmployeeTraining.ClientName = entityDB.Organizations.Find(clientId).Name;
                addEmployeeTraining.ClientTemplateId = employeeTraining.ClientTemplateId;
                var sysuserOrg = entityDB.SystemUsersOrganizations.Find(employeeTraining.SysUserOrgId);
                if (sysuserOrg != null)
                {
                    addEmployeeTraining.EmpId = sysuserOrg.UserId;
                    addEmployeeTraining.EmployeeName = sysuserOrg.SystemUsers.Contacts[0].LastName + ", " + sysuserOrg.SystemUsers.Contacts[0].FirstName;
                }
                addEmployeeTraining.TemplateName = employeeTraining.ClientTemplates.Templates.TemplateName;
                addEmployeeTraining.SysUserOrgQuizId = employeeTraining.SysUserOrgQuizId;
                QuizIds.Add(employeeTraining.SysUserOrgQuizId);
                addEmployeeTraining.ClientId = clientId; // Kiran on 11/25/2015
                if (employeeTraining.QuizExpirationDate != null && employeeTraining.QuizExpirationDate >= DateTime.Now.AddDays(30))
                    continue;
                getTrainingDetails.employeeAdditionAlFeeTrainings.Add(addEmployeeTraining);
            }

            //sandhya on 06/26/2015
            if (getTrainingDetails.employeeAdditionAlFeeTrainings.Count == 0)
            {
                return RedirectToAction("AddEmployees");
            }
            //Ends

            List<GetTrainingDetails> trainingDetails = new List<GetTrainingDetails>();
            //var vendorPrequalificationsGroupedByClient = entityDB.Prequalification.Where(rec => rec.VendorId == currentOrgId).GroupBy(rec => rec.ClientId).ToList();
            var vendorPrequalificationsGroupedByClient = entityDB.LatestPrequalification.Where(rec => rec.VendorId == currentOrgId && rec.PrequalificationStatusId != 10).ToList();
            long clientID = -1;
            foreach (var latestPrequalificationForCurrentClient in vendorPrequalificationsGroupedByClient)
            {

                //var latestPrequalificationForCurrentClient = prequalification.OrderByDescending(rec => rec.PrequalificationStart).FirstOrDefault();
                decimal subTotal = 0.0M;

                if (latestPrequalificationForCurrentClient.PrequalificationSites != null)
                {
                    clientID = latestPrequalificationForCurrentClient.ClientId;

                    List<long?> selectedPrequalBusinessUnitSites = (from prequalificationSites in entityDB.PrequalificationSites.Where(rec => rec.PrequalificationId == latestPrequalificationForCurrentClient.PrequalificationId).ToList()
                                                                    select prequalificationSites.ClientBusinessUnitSiteId).Distinct().ToList();

                    var clientIds = new List<long>();
                    if (latestPrequalificationForCurrentClient.Client.OrganizationType == OrganizationType.SuperClient)
                    {
                        try
                        {
                            clientIds = entityDB.PrequalificationClient.FirstOrDefault(r => r.PQId == latestPrequalificationForCurrentClient.PrequalificationId).ClientIdsList.ToList();
                        }
                        catch
                        {
                            continue;
                        }
                    }
                    else
                    {
                        clientIds.Add(latestPrequalificationForCurrentClient.ClientId);
                    }
                    foreach (var clientTrainingTemplate in getTrainingDetails.employeeAdditionAlFeeTrainings.Where(rec => clientIds.Contains((long)rec.ClientId)).ToList()) // Kiran on 11/25/2015 to reduce the no. of iterations
                    {
                        if (latestPrequalificationForCurrentClient.Client.OrganizationType == OrganizationType.SuperClient)
                        {
                            // If there is same client PQ with this vendor then skip payment calculation
                            if (vendorPrequalificationsGroupedByClient.Any(r => r.ClientId == clientTrainingTemplate.ClientId))
                                continue;
                        }
                        decimal trainingFeeToPay = 0;
                        List<long> applicableClientTemplateBuIds = new List<long>();

                        var AnnualClientTemplateForBU = entityDB.ClientTemplatesForBU.Where(rec => rec.ClientTemplateId == latestPrequalificationForCurrentClient.ClientTemplateId && rec.ClientTemplatesBUGroupPrice.GroupingFor == 1 && rec.ClientTemplatesBUGroupPrice.GroupPriceType != -1 && rec.ClientTemplatesBUGroupPrice.GroupPrice != 0).Select(rec => rec.ClientBusinessUnitId).ToList(); // Kiran on 11/25/2015 for FV-232

                        var applicableClientTemplatesForBU = entityDB.ClientTemplatesForBU.Where(rec => rec.ClientTemplateId == clientTrainingTemplate.ClientTemplateId && rec.ClientTemplatesBUGroupPrice.GroupingFor == 2 && rec.ClientTemplatesBUGroupPrice.GroupPriceType != -1).ToList();

                        foreach (var clientTemplatesForBU in applicableClientTemplatesForBU)
                        {
                            if (!AnnualClientTemplateForBU.Contains(clientTemplatesForBU.ClientBusinessUnitId))
                            {
                                var clientTemplatesForBUSites = clientTemplatesForBU.ClientTemplatesForBUSites.Where(rec => selectedPrequalBusinessUnitSites.Contains(rec.ClientBusinessUnitSiteId)).ToList();
                                var clientTemplatesForBuIds = clientTemplatesForBUSites.Select(rec => rec.ClientTemplateForBUId).Distinct().ToList();
                                applicableClientTemplateBuIds.AddRange(clientTemplatesForBuIds);
                            }
                        }

                        var applicableBusinessUnitsGroups = entityDB.ClientTemplatesForBU.Where(rec => rec.ClientTemplateId == clientTrainingTemplate.ClientTemplateId && applicableClientTemplateBuIds.Contains(rec.ClientTemplateForBUId)).Select(rec => rec.ClientTemplateBUGroupId).Distinct().ToList();

                        foreach (var clientTemplateBUGroup in applicableBusinessUnitsGroups)
                        {
                            try
                            {
                                //sandhya on 08/06/2015
                                //foreach (var Quizs in QuizIds)
                                //{
                                var systemUserQuizes = entityDB.SystemUsersOrganizationsQuizzes.FirstOrDefault(rec => rec.ClientTemplateId == clientTrainingTemplate.ClientTemplateId && rec.SysUserOrgQuizId == clientTrainingTemplate.SysUserOrgQuizId);
                                if (systemUserQuizes != null)
                                {
                                    var quizpayments = entityDB.QuizPayments.FirstOrDefault(rec => rec.SysUserOrgQuizId == systemUserQuizes.SysUserOrgQuizId && rec.IsOpen.Value);

                                    if (quizpayments == null && systemUserQuizes.QuizResult != null && systemUserQuizes.QuizResult.Value && !(((DateTime)systemUserQuizes.QuizExpirationDate).AddDays(-30).Date <= DateTime.Now.Date))
                                        quizpayments = entityDB.QuizPayments.FirstOrDefault(
                                            rec =>
                                                rec.SysUserOrgQuizId == systemUserQuizes.SysUserOrgQuizId &&
                                                rec.SubmittedDate == systemUserQuizes.QuizDateSubmitted);

                                    if (quizpayments == null)
                                    {
                                        trainingFeeToPay = trainingFeeToPay + (decimal)entityDB.ClientTemplatesBUGroupPrice.FirstOrDefault(rec => rec.ClientTemplateBUGroupId == clientTemplateBUGroup.Value).GroupPrice;
                                    }
                                    else if (quizpayments != null)
                                    {
                                        var QuizzesGroupIds = entityDB.QuizPaymentsItems.Where(rec => rec.QuizPaymentId == quizpayments.QuizPaymentId).Select(rec => rec.AsOfClientTemplateBUGroupId).Distinct().ToList();
                                        if (QuizzesGroupIds.Count > 0 && !QuizzesGroupIds.Contains(clientTemplateBUGroup.Value))
                                        {
                                            trainingFeeToPay = trainingFeeToPay + (decimal)entityDB.ClientTemplatesBUGroupPrice.FirstOrDefault(rec => rec.ClientTemplateBUGroupId == clientTemplateBUGroup.Value).GroupPrice;
                                        }
                                    }
                                }
                                //}
                                //Ends>>
                            }
                            catch { }
                        }


                        GetTrainingDetails addTrainingDetails = new GetTrainingDetails();
                        addTrainingDetails.ClientTemplateId = clientTrainingTemplate.ClientTemplateId;
                        addTrainingDetails.TrainingFeeToPay = trainingFeeToPay;
                        addTrainingDetails.PrequalificationID = latestPrequalificationForCurrentClient.PrequalificationId;
                        addTrainingDetails.SysuserOrgQuizId = clientTrainingTemplate.SysUserOrgQuizId;
                        addTrainingDetails.ClientId = clientTrainingTemplate.ClientId;
                        subTotal += trainingFeeToPay;

                        trainingDetails.Add(addTrainingDetails);

                    }
                }
                var currentDate = DateTime.Now;
                var currentActivePQ = latestPrequalificationForCurrentClient;//.OrderByDescending(rec => currentDate >= rec.PrequalificationStart && currentDate < rec.PrequalificationFinish).FirstOrDefault();
                totalAmountDue += this.CheckAgainstTrainingFeeCap(trainingDetails.Any()?trainingDetails.FirstOrDefault().ClientId?? latestPrequalificationForCurrentClient.ClientId : latestPrequalificationForCurrentClient.ClientId, currentActivePQ.PrequalificationId, subTotal);

                ViewBag.FeeCap = entityDB.Organizations.FirstOrDefault(rec => rec.OrganizationID == latestPrequalificationForCurrentClient.ClientId).TrainingFeeCap;
            }
            getTrainingDetails.employeeAdditionAlFeeTrainings.ForEach(rec => rec.TrainingFee = trainingDetails.FirstOrDefault(rec1 => rec1.SysuserOrgQuizId == rec.SysUserOrgQuizId).TrainingFeeToPay);
            getTrainingDetails.employeeAdditionAlFeeTrainings.ForEach(rec => rec.PrequalificationID = trainingDetails.FirstOrDefault(rec1 => rec1.ClientTemplateId == rec.ClientTemplateId).PrequalificationID);


            string quizIds = "";
            decimal actualTrainingFeeToPay = 0;
            foreach (var training in getTrainingDetails.employeeAdditionAlFeeTrainings)
            {
                quizIds = quizIds + training.SysUserOrgQuizId + ",";
                actualTrainingFeeToPay = actualTrainingFeeToPay + training.TrainingFee;
            }
            ViewBag.TotalDue = totalAmountDue;
            quizIds = quizIds.Substring(0, quizIds.Length - 1);

            bool vendorIsExemptFromTrainingFee = false;
            //TODO Check to see if vendor is exempt
            var props = entityDB.ClientVendorProperties.FirstOrDefault(r => r.ClientID == clientID && r.VendorID == currentOrgId);
            if (props != null)
            {
                if (props.TrainingPaymentExempt != null && props.TrainingPaymentExempt == true)
                {
                    vendorIsExemptFromTrainingFee = true;
                }
            }

            if (actualTrainingFeeToPay.Equals(0) || vendorIsExemptFromTrainingFee)

            {
                RecordZeroPayments(quizIds);
                return Redirect("SendMailsForEmpQuizzes?quizIds=" + quizIds + "&notify=" + true + "");
            }
            else
                return View(getTrainingDetails);
        }

        private void RecordZeroPayments(string quizIdsStr)
        {
            var quizIds = quizIdsStr.Split(',').Select(long.Parse);
            var quizes = entityDB.SystemUsersOrganizationsQuizzes.Where(r => quizIds.Contains(r.SysUserOrgQuizId)).ToList();
            foreach (var quiz in quizes)
            {
                var quizCheck = entityDB.QuizPayments.FirstOrDefault(rec => rec.SysUserOrgQuizId == quiz.SysUserOrgQuizId && rec.IsOpen != null && rec.IsOpen.Value);
                if (quizCheck != null)
                    continue;
                if (!quiz.QuizPayments.Any())
                    quiz.QuizPayments = new List<QuizPayments>();
                bool isZeroFeeTraining = _employee.IsZeroFeeTraining(quiz.ClientTemplateId);
                bool isVendorReachedTrainingFeeCap =
                    _employee.IsVendorReachedTrainingFeeCap(quiz.VendorId, quiz.ClientId);
                var userQuiz = new QuizPayments()
                {
                    ExpirationDate = quiz.QuizExpirationDate,
                    IsOpen = true,
                    PaymentReceivedAmount = 0,
                    PaymentReceivedDate = DateTime.Now,
                    QuizPaymentsItems = new List<QuizPaymentsItems>(),
                    SubmittedDate = quiz.QuizDateSubmitted,
                    SysUserOrgQuizId = quiz.SysUserOrgQuizId,
                    IsZeroFeeTraining = isZeroFeeTraining,
                    IsFeeCapReached = isVendorReachedTrainingFeeCap
                };
                quiz.QuizPayments.Add(userQuiz);
                //foreach (var userQuiz in quiz.QuizPayments)
                //{
                var clientTemplateForBU = entityDB.ClientTemplatesForBU.Where(rec => rec.ClientTemplateId == quiz.ClientTemplateId && rec.ClientTemplatesBUGroupPrice.GroupingFor == 2).Select(row => row).ToList();
                foreach (var businessUnit in clientTemplateForBU)
                {
                    var clientTemplatesForBUSites = entityDB.ClientTemplatesForBUSites.Where(rec => rec.ClientTemplateForBUId == businessUnit.ClientTemplateForBUId).Select(row => row).ToList();
                    foreach (var businessUniSite in clientTemplatesForBUSites)
                    {
                        QuizPaymentsItems addQuizPaymentItem = new QuizPaymentsItems();
                        addQuizPaymentItem.ClientTemplateForBUId = businessUnit.ClientTemplateForBUId;
                        addQuizPaymentItem.ClientTemplateForBUSitesId = businessUniSite.ClientTemplateForBUSitesId;
                        addQuizPaymentItem.AsOfClientTemplateBUGroupId = businessUnit.ClientTemplateBUGroupId;
                        addQuizPaymentItem.QuizPaymentId = userQuiz.QuizPaymentId;
                        entityDB.QuizPaymentsItems.Add(addQuizPaymentItem);

                    }
                }
                //}
                entityDB.SaveChanges();
            }

        }

        private decimal CheckAgainstTrainingFeeCap(long clientID, long prequalificationID, decimal PrequalificationAmountToPayTotal)
        {
            /*
             * Fee cap is applicable for the current year prequalification(Fee cap period = prequalification period). 
             * If vendor makes a payment in perticular prequalification period then that was more than $XX then we are applying fee cap
             */
            var TrainingFeeCap = entityDB.Organizations.FirstOrDefault(rec => rec.OrganizationID == clientID).TrainingFeeCap;
            decimal subtotal = 0.0M;
            decimal amountPaidSoFar = 0.0M;

            //Have to take the active PQ for the period of active one.
            //So, Please double check the active(The PQ which have a final status as of current date. 
            //In renewal we may have the old PQ is the active one.) one not a latest.
            var currentDate = DateTime.Now;
            var latestPQ = entityDB.Prequalification.Find(prequalificationID);
            var activePQ = entityDB.Prequalification.OrderByDescending(rec =>rec.ClientId == latestPQ.ClientId && rec.VendorId == latestPQ.VendorId && currentDate >= rec.PrequalificationStart && currentDate < rec.PrequalificationFinish).FirstOrDefault();

            if (TrainingFeeCap.HasValue && TrainingFeeCap.Value > 0)
            {
                var quizPayments = from quiz in entityDB.QuizPayments
                                   join suoq in entityDB.SystemUsersOrganizationsQuizzes on quiz.SysUserOrgQuizId equals suoq.SysUserOrgQuizId
                                   join p in entityDB.Prequalification on suoq.ClientTemplates.ClientID equals p.ClientTemplates.ClientID
                                   where p.PrequalificationId == activePQ.PrequalificationId && suoq.VendorId == p.VendorId
                                   && p.PrequalificationStart <= quiz.PaymentReceivedDate && p.PrequalificationFinish >= quiz.PaymentReceivedDate
                                   select quiz;
                amountPaidSoFar += quizPayments.Select(r => r.PaymentReceivedAmount).Sum().ToDecimalNullSafe();
                //amountPaidSoFar += quizPayments.Where(r => r.TransactionMetaData != null).Select(r => new { r.PaymentTransactionId, r.TransactionMetaData }).Distinct().ToList().Select(r => decimal.Parse(r.TransactionMetaData)).Sum().ToDecimalNullSafe();
            }

            if (TrainingFeeCap.HasValue && TrainingFeeCap.Value > 0)
            {
                if (PrequalificationAmountToPayTotal + amountPaidSoFar <= TrainingFeeCap.Value)
                {
                    subtotal += PrequalificationAmountToPayTotal;
                }
                else
                {
                    var tmpValue = (TrainingFeeCap.Value - amountPaidSoFar);
                    if (tmpValue < 0) tmpValue = 0;
                    subtotal += tmpValue;
                }
            }
            else
            {
                subtotal += PrequalificationAmountToPayTotal;
            }
            return subtotal;
        }
        [HttpPost]
        public ActionResult TrainingFeePaymentForAssignedTrainings(LocalGetTrainingDetails getTrainingDetails)
        {
            long currentOrgId = (long)Session["currentOrgId"];
            string custom = "";
            decimal adjustedTotalToPay = 0.0M;

            getTrainingDetails.trainingDetails = new List<GetTrainingDetails>();
            //Get Assigned Training clients
            
            var vendorPrequalificationsGroupedByClient = entityDB.LatestPrequalification.Where(rec => rec.VendorId == currentOrgId && rec.PrequalificationStatusId!=10).ToList();


            foreach (var prequalification in vendorPrequalificationsGroupedByClient)
            {
                var latestPrequalificationForCurrentClient = prequalification;//prequalification.OrderByDescending(rec => rec.PrequalificationStart).FirstOrDefault();
                if (latestPrequalificationForCurrentClient.PrequalificationSites != null)
                {
                    List<long> clientIds = new List<long>();
                    if (latestPrequalificationForCurrentClient.Client.OrganizationType == OrganizationType.SuperClient)
                    {
                        var pqClients = entityDB.PrequalificationClient.FirstOrDefault(r => r.PQId == latestPrequalificationForCurrentClient.PrequalificationId);
                        if (pqClients != null)
                            clientIds = pqClients.ClientIdsList.ToList();
                    }
                    else
                    {
                        clientIds.Add(latestPrequalificationForCurrentClient.ClientId);
                    }
                    //List<long?> selectedPrequalBusinessUnitSites = (from prequalificationSites in entityDB.PrequalificationSites.Where(rec => rec.PrequalificationId == latestPrequalificationForCurrentClient.PrequalificationId).ToList()
                    //                                                select prequalificationSites.ClientBusinessUnitSiteId).Distinct().ToList();
                    foreach (var clientTrainingTemplate in getTrainingDetails.employeeAdditionAlFeeTrainings.Where(r => r.ClientId == null || clientIds.Contains((long)r.ClientId)))
                    {
                        if (latestPrequalificationForCurrentClient.Client.OrganizationType == OrganizationType.SuperClient)
                        {
                            // If there is same client PQ then skip payment calculation
                            if (vendorPrequalificationsGroupedByClient.Any(r => r.ClientId == clientTrainingTemplate.ClientId))
                                continue;
                        }
                        if (clientTrainingTemplate.ClientId == null)
                        {
                            var clientId = entityDB.SystemUsersOrganizationsQuizzes.Find(clientTrainingTemplate.SysUserOrgQuizId).ClientId;
                            if (latestPrequalificationForCurrentClient.ClientId != clientId)
                                continue;

                        }

                        //if (latestPrequalificationForCurrentClient.ClientId == clientId)
                        {
                            // For forming the required parameters string
                            GetTrainingDetails addTrainingDetails = new GetTrainingDetails();
                            addTrainingDetails.ClientTemplateId = clientTrainingTemplate.ClientTemplateId;
                            addTrainingDetails.GetPrequalBusinessUnitsAndSites = new List<GetPrequalificationBusinessUnitsAndSites>();
                            var prequalSites = entityDB.PrequalificationSites.Where(rec => rec.PrequalificationId == latestPrequalificationForCurrentClient.PrequalificationId).Select(r => new GetPrequalificationBusinessUnitsAndSites() { BusinessUnitId= (long)r.ClientBusinessUnitId, SiteId=(long)r.ClientBusinessUnitSiteId }).ToList();
                            //foreach (var prequalSite in prequalSites)
                            //{
                            //    GetPrequalificationBusinessUnitsAndSites addPrequalSite = new GetPrequalificationBusinessUnitsAndSites();
                            //    addPrequalSite.BusinessUnitId = (long)prequalSite.ClientBusinessUnitId;
                            //    addPrequalSite.SiteId = (long)prequalSite.ClientBusinessUnitSiteId;
                            //    addTrainingDetails.GetPrequalBusinessUnitsAndSites.Add(addPrequalSite);
                            //}
                            addTrainingDetails.GetPrequalBusinessUnitsAndSites.AddRange(prequalSites);
                            getTrainingDetails.trainingDetails.Add(addTrainingDetails);


                        }

                        // Ends<<<
                    }
                    var currentDate = DateTime.Now;
                    //var currentActivePQ = prequalification;//prequalification.OrderByDescending(rec => currentDate >= rec.PrequalificationStart && currentDate < rec.PrequalificationFinish).FirstOrDefault();
                    var tmpClientId = getTrainingDetails.employeeAdditionAlFeeTrainings.Any() ? getTrainingDetails.employeeAdditionAlFeeTrainings.FirstOrDefault().ClientId ?? latestPrequalificationForCurrentClient.ClientId : latestPrequalificationForCurrentClient.ClientId;
                        adjustedTotalToPay += this.CheckAgainstTrainingFeeCap(tmpClientId,
                        latestPrequalificationForCurrentClient.PrequalificationId,
                        getTrainingDetails.employeeAdditionAlFeeTrainings.Where(r => r.PrequalificationID == latestPrequalificationForCurrentClient.PrequalificationId).Sum(r => r.TrainingFee));
                }

            }
            //getTrainingDetails.employeeAdditionAlFeeTrainings.ForEach(rec => rec.TrainingFee = getTrainingDetails.trainingDetails.FirstOrDefault(rec1 => rec1.ClientTemplateId == rec.ClientTemplateId).TrainingFeeToPay);
            getTrainingDetails.employeeAdditionAlFeeTrainings.ForEach(rec => rec.GetPrequalBusinessUnitsAndSites = getTrainingDetails.trainingDetails.FirstOrDefault(rec1 => rec1.ClientTemplateId == rec.ClientTemplateId).GetPrequalBusinessUnitsAndSites);

            foreach (var employeeTraining in getTrainingDetails.employeeAdditionAlFeeTrainings)
            {
                var alaCarteApplicableBUs = entityDB.ClientTemplatesForBU.Where(rec => rec.ClientTemplateId == employeeTraining.ClientTemplateId && rec.ClientTemplatesBUGroupPrice.GroupingFor == 2 && rec.ClientTemplatesBUGroupPrice.GroupPriceType != -1).Select(rec => rec.ClientBusinessUnitId).ToList();
                custom = custom + employeeTraining.SysUserOrgQuizId + "-" + employeeTraining.TrainingFee + ":"; //sandhya on 06/15/2015
                foreach (var businessUnit in employeeTraining.GetPrequalBusinessUnitsAndSites)
                {
                    if (alaCarteApplicableBUs.Contains(businessUnit.BusinessUnitId))
                    {
                        custom = custom + businessUnit.BusinessUnitId + "_" + businessUnit.SiteId + "|";
                    }
                }
                custom = custom + ";";
            }
            string quizIds = "";
            decimal actualTrainingFeeToPay = 0;
            foreach (var training in getTrainingDetails.employeeAdditionAlFeeTrainings)
            {
                quizIds = quizIds + training.SysUserOrgQuizId + ",";
                actualTrainingFeeToPay = actualTrainingFeeToPay + training.TrainingFee;
            }
            quizIds = quizIds.Substring(0, quizIds.Length - 1);
            quizIds = HttpUtility.HtmlDecode(HttpUtility.UrlDecode(quizIds));
            LocalInitialTrainingPayment quizPaymentdetails = new LocalInitialTrainingPayment();

            string clientname = "";
            string employeename = "";
            clientname = string.Join(",", getTrainingDetails.employeeAdditionAlFeeTrainings.Select(rec => rec.ClientName).Distinct());
            employeename = string.Join(Environment.NewLine, getTrainingDetails.employeeAdditionAlFeeTrainings.Where(rec => rec.TrainingFee != 0).Select(rec => rec.EmployeeName + " ( " + rec.SysUserOrgQuizId + ")").Distinct());
            var vendorName = entityDB.SystemUsersOrganizationsQuizzes.Find(getTrainingDetails.employeeAdditionAlFeeTrainings.FirstOrDefault().SysUserOrgQuizId).SystemUsersOrganizations.Organizations.Name;


            TempAssignBulkTrainingDetails tempAssignBulkTraining = new TempAssignBulkTrainingDetails();
            Guid paymentId = Guid.NewGuid();
            tempAssignBulkTraining.BulkTrainingPaymentInfo = custom;
            tempAssignBulkTraining.PaymentId = paymentId;
            tempAssignBulkTraining.QuizIds = quizIds;
            tempAssignBulkTraining.Employees = employeename;
            entityDB.TempAssignBulkTrainingDetails.Add(tempAssignBulkTraining);
            entityDB.SaveChanges();

            //quizPaymentdetails.EmpQuizIds = quizIds;
            quizPaymentdetails.PaymentId = paymentId;
            quizPaymentdetails.ClientName = clientname;
            //quizPaymentdetails.EmployeeName = employeename;
            quizPaymentdetails.VendorName = vendorName;

            //custom = HttpUtility.UrlDecode(custom);
            //quizPaymentdetails.custom = custom;
            //quizPaymentdetails.PaymentAmount = (decimal)actualTrainingFeeToPay;
            quizPaymentdetails.PaymentAmount = Math.Round((decimal)adjustedTotalToPay, 2);


            return RedirectToAction("PostToPaypalForInitialTraining", quizPaymentdetails);
        }

        // Kiran on 11/25/2014
        [HttpGet]
        [SessionExpireForView]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "PREQUALIFICATIONREQUIREMENTS", sideMenuName = "", ViewName = "UpdateEmployeeTrainings")]
        public ActionResult UpdateEmployeeTrainings(Guid userId)
        {
            var updateTrainings = new LocalEmployeeUpdateTrainings();
            var sysUserOrgId = entityDB.SystemUsersOrganizations.FirstOrDefault(rec => rec.UserId == userId).SysUserOrganizationId;
            updateTrainings.sysUserOrgId = sysUserOrgId;
            //var employeeSystemUserOrgQuizzes = new List<SystemUsersOrganizationsQuizzes>();

            var employeeSystemuserOrgQuizzesWithTakenTrainings = entityDB.SystemUsersOrganizationsQuizzes.Where(rec => rec.SysUserOrgId == sysUserOrgId).ToList();

            updateTrainings.employeeTrainings = (from sysUserOrgQuizzes in employeeSystemuserOrgQuizzesWithTakenTrainings.Where(rec => rec.SysUserOrgId == sysUserOrgId)
                                                 select new CheckBoxesControlForEmployeeQuizzes
                                                 {
                                                     id = sysUserOrgQuizzes.SysUserOrgQuizId.ToString(),
                                                     Text = sysUserOrgQuizzes.ClientTemplates.Templates.TemplateName,
                                                     employeeTakenTraining = (sysUserOrgQuizzes.QuizDateStart != null && sysUserOrgQuizzes.QuizResult != null) ? true : false, // kiran on 12/18/2014
                                                     assignedStatus = (sysUserOrgQuizzes.QuizRemoved == false || sysUserOrgQuizzes.QuizRemoved == null),// Kiran on 12/23/2014
                                                     clientTemplateID = sysUserOrgQuizzes.ClientTemplateId.ToString(),
                                                     clientOrgName = sysUserOrgQuizzes.ClientTemplates.Organizations.Name
                                                 }).ToList();

            //updateTrainings.trainingsToBeAssignedToEmployee = new List<CheckBoxControlForAssigningTrainingsToEmployee>();
            // Kiran on 11/28/2014
            long vendorOrgId = 0;
            if (!@Session["RoleName"].ToString().Equals("Admin"))
            {
                vendorOrgId = (long)Session["currentOrgId"];
            }
            else
            {
                vendorOrgId = entityDB.SystemUsersOrganizations.FirstOrDefault(rec => rec.UserId == userId).OrganizationId;
            }
            // Ends<<<
            var vendorPrequalifications = entityDB.Prequalification.Where(rec => rec.VendorId == vendorOrgId && rec.PrequalificationStatusId != 14).ToList(); // Kiran on 02/10/2015

            foreach (var prequal in vendorPrequalifications)
            {
                if (prequal.Client.TrainingRequired == true && prequal.PrequalificationFinish >= DateTime.Now)
                {
                    var clientQuizTemplates = entityDB.ClientTemplates.Where(rec => rec.ClientID == prequal.Client.OrganizationID).ToList();
                    foreach (var clientQuizTemplate in clientQuizTemplates)
                    {
                        if (clientQuizTemplate.Templates.TemplateType == 1 && clientQuizTemplate.Templates.TemplateStatus == 1 && clientQuizTemplate.Templates.IsHide == false)
                        {
                            if (prequal.PrequalificationSites != null)
                            {
                                var prequalSites = (from prequalificationSites in entityDB.PrequalificationSites.Where(rec => rec.PrequalificationId == prequal.PrequalificationId).ToList()
                                                    select prequalificationSites.ClientBusinessUnitId).ToList();

                                var quizClientTemplateBus = entityDB.ClientTemplatesForBU.Where(rec => prequalSites.Contains(rec.ClientBusinessUnitId) && rec.ClientTemplateId == clientQuizTemplate.ClientTemplateID).ToList(); // Kiran on 11/18/2014

                                if (quizClientTemplateBus != null && quizClientTemplateBus.Count != 0)
                                {
                                    if (entityDB.SystemUsersOrganizationsQuizzes.FirstOrDefault(rec => rec.ClientTemplateId == clientQuizTemplate.ClientTemplateID && rec.SysUserOrgId == sysUserOrgId) == null)
                                    {
                                        if (employeeSystemuserOrgQuizzesWithTakenTrainings != null && employeeSystemuserOrgQuizzesWithTakenTrainings.Count != 0)
                                        {
                                            foreach (var employeeAssignedTraining in employeeSystemuserOrgQuizzesWithTakenTrainings)
                                            {
                                                if (updateTrainings.employeeTrainings.FirstOrDefault(rec => rec.id == clientQuizTemplate.ClientTemplateID.ToString()) == null)
                                                {
                                                    CheckBoxesControlForEmployeeQuizzes assignTraining = new CheckBoxesControlForEmployeeQuizzes();
                                                    assignTraining.id = clientQuizTemplate.ClientTemplateID.ToString();
                                                    assignTraining.Text = clientQuizTemplate.Templates.TemplateName;
                                                    assignTraining.assignedStatus = false;
                                                    assignTraining.clientOrgName = clientQuizTemplate.Organizations.Name;
                                                    updateTrainings.employeeTrainings.Add(assignTraining);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            CheckBoxesControlForEmployeeQuizzes assignTraining = new CheckBoxesControlForEmployeeQuizzes();
                                            assignTraining.id = clientQuizTemplate.ClientTemplateID.ToString();
                                            assignTraining.Text = clientQuizTemplate.Templates.TemplateName;
                                            assignTraining.assignedStatus = false;
                                            assignTraining.clientOrgName = clientQuizTemplate.Organizations.Name;
                                            updateTrainings.employeeTrainings.Add(assignTraining);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            updateTrainings.employeeTrainings = updateTrainings.employeeTrainings.OrderBy(rec => rec.clientOrgName).ToList();

            return View(updateTrainings);
        }

        [HttpPost]
        [SessionExpireForView]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "PREQUALIFICATIONREQUIREMENTS", sideMenuName = "", ViewName = "UpdateEmployeeTrainings")]
        public ActionResult UpdateEmployeeTrainings(LocalEmployeeUpdateTrainings updateEmployeeTrainings)
        {
            ViewBag.VBDisplayError = false;
            bool mailToEmployee = false; // Kiran on 11/25/2015 to stop sending mails to employees if they have already taken training
            if (updateEmployeeTrainings.employeeTrainings != null)
            {
                var emailTemplateFormatForEmployeeInvitation = entityDB.EmailTemplates.FirstOrDefault(rec => rec.EmailUsedFor == 6); // Kiran on 01/13/2015
                var subject = emailTemplateFormatForEmployeeInvitation.EmailSubject;
                var body = emailTemplateFormatForEmployeeInvitation.EmailBody;
                var employeeContactDetails = entityDB.SystemUsersOrganizations.FirstOrDefault(rec => rec.SysUserOrganizationId == updateEmployeeTrainings.sysUserOrgId).SystemUsers.Contacts[0];
                var employeeSystemUserDetails = entityDB.SystemUsersOrganizations.FirstOrDefault(rec => rec.SysUserOrganizationId == updateEmployeeTrainings.sysUserOrgId).SystemUsers;
                string trainingsList = ""; // Kiran on 01/05/2015
                var sysUserOrg = entityDB.SystemUsersOrganizations.Find(updateEmployeeTrainings.sysUserOrgId);
                foreach (var training in updateEmployeeTrainings.employeeTrainings.ToList())
                {
                    // Kiran on 12/23/2014
                    if (training.status == true) //Insertion Block
                    {
                        try//If record Exist Update it to visible
                        {
                            long empSysUserOrgQuizId = Convert.ToInt64(training.id);
                            var empSysUserOrganizationQuiz = entityDB.SystemUsersOrganizationsQuizzes.Find(empSysUserOrgQuizId);

                            // Kiran on 11/25/2015 to stop sending mails to employees if they have already taken training
                            if (empSysUserOrganizationQuiz.QuizResult == null || empSysUserOrganizationQuiz.QuizResult == false || (empSysUserOrganizationQuiz.QuizResult == true && empSysUserOrganizationQuiz.QuizExpirationDate != null && empSysUserOrganizationQuiz.QuizExpirationDate <= DateTime.Now))
                                mailToEmployee = true;
                            // kiran on 11/25/2015 ends<<<

                            if (empSysUserOrganizationQuiz.QuizRemoved == true)
                            {
                                empSysUserOrganizationQuiz.QuizRemoved = false;
                                entityDB.Entry(empSysUserOrganizationQuiz).State = EntityState.Modified;
                                entityDB.SaveChanges();
                            }
                        }
                        catch//Else Insert New Record
                        {
                            Guid clientTemplateId = new Guid(training.id);

                            if (entityDB.SystemUsersOrganizationsQuizzes.FirstOrDefault(rec => rec.ClientTemplateId == clientTemplateId && rec.SysUserOrgId == updateEmployeeTrainings.sysUserOrgId) == null)
                            {
                                SystemUsersOrganizationsQuizzes assignTrainingToEmployee = new SystemUsersOrganizationsQuizzes();
                                assignTrainingToEmployee.ClientTemplateId = clientTemplateId;
                                assignTrainingToEmployee.VendorId = sysUserOrg.OrganizationId; // Kiran on 03/11/2015
                                assignTrainingToEmployee.ClientId = entityDB.ClientTemplates.Find(clientTemplateId).ClientID;
                                assignTrainingToEmployee.SysUserOrgId = updateEmployeeTrainings.sysUserOrgId;
                                entityDB.SystemUsersOrganizationsQuizzes.Add(assignTrainingToEmployee);
                                entityDB.SaveChanges();
                                mailToEmployee = true;// Kiran on 11/25/2015 to stop sending mails to employees if they have already taken training
                            }
                        }
                    }
                    else //Deletion Block
                    {
                        try
                        {
                            long empSysUserOrgQuizId = Convert.ToInt64(training.id);
                            var empSysUserOrganizationQuiz = entityDB.SystemUsersOrganizationsQuizzes.Find(empSysUserOrgQuizId);
                            if (empSysUserOrganizationQuiz != null)//Record exist
                            {
                                if (empSysUserOrganizationQuiz.QuizDateStart == null && empSysUserOrganizationQuiz.PreviousLastQuizSubmittedDate == null && !empSysUserOrganizationQuiz.QuizPayments.Any())//Quiz not started
                                {
                                    //var emptrainingCheckLists = entityDB.SystemUsersOrganizationsEmpQuizChecklist.Where(rec => rec.SysUserOrgQuizId == empSysUserOrgQuizId).ToList();
                                    //if (emptrainingCheckLists != null)
                                    //{
                                    //    foreach (var empTraining in emptrainingCheckLists)
                                    //    {
                                    //        entityDB.SystemUsersOrganizationsEmpQuizChecklist.Remove(empTraining);
                                    //    }
                                    //}
                                    entityDB.Entry(empSysUserOrganizationQuiz).State = EntityState.Deleted;
                                    entityDB.SaveChanges();
                                }
                                else
                                {
                                    empSysUserOrganizationQuiz.QuizRemoved = true;
                                    entityDB.Entry(empSysUserOrganizationQuiz).State = EntityState.Modified;
                                    entityDB.SaveChanges();
                                }
                            }
                        }
                        catch { }
                    }
                    // Ends<<<

                    if (training.assignedStatus == false && training.status == true)
                    {
                        trainingsList = trainingsList + "<li>" + training.clientOrgName + " - " + training.Text + "</li>"; // Kiran on 01/05/2015
                        // Ends<<<
                    }
                }
                if (updateEmployeeTrainings.employeeTrainings.Where(rec => rec.assignedStatus == false && rec.status == true) != null && updateEmployeeTrainings.employeeTrainings.Where(rec => rec.assignedStatus == false && rec.status == true).Count() != 0 && mailToEmployee == true) // kiran on 11/25/2015 to stop mail sending to employees once they have taken trainings)
                {
                    try
                    {
                        //Guid clientTemplateId = new Guid(training.id);
                        //var clientName = entityDB.ClientTemplates.FirstOrDefault(rec => rec.ClientTemplateID == clientTemplateId).Organizations.Name;

                        Guid vendorLoggedInUserId = (Guid)Session["UserId"];
                        var vendorLoggedInUser = entityDB.SystemUsers.Find(vendorLoggedInUserId);

                        var vendorUserName = "";
                        try
                        {
                            vendorUserName = vendorLoggedInUser.Contacts[0].LastName + ", " + vendorLoggedInUser.Contacts[0].FirstName;
                        }
                        catch
                        {
                            vendorUserName = vendorLoggedInUser.Email;
                        }

                        //var vendorUserName = vendorLoggedInUser.Contacts[0]!=null?vendorLoggedInUser.Contacts[0].FirstName + " " + vendorLoggedInUser.Contacts[0].LastName: vendorLoggedInUser.Email;
                        var vendorOrgName = entityDB.SystemUsersOrganizations.FirstOrDefault(rec => rec.UserId == vendorLoggedInUserId).Organizations.Name;

                        //subject = subject.Replace("[ClientName]", clientName);
                        body = body.Replace("[Date]", DateTime.Now + "");
                        body = body.Replace("[EmployeeName]", employeeContactDetails.LastName + ", " + employeeContactDetails.FirstName);
                        //body = body.Replace("[CLIENTNAME]", );
                        body = body.Replace("[VENDORNAME]", vendorOrgName);
                        body = body.Replace("[VendorUserName]", vendorUserName);
                        body = body.Replace("[Email]", employeeSystemUserDetails.Email);
                        body = body.Replace("[Password]", Utilities.EncryptionDecryption.Decryptdata(employeeSystemUserDetails.Password));
                        body = body.Replace("[ClientName - TrainingName]", trainingsList);
                        body = body + "</br>" + emailTemplateFormatForEmployeeInvitation.CommentText;

                    }
                    catch { }

                    List<Attachment> mailAttachments = new List<Attachment>();
                    if (emailTemplateFormatForEmployeeInvitation.EmailTemplateAttachments != null && emailTemplateFormatForEmployeeInvitation.EmailTemplateAttachments.Count() > 0)
                    {
                        try
                        {
                            foreach (var file in emailTemplateFormatForEmployeeInvitation.EmailTemplateAttachments)
                            {
                                var filePath = Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["EmailTemplateAttachmentsPath"]), emailTemplateFormatForEmployeeInvitation.EmailTemplateID + file.AttachmentId.ToString() + file.FileName);

                                var f = new FileStream(filePath, FileMode.Open, FileAccess.Read);
                                if (filePath != null)
                                {
                                    mailAttachments.Add(new Attachment(f, file.FileName));
                                }
                            }
                        }
                        catch { }
                    }
                    if (!String.IsNullOrEmpty(employeeSystemUserDetails.Email))
                    {
                        var message = Utilities.SendMail.sendMail(employeeSystemUserDetails.Email, body, subject, "", "", mailAttachments);
                    }
                }
            }

            entityDB.SaveChanges();
            return RedirectToAction("PopUpCloseView", "QuizTemplateBuilder");
        }

        // Kiran on 11/29/2014 for downloading the Employer guide document
        [SessionExpire]
        public ActionResult DownloadDoc(string fileName, string ConfigPath, long? clientId)//Rajesh on 1/6/2015 // Kiran on 01/20/2015
        {
            try
            {
                // kiran on 12/16/2014
                string invalidChars = Regex.Escape(new string(System.IO.Path.GetInvalidFileNameChars()));
                string invalidReStr = string.Format(@"[{0}]+", invalidChars);
                fileName = Regex.Replace(fileName, invalidReStr, "_").Replace(";", "").Replace(",", "");
                // Ends<<<

                var filePath = Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["EmployerGuideToAddEmployees"]), fileName);
                //Rajesh on 1/6/2015
                if (!string.IsNullOrEmpty(ConfigPath))
                {
                    filePath = Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings[ConfigPath]), clientId + "/" + fileName); // Kiran on 01/20/2015
                }
                //Ends<<<
                if (System.IO.File.Exists(filePath))
                {
                    var fs = System.IO.File.OpenRead(filePath);
                    //return File(fs, "file", userEnteredFilename);
                    Response.AppendHeader("Content-Disposition", "inline; filename=" + fileName);
                    return File(fs, "application/pdf");
                }
                return View("DocDownloadFailed");
            }
            catch
            {
                throw new HttpException(404, @Resources.Resources.EmailTemplatesCouldNotFound + fileName);
            }
        }
        // Ends<<<

        
        public ActionResult PostToPaypalForInitialTraining(LocalInitialTrainingPayment trainingPayment)
        {
            LocalPaypal paypalForInitialTraining = new LocalPaypal();
            paypalForInitialTraining.cmd = "_xclick";
            paypalForInitialTraining.business = ConfigurationManager.AppSettings["BusinessAccountKey"];
            bool useSandbox = Convert.ToBoolean(ConfigurationManager.AppSettings["UseSandbox"]);
            ViewBag.actionURL = useSandbox ? "https://www.sandbox.paypal.com/cgi-bin/webscr" : "https://www.paypal.com/cgi-bin/webscr";

            paypalForInitialTraining.cancel_return = ConfigurationManager.AppSettings["CancelURLForTraining"];

            paypalForInitialTraining.notify_url = ConfigurationManager.AppSettings["NotifyURLForInitialTraining"];
            paypalForInitialTraining.currency_code = ConfigurationManager.AppSettings["CurrencyCode"];
            var request = entityDB.TempAssignBulkTrainingDetails.FirstOrDefault(r => r.PaymentId == trainingPayment.PaymentId);
            var empNames = "";
            if (request != null)
                empNames = request.Employees;
            paypalForInitialTraining.item_name = HttpUtility.HtmlDecode(HttpUtility.UrlDecode("Training Payments ----- Client :" + trainingPayment.ClientName + "  Vendor:" + trainingPayment.VendorName + "  Employees:" + empNames));
            //paypalForInitialTraining.item_name = HttpUtility.HtmlDecode(HttpUtility.UrlDecode("Training Payments ----- Client :" + trainingPayment.ClientName + "  Vendor:" + trainingPayment.VendorName));
            paypalForInitialTraining.custom = trainingPayment.PaymentId.ToString();

            //paypalForInitialTraining.amount = string.Format("{0:c}", trainingPayment.PaymentAmount);
            paypalForInitialTraining.amount = trainingPayment.PaymentAmount.ToString();

            string returnURL = ConfigurationManager.AppSettings["ReturnURLForTraining"];
            //string returnParams = "quizIds=" + trainingPayment.EmpQuizIds + "&notify=" + false + "&paymentId=" + trainingPayment.PaymentId;
            string returnParams = "notify=" + false + "&paymentId=" + trainingPayment.PaymentId;

            if (trainingPayment.PaymentAmount == 0)
            {
                returnParams = "quizIds=" + trainingPayment.EmpQuizIds + "&notify=" + false + "&paymentId=" + trainingPayment.PaymentId;
                paypalForInitialTraining.@return = returnURL + QueryStringModule.Encrypt(returnParams);
                return Redirect(paypalForInitialTraining.@return);
            }
            string url = getPaymentURL(paypalForInitialTraining, returnURL, returnParams);
            return Redirect(url);
        }

        //return View(paypalForInitialTraining);
        //}


        public ActionResult SendMailsForEmpQuizzes(string quizIds, bool? notify, Guid? paymentId, string guid)
        {
            if (paymentId != null && guid != null)
                PayPal.Sample.Common.ExecutePayment(Request.Params["PayerID"], Session[guid] as string);

            var emailTemplateFormatForEmployeeInvitation = entityDB.EmailTemplates.FirstOrDefault(rec => rec.EmailUsedFor == 6); // Kiran on 01/13/2015
            var subject = emailTemplateFormatForEmployeeInvitation.EmailSubject;
            string trainingsList = "";
            List<SystemUsersOrganizationsQuizzes> empQuizzes = new List<SystemUsersOrganizationsQuizzes>();

            if (paymentId != null)
            {
                var paymentData = entityDB.TempAssignBulkTrainingDetails.FirstOrDefault(r => r.PaymentId == paymentId);

                quizIds = paymentData.QuizIds;
            }
            foreach (var quiz in quizIds.Split(','))
            {
                if (quiz != "")
                {
                    long sysUserOrgQuizId = Convert.ToInt64(quiz);
                    var sysUserOrgQuiz = entityDB.SystemUsersOrganizationsQuizzes.Find(sysUserOrgQuizId);
                    empQuizzes.Add(sysUserOrgQuiz);
                }
            }
            foreach (var employee in empQuizzes.GroupBy(rec => rec.SysUserOrgId))
            {
                bool mailToEmployee = false; // Kiran on 11/25/2015 to stop mail sending to employees once they have taken trainings
                var body = emailTemplateFormatForEmployeeInvitation.EmailBody;
                var empSysUserOrg = entityDB.SystemUsersOrganizations.Find(employee.Key);
                foreach (var empTraining in employee)
                {
                    trainingsList = trainingsList + "<li>" + empTraining.Client.Name + " - " + empTraining.ClientTemplates.Templates.TemplateName + "</li>";
                    // Kiran on 11/25/2015 to stop mail sending to employees once they have taken trainings
                    if (empTraining.QuizResult == null || empTraining.QuizResult == false || (empTraining.QuizResult == true && empTraining.QuizExpirationDate != null && empTraining.QuizExpirationDate <= DateTime.Now))
                    {
                        //If we have no email. We cannot send one.
                        if (!String.IsNullOrEmpty(empSysUserOrg.SystemUsers.Email))
                        {
                            mailToEmployee = true;
                        }
                    }
                    // Kiran on 11/25/2015 ends<<
                }
                if (mailToEmployee == true) // Kiran on 11/25/2015
                {
                    try
                    {
                        Guid vendorLoggedInUserId = (Guid)Session["UserId"];
                        long loggedInOrgId = Convert.ToInt64(Session["currentOrgId"]);
                        var vendorLoggedInUser = entityDB.SystemUsers.Find(vendorLoggedInUserId);

                        var vendorUserName = "";
                        try
                        {
                            vendorUserName = vendorLoggedInUser.Contacts[0].LastName + ", " + vendorLoggedInUser.Contacts[0].FirstName;
                        }
                        catch
                        {
                            vendorUserName = vendorLoggedInUser.Email;
                        }

                        var vendorOrgName = entityDB.Organizations.Find(loggedInOrgId).Name;

                        body = body.Replace("[Date]", DateTime.Now + "");
                        body = body.Replace("[EmployeeName]", empSysUserOrg.SystemUsers.Contacts[0].LastName + ", " + empSysUserOrg.SystemUsers.Contacts[0].FirstName);
                        body = body.Replace("[VENDORNAME]", vendorOrgName);
                        body = body.Replace("[VendorUserName]", vendorUserName);
                        body = body.Replace("[Email]", empSysUserOrg.SystemUsers.Email);
                        body = body.Replace("[Password]", Utilities.EncryptionDecryption.Decryptdata(empSysUserOrg.SystemUsers.Password));
                        body = body.Replace("[ClientName - TrainingName]", trainingsList);
                        body = body + "</br>" + emailTemplateFormatForEmployeeInvitation.CommentText;

                    }
                    catch { }

                    List<Attachment> mailAttachments = new List<Attachment>();
                    if (emailTemplateFormatForEmployeeInvitation.EmailTemplateAttachments != null && emailTemplateFormatForEmployeeInvitation.EmailTemplateAttachments.Count() > 0)
                    {
                        try
                        {
                            foreach (var file in emailTemplateFormatForEmployeeInvitation.EmailTemplateAttachments)
                            {
                                var filePath = Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["EmailTemplateAttachmentsPath"]), emailTemplateFormatForEmployeeInvitation.EmailTemplateID + file.AttachmentId.ToString() + file.FileName);

                                var f = new FileStream(filePath, FileMode.Open, FileAccess.Read);
                                if (filePath != null)
                                {
                                    mailAttachments.Add(new Attachment(f, file.FileName));
                                }
                            }
                        }
                        catch { }
                    }
                    var message = Utilities.SendMail.sendMail(empSysUserOrg.SystemUsers.Email, body, subject, "", "", mailAttachments);
                }
            }
            if (notify != null && notify == true)
            {
                //return Content("<script language='javascript' type='text/javascript'>alert('Save Successfully');</script>");
                return Redirect("AddEmployees?notified=" + true + "");
            }

            if (notify != null && notify == false)
            {
                var tempAssignBultkTrainingDetails = entityDB.TempAssignBulkTrainingDetails.FirstOrDefault(rec => rec.PaymentId == paymentId);

                if (tempAssignBultkTrainingDetails != null)
                {
                    foreach (var trainingDetails in tempAssignBultkTrainingDetails.BulkTrainingPaymentInfo.Split(';'))
                    {
                        if (trainingDetails != "")
                        {
                            var training = trainingDetails.Split(':');
                            //long quizId = Convert.ToInt64(training[0]);
                            var quizIdwithpayments = training[0].Split('-');
                            long quizId = Convert.ToInt64(quizIdwithpayments[0]);
                            decimal quizAmount = Convert.ToDecimal(quizIdwithpayments[1]);

                            var sysUserOrgQuiz = entityDB.SystemUsersOrganizationsQuizzes.Find(quizId);
                            foreach (var prequalSite in training[1].Split('|'))
                            {
                                if (prequalSite != "")
                                {
                                    var businessUnit = prequalSite.Split('_');

                                    long? businessUnitId = Convert.ToInt64(businessUnit[0]);
                                    long? businessUnitSiteId = Convert.ToInt64(businessUnit[1]);
                                    var clientTemplateForBU = entityDB.ClientTemplatesForBU.FirstOrDefault(rec => rec.ClientBusinessUnitId == businessUnitId && rec.ClientTemplateId == sysUserOrgQuiz.ClientTemplateId && rec.ClientTemplatesBUGroupPrice.GroupingFor == 2);
                                    var clientTemplatesForBUSite = entityDB.ClientTemplatesForBUSites.FirstOrDefault(rec => rec.ClientTemplateForBUId == clientTemplateForBU.ClientTemplateForBUId && rec.ClientBusinessUnitSiteId == businessUnitSiteId);
                                    QuizPayments addQuizPayment = new QuizPayments();
                                    //List<long> clientTemplateBUGroupIds = new List<long>();
                                    //clientTemplateBUGroupIds.Add((long)clientTemplateForBU.ClientTemplateBUGroupId);
                                    //clientTemplateBUGroupIds = clientTemplateBUGroupIds.Distinct().ToList();
                                    var quizCheck = entityDB.QuizPayments.FirstOrDefault(rec => rec.SysUserOrgQuizId == quizId && rec.IsOpen != null && rec.IsOpen.Value);
                                    if (quizCheck == null)//Get current quiz submited payment
                                    {
                                        if (sysUserOrgQuiz.QuizResult != null && sysUserOrgQuiz.QuizResult.Value && !(((DateTime)sysUserOrgQuiz.QuizExpirationDate).AddDays(-30).Date <= DateTime.Now.Date))
                                            quizCheck = entityDB.QuizPayments.FirstOrDefault(rec => rec.SysUserOrgQuizId == quizId && rec.SubmittedDate == sysUserOrgQuiz.QuizDateSubmitted);
                                    }
                                    //if (!clientTemplateBUGroupIds.Contains((long)clientTemplateForBU.ClientTemplateBUGroupId))
                                    if (quizCheck == null)
                                    {
                                        //var groupPrice = entityDB.ClientTemplatesBUGroupPrice.Find(clientTemplateForBU.ClientTemplateBUGroupId).GroupPrice;
                                        addQuizPayment.PaymentReceivedAmount = quizAmount;
                                        addQuizPayment.PaymentReceivedDate = DateTime.Now;
                                        addQuizPayment.SysUserOrgQuizId = quizId;
                                        addQuizPayment.IsOpen = true;
                                        addQuizPayment.IsZeroFeeTraining =
                                            _employee.IsZeroFeeTraining(sysUserOrgQuiz.ClientTemplateId);
                                        addQuizPayment.IsFeeCapReached =
                                            _employee.IsVendorReachedTrainingFeeCap(sysUserOrgQuiz.VendorId,
                                                sysUserOrgQuiz.ClientId);
                                        entityDB.QuizPayments.Add(addQuizPayment);
                                        entityDB.SaveChanges();
                                    }
                                    if (clientTemplatesForBUSite != null)
                                    {
                                        QuizPaymentsItems addQuizPaymentItem = new QuizPaymentsItems();
                                        long quizPaymentId;
                                        if (quizCheck == null)
                                            quizPaymentId = addQuizPayment.QuizPaymentId;
                                        else
                                            quizPaymentId = quizCheck.QuizPaymentId;
                                        addQuizPaymentItem.QuizPaymentId = quizPaymentId;
                                        var quizItems = entityDB.QuizPaymentsItems.FirstOrDefault(rec => rec.QuizPaymentId == quizPaymentId && rec.ClientTemplateForBUSitesId == clientTemplatesForBUSite.ClientTemplateForBUSitesId);
                                        if (quizItems == null)
                                        {
                                            addQuizPaymentItem.ClientTemplateForBUId = clientTemplateForBU.ClientTemplateForBUId;
                                            addQuizPaymentItem.ClientTemplateForBUSitesId = clientTemplatesForBUSite.ClientTemplateForBUSitesId;
                                            addQuizPaymentItem.AsOfClientTemplateBUGroupId = clientTemplateForBU.ClientTemplateBUGroupId;
                                            entityDB.QuizPaymentsItems.Add(addQuizPaymentItem);
                                            entityDB.SaveChanges();

                                            clientTemplatesForBUSite.IsPaymentDone = true;
                                            entityDB.Entry(clientTemplatesForBUSite).State = EntityState.Modified;
                                            entityDB.SaveChanges();
                                        }
                                    }
                                }
                            }
                        }
                    }

                }
                // Ends<<<
            }

            return RedirectToAction("AddEmployees");
        }

        [HttpPost]
        public ActionResult NotifyURLForInitialTrainingPayment()
        {
            try
            {
                byte[] param = Request.BinaryRead(Request.ContentLength);
                string strRequest = Encoding.ASCII.GetString(param);

                // append PayPal verification code to end of string
                strRequest += "&cmd=_notify-validate";

                // create an HttpRequest channel to perform handshake with PayPal
                // Kiran on 7/30/2014
                bool useSandbox = Convert.ToBoolean(ConfigurationManager.AppSettings["UseSandbox"]);
                HttpWebRequest req;
                if (useSandbox)
                {
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    req = (HttpWebRequest)WebRequest.Create(@"https://ipnpb.sandbox.paypal.com/cgi-bin/webscr");
                }
                else
                    req = (HttpWebRequest)WebRequest.Create(@"https://ipnpb.paypal.com/cgi-bin/webscr");
                // Ends<<<
                req.Method = "POST";
                req.ContentType = "application/x-www-form-urlencoded";
                req.ContentLength = strRequest.Length;


                StreamWriter streamOut = new StreamWriter(req.GetRequestStream(), Encoding.ASCII);
                streamOut.Write(strRequest);
                streamOut.Close();

                // receive response from PayPal
                StreamReader streamIn = new StreamReader(req.GetResponse().GetResponseStream());
                string strResponse = streamIn.ReadToEnd();
                streamIn.Close();

                string txn_id = Request.Form["txn_id"];

                //string fileName = txn_id;

                //Directory.CreateDirectory(Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["PaypalNotificationForInitialTraining"])));
                //var filePath = Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["PaypalNotificationForInitialTraining"]), fileName + ".txt");

                //System.IO.File.WriteAllText(filePath, strRequest + "\n" + strResponse);

                // if PayPal response is successful / verified
                if (strResponse.Equals("VERIFIED"))
                {
                    // paypal has verified the data, it is safe for us to perform processing now

                    // extract the form fields expected: buyer and seller email, payment status, amount
                    string payerEmail = Request.Form["payer_email"];
                    string paymentStatus = Request.Form["payment_status"];
                    string receiverEmail = Request.Form["receiver_email"];
                    string metaData = Request.Form["mc_gross"]; // Kiran on 7/30/2014
                    string transactionId = Request.Form["txn_id"]; // Kiran on 7/30/2014
                    string paymentAmount = Request.Form["mc_gross"];
                    string itemName = HttpUtility.HtmlDecode(Request.Form["item_name1"]);
                    string custom = Request.Form["custom"];
                    if (paymentStatus.Equals("Completed") || paymentStatus.Equals("Pending"))
                    {
                        Logger.Log(LoggingLevel.Error, custom);
                        Guid paymentId = new Guid(custom);
                        var tempAssignBultkTrainingDetails = entityDB.TempAssignBulkTrainingDetails.FirstOrDefault(rec => rec.PaymentId == paymentId);

                        if (tempAssignBultkTrainingDetails != null)
                        {
                            foreach (var trainingDetails in tempAssignBultkTrainingDetails.BulkTrainingPaymentInfo.Split(';'))
                            {
                                {
                                    if (trainingDetails != "")
                                    {
                                        var training = trainingDetails.Split(':');
                                        var quizIdwithpayments = training[0].Split('-');
                                        long quizId = Convert.ToInt64(quizIdwithpayments[0]);

                                        var sysUserOrgQuiz = entityDB.SystemUsersOrganizationsQuizzes.Find(quizId);
                                        QuizPayments quizCheck = entityDB.QuizPayments.FirstOrDefault(rec => rec.SysUserOrgQuizId == quizId && rec.IsOpen != null && rec.IsOpen.Value);
                                        if (quizCheck == null)//Get current quiz submited payment
                                        {
                                            if (sysUserOrgQuiz.QuizResult != null && sysUserOrgQuiz.QuizResult.Value && !(((DateTime)sysUserOrgQuiz.QuizExpirationDate).AddDays(-30).Date <= DateTime.Now.Date))
                                                quizCheck = entityDB.QuizPayments.FirstOrDefault(rec => rec.SysUserOrgQuizId == quizId && rec.SubmittedDate == sysUserOrgQuiz.QuizDateSubmitted);
                                        }
                                        if (quizCheck == null)
                                        {
                                            Logger.Log(LoggingLevel.Error, "quizCheck is null");
                                            decimal quizAmount = Convert.ToDecimal(quizIdwithpayments[1]);
                                            quizCheck = new QuizPayments(); // Added by Kiran on 01/22/2016 for handling null exception
                                            quizCheck.PaymentReceivedAmount = quizAmount;
                                            quizCheck.PaymentReceivedDate = DateTime.Now;
                                            quizCheck.SysUserOrgQuizId = quizId;
                                            quizCheck.PaymentTransactionId = transactionId;
                                            quizCheck.TransactionMetaData = metaData;
                                            quizCheck.IsOpen = true;
                                            quizCheck.IsZeroFeeTraining =
                                                _employee.IsZeroFeeTraining(sysUserOrgQuiz.ClientTemplateId);
                                            quizCheck.IsFeeCapReached =
                                                _employee.IsVendorReachedTrainingFeeCap(sysUserOrgQuiz.VendorId,
                                                    sysUserOrgQuiz.ClientId);
                                            entityDB.QuizPayments.Add(quizCheck);
                                            entityDB.SaveChanges();
                                        }
                                        else
                                        {
                                            Logger.Log(LoggingLevel.Error, "quizCheck is not null");
                                            if (quizCheck.PaymentTransactionId == null)
                                            {
                                                quizCheck.PaymentTransactionId = transactionId;
                                                quizCheck.TransactionMetaData = metaData;
                                                entityDB.Entry(quizCheck).State = EntityState.Modified;
                                                entityDB.SaveChanges();
                                            }
                                        }
                                        Logger.Log(LoggingLevel.Error, "quizCheck saved");
                                        foreach (var prequalSite in training[1].Split('|'))
                                        {
                                            if (prequalSite != "") // Added if condition by Kiran on 01/22/2016 for handling null exception
                                            {
                                                var businessUnit = prequalSite.Split('_');

                                                long? businessUnitId = Convert.ToInt64(businessUnit[0]);
                                                long? businessUnitSiteId = Convert.ToInt64(businessUnit[1]);
                                                Logger.Log(LoggingLevel.Error, "Test1 - " + businessUnitSiteId.ToStringNullSafe());
                                                var clientTemplateForBU = entityDB.ClientTemplatesForBU.FirstOrDefault(rec => rec.ClientBusinessUnitId == businessUnitId && rec.ClientTemplateId == sysUserOrgQuiz.ClientTemplateId && rec.ClientTemplatesBUGroupPrice.GroupingFor == 2);
                                                var clientTemplatesForBUSite = entityDB.ClientTemplatesForBUSites.FirstOrDefault(rec => rec.ClientTemplateForBUId == clientTemplateForBU.ClientTemplateForBUId && rec.ClientBusinessUnitSiteId == businessUnitSiteId);
                                                if (clientTemplateForBU != null)
                                                {
                                                    Logger.Log(LoggingLevel.Error, "Test2 - " + clientTemplateForBU.ClientTemplateForBUId);
                                                }

                                                //QuizPayments addQuizPayment = new QuizPayments();
                                                //List<long> clientTemplateBUGroupIds = new List<long>();
                                                //clientTemplateBUGroupIds.Add((long)clientTemplateForBU.ClientTemplateBUGroupId);
                                                //clientTemplateBUGroupIds = clientTemplateBUGroupIds.Distinct().ToList();

                                                //if (!clientTemplateBUGroupIds.Contains((long)clientTemplateForBU.ClientTemplateBUGroupId))
                                                //{
                                                //    var groupPrice = entityDB.ClientTemplatesBUGroupPrice.Find(clientTemplateForBU.ClientTemplateBUGroupId).GroupPrice;
                                                //    addQuizPayment.PaymentReceivedAmount = groupPrice;
                                                //    addQuizPayment.PaymentReceivedDate = DateTime.Now;
                                                //    addQuizPayment.PaymentTransactionId = transactionId;
                                                //    addQuizPayment.SysUserOrgQuizId = quizId;
                                                //    addQuizPayment.TransactionMetaData = metaData;
                                                //    entityDB.QuizPayments.Add(addQuizPayment);
                                                //    entityDB.SaveChanges();

                                                if (clientTemplatesForBUSite != null)
                                                {
                                                    Logger.Log(LoggingLevel.Error, "Test3 - ");
                                                    QuizPaymentsItems addQuizPaymentItem = new QuizPaymentsItems();
                                                    long quizPaymentId;
                                                    if (quizCheck == null)
                                                        quizPaymentId = quizCheck.QuizPaymentId;
                                                    else
                                                        quizPaymentId = quizCheck.QuizPaymentId;
                                                    addQuizPaymentItem.QuizPaymentId = quizPaymentId;
                                                    var quizItems = entityDB.QuizPaymentsItems.FirstOrDefault(rec => rec.QuizPaymentId == quizPaymentId && rec.ClientTemplateForBUSitesId == clientTemplatesForBUSite.ClientTemplateForBUSitesId);
                                                    if (quizItems == null)
                                                    {
                                                        addQuizPaymentItem.ClientTemplateForBUId = clientTemplateForBU.ClientTemplateForBUId;
                                                        addQuizPaymentItem.ClientTemplateForBUSitesId = clientTemplatesForBUSite.ClientTemplateForBUSitesId;
                                                        addQuizPaymentItem.AsOfClientTemplateBUGroupId = clientTemplateForBU.ClientTemplateBUGroupId;
                                                        entityDB.QuizPaymentsItems.Add(addQuizPaymentItem);
                                                        entityDB.SaveChanges();

                                                        clientTemplatesForBUSite.IsPaymentDone = true;
                                                        entityDB.Entry(clientTemplatesForBUSite).State = EntityState.Modified;
                                                        entityDB.SaveChanges();
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        // Ends<<<
                    }
                    // else
                    else
                    {
                        // payment not complete yet, may be undergoing additional verification or processing

                        // do nothing - wait for paypal to send another IPN when payment is complete
                    }

                }
                string fileName = txn_id;

                Directory.CreateDirectory(Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["PaypalNotificationForInitialTraining"])));
                var filePath = Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["PaypalNotificationForInitialTraining"]), fileName + ".txt");

                System.IO.File.WriteAllText(filePath, strRequest + "\n" + strResponse);
            }

            catch (Exception ee)
            {
                //error = ee.Message;
                string fileName2 = DateTime.Now.ToString("MMddyyyyHHmmssfff");
                Directory.CreateDirectory(
                                  Path.Combine(
                                      HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["PaypalNotification"])));
                var filePath2 =
                    Path.Combine(
                        HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["PaypalNotification"]),
                        fileName2 + ".txt");

                System.IO.File.WriteAllText(filePath2, ee.Message);
                string toEmail = ConfigurationManager.AppSettings["EmailForPaymentNotice"];
                Utilities.SendMail.sendMail(toEmail, Request.Form["txn_id"],
                    "" + Request.Form["item_name1"] + " - Training Payment Crash");
                // To get the ipn history(last 28 days only) - https://www.paypal.com/us/cgi-bin/webscr?cmd=_display-ipns-history
                //SendMail.sendMail("kiran.talluri@ahaapps.com", ee.Message, "Training Payment Crash", null, "rajesh.pendela@ahaapps.com");
            }

            return null;
        }

        

        public ActionResult GenerateTrainingCompletionCertificate(long sysUserOrgQuizId)
        {
            SystemUsersOrganizationsQuizzes sysUserQuiz = entityDB.SystemUsersOrganizationsQuizzes.FirstOrDefault(rec => rec.SysUserOrgQuizId == sysUserOrgQuizId);
            string body = string.Empty;
            try
            {
                body = System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/html/TrainingCertificate" + sysUserQuiz.ClientId + ".html"));
            }
            catch
            {
                //If you cannot find a client specific one grab a generic one.
                body = System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/html/TrainingCertificate.html"));
            }
            string absolutePathOfHtmlFile = System.Web.HttpContext.Current.Server.MapPath("~/html/TrainingCertificate.html");

            Contact empContact = sysUserQuiz.SystemUsersOrganizations.SystemUsers.Contacts[0];
            body = body.Replace("[EmpName]", empContact.LastName + ", " + empContact.FirstName);
            body = body.Replace("[EmpSMISafetyId]", empContact.ContactId + "");
            body = body.Replace("[TrainingName]", sysUserQuiz.ClientTemplates.Templates.TemplateName);
            body = body.Replace("[QuizId]", sysUserOrgQuizId + "");
            body = body.Replace("[QuizSubmittedDate]", sysUserQuiz.QuizDateSubmitted.Value.Date.ToShortDateString() + " " + sysUserQuiz.QuizDateSubmitted.Value.ToShortTimeString());

            Directory.CreateDirectory(Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["TrainingCertificates"])));
            var path = Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["TrainingCertificates"]), "TrainingCertificate" + sysUserOrgQuizId + ".pdf");

            string pdf_page_size = "A4";
            PdfPageSize pageSize = (PdfPageSize)Enum.Parse(typeof(PdfPageSize),
                pdf_page_size, true);

            string pdf_orientation = "Landscape";
            PdfPageOrientation pdfOrientation =
                (PdfPageOrientation)Enum.Parse(typeof(PdfPageOrientation),
                pdf_orientation, true);

            int webPageWidth = 1024;

            int webPageHeight = 787;


            // instantiate a html to pdf converter object
            HtmlToPdf converter = new HtmlToPdf();

            // set converter options
            converter.Options.PdfPageSize = pageSize;
            converter.Options.PdfPageOrientation = pdfOrientation;
            converter.Options.WebPageWidth = webPageWidth;
            converter.Options.WebPageHeight = webPageHeight;

            // create a new pdf document converting an url
            PdfDocument doc = converter.ConvertHtmlString(body, absolutePathOfHtmlFile);
            if (path != null)
            {
                System.IO.File.Delete(path);
            }
            // save pdf document
            doc.Save(path);
            // close pdf document
            doc.Close();

            var fs = System.IO.File.OpenRead(path);

            Response.AppendHeader("Content-Disposition", "inline; filename=Training Certificate.pdf");
            return File(fs, "application/pdf");
        }
        
        public ActionResult GenerateQuizWaiverFormCertificate(long sysUserOrgQuizId)
        {
            var quiz = _quizBusiness.getQuizDetails(sysUserOrgQuizId);
            var employeeDetails = _sysUserBusiness.GetUserInfo(quiz.UserId);
            var vendorDetails = _organizationBusiness.GetOrganization((long)quiz.VendorId);
            var quizWaiverFormCompletionLog = _quizBusiness.getWaiverFormLog(sysUserOrgQuizId, quiz.QuizSubmittedDate);

            string body = string.Empty;
            try
            {
                body = System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/html/SafetyWaiverNotice" + quiz.ClientId + ".html"));
            }
            catch
            {
                //If you cannot find a client specific one grab a generic one.
                body = System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/html/SafetyWaiverNotice.html"));
            }
            string absolutePathOfHtmlFile = System.Web.HttpContext.Current.Server.MapPath("~/html/SafetyWaiverNotice.html");

            body = body.Replace("[Content]", quiz.WaiverFormContent);
            body = body.Replace("[EmployeeName]", employeeDetails.LastName + ", " + employeeDetails.FirstName);
            body = body.Replace("[VendorName]", vendorDetails.Name);
            body = body.Replace("[Signature]", quizWaiverFormCompletionLog.Signature);
            body = body.Replace("[WaiverFormSubmittedDate]", quiz.QuizSubmittedDate.ToString());
            body = body.Replace("[logo]", ConfigurationSettings.AppSettings["LogoUploadPath"].ToString() + "/"+ quiz.ClientId + ".png");
            Directory.CreateDirectory(Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["QuizWaiverForm"])));
            var path = Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["QuizWaiverForm"]), "QuizWaiverForm" + sysUserOrgQuizId + ".pdf");

            string pdf_page_size = "A4";
            PdfPageSize pageSize = (PdfPageSize)Enum.Parse(typeof(PdfPageSize),
                pdf_page_size, true);

            string pdf_orientation = "Portrait";
            PdfPageOrientation pdfOrientation =
                (PdfPageOrientation)Enum.Parse(typeof(PdfPageOrientation),
                pdf_orientation, true);

            int webPageWidth = 1024;

            int webPageHeight = 787;


            // instantiate a html to pdf converter object
            HtmlToPdf converter = new HtmlToPdf();

            // set converter options
            converter.Options.PdfPageSize = pageSize;
            converter.Options.PdfPageOrientation = pdfOrientation;
            converter.Options.WebPageWidth = webPageWidth;
            converter.Options.WebPageHeight = webPageHeight;

            // create a new pdf document converting an url
            PdfDocument doc = converter.ConvertHtmlString(body, absolutePathOfHtmlFile);
            if (path != null)
            {
                System.IO.File.Delete(path);
            }
            // save pdf document
            doc.Save(path);
            // close pdf document
            doc.Close();

            var fs = System.IO.File.OpenRead(path);

            Response.AppendHeader("Content-Disposition", "inline; filename=Waiver Form.pdf");
            return File(fs, "application/pdf");
        }

        [HttpGet]
        [SessionExpire]
        public ActionResult EmployeeAcknowledgement()
        {
            Guid userId = SSession.UserId;
            var empContact = entityDB.Contact.FirstOrDefault(rec => rec.UserId == userId);
            
            LocalEmployeeDetails employeeData = new LocalEmployeeDetails();
            employeeData.UserId = userId;
            employeeData.LastName = empContact.LastName;
            employeeData.FirstName = empContact.FirstName;
            employeeData.HasExistingData = empContact.DocumentType != null;
            ViewBag.EmployeeName = empContact.FullName;
            return View(employeeData);
        }
        [HttpPost]
        [SessionExpire]
        public ActionResult EmployeeAcknowledgement(LocalEmployeeDetails empData)
        {
            ViewBag.EmployeeName = empData.LastName.Trim() + ", " + empData.FirstName.Trim();
            if (empData.AckPol.Equals(false))
            {
                ViewBag.checkBoxStatus = false;
                return View(empData);
            }
            if(!empData.Signature.Trim().Equals(ViewBag.EmployeeName.Trim()))
            {
                ViewBag.signatureMismatch = false;
                return View(empData);
            }
            if (ModelState.IsValid && !empData.HasExistingData)
            {
                _employee.UpdateEmployeeDetailsAndAcknowledge(empData);
                _employee.AcknowledgeEmployee(empData.UserId);
                return RedirectToAction("EmployeeQuizzes");
            }
            if (empData.HasExistingData)
            {
                _employee.AcknowledgeEmployee(empData.UserId);
                return RedirectToAction("EmployeeQuizzes");
            }
            return View(empData);
            
        }
    }
}

