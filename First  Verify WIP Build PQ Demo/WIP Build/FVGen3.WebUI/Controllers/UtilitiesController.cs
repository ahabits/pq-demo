﻿/*
Page Created Date:  07/16/2013
Created By: Kiran Talluri
Purpose:
Version: 1.0
****************************************************
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FVGen3.WebUI.Controllers
{
    public class UtilitiesController : BaseController
    {
        //
        // GET: /Utilities/

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult SessionExpirePage()
        {
            return View();
        }
        [ChildActionOnly]
        public ActionResult GetHtmlPage(string path)
        {
            return new FilePathResult(path, "text/html");
        }
    }
}
