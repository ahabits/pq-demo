﻿/*
Page Created Date:  29/04/2013
Created By: Rajesh Pendela
Purpose:
Version: 1.0
****************************************************
Date:7/04/2013 Changes by: Suma
Fixed issues in login page, changed functionality
****************************************************
****************************************************
Date:7/24/2013 Changes by: Rajesh
For Prequalification Sites created a new function:ClientBusUnitsByLocation
****************************************************
Date:8/08/2013 Changes by: Kiran
For Bidding interest's addition and activation of same
****************************************************
Date:8/06/2013 Changes by: Suma
For Template pricing - tiered pricing structure
****************************************************
Date:8/14/2013 Changes by: Rajesh
Changes has been done in Dashboard, this is what decides where the user navigates to
****************************************************

*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FVGen3.Domain.Abstract;
using FVGen3.Domain.Entities;
using FVGen3.Domain.Concrete;
using System.Data.Entity;
using FVGen3.WebUI.Annotations;
using System.Configuration;
using FVGen3.WebUI.Utilities;
using System.IO;
using FVGen3.WebUI.Enums;
using WebGrease.Css;
using FVGen3.BusinessLogic.Interfaces;
using FVGen3.DataLayer.DTO;
using FVGen3.WebUI.Constants;
using FVGen3.Domain.LocalModels;
using FVGen3.WebUI.Utilities.ErrorLogs;
using Resources;
using System.Threading.Tasks;
using log4net.Repository.Hierarchy;

namespace FVGen3.WebUI.Controllers
{
    public class SystemUsersController : BaseController
    {
        //
        // GET: /SystemUsers/

        // EFDbContext entityDB;
        private ISystemUserBusiness _sysUser;
        private IClientBusiness _clientBusiness;
        private IEmployeeBusiness _emp;
        private IOrganizationBusiness _org;
        Messages.Messages messages;
        public SystemUsersController(ISystemUsersRepository repositorySystemUsers, ISystemUserBusiness _sysUser, IEmployeeBusiness _emp, IClientBusiness _clientBusiness, IOrganizationBusiness _org)
        {
            //   entityDB = new EFDbContext();
            messages = new Messages.Messages();
            this._sysUser = _sysUser;
            this._clientBusiness = _clientBusiness;
            this._emp = _emp;
            this._org = _org;
        }
        [SessionExpire]
        public ActionResult Index()
        {
            //return View(entityDB.SystemUsers);
            return View();
        }

        public ActionResult Maintenance()
        {
            return View();
        }
        //public async Task<ActionResult> GetAllLanguages()
        //{
        //    return Json(await _clientBusiness.GetLanguagesAsync());
        //}
        public async Task<ActionResult> GetAllLanguages()
        {
            return Json(await _clientBusiness.GetLanguagesAsync());
        }
        public ActionResult EmpRegistration()
        {
            return View();
        }
        [HttpPost]
        public ActionResult EmpRegistration(EmpRegistrationLocalModel form)
        {

            var result = "";
            form.Password = Utilities.EncryptionDecryption.EncryptData(form.Password);
            try { result = _emp.AddEmployee(form); }
            catch (Exception e)
            {
                result = Resources.Resources.Unable_To_Process;
                return View(form);
            }
            return Json(result);
        }
        [SessionExpire]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "MyAccount", sideMenuName = "ChangePassword", ViewName = "ChangePassword")]
        public ActionResult ChangePassword()
        {
            Guid guidUserId = SSession.UserId;

            LocalPasswordModel m = new LocalPasswordModel();
            m.id = guidUserId;

            ViewBag.Id = guidUserId;
            Session["UserId"] = guidUserId;
            return View(m);
        }
        public bool isTest()
        {
            return ConfigurationSettings.AppSettings["IsTest"] == "true";
        }
        // Rajesh, 01/05/2013 --->
        // Change Password ---->
        [HttpPost]
        [SessionExpire]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "MyAccount", sideMenuName = "ChangePassword", ViewName = "ChangePassword")]
        public ActionResult ChangePassword(LocalPasswordModel modelLocalpassword)
        {
            if (ModelState.IsValid)
            {
                SystemUsers SysUser_currentUser = _sysUser.FindUser(SSession.UserId);
                if (!modelLocalpassword.NewPassword.Any(characters => char.IsDigit(characters)) || !modelLocalpassword.NewPassword.Any(characters => char.IsUpper(characters)) || !modelLocalpassword.NewPassword.Any(characters => char.IsLower(characters)))
                {
                    ModelState.AddModelError("NewPassword", "*");
                    ModelState.AddModelError("", @Resources.Resources.Changepassword_newpasswordrequired);
                }
                else
                {
                    if (!SysUser_currentUser.Password.Equals(EncryptionDecryption.EncryptData(modelLocalpassword.OldPassword)))
                    {
                        ModelState.AddModelError("OldPassword", @Resources.Resources.ChangePassword_InvalidPassword);
                    }
                }
                if (ModelState.IsValid)
                {
                    _sysUser.UpdateUserPassword(EncryptionDecryption.EncryptData(modelLocalpassword.NewPassword), SSession.UserId);

                    ViewBag.isSuccess = true;
                    return View(new LocalPasswordModel());
                }
                return View(new LocalPasswordModel());
            }
            return View(modelLocalpassword);
        }

        //>>DV:SB
        //to display myAccount page, with details 
        //returns view with localmyaccountmodel
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "MyAccount", sideMenuName = "MyAccount", ViewName = "MyAccount")]
        [SessionExpire]
        public ActionResult MyAccount()
        {
            ViewBag.isSuccess = false;
            Guid guidRecordId = SSession.UserId;
            return View(_sysUser.GetAccount(guidRecordId));
        }

        /*
        Created Date:  07/19/2013   Created By: Kiran Talluri
        Purpose: To display Client's business units in the My Account view
        */

        [HttpGet]
        [SessionExpireForView]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "MyAccount", sideMenuName = "MyAccount", ViewName = "UpdateClientUnitAndSite")]
        public ActionResult UpdateClientUnitAndSite(long clientId, long siteId, long selectedBusUnitId) // Added parameter UnitLocation on 07/24/2013 by Kiran
        {
            ViewBag.siteExists = false;

            var currentUserOrgBusUnitNames = from busUnits in _sysUser.GetClientBuUnits(clientId)   //Move to Business Layer By Vasavi                                                                                                            //SysUser_currentUser.Password = EncryptionDecryption.EncryptData(modelLocalpassword.NewPassword);
                                             select new SelectListItem
                                             {
                                                 Text = busUnits.BusinessUnitName,
                                                 Value = busUnits.ClientBusinessUnitId.ToString(),
                                                 Selected = (selectedBusUnitId == busUnits.ClientBusinessUnitId)
                                             };

            ViewBag.VBcurrentUserOrgBusUnitNames = currentUserOrgBusUnitNames;
            LocalUpdateClientUnitAndSite currentClientUnitSite = new LocalUpdateClientUnitAndSite();
            currentClientUnitSite.oldSiteId = siteId;
            currentClientUnitSite.selectedBusUnitId = selectedBusUnitId;
            currentClientUnitSite.unitId = selectedBusUnitId;
            currentClientUnitSite.currentSiteId = siteId;
            return View(currentClientUnitSite);
        }
        // Ends <<<<

        /*
        Created Date:  07/19/2013   Created By: Kiran Talluri
        Purpose: To Update Client's business units & sites from MyAccount view
        */
        [SessionExpire]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "Power BI Report", sideMenuName = "", ViewName = "PowerBiReports")]
        public ActionResult PowerBiReports(int reportId = 0)
        {
            var reports = _sysUser.GetUserReports(SSession.OrganizationId, SSession.UserId, SSession.Role);

            if (reports.Any())
            {
                var report = reports.FirstOrDefault(r => reportId != 0 && r.ReportId == reportId);
                if (report == null) reports[0].IsSelected = true;
                else report.IsSelected = true;
                ViewBag.Url = reports.FirstOrDefault(r => r.IsSelected).Url;
            }

            return View(reports);
        }
        [SessionExpireForView(pageType = "popupview")]
        [HttpPost]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "MyAccount", sideMenuName = "MyAccount", ViewName = "UpdateClientUnitAndSite")]
        public ActionResult UpdateClientUnitAndSite(LocalUpdateClientUnitAndSite currentUnitSite)
        {
            // Added line on 07/24/2013 by Kiran
            ViewBag.siteExists = false;
            currentUnitSite.selectedBusUnitId = currentUnitSite.unitId;
            var currentUserOrgBusUnitNames = from busUnits in _sysUser.GetClientBuUnits(currentUnitSite.clientId)//Move to Business Layer By Vasavi
                                             select new SelectListItem
                                             {
                                                 Text = busUnits.BusinessUnitName,
                                                 Value = busUnits.ClientBusinessUnitId.ToString(),
                                                 Selected = (currentUnitSite.selectedBusUnitId == busUnits.ClientBusinessUnitId)
                                             };

            ViewBag.VBcurrentUserOrgBusUnitNames = currentUserOrgBusUnitNames;
            // Changes Ends <<<<
            if (ModelState.IsValid)
            {
                Guid userId = SSession.UserId;          //Changed By Vasavi

                // to remove the old site safety representative
                _sysUser.RemoveClientBusinessUnitSiteRepresentatives(currentUnitSite.oldSiteId);  //Move to Business Layer By Vasavi
                //var oldSiteRepresentativeRecord = entityDB.ClientBusinessUnitSiteRepresentatives.FirstOrDefault(record => record.ClientBUSiteID == currentUnitSite.oldSiteId);

                //entityDB.ClientBusinessUnitSiteRepresentatives.Remove(oldSiteRepresentativeRecord);
                ////entityDB.SaveChanges();

                // to insert a new site safety representative 
                var currentSiteRecord = _sysUser.GetClientBusinessUnitSites(currentUnitSite.currentSiteId); //Move to BusinessLayer By Vasavi
                                                                                                            //   var currentSiteRecord = entityDB.ClientBusinessUnitSites.FirstOrDefault(record => record.ClientBusinessUnitSiteId == currentUnitSite.currentSiteId);

                // Changes to display error message if Site exist for a safety rep
                // Changes on 07/24/2013 by Kiran
                var currentUserSiteSafetyRepresentatives = _sysUser.GetClientBuSiteRepresentatives(userId);       // Move to BusinessLayer By Vasavi
                foreach (var safetyRepresentative in currentUserSiteSafetyRepresentatives)
                {
                    if (safetyRepresentative.ClientBusinessUnitSites != null)
                    {
                        if (safetyRepresentative.ClientBUSiteID == currentSiteRecord.ClientBusinessUnitSiteId)
                        {
                            ViewBag.siteExists = true;
                            return View(currentUnitSite);
                        }
                    }
                }
                // Ends <<<

                _sysUser.InsertClientBuSiteRepresentatives(currentSiteRecord.ClientBusinessUnitSiteId, userId);


                currentUnitSite.UserId = userId;
                return RedirectToAction("CloseUnitSite", currentUnitSite);
            }
            return View(currentUnitSite);
        }
        // Ends <<<<

        /*
        Created Date:  07/19/2013   Created By: Kiran Talluri
        Purpose: Closing the popup view & to update the client's business unit & its related sites in partial page of MyAccount view
        */
        [HttpPost]
        public ActionResult GetUserInfo()
        {
            return Json(SSession);
        }
        [ValidateInput(false)]
        public ActionResult SaveError(string msg, string url)
        {
            string path = Server.MapPath(@"\ajaxError\") + SSession.UserId + "\\";
            System.IO.Directory.CreateDirectory(path);
            var fileName = path.CombinePath(DateTime.Now.ToString("mm-dd-yyyy HH:MM a")) + ".html";
            System.IO.File.WriteAllText(fileName, msg);
            return Json(fileName);
        }
        public ActionResult CloseUnitSite(LocalUpdateClientUnitAndSite unitSite)
        {
            return View(unitSite);
        }
        // Ends <<<

        /*
        Created Date:  07/19/2013   Created By: Kiran Talluri
        Purpose: To display Client's business unit Sites
        Also used by Rajesh for Prequalification Sites
        */

        public ActionResult ClientBusUnitSites(long busUnitId)
        {
            /*var busUnit = _sysUser.GetClientBusinessUnits(busUnitId);  */

            //entityDB.ClientBusinessUnits.FirstOrDefault(record => record.ClientBusinessUnitId == busUnitId);
            var curretUserOrgBusUnitSites = new List<SelectListItem>();
            curretUserOrgBusUnitSites.Add(new SelectListItem() { Text = "", Value = "" });
            curretUserOrgBusUnitSites.AddRange((from busUnitSites in _sysUser.GetClientBusinessUnitSitesList(busUnitId)
                                                select new SelectListItem
                                                {
                                                    Text = busUnitSites.SiteName + " - " + busUnitSites.BusinessUnitLocation,
                                                    Value = busUnitSites.ClientBusinessUnitSiteId.ToString(),
                                                }).ToList());

            ViewBag.VBcurretUserOrgBusUnitSites = curretUserOrgBusUnitSites;

            return Json(curretUserOrgBusUnitSites);
        }

        /*
        Created Date:  3/12/2014   Created By: Kiran Talluri
        Purpose: To display Client's business unit Sites
        Used by Rajesh for Prequalification Sites
        */

        public ActionResult PrequalificationClientBusUnitSites(long busUnitId)
        {
            /* var busUnit = _sysUser.GetClientBusinessUnits(busUnitId);*/  // Kiran on 3/22/2014

            var currentUserOrgBusUnitSites = from busUnitSites in _sysUser.GetClientBusinessUnitSitesList(busUnitId)
                                             select new ClientBusinessUnitsSiteWithType
                                             {
                                                 Text = busUnitSites.BusinessUnitLocation.ToString().Trim() == "" ? busUnitSites.SiteName : busUnitSites.SiteName + " - " + busUnitSites.BusinessUnitLocation, // Kiran on 11/27/2014
                                                 Value = busUnitSites.ClientBusinessUnitSiteId.ToString(),
                                             };
            ClientBusinessUnitsSiteWithType emptyItem = new ClientBusinessUnitsSiteWithType();
            emptyItem.Text = "Select BU Site";//Rajeshy on 2/16/2015
            emptyItem.Value = "";
            emptyItem.Type = 0;

            //Rajesh on 2/16/2015
            List<ClientBusinessUnitsSiteWithType> currentUserOrgBusUnitSiteswithDefaultItem = new List<ClientBusinessUnitsSiteWithType>();
            //var currentUserOrgBusUnitSiteswithDefaultItem = currentUserOrgBusUnitSites.ToList();
            currentUserOrgBusUnitSiteswithDefaultItem.Add(emptyItem);
            currentUserOrgBusUnitSiteswithDefaultItem.AddRange(currentUserOrgBusUnitSites.ToList());
            //Ends<<<

            ViewBag.VBcurretUserOrgBusUnitSites = currentUserOrgBusUnitSiteswithDefaultItem;
            return Json(currentUserOrgBusUnitSiteswithDefaultItem);

            // Ends<<<
        }
        // Ends <<<<

        // Sandhya on 04/08/2015 under Kiran guidance
        public ActionResult GetClientBusUnitSites(long busUnitId, long prequlificationId)
        {
            var currentPQ = _sysUser.FindPq(prequlificationId);  // Move to BusinessLayer By Vasavi
                                                                 // entityDB.Prequalification.Find(prequlificationId);
            var exisitingSites = _sysUser.GetClientBusinessUnitSiteIdsInPq(prequlificationId); // Move to BusinessLayer By Vasavi
            //entityDB.PrequalificationSites.Where(rec => rec.PrequalificationId == prequlificationId).Select(rec => rec.ClientBusinessUnitSiteId).ToList(); // mani on 04/10/2015
            var busUnit = _sysUser.GetClientBusinessUnits(busUnitId);// Move to BusinessLayer By Vasavi

            // Kiran on 3/22/2014
            var selectedRegions = _sysUser.GetPQClients(prequlificationId)?.RegionsList;
            var sites = _sysUser.GetClientBusinessUnitSitesList(busUnitId).Where(rec => !exisitingSites.Contains(rec.ClientBusinessUnitSiteId));
            if (selectedRegions != null && selectedRegions.Any())
            {
                var statesList = _sysUser.GetStates(selectedRegions);// Move to BusinessLayer By Vasavi
                                                                     //entityDB.RegionStates.Where(r => selectedRegions.Contains(r.Regionid)).Select(r => r.StateName).ToList();


                //if (selectedRegions.Contains(RegionsConstant.INTERNATIONAL))
                //{
                //    var interNationalRegions = entityDB.ClientBusinessUnitSites.Where(r => !entityDB.RegionStates.Any(r1 => r1.StateName == r.BusinessUnitLocation)).Select(r => r.BusinessUnitLocation).Distinct().ToList();
                //    statesList.AddRange(interNationalRegions);
                //}
                sites = sites.Where(r => statesList.Contains(r.BusinessUnitLocation));
            }

            var currentUserOrgBusUnitSites = from busUnitSites in sites.ToList()
                                             select new ClientBusinessUnitsSiteWithType
                                             {
                                                 Text = busUnitSites.BusinessUnitLocation.ToString().Trim() == "" ? busUnitSites.SiteName : busUnitSites.SiteName + " - " + busUnitSites.BusinessUnitLocation, // Kiran on 11/27/2014
                                                 Value = busUnitSites.ClientBusinessUnitSiteId.ToString(),
                                             }; // Mani on 04/10/2015


            ClientBusinessUnitsSiteWithType emptyItem = new ClientBusinessUnitsSiteWithType();
            emptyItem.Text = "Select All";//Rajesh on 2/16/2015
            emptyItem.Value = "999";
            emptyItem.Type = 0;

            //Rajesh on 2/16/2015
            List<ClientBusinessUnitsSiteWithType> currentUserOrgBusUnitSiteswithDefaultItem = new List<ClientBusinessUnitsSiteWithType>();
            //var currentUserOrgBusUnitSiteswithDefaultItem = currentUserOrgBusUnitSites.ToList();

            if (currentUserOrgBusUnitSites.Count() != 0) // Mani on 04/10/2015
                currentUserOrgBusUnitSiteswithDefaultItem.Add(emptyItem);
            currentUserOrgBusUnitSiteswithDefaultItem.AddRange(currentUserOrgBusUnitSites.OrderBy(rec => rec.Text).ToList());
            //Ends<<<

            ViewBag.VBcurretUserOrgBusUnitSites = currentUserOrgBusUnitSiteswithDefaultItem;
            return Json(currentUserOrgBusUnitSiteswithDefaultItem);

            // Ends<<<
        }

        //Dev:RP DT:-7/24/2013
        //To get all bussiness units related to location, extended the above function
        //public ActionResult ClientBusUnitsByLocation(string busUnitlocation, long preQualificationId)
        //{
        //    //var busUnit = entityDB.ClientBusinessUnits.FirstOrDefault(record => record.ClientBusinessUnitId == busUnitId);

        //    long clientId = entityDB.Prequalification.Find(preQualificationId).ClientId;
        //    var curretUserOrgBusUnits = from busUnits in entityDB.ClientBusinessUnits.Where(rec => rec.ClientId == clientId).ToList()
        //                                where busUnits.BusinessUnitLocation == busUnitlocation
        //                                select new SelectListItem
        //                                {
        //                                    Text = busUnits.BusinessUnitName,
        //                                    Value = busUnits.ClientBusinessUnitId.ToString(),
        //                                };



        //    return Json(curretUserOrgBusUnits);
        //}
        // Ends <<<<

        // Ends <<<<

        /*
        Created Date:  07/20/2013   Created By: Kiran Talluri
        Purpose: To delete the Client's BU site
        */

        [SessionExpireForView(pageType = "partial")]
        public string DeleteLoggedinClientUnitSite(long siteId)
        {
            _sysUser.RemoveClientBusinessUnitSiteRepresentatives(siteId);  //Moved to Business Layer By Vasavi
                                                                           // var currentSiteRecord = entityDB.ClientBusinessUnitSiteRepresentatives.FirstOrDefault(record => record.ClientBUSiteID == siteId);
                                                                           //entityDB.ClientBusinessUnitSiteRepresentatives.Remove(currentSiteRecord);
                                                                           //entityDB.SaveChanges();
            return "SiteDeleteSuccess";
        }
        // Ends <<<

        /*
        Created Date:  07/20/2013   Created By: Kiran Talluri
        Purpose: To display Business units & sites of the client
        */

        [SessionExpireForView(pageType = "partial")]
        [OutputCache(Duration = 0)]
        public ActionResult PartialClientUnitAndSites()
        {
            Guid registeredUserId = SSession.UserId;// (Guid)Session["UserId"]; Changed By Vasavi
            ViewBag.VBunitSiteRepresentatives = _sysUser.GetSiteRepresents(registeredUserId);//Moved to Business Layer By Vasavi
                                                                                             //var busUnitSiteRepresentatives =entityDB.ClientBusinessUnitSiteRepresentatives.Where(record => record.SafetyRepresentative == registeredUserId).ToList();
                                                                                             //  ViewBag.VBunitSiteRepresentatives = busUnitSiteRepresentatives;
            return PartialView();
        }
        // Ends <<<


        //>>DV:SB
        //to save changes in myAccount page
        //takes localmyaccountmodel Form as argument
        //save changes and returns the same form with ViewBag success message.
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "MyAccount", sideMenuName = "MyAccount", ViewName = "MyAccount")]
        [AcceptVerbs(HttpVerbs.Post)]
        [SessionExpire]
        public ActionResult MyAccount(LocalMyAccountModel form)
        {
            if (SSession.Role.ToString().Equals(LocalConstants.Admin) || SSession.Role.ToString().Equals(LocalConstants.Client))
            {
                ModelState.Remove("PrincipalCompanyOfficerName");
            }
            // If valid, save to database
            if (ModelState.IsValid)
            {
                _sysUser.UpdateSystemUsersAccount(form);

                var userOrgId = _sysUser.GetSystemUsersOrgs(form.UserId).OrganizationId;

                var userOrganizationData = _sysUser.GetOrgs(userOrgId);

                if (userOrganizationData.OrganizationType == "Vendor")
                {
                    if (userOrganizationData.PrincipalCompanyOfficerName != form.PrincipalCompanyOfficerName)
                    {
                        var pathForCSI = HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["WebResponseFiles"]);
                        var data = Utilities.CSIWebServiceInitiateRequest.CSIWebService(userOrganizationData.OrganizationID, null, form.PrincipalCompanyOfficerName, pathForCSI); // Kiran on 8/8/2014

                        var adminSuperUserEmail = _sysUser.GetAdminSuperUserEmail();
                        string body = System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Views/Companies/EmailFormatForvendorInformation.htm"));

                        string subject = form.CompanyName + " changed its company information";
                        body = body.Replace("_Vendor", form.CompanyName);
                        var items = "";
                        if (userOrganizationData.PrincipalCompanyOfficerName != form.PrincipalCompanyOfficerName)
                        {
                            items += "<li style='width:100%;float:left;padding:0.5% 0;'>Principal Officer Name</li>";
                        }
                        body = body.Replace("_items", items);

                        Utilities.SendMail.sendMail(adminSuperUserEmail, body, subject);
                        _sysUser.AddArchiveOrgDetails(userOrganizationData.OrganizationID, form.UserId);
                    }
                }

                _sysUser.UpdateUserOrgnizationdata(SSession.Role, userOrganizationData, form);
                ViewBag.isSuccess = true;
                return View(form);
            }

            // Otherwise, reshow form
            ViewBag.isSuccess = false;
            return View(form);
        }
        //<<SB MyAccount HTTP Post Ends <<<


        //>>DV:SLJ
        // SUMA on (MM/DD/YYYY)05/05/2013 For Login >>>
        // Dev:SLJ >>>
        [Localization]
        //(if messageType == 1)-->Your registration has been completed successfully, please log in with your userid and password. 
        //if (messageType==2)-->Password has been changed successfully.
        //if (messageType==3)-->Session Expired.
        public ActionResult Login(String lastLoginPage, int? messageType)
        {
            //Logger.Initialize();
            ViewBag.messageType = messageType;
            ViewBag.PageType = "Login";
            LocalLoginModel local = new LocalLoginModel();
            local.lastLoginURl = lastLoginPage;
            HttpCookie cookie = Request.Cookies["Login"];

            if (cookie != null && cookie.Values["UserName"] != null)
            {
                local.UserName = cookie.Values["UserName"];
                local.Password = cookie.Values["Password"];
                string encryptedpassword = Utilities.EncryptionDecryption.EncryptData(local.Password);
                if (cookie.Values["isLogin"].Equals("true"))
                {
                    var varlogMail = _sysUser.GetUser(local.UserName, encryptedpassword);//moved to business layer by vasavi
                                                                                         // var varlogMail = entityDB.SystemUsers.FirstOrDefault(a => a.UserName == local.UserName && a.Password == encryptedpassword);



                    if (varlogMail != null)
                    {
                        // Rajesh on 09/05/2013 to check the user status
                        if (varlogMail.UserStatus == null || varlogMail.UserStatus == false)
                        {
                            ModelState.AddModelError("Password", @Resources.Resources.Login_UserIdInactive);
                            return View(local);
                        }
                        // Ends <<<<

                        Session["UserId"] = varlogMail.UserId;
                        Session["UserName"] = varlogMail.UserName;
                        Session["Email"] = varlogMail.Email;

                        //var varSysUser = entityDB.SystemUsers.First(m => m.UserId == varlogMail.UserId);
                        var userInfo = _sysUser.GetUserInfo(varlogMail.UserId);
                        //var varSysUserOrg = varlogMail.SystemUsersOrganizations.FirstOrDefault();

                        var varOrgID = userInfo.OrgId;
                        var varOrgType = userInfo.OrgType;
                        Session["RoleName"] = varOrgType;
                        Session["currentOrgId"] = varOrgID;
                        // Kiran 11/27/2013 >>>
                        var loginVendorId = Convert.ToInt64(Session["currentOrgId"]);
                        Session["LoginOrganizationName"] = _sysUser.GetOrgs(loginVendorId).Name;
                        // Ends <<<

                        try
                        {
                            setUpMenus(varOrgType);
                        }
                        catch (Exception ee)
                        {
                            ModelState.AddModelError("", @Resources.Resources.Login_noroleAvailable);
                            return View(local);
                        }

                        //If user doesn't have any permissions >>>
                        // Rajesh on 08/14/2013
                        var headerMenus = (List<LocalHeaderMenuModel>)HttpContext.Session["headerMenus"];
                        if (headerMenus == null || headerMenus.Count == 0)
                        {
                            ModelState.AddModelError("", @Resources.Resources.Login_noroleAvailable);
                            return View(local);
                        }
                        // Ends <<<<

                        //if (varlogMail.Contacts != null && varlogMail.Contacts.Count > 0)
                        //    Session["UserName"] = varlogMail.Contacts[0].FirstName;
                        //else
                        //    Session["UserName"] = varlogMail.UserName;
                        if (!string.IsNullOrEmpty(userInfo.FirstName))
                            Session["UserName"] = userInfo.FirstName + "," + userInfo.LastName;
                        else
                            Session["UserName"] = userInfo.Email;

                        lastLoginPage = getLastLoginURL(lastLoginPage);    //Rajesh on 08/16/2013

                        if (lastLoginPage != null)
                        {
                            return Redirect(local.lastLoginURl);

                        }
                        else
                        {
                            // Commented below line and added last line
                            // Rajesh on 08/14/2013
                            //if (varOrgType.Equals("Admin"))
                            //{
                            //    return RedirectToAction("AdminDashboard", "AdminVendors");
                            //}
                            //else if (varOrgType.Equals("Vendor"))
                            //{
                            //    return RedirectToAction("VendorDashboard", "Vendor");
                            //}
                            //else if (varOrgType.Equals("Client"))
                            //{
                            //    return RedirectToAction("ClientDashboard", "Client");
                            //}
                            //else
                            //    return View();
                            return RedirectToAction("DashBoard");
                            // Ends <<<
                        }
                    }

                    else
                        return View(local);
                }
                else
                {
                    return View(local);
                }
            }
            else
                return View(local);

        }
        // Suma - Login Http Get Ends <<<

        // SUMA on (MM/DD/YYYY)05/05/2013 For Login >>>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Login(LocalLoginModel Sys)
        {
            //HttpContext.Application["isconfirm"] = false;
            //If both the fields are Empty
            ViewBag.PageType = "Login";
            if ((String.IsNullOrEmpty(Sys.UserName)) && (String.IsNullOrEmpty(Sys.Password)))
            {
                ModelState.AddModelError("UserName", @Resources.Resources.Login_UserName);
                return View(Sys);
            }

            //If UserName field is Empty
            else if (String.IsNullOrEmpty(Sys.UserName))
            {
                ModelState.AddModelError("UserName", @Resources.Resources.Login_UserName);
                return View(Sys);

            }

            //If Password Field is Empty
            else if (String.IsNullOrEmpty(Sys.Password))
            {
                ModelState.AddModelError("UserName", @Resources.Resources.Login_Password);
                return View(Sys);

            }
            //After Clientside Validations
            if (ModelState.IsValid)
            {
                var varlogMail = _sysUser.GetSystemUser(Sys.UserName); //Moved to Business Layer by vasavi
                                                                       //var varlogMail = entityDB.SystemUsers.FirstOrDefault(a => a.UserName == Sys.UserName);
                string encryptedpwd = Utilities.EncryptionDecryption.EncryptData(Sys.Password);
                //SystemUserLogs userlog = new SystemUserLogs();

                //If UserName is wrong
                if (varlogMail == null)
                {
                    ModelState.AddModelError("Password", @Resources.Resources.Login_InvalidUserName);
                    return View(Sys);
                }
                else
                {
                    if (!varlogMail.Password.Equals(encryptedpwd))//If password Wrong
                    {
                        ModelState.AddModelError("Password", @Resources.Resources.Login_InvalidPassword);
                        _sysUser.UpdateWrongPasswordAttempts(varlogMail);  //Moved To Business Layer By vasavi
                        //int? FailedCount = varlogMail.FailedPasswordAttemptCount;
                        //varlogMail.FailedPasswordAttemptWindowStart = DateTime.Now;
                        //varlogMail.FailedPasswordAttemptCount = FailedCount + 1;
                        //entityDB.Entry(varlogMail).State = EntityState.Modified;
                        //entityDB.SaveChanges();

                        return View(Sys);
                    }
                    else//If login success
                    {
                        //Suma DT7/17/2013 -->>>To handle user status(Is Email verified or not)
                        if (varlogMail.UserStatus == null || varlogMail.UserStatus == false)
                        {
                            ModelState.AddModelError("Password", @Resources.Resources.Login_UserIdInactive);
                            return View(Sys);
                        }
                        //--<<<End
                        //var orgID = SysUserOrg.OrganizationId;
                        var SysUser = _sysUser.FindUser(varlogMail.UserId); //Moved to Business Layer by vasavi
                                                                            // var SysUser = entityDB.SystemUsers.First(m => m.UserId == varlogMail.UserId);
                                                                            //var SysUserOrg = SysUser.SystemUsersOrganizations.FirstOrDefault(m => m.SystemUsers.UserId == SysUser.UserId);
                        var SysUserOrg = _sysUser.GetSystemUserOrganizations(SysUser.UserId);

                        if (SysUserOrg == null)
                        {
                            ModelState.AddModelError("", @Resources.Resources.Login_OrgNotAvailable);
                            return View(Sys);
                        }
                        var orgID = SysUserOrg.OrganizationId;
                        //var varUserrole="";
                        //Save values in SystemusersLog table
                        _sysUser.UpdateSystemUserLogs(varlogMail.UserId);//moved to business Layer by vasavi
                                                                         //userlog.SystemUserId = varlogMail.UserId;
                                                                         //userlog.SystemActionId = 1;//Login
                                                                         //userlog.SystemActionWhen = DateTime.Now;
                                                                         //entityDB.SystemUserLogs.Add(userlog);

                        //Save values in SystemUsers Table
                        //varlogMail.Email = varlogMail.UserName;
                        _sysUser.UpdateLoginDate(varlogMail.UserId);
                        //  varlogMail.LastLoginDate = DateTime.Now;
                        //   entityDB.Entry(varlogMail).State = EntityState.Modified;
                        //   entityDB.SaveChanges();

                        //Get the FirstName of the User
                        var varContactRec = _sysUser.GetName(varlogMail.UserId);//moved to business Layer by vasavi
                                                                                // var varContactRec = entityDB.Contact.FirstOrDefault(m => m.UserId == varlogMail.UserId);
                                                                                //ViewBag.VBContactName = varContactRec.FirstName;

                        //Cookies 
                        if (Sys.RememberMe == true)
                        {
                            Response.Cookies["Login"]["UserName"] = Sys.UserName;
                            //Response.Cookies["Login"].Expires = DateTime.Now.AddYears(1);
                            Response.Cookies["Login"]["Password"] = Sys.Password;
                            Response.Cookies["Login"]["isLogin"] = "true";

                            Response.Cookies["Login"].Expires = DateTime.Now.AddYears(1);
                        }

                        //Creating session variable for UserId
                        Session["UserId"] = varlogMail.UserId;
                        Session["UserName"] = _sysUser.LoginUserName(varlogMail);
                        //if (varlogMail.Contacts != null && varlogMail.Contacts.Count > 0)
                        //    Session["UserName"] = varlogMail.Contacts[0].FirstName + ", " + varlogMail.Contacts[0].LastName;
                        //else
                        //    Session["UserName"] = varlogMail.UserName;


                        //Session["UserName"] = varContactRec.FirstName; //varlogMail.UserName;


                        var recOrganizations = _sysUser.GetOrgs(orgID);
                        //var recOrganizations = entityDB.Organizations.FirstOrDefault(m => m.OrganizationID == orgID);
                        var OrgType = recOrganizations.OrganizationType;
                        Session["RoleName"] = OrgType;
                        Session["currentOrgId"] = recOrganizations.OrganizationID;
                        Session["Email"] = SysUser.Email;
                        // Kiran 11/27/2013 >>>
                        var loginVendorId = Convert.ToInt64(Session["currentOrgId"]);
                        Session["LoginOrganizationName"] = _sysUser.GetOrgs(orgID).Name;// entityDB.Organizations.FirstOrDefault(rec => rec.OrganizationID == loginVendorId).Name;
                        // Ends <<<
                        setUpMenus(OrgType);
                        //try
                        //{
                        //    setUpMenus(OrgType);
                        //}
                        //catch (Exception ee)
                        //{
                        //    ModelState.AddModelError("", @Resources.Resources.Login_noroleAvailable);
                        //    return View(Sys);
                        //}

                        //If user doesn't have any permissions >>>
                        // Rajesh on 08/14/2013
                        var headerMenus = (List<LocalHeaderMenuModel>)HttpContext.Session["headerMenus"];
                        if (headerMenus == null || headerMenus.Count == 0)
                        {
                            ModelState.AddModelError("", @Resources.Resources.Login_noroleAvailable);
                            return View(Sys);
                        }
                        // Ends <<<<

                        Sys.lastLoginURl = getLastLoginURL(Sys.lastLoginURl);    //Rajesh on 08/16/2013

                        if (Sys.lastLoginURl == null)
                        {
                            // Commented the below code and added last line >>>
                            // Rajesh on 08/14/2013
                            //if (OrgType.Equals("Admin"))
                            //{
                            //    return RedirectToAction("AdminDashboard", "AdminVendors");
                            //}
                            //else if (OrgType.Equals("Vendor"))
                            //{
                            //    return RedirectToAction("VendorDashboard", "Vendor");
                            //}
                            //else if (OrgType.Equals("Client"))
                            //{
                            //    return RedirectToAction("ClientDashboard", "Client");
                            //}
                            //else
                            //    return View();
                            return RedirectToAction("DashBoard");
                            // Ends <<<
                        }
                        else
                            return Redirect(Sys.lastLoginURl);


                    }
                }

            }
            else
                return View(Sys);
        }
        // Suma - Log in Http Post Ends <<<        

        // Suma - PASSWORD ENCRYPTION >>>>
        //private string Encryptdata(string password)
        //{
        //    try
        //    {
        //        TripleDESCryptoServiceProvider objDESCrypto = new TripleDESCryptoServiceProvider();
        //        MD5CryptoServiceProvider objHashMD5 = new MD5CryptoServiceProvider();
        //        byte[] byteHash, byteBuff;
        //        string strTempKey = ConfigurationSettings.AppSettings["encryptionkey"].ToString();
        //        byteHash = objHashMD5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(strTempKey));
        //        objHashMD5 = null;
        //        objDESCrypto.Key = byteHash;
        //        objDESCrypto.Mode = CipherMode.ECB; //CBC, CFB
        //        byteBuff = ASCIIEncoding.ASCII.GetBytes(password);
        //        return Convert.ToBase64String(objDESCrypto.CreateEncryptor().
        //            TransformFinalBlock(byteBuff, 0, byteBuff.Length));
        //    }
        //    catch (Exception ex)
        //    {
        //        return "Wrong Input. " + ex.Message;
        //    }
        //}
        // Suma - Password Encryption Ends <<<

        // SUMA On (MM/DD/YYYY)05/07/2013 for MyCompanyDetails >>>
        [SessionExpire]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "MyAccount", sideMenuName = "CompanyDetails", ViewName = "MyCompanyDetails")]
        public ActionResult MyCompanyDetails()
        {
            ViewBag.isTaxIdExists = false;
            ViewBag.taxIdCountError = false;
            ViewBag.VBisSuccess = false;
            Guid guidRecordId = SSession.UserId;

            var varSysUser = _sysUser.GetUserAccount(guidRecordId);
            var varSysUserOrg = _sysUser.GetSystemUserOrganizations(varSysUser.UserId);

            var varOrgId = varSysUserOrg.OrganizationId;
            if (SSession.Role == OrganizationType.Vendor)
            {
                ViewBag.HasFinancialData = _sysUser.HasFinancialData(varOrgId);
            }
            if (varSysUserOrg == null)
                return View();
            else
            {
                OrganizationData organizationData = _org.GetOrganization(varOrgId);

                var localMyCompanyDetails = new LocalMyCompanyDetails();
                localMyCompanyDetails.Name = organizationData.Name;
                localMyCompanyDetails.Address1 = organizationData.Address1;
                localMyCompanyDetails.Address2 = organizationData.Address2;
                localMyCompanyDetails.Address3 = organizationData.Address3;
                localMyCompanyDetails.City = organizationData.City;
                localMyCompanyDetails.State = organizationData.State;
                localMyCompanyDetails.CountryType = organizationData.CountryType;
                localMyCompanyDetails.Zip = organizationData.Zip;
                localMyCompanyDetails.Country = organizationData.Country;
                localMyCompanyDetails.PhoneNumber = organizationData.PhoneNumber;
                localMyCompanyDetails.ExtNumber = organizationData.ExtNumber;
                localMyCompanyDetails.FaxNumber = organizationData.FaxNumber;
                localMyCompanyDetails.WebsiteURL = organizationData.WebsiteURL;
                localMyCompanyDetails.FinancialMonth = organizationData.FinancialMonth;
                localMyCompanyDetails.FinancialDate = organizationData.FinancialDate;

                localMyCompanyDetails.OrganizationID = organizationData.OrganizationID;
                localMyCompanyDetails.TaxID = organizationData.TaxID;

                return View(localMyCompanyDetails);
            }
        }


        public bool ValidateMyCompanyForm(LocalMyCompanyDetails locVenReg_form)
        {
            if (locVenReg_form.CountryType != (int)StateType.USA)
            {
                var taxerror = ModelState["TaxID"].Errors.FirstOrDefault(rec => rec.ErrorMessage == Resources.Resources.Common_InvalidTaxID);
                ModelState["TaxID"].Errors.Remove(taxerror);
            }

            return ModelState.IsValid;
        }
        // Suma - Http Get MyCompanyDetails Ends <<<

        // Suma - Http Post MyCompanyDetails >>>
        [AcceptVerbs(HttpVerbs.Post)]
        [SessionExpire]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "MyAccount", sideMenuName = "CompanyDetails", ViewName = "MyCompanyDetails")]
        public ActionResult MyCompanyDetails(LocalMyCompanyDetails orgForm)
        {
            //var vFinancialAnalyticsModel= _sysUser.IsUserHasFinancialAnalytics(orgForm.OrganizationID); 
            //ViewBag.Vendor = vFinancialAnalyticsModel;

            ViewBag.isTaxIdExists = false;
            ViewBag.taxIdCountError = false;
            ViewBag.VBisSuccess = false;

            if (SSession.Role == OrganizationType.Vendor)
            {
                ViewBag.HasFinancialData = _sysUser.HasFinancialData(orgForm.OrganizationID);
            }
            if (String.IsNullOrEmpty(orgForm.Name))
            {
                ModelState.AddModelError("Name", " ");
            }

            else if (String.IsNullOrEmpty(orgForm.Address1))
            {
                ModelState.AddModelError("Address1", " ");
            }
            if (String.IsNullOrEmpty(orgForm.City))
            {
                ModelState.AddModelError("City", " ");
            }
            if (String.IsNullOrEmpty(orgForm.State))
            {
                ModelState.AddModelError("State", " ");
            }
            if (String.IsNullOrEmpty(orgForm.Zip))
            {
                ModelState.AddModelError("Zip", " ");
            }

            if (String.IsNullOrEmpty(orgForm.Country))
            {
                ModelState.AddModelError("Country", " ");
            }
            if (String.IsNullOrEmpty(orgForm.PhoneNumber))
            {
                ModelState.AddModelError("PhoneNumber", " ");
            }
            Guid guidRecordId = SSession.UserId;

            var varSysUser = _sysUser.GetUserAccount(guidRecordId);

            var varSysUserOrg = _sysUser.GetSystemUserOrganizations(varSysUser.UserId);
            var varOrgId = varSysUserOrg.OrganizationId;

            if (ValidateMyCompanyForm(orgForm))
            {
                var organizationWithTaxId = _sysUser.OrganizationWithTaxId(orgForm.TaxID, orgForm.OrganizationID);

                var taxIdCount = orgForm.TaxID.Replace("-", "").Count();
                if (orgForm.CountryType == (int)StateType.USA && (taxIdCount == 0 || taxIdCount != 9))
                {
                    ViewBag.taxIdCountError = true;
                    return View(orgForm);
                }

                if (organizationWithTaxId != null)
                {
                    ViewBag.isTaxIdExists = true;
                    ViewBag.VBTaxIdOrg = organizationWithTaxId.Name;
                    return View(orgForm);
                }

                var organizationsinUserOrganizations = _sysUser.OrgsInUserOrganization(varSysUser.UserId);

                if (organizationsinUserOrganizations == null)
                {
                    _sysUser.AddOrganizations(orgForm, varSysUserOrg);
                    ViewBag.VBisSuccess = true;
                }
                else
                {
                    var org_updata = _sysUser.GetOrgs(varOrgId);

                    var pathForCSI = HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["WebResponseFiles"]);
                    if (org_updata.OrganizationType == "Vendor")
                    {
                        if (org_updata.Name != orgForm.Name)
                        {
                            var data = Utilities.CSIWebServiceInitiateRequest.CSIWebService(org_updata.OrganizationID, orgForm.Name, null, pathForCSI);
                        }

                        if (org_updata.Name != orgForm.Name || org_updata.Address1 != orgForm.Address1 || org_updata.Address2 != orgForm.Address2)
                        {
                            var adminSuperUserEmail = _sysUser.GetAdminSuperUserEmail();

                            string body = System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Views/Companies/EmailFormatForvendorInformation.htm"));

                            string subject = orgForm.Name + " changed its company information";
                            body = body.Replace("_Vendor", orgForm.Name);
                            var items = "";

                            if (org_updata.Name != orgForm.Name)
                            {
                                items += "<li style='width:100%;float:left;padding:0.5% 0;'>Legal Name of Business</li>";
                            }
                            if (org_updata.Address1 != orgForm.Address1)
                            {
                                items += "<li style='width:100%;float:left;padding:0.5% 0;'>Address1</li>";
                            }
                            if (org_updata.Address2 != orgForm.Address2)
                            {
                                items += "<li style='width:100%;float:left;padding:0.5% 0;'>Address2</li>";
                            }
                            if (org_updata.Address3 != orgForm.Address3)
                            {
                                items += "<li style='width:100%;float:left;padding:0.5% 0;'>Address3</li>";
                            }

                            body = body.Replace("_items", items);

                            Utilities.SendMail.sendMail(adminSuperUserEmail, body, subject);
                            _sysUser.AddArchiveVendorData(orgForm, SSession.UserId, null);

                        }
                    }

                    _sysUser.UpdateOrganizations(orgForm, org_updata);

                    ViewBag.VBisSuccess = true;
                }

                return RedirectToAction("MyCompanyDetails");
            }
            else
            {
                ViewBag.VBisSuccess = false;
                return View(orgForm);
            }

        }
        // Suma - MyCompanyDetails Http Post Ends <<<

        //=========SUMA On (MM/DD/YYYY)13/05/2013 
        // for FORGOT PASSWORD >>>>
        public ActionResult ForgotPassword()
        {
            ViewBag.isSuccess = true;
            return View();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ForgotPassword(LocalForgotPasswordModel userEmail)
        {
            if (ModelState.IsValid)
            {
                string strEmail = userEmail.EmailId;
                var varUserExists = _sysUser.GetSystemUserInfo(strEmail);
                if (varUserExists == null)
                {
                    ModelState.AddModelError("email", "User does not exist");
                    return View("ForgotPassword");
                }
                else
                {
                    var varbody = varUserExists.Password;
                    //Utilities.SendMail.sendMail(email, "ResetPassword", "Your Password is:" + varbody);
                    string url = ConfigurationSettings.AppSettings["ResetPasswordURL"];
                    var strMessage = Utilities.SendMail.sendMail(strEmail, @Resources.Resources.PasswordResetMessage_part1 + "<a href='" + url + varUserExists.UserId + "'>" + url + varUserExists.UserId + "</a>" + @Resources.Resources.passwordReset_part2, "Password reset request");
                    if (strMessage.ToString() == "Success")
                    {
                        return View("PWDResult");
                    }
                    else
                    {
                        ViewBag.isSuccess = false;
                        return View();
                    }
                    return View("PWDResult");
                }
            }
            else
            {
                return View();
            }

        }

        public ActionResult PWDResult()
        {
            return View();
        }



        public ActionResult ResetPassword(Guid userId)
        {
            LocalResetPasswordModel passwordReset = new LocalResetPasswordModel();
            passwordReset.userId = userId;
            passwordReset.email = _sysUser.FindUser(userId).Email;
            return View(passwordReset);
        }
        [HttpPost]
        public ActionResult ResetPassword(LocalResetPasswordModel passwordReset)
        {
            if (ModelState.IsValid)
            {
                var Password = Utilities.EncryptionDecryption.EncryptData(passwordReset.ConfirmPassword);
                _sysUser.ResetPassword(passwordReset.userId, Password);

                //   var currentUser = _sysUser.FindUser(passwordReset.userId);
                // // 07/04/2013 Suma 
                //// Rajesh 07/08/2013 
                //// Change last password change date >>>
                //currentUser.LastPasswordChangedDate = DateTime.Now;
                //// Change Last password change date Ends <<<

                //entityDB.Entry(currentUser).State = EntityState.Modified;
                //entityDB.SaveChanges();
                return Redirect("Login?messageType=2"); // 2 means password changed Successful
            }
            return View(passwordReset);
        }

        // for FORGOT PASSWORD ENDS <<<<<<

        //===================================HELP PAGE==========================
        public ActionResult HelpPage()
        {
            ViewBag.PageType = "Help";
            return View();
        }

        //<< DV:SLJ

        /*
            Created Date:  06/18/2013   Created By: Kiran Talluri
            Purpose: To display the dafault registration form
        */

        [SessionExpire]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "MyAccount", sideMenuName = "SearchUser", ViewName = "AddNewUser")]
        public ActionResult AddNewUser()
        {

            ViewBag.isEmailExists = false;

            ViewBag.VBUserOrgTypes = new[] {
                                        new SelectListItem() {Text= "Select Organization Type", Value = ""},
                                        new SelectListItem() { Text = LocalConstants.Admin, Value="0" },
                                        new SelectListItem() { Text = OrganizationType.SuperClient, Value= "3" },
                                        new SelectListItem() { Text = OrganizationType.Client, Value="1" },
            new SelectListItem() { Text = LocalConstants.Vendor, Value= "2" }};
            if (@Session["RoleName"].ToString().Equals(LocalConstants.Admin))
            {
                ViewBag.VBOrganizationsList = new[] {
                                            new SelectListItem() {Text= "Select Organization", Value=""}};
            }
            else
            {
                var currentUserOrgId = (long)Session["currentOrgId"];

                var orgs = _sysUser.GetOrganizations(currentUserOrgId);
                List<SelectListItem> clientOrg = new List<SelectListItem>();
                orgs.ForEach(s => clientOrg.Add(new SelectListItem() { Text = s.Key, Value = s.Value.ToString() }));

                //var clientOrg = clientOrg.ToList();
                SelectListItem defaultItem = new SelectListItem();
                defaultItem.Text = "Select Organization";
                defaultItem.Value = "";
                clientOrg.Insert(0, defaultItem);
                ViewBag.VBOrganizationsList = clientOrg;
            }
            ViewBag.Roles = new[] {
                                            new SelectListItem() {
                                                Text = "Select Role", Value=""
                                            } };
            var departments = _sysUser.GetDepartments();
            List<SelectListItem> clientDepartments = new List<SelectListItem>();
            departments.ForEach(s => clientDepartments.Add(new SelectListItem() { Text = s.Key, Value = s.Value.ToString() }));

            SelectListItem defaultItem1 = new SelectListItem();
            defaultItem1.Text = "Select Department";
            defaultItem1.Value = "";
            clientDepartments.Insert(0, defaultItem1);
            ViewBag.Departments = clientDepartments;

            return View();
        }

        /*
            Created Date:  06/18/2013   Created By: Kiran Talluri
            Purpose: To register the new user
        */

        [SessionExpire]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "MyAccount", sideMenuName = "SearchUser", ViewName = "AddNewUser")]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddNewUser(LocalAddNewUser form)
        {
            // If valid, save to database
            if (ModelState.IsValid)
            {

                ViewBag.isEmailExists = false;

                var departments = _sysUser.GetDepartments();
                List<SelectListItem> clientDepartments = new List<SelectListItem>();
                departments.ForEach(s => clientDepartments.Add(new SelectListItem() { Text = s.Key, Value = s.Value.ToString() }));

                SelectListItem defaultItem = new SelectListItem();
                defaultItem.Text = "Select Department";
                defaultItem.Value = "";
                clientDepartments.Insert(0, defaultItem);
                ViewBag.Departments = clientDepartments;

                SystemUsers currentUser = _sysUser.GetSystemUserInfo(form.Email);

                if (currentUser != null)
                {
                    ViewBag.isEmailExists = true;
                    GetSelectedOrg(form);
                    return View(form);
                }
                string strPassword = Utilities.EncryptionDecryption.EncryptData(form.Password);
                var local = _sysUser.AddNewUser(form, strPassword);

                return RedirectToAction("UpdateNewUserContactDetails", local);
            }
            else
            {
                GetSelectedOrg(form);
            }
            ViewBag.isEmailExists = false;
            return View(form);
        }

        // Kiran on 7/30/2014
        private void GetSelectedOrg(LocalAddNewUser form)
        {
            ViewBag.VBUserOrgTypes = new[] {
                                        new SelectListItem() {Text= "Select Organization Type", Value = ""},
                                        new SelectListItem() { Text = "Admin", Value="0" },
                                        new SelectListItem() { Text = "Client", Value="1" },
                                        new SelectListItem() { Text = "Vendor", Value= "2" }};
            SelectListItem defaultItem = new SelectListItem();
            defaultItem.Text = "Select Organization";
            defaultItem.Value = "";

            if (!@Session["RoleName"].ToString().Equals("Admin"))
            {
                var currentUserOrgId = (long)Session["currentOrgId"];
                List<SelectListItem> OrganizationsList = new List<SelectListItem>();

                var orgs = _sysUser.GetOrganizations(currentUserOrgId);

                orgs.ForEach(s => OrganizationsList.Add(new SelectListItem() { Text = s.Key, Value = s.Value.ToString(), Selected = (form.OrganizationId == s.Value) }));

                var orgsListWithDefaultItem = OrganizationsList;

                orgsListWithDefaultItem.Insert(0, defaultItem);
                ViewBag.VBOrganizationsList = OrganizationsList;
                return;
            }
            if (form.UserType == 0)
            {
                var organizationsList = from orgs in _sysUser.OrganizationsList(OrganizationType.Admin)
                                        select new SelectListItem
                                        {
                                            Text = orgs.Name,
                                            Value = orgs.OrganizationID.ToString(),
                                            Selected = (form.OrganizationId == orgs.OrganizationID)
                                        };
                var orgsListWithDefaultItem = organizationsList.ToList();

                orgsListWithDefaultItem.Insert(0, defaultItem);
                ViewBag.VBOrganizationsList = orgsListWithDefaultItem;

            }
            else if (form.UserType == 1)
            {
                var organizationsList = from orgs in _sysUser.GetClientsList(OrganizationType.Client)
                                        select new SelectListItem
                                        {
                                            Text = orgs.Name,
                                            Value = orgs.OrganizationID.ToString(),
                                            Selected = (form.OrganizationId == orgs.OrganizationID)
                                        };
                var orgsListWithDefaultItem = organizationsList.ToList();

                orgsListWithDefaultItem.Insert(0, defaultItem);
                ViewBag.VBOrganizationsList = orgsListWithDefaultItem;

            }
            else if (form.UserType == 2)
            {
                var organizationsList = from orgs in _sysUser.OrganizationsList(OrganizationType.Vendor)
                                        select new SelectListItem
                                        {
                                            Text = orgs.Name,
                                            Value = orgs.OrganizationID.ToString(),
                                            Selected = (form.OrganizationId == orgs.OrganizationID)
                                        };
                var orgsListWithDefaultItem = organizationsList.ToList();

                orgsListWithDefaultItem.Insert(0, defaultItem);

                ViewBag.VBOrganizationsList = orgsListWithDefaultItem;
            }
            else
            {
                var organizationsList = from orgs in _sysUser.OrganizationsList(OrganizationType.SuperClient)
                                        select new SelectListItem
                                        {
                                            Text = orgs.Name,
                                            Value = orgs.OrganizationID.ToString(),
                                            Selected = (form.OrganizationId == orgs.OrganizationID)
                                        };
                var orgsListWithDefaultItem = organizationsList.ToList();

                orgsListWithDefaultItem.Insert(0, defaultItem);
                ViewBag.VBOrganizationsList = orgsListWithDefaultItem;
            }
        }
        // Ends<<<

        /*
            Created Date:  05/20/2013   Created By: Kiran Talluri
            Purpose: To display the default company access form with list of organizations & accessed organizations along with roles while updating.
        */

        [HttpGet]
        [SessionExpireForView(pageType = "popupview")]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "MyAccount", sideMenuName = "SearchUser", ViewName = "AddCompanyAccess")]
        public ActionResult AddCompanyAccess(Guid userId, long userOrgID)
        {
            ViewBag.isCompanyExists = false;
            ViewBag.roleNotSelected = false;
            ViewBag.isEdit = false;
            LocalCompaniesList form = new LocalCompaniesList();
            form.oldOrgId = userOrgID;
            form.isEdit = false;
            form.userId = userId;
            form.Roles_CheckBoxStatus = new List<bool>();
            var orgType = _sysUser.OrgsInUserOrganization(userId).OrganizationType;
            form.RolesList = _sysUser.GetRoles(orgType);
            Organizations org = new Organizations();
            if (userOrgID == -1)
            {
                if (Session["RoleName"].ToString().Equals("Admin"))
                {
                    var organizationsList = from orgs in _sysUser.OrganizationsList(OrganizationType.Admin)
                                            select new SelectListItem
                                            {
                                                Text = orgs.Name,
                                                Value = orgs.OrganizationID.ToString()
                                            };

                    ViewBag.VBorganizationsList = organizationsList;
                }
                else if (Session["RoleName"].ToString().Equals("Client"))
                {
                    Guid currentUserId = new Guid(Session["UserId"].ToString());
                    var orgs = _sysUser.GetOrganizationsInSystemUserOrgs(currentUserId);
                    List<SelectListItem> organizationsList = new List<SelectListItem>();
                    orgs.ForEach(s => organizationsList.Add(new SelectListItem() { Text = s.Key, Value = s.Value.ToString() }));

                    ViewBag.VBorganizationsList = organizationsList;
                }

            }

            //To add checkboxes status

            for (int i = 0; i < form.RolesList.Count; i++)
            {
                form.Roles_CheckBoxStatus.Add(false);
            }

            if (userOrgID != -1)
            {
                form.isEdit = true;
                SystemUsersOrganizations userOrg = _sysUser.FindUserOrgs(userOrgID);
                var orgId = userOrg.OrganizationId;
                var orgs = _sysUser.GetOrganizations(orgId);
                List<SelectListItem> organizationsList = new List<SelectListItem>();
                orgs.ForEach(s => organizationsList.Add(new SelectListItem() { Text = s.Key, Value = s.Value.ToString() }));
                ViewBag.VBorganizationsList = organizationsList;

                var sysUserOrgRolesCurrentUser = _sysUser.GetUserOrganizationRoles(userOrgID);

                for (int i = 0; i < form.RolesList.Count; i++)
                {
                    foreach (SystemUsersOrganizationsRoles selectedRoles in sysUserOrgRolesCurrentUser)
                    {
                        if (form.RolesList[i].RoleId.Equals(selectedRoles.SysRoleId))
                            form.Roles_CheckBoxStatus[i] = true;
                    }
                }
                return View(form);
            }

            form.OrganizationId = userOrgID;

            return View(form);
        }

        /*
            Created Date:  05/20/2013   Created By: Kiran Talluri
            Purpose: To add the companies that are accessed by the user
        */
        [AcceptVerbs(HttpVerbs.Post)]
        [SessionExpireForView(pageType = "popupview")]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "MyAccount", sideMenuName = "SearchUser", ViewName = "AddCompanyAccess")]
        public ActionResult AddCompanyAccess(LocalCompaniesList form)
        {
            ViewBag.isCompanyExists = false;
            ViewBag.roleNotSelected = false;
            var OrgType = _sysUser.OrgsInUserOrganization(form.userId).OrganizationType;
            form.RolesList = _sysUser.GetRoles(OrgType);
            var Orgs = _sysUser.GetOrganizations(form.OrganizationId);
            List<SelectListItem> organizationsList = new List<SelectListItem>();
            Orgs.ForEach(s => organizationsList.Add(new SelectListItem() { Text = s.Key, Value = s.Value.ToString() }));

            ViewBag.VBorganizationsList = organizationsList;

            if (form.isEdit == false)
            {
                ViewBag.isEdit = false;
                var sysUserOrgExistsCurrentUser = _sysUser.UserOrganizations(form.OrganizationId, form.userId);

                // ================ To check whether the user selected organization already exists or not ============== //

                if (sysUserOrgExistsCurrentUser != null)
                {
                    ViewBag.isCompanyExists = true;
                    return View(form);
                }

                // ============== To check whether the user has selected atleast one role ============== // 

                var count = 0;
                if (form.RolesList.Count > 0)
                {
                    for (int j = 0; j < form.RolesList.Count; j++)
                    {
                        if (form.Roles_CheckBoxStatus[j] == true)
                        {
                            count++;
                        }
                    }
                }
                if (count == 0)
                {
                    ViewBag.roleNotSelected = true;
                    return View(form);
                }

                // =============== To save all the roles selected selected by user ================== //
                _sysUser.SaveOrganizationRoles(form);

                LocalNewUserContactDetails local = new LocalNewUserContactDetails();
                local.userId = form.userId;
                return RedirectToAction("CloseCompanyAccess", local);
            }
            else
            {
                ViewBag.isEdit = true;

                // ============== To check whether the user has selected atleast one role ============== // 

                var count = 0;

                for (int j = 0; j < form.RolesList.Count; j++)
                {
                    if (form.Roles_CheckBoxStatus[j] == true)
                    {
                        count++;
                    }
                }

                if (count == 0)
                {
                    ViewBag.roleNotSelected = true;
                    return View(form);
                }

                // ============= To remove the old organization roles selected by user ============== //


                _sysUser.RemoveOldOrganizationRoles(form.oldOrgId);
                // ============= To remove the old organization selected by user ============== //
                _sysUser.UpdateSelectedUserCompanyAccess(form);

                LocalNewUserContactDetails local = new LocalNewUserContactDetails();
                local.userId = form.userId;
                return RedirectToAction("CloseCompanyAccess", local);
            }
        }

        /*
            Created Date:  05/20/2013   Created By: Kiran Talluri
            Purpose: To close the companyaccess & to update the accessed company details in form(partial page)
        */
        // ================ To close the pop-up =================//

        [SessionExpire]
        public ActionResult CloseCompanyAccess(LocalNewUserContactDetails local)
        {
            return View(local);
        }

        /*
            Created Date:  05/25/2013   Created By: Kiran Talluri
            Purpose: To display the user details while editing through user search
        */

        [SessionExpire(lastPageUrl = "~/SystemUsers/DashBoard")]
        public ActionResult UpdateNewUserContactDetails_Update(Guid userId)
        {
            ViewBag.userSearchEdit = true;
            LocalNewUserContactDetails local = new LocalNewUserContactDetails();
            local.userId = userId;
            local.UserSearchEdit = true;
            // Date: 7/10/2013 Added code to handle company access & deleting user if logged in as admin while editing through search

            local.currentUser_OrgType = _sysUser.OrgsInUserOrganization(userId).OrganizationType; //entityDB.SystemUsersOrganizations.FirstOrDefault(record => record.UserId == userId).Organizations.OrganizationType;

            // Changed code ends
            return RedirectToAction("UpdateNewUserContactDetails", local);
        }

        /*
            Created Date:  05/25/2013   Created By: Kiran Talluri
            Purpose: To add the new user contact details & to display user details while retrieving
        */

        [SessionExpire(lastPageUrl = "~/SystemUsers/DashBoard")]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "MyAccount", sideMenuName = "SearchUser", ViewName = "UpdateNewUserContactDetails")]
        public ActionResult UpdateNewUserContactDetails(LocalNewUserContactDetails local)
        {
            Guid loggedInUserUserId = SSession.UserId;

            ViewBag.VBCanLoginAsAnyUser = false;
            ViewBag.HasUsers = false;
            var usersCount = _sysUser.CheckOrgUsersCount(local.userId);
            if (usersCount > 0) ViewBag.HasUsers = true;
            bool canLoginAsAnyUser = _sysUser.CheckUserCanLoginAsAnyUser(loggedInUserUserId);
            if (canLoginAsAnyUser == true)
                ViewBag.VBCanLoginAsAnyUser = true;
            // Moved to top so as to reduce the code redundancy on 9/24/2013 for hiding delete button & roles link if the user is super user
            ViewBag.isEmailExists = false;
            var userSysUserOrg = _sysUser.GetSystemUsersOrgs(local.userId);
            local.currentUser_OrgType = _sysUser.OrgsInUserOrganization(local.userId).OrganizationType;
            var superUserRoleId = _sysUser.SuperUserRoleId(local.currentUser_OrgType);
            var currUserSysUserOrgId = userSysUserOrg.SysUserOrganizationId;
            var currUserIsSuperUser = _sysUser.isSuperUser(currUserSysUserOrgId, superUserRoleId);

            SelectListItem defaultItem = new SelectListItem();
            defaultItem.Text = "Select Department";
            defaultItem.Value = "-1";
            var departments = _sysUser.GetClientDepartments(userSysUserOrg.OrganizationId)
                .Select(r => new SelectListItem
                {
                    Text = r.Key,
                    Value = r.Value.ToString(),

                }).ToList();
            departments.Insert(0, defaultItem);
            ViewBag.Departments = departments;
            var userDepartment = _sysUser.GetUserDepartment(local.userId);

            var loggedInUserIsSuperUser = _sysUser.GetSystemUsersOrgs(loggedInUserUserId).PrimaryUser;
            ViewBag.VBisLoggedInUserIsSuperUser = loggedInUserIsSuperUser;
            ViewBag.LoggedInUserId = loggedInUserUserId;

            local.ClientBusinessUnits = _clientBusiness.GetClientBusinessUnits(userSysUserOrg.OrganizationId);

            getUserBUSites(local);
            local.UserOrganizationId = userSysUserOrg.OrganizationId;

            if (local.Email == null)
            {
                ModelState.Clear();

                var loggedInUserOrgId = SSession.OrganizationId;
                SystemUsers sysUserCurrentUser = _sysUser.FindUser(local.userId);
                local.OrganizationsList = _sysUser.userOrganizationsList(local.userId);

                // Date: 7/10/2013 Added code to display user status,lastlogindate & lastpasswordchangeddate fro db
                // Code to display contact details while editing
                var userContacts = _sysUser.UserContacts(local.userId);
                if (userContacts.Count != 0)
                {
                    Contact userContact = userContacts[0];
                    local.FirstName = userContact.FirstName;
                    local.LastName = userContact.LastName;
                    local.ContactTitle = userContact.ContactTitle;
                    local.PhoneNumber = userContact.PhoneNumber;
                    local.ExtNumber = userContact.ExtNumber;
                    local.AdditionalDescription = userContact.AdditionalDescription;
                    local.PhoneNumberInternational = userContact.PhoneNumberInternational;
                }

                // Code to display the common data during insertion as well as updation

                local.UserStatus = sysUserCurrentUser.UserStatus;
                local.Email = sysUserCurrentUser.Email;
                local.userId = sysUserCurrentUser.UserId;
                local.LastLoginDate = sysUserCurrentUser.LastLoginDate;
                local.LastPasswordChangedDate = sysUserCurrentUser.LastPasswordChangedDate;
                var userDepartmentId = -1;
                if (userDepartment != null)
                {
                    userDepartmentId = userDepartment.DepartmentId;

                }

                local.DepartmentId = userDepartmentId;
                // Code to display the password in decrypted format while adding contact details for new user
                // Changed below condition on 08/19/2013 for displaying password in text format
                if (local.UserSearchEdit == false)
                {
                    ViewBag.userSearchEdit = false;
                    string encryptedPassword = sysUserCurrentUser.Password;
                    string decryptedPassword = Utilities.EncryptionDecryption.Decryptdata(encryptedPassword);
                    local.Password = decryptedPassword;
                    local.ConfirmPassword = decryptedPassword;
                    // Added on 9/24/2013 for hiding delete button & roles if the user is super user

                    local.isSuperUser = currUserIsSuperUser;

                    // Ends<<<<
                }
                // Changed code ends
                else
                {
                    ViewBag.userSearchEdit = true;
                    string encryptedPassword = sysUserCurrentUser.Password;
                    string decryptedPassword = Utilities.EncryptionDecryption.Decryptdata(encryptedPassword);
                    local.Password = decryptedPassword;
                    local.ConfirmPassword = decryptedPassword;

                    local.isSuperUser = currUserIsSuperUser;
                }

                return View(local);
            }
            else
            {
                ViewBag.userSearchEdit = false;
                if (local.currentUser_OrgType.ToLower().Equals("vendor") || local.currentUser_OrgType.ToLower().Equals("admin"))
                {
                    ModelState.Remove("ClientBU");
                    ModelState.Remove("ClientBUSite");
                }
                if (ModelState.IsValid)
                {
                    ViewBag.userSearchEdit = false;

                    SystemUsers emailExists = _sysUser.emailExists(local.Email, local.userId);

                    if (emailExists != null)
                    {
                        ViewBag.isEmailExists = true;
                        return View(local);
                    }

                    _sysUser.UpdateNewUserContactDetails(local);

                    local.isSuperUser = currUserIsSuperUser;

                    // Ends<<<
                    local.ForSuccessMessage = true;
                    getUserBUSites(local);
                    return View(local);
                }
                return View(local);
            }
        }

        private void getUserBUSites(LocalNewUserContactDetails local)
        {
            var userBUSites = _sysUser.UserBuSites(local.userId);// entityDB.SystemUsersBUSites.FirstOrDefault(row => row.SystemUserId == local.userId);
            ViewBag.userBUSites = userBUSites;
            if (userBUSites != null)
            {
                if (!string.IsNullOrEmpty(userBUSites.BUId))
                {
                    local.ClientBUSites = _clientBusiness.GetClientBUSitesByBU(userBUSites.BUId) ?? new List<KeyValuePair<string, string>>();
                }
            }
        }

        public ActionResult getClientBUSitesbyBU(long userOrgId, string buIds)
        {
            var res = _clientBusiness.GetClientBUSitesByBU(buIds);
            return Json(res);
        }

        /*
            Created Date:  06/14/2013   Created By: Kiran Talluri
            Purpose: To decrypt the password so as to display in form while retrieving
        */
        //private string Decryptdata(string encryptpwd)
        //{
        //    try
        //    {
        //        TripleDESCryptoServiceProvider objDESCrypto =
        //            new TripleDESCryptoServiceProvider();
        //        MD5CryptoServiceProvider objHashMD5 = new MD5CryptoServiceProvider();
        //        byte[] byteHash, byteBuff;
        //        string strTempKey = ConfigurationSettings.AppSettings["encryptionkey"].ToString();
        //        byteHash = objHashMD5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(strTempKey));
        //        objHashMD5 = null;
        //        objDESCrypto.Key = byteHash;
        //        objDESCrypto.Mode = CipherMode.ECB; //CBC, CFB
        //        byteBuff = Convert.FromBase64String(encryptpwd);
        //        string strDecrypted = ASCIIEncoding.ASCII.GetString
        //        (objDESCrypto.CreateDecryptor().TransformFinalBlock
        //        (byteBuff, 0, byteBuff.Length));
        //        objDESCrypto = null;
        //        return strDecrypted;
        //    }
        //    catch (Exception ex)
        //    {
        //        return "Wrong Input. " + ex.Message;
        //    }
        //}

        /*
            Created Date:  05/20/2013   Created By: Kiran Talluri
            Purpose: To delete the user that is already created
        */

        [SessionExpire(lastPageUrl = "~/SystemUsers/DashBoard")]
        public string DeleteUser(Guid UserId)
        {
            var user = _sysUser.DeleteUser(UserId);
            //LocalNewUserContactDetails form = new LocalNewUserContactDetails();
            //LocalCompaniesList local = new LocalCompaniesList();

            //form.userId = UserId;

            //// Changed code on 2/7/2014 by kiran
            //var sysUserOrgs_currentUser = entityDB.SystemUsersOrganizations.FirstOrDefault(m => m.UserId == form.userId);

            //SystemUsers sysUser_currentUser = entityDB.SystemUsers.Find(UserId);

            //var sysUserOrgRoles_currentUser = entityDB.SystemUsersOrganizationsRoles.Where(roles => roles.SysUserOrgId == sysUserOrgs_currentUser.SysUserOrganizationId).ToList();

            //foreach (var sysUserOrgRole in sysUserOrgRoles_currentUser)
            //{
            //    entityDB.SystemUsersOrganizationsRoles.Remove(sysUserOrgRole);
            //}

            //entityDB.SystemUsersOrganizations.Remove(sysUserOrgs_currentUser);

            //// Ends<<<

            //if (sysUser_currentUser.Contacts.Count != 0)
            //{
            //    entityDB.Contact.Remove(sysUser_currentUser.Contacts[0]);
            //}
            //entityDB.SystemUsers.Remove(sysUser_currentUser);

            //var currentUser_logs = entityDB.SystemUserLogs.Where(record => record.SystemUserId == UserId);

            //foreach (var currentUserLog in currentUser_logs)
            //{
            //    entityDB.SystemUserLogs.Remove(currentUserLog);
            //}

            //var currentUser_SiteRepresentatives = entityDB.ClientBusinessUnitSiteRepresentatives.Where(record => record.SafetyRepresentative == UserId);

            //if (currentUser_SiteRepresentatives != null)
            //{
            //    foreach (var siteRepresentative in currentUser_SiteRepresentatives)
            //    {
            //        {
            //            entityDB.ClientBusinessUnitSiteRepresentatives.Remove(siteRepresentative);
            //        }
            //    }
            //}
            //try
            //{
            //    entityDB.SaveChanges();
            //}
            //catch
            //{
            //    entityDB = new EFDbContext();
            //    sysUser_currentUser = entityDB.SystemUsers.Find(UserId);
            //    sysUser_currentUser.UserStatus = false;
            //    entityDB.Entry(sysUser_currentUser).State = EntityState.Modified;

            //    entityDB.SaveChanges();
            //    return "Error";
            //}
            if (user == "Error")
                return user;

            if (Session["RoleName"].ToString().Equals("Admin"))
                return "../SystemUsers/SearchUser";
            else
                return "../Client/ClientOrVendorUsersList";
        }

        /*
            Created Date:  05/22/2013   Created By: Kiran Talluri
            Purpose: To display the default usersearch form
        */

        [SessionExpire]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "MyAccount", sideMenuName = "SearchUser", ViewName = "SearchUser")]
        public ActionResult SearchUser()
        {
            SearchUsersList();
            return View();
        }
        public string ChangeEmail(Guid UserId, String Email)
        {
            return _sysUser.UpdateUserEmail(UserId, Email);
        }
        /*
            Created Date:  05/22/2013   Created By: Kiran Talluri
            Purpose: To display organizations in add user form
        */
        [SessionExpireForView(pageType = "partial")]
        public ActionResult getOrganizationsList(int UserType)
        {
            SelectListItem defaultItem = new SelectListItem();
            defaultItem.Text = "Select Organization";
            defaultItem.Value = "";

            if (Session["RoleName"].ToString() == "Admin")
            {
                if (UserType == 0)
                {
                    var organizationsList = from orgs in _sysUser.OrganizationsList(OrganizationType.Admin)
                                            select new SelectListItem
                                            {
                                                Text = orgs.Name,
                                                Value = orgs.OrganizationID.ToString()

                                            };
                    var orgsListWithDefaultItem = organizationsList.ToList();

                    orgsListWithDefaultItem.Insert(0, defaultItem);

                    return Json(orgsListWithDefaultItem);
                }
                else if (UserType == 1)
                {
                    var organizationsList = from orgs in _sysUser.GetClientsList(OrganizationType.Client)//entityDB.Organizations.Where(orgs => orgs.OrganizationType == "Client" && orgs.ShowInApplication == true).OrderBy(rec => rec.Name).ToList()
                                            select new SelectListItem
                                            {
                                                Text = orgs.Name,
                                                Value = orgs.OrganizationID.ToString()
                                            };
                    var orgsListWithDefaultItem = organizationsList.ToList();

                    orgsListWithDefaultItem.Insert(0, defaultItem);

                    return Json(orgsListWithDefaultItem);
                }
                else if (UserType == 2)
                {
                    var organizationsList = from orgs in _sysUser.OrganizationsList(OrganizationType.Vendor)//entityDB.Organizations.Where(orgs => orgs.OrganizationType == "Vendor").OrderBy(rec => rec.Name).ToList()
                                            select new SelectListItem
                                            {
                                                Text = orgs.Name,
                                                Value = orgs.OrganizationID.ToString()
                                            };
                    var orgsListWithDefaultItem = organizationsList.ToList();

                    orgsListWithDefaultItem.Insert(0, defaultItem);

                    return Json(orgsListWithDefaultItem);
                }
                else
                {
                    var organizationsList = from orgs in _sysUser.OrganizationsList(OrganizationType.SuperClient)//entityDB.Organizations.Where(orgs => orgs.OrganizationType.Equals(OrganizationType.SuperClient)).OrderBy(rec => rec.Name).ToList()
                                            select new SelectListItem
                                            {
                                                Text = orgs.Name,
                                                Value = orgs.OrganizationID.ToString()
                                            };
                    var orgsListWithDefaultItem = organizationsList.ToList();

                    orgsListWithDefaultItem.Insert(0, defaultItem);
                    return Json(orgsListWithDefaultItem);
                }
            }
            else
            {
                string userId = Session["UserId"].ToString();
                Guid currentUserUserId = new Guid(userId);

                var orgs = _sysUser.GetOrganizationsInSystemUserOrgs(currentUserUserId);
                List<SelectListItem> organizationsList = new List<SelectListItem>();
                orgs.ForEach(s => organizationsList.Add(new SelectListItem() { Text = s.Key, Value = s.Value.ToString() }));
                var orgsListWithDefaultItem = organizationsList;

                orgsListWithDefaultItem.Insert(0, defaultItem);

                return Json(organizationsList);
            }
            // Ends<<<
        }
        [SessionExpireForView(pageType = "partial")]
        public ActionResult getOrganizationRoles(int UserType)
        {
            SelectListItem defaultItem = new SelectListItem();
            defaultItem.Text = "Select Role";
            defaultItem.Value = "";
            ViewBag.Roles = new[] {
                                            new SelectListItem() {
                                                Text = "Select Role", Value=""
                                            } };
            var usertype = "";
            if (UserType == 1) usertype = LocalConstants.Client;
            else if (UserType == 2) usertype = LocalConstants.Vendor;
            else { usertype = LocalConstants.Admin; }
            var Roles = _sysUser.GetRoles(usertype);
            var OrganizationRoles = Roles.Select(r => new SelectListItem
            {
                Text = r.RoleName,
                Value = r.RoleId
            }).ToList();
            OrganizationRoles.Insert(0, defaultItem);

            return Json(OrganizationRoles);
        }
        [HttpPost]
        [SessionExpireForView(pageType = "partial")]
        [OutputCache(Duration = 0)]
        public ActionResult GetSysUserOrganizations(DataSourceRequest request, bool searchType, long SuperClientId, string fname, string lname, string email, bool userStatus_Active, bool userStatus_InActive, long clientId, long vendorId, long adminId)
        {
            SearchUsersList();
            var finalUsersList = _sysUser.UsersList(searchType, SuperClientId, fname, lname, email, userStatus_Active, userStatus_InActive, clientId, vendorId, adminId, request);

            return Json(finalUsersList, JsonRequestBehavior.AllowGet);
        }


        //==============On 2013-5-22 to Search the users========================== //
        /*
            Created Date:  05/22/2013   Created By: Kiran Talluri
            Purpose: To display the list of clients & vendors in advanced search form
        //*/
        // Search USer KT --->
        private void SearchUsersList()
        {
            var clientsList = from org in _sysUser.OrganizationsList(OrganizationType.Client)
                              select new SelectListItem
                              {
                                  Text = org.Name,
                                  Value = org.OrganizationID.ToString()
                              };

            SelectListItem defaultItem = new SelectListItem();
            defaultItem.Text = @Resources.Resources.Common_DropdownValue_Any;
            defaultItem.Value = "-1";
            var clientsList_defaultItem = clientsList.ToList();
            clientsList_defaultItem.Insert(0, defaultItem);
            ViewBag.VBclientsList = clientsList_defaultItem;
            //-------------------------
            var SuperclientsList = from org in _sysUser.OrganizationsList(OrganizationType.SuperClient)
                                   select new SelectListItem
                                   {
                                       Text = org.Name,
                                       Value = org.OrganizationID.ToString()
                                   };

            SelectListItem DefaultItem = new SelectListItem();
            DefaultItem.Text = @Resources.Resources.Common_DropdownValue_Any;
            DefaultItem.Value = "0";
            var SuperclientsList_defaultItem = SuperclientsList.ToList();
            SuperclientsList_defaultItem.Insert(0, DefaultItem);
            ViewBag.VBSuperclientsList = SuperclientsList_defaultItem;
            var vendorsList = from org in _sysUser.OrganizationsList(OrganizationType.Vendor)

                              select new SelectListItem
                              {
                                  Text = org.Name,
                                  Value = org.OrganizationID.ToString()
                              };
            defaultItem = new SelectListItem();
            defaultItem.Text = @Resources.Resources.Common_DropdownValue_Any;
            defaultItem.Value = "-1";
            var VBvendorsList_defaultItem = vendorsList.ToList();
            VBvendorsList_defaultItem.Insert(0, defaultItem);
            ViewBag.VBvendorsList = VBvendorsList_defaultItem;
            //-------------------------
            var adminList =
            (from org in _sysUser.OrganizationsList(OrganizationType.Admin)
             select new SelectListItem
             {
                 Text = org.Name,
                 Value = org.OrganizationID.ToString()
             }).ToList();
            defaultItem = new SelectListItem
            {
                Text = @Resources.Resources.Common_DropdownValue_Any,
                Value = "-1"
            };
            adminList.Insert(0, defaultItem);
            ViewBag.VBadminList = adminList;
        }
        // Search User KT <----

        /*
            Created Date:  05/20/2013   Created By: Kiran Talluri
            Purpose: To display the companies accessed by the user
        */
        [SessionExpireForView(pageType = "partial")]
        [OutputCache(Duration = 0)]
        public ActionResult PartialCompanyAccessResultsList(Guid registeredUserId, bool isSuperUser)
        {
            Guid userid = registeredUserId;
            // var sysUserOrgs = entityDB.SystemUsersOrganizations.Where(m => m.UserId == userid);
            var sysUserOrgs = _sysUser.GetUserRoles(userid);
            // Kiran on 2/13/2014
            var userCompanyPrincipalOfficerName = _sysUser.GetUserOfficeName(userid);//entityDB.SystemUsersOrganizations.FirstOrDefault(record => record.UserId == registeredUserId).Organizations.PrincipalCompanyOfficerName;
            ViewBag.VBCompanyOfficerName = userCompanyPrincipalOfficerName;
            // Ends<<<
            //ViewBag.VBUSerOrgRoles=_sysUser.GetUserOrganizationRoles()
            ViewBag.VBsystemUsersOrgList = sysUserOrgs;
            ViewBag.isSuperUser = isSuperUser;
            return PartialView();
        }


        /*
            Created Date:  05/30/2013   Created By: Kiran Talluri
            Purpose: To display the registration form
        */
        [HttpPost]
        public string getUserSessionValue()
        {
            if (Session["UserId"] == null)
            {
                return @Resources.Resources.JQuerySessionExpired;
            }
            else
            {
                return @Resources.Resources.JQuerySessionExist;
            }
            //return Session["UserId"].ToString();
        }
        //<< DV:KT

        //DV:RP   DT:7/3/2013
        //Description :To Goto Dashboard
        [SessionExpire]
        public ActionResult DashBoard()
        {
            var headerMenus = (List<LocalHeaderMenuModel>)HttpContext.Session["headerMenus"];

            if (headerMenus.FirstOrDefault(rec => rec.menuName == "DashBoard") != null)
            {
                String OrgType = Session["RoleName"].ToString();
                if (OrgType == "Admin")
                {
                    return RedirectToAction("AdminDashboard", "AdminVendors");
                }
                else if (OrgType == "Vendor")
                {
                    return RedirectToAction("VendorDashboard", "Vendor");
                }
                else if (OrgType == "Client" || OrgType == OrganizationType.SuperClient)
                {
                    return RedirectToAction("ClientDashboard", "Client");
                }
                else
                    return View();
            }
            else
            {
                var menu = headerMenus.FirstOrDefault(rec => rec.actionName != "" && rec.controllerName != "");
                return RedirectToAction(menu.actionName, menu.controllerName);
            }
        }
        // Goto Dashboard Ends <<<<<

        //DV:RP   DT:7/3/2013
        //Description :To setup Menus dynamically based on role after login 
        public void setUpMenus(string role)
        {

            //After Login Get SystemRolesPermissions based on login user
            //listofSysRolePermissions= SystemUsersOrganizations(where userid=currentUserId and orgId=organizationId)->SystemUsersOrganizationsRoles->SystemRoles->SystemRolesPermissions

            //foreach listofSysRolePermissions
            //{
            //    get all sidemenus and header menus(Get all permissions)
            //}

            //group all header menus and sidemenus(Get all permission)

            var userId = new Guid(Session["UserId"].ToString());//SSession.UserId; 
            var currentOrgId = Convert.ToInt64(Session["currentOrgId"].ToString());
            //// Kiran 11/27/2013 >>>
            ////var loginVendorId = Convert.ToInt64(Session["currentOrgId"]);
            ////Session["LoginOrganizationName"] = entityDB.Organizations.FirstOrDefault(rec => rec.OrganizationID == loginVendorId).Name;
            //// Ends <<<

            //var systemUsersOrgRolesList = _sysUser.GetUserOrgRoles(userId,currentOrgId); //entityDB.SystemUsersOrganizations.FirstOrDefault(rec => rec.UserId == userId && rec.OrganizationId == currentOrgId).SystemUsersOrganizationsRoles;


            //List<LocalHeaderMenuModel> headerMenus = new List<LocalHeaderMenuModel>();
            //var presentHeaderMenu = new LocalHeaderMenuModel();
            //var presentSideMenu = new LocalSideMenuModel();
            //foreach (var sysUserOrgRole in systemUsersOrgRolesList)
            //{
            //    var sysRolesPermissions = sysUserOrgRole.SystemRoles.SystemRolesPermissions;
            //    foreach (var rolePermission in sysRolesPermissions)//In here we have all views related to login user
            //    {
            //        if (rolePermission.SysViewsId >= 1000) { continue; }

            //        presentHeaderMenu = headerMenus.FirstOrDefault(rec => rec.menuName == rolePermission.SystemViews.SystemSideMenus.SystemHeaderMenus.SysMenuName);
            //        if (presentHeaderMenu == null)
            //        {
            //            presentHeaderMenu = new LocalHeaderMenuModel(rolePermission.SystemViews.SystemSideMenus.SystemHeaderMenus);
            //            headerMenus.Add(presentHeaderMenu);
            //        }
            //        string permission = "";
            //        if (rolePermission.ReadPermission != null && rolePermission.ReadPermission == true)
            //            permission += "R";
            //        if (rolePermission.InsertPermission != null && rolePermission.InsertPermission == true)
            //            permission += "I";
            //        if (rolePermission.ModifyPermission != null && rolePermission.ModifyPermission == true)
            //            permission += "M";
            //        if (rolePermission.DeletePermission != null && rolePermission.DeletePermission == true)
            //            permission += "D";

            //        if (permission.Equals(""))//If User has no permissions on this view
            //        {
            //            continue;
            //        }
            //        if (presentHeaderMenu.sideMenus == null)
            //        {
            //            presentHeaderMenu.sideMenus = new List<LocalSideMenuModel>();
            //            presentSideMenu = new LocalSideMenuModel(rolePermission.SystemViews.SystemSideMenus);
            //            presentSideMenu.ViewsAndPermissions = new List<ViewAndPermissions>();
            //            ViewAndPermissions viewPermission = new ViewAndPermissions();
            //            viewPermission.ViewPermission = permission;
            //            viewPermission.ViewName = rolePermission.SystemViews.SystemViewCSHTML;
            //            viewPermission.ControllerName = rolePermission.SystemViews.SystemController;   // Rajesh on 08/19/2013
            //            presentSideMenu.ViewsAndPermissions.Add(viewPermission);
            //            presentHeaderMenu.sideMenus.Add(presentSideMenu);


            //            if (presentSideMenu.controllerName != presentHeaderMenu.controllerName || presentHeaderMenu.actionName != presentSideMenu.actionName)
            //            {
            //                presentHeaderMenu.actionName = presentSideMenu.actionName;
            //                presentHeaderMenu.controllerName = presentSideMenu.controllerName;
            //            }
            //        }
            //        else if (presentHeaderMenu.sideMenus.FirstOrDefault(rec => rec.menuName == rolePermission.SystemViews.SystemSideMenus.SysMenuNameSM) == null)
            //        {
            //            presentSideMenu = new LocalSideMenuModel(rolePermission.SystemViews.SystemSideMenus);
            //            presentSideMenu.ViewsAndPermissions = new List<ViewAndPermissions>();
            //            ViewAndPermissions viewPermission = new ViewAndPermissions();
            //            viewPermission.ViewPermission = permission;
            //            viewPermission.ViewName = rolePermission.SystemViews.SystemViewCSHTML;
            //            viewPermission.ControllerName = rolePermission.SystemViews.SystemController;   // Rajesh on 08/19/2013
            //            presentSideMenu.ViewsAndPermissions.Add(viewPermission);
            //            presentHeaderMenu.sideMenus.Add(presentSideMenu);
            //        }
            //        else
            //        {
            //            presentSideMenu = presentHeaderMenu.sideMenus.FirstOrDefault(rec => rec.menuName == rolePermission.SystemViews.SystemSideMenus.SysMenuNameSM);// Rajesh On 8/12/2013
            //            ViewAndPermissions viewPermission = new ViewAndPermissions();
            //            viewPermission.ViewPermission = permission;
            //            viewPermission.ViewName = rolePermission.SystemViews.SystemViewCSHTML;
            //            viewPermission.ControllerName = rolePermission.SystemViews.SystemController;   // Rajesh on 08/19/2013
            //            presentSideMenu.ViewsAndPermissions.Add(viewPermission);
            //        }

            //    }

            //}


            ////Rajesh on 8/19/2013
            //foreach (var header in headerMenus.ToList())
            //{
            //    foreach (var sideMenu in header.sideMenus.ToList().OrderBy(rec => rec.DisplayOrder))
            //    {
            //        if (sideMenu.ViewsAndPermissions.FirstOrDefault(rec => rec.ViewName == sideMenu.actionName && rec.ControllerName == sideMenu.controllerName) == null)
            //        {
            //            var flag = false;
            //            if (header.actionName == sideMenu.actionName && header.controllerName == sideMenu.controllerName && header.hasSideMenu)
            //                flag = true;
            //            headerMenus.FirstOrDefault(rec => rec.headerMenuId == header.headerMenuId).sideMenus.Remove(sideMenu);
            //            try
            //            {
            //                if (flag)//IF this is 1st side menu then assign 2nd side menu action as a header action
            //                {
            //                    headerMenus.FirstOrDefault(rec => rec.headerMenuId == header.headerMenuId).actionName = headerMenus.FirstOrDefault(rec => rec.headerMenuId == header.headerMenuId).sideMenus.OrderBy(rec => rec.DisplayOrder).FirstOrDefault().actionName;
            //                    headerMenus.FirstOrDefault(rec => rec.headerMenuId == header.headerMenuId).controllerName = headerMenus.FirstOrDefault(rec => rec.headerMenuId == header.headerMenuId).sideMenus.OrderBy(rec => rec.DisplayOrder).FirstOrDefault().controllerName;
            //                }
            //            }
            //            catch (Exception ee)//Means no side menus in this menu
            //            {
            //                headerMenus.Remove(headerMenus.FirstOrDefault(rec => rec.headerMenuId == header.headerMenuId));
            //            }
            //        }
            //    }
            //}
            // Ends <<<




            // Keeping it for future use of code >>>>>
            //try
            //{
            //    if (role.Equals("Admin"))
            //    {

            //        //Creating dashboard
            //        LocalHeaderMenuModel dashBoard = new LocalHeaderMenuModel("DashBoard", "DashBoard", false, "AdminDashboard", "AdminVendors", false);
            //        headerMenus.Add(dashBoard);

            //        //Creating Vendors
            //        LocalHeaderMenuModel Vendors = new LocalHeaderMenuModel("Vendors", "Vendors", true, "VendorSearch", "AdminVendors", false);
            //        Vendors.sideMenus = new List<LocalSideMenuModel>();
            //        {
            //            LocalSideMenuModel sideMenu_venSearch = new LocalSideMenuModel("Vendor Search", "VendorSearch", "VendorSearch", "AdminVendors", false);
            //            Vendors.sideMenus.Add(sideMenu_venSearch);
            //            LocalSideMenuModel sideMenu_inviteVen = new LocalSideMenuModel("Invite Vendor", "InviteVendor", "InviteVendor", "AdminVendors", true);
            //            Vendors.sideMenus.Add(sideMenu_inviteVen);
            //        }
            //        headerMenus.Add(Vendors);

            //        //Creating Reports
            //        LocalHeaderMenuModel Reports = new LocalHeaderMenuModel("Reports", "Reports", false, "", "", false);
            //        headerMenus.Add(Reports);

            //        //Creating PREQUALIFICATION REQUIREMENTS
            //        LocalHeaderMenuModel preQualifiyReq = new LocalHeaderMenuModel("PREQUALIFICATION REQUIREMENTS", "PREQUALIFICATIONREQUIREMENTS", false, "", "", false);
            //        headerMenus.Add(preQualifiyReq);

            //        //Creating  CONTACT FIRST, VERIFY
            //        LocalHeaderMenuModel support = new LocalHeaderMenuModel("CONTACT FIRST, VERIFY", "CONTACTFIRSTVERIFY", false, "", "", false);
            //        headerMenus.Add(support);

            //        //Creating MY ACCOUNT

            //        LocalHeaderMenuModel myAccount = new LocalHeaderMenuModel("My Account", "MyAccount", true, "MyAccount", "SystemUsers", false);

            //        myAccount.sideMenus = new List<LocalSideMenuModel>()
            //         {
            //             new LocalSideMenuModel("My Account","MyAccount",  "MyAccount", "SystemUsers", false),
            //             new LocalSideMenuModel("Change Password","ChangePassword",  "ChangePassword", "SystemUsers", false),
            //             new LocalSideMenuModel("Company Details","CompanyDetails",  "MyCompanyDetails", "SystemUsers", false),
            //             new LocalSideMenuModel("Search User","SearchUser",  "SearchUser", "SystemUsers", false), 
            //             new LocalSideMenuModel("Companies","Companies",  "SearchCompany", "Companies", false), 
            //             new LocalSideMenuModel("Sites","Sitetypes",  "ManageSites", "SitesManagement", false), 
            //             new LocalSideMenuModel("Email Templates","EmailTemplates" , "EmailTemplatesList", "EmailTemplates", false),
            //             new LocalSideMenuModel("Roles And Permissions","RolesAndPermissions" , "AddSystemRole", "RolesAndPermissions", false)  // Siva on 07/31/2013
            //         };


            //        headerMenus.Add(myAccount);
            //    }
            //    else if (role.Equals("Client"))
            //    {

            //        //Creating dashboard
            //        LocalHeaderMenuModel dashBoard = new LocalHeaderMenuModel("DashBoard", "DashBoard", false, "ClientDashboard", "Client", false);
            //        headerMenus.Add(dashBoard);

            //        //Creating Vendors
            //        LocalHeaderMenuModel Vendors = new LocalHeaderMenuModel("Vendors", "Vendors", true, "InviteVendor", "AdminVendors", false);
            //        Vendors.sideMenus = new List<LocalSideMenuModel>();
            //        {
            //            LocalSideMenuModel sideMenu_venSearch = new LocalSideMenuModel("Vendor Search", "VendorSearch", "VendorSearch", "AdminVendors", false);
            //            Vendors.sideMenus.Add(sideMenu_venSearch);
            //            LocalSideMenuModel sideMenu_inviteVen = new LocalSideMenuModel("Invite Vendor", "InviteVendor", "InviteVendor", "AdminVendors", true);
            //            Vendors.sideMenus.Add(sideMenu_inviteVen);
            //        }
            //        headerMenus.Add(Vendors);

            //        //Creating Reports
            //        LocalHeaderMenuModel Reports = new LocalHeaderMenuModel("Reports", "Reports", false, "", "", false);
            //        headerMenus.Add(Reports);

            //        //Creating PREQUALIFICATION REQUIREMENTS
            //        LocalHeaderMenuModel preQualifiyReq = new LocalHeaderMenuModel("PREQUALIFICATION REQUIREMENTS", "PREQUALIFICATIONREQUIREMENTS", false, "", "", false);
            //        headerMenus.Add(preQualifiyReq);

            //        //Creating  CONTACT FIRST, VERIFY
            //        LocalHeaderMenuModel support = new LocalHeaderMenuModel("CONTACT FIRST, VERIFY", "CONTACTFIRSTVERIFY", false, "", "", false);
            //        headerMenus.Add(support);

            //        //Creating MY ACCOUNT

            //        LocalHeaderMenuModel myAccount = new LocalHeaderMenuModel("My Account", "MyAccount", true, "MyAccount", "SystemUsers", false);

            //        myAccount.sideMenus = new List<LocalSideMenuModel>()
            //         {
            //             new LocalSideMenuModel("My Account","MyAccount",  "MyAccount", "SystemUsers", false),
            //             new LocalSideMenuModel("Change Password","ChangePassword",  "ChangePassword", "SystemUsers", false),            
            //             new LocalSideMenuModel("Users List","SearchUser",  "ClientUsersList", "Client", false),
            //             new LocalSideMenuModel("Setup Notifications","Notifications",  "EmailSetupNotificationsList", "EmailSetupNotifications", false)
            //         };


            //        headerMenus.Add(myAccount);
            //    }
            //    else if (role.Equals("Vendor"))
            //    {
            //        //Creating dashboard
            //        LocalHeaderMenuModel dashBoard = new LocalHeaderMenuModel("DashBoard", "DashBoard", false, "VendorDashboard", "Vendor", false);
            //        headerMenus.Add(dashBoard);



            //        //Creating PREQUALIFICATION REQUIREMENTS
            //        LocalHeaderMenuModel preQualifiyReq = new LocalHeaderMenuModel("PREQUALIFICATION REQUIREMENTS", "PREQUALIFICATIONREQUIREMENTS", false, "", "", false);
            //        headerMenus.Add(preQualifiyReq);

            //        //Creating  CONTACT FIRST, VERIFY
            //        LocalHeaderMenuModel support = new LocalHeaderMenuModel("CONTACT FIRST, VERIFY", "CONTACTFIRSTVERIFY", false, "", "", false);
            //        headerMenus.Add(support);

            //        //Creating MY ACCOUNT

            //        LocalHeaderMenuModel myAccount = new LocalHeaderMenuModel("My Account", "MyAccount", true, "MyAccount", "SystemUsers", false);


            //        myAccount.sideMenus = new List<LocalSideMenuModel>()
            //     {
            //         new LocalSideMenuModel("My Account","MyAccount",  "MyAccount", "SystemUsers", false),
            //         new LocalSideMenuModel("Change Password","ChangePassword",  "ChangePassword", "SystemUsers", false),            

            //     };


            //        headerMenus.Add(myAccount);
            //    }
            //}
            //catch (Exception ee)
            //{
            //}
            Session["headerMenus"] = _sysUser.setUpMenus(role, userId, currentOrgId);//headerMenus.OrderBy(rec => rec.DisplayOrder).ToList();
        }
        // Dynamic Side Menu Ends <<<<


        //Rajesh on 08/16/2013
        //Desc:-To get last login url of a user
        public string getLastLoginURL(string lastLoginURL)
        {
            if (lastLoginURL == null)
                return null;
            string[] URLparams = lastLoginURL.Split('/');//0-Empty 1-Controler 2-View
            var HeaderAndSideMenus = (List<LocalHeaderMenuModel>)Session["headerMenus"];
            var viewName = URLparams[2].Split('?')[0];
            if (HeaderAndSideMenus.FirstOrDefault(rec => rec.controllerName == URLparams[1] && rec.actionName == viewName) != null)//Last login url found
            {
                return lastLoginURL;
            }
            else
                foreach (var header in HeaderAndSideMenus)
                {
                    if (header.sideMenus.FirstOrDefault(rec => rec.controllerName == URLparams[1] && rec.actionName == viewName) != null)//Last login url found
                    {
                        return lastLoginURL;
                    }
                    else
                    {
                        foreach (var sideMenus in header.sideMenus)
                        {
                            if (sideMenus.ViewsAndPermissions.FirstOrDefault(rec => rec.ControllerName == URLparams[1] && rec.ViewName == viewName) != null)//Last login url found
                            {
                                return lastLoginURL;
                            }
                        }
                    }
                }

            return null;
        }
        //<<<getLastLoginURL End



        //>>DV:RP
        //==============On 2013-5-7 for LogOut Page==========================>
        public ActionResult LogOut()
        {
            Session.Clear();
            Session.Abandon();
            HttpCookie cookie = Request.Cookies["Login"];
            //cookie.Values["isLogin"] = "false";

            if (cookie != null)  // Rajesh 07/08/2013 Added condition
            {
                Response.Cookies["Login"]["UserName"] = cookie.Values["UserName"];
                //Response.Cookies["Login"].Expires = DateTime.Now.AddYears(1);
                Response.Cookies["Login"]["Password"] = cookie.Values["Password"];
                Response.Cookies["Login"]["isLogin"] = "false";

                Response.Cookies["Login"].Expires = DateTime.Now.AddYears(1);
            }

            return View();
        }
        //<<DV:RP DT:04/30/2013 Logout Ends <<<<

        // Kiran on 08/08/2013
        // To display the list of bidding interests >>>>
        [SessionExpire]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "MyAccount", sideMenuName = "BiddingInterests", ViewName = "BiddingInterestsList")]
        public ActionResult BiddingInterestsList()
        {
            var biddingInterestsList = _sysUser.GetBiddings(); //entityDB.BiddingInterests.OrderBy(m => m.BiddingInterestName).ToList();
            ViewBag.VBbiddingInterestsList = biddingInterestsList;
            return View();
        }
        [SessionExpire]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "MyAccount", sideMenuName = "ClaycoBiddingInterests", ViewName = "ClaycoBiddingInterestsList")]
        public ActionResult ClaycoBiddingInterestsList()
        {
            var biddingInterestsList = _sysUser.GetClaycoBiddingsList();// entityDB.ClaycoBiddingInterests.OrderBy(m => m.BiddingInterestName).ToList();
            ViewBag.VBbiddingInterestsList = biddingInterestsList;
            return View();
        }

        // Ends <<<

        // Kiran on 08/08/2013
        // To add bidding interests - Get method
        [HttpGet]
        [SessionExpireForView]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "MyAccount", sideMenuName = "BiddingInterests", ViewName = "AddBiddingInterest")]
        public ActionResult AddBiddingInterest(long biddingId)
        {
            LocalAddBiddingInterest currentBidding = new LocalAddBiddingInterest();
            var languages = from language in _sysUser.GetLanguages()//entityDB.Language.OrderBy(rec => rec.LanguageName).ToList()
                            select new SelectListItem
                            {
                                Text = language.LanguageName,
                                Value = language.LanguageId.ToString()
                            };
            ViewBag.Languages = languages;
            if (biddingId != -1)
            {
                var currentBiddingInterestRecord = _sysUser.GetFirstBiddings(biddingId);//entityDB.BiddingInterests.FirstOrDefault(record => record.BiddingInterestId == biddingId);
                currentBidding.LanguageId = currentBiddingInterestRecord.LanguageId;

                currentBidding.isEdit = true;
                currentBidding.BiddingInterestId = biddingId;
                currentBidding.BiddingInterestName = currentBiddingInterestRecord.BiddingInterestName;
                if (currentBiddingInterestRecord.Status != null)
                {
                    int currentBiddingStatus = (int)currentBiddingInterestRecord.Status;
                    if (currentBiddingStatus == 1)
                    {
                        currentBidding.Status = true;
                    }
                    else
                    {
                        currentBidding.Status = false;
                    }
                }
                return View(currentBidding);
            }
            currentBidding.BiddingInterestId = biddingId;
            return View(currentBidding);
        }
        // Ends <<<<
        [HttpGet]
        [SessionExpireForView]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "MyAccount", sideMenuName = "ClaycoBiddingInterests", ViewName = "AddClaycoBiddingInterest")]
        public ActionResult AddClaycoBiddingInterest(long biddingId)
        {
            var languages = from language in _sysUser.GetLanguages()//entityDB.Language.OrderBy(rec => rec.LanguageName).ToList()
                            select new SelectListItem
                            {
                                Text = language.LanguageName,
                                Value = language.LanguageId.ToString()
                            };
            ViewBag.Languages = languages;
            LocalAddClaycoBiddingInterest currentBidding = new LocalAddClaycoBiddingInterest();
            if (biddingId != -1)
            {
                var currentBiddingInterestRecord = _sysUser.GetFirstClaycoBiddings(biddingId); //entityDB.ClaycoBiddingInterests.FirstOrDefault(record => record.BiddingInterestId == biddingId);


                currentBidding.isEdit = true;
                currentBidding.BiddingInterestId = biddingId;
                currentBidding.BiddingInterestName = currentBiddingInterestRecord.BiddingInterestName;
                currentBidding.BiddingInterestCode = currentBiddingInterestRecord.BiddingInterestCode;
                currentBidding.LanguageId = currentBiddingInterestRecord.LanguageId;
                if (currentBiddingInterestRecord.Status != null)
                {
                    int currentBiddingStatus = (int)currentBiddingInterestRecord.Status;
                    if (currentBiddingStatus == 1)
                    {
                        currentBidding.Status = true;
                    }
                    else
                    {
                        currentBidding.Status = false;
                    }
                }
                return View(currentBidding);
            }
            currentBidding.BiddingInterestId = biddingId;
            return View(currentBidding);
        }



        // Kiran on 08/08/2013
        // To add bidding interests - Post method
        [HttpPost]
        [SessionExpireForView(pageType = "popupview")]
        public ActionResult AddBiddingInterest(LocalAddBiddingInterest bidding)
        {
            var languages = from language in _sysUser.GetLanguages()//entityDB.Language.OrderBy(rec => rec.LanguageName).ToList()
                            select new SelectListItem
                            {
                                Text = language.LanguageName,
                                Value = language.LanguageId.ToString()
                            };
            ViewBag.Languages = languages;

            if (ModelState.IsValid)
            {
                _sysUser.AddBiddings(bidding);  //Moved to Business Layer
                //if (bidding.isEdit == false)
                //{
                //    BiddingInterests currentBidding = new BiddingInterests();
                //    currentBidding.BiddingInterestName = bidding.BiddingInterestName;
                //    currentBidding.LanguageId = bidding.LanguageId;
                //    bool UserStatus = bidding.Status;
                //    if (UserStatus == true)
                //    {
                //        currentBidding.Status = 1;
                //    }
                //    else
                //    {
                //        currentBidding.Status = 0;
                //    }

                //    entityDB.BiddingInterests.Add(currentBidding);
                //    entityDB.SaveChanges();
                //}
                //else
                //{
                //    BiddingInterests currentBiddingInterestRecord = entityDB.BiddingInterests.FirstOrDefault(record => record.BiddingInterestId == bidding.BiddingInterestId);
                //    currentBiddingInterestRecord.BiddingInterestName = bidding.BiddingInterestName;
                //    currentBiddingInterestRecord.LanguageId = bidding.LanguageId;
                //    bool UserStatus = bidding.Status;
                //    if (UserStatus == true)
                //    {
                //        currentBiddingInterestRecord.Status = 1;
                //    }
                //    else
                //    {
                //        currentBiddingInterestRecord.Status = 0;
                //    }

                //    entityDB.Entry(currentBiddingInterestRecord).State = EntityState.Modified;
                //    entityDB.SaveChanges();
                //}
                return RedirectToAction("CloseBidding");
            }
            return View(bidding);
        }
        // Ends <<<
        [HttpPost]
        [SessionExpireForView(pageType = "popupview")]
        public ActionResult AddClaycoBiddingInterest(LocalAddClaycoBiddingInterest bidding)
        {
            var languages = from language in _sysUser.GetLanguages()
                            select new SelectListItem
                            {
                                Text = language.LanguageName,
                                Value = language.LanguageId.ToString()
                            };
            ViewBag.Languages = languages;

            if (ModelState.IsValid)
            {
                _sysUser.AddClaycoBidddings(bidding);
                //if (bidding.isEdit == false)
                //{
                //    ClaycoBiddingInterests currentBidding = new ClaycoBiddingInterests();
                //    currentBidding.BiddingInterestName = bidding.BiddingInterestName;
                //    currentBidding.LanguageId = bidding.LanguageId;
                //    currentBidding.BiddingInterestCode = bidding.BiddingInterestCode;

                //    bool UserStatus = bidding.Status;
                //    if (UserStatus == true)
                //    {
                //        currentBidding.Status = 1;
                //    }
                //    else
                //    {
                //        currentBidding.Status = 0;
                //    }

                //    entityDB.ClaycoBiddingInterests.Add(currentBidding);
                //    entityDB.SaveChanges();
                //}
                //else
                //{
                //    ClaycoBiddingInterests currentBiddingInterestRecord = entityDB.ClaycoBiddingInterests.FirstOrDefault(record => record.BiddingInterestId == bidding.BiddingInterestId);
                //    currentBiddingInterestRecord.BiddingInterestName = bidding.BiddingInterestName;
                //    currentBiddingInterestRecord.BiddingInterestCode = bidding.BiddingInterestCode;
                //    currentBiddingInterestRecord.LanguageId = bidding.LanguageId;

                //    bool UserStatus = bidding.Status;
                //    if (UserStatus == true)
                //    {
                //        currentBiddingInterestRecord.Status = 1;
                //    }
                //    else
                //    {
                //        currentBiddingInterestRecord.Status = 0;
                //    }

                //    entityDB.Entry(currentBiddingInterestRecord).State = EntityState.Modified;
                //    entityDB.SaveChanges();
                //}
                return RedirectToAction("CloseBidding");
            }
            return View(bidding);
        }
        // Ends <<<
        // Kiran on 08/08/2013
        // To close bidding interests view >>>
        public ActionResult CloseBidding()
        {
            return View();
        }

        public string DeleteBiddingInterest(long BiddingId)
        {
            _sysUser.DeleteBidding(BiddingId);
            //    var currentBiddingInterestRecord = entityDB.BiddingInterests.FirstOrDefault(record => record.BiddingInterestId == BiddingId);

            //    var currentBiddingPrequalificationReportingRecords = entityDB.PrequalificationReportingData.Where(record => record.ReportingDataId == BiddingId && record.ReportingType == 0).ToList();
            //    if (currentBiddingPrequalificationReportingRecords == null || currentBiddingPrequalificationReportingRecords.Count == 0)
            //    {
            //        entityDB.BiddingInterests.Remove(currentBiddingInterestRecord);
            //        entityDB.SaveChanges();
            //    }
            return "BiddingDeleteSuccess";
        }
        // Ends<<<

        // SUMA on TemplatePricing on 8/6/2013====>>>>
        //[SessionExpire]
        //[HttpGet]
        //[HeaderAndSidebar(headerName = "MyAccount", sideMenuName = "TieredPricing", ViewName = "TemplatePricing")]
        //public ActionResult TemplatePricing()
        //{
        //    // Added code on 5/11/2013 for displaying only questionnaire templates that are approved too
        //    var templateNameList = from record in entityDB.Templates.Where( record=>record.TemplateType == 0 && record.TemplateStatus == 1).OrderBy(rec => rec.TemplateName).ToList()                                  
        //                           select new SelectListItem
        //                           {
        //                               Text = record.TemplateName,
        //                               Value = record.TemplateID.ToString()
        //                           };
        //    // <<< Ends

        //    ViewBag.VBtemplateNames = templateNameList.ToList();
        //    var templatePricesRecord = entityDB.TemplatePrices.ToList();

        //    ViewBag.VBtemplatesPricesRecord = templatePricesRecord.ToList();

        //    ViewBag.VBcount = templatePricesRecord.Count;
        //    return View();
        //}
        //===Ends<<<

        // Suma on 09/02/2013 
        // To get the data required for partial view onLoad
        //[SessionExpireForView]
        //public ActionResult TemplatePricingPartialdata()
        //{

        //    //var templateNameList = entityDB.TemplatePrices.Where(m => m.TemplateID == selectedtemplateID ).ToList();                                
        //    //ViewBag.VBtemplateNames = templateNameList;
        //    var templatePricesRecord = entityDB.TemplatePrices.ToList();

        //    ViewBag.VBtemplatesPricesRecord = templatePricesRecord.OrderBy(rec=>rec.Templates.TemplateName).ToList();// Changes by Mani on 8/7/2015 for fvos-43

        //    ViewBag.VBcount = templatePricesRecord.Count;

        //    return PartialView("TemplatePricingListPartialView");
        //}
        // Ends <<<<

        /*
           Created Date:  11/04/2013   Created By: Kiran Talluri
           Purpose: To delete the template discount pricing record
       */
        //public string DeleteTemplateDiscountPricing(long TemplateDiscountId)
        //{
        //    var currentTemplateDiscountPricingRecord = entityDB.TemplateDiscountPrices.Find(TemplateDiscountId);

        //    entityDB.TemplateDiscountPrices.Remove(currentTemplateDiscountPricingRecord);
        //    entityDB.SaveChanges();
        //    return "TemplateDiscountPricingDeleteSuccess";
        //}

        /*
           Created Date:  11/04/2013   Created By: Kiran Talluri
           Purpose: To delete the template pricing & its associated discount pricing records
       */
        public string DeleteTemplatePricing(long TemplatePriceId)
        {
            _sysUser.DeleteTemplatePrice(TemplatePriceId);//Moved To Business Layer By vasavi
            //var currentTemplatePricingRecord = entityDB.TemplatePrices.Find(TemplatePriceId);
            //var currentTemplatePricingDiscountRecords = entityDB.TemplateDiscountPrices.Where(record => record.TemplatePriceId == TemplatePriceId).ToList();

            //foreach (var templateDiscountPricing in currentTemplatePricingDiscountRecords)
            //{
            //    entityDB.TemplateDiscountPrices.Remove(templateDiscountPricing);
            //    entityDB.SaveChanges();
            //}
            // entityDB.TemplatePrices.Remove(currentTemplatePricingRecord);
            // entityDB.SaveChanges();
            return "TemplatePricingDeleteSuccess";
        }



        //public JsonResult displaydefaultTemplate(Guid selectedtemplateID)
        //{
        //    var vardefaultTemplate = "";

        //    var templatesdefaultTemplate = entityDB.Templates.FirstOrDefault(m => m.TemplateID == selectedtemplateID);
        //    bool templatevalue = true;
        //    if (templatesdefaultTemplate.DefaultTemplate.Equals(templatevalue))
        //    {
        //        vardefaultTemplate = "Standard";

        //    }
        //    else
        //    {
        //        vardefaultTemplate = "Custom";
        //    }
        //    return Json(vardefaultTemplate, JsonRequestBehavior.AllowGet);
        //}

        //[SessionExpireForView]
        // [OutputCache(Duration=0)]
        //public ActionResult TemplatePricingListPartialView(Guid selectedtemplateID, Decimal selectedtemplatePrice)
        //{

        //    TemplatePrices sampleTable = new TemplatePrices();

        //    var checkrecordTemplatePrices = entityDB.TemplatePrices.FirstOrDefault(m => m.TemplateID == selectedtemplateID);
        //    if (checkrecordTemplatePrices == null)
        //    {
        //        sampleTable.TemplateID = selectedtemplateID;
        //        sampleTable.TemplatePrice = selectedtemplatePrice;
        //        entityDB.TemplatePrices.Add(sampleTable);
        //        entityDB.SaveChanges();

        //    }
        //    else
        //    {
        //        return Json("Error :Price for this product already exists");

        //    }
        //    entityDB = new EFDbContext();
        //    var templatePricerecord = entityDB.TemplatePrices.ToList();// (m => m.TemplateID == selectedtemplateID).ToList();
        //    ViewBag.VBcount = templatePricerecord.Count;
        //    ViewBag.VBtemplatesPricesRecord = templatePricerecord.OrderBy(rec=>rec.Templates.TemplateName).ToList();
        //    return PartialView("TemplatePricingListPartialView");

        //}

        // Suma on 09/03/2013 
        // for Discount pricing partial view onLoad
        //[SessionExpireForView]
        //public ActionResult TemplateDiscountPricingPartialData(Guid SelectedtemplateID)
        //{
        //    var Templaterecord = entityDB.TemplatePrices.FirstOrDefault(m => m.TemplateID == SelectedtemplateID);
        //    var selectedpriceId = Templaterecord.TemplatePriceId;
        //    Session["SelectedTemplatePriceID"] = selectedpriceId;
        //    Session["SelectedTemplatePrice"] = Templaterecord.TemplatePrice;

        //    @ViewBag.VBTemplatePrice = Templaterecord.TemplatePrice;
        //    TemplateDiscountPricesNotInUse sampleTable = new TemplateDiscountPricesNotInUse();

        //    ViewBag.VBselectedTemplateID = SelectedtemplateID;

        //    var checkTemplatePriceId = from record in entityDB.TemplateDiscountPrices.ToList()
        //                               where (record.TemplatePriceId == selectedpriceId)
        //                               select record;
        //    ViewBag.VBcount = checkTemplatePriceId.Count();
        //    ViewBag.VBtemplatesDiscountPricesRecord = checkTemplatePriceId.ToList();

        //    return PartialView("TemplateDiscountPricingListPartialView");
        //}

        //[HttpGet]
        //[HeaderAndSidebar(headerName = "MyAccount", sideMenuName = "TieredPricing", ViewName = "TemplateDiscountPricing")]
        //[SessionExpire]
        //public ActionResult TemplateDiscountPricing(Guid SelectedtemplateID)
        //{
        //    var Templaterecord = entityDB.TemplatePrices.FirstOrDefault(m => m.TemplateID == SelectedtemplateID);
        //    var selectedpriceId = Templaterecord.TemplatePriceId;
        //    Session["SelectedTemplatePriceID"] = selectedpriceId;
        //    Session["SelectedTemplatePrice"] = Templaterecord.TemplatePrice;
        //    @ViewBag.templateName = Templaterecord.Templates.TemplateName;//Suma on 5/12/2013
        //    @ViewBag.VBTemplatePrice = Templaterecord.TemplatePrice;
        //    //TemplateDiscountPrices sampleTable = new TemplateDiscountPrices();

        //    //ViewBag.VBselectedTemplateID = SelectedtemplateID;

        //    //var checkTemplatePriceId = from record in entityDB.TemplateDiscountPrices.ToList()
        //    //                           where (record.TemplatePriceId == selectedpriceId)
        //    //                           select record;
        //    //ViewBag.VBcount = checkTemplatePriceId.Count();
        //    //ViewBag.VBtemplatesDiscountPricesRecord = checkTemplatePriceId.ToList();
        //    ViewBag.VBtemplateId = SelectedtemplateID;
        //    return View();

        //}

        //[HttpPost]
        //[SessionExpireForView]

        //public ActionResult TemplateDiscountPricingListPartialView(int discounttype, int minPrequalifications, int maxPrequalifications, decimal discountedPrice)
        //{
        //    TemplateDiscountPricesNotInUse sampleTable = new TemplateDiscountPricesNotInUse();
        //    long selectedTemplatePriceId = (long)Session["SelectedTemplatePriceID"];
        //    decimal selectedTemplatePrice = (decimal)Session["SelectedTemplatePrice"];

        //    int localdiscountType = discounttype;
        //    int localminPrequalifications = minPrequalifications;
        //    int localmaxPrequalifications = maxPrequalifications;
        //    decimal localdiscountedPrice = discountedPrice;

        //    //==ClientSide Validations
        //    //if (localmaxPrequalifications <= localminPrequalifications)
        //    //{
        //    //   // ModelState.AddModelError("MaxNoOfPrequalification", "MaxPrequalifications should be greater than MinPrequalifications");
        //    //    return Json("Error :MaxPrequalifications should be greater than MinPrequalifications");
        //    //}
        //    //if (localdiscountedPrice >= selectedTemplatePrice)
        //    //{
        //    //    //ModelState.AddModelError("DiscountedPrice", "Not a valid Discount");
        //    //    return Json("Error :Not a valid Discount");


        //    //}

        //    var templateDiscountPricingrecords = entityDB.TemplateDiscountPrices.Where(m => m.TemplatePriceId == selectedTemplatePriceId && m.DiscountType == localdiscountType).OrderBy(m => m.MinNoOfPrequalification).ToList();

        //    if (templateDiscountPricingrecords.Count == 0)
        //    {
        //        sampleTable.TemplatePriceId = selectedTemplatePriceId;
        //        sampleTable.DiscountType = localdiscountType;
        //        sampleTable.MinNoOfPrequalification = localminPrequalifications;
        //        sampleTable.MaxNoOfPrequalification = localmaxPrequalifications;
        //        sampleTable.DiscountedPrice = localdiscountedPrice;
        //        entityDB.TemplateDiscountPrices.Add(sampleTable);
        //        entityDB.SaveChanges();

        //    }
        //    else
        //    {
        //        //int  conditionsatisfied = 0;
        //        bool LowerFlag = false;
        //        bool HigherFlag = false;
        //        foreach (var record in templateDiscountPricingrecords)
        //        {
        //            //&& ((localminPrequalifications != record.MinNoOfPrequalification) && (localmaxPrequalifications != record.MinNoOfPrequalification))
        //            if (((localminPrequalifications < record.MinNoOfPrequalification) && (localmaxPrequalifications < record.MinNoOfPrequalification)))
        //            {
        //                LowerFlag = true;
        //            }
        //            else
        //            {
        //                LowerFlag = false;
        //            }

        //            //else
        //            //{
        //            //    LowerFlag = false;
        //            //}
        //            //&& ((localminPrequalifications != record.MaxNoOfPrequalification) && (localmaxPrequalifications != record.MaxNoOfPrequalification))
        //            if (((localminPrequalifications > record.MaxNoOfPrequalification) && (localmaxPrequalifications > record.MaxNoOfPrequalification)))
        //            {
        //                //if ((localmaxPrequalifications > record.MinNoOfPrequalification) && (localmaxPrequalifications > record.MaxNoOfPrequalification))
        //                //{
        //                HigherFlag = true;
        //                //}
        //            }
        //            else
        //            {
        //                HigherFlag = false;
        //            }
        //            //}
        //            //else
        //            //{
        //            //    HigherFlag = false;
        //            //}

        //        }
        //        if (HigherFlag || LowerFlag)
        //        {
        //            sampleTable.TemplatePriceId = selectedTemplatePriceId;
        //            sampleTable.DiscountType = localdiscountType;
        //            sampleTable.MinNoOfPrequalification = localminPrequalifications;
        //            sampleTable.MaxNoOfPrequalification = localmaxPrequalifications;
        //            sampleTable.DiscountedPrice = localdiscountedPrice;
        //            entityDB.TemplateDiscountPrices.Add(sampleTable);
        //            entityDB.SaveChanges();
        //        }
        //        else
        //        {
        //            return Json("Error :Price range already exists");
        //        }

        //    }
        //    entityDB = new EFDbContext();
        //    var pricingRecords = entityDB.TemplateDiscountPrices.Where(m => m.TemplatePriceId == selectedTemplatePriceId).OrderBy(m => m.MinNoOfPrequalification).ToList();
        //    ViewBag.VBcount = pricingRecords.Count();
        //    ViewBag.VBtemplatesDiscountPricesRecord = pricingRecords.ToList();
        //    return PartialView("TemplateDiscountPricingListPartialView");

        //}

        /*
    Created Date:  10/03/2013   Created By: Kiran Talluri
    Purpose: To display the Proficiency Capabilities form
 */
        [HttpGet]
        [SessionExpireForView(pageType = "popupview")]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "MyAccount", sideMenuName = "ProficiencyCapabilities", ViewName = "AddProficiencyCapability")]
        public ActionResult AddProficiencyCapability(long ProficiencyId)
        {
            //var categoriesList = (from categoryNames in
            //                          (from categories in entityDB.ProficiencyCapabilities.ToList()
            //                           select categories.ProficiencyCategory).Distinct()
            //                      select new SelectListItem
            //                      {
            //                          Text = categoryNames,
            //                          Value = categoryNames
            //                      });
            ViewBag.HasPQ = _sysUser.HasPrequalificationReportingData(ProficiencyId);//  entityDB.PrequalificationReportingData.Any(r => r.ReportingType == 1 && r.ReportingDataId == ProficiencyId);

            var categoriesList = (from categoryNames in _sysUser.GetProficiencyCategory().Select(r => r.ProficiencyCategory).Distinct()
                                  select new SelectListItem
                                  {
                                      Text = categoryNames,//categoryNames.ProficiencyCategory.Distinct(),
                                      Value = categoryNames//categoryNames.ProficiencyCategory// categoryNames
                                  });
            SelectListItem defaultItem = new SelectListItem();
            defaultItem.Text = "Other";
            defaultItem.Value = "OTHERS";
            var categoriesListWithDefaultItem = categoriesList.ToList();
            categoriesListWithDefaultItem.Add(defaultItem);

            ViewBag.VBCategoriesList = categoriesListWithDefaultItem;

            var proficiencyCapabilities = _sysUser.GetProficiencyCategory();// entityDB.ProficiencyCapabilities.ToList();
            ViewBag.VBProficiencyCapabilities = proficiencyCapabilities;

            LocalAddProficiencyCapability addProficiency = new LocalAddProficiencyCapability();
            if (ProficiencyId == -1)
            {
                addProficiency.isEdit = false;
                addProficiency.Status = true;
            }
            else
            {
                addProficiency.isEdit = true;
                var currentProficiencyRecord = _sysUser.FindProficiencyCategory(ProficiencyId);//entityDB.ProficiencyCapabilities.Find(ProficiencyId);
                if (currentProficiencyRecord.Status != null)
                {
                    int currentProficiencyStatus = (int)currentProficiencyRecord.Status;

                    if (currentProficiencyStatus == 1)
                    {
                        addProficiency.Status = true;
                    }
                    else
                    {
                        addProficiency.Status = false;
                    }
                }
                addProficiency.ProficiencyCategory = currentProficiencyRecord.ProficiencyCategory;
                addProficiency.ProficiencySubCategory = currentProficiencyRecord.ProficiencySubCategroy;
                addProficiency.ProficiencyName = currentProficiencyRecord.ProficiencyName;

                //ViewBag.VBCategoriesList = (from categoryNames in
                //                                (from categories in entityDB.ProficiencyCapabilities.ToList()
                //                                 where categories.ProficiencyCategory == currentProficiencyRecord.ProficiencyCategory
                //                                 select categories.ProficiencyCategory).Distinct()
                //                            select new SelectListItem
                //                            {
                //                                Text = categoryNames,
                //                                Value = categoryNames
                //                            }); ;
                ViewBag.VBCategoriesList = (from categoryNames in _sysUser.GetProficiencyCategoryNames(currentProficiencyRecord.ProficiencyCategory)     //entityDB.ProficiencyCapabilities.Where(rec => rec.ProficiencyCategory == currentProficiencyRecord.ProficiencyCategory).Select(rec => rec.ProficiencyCategory).Distinct().ToList()

                                            select new SelectListItem
                                            {
                                                Text = categoryNames,
                                                Value = categoryNames
                                            }); ;
            }
            return View(addProficiency);
        }

        /*
           Created Date:  10/03/2013   Created By: Kiran Talluri
           Purpose: To add  the Proficiency Capabilities 
        */
        [HttpPost]
        [SessionExpireForView(pageType = "popupview")]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "MyAccount", sideMenuName = "ProficiencyCapabilities", ViewName = "AddProficiencyCapability")]
        public ActionResult AddProficiencyCapability(LocalAddProficiencyCapability proficiency)
        {
            ViewBag.HasPQ = _sysUser.HasPrequalificationReportingData(proficiency.ProficiencyId);// entityDB.PrequalificationReportingData.Any( r => r.ReportingType == 1 && r.ReportingDataId == proficiency.ProficiencyId);
            return RedirectToAction(_sysUser.AddProficiency(proficiency));
            //if (proficiency.isEdit == false)
            //{
            //    ProficiencyCapabilities addProficiency = new ProficiencyCapabilities();
            //    if (proficiency.ProficiencyCategory == "OTHERS" || proficiency.ProficiencyCategory == null)
            //    {
            //        proficiency.ProficiencyCategoryName.Trim();
            //        addProficiency.ProficiencyCategory = proficiency.ProficiencyCategoryName;
            //    }
            //    else
            //    {
            //        addProficiency.ProficiencyCategory = proficiency.ProficiencyCategory;
            //    }

            //    if (proficiency.ProficiencySubCategory == "OTHERS")
            //    {
            //        proficiency.ProficiencySubCategoryName.Trim();
            //        addProficiency.ProficiencySubCategroy = proficiency.ProficiencySubCategoryName;
            //    }
            //    else if (proficiency.ProficiencySubCategory == "NONE")
            //    {
            //        addProficiency.ProficiencySubCategroy = "";
            //    }
            //    else
            //    {
            //        addProficiency.ProficiencySubCategroy = proficiency.ProficiencySubCategory;
            //    }

            //    addProficiency.ProficiencyName = proficiency.ProficiencyName;

            //    bool ProficiencyStatus = proficiency.Status;
            //    if (ProficiencyStatus == true)
            //    {
            //        addProficiency.Status = 1;
            //    }
            //    else
            //    {
            //        addProficiency.Status = 0;
            //    }
            //    entityDB.ProficiencyCapabilities.Add(addProficiency);
            //    entityDB.SaveChanges();
            //    return RedirectToAction("CloseProficiencyCapability");
            //}
            //else
            //{
            //    var currentProficiencyRecord = entityDB.ProficiencyCapabilities.Find(proficiency.ProficiencyId);
            //    currentProficiencyRecord.ProficiencyName = proficiency.ProficiencyName;
            //    bool ProficiencyStatus = proficiency.Status;
            //    if (ProficiencyStatus == true)
            //    {
            //        currentProficiencyRecord.Status = 1;
            //    }
            //    else
            //    {
            //        currentProficiencyRecord.Status = 0;
            //    }
            //    entityDB.Entry(currentProficiencyRecord).State = EntityState.Modified;
            //    entityDB.SaveChanges();
            //    return RedirectToAction("CloseProficiencyCapability");
            //}
        }



        public ActionResult CloseProficiencyCapability()
        {
            return View();
        }

        /*
          Created Date:  10/03/2013   Created By: Kiran Talluri
          Purpose: To get the list of subcategories based on selected category
       */
        public ActionResult getSubCategories(string Category, bool isEdit, long ProficiencyId)
        {
            //var subCategoriesList = (from subCategoryNames in
            //                             (from subCategories in entityDB.ProficiencyCapabilities.ToList()
            //                              where subCategories.ProficiencyCategory == Category && subCategories.ProficiencySubCategroy != ""
            //                              select subCategories.ProficiencySubCategroy).Distinct()
            //                         select new SelectListItem
            //                         {
            //                             Text = subCategoryNames,
            //                             Value = subCategoryNames
            //                         });
            var subCategoriesList = (from subCategoryNames in
                                        _sysUser.GetProficiencySubCategoryNamesWithCategory(Category)
                                     select new SelectListItem
                                     {
                                         Text = subCategoryNames,
                                         Value = subCategoryNames
                                     });

            SelectListItem defaultItem1 = new SelectListItem();
            defaultItem1.Text = "Other";
            defaultItem1.Value = "OTHERS";
            SelectListItem Item2 = new SelectListItem();
            Item2.Text = "None";
            Item2.Value = "NONE";
            var subCategoriesListWithDefaultItem = subCategoriesList.ToList();
            subCategoriesListWithDefaultItem.Add(defaultItem1);
            subCategoriesListWithDefaultItem.Insert(0, Item2);

            if (isEdit == true)
            {
                //subCategoriesList = (from subCategoryNames in
                //                         (from subCategories in entityDB.ProficiencyCapabilities.ToList()
                //                          where subCategories.ProficiencyId == ProficiencyId
                //                          select subCategories.ProficiencySubCategroy).Distinct()
                //                     select new SelectListItem
                //                     {
                //                         Text = subCategoryNames,
                //                         Value = subCategoryNames
                //                     });
                subCategoriesList = (from subCategoryNames in _sysUser.GetProficiencySubCategoryNames(ProficiencyId)//entityDB.ProficiencyCapabilities.Where(rec => rec.ProficiencyId == ProficiencyId).Select(rec => rec.ProficiencySubCategroy).Distinct().ToList()
                                     select new SelectListItem
                                     {
                                         Text = subCategoryNames,
                                         Value = subCategoryNames
                                     });
                return Json(subCategoriesList);
            }

            return Json(subCategoriesListWithDefaultItem);
        }

        /*
          Created Date:  10/03/2013   Created By: Kiran Talluri
          Purpose: To Display the list of Proficiency Capabilities
        */
        [SessionExpire]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "MyAccount", sideMenuName = "ProficiencyCapabilities", ViewName = "ProficiencyCapabilitiesList")]
        public ActionResult ProficiencyCapabilitiesList()
        {
            var proficiencyCapabilitiesList = _sysUser.GetProficiencyData();                //entityDB.ProficiencyCapabilities.OrderBy(rec => rec.ProficiencyCategory)
                                                                                            //    .ThenBy(r => r.ProficiencySubCategroy)
                                                                                            //    .Select(
                                                                                            //        r =>
                                                                                            //            new ProficiancyWithIsEdit()
                                                                                            //            {
                                                                                            //                ProficiencyCategory = r.ProficiencyCategory,
                                                                                            //                ProficiencyId = r.ProficiencyId,
                                                                                            //                ProficiencySubCategroy = r.ProficiencySubCategroy,
                                                                                            //                ProficiencyName = r.ProficiencyName,
                                                                                            //                Status = r.Status,
                                                                                            //                IsEdit =
                                                                                            //                !entityDB.PrequalificationReportingData.Any(
                                                                                            //                    rec => rec.ReportingType == 1 && rec.ReportingDataId == r.ProficiencyId)
                                                                                            //            })
                                                                                            //    .ToList();
            ViewBag.VBProficiencyCapabilitiesList = proficiencyCapabilitiesList;
            return View();
        }

        public int GetproficiencyData(long proficiencyId)
        {
            var data = _sysUser.GetPrequalificationReportingData(proficiencyId);
            return data.Count();
        }

        public string EditProficiencyCapability(long proficiencyId, string category, string SubCategroy)
        {
            return _sysUser.EditProficiency(proficiencyId, category, SubCategroy);
            //try
            //{
            //    var proficiencydata = entityDB.ProficiencyCapabilities.Find(proficiencyId);
            //    proficiencydata.ProficiencyCategory = category == null ? null : category.Trim();
            //    proficiencydata.ProficiencySubCategroy = SubCategroy == null ? null : SubCategroy.Trim();
            //    entityDB.Entry(proficiencydata).State = EntityState.Modified;
            //    entityDB.SaveChanges();
            //    return "OK";
            //}
            //catch (Exception ee)
            //{
            //    return "NO";
            //}

        }


        /*
          Created Date:  11/05/2013   Created By: Kiran Talluri
          Purpose: To Display the list of Document Types
        */
        [SessionExpire]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "MyAccount", sideMenuName = "DocumentTypes", ViewName = "DocumentTypesList")]
        public ActionResult DocumentTypesList()
        {
            var documentTypesList = _sysUser.GetDocuments();//entityDB.DocumentType.ToList();
            ViewBag.VBDocumentTypesList = documentTypesList;
            return View();
        }

        [HttpGet]
        [SessionExpireForView(pageType = "popupview")]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "MyAccount", sideMenuName = "DocumentTypes", ViewName = "AddDocumentType")]
        public ActionResult AddDocumentType(long DocumentTypeId)
        {
            ViewBag.isDocumentTypeExists = false;
            //LocalAddDocumentType addDocument = new LocalAddDocumentType();
            //if (DocumentTypeId == -1)
            //{
            //    addDocument.isEdit = false;
            //}
            //else
            //{
            //    addDocument.isEdit = true;
            //    var currentDocumentTypeRecord = entityDB.DocumentType.Find(DocumentTypeId);
            //    addDocument.DocumentTypeName = currentDocumentTypeRecord.DocumentTypeName;
            //    addDocument.DocumentTypeId = currentDocumentTypeRecord.DocumentTypeId;
            //    addDocument.RestrictAccess = currentDocumentTypeRecord.RestrictAccess.ToBoolNullSafe(); 
            //    if (currentDocumentTypeRecord.DocumentExpires == true)
            //    {
            //        addDocument.DocumentExpires = true;
            //    }
            //}

            return View(_sysUser.GetDocumentTypeBasedOnId(DocumentTypeId));
        }

        [HttpPost]
        [SessionExpireForView(pageType = "popupview")]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "MyAccount", sideMenuName = "DocumentTypes", ViewName = "AddDocumentType")]
        public ActionResult AddDocumentType(LocalAddDocumentType addDocumentType)
        {
            if (ModelState.IsValid)
            {
                var currentDocumentType = _sysUser.HasDocument(addDocumentType.DocumentTypeName, addDocumentType.DocumentTypeId); //entityDB.DocumentType.FirstOrDefault(record => record.DocumentTypeName.ToUpper() == addDocumentType.DocumentTypeName.Trim().ToUpper() && record.DocumentTypeId != addDocumentType.DocumentTypeId);
                if (currentDocumentType)
                {
                    ViewBag.isDocumentTypeExists = true;
                    return View(addDocumentType);
                }
                //if (addDocumentType.isEdit == false)
                //{
                //    DocumentType documentType = new DocumentType();
                //    documentType.DocumentTypeName = addDocumentType.DocumentTypeName.Trim();
                //    documentType.DocumentExpires = addDocumentType.DocumentExpires;
                //    documentType.RestrictAccess = addDocumentType.RestrictAccess;
                //    documentType.DocumentInstruction = addDocumentType.DocumentTypeName.Trim();
                //    documentType.PrequalificationUseOnly = true;

                //    entityDB.DocumentType.Add(documentType);
                //    entityDB.SaveChanges();
                //}
                //else
                //{
                //    var currentDocumentTypeRecord = entityDB.DocumentType.Find(addDocumentType.DocumentTypeId);
                //    currentDocumentTypeRecord.DocumentTypeName = addDocumentType.DocumentTypeName.Trim();
                //    currentDocumentTypeRecord.DocumentExpires = addDocumentType.DocumentExpires;
                //    currentDocumentTypeRecord.RestrictAccess = addDocumentType.RestrictAccess;
                //    currentDocumentTypeRecord.DocumentInstruction = addDocumentType.DocumentTypeName.Trim();

                //    entityDB.Entry(currentDocumentTypeRecord).State = EntityState.Modified;
                //    entityDB.SaveChanges();
                //}
                _sysUser.AddDocumentType(addDocumentType);
                return RedirectToAction("CloseBidding");
            }
            return View(addDocumentType);
        }

        ///////////////////
        // Instruction Banner
        [HeaderAndSidebar(headerName = "MyAccount", sideMenuName = "InstructionBanner", ViewName = "InstructionBanner")]
        [SessionExpire]
        public ActionResult InstructionBanner()
        {
            InstructionBanner banner = new InstructionBanner();
            banner.Instruction = "";

            try
            {
                var filePath = Path.Combine(System.Web.HttpContext.Current.Server.MapPath("~/Views/SystemUsers/Instruction.txt"));
                if (System.IO.File.Exists(filePath))
                {
                    banner.Instruction = System.IO.File.ReadAllText(filePath);
                }
            }
            catch (Exception ex)
            {
            }

            ViewBag.isFailed = false;
            ViewBag.isSuccess = false;
            ViewBag.Header = banner.Instruction;
            return View(banner);
        }
        //Rajesh on 1/10/2015
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "MyAccount", sideMenuName = "AssignRoles", ViewName = "AssignRoles")]
        [SessionExpire]
        public ActionResult AssignRoles(string Message)
        {
            ViewBag.VBUserOrgTypes = new[] {
                                        new SelectListItem() {Text= "Select Organization Type", Value = ""},
                                        new SelectListItem() { Text = "Admin", Value="0" },
                                        new SelectListItem() { Text = "Client", Value="1" },
                                        new SelectListItem() { Text = OrganizationType.SuperClient, Value="3" },
                                        new SelectListItem() { Text = "Vendor", Value= "2" }};
            if (@Session["RoleName"].ToString().Equals("Admin"))
            {
                ViewBag.VBOrganizationsList = new[] {
                                            new SelectListItem() {Text= "Select Organization", Value=""}};
            }
            else
            {
                var currentUserOrgId = (long)Session["currentOrgId"];
                var orgs = _sysUser.GetOrganizations(currentUserOrgId);
                List<SelectListItem> clientOrg = new List<SelectListItem>();
                orgs.ForEach(s => clientOrg.Add(new SelectListItem() { Text = s.Key, Value = s.Value.ToString() }));
                //var clientOrg = from orgs in entityDB.Organizations.ToList().Where(rec => rec.OrganizationID == currentUserOrgId).ToList()
                //                select new SelectListItem
                //                {
                //                    Text = orgs.Name,
                //                    Value = orgs.OrganizationID.ToString()
                //                };

                ViewBag.VBOrganizationsList = clientOrg;
            }
            ViewBag.Message = Message;
            return View();
        }

        [HttpPost]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "MyAccount", sideMenuName = "AssignRoles", ViewName = "AssignRoles")]
        [SessionExpire]
        public ActionResult AssignRoles(List<LocalAssignRoles> UserRoles)
        {
            //long OrgId = -1;
            //var SuperUserRole = "";

            //var sysUserOrgs = UserRoles.Select(rec => rec.SysUserOrgId);
            //var oldSuperUsersList = entityDB.SystemUsersOrganizations.Where(rec => sysUserOrgs.Contains(rec.SysUserOrganizationId) && rec.PrimaryUser == true).ToList();

            //foreach (var UserRole in UserRoles)
            //{
            //    var OrgRoles = entityDB.SystemUsersOrganizationsRoles.Where(rec => rec.SysUserOrgId == UserRole.SysUserOrgId).ToList();
            //    foreach (var OrgRole in OrgRoles)
            //    {

            //        entityDB.SystemUsersOrganizationsRoles.Remove(OrgRole);

            //    }

            //    for (int i = 0; i < UserRole.Roles.Count; i++)
            //    {
            //        if (UserRole.IsRoleSelected[i] == false)
            //            continue;
            //        SystemUsersOrganizationsRoles OrgRoleLoc = new SystemUsersOrganizationsRoles();
            //        OrgRoleLoc.SysRoleId = UserRole.Roles[i];
            //        OrgRoleLoc.SysUserOrgId = UserRole.SysUserOrgId;
            //        entityDB.SystemUsersOrganizationsRoles.Add(OrgRoleLoc);

            //        if (OrgId == -1)
            //        {
            //            OrgId = entityDB.SystemUsersOrganizations.Find(UserRole.SysUserOrgId).OrganizationId;
            //            var OrgType = entityDB.SystemUsersOrganizations.Find(UserRole.SysUserOrgId).Organizations.OrganizationType;
            //            SuperUserRole = entityDB.SystemRoles.FirstOrDefault(rec => rec.OrganizationType == OrgType && rec.RoleName == "Super User").RoleId;
            //        }

            //        if (UserRole.Roles[i].ToLower().Equals(SuperUserRole.ToLower()))
            //        {
            //            if (oldSuperUsersList.Any())
            //            {
            //                foreach (var oldSuperUser in oldSuperUsersList)
            //                {
            //                    oldSuperUser.PrimaryUser = false;
            //                    entityDB.Entry(oldSuperUser).State = EntityState.Modified;
            //                }
            //            }
            //            var superUser = entityDB.SystemUsersOrganizations.Find(UserRole.SysUserOrgId);
            //            superUser.PrimaryUser = true;
            //            entityDB.Entry(superUser).State = EntityState.Modified;
            //        }
            //        entityDB.SaveChanges();
            //    }

            //}
            ViewBag.OrgId = _sysUser.AssignUserRoles(UserRoles);
            //  ViewBag.OrgId = OrgId;
            return Redirect("AssignRoles?Message=Ok");
        }
        [SessionExpire]
        public ActionResult GetUsersWithPermissions(long OrgId)
        {
            //var UserRoles = new List<LocalAssignRoles>();
            //var SelectedOrg =  entityDB.Organizations.Find(OrgId);

            //var SelectedSysUserOrgList = SelectedOrg.SystemUsersOrganizations.ToList();
            //var OrgType = SelectedOrg.OrganizationType;
            //var SysRoles = entityDB.SystemRoles.Where(rec => rec.OrganizationType == OrgType && rec.RoleName != "Employee Training").ToList();
            //foreach (var SysUserOrg in SelectedSysUserOrgList)
            //{
            //    var temp = SysUserOrg.SystemUsersOrganizationsRoles.FirstOrDefault(rec => rec.SystemRoles.RoleName == "Employee Training");
            //    if (temp != null)
            //        continue;
            //    LocalAssignRoles Role = new LocalAssignRoles();

            //    Role.Roles = SysRoles.Select(rec => rec.RoleId).ToList();
            //    Role.RoleNames = SysRoles.Select(rec => rec.RoleName).ToList();
            //    Role.SysUserOrgId = SysUserOrg.SysUserOrganizationId;
            //    var contact = SysUserOrg.SystemUsers.Contacts;
            //    if (contact != null && contact.Count != 0)
            //        Role.UserName = contact[0].LastName + ", " + contact[0].FirstName;
            //    else
            //        Role.UserName = SysUserOrg.SystemUsers.Email;

            //    Role.IsRoleSelected = new List<bool>();
            //    foreach (var RoleLoc in Role.Roles)
            //    {
            //        var flag = false;
            //        try { flag = SysUserOrg.SystemUsersOrganizationsRoles.FirstOrDefault(rec => rec.SysRoleId == RoleLoc) != null; }
            //        catch { }
            //        Role.IsRoleSelected.Add(flag);
            //    }
            //    UserRoles.Add(Role);
            //}
            var UserRoles = _sysUser.UserWithPermissions(OrgId).OrderBy(r => r.UserName).ToList();//Moved To Business Layer By Vasavi
            return PartialView(UserRoles);
        }
        //Ends<<<

        [HeaderAndSidebar(headerName = "MyAccount", sideMenuName = "InstructionBanner", ViewName = "InstructionBanner")]
        [SessionExpire]
        [HttpPost]
        public ActionResult InstructionBanner(InstructionBanner form)
        {
            try
            {
                var filePath = Path.Combine(System.Web.HttpContext.Current.Server.MapPath("~/Views/SystemUsers/Instruction.txt"));
                if (System.IO.File.Exists(filePath))
                {
                    System.IO.File.WriteAllText(filePath, form.Instruction);
                    ViewBag.isSuccess = true;
                    ViewBag.isFailed = false;
                }
                else
                {
                    ViewBag.isSuccess = false;
                    ViewBag.isFailed = true;
                }
            }
            catch (Exception ex)
            {
                ViewBag.isSuccess = false;
                ViewBag.isFailed = true;
            }

            return View(form);
        }
        public SelectListItem[] GetStateSelectListItems(bool includeCanada = false)
        {
            var helper = new FVGen3.BusinessLogic.DropdDownDataSupplier();
            //var states = helper.GetDynamicTableData("UnitedStates");
            var states = helper.GetUnitedStatesTableData(-1, false, includeCanada);
            List<SelectListItem> selectLists = new List<SelectListItem>();
            //'<option>' + datasite + '</option>'
            states.ForEach(s => selectLists.Add(new SelectListItem() { Text = s.Key, Value = s.Value }));
            return selectLists.ToArray();
        }
        public ActionResult LoginHelp()
        {
            LocalLoginHelp loginHelp = new LocalLoginHelp();
            return View(loginHelp);
        }

        [HttpPost]
        public ActionResult LoginHelp(LocalLoginHelp loginHelp)
        {
            ViewBag.VBMailSent = true;
            if (ModelState.IsValid)
            {
                string strMessage = "";
                string body = System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Views/SystemUsers/EMailFormatOfLoginHelp.htm"));
                body = body.Replace("[PersonName]", loginHelp.PersonName);
                body = body.Replace("[CompanyName]", loginHelp.CompanyName);
                body = body.Replace("[Address]", loginHelp.Address);
                body = body.Replace("[TelephoneNumber]", loginHelp.TelephoneNumber);
                body = body.Replace("[EmailAddress]", loginHelp.Email);// sumanth on 10/15/2015 for SMI-267 
                body = body.Replace("[Comments]", loginHelp.Comments);
                string toEmail = ConfigurationManager.AppSettings["EmailForLoginHelp"];

                strMessage = Utilities.SendMail.sendMail(toEmail, body, loginHelp.PersonName + " requested for Login Help");
                if (strMessage.ToString() == "Success")
                {
                    ViewBag.isSuccess = true;
                }
                else
                {
                    ViewBag.VBMailSent = false;
                }
                return View();
            }
            else
            {
                return View();
            }
        }
        public bool IsUserHasPermission(long roleId)
        {
            return _sysUser.IsUserHasPermission(SSession.UserId, roleId);
        }
        public ActionResult UnauthorizedAccess()
        {
            return View();
        }
        public ActionResult ExceptionHandling(long ErrorId)
        {

            ViewBag.errorid = ErrorId;


            return View();
        }

        //public class ClientBusinessUnitsSiteWithType
        //{
        //    public string Text { get; set; }
        //    public string Value { get; set; }
        //    public int Type { get; set; }
        //}

        [SessionExpire]
        public ActionResult LoginAsSelectedUser(Guid userId)
        {
            Guid LoggedInUseruserId = (Guid)Session["UserId"];

            bool canLoginAsAnyUser = _sysUser.CheckUserCanLoginAsAnyUser(LoggedInUseruserId);
            if (canLoginAsAnyUser != null && canLoginAsAnyUser == true)
            {
                UserInfo userInfo = _sysUser.GetUserInfo(userId);
                bool status = _sysUser.UpdateUserLog(userInfo.UserId);

                Session["OldUserName"] = Session["UserName"];
                Session["OldEmail"] = Session["Email"];

                Session["UserId"] = userInfo.UserId;
                Session["UserName"] = userInfo.UserName;
                Session["RoleName"] = userInfo.OrgType;
                Session["currentOrgId"] = userInfo.OrgId;
                Session["LoginOrganizationName"] = userInfo.OrgName;
                Session["Email"] = userInfo.Email;

                setUpMenus(userInfo.OrgType);
            }
            return RedirectToAction("DashBoard");

        }

        [SessionExpire]
        public ActionResult IncognitoUser()
        {
            var IncognitoEmail = Session["OldEmail"].ToString();
            if (string.IsNullOrEmpty(IncognitoEmail))
            {
                return RedirectToAction("LogOut");
            }
            UserInfo userInfo = _sysUser.GetUserInfo(IncognitoEmail);

            Session["OldUserName"] = "";
            Session["OldEmail"] = "";

            Session["UserId"] = userInfo.UserId;
            Session["UserName"] = userInfo.UserName;
            Session["RoleName"] = userInfo.OrgType;
            Session["currentOrgId"] = userInfo.OrgId;
            Session["LoginOrganizationName"] = userInfo.OrgName;
            Session["Email"] = userInfo.Email;

            setUpMenus(userInfo.OrgType);

            return RedirectToAction("DashBoard");
        }
        [HttpPost]
        [SessionExpire]
        public ActionResult BulkUserInsertion(FormCollection form, HttpPostedFileBase file)
        {
            string result = new StreamReader(file.InputStream).ReadToEnd();
            var OrgId = Convert.ToInt64(form["OrgId"]);
            var RoleId = form["RoleID"];
            var DepartmentId = -1;
            if (!string.IsNullOrEmpty(form["DepartmentId"]))
                DepartmentId = Convert.ToInt32(form["DepartmentId"]);

            var Data = _sysUser.UserInsertion(result, OrgId, RoleId, DepartmentId);
            return Json(Data, JsonRequestBehavior.AllowGet);
        }
        [SessionExpire]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "MyAccount", sideMenuName = "Departments", ViewName = "AddDepartments")]

        public ActionResult AddDepartments()
        {
            return View();
        }
        [HttpPost]
        public ActionResult GetDepartments(DataSourceRequest request, string filters)
        {
            var Departments = _sysUser.GetAllDepartments(request, filters);
            return Json(Departments, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public string UpdateDepartment(int Id, string Name, long ClientId)
        {
            var Department = _sysUser.UpdateDepartmentName(Id, Name);
            return Department;

        }
        public string AddDepartment(string Name, long ClientID)
        {
            var Department = _sysUser.AddDepartment(Name, ClientID);
            return Department;

        }
        [SessionExpire]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "MyAccount", sideMenuName = "InvitationTemplates", ViewName = "AddInvitationTemplates")]

        public ActionResult AddInvitationTemplates()
        {
            var Templates = _sysUser.GetAllEmailTemplates();
            List<SelectListItem> EmailTemplates = new List<SelectListItem>();
            Templates.ForEach(s => EmailTemplates.Add(new SelectListItem() { Text = s.Key, Value = s.Value }));

            ViewBag.EmailTemplate = EmailTemplates;
            return View();
        }
        [HttpPost]
        public ActionResult GetClientEmailTemplates(DataSourceRequest request, string filters)
        {
            var EmailTemplates = _sysUser.GetEmailtemplates(request, filters);
            return Json(EmailTemplates, JsonRequestBehavior.AllowGet);
        }
        public string UpdateEmailTemplate(FormCollection form)
        {

            return _sysUser.UpdateEmailTemplate(Convert.ToInt32(form["EmailTemplateID"]), Convert.ToInt64(form["ClientID"]), Convert.ToInt32(form["TemplateID"]), Convert.ToInt32(form["type"]));

        }
        [HttpPost]
        public ActionResult DeleteEmailTemplate(int ID)
        {
            return Json(_sysUser.DeleteEmailTemplate(ID), JsonRequestBehavior.AllowGet);
        }
        public string AddEmailTemplate(FormCollection form)
        {

            var Template = _sysUser.AddEmailTemplate(Convert.ToInt64(form["ClientID"]), Convert.ToInt32(form["EmailTemplateID"]), Convert.ToInt32(form["type"]));
            return Template;
        }
        [HttpPost]
        public ActionResult GetClientDepartments(long OrgId)
        {
            SelectListItem defaultItem = new SelectListItem();
            defaultItem.Text = "Select Department";
            defaultItem.Value = "";
            var ClientDepartments = _sysUser.GetClientDepartments(OrgId);
            var Departments = ClientDepartments.Select(r => new SelectListItem
            {
                Text = r.Key,

                Value = r.Value

            }).ToList();
            Departments.Insert(0, defaultItem);
            return Json(Departments, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetOrgUsers(Guid UserId)
        {
            var Users = _sysUser.GetOrganizationUsers(UserId);
            return Json(Users, JsonRequestBehavior.AllowGet);
        }
        public string UpdateUser(Guid NewUser, Guid CurrentUser, bool Isdelete)
        {
            var user = _sysUser.UpdateUser(NewUser, CurrentUser, Isdelete);
            if (user == "Error")
                return user;
            if (user == "updated Successfully") return "Updated Successfully";
            if (Session["RoleName"].ToString().Equals("Admin"))
                return "../SystemUsers/SearchUser";
            else
                return "../Client/ClientOrVendorUsersList";
        }
        public string Updatepassword(Guid UserId, string Password)
        {
            string strPassword = Utilities.EncryptionDecryption.EncryptData(Password);
            _sysUser.UpdateUserPassword(strPassword, UserId);
            return "Success";
        }
        [SessionExpire]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "MyAccount", sideMenuName = "Invitation CC Setup", ViewName = "AddInviteNotifications")]

        public ActionResult AddInviteNotifications()
        {
            ViewBag.Type = (int)ClientNotificationType.Inviatation;
            return View();
        }
        public ActionResult GetClientUsersData(long ClientId)
        {

            return Json(_sysUser.GetClientUserEmails(ClientId, ""), JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetOrganizationUsers(long OrgId)
        {
            var users = _sysUser.GetClientUserEmails(OrgId, "");
            users.Add(new Dropdown { UserId = null, Email = "", Name = LocalConstants.Allstr });

            return Json(users, JsonRequestBehavior.AllowGet);
        }
        public ActionResult SaveClientInvitaionEmails(string ClientUserEmails, string UserIds, long ClientId, long? Id, int Type)
        {
            return Json(_sysUser.SaveClientInvitaionEmails(ClientUserEmails, UserIds, ClientId, Id, Type), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetClientUsers(DataSourceRequest request, string filters, int Type)
        {

            var Departments = _sysUser.GetInviteUsers(request, filters, Type);
            return Json(Departments, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetClients()
        {
            return Json(_sysUser.NotificationClients(), JsonRequestBehavior.AllowGet);
        }
        public string DeleteClient(long Id)
        {
            return _sysUser.DeleteClientNotification(Id);
        }
        [SessionExpire]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "MyAccount", sideMenuName = "Notifications CC Setup", ViewName = "CronjobNotificationSetup")]
        public ActionResult CronjobNotificationSetup()
        {
            ViewBag.Type = (int)ClientNotificationType.Cronjob;
            return View();
        }
        [SessionExpire]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "MyAccount", sideMenuName = "Power BI Permissions", ViewName = "AddPowerBiPermissions")]

        public ActionResult AddPowerBiPermissions()
        {
            var reportNames = _sysUser.GetPowerBiReportNames();
            List<SelectListItem> Reports = new List<SelectListItem>();
            reportNames.ForEach(s => Reports.Add(new SelectListItem() { Text = s.Key, Value = s.Value.ToString() }));
            ViewBag.VBReportsList = Reports;
            return View();
        }
        [HttpPost]
        public ActionResult GetPowerBiUsers(DataSourceRequest request, string filters)
        {
            var powerbiUsers = _sysUser.GetAllPowerBiUserPermissions(request, filters);
            return Json(powerbiUsers, JsonRequestBehavior.AllowGet);
        }
        public ActionResult PostPowerBiUsersData(string UserIds, long? OrgId, long? Id, int Type, long ReportId)
        {
            return Json(_sysUser.PostPowerBiUsersData(UserIds, OrgId, Id, Type, ReportId), JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetOrganizations(int Type)
        {
            var orgs = _sysUser.GetOrganizations(Type);
            orgs.Insert(0, new KeyValuePair<string, string>(" ", LocalConstants.Allstr));
            return Json(orgs, JsonRequestBehavior.AllowGet);
        }
        public ActionResult DeletePowerBiUserPermission(long Id)
        {
            return Json(_sysUser.DeletePowerBiUserpermission(Id), JsonRequestBehavior.AllowGet);
        }
    }
}


