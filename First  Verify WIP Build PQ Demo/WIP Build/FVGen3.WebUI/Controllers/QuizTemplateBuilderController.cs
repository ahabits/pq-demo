﻿/*
Page Created Date:  10/07/2014
Created By: Kiran Talluri
Purpose: Creation of Quiz template
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FVGen3.WebUI.Annotations;
using FVGen3.Domain.Abstract;
using FVGen3.Domain.Concrete;
using FVGen3.Domain.Entities;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Web.Script.Serialization;
using FVGen3.WebUI.Constants;

namespace FVGen3.WebUI.Controllers
{
    public class QuizTemplateBuilderController : BaseController
    {
        EFDbContext entityDB;
        public QuizTemplateBuilderController()
        {
            entityDB = new EFDbContext();
        }

        [SessionExpire]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "QuestionnaireTool", sideMenuName = "QuizTemplateBuilder", ViewName = "QuizTemplatesAndSections")]
        public ActionResult QuizTemplatesAndSections()
        {
            var quizTemplates = entityDB.Templates.Where(tempRec => tempRec.TemplateType == 1 && tempRec.DefaultTemplate == false).OrderBy(rec => rec.TemplateName).ToList(); // Kiran on 03/13/2015

            ViewBag.VBVideoDocuments = entityDB.Document.Where(rec => rec.ReferenceType == "QuizVideos").ToList();
            return View(quizTemplates);
        }

        [HttpGet]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "QuestionnaireTool", sideMenuName = "QuizTemplateBuilder", ViewName = "AddOrEditCustomQuizTemplate")]
        public ActionResult AddOrEditCustomQuizTemplate(Guid? TemplateId)
        {
            LocalAddQuizTemplate template = new LocalAddQuizTemplate();
            template.QuizRetake = false;
            var firstOrDefault = entityDB.ClientTemplates.FirstOrDefault(rec => rec.TemplateID == TemplateId);
            ViewBag.readOnly = false;
            if (firstOrDefault != null)
            {
                var templateStatus = firstOrDefault.Templates.TemplateStatus;
                ViewBag.readOnly = templateStatus == 1;
            }

            if (TemplateId != null)
            {
                Templates mainTemp = entityDB.Templates.Find(TemplateId);
                template.TemplateName = mainTemp.TemplateName;
                template.Templateid = mainTemp.TemplateID;
                template.TemplateNote = mainTemp.TemplateNotes;
                template.ClientId = mainTemp.ClientTemplates.FirstOrDefault().ClientID + "";
                template.TemplateCode = mainTemp.TemplateCode;
                template.LanguageId = mainTemp.LanguageId;
                var clientTemplateId = entityDB.ClientTemplates.FirstOrDefault(rec => rec.TemplateID == TemplateId).ClientTemplateID;
                ClientTemplatesQuizDetails quizTemplateDetails = entityDB.ClientTemplatesQuizDetails.FirstOrDefault(rec => rec.ClientTemplateID == clientTemplateId);
                if (quizTemplateDetails != null)
                {
                    template.QuizPassRate = quizTemplateDetails.QuizPassRate;
                    template.QuizRetakeFee = quizTemplateDetails.QuziRetakeFee;
                    if (quizTemplateDetails.QuziRetakeFee != null)
                    {
                        template.QuizRetake = true;
                    }
                    template.WaiverFormContent = quizTemplateDetails.WaiverFormContent;
                    if(quizTemplateDetails.WaiverFormContent != null)
                    {
                        template.WaiverForm = true;
                    }
                    template.ClientQuizDetailsID = quizTemplateDetails.ClientQuizDetailsID;
                    template.NumberOfAttempts = quizTemplateDetails.NumberOfAttempts;

                    //Rajesh on 11/7/2014
                    template.PeriodLength = quizTemplateDetails.PeriodLength;
                    template.PeriodLengthUnit = quizTemplateDetails.PeriodLengthUnit;
                    //Ends<<<
                }
            }


            ViewBag.Clients = (from clients in entityDB.Organizations.Where(rec => rec.OrganizationType == "Client").ToList().OrderBy(rec => rec.Name)
                               select new SelectListItem()
                               {
                                   Text = clients.Name,
                                   Value = clients.OrganizationID + ""
                               }).ToList();
            ViewBag.Languages = (from languages in entityDB.Language.ToList().OrderBy(rec => rec.LanguageName)
                select new SelectListItem()
                {
                    Text = languages.LanguageName,
                    Value = languages.LanguageId + ""
                }).ToList();

            return View(template);
        }

        [HttpPost]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "QuestionnaireTool", sideMenuName = "QuizTemplateBuilder", ViewName = "AddOrEditCustomQuizTemplate")]
        public ActionResult AddOrEditCustomQuizTemplate(LocalAddQuizTemplate Form)
        {
            ViewBag.readOnly = false;
            ViewBag.Clients = (from clients in entityDB.Organizations.Where(rec => rec.OrganizationType == "Client").OrderBy(rec => rec.Name).ToList()
                               select new SelectListItem()
                               {
                                   Text = clients.Name,
                                   Value = clients.OrganizationID + ""
                               }).ToList();
            ViewBag.Languages = (from languages in entityDB.Language.ToList().OrderBy(rec => rec.LanguageName)
                select new SelectListItem()
                {
                    Text = languages.LanguageName,
                    Value = languages.LanguageId + ""
                }).ToList();
            // Kiran on 12/24/2014
            if (Form.NumberOfAttempts == 999)
            {
                ModelState.Remove("QuizRetake");
            }
            
            if (Form.WaiverForm == false)
                ModelState.Remove("WaiverFormContent");

            if (!ModelState.IsValid)
                return View(Form);
            Templates newTemplate = new Templates();
            if (Form.Templateid != null)//Update Template
            {
                newTemplate = entityDB.Templates.Find(Form.Templateid);
                newTemplate.TemplateName = Form.TemplateName;
                newTemplate.TemplateNotes = Form.TemplateNote;
                newTemplate.TemplateCode = Form.TemplateCode;
                newTemplate.LanguageId = Form.LanguageId;
                var quizClientTemplateDetails = entityDB.ClientTemplatesQuizDetails.Find(Form.ClientQuizDetailsID);
                // Kiran on 11/10/2014
                quizClientTemplateDetails.QuizPassRate = Form.QuizPassRate;
                if (Form.NumberOfAttempts == 999)
                {
                    quizClientTemplateDetails.QuizRetake = null;
                    quizClientTemplateDetails.QuziRetakeFee = null;
                }
                else
                {
                    quizClientTemplateDetails.QuizRetake = Form.QuizRetake;
                    if (Form.QuizRetake)
                        quizClientTemplateDetails.QuziRetakeFee = Form.QuizRetakeFee;
                }
                // Ends<<<
                quizClientTemplateDetails.NumberOfAttempts = Form.NumberOfAttempts;

                //Rajesh on 11/7/2014
                quizClientTemplateDetails.PeriodLength = Form.PeriodLength;
                quizClientTemplateDetails.PeriodLengthUnit = Form.PeriodLengthUnit;
                //Ends<<<
                quizClientTemplateDetails.WaiverForm = Form.WaiverForm;
                if (Form.WaiverForm)
                    quizClientTemplateDetails.WaiverFormContent = Form.WaiverFormContent;

                entityDB.Entry(quizClientTemplateDetails).State = EntityState.Modified;
                entityDB.Entry(newTemplate).State = EntityState.Modified;

                entityDB.SaveChanges();
            }
            else//Save Template
            {
                newTemplate.TemplateCode = Form.TemplateCode;
                newTemplate.TemplateName = Form.TemplateName;
                newTemplate.TemplateType = 1;
                newTemplate.TemplateNotes = Form.TemplateNote;
                newTemplate.DefaultTemplate = false;
                newTemplate.TemplateStatus = 0;
                newTemplate.LanguageId = Form.LanguageId;
                ClientTemplates clientTemplate = new ClientTemplates();
                clientTemplate.ClientID = Convert.ToInt64(Form.ClientId);
                clientTemplate.DisplayOrder = 1;
                clientTemplate.Visible = true;
                clientTemplate.DefaultTemplate = false;
                newTemplate.ClientTemplates = new List<ClientTemplates>();

                ClientTemplatesQuizDetails quizClientTemplateDetails = new ClientTemplatesQuizDetails();
                quizClientTemplateDetails.QuizPassRate = Form.QuizPassRate;
                // Kiran on 11/10/2014
                if (Form.NumberOfAttempts == 999)
                {
                    quizClientTemplateDetails.QuizRetake = null;
                    quizClientTemplateDetails.QuziRetakeFee = null;
                }
                else
                {
                    quizClientTemplateDetails.QuizRetake = Form.QuizRetake;
                    if (Form.QuizRetake)
                        quizClientTemplateDetails.QuziRetakeFee = Form.QuizRetakeFee;
                }
                // Ends<<<
                quizClientTemplateDetails.NumberOfAttempts = Form.NumberOfAttempts;

                //Rajesh on 11/7/2014
                quizClientTemplateDetails.PeriodLength = Form.PeriodLength;
                quizClientTemplateDetails.PeriodLengthUnit = Form.PeriodLengthUnit;
                //Ends<<<
                quizClientTemplateDetails.WaiverForm = Form.WaiverForm;
                if (Form.WaiverForm)
                    quizClientTemplateDetails.WaiverFormContent = Form.WaiverFormContent;
                newTemplate.ClientTemplates.Add(clientTemplate);

                entityDB.ClientTemplatesQuizDetails.Add(quizClientTemplateDetails);

                entityDB.Templates.Add(newTemplate);

                entityDB.SaveChanges();
            }
            return Redirect("PopUpCloseView");
        }

        public ActionResult PopUpCloseView()
        {
            return View();
        }

        [HttpGet]
        [SessionExpireForView]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "QuestionnaireTool", sideMenuName = "QuizTemplateBuilder", ViewName = "AddOrEditQuizSection")]
        public ActionResult AddOrEditQuizSection(Guid TemplateId, long TemplateSectionID)
        {
            LocalQuizSections section = new LocalQuizSections();
            section.Templateid = TemplateId;
            section.TemplateSectionID = TemplateSectionID;

            section.Permission = 3;

            ViewBag.Template = entityDB.Templates.Find(TemplateId);

            if (TemplateSectionID != -1)
            {
                var Tsection = entityDB.TemplateSections.Find(TemplateSectionID);
                section.VisibleToClient = Tsection.VisibleToClient == true;
                section.SectionName = Tsection.SectionName;
                section.SectionType = Tsection.TemplateSectionType == null ? 0 : (int)Tsection.TemplateSectionType;
                section.Visible = !(Tsection.Visible == true);

                section.Permission = 3;
                try
                {
                    section.Permission = Tsection.TemplateSectionsPermission.FirstOrDefault().VisibleTo;
                }
                catch { }
            }
            return View(section);
        }

        [HttpPost]
        [SessionExpireForView]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "QuestionnaireTool", sideMenuName = "QuizTemplateBuilder", ViewName = "AddOrEditQuizSection")]
        public ActionResult AddOrEditQuizSection(LocalQuizSections Form)
        {
            ViewBag.Template = entityDB.Templates.Find(Form.Templateid);
            if (!ModelState.IsValid)
                return View(Form);
            var videoSections = entityDB.TemplateSections.Where(rec => rec.TemplateID == Form.Templateid && rec.TemplateSectionType == 21).ToList(); // Sandhya on 05/08/2015
            var iFrameSections = entityDB.TemplateSections.Where(rec => rec.TemplateID == Form.Templateid && rec.TemplateSectionType == 22).ToList();

            TemplateSections section = new TemplateSections();
            if (Form.TemplateSectionID == -1)
            {
                section.Visible = true;
                section.SectionName = Form.SectionName;
                entityDB.TemplateSections.Add(section);
                section.TemplateSectionType = Form.SectionType;

                // Sandhya on 05/08/2015
                var questionSections = entityDB.TemplateSections.Where(rec => rec.TemplateID == Form.Templateid && rec.TemplateSectionType == 1).ToList();
                if (videoSections.Count == 0 && questionSections.Count == 0 && Form.SectionType == 1)
                    section.DisplayOrder = 2;
                else if ((videoSections.Count == 0 && iFrameSections.Count == 0) && (Form.SectionType == 21 || Form.SectionType == 22))
                    section.DisplayOrder = 1;
                else
                {
                    try
                    {
                        section.DisplayOrder = entityDB.Templates.Find(Form.Templateid).TemplateSections.Count() + 1;
                    }
                    catch
                    {
                        section.DisplayOrder = 1;
                    }
                }
                // Ends<<<
                TemplateSectionsPermission permission = new TemplateSectionsPermission();
                permission.VisibleTo = Form.Permission;
                section.TemplateSectionsPermission = new List<TemplateSectionsPermission>();
                section.TemplateSectionsPermission.Add(permission);

                if (Form.SectionType == 21)
                {
                    TemplateSubSections subSection = new TemplateSubSections();
                    subSection.SubSectionType = 2;
                    subSection.TemplateSectionID = section.TemplateSectionID;
                    subSection.DisplayOrder = 1;
                    subSection.Visible = true; // Kiran on 03/20/2015
                    subSection.SubSectionName = "Videos";
                    subSection.SubSectionTypeCondition = "QuizVideos";
                    entityDB.TemplateSubSections.Add(subSection);
                }
                if (Form.SectionType == 22)
                {
                    TemplateSubSections subSection = new TemplateSubSections();
                    subSection.SubSectionType = 2;
                    subSection.TemplateSectionID = section.TemplateSectionID;
                    subSection.DisplayOrder = 1;
                    subSection.Visible = true; // Kiran on 03/20/2015
                    subSection.SubSectionName = "IFrame";
                    subSection.SubSectionTypeCondition = "QuizIFrame";
                    entityDB.TemplateSubSections.Add(subSection);
                }
            }
            else//Update
            {
                section = entityDB.TemplateSections.Find(Form.TemplateSectionID);

                if (section.TemplateSectionsPermission == null || section.TemplateSectionsPermission.Count == 0)
                {
                    TemplateSectionsPermission permission = new TemplateSectionsPermission();
                    permission.VisibleTo = Form.Permission;
                    section.TemplateSectionsPermission = new List<TemplateSectionsPermission>();
                    section.TemplateSectionsPermission.Add(permission);
                }
                else
                {
                    section.TemplateSectionsPermission.FirstOrDefault().VisibleTo = Form.Permission;
                }

                entityDB.Entry(section).State = EntityState.Modified;
                section.Visible = !Form.Visible;
            }
            ViewBag.Template = entityDB.Templates.Find(Form.Templateid);
            section.SectionName = Form.SectionName;
            section.TemplateID = Form.Templateid;
            entityDB.SaveChanges();
            return Redirect("PopUpCloseView");
        }

        [HttpGet]
        public string DeleteQuizTemplate(Guid TemplateId)
        {
            var template = entityDB.Templates.Find(TemplateId);

            if (template.ClientTemplates != null)
            {
                foreach (var Ctemplate in template.ClientTemplates.ToList())
                {
                    if (Ctemplate.ClientTemplatesForBU != null)
                    {
                        foreach (var clientTemplateBU in Ctemplate.ClientTemplatesForBU.ToList())
                        {
                            entityDB.Entry(clientTemplateBU).State = EntityState.Deleted;
                        }
                    }
                    if (Ctemplate.ClientTemplatesQuizDetails != null)
                    {
                        var quizClienntTemplateDetails = entityDB.ClientTemplatesQuizDetails.Find(Ctemplate.ClientTemplatesQuizDetails.FirstOrDefault().ClientQuizDetailsID);
                        entityDB.Entry(quizClienntTemplateDetails).State = EntityState.Deleted;
                    }
                    entityDB.Entry(Ctemplate).State = EntityState.Deleted;
                }
            }
            if (template.TemplateSections != null)
                foreach (var Tsection in template.TemplateSections.ToList())
                {
                    if (Tsection.TemplateSectionsHeaderFooter != null)
                        foreach (var HandF in Tsection.TemplateSectionsHeaderFooter.ToList())
                        {
                            entityDB.Entry(HandF).State = EntityState.Deleted;
                        }
                    if (Tsection.TemplateSubSections != null)
                        foreach (var Ssection in Tsection.TemplateSubSections.ToList())
                        {
                            if (Ssection.Questions != null)
                                foreach (var question in Ssection.Questions.ToList())//Delete Questions
                                {
                                    if (question.QuestionColumnDetails != null)
                                        foreach (var Qcolumn in question.QuestionColumnDetails.ToList())
                                            entityDB.Entry(Qcolumn).State = EntityState.Deleted;
                                    if (question.QuestionsDependants != null)
                                    {
                                        foreach (var Qdependent in question.QuestionsDependants.ToList())
                                            entityDB.Entry(Qdependent).State = EntityState.Deleted;
                                    }
                                    entityDB.Entry(question).State = EntityState.Deleted;
                                }
                            if (Ssection.TemplateSubSectionsPermissions != null)
                            {
                                foreach (var SubSecPermission in Ssection.TemplateSubSectionsPermissions.ToList())
                                {
                                    if (SubSecPermission.TemplateSubSectionsUserPermissions != null)
                                    {
                                        foreach (var SubSecUserPermission in SubSecPermission.TemplateSubSectionsUserPermissions.ToList())
                                        {
                                            entityDB.Entry(SubSecUserPermission).State = EntityState.Deleted;
                                        }
                                    }
                                    entityDB.Entry(SubSecPermission).State = EntityState.Deleted;
                                }
                                if (Ssection.TemplateSubSectionsNotifications != null)
                                    foreach (var notification in Ssection.TemplateSubSectionsNotifications.ToList())
                                    {
                                        entityDB.Entry(notification).State = EntityState.Deleted;
                                    }
                            }

                            if (entityDB.Document.Where(rec => (rec.ReferenceType == "QuizVideos" || rec.ReferenceType == "QuizAlternateFormats") && rec.ReferenceRecordID == Ssection.SubSectionID).ToList() != null) // kiran on 12/17/2014
                            {
                                foreach (var document in entityDB.Document.Where(rec => (rec.ReferenceType == "QuizVideos" || rec.ReferenceType == "QuizAlternateFormats") && rec.ReferenceRecordID == Ssection.SubSectionID).ToList()) // kiran on 12/17/2014
                                {
                                    entityDB.Entry(document).State = EntityState.Deleted;

                                    var dotIndex = document.DocumentName.LastIndexOf('.');
                                    var extension = document.DocumentName.Substring(dotIndex);
                                    Directory.CreateDirectory(Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["TrainingVideosPath"])));
                                    var path = Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["TrainingVideosPath"]), document.DocumentId + extension);
                                    if (path != null)
                                    {
                                        System.IO.File.Delete(path);
                                    }
                                }
                            }
                            entityDB.Entry(Ssection).State = EntityState.Deleted;
                        }
                    if (Tsection.TemplateSectionsPermission != null)
                    {
                        foreach (var permission in Tsection.TemplateSectionsPermission.ToList())
                        {
                            entityDB.Entry(permission).State = EntityState.Deleted;
                        }
                    }
                    entityDB.Entry(Tsection).State = EntityState.Deleted;
                }
            entityDB.Entry(template).State = EntityState.Deleted;
            entityDB.SaveChanges();
            return "DeleteSuccess";
        }

        public string DeleteQuizSection(long TemplateSectionID)
        {
            var Tsection = entityDB.TemplateSections.Find(TemplateSectionID);

            foreach (var HandF in Tsection.TemplateSectionsHeaderFooter.ToList())
            {
                entityDB.Entry(HandF).State = EntityState.Deleted;
            }
            foreach (var Ssection in Tsection.TemplateSubSections.ToList())//Delete SubSections
            {
                foreach (var question in Ssection.Questions.ToList())//Delete Questions
                {
                    if (question.QuestionColumnDetails != null)
                        foreach (var Qcolumn in question.QuestionColumnDetails.ToList())
                            entityDB.Entry(Qcolumn).State = EntityState.Deleted;

                    entityDB.Entry(question).State = EntityState.Deleted;
                }
                if (Ssection.TemplateSubSectionsPermissions != null)
                {
                    foreach (var SubSecPermission in Ssection.TemplateSubSectionsPermissions.ToList())
                    {
                        if (SubSecPermission.TemplateSubSectionsUserPermissions != null)
                        {
                            foreach (var SubSecUserPermission in SubSecPermission.TemplateSubSectionsUserPermissions.ToList())
                            {
                                entityDB.Entry(SubSecUserPermission).State = EntityState.Deleted;
                            }
                        }
                        entityDB.Entry(SubSecPermission).State = EntityState.Deleted;
                    }
                }
                if (entityDB.Document.Where(rec => (rec.ReferenceType == "QuizVideos" || rec.ReferenceType == "QuizAlternateFormats") && rec.ReferenceRecordID == Ssection.SubSectionID).ToList() != null) // kiran on 12/17/2014
                {
                    foreach (var document in entityDB.Document.Where(rec => (rec.ReferenceType == "QuizVideos" || rec.ReferenceType == "QuizAlternateFormats") && rec.ReferenceRecordID == Ssection.SubSectionID).ToList()) // kiran on 12/17/2014
                    {
                        entityDB.Entry(document).State = EntityState.Deleted;

                        var dotIndex = document.DocumentName.LastIndexOf('.');
                        var extension = document.DocumentName.Substring(dotIndex);
                        Directory.CreateDirectory(Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["TrainingVideosPath"])));
                        var path = Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["TrainingVideosPath"]), document.DocumentId + extension);
                        if (path != null)
                        {
                            System.IO.File.Delete(path);
                        }
                    }
                }
                entityDB.Entry(Ssection).State = EntityState.Deleted;
            }

            if (Tsection.TemplateSectionsPermission != null)
            {
                foreach (var permission in Tsection.TemplateSectionsPermission.ToList())
                {
                    entityDB.Entry(permission).State = EntityState.Deleted;
                }
            }
            //Ends<<<
            entityDB.Entry(Tsection).State = EntityState.Deleted;
            entityDB.SaveChanges();
            return "OK";
        }
        //<<<Ends
        [SessionExpire]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "QuestionnaireTool", sideMenuName = "QuizTemplateBuilder", ViewName = "QuizSubSectionView")]
        public ActionResult QuizSubSectionView(long SectionId)
        {

            LocalQuizTemplateBuilder form = new LocalQuizTemplateBuilder();
            form.SectionId = SectionId;
            var Header = entityDB.TemplateSectionsHeaderFooter.FirstOrDefault(rec => rec.TemplateSectionId == SectionId && rec.PositionType == 0);
            if (Header != null)
            {
                form.SectionHeaderId = Header.HeaderFooterId;
                form.SectionHeader = Header.PostionContent;
            }
            var Footer = entityDB.TemplateSectionsHeaderFooter.FirstOrDefault(rec => rec.TemplateSectionId == SectionId && rec.PositionType == 1);
            if (Footer != null)
            {
                form.SectionFooterId = Footer.HeaderFooterId;
                form.SectionFooter = Footer.PostionContent;
            }
            var Section = entityDB.TemplateSections.Find(SectionId);
            var VideoSubSectionId = Section.TemplateSubSections.FirstOrDefault(rec => rec.SubSectionTypeCondition == "QuizVideos").SubSectionID;
            Document saveVideo = entityDB.Document.FirstOrDefault(rec => rec.ReferenceRecordID == VideoSubSectionId && rec.ReferenceType == "QuizVideos");
            try
            {
                var AlternateSubSectionId = Section.TemplateSubSections.FirstOrDefault(rec => rec.SubSectionTypeCondition == "QuizAlternateFormats").SubSectionID;
                ViewBag.AlternateTraining = entityDB.Document.Where(rec => rec.ReferenceRecordID == AlternateSubSectionId && rec.ReferenceType == "QuizAlternateFormats").ToList();
            }
            catch { }
            try
            {
                form.QuizVideo = saveVideo.DocumentName;
                form.TempQuizVideo = saveVideo.DocumentName;
            }
            catch { }
            ViewBag.Section = Section;
            return View(form);
            //Ends<<<
        }

        [HttpPost]
        [SessionExpire]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "QuestionnaireTool", sideMenuName = "QuizTemplateBuilder", ViewName = "QuizSubSectionView")]
        [ValidateInput(false)]
        public ActionResult QuizSubSectionView(LocalQuizTemplateBuilder form)
        {

            var Section = entityDB.TemplateSections.Find(form.SectionId);
            var ClientId = Section.Templates.ClientTemplates.FirstOrDefault().ClientID;
            var SubSectionId = Section.TemplateSubSections.FirstOrDefault(rec => rec.SubSectionTypeCondition == "QuizVideos").SubSectionID;
            try
            {
                var AlternateSubSectionId = Section.TemplateSubSections.FirstOrDefault(rec => rec.SubSectionTypeCondition == "QuizAlternateFormats").SubSectionID;
                ViewBag.AlternateTraining = entityDB.Document.Where(rec => rec.ReferenceRecordID == AlternateSubSectionId && rec.ReferenceType == "QuizAlternateFormats").ToList();
            }
            catch { }
            ViewBag.Section = Section;
            var Header = entityDB.TemplateSectionsHeaderFooter.FirstOrDefault(rec => rec.TemplateSectionId == form.SectionId && rec.PositionType == 0);
            if (Header == null)
            {
                Header = new TemplateSectionsHeaderFooter();
                Header.TemplateSectionId = form.SectionId;
                Header.PositionType = 0;
                entityDB.TemplateSectionsHeaderFooter.Add(Header);
            }
            else
            {
                entityDB.Entry(Header).State = EntityState.Modified;
            }
            Header.PostionContent = form.SectionHeader;
            var Footer = entityDB.TemplateSectionsHeaderFooter.FirstOrDefault(rec => rec.TemplateSectionId == form.SectionId && rec.PositionType == 1);
            if (Footer == null)
            {
                Footer = new TemplateSectionsHeaderFooter();
                Footer.TemplateSectionId = form.SectionId;
                Footer.PositionType = 1;
                entityDB.TemplateSectionsHeaderFooter.Add(Footer);
            }
            else
            {
                entityDB.Entry(Footer).State = EntityState.Modified;
            }
            Footer.PostionContent = form.SectionFooter;
            entityDB.SaveChanges();
            form.SectionHeaderId = Header.HeaderFooterId;
            form.SectionFooterId = Footer.HeaderFooterId;


            if (Request.Files != null && Request.Files.Count > 0)
            {
                for (int i = 0; i < Request.Files.Count; i++)
                {
                    if (Request.Files[i].ContentLength > 0)
                    {
                        Document saveVideo = entityDB.Document.FirstOrDefault(rec => rec.ReferenceRecordID == SubSectionId && rec.ReferenceType == "QuizVideos");
                        if (saveVideo == null)
                        {
                            saveVideo = new Document();
                            saveVideo.DocumentTypeId = entityDB.DocumentType.FirstOrDefault(rec => rec.DocumentTypeName.ToLower() == "quiz videos").DocumentTypeId;
                            saveVideo.OrganizationId = ClientId;
                            saveVideo.ReferenceType = "QuizVideos";
                            saveVideo.ReferenceRecordID = SubSectionId;
                            saveVideo.DocumentName = Request.Files[i].FileName;
                            saveVideo.Uploaded = DateTime.Now;
                            entityDB.Document.Add(saveVideo);
                            entityDB.SaveChanges();
                        }
                        // Rajesh on 12/22/2014 
                        else
                        {
                            saveVideo.DocumentName = Request.Files[i].FileName;
                            entityDB.Entry(saveVideo).State = EntityState.Modified;
                            entityDB.SaveChanges();

                        }
                        // Ends<<<
                        // To save the physical file
                        var fileName = Path.GetFileName(Request.Files[i].FileName);
                        var extension = Path.GetExtension(Request.Files[i].FileName);
                        form.QuizVideo = fileName;
                        Directory.CreateDirectory(Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["TrainingVideosPath"])));
                        var path = Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["TrainingVideosPath"]), saveVideo.DocumentId + extension);
                        Request.Files[i].SaveAs(path);
                    }
                    else
                    {
                        form.QuizVideo = form.TempQuizVideo;
                    }

                }
            }
            return Redirect("QuizSubSectionView?SectionId=" + form.SectionId);
            //Ends<<<
        }


        [SessionExpire]
        [HeaderAndSidebar(headerName = "QuestionnaireTool", sideMenuName = "QuizTemplateBuilder", ViewName = "QuizSubSectionView")]
        public ActionResult QuizSubSectionIFrameView(long SectionId)
        {

            LocalQuizTemplateBuilderIFrame form = new LocalQuizTemplateBuilderIFrame();

            form.SectionId = SectionId;
            var Header = entityDB.TemplateSectionsHeaderFooter.FirstOrDefault(rec => rec.TemplateSectionId == SectionId && rec.PositionType == 0);
            if (Header != null)
            {
                form.SectionHeaderId = Header.HeaderFooterId;
                form.SectionHeader = Header.PostionContent;
            }



            var Section = entityDB.TemplateSections.Find(SectionId);
            var SubSection = Section.TemplateSubSections.FirstOrDefault(rec => rec.SubSectionTypeCondition == "QuizIFrame");
            if (SubSection.MetaJSON != null)
            {
                LocalQuizIFrameMeta metaData = (LocalQuizIFrameMeta)new JavaScriptSerializer().Deserialize<LocalQuizIFrameMeta>(SubSection.MetaJSON);
                form.QuizIFrameURL = metaData.IFrameURL;
                form.QuizIFrameWidth = metaData.Width;
                form.QuizIFrameHeight = metaData.Height;
            }


            var Footer = entityDB.TemplateSectionsHeaderFooter.FirstOrDefault(rec => rec.TemplateSectionId == SectionId && rec.PositionType == 1);
            if (Footer != null)
            {
                form.SectionFooterId = Footer.HeaderFooterId;
                form.SectionFooter = Footer.PostionContent;
            }

            //var Section = entityDB.TemplateSections.Find(SectionId);
            //var VideoSubSectionId = Section.TemplateSubSections.FirstOrDefault(rec => rec.SubSectionTypeCondition == "QuizIFrame").SubSectionID;

            ViewBag.Section = Section;
            return View(form);
            //Ends<<<
        }
        [HttpPost]
        [SessionExpire]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "QuestionnaireTool", sideMenuName = "QuizTemplateBuilder", ViewName = "QuizSubSectionView")]
        [ValidateInput(false)]
        public ActionResult QuizSubSectionIFrameView(LocalQuizTemplateBuilderIFrame form)
        {

            var Section = entityDB.TemplateSections.Find(form.SectionId);
            var ClientId = Section.Templates.ClientTemplates.FirstOrDefault().ClientID;


            ViewBag.Section = Section;
            var Header = entityDB.TemplateSectionsHeaderFooter.FirstOrDefault(rec => rec.TemplateSectionId == form.SectionId && rec.PositionType == 0);
            if (Header == null)
            {
                Header = new TemplateSectionsHeaderFooter();
                Header.TemplateSectionId = form.SectionId;
                Header.PositionType = 0;
                entityDB.TemplateSectionsHeaderFooter.Add(Header);
            }
            else
            {
                entityDB.Entry(Header).State = EntityState.Modified;
            }
            Header.PostionContent = form.SectionHeader;

            //IFrame Data

            LocalQuizIFrameMeta metaData = new LocalQuizIFrameMeta();
            metaData.Height = form.QuizIFrameHeight;
            metaData.Width = form.QuizIFrameWidth;
            metaData.IFrameURL = form.QuizIFrameURL;

            var SubSection = Section.TemplateSubSections.FirstOrDefault(rec => rec.SubSectionTypeCondition == "QuizIFrame");
            SubSection.MetaJSON = new JavaScriptSerializer().Serialize(metaData);
            entityDB.Entry(SubSection).State = EntityState.Modified;


            var Footer = entityDB.TemplateSectionsHeaderFooter.FirstOrDefault(rec => rec.TemplateSectionId == form.SectionId && rec.PositionType == 1);
            if (Footer == null)
            {
                Footer = new TemplateSectionsHeaderFooter();
                Footer.TemplateSectionId = form.SectionId;
                Footer.PositionType = 1;
                entityDB.TemplateSectionsHeaderFooter.Add(Footer);
            }
            else
            {
                entityDB.Entry(Footer).State = EntityState.Modified;
            }
            Footer.PostionContent = form.SectionFooter;
            entityDB.SaveChanges();
            form.SectionHeaderId = Header.HeaderFooterId;
            form.SectionFooterId = Footer.HeaderFooterId;



            return Redirect("QuizSubSectionIFrameView?SectionId=" + form.SectionId);
            //Ends<<<
        }
        //[SessionExpire]
        //[HeaderAndSidebar(headerName = "QuestionnaireTool", sideMenuName = "QuizTemplateBuilder", ViewName = "QuizSubSectionView")]
        //public ActionResult QuizSubSectionView(long SectionId)
        //{
        //    ViewBag.Clients = entityDB.Organizations.Where(rec => rec.OrganizationType == "Client").ToList();

        //    var Section = entityDB.TemplateSections.Find(SectionId);
        //    if (Section.Templates.DefaultTemplate == false)
        //    {
        //        var clientId = Section.Templates.ClientTemplates.FirstOrDefault().ClientID;
        //        ViewBag.Clients = entityDB.Organizations.Where(rec => rec.OrganizationType == "Client" && rec.OrganizationID == clientId).ToList();
        //    }

        //    //To Change Side Menu Selection>>>
        //    if (Section.Templates.DefaultTemplate == null || Section.Templates.DefaultTemplate == false)
        //    {
        //        List<LocalHeaderMenuModel> headerMenu = (List<LocalHeaderMenuModel>)ViewBag.headerMenus;
        //        headerMenu.FirstOrDefault(record => record.menuName == "QuestionnaireTool").sideMenus.ForEach(record => record.isChecked = false);
        //        headerMenu.FirstOrDefault(record => record.menuName == "QuestionnaireTool").sideMenus.FirstOrDefault(record => record.menuName == "QuizTemplateBuilder").isChecked = true;
        //        ViewBag.headerMenus = headerMenu;
        //    }
        //    //Ends<<<
        //    //To Handle Dropdown Change in subsection view

        //    ViewBag.Section = Section;
        //    ViewBag.SectionId = SectionId;
        //    ViewBag.TemplateId = Section.Templates.TemplateID;
        //    ViewBag.VBClientTemplateId = Section.Templates.ClientTemplates.FirstOrDefault(rec => rec.TemplateID == Section.Templates.TemplateID).ClientTemplateID;
        //    ViewBag.SubSections = entityDB.TemplateSubSections.Where(rec => rec.TemplateSectionID == SectionId).ToList();

        //    ViewBag.Header = Section.TemplateSectionsHeaderFooter.FirstOrDefault(rec => rec.PositionType == 0);
        //    ViewBag.Footer = Section.TemplateSectionsHeaderFooter.FirstOrDefault(rec => rec.PositionType == 1);

        //    ViewBag.SectionName = Section.SectionName;
        //    ViewBag.TemplateName = Section.Templates.TemplateName;
        //    //ViewBag.VBQuizVideoURL = Section.Templates.ClientTemplates.FirstOrDefault(rec => rec.TemplateID == Section.TemplateID).QuizVideoURL;
        //    ViewBag.Templates = (from templates in entityDB.Templates.Where(rec => rec.TemplateType == 1).ToList()
        //                         select new SelectListItem()
        //                         {
        //                             Text = templates.TemplateName,
        //                             Value = templates.TemplateID + "",
        //                             Selected = templates.TemplateID == Section.Templates.TemplateID
        //                         }).ToList();

        //    ViewBag.VBVideoOrAlternateTrainingFormats = entityDB.Document.Where(rec => rec.ReferenceType == "QuizVideos" || rec.ReferenceType == "QuizAlternateFormats").ToList();
        //    ViewBag.VBAlternateFormatSubSection = entityDB.TemplateSubSections.FirstOrDefault(rec => rec.TemplateSectionID == SectionId && rec.SubSectionTypeCondition == "QuizAlternateFormats");
        //    return View();
        //    //Ends<<<
        //}

        [HttpPost]
        public ActionResult GetQuizSections(Guid TemplateId, long SectionId)
        {

            var curretUserOrgBusUnits = (from sec in entityDB.TemplateSections.Where(rec => rec.TemplateID == TemplateId).ToList()
                                         select new SelectListItem
                                         {
                                             Text = sec.SectionName,
                                             Value = sec.TemplateSectionID + "",
                                             Selected = SectionId == sec.TemplateSectionID
                                         }).ToList();



            return Json(curretUserOrgBusUnits);
        }

        //[SessionExpireForView]
        //[HttpGet]
        //[HeaderAndSidebar(headerName = "QuestionnaireTool", sideMenuName = "QuizTemplateBuilder", ViewName = "AddOrEditQuizSubSections")]
        //public ActionResult AddOrEditQuizSubSections(long SectionId, long subSecId)
        //{
        //    var section = entityDB.TemplateSections.Find(SectionId);
        //    ViewBag.sectionType = section.TemplateSectionType;
        //    ViewBag.isDefault = section.Templates.DefaultTemplate;
        //    LocalQuizSubSection subSection = new LocalQuizSubSection();
        //    subSection.PermissionType = 0;
        //    subSection.SectionId = SectionId;
        //    ViewBag.Section = section;
        //    subSection.SubSectionType = "0";
        //    subSection.NotificationExist = false;
        //    subSection.SubSecId = subSecId;

        //    if (subSecId != -1)
        //    {
        //        var subSec = entityDB.TemplateSubSections.Find(subSecId);

        //        if (subSec.TemplateSubSectionsPermissions == null || subSec.TemplateSubSectionsPermissions.Count == 0)
        //            subSection.PermissionType = 0;
        //        else
        //            try { subSection.PermissionType = subSec.TemplateSubSectionsPermissions.FirstOrDefault(rec => rec.PermissionFor == "Client").PermissionType; }
        //            catch { }

        //        if (subSec.SubSectionType != null)
        //            subSection.SubSectionType = subSec.SubSectionType + "";
        //        subSection.SubSectionTypeCondition = subSec.SubSectionTypeCondition;
        //        switch ((int)subSec.SubSectionType)
        //        {
        //            case 1: subSection.Dynamic_Condition = subSec.SubSectionTypeCondition; break;
        //            case 2: subSection.Predefined_Condition = subSec.SubSectionTypeCondition; break;
        //            case 3: subSection.Reporting_Condition = subSec.SubSectionTypeCondition; break;
        //        }
        //        subSection.SubSectionName = subSec.SubSectionName;
        //        subSection.SubSectionNote = subSec.SectionNotes;
        //        subSection.NoOfColumns = (int)subSec.NoOfColumns;
        //        subSection.Visible = !(bool)subSec.Visible;
        //    }
        //    return View(subSection);
        //}

        //[HttpPost]
        //[SessionExpireForView]
        //[HeaderAndSidebar(headerName = "QuestionnaireTool", sideMenuName = "QuizTemplateBuilder", ViewName = "AddOrEditQuizSubSections")]
        //public ActionResult AddOrEditQuizSubSections(LocalSubSection Form)
        //{
        //    TemplateSubSections subSection = new TemplateSubSections();
        //    int displayOrder = 1;
        //    var TSection = entityDB.TemplateSections.Find(Form.SectionId);

        //    try { displayOrder = TSection.TemplateSubSections.Count() + 1; }
        //    catch { }

        //    if (Form.SubSecId != -1)
        //    {
        //        subSection = entityDB.TemplateSubSections.Find(Form.SubSecId);
        //        subSection.Visible = !Form.Visible;
        //        if (subSection.TemplateSubSectionsPermissions == null || subSection.TemplateSubSectionsPermissions.Count == 0)
        //        {
        //            CreateTemplateSubSecPermission(Form.PermissionType, subSection, TSection);
        //        }
        //    }
        //    else//Insertion
        //    {
        //        subSection.DisplayOrder = displayOrder;
        //        subSection.Visible = true;

        //        subSection.SubSectionType = Convert.ToInt32(Form.SubSectionType);
        //        subSection.TemplateSectionID = Form.SectionId;

        //        CreateTemplateSubSecPermission(Form.PermissionType, subSection, TSection);
        //    }

        //    subSection.SubSectionName = Form.SubSectionName;
        //    subSection.NoOfColumns = Form.NoOfColumns;
        //    subSection.SectionNotes = Form.SubSectionNote;
        //    if (Form.SubSecId == -1)
        //        entityDB.TemplateSubSections.Add(subSection);
        //    else
        //        entityDB.Entry(subSection).State = EntityState.Modified;
        //    entityDB.SaveChanges();
        //    return Redirect("PopUpCloseView");
        //}

        private static void CreateTemplateSubSecPermission(int permissionType, TemplateSubSections subSection, TemplateSections TSection)
        {
            try
            {
                var VisibleTo = 3;
                try
                {
                    VisibleTo = VisibleTo = TSection.TemplateSectionsPermission.FirstOrDefault().VisibleTo;
                }
                catch { }

                TemplateSubSectionsPermissions permission = new TemplateSubSectionsPermissions();
                permission.SectionsPermissionId = TSection.TemplateSectionsPermission.FirstOrDefault().SectionsPermissionId;
                permission.PermissionType = 0;
                permission.PermissionFor = "Admin";
                subSection.TemplateSubSectionsPermissions = new List<TemplateSubSectionsPermissions>();
                subSection.TemplateSubSectionsPermissions.Add(permission);

                permission = new TemplateSubSectionsPermissions();
                permission.SectionsPermissionId = TSection.TemplateSectionsPermission.FirstOrDefault().SectionsPermissionId;
                permission.PermissionType = permissionType;
                permission.PermissionFor = "Client";

                subSection.TemplateSubSectionsPermissions.Add(permission);
                permission = new TemplateSubSectionsPermissions();
                permission.SectionsPermissionId = TSection.TemplateSectionsPermission.FirstOrDefault().SectionsPermissionId;
                permission.PermissionType = permissionType;
                permission.PermissionFor = OrganizationType.SuperClient;

                subSection.TemplateSubSectionsPermissions.Add(permission);

                permission = new TemplateSubSectionsPermissions();
                permission.SectionsPermissionId = TSection.TemplateSectionsPermission.FirstOrDefault().SectionsPermissionId;
                permission.PermissionType = permissionType;
                permission.PermissionFor = "Vendor";

                subSection.TemplateSubSectionsPermissions.Add(permission);
            }
            catch { }
        }

        [HttpPost]
        public ActionResult AddQuizHeaderOrFooter(long headerFooterId, long SectionId, int type, string title, string content)
        {
            TemplateSectionsHeaderFooter headerFooter = new TemplateSectionsHeaderFooter();
            try
            {
                content = HttpUtility.HtmlDecode(content);
                title = HttpUtility.HtmlDecode(title);
                if (headerFooterId != -1)
                {
                    headerFooter = entityDB.TemplateSectionsHeaderFooter.Find(headerFooterId);
                }
                headerFooter.TemplateSectionId = SectionId;
                headerFooter.PositionType = type;
                headerFooter.PositionTitle = title.Trim();
                headerFooter.PostionContent = content.Trim();
                if (headerFooterId == -1)
                    entityDB.TemplateSectionsHeaderFooter.Add(headerFooter);
                else
                    entityDB.Entry(headerFooter).State = EntityState.Modified;
                entityDB.SaveChanges();
            }
            catch (Exception ee) { return Json(ee.Message); }
            var Section = entityDB.TemplateSections.Find(SectionId);
            var data = entityDB.TemplateSectionsHeaderFooter.ToList();
            return Json(headerFooter.HeaderFooterId + "");
        }


        public string DeleteQuizHeaderOrFooter(long headerFooterId)
        {
            entityDB.TemplateSectionsHeaderFooter.Remove(entityDB.TemplateSectionsHeaderFooter.Find(headerFooterId));
            entityDB.SaveChanges();
            return "Ok";
        }

        [SessionExpire]
        [HttpGet]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "QuestionnaireTool", sideMenuName = "QuizTemplateBuilder", ViewName = "QuizQuestionsList")]
        public ActionResult QuizQuestionsList(long SubSectionId, long SectionId)
        {
            var SubSection = entityDB.TemplateSubSections.Find(SubSectionId);
            var section = entityDB.TemplateSections.Find(SectionId);
            //To Change Side Menu Selection>>>
            if (section.Templates.DefaultTemplate == null || section.Templates.DefaultTemplate == false)
            {
                List<LocalHeaderMenuModel> headerMenu = (List<LocalHeaderMenuModel>)ViewBag.headerMenus;
                headerMenu.FirstOrDefault(record => record.menuName == "QuestionnaireTool").sideMenus.ForEach(record => record.isChecked = false);
                headerMenu.FirstOrDefault(record => record.menuName == "QuestionnaireTool").sideMenus.FirstOrDefault(record => record.menuName == "QuizTemplateBuilder").isChecked = true;
                ViewBag.headerMenus = headerMenu;
            }

            //Ends<<<

            LocalTrainingQuestions questions = new LocalTrainingQuestions();
            questions.isEdit = false;
            questions.TemplateStatus = section.Templates.TemplateStatus;
            questions.TemplateType = section.Templates.DefaultTemplate == true ? 1 : 0;//To Know Template Type is standard or Custom
            questions.SectionId = section.TemplateSectionID;
            questions.TemplateName = section.Templates.TemplateName;
            questions.SectionName = section.SectionName;
            questions.HeaderId = -1;
            questions.FooterId = -1;
            if (SubSectionId != -1)
            {
                questions.isEdit = true;
                questions.SubSectionName = SubSection.SubSectionName;
                questions.SubSectionType = SubSection.SubSectionType;
                questions.SubSectionID = SubSectionId;
                var subSectionHeader = entityDB.TemplateSectionsHeaderFooter.FirstOrDefault(rec => rec.TemplateSectionId == SectionId && rec.PositionType == 0);
                var subSectionFooter = entityDB.TemplateSectionsHeaderFooter.FirstOrDefault(rec => rec.TemplateSectionId == SectionId && rec.PositionType == 1);
                if (subSectionHeader != null)
                {
                    questions.HeaderId = subSectionHeader.HeaderFooterId;
                    questions.HeaderContent = HttpUtility.HtmlEncode(subSectionHeader.PostionContent);
                }
                if (subSectionFooter != null)
                {
                    questions.FooterId = subSectionFooter.HeaderFooterId;
                    questions.FooterContent = HttpUtility.HtmlEncode(subSectionFooter.PostionContent);
                }
            }


            ViewBag.QuestionsBankList = entityDB.QuestionsBank.Where(rec => rec.QuestionReference == 2 && rec.QuestionStatus == 1 && rec.UsedFor == 1 && rec.QuestionBankId != 1000).ToList();

            ViewBag.QuestionsList = entityDB.Questions.Where(rec => rec.SubSectionId == SubSectionId).OrderBy(rec => rec.DisplayOrder).ToList();
            return View(questions);
        }

        [SessionExpire]
        [HttpPost]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "QuestionnaireTool", sideMenuName = "QuizTemplateBuilder", ViewName = "QuizQuestionsList")]
        public ActionResult QuizQuestionsList(LocalTrainingQuestions form)
        {
            var subSection = entityDB.TemplateSubSections.Find(form.SubSectionID);
            var templateSection = entityDB.TemplateSections.Find(form.SectionId);
            int questionOrderNumber = 1;
            try { questionOrderNumber = subSection.Questions.Count() + 1; }
            catch { }
            form.TemplateStatus = templateSection.Templates.TemplateStatus;
            form.TemplateType = templateSection.Templates.DefaultTemplate == true ? 1 : 0;//To Know Template Type is standard or Custom
            form.SectionId = templateSection.TemplateSectionID;
            form.TemplateName = templateSection.Templates.TemplateName;
            form.SectionName = templateSection.SectionName;
            //if (form.isEdit == true)
            //{
            //    form.SubSectionName = subSection.SubSectionName;
            //    form.SubSectionType = subSection.SubSectionType;
            //}

            //Questions question;
            if (form.isEdit == false) // Insertion
            {
                var section = entityDB.TemplateSections.Find(form.SectionId);
                TemplateSubSections trainingSubSection = new TemplateSubSections();
                if (section.TemplateSectionType == 1)
                {
                    trainingSubSection.SubSectionType = 0;
                    trainingSubSection.SubSectionName = form.SubSectionName;
                    trainingSubSection.DisplayOrder = 1;
                    trainingSubSection.Visible = true;
                    trainingSubSection.TemplateSectionID = form.SectionId;
                    trainingSubSection.NoOfColumns = 1;
                    entityDB.TemplateSubSections.Add(trainingSubSection);
                    CreateTemplateSubSecPermission(0, trainingSubSection, section);
                }

                if (form.LocalQuestions != null)
                {
                    foreach (var newQuestion in form.LocalQuestions)
                    {
                        //if (newQuestion.QuestionId == 0)
                        //    continue;
                        Questions question = new Questions();
                        question.QuestionText = entityDB.QuestionsBank.Find(newQuestion.QuestionBankId).QuestionString;
                        question.SubSectionId = trainingSubSection.SubSectionID;
                        question.DisplayOrder = questionOrderNumber;
                        question.Visible = true;
                        question.NumberOfColumns = 1;

                        question.IsMandatory = true;
                        question.IsBold = true;
                        question.ResponseRequired = true;
                        question.DisplayResponse = true;
                        question.HasDependantQuestions = false;
                        question.QuestionBankId = newQuestion.QuestionBankId;
                        entityDB.Questions.Add(question);

                        QuestionColumnDetails questionColumns = new QuestionColumnDetails();
                        questionColumns.ColumnNo = 1;
                        questionColumns.DisplayType = 1;
                        questionColumns.QuestionControlTypeId = 7;
                        questionColumns.QuestionId = question.QuestionID;

                        entityDB.QuestionColumnDetails.Add(questionColumns);
                        entityDB.SaveChanges();
                    }
                }

                if (form.HeaderContent != null && form.HeaderContent != "")
                {
                    TemplateSectionsHeaderFooter addHeaderForTraining = new TemplateSectionsHeaderFooter();
                    addHeaderForTraining.PostionContent = HttpUtility.HtmlDecode(form.HeaderContent).Trim();
                    addHeaderForTraining.TemplateSectionId = form.SectionId;
                    addHeaderForTraining.PositionType = 0;
                    entityDB.TemplateSectionsHeaderFooter.Add(addHeaderForTraining);
                }
                if (form.FooterContent != null && form.FooterContent != "")
                {
                    TemplateSectionsHeaderFooter addHeaderForTraining = new TemplateSectionsHeaderFooter();
                    addHeaderForTraining.PostionContent = HttpUtility.HtmlDecode(form.FooterContent).Trim();
                    addHeaderForTraining.TemplateSectionId = form.SectionId;
                    addHeaderForTraining.PositionType = 1;
                    entityDB.TemplateSectionsHeaderFooter.Add(addHeaderForTraining);
                }
                entityDB.SaveChanges();
                return Redirect("QuizQuestionsList?SubSectionId=" + trainingSubSection.SubSectionID + "&SectionId=" + form.SectionId);
            }
            else // Edit mode
            {
                var subSectionToEdit = entityDB.TemplateSubSections.Find(form.SubSectionID);
                subSectionToEdit.SubSectionName = form.SubSectionName;

                // Kiran on 11/3/2014
                if (form.HeaderId != -1)
                {
                    var sectionHeaderToEdit = entityDB.TemplateSectionsHeaderFooter.FirstOrDefault(rec => rec.HeaderFooterId == form.HeaderId && rec.PositionType == 0);
                    sectionHeaderToEdit.PostionContent = HttpUtility.HtmlDecode(form.HeaderContent);

                    entityDB.Entry(sectionHeaderToEdit).State = EntityState.Modified;
                }
                else if (form.HeaderContent != null && form.HeaderContent != "")
                {
                    TemplateSectionsHeaderFooter addHeaderForTraining = new TemplateSectionsHeaderFooter();
                    addHeaderForTraining.PostionContent = HttpUtility.HtmlDecode(form.HeaderContent).Trim();
                    addHeaderForTraining.TemplateSectionId = form.SectionId;
                    addHeaderForTraining.PositionType = 0;
                    entityDB.TemplateSectionsHeaderFooter.Add(addHeaderForTraining);
                }
                if (form.FooterId != -1)
                {
                    var sectionFooterToEdit = entityDB.TemplateSectionsHeaderFooter.FirstOrDefault(rec => rec.HeaderFooterId == form.FooterId && rec.PositionType == 1);
                    sectionFooterToEdit.PostionContent = HttpUtility.HtmlDecode(form.FooterContent);

                    entityDB.Entry(sectionFooterToEdit).State = EntityState.Modified;

                }
                else if (form.FooterContent != null && form.FooterContent != "")
                {
                    TemplateSectionsHeaderFooter addHeaderForTraining = new TemplateSectionsHeaderFooter();
                    addHeaderForTraining.PostionContent = HttpUtility.HtmlDecode(form.FooterContent).Trim();
                    addHeaderForTraining.TemplateSectionId = form.SectionId;
                    addHeaderForTraining.PositionType = 1;
                    entityDB.TemplateSectionsHeaderFooter.Add(addHeaderForTraining);
                }
                // Ends<<<

                if (form.LocalQuestions != null)
                {
                    foreach (var newQuestion in form.LocalQuestions)
                    {
                        Questions question = new Questions();
                        question.QuestionText = entityDB.QuestionsBank.Find(newQuestion.QuestionBankId).QuestionString;
                        question.SubSectionId = form.SubSectionID;
                        question.DisplayOrder = questionOrderNumber;
                        question.Visible = true;
                        question.NumberOfColumns = 1;

                        question.IsMandatory = true;
                        question.IsBold = true;
                        question.ResponseRequired = true;
                        question.DisplayResponse = true;
                        question.HasDependantQuestions = false;
                        question.QuestionBankId = newQuestion.QuestionBankId;
                        entityDB.Questions.Add(question);

                        QuestionColumnDetails questionColumns = new QuestionColumnDetails();
                        questionColumns.ColumnNo = 1;
                        questionColumns.DisplayType = 1;
                        questionColumns.QuestionControlTypeId = 7;
                        questionColumns.QuestionId = question.QuestionID;

                        entityDB.QuestionColumnDetails.Add(questionColumns);
                        entityDB.SaveChanges();
                    }
                }
                entityDB.Entry(subSectionToEdit).State = EntityState.Modified;
                entityDB.SaveChanges();
            }

            return Redirect("QuizQuestionsList?SubSectionId=" + form.SubSectionID + "&SectionId=" + form.SectionId);
        }


        [HttpPost]
        public string AddQuizQuestionBank(string QuestionString)
        {
            var currentQuestionRecord = entityDB.QuestionsBank.FirstOrDefault(record => record.QuestionString.ToUpper() == QuestionString.ToUpper() && record.QuestionReference == 2 && record.UsedFor == 0); // Kiran on 7/3/2014
            if (currentQuestionRecord != null)
            {
                ViewBag.isQuestionExits = true;
                return "Question being added, exist in question bank";
            }
            QuestionsBank Question = new QuestionsBank();
            Question.QuestionString = QuestionString;
            Question.QuestionReference = 2;
            entityDB.QuestionsBank.Add(Question);
            Question.UsedFor = 1;
            Question.QuestionStatus = 1;

            entityDB.SaveChanges();

            return "Ok";
        }

        [HttpPost]
        public string DeleteQuizQuestions(string questionBankIds, long subSectionId)
        {
            if (questionBankIds == "1")//Delete Blank Questions
            {
                foreach (var question in entityDB.Questions.Where(rec => rec.QuestionBankId == 1 && rec.SubSectionId == subSectionId).ToList())
                {
                    //entityDB.Entry(question).State = EntityState.Deleted;
                    if (question.QuestionColumnDetails != null)
                        foreach (var Qcolumn in question.QuestionColumnDetails.ToList())
                            entityDB.Entry(Qcolumn).State = EntityState.Deleted;
                    if (question.QuestionsDependants != null)
                    {
                        foreach (var Qdependent in question.QuestionsDependants.ToList())
                            entityDB.Entry(Qdependent).State = EntityState.Deleted;
                    }
                    entityDB.Entry(question).State = EntityState.Deleted;
                }
                entityDB.SaveChanges();
                return "Ok";
            }

            foreach (var QBId in questionBankIds.Split(','))
            {
                try
                {
                    long QBankId = Convert.ToInt64(QBId);
                    foreach (var question in entityDB.Questions.Where(rec => rec.QuestionBankId == QBankId && rec.SubSectionId == subSectionId).ToList())
                    {
                        if (question.QuestionColumnDetails != null)
                            foreach (var Qcolumn in question.QuestionColumnDetails.ToList())
                                entityDB.Entry(Qcolumn).State = EntityState.Deleted;
                        if (question.QuestionsDependants != null)
                        {
                            foreach (var Qdependent in question.QuestionsDependants.ToList())
                                entityDB.Entry(Qdependent).State = EntityState.Deleted;
                        }
                        entityDB.Entry(question).State = EntityState.Deleted;
                    }
                }
                catch (Exception ee) { }
            }
            entityDB.SaveChanges();
            return "OK";
        }

        [SessionExpire]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "QuestionnaireTool", sideMenuName = "QuizTemplateBuilder", ViewName = "QuizTemplatePreview")]
        public ActionResult QuizTemplatePreview(long TemplateSectionID, long ClientId, int type, Guid? templateId)//1-Standard 0-Custom
        {
            Templates selectedTemplate = entityDB.Templates.FirstOrDefault(rec => rec.TemplateStatus == 0 && rec.TemplateType == 1 && rec.DefaultTemplate == true);

            if (type == 0)
                selectedTemplate = entityDB.Templates.FirstOrDefault(rec => rec.TemplateStatus == 0 && rec.TemplateType == 1 && rec.DefaultTemplate == false);

            if (templateId != null)
            {
                selectedTemplate = entityDB.Templates.Find(templateId);
            }
            ViewBag.Type = type;

            ViewBag.TemplateId = selectedTemplate.TemplateID;
            //To Change Side Menu Selection>>>
            if (selectedTemplate.DefaultTemplate == null || selectedTemplate.DefaultTemplate == false)
            {
                List<LocalHeaderMenuModel> headerMenu = (List<LocalHeaderMenuModel>)ViewBag.headerMenus;
                headerMenu.FirstOrDefault(record => record.menuName == "QuestionnaireTool").sideMenus.ForEach(record => record.isChecked = false);
                headerMenu.FirstOrDefault(record => record.menuName == "QuestionnaireTool").sideMenus.FirstOrDefault(record => record.menuName == "QuizTemplateBuilder").isChecked = true;
                ViewBag.headerMenus = headerMenu;
            }

            //Ends<<<

            var templateSection = selectedTemplate.TemplateSections.OrderBy(rec => rec.DisplayOrder).FirstOrDefault(rec => rec.TemplateSectionID == TemplateSectionID);
            if (templateSection == null)
                templateSection = selectedTemplate.TemplateSections.Where(rec => rec.TemplateSectionType != 0 && rec.TemplateSectionType != 31).OrderBy(rec => rec.DisplayOrder).FirstOrDefault();

            TemplateSectionID = templateSection.TemplateSectionID;
            ViewBag.AllSections = (from sec in selectedTemplate.TemplateSections.ToList()
                                   select new SelectListItem
                                   {
                                       Text = sec.SectionName,
                                       Value = sec.TemplateSectionID + "",
                                       Selected = sec.TemplateSectionID == TemplateSectionID
                                   }).ToList();
            if (type == 1)
                ViewBag.AllClients = (from org in entityDB.Organizations.Where(rec => rec.OrganizationType == "Client").ToList()
                                      select new SelectListItem
                                      {
                                          Text = org.Name,
                                          Value = org.OrganizationID + "",
                                          Selected = org.OrganizationID == ClientId
                                      }).ToList();

            else
            {
                var clientId = selectedTemplate.ClientTemplates.FirstOrDefault().ClientID;
                ViewBag.AllClients = (from org in entityDB.Organizations.Where(rec => rec.OrganizationType == "Client" && rec.OrganizationID == clientId).ToList()
                                      select new SelectListItem
                                      {
                                          Text = org.Name,
                                          Value = org.OrganizationID + "",
                                          Selected = org.OrganizationID == ClientId
                                      }).ToList();
            }

            var section = entityDB.TemplateSections.Find(TemplateSectionID);
            if (section.TemplateSectionType == 21)
            {
                // To get the video documents
                var videosSubSection = entityDB.TemplateSubSections.Find(section.TemplateSubSections.FirstOrDefault(rec => rec.SubSectionTypeCondition == "QuizVideos").SubSectionID);
                var videoDocuments = entityDB.Document.Where(rec => rec.ReferenceType == "QuizVideos" && rec.ReferenceRecordID == videosSubSection.SubSectionID).ToList();
                ViewBag.VBVideoDocuments = videoDocuments;

                // To get the alternate format documents
                var alternateFormatsSubSection = entityDB.TemplateSections.FirstOrDefault(rec => rec.TemplateSectionID == TemplateSectionID).TemplateSubSections.FirstOrDefault(rec => rec.SubSectionTypeCondition == "QuizAlternateFormats");
                if (alternateFormatsSubSection != null)
                {
                    var alternateFormatDocuments = entityDB.Document.Where(rec => rec.ReferenceType == "QuizAlternateFormats" && rec.ReferenceRecordID == alternateFormatsSubSection.SubSectionID).ToList();
                    ViewBag.VBAlternateFormatDocuments = alternateFormatDocuments;
                }
            }
            if (section.TemplateSectionType == 22)
            {
                // To get the video documents
                LocalQuizIFrameMeta iFrameData = new LocalQuizIFrameMeta();

                var iframeSubSection = entityDB.TemplateSubSections.Find(section.TemplateSubSections.FirstOrDefault(rec => rec.SubSectionTypeCondition == "QuizIFrame").SubSectionID);
                if (iframeSubSection.MetaJSON != null)
                {
                    LocalQuizIFrameMeta metaData = (LocalQuizIFrameMeta)new JavaScriptSerializer().Deserialize<LocalQuizIFrameMeta>(iframeSubSection.MetaJSON);
                    iFrameData.IFrameURL = metaData.IFrameURL;
                    iFrameData.Width = metaData.Width;
                    iFrameData.Height = metaData.Height;
                }

                @ViewBag.IFrameData = iFrameData;

            }

            getCommand();
            @ViewBag.PreviewTemplateName = selectedTemplate.TemplateName;
            ViewBag.previewTemplateType = templateSection.TemplateSectionType;
            ViewBag.TemplateSectionID = TemplateSectionID;
            @ViewBag.ClientId = ClientId;

            ViewBag.TrainingPath = ConfigurationSettings.AppSettings["TrainingVideosPath"];//Rajesh on 10/31/2014
            return View(selectedTemplate);
        }

        public void getCommand()
        {
            SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["EFDbContext"].ConnectionString);
            connection.Open();
            ViewBag.command = connection.CreateCommand();

        }

        [HttpPost]
        public string AddOrEditQuizQuestion(string QuestionText, long SubSectionId, int NumberOfColumns, long QuestionBankId, long QuestionId)
        {
            var subSection = entityDB.TemplateSubSections.Find(SubSectionId);
            int questionOrderNumber = 1;
            try { questionOrderNumber = subSection.Questions.Count() + 1; }
            catch { }
            Questions question;
            long questionId = -1;
            if (QuestionId == -1)
            {
                var SubSection = entityDB.TemplateSubSections.Find(SubSectionId);
                question = new Questions();
                question.QuestionText = QuestionText;
                question.SubSectionId = SubSectionId;
                question.DisplayOrder = questionOrderNumber;
                question.Visible = true;
                question.NumberOfColumns = NumberOfColumns;

                question.IsMandatory = true;
                question.IsBold = true;
                question.ResponseRequired = true;
                question.DisplayResponse = true;
                question.HasDependantQuestions = false;
                question.QuestionBankId = QuestionBankId;
                entityDB.Questions.Add(question);
                questionId = question.QuestionID;

            }
            else
            {
                question = entityDB.Questions.Find(QuestionId);
                question.HasDependantQuestions = false;
                if (NumberOfColumns != question.NumberOfColumns && question.QuestionColumnDetails != null)
                {
                    foreach (var QColumn in question.QuestionColumnDetails.ToList())
                        entityDB.Entry(QColumn).State = EntityState.Deleted;
                }
                question.NumberOfColumns = NumberOfColumns;
                questionId = question.QuestionID;

                entityDB.Entry(question).State = EntityState.Modified;
            }

            entityDB.SaveChanges();
            return "" + question.QuestionID;
        }

        [HttpGet]
        [SessionExpireForView]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "QuestionnaireTool", sideMenuName = "QuizTemplateBuilder", ViewName = "AddOrEditQuizQuestionColumnDetails")]
        public ActionResult AddOrEditQuizQuestionColumnDetails(long QuestionId)
        {
            var Question = entityDB.Questions.Find(QuestionId);
            ViewBag.Question = Question;
            var questionColumnDetails = entityDB.QuestionColumnDetails.FirstOrDefault(rec => rec.QuestionId == QuestionId);
            LocalQuizQuestionColumnDetails details = new LocalQuizQuestionColumnDetails();
            details.QuestionId = QuestionId;
            details.Visible = !Question.Visible;

            if (questionColumnDetails != null)
            {
                details.QuestionOptionValues = questionColumnDetails.ColumnValues;
                details.QuestionAnswer = Question.CorrectAnswer; // Kiran on 11/3/2014
            }

            return View(details);
        }

        [HttpPost]
        [SessionExpireForView]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "QuestionnaireTool", sideMenuName = "QuizTemplateBuilder", ViewName = "AddOrEditQuizQuestionColumnDetails")]
        public ActionResult AddOrEditQuizQuestionColumnDetails(LocalQuizQuestionColumnDetails form)
        {
            var Question = entityDB.Questions.Find(form.QuestionId);

            ViewBag.VBisErrorExists = false;

            ViewBag.Question = Question;
            if (ModelState.IsValid)
            {
                Question.IsBold = true;
                Question.IsMandatory = true;
                Question.Visible = !form.Visible;

                Question.HasDependantQuestions = false;

                if (Question.QuestionColumnDetails != null && !(Question.QuestionColumnDetails.Where(rec => rec.PrequalificationUserInputs.Count() > 0).Count() > 0))
                {
                    var questionColumnDetails = entityDB.QuestionColumnDetails.FirstOrDefault(rec => rec.QuestionId == form.QuestionId);
                    if (string.IsNullOrEmpty(form.QuestionOptionValues))
                    {
                        ViewBag.VBisErrorExists = true;
                        return View(form);
                    }
                    if (!string.IsNullOrEmpty(form.QuestionOptionValues) && string.IsNullOrEmpty(form.QuestionAnswer))
                    {
                        ViewBag.VBisErrorExists = true;
                        return View(form);
                    }


                    questionColumnDetails.ColumnValues = form.QuestionOptionValues;
                    // Kiran on 11/3/2014
                    //questionColumnDetails.CorrectAnswer = form.QuestionAnswer;
                    Question.CorrectAnswer = form.QuestionAnswer;
                    entityDB.Entry(Question).State = EntityState.Modified;
                    // Ends<<<
                    entityDB.Entry(questionColumnDetails).State = EntityState.Modified;
                }
                entityDB.SaveChanges();
                return Redirect("PopUpCloseView");
            }
            return View(form);
        }

        //[HttpGet]
        //[SessionExpireForView]
        //[HeaderAndSidebar(headerName = "QuestionnaireTool", sideMenuName = "QuizTemplateBuilder", ViewName = "AddTrainingVideoOrAlternateFormat")]
        //public ActionResult AddTrainingVideoOrAlternateFormat(long SubSectionId, string FileName, bool mode, long SectionId, string SubSectionType, Guid? DocumentId)
        //{
        //    LocalAddTrainingVideo addVideo = new LocalAddTrainingVideo();
        //    addVideo.SubSectionId = SubSectionId;
        //    addVideo.SectionId = SectionId;
        //    addVideo.ClientId = entityDB.TemplateSections.Find(SectionId).Templates.ClientTemplates.FirstOrDefault().ClientID;
        //    var subSection = entityDB.TemplateSubSections.Find(SubSectionId);
        //    if(subSection != null)
        //    {
        //        addVideo.SubSectionName = subSection.SubSectionName;
        //        addVideo.SubSectionTypeCondition = subSection.SubSectionTypeCondition;
        //        if (SubSectionType == "QuizVideos")
        //            ViewBag.VBTrainingVideos = entityDB.Document.Where(rec => rec.ReferenceType == "QuizVideos" && rec.ReferenceRecordID == SubSectionId).ToList();
        //        else if (SubSectionType == "QuizAlternateFormats")
        //            ViewBag.VBTrainingAlternateFormats = entityDB.Document.Where(rec => rec.ReferenceType == "QuizAlternateFormats" && rec.ReferenceRecordID == SubSectionId).ToList();
        //    }

        //    if (mode == false)
        //    {
        //        addVideo.mode = false;
        //        addVideo.SubSectionTypeCondition = SubSectionType;
        //    }
        //    else
        //    {
        //        addVideo.DocumentId = DocumentId;
        //        addVideo.mode = true;
        //        addVideo.FileNameOld = FileName;
        //    }
        //    return View(addVideo);
        //}
        //[HttpPost]
        //[SessionExpireForView]
        //[HeaderAndSidebar(headerName = "QuestionnaireTool", sideMenuName = "QuizTemplateBuilder", ViewName = "AddOrEditQuizQuestionColumnDetails")]
        //public ActionResult AddOrEditQuizQuestionColumnDetails(LocalQuizQuestionColumnDetails form)
        //{
        //    var Question = entityDB.Questions.Find(form.QuestionId);

        //    ViewBag.VBisErrorExists = false;

        //    ViewBag.Question = Question;
        //    if (ModelState.IsValid)
        //    {
        //        Question.IsBold = true;
        //        Question.IsMandatory = true;
        //        Question.Visible = !form.Visible;

        //        Question.HasDependantQuestions = false;

        //        if (Question.QuestionColumnDetails != null && !(Question.QuestionColumnDetails.Where(rec => rec.PrequalificationUserInputs.Count() > 0).Count() > 0))
        //        {
        //            var questionColumnDetails = entityDB.QuestionColumnDetails.FirstOrDefault(rec => rec.QuestionId == form.QuestionId);
        //            if (string.IsNullOrEmpty(form.QuestionOptionValues))
        //            {
        //                ViewBag.VBisErrorExists = true;
        //                return View(form);
        //            }
        //            if (!string.IsNullOrEmpty(form.QuestionOptionValues) && string.IsNullOrEmpty(form.QuestionAnswer))
        //            {
        //                ViewBag.VBisErrorExists = true;
        //                return View(form);
        //            }


        //            questionColumnDetails.ColumnValues = form.QuestionOptionValues;
        //            questionColumnDetails.CorrectAnswer = form.QuestionAnswer;
        //            entityDB.Entry(questionColumnDetails).State = EntityState.Modified;
        //        }
        //        entityDB.SaveChanges();
        //        return Redirect("PopUpCloseView");
        //    }
        //    return View(form);
        //}

        [HttpGet]
        [SessionExpireForView]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "QuestionnaireTool", sideMenuName = "QuizTemplateBuilder", ViewName = "AddTrainingVideoOrAlternateFormat")]
        public ActionResult AddTrainingVideoOrAlternateFormat(long SubSectionId, string FileName, bool mode, long SectionId, string SubSectionType, Guid? DocumentId)
        {
            LocalAddTrainingVideo addVideo = new LocalAddTrainingVideo();
            addVideo.SubSectionId = SubSectionId;
            addVideo.SectionId = SectionId;
            addVideo.ClientId = entityDB.TemplateSections.Find(SectionId).Templates.ClientTemplates.FirstOrDefault().ClientID;
            var subSection = entityDB.TemplateSubSections.Find(SubSectionId);
            if (subSection != null)
            {
                addVideo.SubSectionName = subSection.SubSectionName;
                addVideo.SubSectionTypeCondition = subSection.SubSectionTypeCondition;
                if (SubSectionType == "QuizVideos")
                    ViewBag.VBTrainingVideos = entityDB.Document.Where(rec => rec.ReferenceType == "QuizVideos" && rec.ReferenceRecordID == SubSectionId).ToList();
                else if (SubSectionType == "QuizAlternateFormats")
                    ViewBag.VBTrainingAlternateFormats = entityDB.Document.Where(rec => rec.ReferenceType == "QuizAlternateFormats" && rec.ReferenceRecordID == SubSectionId).ToList();
            }

            if (mode == false)
            {
                addVideo.mode = false;
                addVideo.SubSectionTypeCondition = SubSectionType;
            }
            else
            {
                addVideo.DocumentId = DocumentId;
                addVideo.mode = true;
                addVideo.FileNameOld = FileName;
            }
            return View(addVideo);
        }

        [HttpPost]
        [SessionExpireForView]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "QuestionnaireTool", sideMenuName = "QuizTemplateBuilder", ViewName = "AddTrainingVideoOrAlternateFormat")]
        public ActionResult AddTrainingVideoOrAlternateFormat(List<HttpPostedFileBase> videos, LocalAddTrainingVideo addVideo)
        {
            var subSection = entityDB.TemplateSubSections.Find(addVideo.SubSectionId);
            var templateSection = entityDB.TemplateSections.Find(addVideo.SectionId);
            if (addVideo.mode == false) // Insertion
            {
                TemplateSubSections subSectionForAlternateFormats = new TemplateSubSections();
                if (addVideo.SubSectionTypeCondition == "QuizVideos")
                {
                    subSection.SubSectionName = addVideo.SubSectionName;
                }
                else if (addVideo.SubSectionTypeCondition == "QuizAlternateFormats")
                {
                    if (subSection == null)
                    {
                        subSectionForAlternateFormats.SubSectionName = addVideo.SubSectionName;
                        subSectionForAlternateFormats.SubSectionTypeCondition = "QuizAlternateFormats";
                        subSectionForAlternateFormats.ClientId = addVideo.ClientId;
                        subSectionForAlternateFormats.SubSectionType = 2;
                        subSectionForAlternateFormats.DisplayOrder = templateSection.TemplateSubSections.Count() + 1;
                        subSectionForAlternateFormats.TemplateSectionID = addVideo.SectionId;
                        entityDB.TemplateSubSections.Add(subSectionForAlternateFormats);
                    }
                    else
                    {
                        subSection.SubSectionName = addVideo.SubSectionName;
                        entityDB.Entry(subSection).State = EntityState.Modified;
                        subSectionForAlternateFormats = subSection;
                    }
                    entityDB.SaveChanges();
                }
                if (Request.Files != null && Request.Files.Count > 0)
                {
                    for (int i = 0; i < Request.Files.Count; i++)
                    {
                        if (Request.Files[i].ContentLength > 0)
                        {
                            Document saveVideo = new Document();
                            saveVideo.DocumentTypeId = entityDB.DocumentType.FirstOrDefault(rec => rec.DocumentTypeName.ToLower() == "quiz videos").DocumentTypeId;
                            saveVideo.OrganizationId = addVideo.ClientId;
                            if (addVideo.SubSectionTypeCondition == "QuizVideos")
                            {
                                saveVideo.ReferenceType = "QuizVideos";
                                saveVideo.ReferenceRecordID = addVideo.SubSectionId;
                            }
                            else if (addVideo.SubSectionTypeCondition == "QuizAlternateFormats")
                            {
                                saveVideo.ReferenceType = "QuizAlternateFormats";
                                saveVideo.ReferenceRecordID = subSectionForAlternateFormats.SubSectionID;
                            }
                            saveVideo.DocumentName = Request.Files[i].FileName;
                            saveVideo.Uploaded = DateTime.Now;
                            entityDB.Document.Add(saveVideo);
                            entityDB.SaveChanges();

                            // To save the physical file
                            var fileName = Path.GetFileName(Request.Files[i].FileName);
                            var extension = Path.GetExtension(Request.Files[i].FileName);

                            Directory.CreateDirectory(Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["TrainingVideosPath"])));
                            var path = Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["TrainingVideosPath"]), saveVideo.DocumentId + extension);
                            Request.Files[i].SaveAs(path);
                        }

                    }
                }
            }
            else
            {
                if (Request.Files != null && Request.Files.Count > 0)
                {
                    for (int i = 0; i < Request.Files.Count; i++)
                    {
                        var fileName = Path.GetFileName(Request.Files[i].FileName);
                        var extension = Path.GetExtension(Request.Files[i].FileName);

                        Document saveVideo = new Document();
                        saveVideo.DocumentTypeId = entityDB.DocumentType.FirstOrDefault(rec => rec.DocumentTypeName.ToLower() == "quiz videos").DocumentTypeId;
                        saveVideo.OrganizationId = Convert.ToInt64(subSection.ClientId);
                        if (subSection.SubSectionTypeCondition == "QuizVideos")
                            saveVideo.ReferenceType = "QuizVideos";
                        else if (subSection.SubSectionTypeCondition == "QuizAlternateFormats")
                            saveVideo.ReferenceType = "QuizAlternateFormats";
                        saveVideo.ReferenceRecordID = addVideo.SubSectionId;
                        saveVideo.DocumentName = Request.Files[i].FileName;
                        saveVideo.Uploaded = DateTime.Now;
                        entityDB.Document.Add(saveVideo);

                        Directory.CreateDirectory(Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["TrainingVideosPath"])));
                        var path = Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["TrainingVideosPath"]), saveVideo.DocumentId + extension);
                        Request.Files[i].SaveAs(path);

                        var oldFilePath = Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["TrainingVideosPath"]), addVideo.DocumentId + extension); ;
                        System.IO.File.Delete(oldFilePath);

                        var oldDocument = entityDB.Document.Find(addVideo.DocumentId);
                        entityDB.Document.Remove(oldDocument);
                        entityDB.SaveChanges();
                    }
                }
            }

            entityDB.SaveChanges();
            return Redirect("PopUpCloseView");
        }

        //[HttpPost]
        //[SessionExpireForView]
        //[HeaderAndSidebar(headerName = "QuestionnaireTool", sideMenuName = "QuizTemplateBuilder", ViewName = "AddTrainingVideoOrAlternateFormat")]
        //public ActionResult AddTrainingVideoOrAlternateFormat(List<HttpPostedFileBase> videos, LocalAddTrainingVideo addVideo)
        //{
        //    var subSection = entityDB.TemplateSubSections.Find(addVideo.SubSectionId);
        //    if (addVideo.mode == false) // Insertion
        //    {
        //        TemplateSubSections subSectionForAlternateFormats = new TemplateSubSections();
        //        if (addVideo.SubSectionTypeCondition == "QuizVideos")
        //        {
        //            subSection.SubSectionName = addVideo.SubSectionName;   
        //        }
        //        else if (addVideo.SubSectionTypeCondition == "QuizAlternateFormats")
        //        {   
        //            subSectionForAlternateFormats.SubSectionName = addVideo.SubSectionName;
        //            subSectionForAlternateFormats.SubSectionTypeCondition = "QuizAlternateFormats";
        //            subSectionForAlternateFormats.ClientId = addVideo.ClientId;
        //            subSectionForAlternateFormats.SubSectionType = 0;
        //            subSectionForAlternateFormats.TemplateSectionID = addVideo.SectionId;
        //            entityDB.TemplateSubSections.Add(subSectionForAlternateFormats);
        //            entityDB.SaveChanges();
        //        }
        //        if (Request.Files != null && Request.Files.Count > 0)
        //        {
        //            for (int i = 0; i < Request.Files.Count; i++)
        //            {
        //                if (Request.Files[i].ContentLength > 0)
        //                {
        //                    Document saveVideo = new Document();
        //                    saveVideo.DocumentTypeId = entityDB.DocumentType.FirstOrDefault(rec => rec.DocumentTypeName.ToLower() == "quiz videos").DocumentTypeId;
        //                    saveVideo.OrganizationId = addVideo.ClientId;
        //                    if (addVideo.SubSectionTypeCondition == "QuizVideos")
        //                    {
        //                        saveVideo.ReferenceType = "QuizVideos";
        //                        saveVideo.ReferenceRecordID = addVideo.SubSectionId;
        //                    }
        //                    else if (addVideo.SubSectionTypeCondition == "QuizAlternateFormats")
        //                    {
        //                        saveVideo.ReferenceType = "QuizAlternateFormats";
        //                        saveVideo.ReferenceRecordID = subSectionForAlternateFormats.SubSectionID;
        //                    }
        //                    saveVideo.DocumentName = Request.Files[i].FileName;
        //                    saveVideo.Uploaded = DateTime.Now;
        //                    entityDB.Document.Add(saveVideo);
        //                    entityDB.SaveChanges();

        //                    // To save the physical file
        //                    var fileName = Path.GetFileName(Request.Files[i].FileName);
        //                    var extension = Path.GetExtension(Request.Files[i].FileName);

        //                    Directory.CreateDirectory(Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["TrainingVideosPath"])));
        //                    var path = Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["TrainingVideosPath"]), saveVideo.DocumentId + extension);
        //                    Request.Files[i].SaveAs(path);
        //                }

        //            }
        //        }   
        //    }   
        //    else
        //    {
        //        if (Request.Files != null && Request.Files.Count > 0)
        //        {
        //            for (int i = 0; i < Request.Files.Count; i++)
        //            {
        //                var fileName = Path.GetFileName(Request.Files[i].FileName);
        //                var extension = Path.GetExtension(Request.Files[i].FileName);

        //                Document saveVideo = new Document();
        //                saveVideo.DocumentTypeId = entityDB.DocumentType.FirstOrDefault(rec => rec.DocumentTypeName.ToLower() == "quiz videos").DocumentTypeId;
        //                saveVideo.OrganizationId = Convert.ToInt64(subSection.ClientId);
        //                if (subSection.SubSectionTypeCondition == "QuizVideos")
        //                    saveVideo.ReferenceType = "QuizVideos";
        //                else if (subSection.SubSectionTypeCondition == "QuizAlternateFormats")
        //                    saveVideo.ReferenceType = "QuizAlternateFormats";
        //                saveVideo.ReferenceRecordID = addVideo.SubSectionId;
        //                saveVideo.DocumentName = Request.Files[i].FileName;
        //                saveVideo.Uploaded = DateTime.Now;
        //                entityDB.Document.Add(saveVideo);

        //                Directory.CreateDirectory(Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["TrainingVideosPath"])));
        //                var path = Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["TrainingVideosPath"]), saveVideo.DocumentId + extension);
        //                Request.Files[i].SaveAs(path);

        //                var oldFilePath = Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["TrainingVideosPath"]), addVideo.DocumentId + extension); ;
        //                System.IO.File.Delete(oldFilePath);

        //                var oldDocument = entityDB.Document.Find(addVideo.DocumentId);
        //                entityDB.Document.Remove(oldDocument);
        //                entityDB.SaveChanges();
        //            }
        //        }
        //    }

        //    entityDB.SaveChanges();
        //    return Redirect("PopUpCloseView");
        //}

        public string DeleteTrainingVideoOrAlternateFormat(Guid DocumentId)
        {
            Document document = entityDB.Document.Find(DocumentId);
            if (document != null)
            {
                var dotIndex = document.DocumentName.LastIndexOf('.');
                var extension = document.DocumentName.Substring(dotIndex);
                var path = Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["TrainingVideosPath"]), document.DocumentId + extension);
                if (path != null)
                {
                    System.IO.File.Delete(path);
                    entityDB.Document.Remove(document);
                    entityDB.SaveChanges();
                }
            }
            return "DeleteSuccess";
        }

        public string ChangeTemplateSectionsOrder(string templateSectionIds, Guid TemplateId)
        {
            string result = "Ok" + templateSectionIds + "\n";
            int orderNumber = 0;
            var template = entityDB.Templates.Find(TemplateId);
            if (template != null)
                foreach (var ids in templateSectionIds.Split('|'))
                {
                    try
                    {
                        long secId = Convert.ToInt64(ids);
                        result += secId;
                        TemplateSections section = template.TemplateSections.ToList().FirstOrDefault(rec => rec.TemplateSectionID == secId);
                        if (section == null)
                            continue;
                        orderNumber++;
                        section.DisplayOrder = orderNumber;
                        entityDB.Entry(section).State = EntityState.Modified;
                    }
                    catch (Exception ee) { result += ee.Message + ""; }
                }
            entityDB.SaveChanges();
            return result;
        }

        public string ChangeQuestionsOrder(string QuestionIds, long templateSubSectionId)
        {
            string result = "Ok" + QuestionIds + "\n";
            try
            {
                int orderNumber = 0;
                var templateSubSec = entityDB.TemplateSubSections.Find(templateSubSectionId);
                if (templateSubSec != null)
                    foreach (var ids in QuestionIds.Split('|'))
                    {
                        try
                        {
                            long secId = Convert.ToInt64(ids);
                            result += secId;
                            Questions Question = templateSubSec.Questions.ToList().FirstOrDefault(rec => rec.QuestionID == secId);
                            if (Question == null)
                                continue;
                            orderNumber++;
                            Question.DisplayOrder = orderNumber;
                            entityDB.Entry(Question).State = EntityState.Modified;
                        }
                        catch (Exception ee) { }
                    }
                entityDB.SaveChanges();
            }
            catch (Exception ee) { result += ee.Message; }
            return result;
        }

        [HttpPost]
        public string SaveSubSectionName(long subSectionId, string subSectionName)
        {
            var subSection = entityDB.TemplateSubSections.Find(subSectionId);
            subSection.SubSectionName = subSectionName;
            entityDB.Entry(subSection).State = EntityState.Modified;
            entityDB.SaveChanges();

            return "SaveSuccess";
        }

        [SessionExpire]
        [HttpGet]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "QuestionnaireTool", sideMenuName = "QuizTemplateBuilder", ViewName = "AddQuizClientTemplateBusinessUnits")]
        public ActionResult AddQuizClientTemplateBusinessUnits()
        {
            ViewBag.VBDisplayError = false;
            var quizTemplateInProcess = entityDB.Templates.FirstOrDefault(rec => rec.TemplateType == 1 && rec.TemplateStatus == 0 && rec.DefaultTemplate == false);
            var clientId = quizTemplateInProcess.ClientTemplates.FirstOrDefault().ClientID;

            LocalAssignQuizClientTemplatesForBU quizClientBU = new LocalAssignQuizClientTemplatesForBU();

            quizClientBU.ClientTemplateId = quizTemplateInProcess.ClientTemplates.FirstOrDefault().ClientTemplateID;

            //quizClientBU.quizBusUnits = (from busUnits in entityDB.ClientBusinessUnits.ToList()
            //                             where busUnits.ClientId == clientId
            //                             select new CheckBoxesControlForQuizzes
            //                             {
            //                                 id = busUnits.ClientBusinessUnitId.ToString(),
            //                                 Text = busUnits.BusinessUnitName + "" 
            //                             }).ToList();
            quizClientBU.quizBusUnits = (from busUnits in entityDB.ClientBusinessUnits.ToList().Where(busUnits => busUnits.ClientId == clientId)
                                         select new CheckBoxesControlForQuizzes
                                         {
                                             id = busUnits.ClientBusinessUnitId.ToString(),
                                             Text = busUnits.BusinessUnitName + ""
                                         }).ToList();
            return View(quizClientBU);
        }

        [SessionExpire]
        [HttpPost]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "QuestionnaireTool", sideMenuName = "QuizTemplateBuilder", ViewName = "AddQuizClientTemplateBusinessUnits")]
        public ActionResult AddQuizClientTemplateBusinessUnits(LocalAssignQuizClientTemplatesForBU quizClientTemplateBU)
        {
            ViewBag.VBDisplayError = false;
            if (quizClientTemplateBU.quizBusUnits != null)
            {
                if (quizClientTemplateBU.quizBusUnits.Where(rec => rec.Status == true) == null || quizClientTemplateBU.quizBusUnits.Where(rec => rec.Status == true).Count() == 0)
                {
                    ViewBag.VBDisplayError = true;
                    return View(quizClientTemplateBU);
                }
                else
                {
                    foreach (var quiz in quizClientTemplateBU.quizBusUnits.Where(rec => rec.Status == true))
                    {
                        long currentBusinessUnitId = Convert.ToInt64(quiz.id);
                        if (entityDB.ClientTemplatesForBU.FirstOrDefault(rec => rec.ClientTemplateId == quizClientTemplateBU.ClientTemplateId && rec.ClientBusinessUnitId == currentBusinessUnitId) == null)
                        {
                            ClientTemplatesForBU addQuizClientTemplateBU = new ClientTemplatesForBU();
                            addQuizClientTemplateBU.ClientBusinessUnitId = Convert.ToInt64(quiz.id);
                            addQuizClientTemplateBU.ClientTemplateId = quizClientTemplateBU.ClientTemplateId;

                            entityDB.ClientTemplatesForBU.Add(addQuizClientTemplateBU);
                        }
                    }
                }
                // Kiran on 11/5/2014
                var clientTemplate = entityDB.ClientTemplates.Find(quizClientTemplateBU.ClientTemplateId);
                var templateInProcess = entityDB.Templates.Find(clientTemplate.TemplateID);
                templateInProcess.TemplateStatus = 1;
                entityDB.Entry(templateInProcess).State = EntityState.Modified;
                entityDB.SaveChanges();

                //var clientId = clientTemplate.ClientID;
                //var empTrainingRoleId = entityDB.SystemRoles.FirstOrDefault(rec => rec.RoleName == "Employee Training").RoleId;
                //var clientPrequalifications = entityDB.Prequalification.Where(rec => rec.ClientId == clientId && rec.PrequalificationStatusId != 14).ToList(); // Kiran on 02/05/2015 given the provision to assign trainings under exception/probation related statuses, prequalified status // kiran on 02/07/2015 to assign trainings to employee under any status except N/A as per the email 02/07/2015. 
                ////var clientPrequalifications = entityDB.Prequalification.Where(rec => rec.ClientId == clientId && rec.PrequalificationStatusId == 9).ToList();
                //if (entityDB.Organizations.Find(clientId).TrainingRequired == true)
                //{
                //    foreach (var prequal in clientPrequalifications)
                //    {
                //        if (prequal.PrequalificationFinish >= DateTime.Now && clientTemplate.Templates.TemplateStatus == 1)
                //        {
                //            if (prequal.PrequalificationSites != null)
                //            {
                //                var prequalSites = entityDB.PrequalificationSites.ToList().Where(rec => rec.PrequalificationId == prequal.PrequalificationId).Select(rec => rec.ClientBusinessUnitId).ToList();

                //                var quizClientTemplateBus = entityDB.ClientTemplatesForBU.Where(rec => prequalSites.Contains(rec.ClientBusinessUnitId) && rec.ClientTemplateId == quizClientTemplateBU.ClientTemplateId).ToList();// Kiran on 11/18/2014

                //                if (quizClientTemplateBus != null && quizClientTemplateBus.Count != 0) // kiran on 11/8/2014
                //                {
                //                    var empUserOrgRoles = entityDB.SystemUsersOrganizationsRoles.Where(rec => rec.SysRoleId == empTrainingRoleId && rec.SystemUsersOrganizations.OrganizationId == prequal.VendorId).ToList();
                //                    foreach (var userOrgRole in empUserOrgRoles)
                //                    {
                //                        SystemUsersOrganizationsQuizzes addQuizForEmployee = new SystemUsersOrganizationsQuizzes();
                //                        addQuizForEmployee.SysUserOrgId = userOrgRole.SystemUsersOrganizations.SysUserOrganizationId;
                //                        addQuizForEmployee.ClientTemplateId = clientTemplate.ClientTemplateID;
                //                        // Kiran on 12/18/2014
                //                        addQuizForEmployee.VendorId = prequal.VendorId;
                //                        addQuizForEmployee.ClientId = prequal.ClientId;
                //                        // Ends<<<
                //                        entityDB.SystemUsersOrganizationsQuizzes.Add(addQuizForEmployee);
                //                    }
                //                }
                //            }
                //        }
                //    }
                //}
                entityDB.SaveChanges();
                return Redirect("PopUpCloseView");
                // Ends<<<
            }
            ViewBag.VBDisplayError = true;
            return View(quizClientTemplateBU);
        }

        public string ApproveQuizTemplate(Guid TemplateID)
        {
            var templateSections = entityDB.Templates.FirstOrDefault(rec => rec.TemplateID == TemplateID).TemplateSections.ToList();
            if (templateSections.Count != 0 && templateSections != null)
            {
                if ((templateSections.Where(rec => rec.TemplateSectionType == 21).ToList() == null || templateSections.Where(rec => rec.TemplateSectionType == 21).ToList().Count == 0) &&
                    (templateSections.Where(rec => rec.TemplateSectionType == 22).ToList() == null || templateSections.Where(rec => rec.TemplateSectionType == 22).ToList().Count == 0))
                {
                    return "Incomplete";
                }
                else if (templateSections.Where(rec => rec.TemplateSectionType == 1).ToList() == null || templateSections.Where(rec => rec.TemplateSectionType == 1).ToList().Count == 0)
                {
                    return "Incomplete";
                }

                foreach (var section in templateSections)
                {
                    if (section.TemplateSectionType == 1) // Quiz Questions
                    {
                        if (section.TemplateSubSections != null && section.TemplateSubSections.Count != 0)
                        {
                            foreach (var subSection in section.TemplateSubSections)
                            {
                                if (subSection.Questions.Count == 0)
                                {
                                    return "Incomplete";
                                }
                                // kiran on 11/6/2014
                                foreach (var question in subSection.Questions.ToList())
                                {
                                    if (question.QuestionColumnDetails == null || question.QuestionColumnDetails.Count == 0 || question.CorrectAnswer == null)
                                    {
                                        return "Incomplete";
                                    }
                                }
                                // Ends<<<
                            }
                        }
                        // Sandhya on 05/12/2015
                        else if (section.TemplateSubSections.Count() == 0)
                        {
                            return "Incomplete";
                        }
                        // Ends<<<
                    }
                    else if (section.TemplateSectionType == 21) // Quiz Videos and alternate formats
                    {
                        if (section.TemplateSubSections != null && section.TemplateSubSections.Count != 0)
                        {
                            foreach (var subSection in section.TemplateSubSections)
                            {
                                if (subSection.SubSectionTypeCondition == "QuizVideos")
                                {
                                    if (entityDB.Document.Where(rec => rec.ReferenceType == "QuizVideos" && rec.ReferenceRecordID == subSection.SubSectionID).ToList() == null || entityDB.Document.Where(rec => rec.ReferenceType == "QuizVideos" && rec.ReferenceRecordID == subSection.SubSectionID).ToList().Count == 0)
                                    {
                                        return "Incomplete";
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                return "Incomplete";
            }

            return "Approvetemplate";
        }

        // Sandhya on 04/29/2015
        [SessionExpire]
        [HttpGet]
        public ActionResult AssignUnitsForTraining(Guid TemplateId)
        {
            //sandhya and mani--- 04/05/2015
            var clientTemplate = entityDB.ClientTemplates.FirstOrDefault(rec => rec.TemplateID == TemplateId);
            var assignBUs = entityDB.ClientBusinessUnits.Where(businessUnits => businessUnits.ClientId == clientTemplate.ClientID).OrderBy(businessUnits => businessUnits.BusinessUnitName).
                Select(rec => new LocalBUDetails
                {
                    BUId = rec.ClientBusinessUnitId,
                    BUName = rec.BusinessUnitName,
                    OrgName = rec.Organization.Name,
                    GroupNumber = -1,
                    TempBUSiteDetails = rec.ClientBusinessUnitSites.OrderBy(rec1 => rec1.SiteName).Select(rec1 => new LocalBUSiteDetails
                    {
                        BUsiteId = rec1.ClientBusinessUnitSiteId,
                        BUsiteName = rec1.SiteName,
                        IsPaymentDone = entityDB.ClientTemplatesForBUSites.FirstOrDefault(record => record.ClientBusinessUnitSiteId == rec1.ClientBusinessUnitSiteId && record.ClientTemplatesForBU.ClientTemplateId == clientTemplate.ClientTemplateID).IsPaymentDone,
                        Ischecked = entityDB.ClientTemplatesForBUSites.Where(record => record.ClientBusinessUnitSiteId == rec1.ClientBusinessUnitSiteId && record.ClientTemplatesForBU.ClientTemplateId == clientTemplate.ClientTemplateID).Count() == 0 ? false : true
                    })
                }).ToList();

            assignBUs.ForEach(rec => rec.BUSiteDetails = rec.TempBUSiteDetails.ToList());
            List<string> Prices = new List<string>();
            var ClienttemplateGroupBUs = entityDB.ClientTemplatesBUGroupPrice.Where(rec => rec.ClientTemplateID == clientTemplate.ClientTemplateID && rec.GroupingFor == 2).OrderBy(rec => rec.GroupPriceType).ToList();
            int i = 0;
            foreach (var group in ClienttemplateGroupBUs)
            {
                var ClientBUIds = group.ClientTemplatesForBU.Where(rec => rec.ClientTemplateId == clientTemplate.ClientTemplateID).Select(rec => rec.ClientBusinessUnitId).ToList();

                assignBUs.ForEach(rec => rec.GroupNumber = ClientBUIds.Contains(rec.BUId) ? group.GroupPriceType : rec.GroupNumber);
                if (group.GroupPriceType != -1)
                    Prices.Add(group.GroupPrice + "");
                i++;
            }

            LocalModelForBusites BUTemplate = new LocalModelForBusites();
            BUTemplate.BUDetails = assignBUs.OrderBy(rec => rec.OrgName).OrderBy(rec => rec.BUName).ToList(); // sumanth on 10/23/2015 for FVOS-74 to display BUs in alphabetical order.
            BUTemplate.ClientTemplateId.Add(clientTemplate.ClientTemplateID);
            BUTemplate.Prices = Prices;


            ViewBag.TemplateName = clientTemplate.Templates.TemplateName;
            ViewBag.OrgName = clientTemplate.Organizations.Name;
            return View(BUTemplate);
        }


        [SessionExpire]
        [HttpPost]
        public ActionResult AssignUnitsForTraining(LocalModelForBusites form)
        {
            var clientTemplateId = form.ClientTemplateId[0];
            //sandhya and mani--- 04/05/2015
            foreach (var businessunit in form.BUDetails.OrderBy(rec => rec.BUName).ToList()) // sumanth on 10/23/2015 for FVOS-274 to display BUs in alphabetical order.
            {
                ClientTemplatesBUGroupPrice Price = new ClientTemplatesBUGroupPrice();

                var checkRecord = entityDB.ClientTemplatesBUGroupPrice.FirstOrDefault(rec => rec.ClientTemplateID == clientTemplateId && rec.GroupPriceType == businessunit.GroupNumber && rec.GroupingFor == 2);
                Price.ClientTemplateID = clientTemplateId;
                Price.GroupingFor = 2;
                if (businessunit.GroupNumber != -1)
                {
                    Price.GroupPrice = Convert.ToDecimal(form.Prices[(int)businessunit.GroupNumber]);
                    Price.GroupPriceType = businessunit.GroupNumber;
                }
                else
                {
                    Price.GroupPrice = 0;
                    Price.GroupPriceType = -1;
                }

                if (checkRecord == null)
                {
                    entityDB.ClientTemplatesBUGroupPrice.Add(Price);

                }
                else if (checkRecord != null && businessunit.GroupNumber != -1)
                {
                    checkRecord.GroupPrice = Convert.ToDecimal(form.Prices[(int)businessunit.GroupNumber]);
                    entityDB.Entry(checkRecord).State = EntityState.Modified;
                }
                entityDB.SaveChanges();

                var checkforTemplatesBU = entityDB.ClientTemplatesForBU.FirstOrDefault(rec => rec.ClientTemplateId == clientTemplateId && rec.ClientBusinessUnitId == businessunit.BUId && rec.ClientTemplatesBUGroupPrice.GroupingFor == 2);
                ClientTemplatesForBU BU = new ClientTemplatesForBU();
                BU.ClientTemplateId = clientTemplateId;
                BU.ClientBusinessUnitId = businessunit.BUId;
                if (checkRecord == null)
                    BU.ClientTemplateBUGroupId = Price.ClientTemplateBUGroupId;
                else
                    BU.ClientTemplateBUGroupId = checkRecord.ClientTemplateBUGroupId;
                if (checkforTemplatesBU == null)
                {
                    entityDB.ClientTemplatesForBU.Add(BU);
                    entityDB.SaveChanges();
                }
                else
                {
                    if (checkRecord == null)
                        checkforTemplatesBU.ClientTemplateBUGroupId = Price.ClientTemplateBUGroupId;
                    else
                        checkforTemplatesBU.ClientTemplateBUGroupId = checkRecord.ClientTemplateBUGroupId;
                    entityDB.Entry(checkforTemplatesBU).State = EntityState.Modified;
                    entityDB.SaveChanges();
                }
                if (businessunit.BUSiteDetails != null)
                {
                    foreach (var site in businessunit.BUSiteDetails)
                    {
                        if (site.Ischecked == true)
                        {
                            var templateBUSites = entityDB.ClientTemplatesForBUSites.FirstOrDefault(rec => rec.ClientBusinessUnitSiteId == site.BUsiteId && rec.ClientTemplatesForBU.ClientTemplatesBUGroupPrice.GroupingFor == 2 && rec.ClientTemplatesForBU.ClientTemplateBUGroupId != null && rec.ClientTemplatesForBU.ClientTemplateId == clientTemplateId);

                            ClientTemplatesForBUSites Busites = new ClientTemplatesForBUSites();
                            if (templateBUSites == null && BU.ClientTemplateForBUId != 0)
                                Busites.ClientTemplateForBUId = BU.ClientTemplateForBUId;
                            else if (templateBUSites == null && BU.ClientTemplateForBUId == 0)
                                Busites.ClientTemplateForBUId = entityDB.ClientTemplatesForBU.FirstOrDefault(rec => rec.ClientTemplateId == clientTemplateId && rec.ClientBusinessUnitId == businessunit.BUId && rec.ClientTemplateBUGroupId != null).ClientTemplateForBUId;

                            else
                                Busites.ClientTemplateForBUId = BU.ClientTemplateForBUId;
                            Busites.ClientBusinessUnitSiteId = site.BUsiteId;
                            if (templateBUSites == null)
                            {
                                entityDB.ClientTemplatesForBUSites.Add(Busites);
                                entityDB.SaveChanges();
                            }
                            else// if busite record exist but changed into other group
                            {
                                templateBUSites.ClientTemplateForBUId = entityDB.ClientTemplatesForBU.FirstOrDefault(rec => rec.ClientTemplateId == clientTemplateId && rec.ClientBusinessUnitId == businessunit.BUId && rec.ClientTemplateBUGroupId != null && rec.ClientTemplatesBUGroupPrice.GroupingFor == 2).ClientTemplateForBUId;
                                entityDB.Entry(checkforTemplatesBU).State = EntityState.Modified;
                                entityDB.SaveChanges();
                            }
                        }
                        else
                        {

                            var templateBUSites = entityDB.ClientTemplatesForBUSites.FirstOrDefault(rec => rec.ClientBusinessUnitSiteId == site.BUsiteId && rec.ClientTemplatesForBU.ClientTemplatesBUGroupPrice.GroupingFor == 2 && rec.ClientTemplatesForBU.ClientTemplateId == clientTemplateId);
                            if (templateBUSites != null && templateBUSites.IsPaymentDone == null)
                            {
                                //sandhya on 08/06/2015
                                var Quizes = entityDB.SystemUsersOrganizationsQuizzes.Where(rec => rec.ClientTemplateId == clientTemplateId).ToList();
                                foreach (var Quiz in Quizes)
                                {
                                    var quizpayment = entityDB.QuizPayments.FirstOrDefault(rec => rec.SysUserOrgQuizId == Quiz.SysUserOrgQuizId);
                                    if (quizpayment != null)
                                    {
                                        var quizpaymentItems = entityDB.QuizPaymentsItems.FirstOrDefault(rec => rec.ClientTemplateForBUSitesId == templateBUSites.ClientTemplateForBUSitesId && rec.QuizPaymentId == quizpayment.QuizPaymentId);
                                        if (quizpaymentItems != null)
                                        {
                                            entityDB.Entry(quizpaymentItems).State = EntityState.Deleted;
                                            entityDB.SaveChanges();
                                        }
                                    }
                                }
                                entityDB.Entry(templateBUSites).State = EntityState.Deleted;
                                entityDB.SaveChanges();
                                //Ends>>
                            }
                        }
                    }
                }
            }
            Templates localTemplate = new Templates();
            var templateId = entityDB.ClientTemplates.Find(clientTemplateId).TemplateID;
            var templateRec = entityDB.Templates.Find(templateId);
            if (templateRec.TemplateStatus == 0)
            {
                templateRec.TemplateStatus = 1;
                entityDB.Entry(templateRec).State = EntityState.Modified;
                entityDB.SaveChanges();
            }
            var formgroupnos = form.BUDetails.Select(rec => rec.GroupNumber).Distinct();
            var dbgroupnos = entityDB.ClientTemplatesBUGroupPrice.Where(rec => rec.ClientTemplateID == clientTemplateId && !formgroupnos.Contains(rec.GroupPriceType)).ToList();
            foreach (var groups in dbgroupnos)
            {
                if (groups.GroupPriceType != 0)
                    entityDB.Entry(groups).State = EntityState.Deleted;
                entityDB.SaveChanges();
            }

            //sandhya on 08/03/2015
            var additionalSites = entityDB.ClientTemplatesForBUSites.Where(rec => rec.ClientTemplatesForBU.ClientTemplateId == clientTemplateId && rec.ClientTemplatesForBU.ClientTemplatesBUGroupPrice.GroupingFor == 2 && rec.IsPaymentDone == null).ToList();
            if (additionalSites.Count != 0)
            {
                foreach (var Sites in additionalSites)
                {
                    var Quizes = entityDB.SystemUsersOrganizationsQuizzes.Where(rec => rec.ClientTemplateId == clientTemplateId).ToList();
                    foreach (var Quiz in Quizes)
                    {
                        var quizpayment = entityDB.QuizPayments.FirstOrDefault(rec => rec.SysUserOrgQuizId == Quiz.SysUserOrgQuizId);
                        if (quizpayment != null)
                        {
                            var quizPaymentBUId = entityDB.QuizPaymentsItems.Where(rec => rec.QuizPaymentId == quizpayment.QuizPaymentId).ToList();
                            if (quizPaymentBUId.Select(rec => rec.AsOfClientTemplateBUGroupId).Contains(Sites.ClientTemplatesForBU.ClientTemplateBUGroupId))
                            {
                                var quizitemscheck = entityDB.QuizPaymentsItems.FirstOrDefault(rec => rec.ClientTemplateForBUSitesId == Sites.ClientTemplateForBUSitesId && rec.QuizPaymentId == quizpayment.QuizPaymentId);
                                if (quizitemscheck == null)
                                {
                                    QuizPaymentsItems QuizPaymentItems = new QuizPaymentsItems();
                                    QuizPaymentItems.QuizPaymentId = quizpayment.QuizPaymentId;
                                    QuizPaymentItems.ClientTemplateForBUId = Sites.ClientTemplateForBUId;
                                    QuizPaymentItems.ClientTemplateForBUSitesId = Sites.ClientTemplateForBUSitesId;
                                    QuizPaymentItems.AsOfClientTemplateBUGroupId = Sites.ClientTemplatesForBU.ClientTemplateBUGroupId;
                                    entityDB.QuizPaymentsItems.Add(QuizPaymentItems);
                                    entityDB.SaveChanges();
                                }
                            }
                        }
                    }
                }
            }
            //Ends >>

            return Redirect("PopUpCloseView");
        }
    }
}
