﻿/*
 *****************************************************
Page Created Date:  06/22/2013
Created By: Kiran Talluri
Purpose:
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FVGen3.Domain.Abstract;
using FVGen3.Domain.Entities;
using FVGen3.Domain.Concrete;
using Ninject;
using FVGen3.WebUI.Annotations;
using System.Data.Entity;
using System.Text.RegularExpressions;
using Resources;
using FVGen3.WebUI.Constants;

namespace FVGen3.WebUI.Controllers
{
    public class EmailSetupNotificationsController : BaseController
    {
        //
        // GET: /EmailSetupNotifications/
        EFDbContext entityDB = new EFDbContext();
        Messages.Messages messages = new Messages.Messages();
        public ActionResult Index()
        {
            return View();
        }

        /*
           Created Date:  06/24/2013   Created By: Kiran Talluri
           Purpose: To display the default form with default values
       */
        [SessionExpire]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "MyAccount", sideMenuName = "Notifications", ViewName = "EmailSetupNotifications")]
        [HttpGet]
        public ActionResult EmailSetupNotifications(long OrgEmailSetupId)
        {
            ViewBag.sendNotification = true;
            ViewBag.NotificationExists = false; // kiran on 12/04/2013
            // get the logged in user userid
            Guid loggedInUserId = (Guid)Session["UserId"];

            // To display the list of users of logged in user organization
            SystemUsersOrganizations sysUserOrg_currentUser = entityDB.SystemUsersOrganizations.FirstOrDefault(m => m.UserId == loggedInUserId);
            
            //--Kiran 7/12/2013-->>>
            //long currentUser_orgId = sysUserOrg_currentUser.SysUserOrganizationId;
            long currentUser_orgId = sysUserOrg_currentUser.OrganizationId;
            //--<<<<

            //var sysUserOrg_users =  (from sysUserOrg in entityDB.SystemUsersOrganizations.ToList()
            //                                           from contact in entityDB.Contact.ToList()
            //                                           where sysUserOrg.OrganizationId == currentUser_orgId && contact.UserId == sysUserOrg.UserId
            //                                           select new SelectListItem
            //                                           {
            //                                               Text = contact.FirstName + "," + contact.LastName,
            //                                               Value = contact.UserId.ToString()
            //                                           }).ToList();

            var SysUserOrgUserIds = entityDB.SystemUsersOrganizations.Where(sysUserOrg => sysUserOrg.OrganizationId == currentUser_orgId).Select(rec => rec.UserId);
            // Kiran on 03/18/2015
            //var sysUserOrg_users = (
            //                        from contact in entityDB.Contact.Where(rec => SysUserOrgUserIds.Contains(rec.UserId)).ToList()
            //                        select new SelectListItem
            //                        {
            //                            Text = contact.FirstName + "," + contact.LastName,
            //                            Value = contact.UserId.ToString()
            //                        }).ToList();

            var sysUserOrg_users = (from systemUser in entityDB.SystemUsers.Where(rec => SysUserOrgUserIds.Contains(rec.UserId)).ToList()
                                    select new SelectListItem
                                    {
                                        Text = systemUser.Contacts.FirstOrDefault() != null ? systemUser.Contacts[0].LastName + ", " + systemUser.Contacts[0].FirstName : systemUser.Email,
                                        Value = systemUser.UserId.ToString()
                                    }).ToList();
            // Kiran on 03/18/2015 Ends<<<

            // Kiran on 11/27/2013
            var emailTemplates = (from template in entityDB.EmailTemplates.Where(rec => rec.EmailUsedFor==0).OrderBy(row => row.Name).ToList()
                                  select new SelectListItem
                                  {
                                      Text = template.Name,
                                      Value = template.EmailTemplateID + ""
                                  }).ToList();
            ViewBag.VBEmailTemplates = emailTemplates;
            // Ends<<<

            LocalOrganizationsEmailSetupNotifications currentNotification = new LocalOrganizationsEmailSetupNotifications();
            if (OrgEmailSetupId == -1)
            {
                currentNotification.OrganizationId = currentUser_orgId;
                currentNotification.isInsert = true;
                var SysUserOrg_PrimaryUser_record = entityDB.SystemUsersOrganizations.FirstOrDefault(row => row.PrimaryUser == true && row.OrganizationId == currentUser_orgId);
                if (SysUserOrg_PrimaryUser_record != null)
                {
                    currentNotification.PrimaryUserId = SysUserOrg_PrimaryUser_record.UserId;
                }
            }

            // To display the notification details to user while updating
            else
            {
                currentNotification.isInsert = false;
                currentNotification.OrgEmailSetupId = OrgEmailSetupId;
                var OrganizationsEmailSetupNotifications_currentNotification = entityDB.OrganizationsEmailSetupNotifications.FirstOrDefault(row => row.OrgEmailSetupId == OrgEmailSetupId);

                currentNotification.OrganizationId = currentUser_orgId;
                currentNotification.SetupType = OrganizationsEmailSetupNotifications_currentNotification.SetupType;
                currentNotification.SetupTypeId = OrganizationsEmailSetupNotifications_currentNotification.SetupTypeId.ToString();
                currentNotification.FirstNotification = (int)OrganizationsEmailSetupNotifications_currentNotification.FirstNotification;
                currentNotification.SecondNotification = OrganizationsEmailSetupNotifications_currentNotification.SecondNotification;
                currentNotification.ThirdNotification = OrganizationsEmailSetupNotifications_currentNotification.ThirdNotification;

                // Kiran on 11/29/2013
                currentNotification.EmailTemplateType = OrganizationsEmailSetupNotifications_currentNotification.EmailTemplateType;
                currentNotification.EmailTemplateID = OrganizationsEmailSetupNotifications_currentNotification.EmailTemplateID;
                // Ends<<<

                var sendNotificationTo = OrganizationsEmailSetupNotifications_currentNotification.SendNotificationTo;
                foreach (var strSelectedNotifications in sendNotificationTo.Split(','))
                {
                    if (strSelectedNotifications == "Client")
                    {
                        currentNotification.SendNotificationToClient = true;
                    }
                    //if (strSelectedNotifications == "Vendor")
                    //{
                    //    currentNotification.SendNotificationToVendor = true;
                    //}
                    if (strSelectedNotifications == "First Verify")
                    {
                        currentNotification.SendNotificationToFirstVerify = true;
                    }
                    if (strSelectedNotifications == "Site Contacts")
                    {
                        currentNotification.SendNotificationToSiteContacts = true;
                    }
                }
                currentNotification.PrimaryUserId = entityDB.SystemUsersOrganizations.FirstOrDefault(row => row.PrimaryUser == true && row.OrganizationId == currentUser_orgId).UserId;               

            }
            ViewBag.VBsysUserOrg_Users = sysUserOrg_users;
            return View(currentNotification);
        }

        /*
           Created Date:  06/24/2013   Created By: Kiran Talluri
           Purpose: To add the emailsetupnotification to database & to enable primary user & to enable receivenotifications for logged in organization site contacts 
        */
        [SessionExpire]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "MyAccount", sideMenuName = "Notifications", ViewName = "EmailSetupNotifications")]
        [HttpPost]
        public ActionResult EmailSetupNotifications(LocalOrganizationsEmailSetupNotifications localEmailSetupNot)
        {
            ViewBag.sendNotification = true;
            ViewBag.NotificationExists = false; // kiran on 12/04/2013
            // get the logged in user userid
            Guid loggedInUserId = (Guid)Session["UserId"];

            // To display the list of users of logged in user orhanization
            SystemUsersOrganizations sysUserOrg_currentUser = entityDB.SystemUsersOrganizations.FirstOrDefault(m => m.UserId == loggedInUserId);
            long currentUser_orgId = sysUserOrg_currentUser.OrganizationId;

            //var sysUserOrg_users = (from sysUserOrg in entityDB.SystemUsersOrganizations.ToList()
            //                        from contact in entityDB.Contact.ToList()
            //                        where sysUserOrg.OrganizationId == currentUser_orgId && contact.UserId == sysUserOrg.UserId
            //                        select new SelectListItem
            //                        {
            //                            Text = contact.FirstName + "," + contact.LastName,
            //                            Value = contact.UserId.ToString()
            //                        }).ToList();

            var SysUserOrgUserIds = entityDB.SystemUsersOrganizations.Where(sysUserOrg => sysUserOrg.OrganizationId == currentUser_orgId).Select(rec => rec.UserId);
            // Kiran on 03/18/2015
            
            var sysUserOrg_users = (from systemUser in entityDB.SystemUsers.Where(rec => SysUserOrgUserIds.Contains(rec.UserId)).ToList()
                                    select new SelectListItem
                                    {
                                        Text = systemUser.Contacts.FirstOrDefault() != null ? systemUser.Contacts[0].LastName + ", " + systemUser.Contacts[0].FirstName : systemUser.Email,
                                        Value = systemUser.UserId.ToString()
                                    }).ToList();
            // Kiran on 03/18/2015 Ends<<<
            

            // Kiran on 11/27/2013
            var emailTemplates = (from template in entityDB.EmailTemplates.Where(rec => rec.EmailUsedFor==0).OrderBy(row => row.Name).ToList()
                                  select new SelectListItem
                                  {
                                      Text = template.Name,
                                      Value = template.EmailTemplateID + ""
                                  }).ToList().OrderBy(row => row.Text);
            ViewBag.VBEmailTemplates = emailTemplates;
            // Ends<<<

            ViewBag.VBsysUserOrg_Users = sysUserOrg_users;            
    
            var temp = "";
            if (localEmailSetupNot.SendNotificationToClient == true || localEmailSetupNot.SendNotificationToFirstVerify == true || localEmailSetupNot.SendNotificationToSiteContacts == true) // Kiran on 12/13/2013
            {
                ViewBag.sendNotification = true;
            }
            else
            {
                ViewBag.sendNotification = false;
                return View(localEmailSetupNot);
            }

            // To save the organization emailsetupnotification to Database while adding notification 
            if (localEmailSetupNot.isInsert == true)
            {
                // Kiran on 12/04/2013
                var NotificationRecord = entityDB.OrganizationsEmailSetupNotifications.FirstOrDefault(record => record.OrganizationId == localEmailSetupNot.OrganizationId && record.SetupType == localEmailSetupNot.SetupType && record.SetupTypeId == Convert.ToInt64(localEmailSetupNot.SetupTypeId)); // Kiran on 12/13/2013
                if (NotificationRecord != null)
                {
                    ViewBag.NotificationExists = true;
                    return View(localEmailSetupNot);
                }
                // Ends<<<

                OrganizationsEmailSetupNotifications orgEmailSetupNotification = new OrganizationsEmailSetupNotifications();

                orgEmailSetupNotification.OrganizationId = localEmailSetupNot.OrganizationId;
                orgEmailSetupNotification.SetupType = localEmailSetupNot.SetupType;
                orgEmailSetupNotification.SetupTypeId = Convert.ToInt64(localEmailSetupNot.SetupTypeId);
                orgEmailSetupNotification.FirstNotification = (int)localEmailSetupNot.FirstNotification;
                orgEmailSetupNotification.SecondNotification = localEmailSetupNot.SecondNotification;
                orgEmailSetupNotification.ThirdNotification = localEmailSetupNot.ThirdNotification;

                // Kiran on 11/27/2013
                orgEmailSetupNotification.EmailTemplateType = localEmailSetupNot.EmailTemplateType;
                orgEmailSetupNotification.EmailTemplateID = localEmailSetupNot.EmailTemplateID;
                // Ends<<<

                if (localEmailSetupNot.SendNotificationToClient == true)
                {
                    temp = "Client,";
                }

                //if (localEmailSetupNot.SendNotificationToVendor == true)
                //{
                //    temp = temp + "Vendor,";
                //}

                if (localEmailSetupNot.SendNotificationToFirstVerify == true)
                {
                    temp = temp+ "First Verify,";
                }

                if (localEmailSetupNot.SendNotificationToSiteContacts == true)
                {
                    temp = temp + "Site Contacts,";
                }

                orgEmailSetupNotification.SendNotificationTo = temp.Substring(0, temp.Length - 1);
                // To enable receivenotifications for the current organization site contacts
                if (localEmailSetupNot.SendNotificationToSiteContacts == true)
                {
                    //var currentOrg_busUnits = (from busUnits in entityDB.ClientBusinessUnits
                    //                            where busUnits.ClientId == currentUser_orgId
                    //                            select busUnits).ToList();
                    var currentOrg_busUnits = entityDB.ClientBusinessUnits.Where(rec=>rec.ClientId==currentUser_orgId).ToList();
                    foreach (var unit in currentOrg_busUnits)
                    {
                        var currentOrg_busUnitSites = unit.ClientBusinessUnitSites;

                        foreach (var busUnitSite in currentOrg_busUnitSites)
                        {
                            var currentOrg_SiteContacts = busUnitSite.ClientBusinessUnitSiteRepresentatives;

                            foreach (var siteContact in currentOrg_SiteContacts)
                            {
                                siteContact.ReceiveNotifications = true;
                            }
                        }
                    }
                }
                var currentNotification_oldPrimaryUser_record = entityDB.SystemUsersOrganizations.FirstOrDefault(record => record.PrimaryUser == true && record.OrganizationId == localEmailSetupNot.OrganizationId);
                if(currentNotification_oldPrimaryUser_record!=null)
                    currentNotification_oldPrimaryUser_record.PrimaryUser = false;

                // To enable the primary user of current organization                    
                var primaryUser_sysUserOrg = entityDB.SystemUsersOrganizations.FirstOrDefault(m => m.UserId == localEmailSetupNot.PrimaryUserId);
                if (primaryUser_sysUserOrg != null)
                {
                    primaryUser_sysUserOrg.PrimaryUser = true;
                    if (currentNotification_oldPrimaryUser_record != null)
                        entityDB.Entry(currentNotification_oldPrimaryUser_record).State = EntityState.Modified;
                }
                entityDB.OrganizationsEmailSetupNotifications.Add(orgEmailSetupNotification);
                entityDB.Entry(primaryUser_sysUserOrg).State = EntityState.Modified;
                entityDB.SaveChanges();
                return RedirectToAction("EmailSetupNotificationsList", "EmailSetupNotifications");
            }
                    
            // To update the organization emailsetupnotification to Database while updating  
            else 
            {
                // Kiran on 12/04/2013
                var selectedSetupTypeId = Convert.ToInt64(localEmailSetupNot.SetupTypeId); // Kiran on 12/13/2013
                var NotificationRecord = entityDB.OrganizationsEmailSetupNotifications.FirstOrDefault(record => record.OrganizationId == localEmailSetupNot.OrganizationId && record.SetupType == localEmailSetupNot.SetupType && record.SetupTypeId == selectedSetupTypeId && record.OrgEmailSetupId != localEmailSetupNot.OrgEmailSetupId); // Kiran on 12/13/2013
                if (NotificationRecord != null)
                {
                    ViewBag.NotificationExists = true;
                    return View(localEmailSetupNot);
                }
                // Ends<<<
                var currentNotification_record = entityDB.OrganizationsEmailSetupNotifications.FirstOrDefault(record => record.OrgEmailSetupId == localEmailSetupNot.OrgEmailSetupId);
                currentNotification_record.OrganizationId = localEmailSetupNot.OrganizationId;
                currentNotification_record.SetupType = localEmailSetupNot.SetupType;
                currentNotification_record.SetupTypeId = Convert.ToInt64(localEmailSetupNot.SetupTypeId);
                currentNotification_record.FirstNotification = (int)localEmailSetupNot.FirstNotification;
                currentNotification_record.SecondNotification = localEmailSetupNot.SecondNotification;
                currentNotification_record.ThirdNotification = localEmailSetupNot.ThirdNotification;

                // Kiran on 11/27/2013
                currentNotification_record.EmailTemplateType = localEmailSetupNot.EmailTemplateType;
                currentNotification_record.EmailTemplateID = localEmailSetupNot.EmailTemplateID;
                // Ends<<<

                if (localEmailSetupNot.SendNotificationToClient == true)
                {
                    temp = "Client,";
                }

                //if (localEmailSetupNot.SendNotificationToVendor == true)
                //{
                //    temp = temp + "Vendor,";
                //}

                if (localEmailSetupNot.SendNotificationToFirstVerify == true)
                {
                    temp = temp + "First Verify,";
                }

                if (localEmailSetupNot.SendNotificationToSiteContacts == true)
                {
                    temp = temp + "Site Contacts,";
                }

                currentNotification_record.SendNotificationTo = temp.Substring(0, temp.Length - 1);

                // To enable receivenotifications for the current organization site contacts
                if (localEmailSetupNot.SendNotificationToSiteContacts == true)
                {
                    //var currentOrg_busUnits = (from busUnits in entityDB.ClientBusinessUnits
                    //                            where busUnits.ClientId == currentUser_orgId
                    //                            select busUnits).ToList();
                    var currentOrg_busUnits = entityDB.ClientBusinessUnits.Where(rec => rec.ClientId == currentUser_orgId).ToList();
                    foreach (var unit in currentOrg_busUnits)
                    {
                        var currentOrg_busUnitSites = unit.ClientBusinessUnitSites;

                        foreach (var busUnitSite in currentOrg_busUnitSites)
                        {
                            var currentOrg_SiteContacts = busUnitSite.ClientBusinessUnitSiteRepresentatives;

                            foreach (var siteContact in currentOrg_SiteContacts)
                            {
                                siteContact.ReceiveNotifications = true;
                            }
                        }
                    }
                }

                // To disable receivenotifications for the current organization site contacts if unchecked the site contacts while updating
                if (localEmailSetupNot.SendNotificationToSiteContacts == false)
                {
                    //var currentOrg_busUnits = (from busUnits in entityDB.ClientBusinessUnits
                    //                            where busUnits.ClientId == currentUser_orgId
                    //                            select busUnits).ToList();
                    var currentOrg_busUnits = entityDB.ClientBusinessUnits.Where(rec => rec.ClientId == currentUser_orgId).ToList();
                    foreach (var unit in currentOrg_busUnits)
                    {
                        var currentOrg_busUnitSites = unit.ClientBusinessUnitSites;

                        foreach (var busUnitSite in currentOrg_busUnitSites)
                        {
                            var currentOrg_SiteContacts = busUnitSite.ClientBusinessUnitSiteRepresentatives;

                            foreach (var siteContact in currentOrg_SiteContacts)
                            {
                                siteContact.ReceiveNotifications = false;
                            }
                        }
                    }
                }


                // to disable the old primary user
                var currentNotification_oldPrimaryUser_record = entityDB.SystemUsersOrganizations.FirstOrDefault(record => record.PrimaryUser == true && record.OrganizationId == localEmailSetupNot.OrganizationId);
                currentNotification_oldPrimaryUser_record.PrimaryUser = false;

                // To enable the primary user of current organization                    
                var primaryUser_sysUserOrg = entityDB.SystemUsersOrganizations.FirstOrDefault(m => m.UserId == localEmailSetupNot.PrimaryUserId);
                primaryUser_sysUserOrg.PrimaryUser = true;

                entityDB.Entry(currentNotification_record).State = EntityState.Modified;
                entityDB.Entry(currentNotification_oldPrimaryUser_record).State = EntityState.Modified;
                entityDB.Entry(primaryUser_sysUserOrg).State = EntityState.Modified;
                entityDB.SaveChanges();
                return RedirectToAction("EmailSetupNotificationsList", "EmailSetupNotifications");
            }           

        }

        /*
            Created Date:  06/22/2013   Created By: Kiran Talluri
            Purpose: To display the document type or prequalification status values
        */
        public ActionResult getDocumentTypeorPrequalificationStatus(int documentorprequalification)
        {
            // if the document type is selected
            if (documentorprequalification == 0)
            {
                //var documentsList = from docs in entityDB.DocumentType.ToList()
                //                    where docs.DocumentExpires==true && docs.PrequalificationUseOnly==true
                //                    select new SelectListItem
                //                    {
                //                        Text = docs.DocumentTypeName,
                //                        Value = docs.DocumentTypeId.ToString()
                //                    };
                var documentsList = from docs in entityDB.DocumentType.Where(docs => docs.DocumentExpires == true && docs.PrequalificationUseOnly == true).ToList()
                                    select new SelectListItem
                                    {
                                        Text = docs.DocumentTypeName,
                                        Value = docs.DocumentTypeId.ToString()
                                    };

                ViewBag.VBdocumentsorPrequalificationsList = documentsList;

                return Json(documentsList);
            }
            // if the prequalification status is selected
            else 
            {
                var prequalificationStatusList = from prequalStatus in entityDB.PrequalificationStatus.ToList().Where(rec => rec.ShowInDashBoard == true)
                                                 select new SelectListItem
                                                 {
                                                     Text = prequalStatus.PrequalificationStatusName,
                                                     Value = prequalStatus.PrequalificationStatusId.ToString()
                                                 };

                ViewBag.VBdocumentsorPrequalificationsList = prequalificationStatusList;

                return Json(prequalificationStatusList);
            }            
        }

        [SessionExpire]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "MyAccount", sideMenuName = "Notifications", ViewName = "EmailSetupNotificationsList")]
        public ActionResult EmailSetupNotificationsList()
        {
            // get the logged in user userid
            Guid loggedInUserId = (Guid)Session["UserId"];

            // To display the list of users of logged in user orhanization
            SystemUsersOrganizations sysUserOrg_currentUser = entityDB.SystemUsersOrganizations.FirstOrDefault(m => m.UserId == loggedInUserId);
            long currentUser_orgId = sysUserOrg_currentUser.OrganizationId;

            // to retrieve all document types

            var currentUserOrg_documentTypes = entityDB.DocumentType.ToList();
            ViewBag.VBcurrentUserOrg_documentTypes = currentUserOrg_documentTypes;

            var currentUserOrg_prequalificationStatus = entityDB.PrequalificationStatus.ToList();
            ViewBag.VBcurrentUserOrg_prequalificationStatus = currentUserOrg_prequalificationStatus;

            var currentUserOrg_emailNotificationsList = entityDB.OrganizationsEmailSetupNotifications.Where(notifications => notifications.OrganizationId == currentUser_orgId).ToList();
                                                        
            foreach (var Notification in currentUserOrg_emailNotificationsList)
            {
                var NotificationTo="";
                foreach (var NotficationToLoc in Notification.SendNotificationTo.Split(','))
                {
                    if (NotficationToLoc.Equals(""))
                        continue;
                    try
                    {
                        Guid userId = new Guid(NotficationToLoc.Trim());

                        SystemUsers user = entityDB.SystemUsers.Find(userId);
                        try
                        {
                            Contact contact = user.Contacts[0];
                            NotificationTo += contact.LastName + " " + contact.FirstName+", ";
                        }
                        catch { NotificationTo += user.UserName+", "; }
                    }
                    catch { NotificationTo += NotficationToLoc+", "; }
                }
                Notification.SendNotificationTo = NotificationTo;
            }
            ViewBag.VBcurrentUserOrg_emailNotificationsList = currentUserOrg_emailNotificationsList.ToList();
            return View();
        }

        // Kiran 11/27/2013 
        // To provide email setup notifications for Admin 
        [HttpGet]
        [SessionExpire]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "MyAccount", sideMenuName = "Notifications", ViewName = "ManageNotifications")]
        public ActionResult ManageNotifications()
        {
            var clientsList = from client in entityDB.Organizations.Where(client=>(client.OrganizationType == OrganizationType.Client || client.OrganizationType == OrganizationType.SuperClient) && client.ShowInApplication == true).OrderBy(rec => rec.Name).ToList()
                              
                              select new SelectListItem
                              {
                                  Text = client.Name,
                                  Value = client.OrganizationID.ToString()
                              };
            //SelectListItem defaultItem = new SelectListItem();
            //defaultItem = new SelectListItem();
            //defaultItem.Text = Resources.Resources.ERI;
            //defaultItem.Value = "-1";
            var clientsList_defaultItem = clientsList.ToList();
            //clientsList_defaultItem.Insert(0, defaultItem);
            ViewBag.VBclientsList = clientsList_defaultItem;
            return View();
        }

        // Sumanth 10/27/2015 for FV-223
        // To provide email setup notifications for Admin 
        // starts 
        [HttpGet]
        [SessionExpire]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "MyAccount", sideMenuName = "InvitationNotifications", ViewName = "ManageInvitationNotifications")]
        public ActionResult ManageInvitationNotifications()
        {
            var clientsList = from client in entityDB.Organizations.Where(client => client.OrganizationType == "Client" && client.ShowInApplication == true).OrderBy(rec => rec.Name).ToList()

                              select new SelectListItem
                              {
                                  Text = client.Name,
                                  Value = client.OrganizationID.ToString()
                              };
            SelectListItem defaultItem = new SelectListItem();
            defaultItem = new SelectListItem();
            defaultItem.Text = "";
            defaultItem.Value = "-1";
            var clientsList_defaultItem = clientsList.ToList();
            clientsList_defaultItem.Insert(0, defaultItem);
            ViewBag.VBclientsList = clientsList_defaultItem;
            return View();
        }


        [SessionExpireForView(pageType = "partial")]
        [OutputCache(Duration = 0)]
        public ActionResult PartialManageInvitationNotificationsList(long clientId)
        {
            ViewBag.notValidId = false;
            if (clientId == -1)
            {
                ViewBag.notValidId = true;
                return PartialView();
            }
         
            var currentUserOrg_emailNotificationsList = entityDB.OrganizationsEmailSetupNotifications.Where(notifications => notifications.OrganizationId == clientId && notifications.SetupType==2 && (notifications.Status == true || notifications.Status == null)).ToList(); 
            ViewBag.VBcurrentUserOrg_emailNotificationsList = currentUserOrg_emailNotificationsList.ToList();
            return PartialView();
        }

        [SessionExpire]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "MyAccount", sideMenuName = "InvitationNotifications", ViewName = "AddEmailSetupInvitationNotification")]
        [HttpGet]
        public ActionResult AddEmailSetupInvitationNotification(long orgId, long orgEmailSetUpId)
        {
            ViewBag.sendNotification = true;
            ViewBag.NotificationExists = false; // Kiran on 12/04/2013
            LocalOrganizationsEmailSetupInvitationNotifications currentNotification = new LocalOrganizationsEmailSetupInvitationNotifications();
            if (orgEmailSetUpId == -1)
            {
                currentNotification.OrganizationId = orgId;
                currentNotification.isInsert = true;
                currentNotification.SetupType = 2;
            }
            else // To display the notification details to user while updating
            {
                currentNotification.isInsert = false;
                currentNotification.OrgEmailSetupId = orgEmailSetUpId;
                var OrganizationsEmailSetupNotifications_currentNotification = entityDB.OrganizationsEmailSetupNotifications.FirstOrDefault(row => row.OrgEmailSetupId == orgEmailSetUpId);

                currentNotification.OrganizationId = orgId;
                currentNotification.SetupType =2;
                currentNotification.SetupTypeId =null;
                currentNotification.FirstNotification = (int)OrganizationsEmailSetupNotifications_currentNotification.FirstNotification;
                currentNotification.SecondNotification = OrganizationsEmailSetupNotifications_currentNotification.SecondNotification;
                currentNotification.ThirdNotification = OrganizationsEmailSetupNotifications_currentNotification.ThirdNotification;
            }
            return View(currentNotification);
        }

        [SessionExpire]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "MyAccount", sideMenuName = "InvitationNotifications", ViewName = "AddEmailSetupInvitationNotification")]
        [HttpPost]
        public ActionResult AddEmailSetupInvitationNotification(LocalOrganizationsEmailSetupInvitationNotifications localEmailSetupNot)
        {
            ViewBag.sendNotification = true;
            ViewBag.NotificationExists = false; 
            var temp = "";
            OrganizationsEmailSetupNotifications orgEmailSetupNotification = new OrganizationsEmailSetupNotifications();
            if (localEmailSetupNot.isInsert == true)
            {
                var NotificationRecord = entityDB.OrganizationsEmailSetupNotifications.FirstOrDefault(record => record.OrganizationId == localEmailSetupNot.OrganizationId && record.SetupType ==2 && record.SetupTypeId == null); // Kiran on 12/13/2013
                if (NotificationRecord != null && NotificationRecord.Status == false)
                {
                    orgEmailSetupNotification = NotificationRecord;
                    orgEmailSetupNotification.Status = null;
                }
                else if (NotificationRecord != null)
                {
                    ViewBag.NotificationExists = true;
                    return View(localEmailSetupNot);
                }
                // Ends<<<

                orgEmailSetupNotification.OrganizationId = localEmailSetupNot.OrganizationId;
                orgEmailSetupNotification.SetupType = 2;
                orgEmailSetupNotification.SetupTypeId = null;
                orgEmailSetupNotification.FirstNotification = (int)localEmailSetupNot.FirstNotification;
                orgEmailSetupNotification.SecondNotification = localEmailSetupNot.SecondNotification;
                orgEmailSetupNotification.ThirdNotification = localEmailSetupNot.ThirdNotification;
               
                //sandhya on 08/10/2015
                if (NotificationRecord != null)
                    entityDB.Entry(orgEmailSetupNotification).State = EntityState.Modified;
                else
                    entityDB.OrganizationsEmailSetupNotifications.Add(orgEmailSetupNotification);
                //Ends>>
                entityDB.SaveChanges();
                return RedirectToAction("ClosePopup", localEmailSetupNot);
                // Ends<<<
            }
            else   // To update the organization emailsetupnotification to Database while updating  
            {
                // Kiran on 12/04/2013
                var selectedSetupTypeId = Convert.ToInt64(localEmailSetupNot.SetupTypeId); // Kiran on 12/13/2013
                var NotificationRecord = entityDB.OrganizationsEmailSetupNotifications.FirstOrDefault(record => record.OrganizationId == localEmailSetupNot.OrganizationId && record.SetupType == localEmailSetupNot.SetupType && record.OrgEmailSetupId != localEmailSetupNot.OrgEmailSetupId && record.SetupTypeId == selectedSetupTypeId); // Kiran on 12/13/2013
                if (NotificationRecord != null)
                {
                    ViewBag.NotificationExists = true;
                    return View(localEmailSetupNot);
                }
                // Ends<<<

                var currentNotification_record = entityDB.OrganizationsEmailSetupNotifications.FirstOrDefault(record => record.OrgEmailSetupId == localEmailSetupNot.OrgEmailSetupId);
                currentNotification_record.OrganizationId = localEmailSetupNot.OrganizationId;
                currentNotification_record.SetupType = 2;
                currentNotification_record.SetupTypeId = null;
                currentNotification_record.FirstNotification = (int)localEmailSetupNot.FirstNotification;
                currentNotification_record.SecondNotification = localEmailSetupNot.SecondNotification;
                currentNotification_record.ThirdNotification = localEmailSetupNot.ThirdNotification;
                                         
                entityDB.Entry(currentNotification_record).State = EntityState.Modified;
                entityDB.SaveChanges();
                return RedirectToAction("ClosePopup", localEmailSetupNot);
                // Ends<<<
            }

        }

        public ActionResult DeleteInvitationNotification(long OrgEmailSetupId)
        {
            var notificationSetUpRecord = entityDB.OrganizationsEmailSetupNotifications.FirstOrDefault(rec => rec.OrgEmailSetupId == OrgEmailSetupId);

            notificationSetUpRecord.Status = false;

            entityDB.Entry(notificationSetUpRecord).State = EntityState.Modified;
            entityDB.SaveChanges();

            return Redirect("../EmailSetupNotifications/PartialManageInvitationNotificationsList?clientId=" + notificationSetUpRecord.OrganizationId);
        }
  


       //sumanth ends on 10/27/2015

        [SessionExpireForView(pageType = "partial")]
        [OutputCache(Duration=0)]
        public ActionResult PartialManageNotificationsList(long clientId)
        {
            ViewBag.notValidId = false;            
            //if (clientId == -1)
            //{
            //    ViewBag.notValidId = true;
            //    return PartialView();
            //}
            var currentUserOrg_documentTypes = entityDB.DocumentType.ToList();
            ViewBag.VBcurrentUserOrg_documentTypes = currentUserOrg_documentTypes;

            var currentUserOrg_prequalificationStatus = entityDB.PrequalificationStatus.ToList();
            ViewBag.VBcurrentUserOrg_prequalificationStatus = currentUserOrg_prequalificationStatus;

            //var currentUserOrg_emailNotificationsList = from notifications in entityDB.OrganizationsEmailSetupNotifications
            //                                            where notifications.OrganizationId == clientId
            //                                            select notifications;
            var currentUserOrg_emailNotificationsList = entityDB.OrganizationsEmailSetupNotifications.Where(notifications => notifications.OrganizationId == clientId && notifications.SetupType!=2 && (notifications.Status == true || notifications.Status == null)).ToList(); // Kiran on 01/19/2015 // sumanth on 10/31/2015 for FVOS-104 added a filter to bring data other than setup type 2

            foreach (var Notification in currentUserOrg_emailNotificationsList)
            {
                var NotificationTo = "";
                foreach (var NotficationToLoc in Notification.SendNotificationTo.Split(','))
                {
                    if (NotficationToLoc.Equals(""))
                        continue;
                    try
                    {
                        Guid userId = new Guid(NotficationToLoc.Trim());

                        SystemUsers user = entityDB.SystemUsers.Find(userId);
                        try
                        {
                            Contact contact = user.Contacts[0];
                            NotificationTo += contact.LastName + " " + contact.FirstName+", ";
                        }
                        catch { NotificationTo += user.UserName+", "; }
                    }
                    catch { NotificationTo += NotficationToLoc + ", "; }
                }
                Notification.SendNotificationTo = NotificationTo;
            }
            ViewBag.VBcurrentUserOrg_emailNotificationsList = currentUserOrg_emailNotificationsList.ToList();
            return PartialView();
        }

        
        [SessionExpire]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "MyAccount", sideMenuName = "Notifications", ViewName = "AddEmailSetupNotification")]
        [HttpGet]
        public ActionResult AddEmailSetupNotification(long orgId, long orgEmailSetUpId)
        {
            ViewBag.sendNotification = true;
            ViewBag.NotificationExists = false; // Kiran on 12/04/2013

            var SysUserOrgUserIds=entityDB.SystemUsersOrganizations.Where(sysUserOrg=>sysUserOrg.OrganizationId == orgId).Select(rec=>rec.UserId);
            // Kiran on 03/18/2015
            var sysUserOrg_users = (
                                    from systemUser in entityDB.SystemUsers.Where(rec=>SysUserOrgUserIds.Contains(rec.UserId)).ToList()                                    
                                    select new SelectListItem
                                    {
                                        Text = systemUser.Contacts.FirstOrDefault() != null ? systemUser.Contacts[0].LastName + ", " + systemUser.Contacts[0].FirstName : systemUser.Email,
                                        Value = systemUser.UserId.ToString()
                                    }).ToList();
            // Kiran on 03/18/2015 Ends<<<

            // kiran on 9/22/2014
            SelectListItem defaultItem = new SelectListItem();
            defaultItem.Text = "Select user";
            defaultItem.Value = "-1";
            sysUserOrg_users.Insert(0, defaultItem);
            var SysUserOrgUserIdsForNonPrimary = entityDB.SystemUsersOrganizations.Where(sysUserOrg => sysUserOrg.OrganizationId == orgId && sysUserOrg.PrimaryUser==false).Select(rec => rec.UserId);
            
            // Kiran on 03/18/2015
            var additionalClientRecipients = (
                                              from systemUser in entityDB.SystemUsers.Where(rec => SysUserOrgUserIdsForNonPrimary.Contains(rec.UserId)).ToList()                                                                                  
                                              select new SelectListItem
                                              {
                                                  Text = systemUser.Contacts.FirstOrDefault() != null ? systemUser.Contacts[0].LastName + ", " + systemUser.Contacts[0].FirstName : systemUser.Email,
                                                  Value = systemUser.UserId.ToString()
                                              }).ToList();
            // Kiran on 03/18/2015 Ends<<<

            additionalClientRecipients.Add(defaultItem);
            ViewBag.VBadditionalClientRecipients = additionalClientRecipients;
            // Ends<<<

            var emailTemplates = (from template in entityDB.EmailTemplates.Where(rec => rec.EmailUsedFor == 0).OrderBy(row => row.Name).ToList()
                                  select new SelectListItem
                                  {
                                      Text = template.Name,
                                      Value = template.EmailTemplateID + ""
                                  }).ToList();
            ViewBag.VBEmailTemplates = emailTemplates;
            
            LocalOrganizationsEmailSetupNotifications currentNotification = new LocalOrganizationsEmailSetupNotifications();
            if (orgEmailSetUpId == -1)
            {
                currentNotification.OrganizationId = orgId;
                currentNotification.isInsert = true;
                var SysUserOrg_PrimaryUser_record = entityDB.SystemUsersOrganizations.FirstOrDefault(row => row.PrimaryUser == true && row.OrganizationId == orgId);
                if (SysUserOrg_PrimaryUser_record != null)
                {
                    currentNotification.PrimaryUserId = SysUserOrg_PrimaryUser_record.UserId;
                }
            }
            else // To display the notification details to user while updating
            {
                currentNotification.isInsert = false;
                currentNotification.OrgEmailSetupId = orgEmailSetUpId;
                var OrganizationsEmailSetupNotifications_currentNotification = entityDB.OrganizationsEmailSetupNotifications.FirstOrDefault(row => row.OrgEmailSetupId == orgEmailSetUpId);

                currentNotification.OrganizationId = orgId;
                currentNotification.SetupType = OrganizationsEmailSetupNotifications_currentNotification.SetupType;
                currentNotification.SetupTypeId = OrganizationsEmailSetupNotifications_currentNotification.SetupTypeId.ToString();
                currentNotification.FirstNotification = (int)OrganizationsEmailSetupNotifications_currentNotification.FirstNotification;
                currentNotification.SecondNotification = OrganizationsEmailSetupNotifications_currentNotification.SecondNotification;
                currentNotification.ThirdNotification = OrganizationsEmailSetupNotifications_currentNotification.ThirdNotification;
                currentNotification.EmailTemplateType = OrganizationsEmailSetupNotifications_currentNotification.EmailTemplateType;
                currentNotification.EmailTemplateID = OrganizationsEmailSetupNotifications_currentNotification.EmailTemplateID;
                
                var sendNotificationTo = OrganizationsEmailSetupNotifications_currentNotification.SendNotificationTo;
                ViewBag.VBsendNotificationTo = sendNotificationTo; // Kiran on 9/22/2014
                if (sendNotificationTo != null && !sendNotificationTo.Equals(""))
                {
                    foreach (var strSelectedNotifications in sendNotificationTo.Split(','))
                    {
                        if (strSelectedNotifications == "Client")
                        {
                            currentNotification.SendNotificationToClient = true;
                        }
                        if (strSelectedNotifications == "Vendor")
                        {
                            currentNotification.SendNotificationToVendor = true;
                        }
                        if (strSelectedNotifications == "First Verify")
                        {
                            currentNotification.SendNotificationToFirstVerify = true;
                        }
                        if (strSelectedNotifications == "Site Contacts")
                        {
                            currentNotification.SendNotificationToSiteContacts = true;
                        }
                    }
                }
                // Kiran on 4/3/2014
                if (entityDB.SystemUsersOrganizations.FirstOrDefault(row => row.PrimaryUser == true && row.OrganizationId == orgId) != null)
                currentNotification.PrimaryUserId = entityDB.SystemUsersOrganizations.FirstOrDefault(row => row.PrimaryUser == true && row.OrganizationId == orgId).UserId;
                // Ends<<<
            }
            ViewBag.VBsysUserOrg_Users = sysUserOrg_users;
            return View(currentNotification);
        }

        
        [SessionExpire]
        [HeaderAndSidebar(isPermissionRequired = true, headerName = "MyAccount", sideMenuName = "Notifications", ViewName = "AddEmailSetupNotification")]
        [HttpPost]
        public ActionResult AddEmailSetupNotification(LocalOrganizationsEmailSetupNotifications localEmailSetupNot)
        {
            ViewBag.sendNotification = true;
            ViewBag.NotificationExists = false; // Kiran on 12/04/2013

            if (localEmailSetupNot.SetupType == 3)
                ModelState.Remove("SetupTypeId");
            var SysUserOrgUserIds = entityDB.SystemUsersOrganizations.Where(sysUserOrg => sysUserOrg.OrganizationId == localEmailSetupNot.OrganizationId).Select(rec => rec.UserId);

            // Kiran on 03/18/2015
            var sysUserOrg_users = (
                                    from systemUser in entityDB.SystemUsers.Where(rec => SysUserOrgUserIds.Contains(rec.UserId)).ToList()
                                    select new SelectListItem
                                    {
                                        Text = systemUser.Contacts.FirstOrDefault() != null ? systemUser.Contacts[0].LastName + ", " + systemUser.Contacts[0].FirstName : systemUser.Email,
                                        Value = systemUser.UserId.ToString()
                                    }).ToList();
            // Kiran on 03/18/2015 Ends<<<

            // kiran on 9/22/2014
            SelectListItem defaultItem = new SelectListItem();
            defaultItem.Text = "Select user";
            defaultItem.Value = "-1";
            sysUserOrg_users.Insert(0, defaultItem);

            var SysUserOrgUserIdsForNonPrimary = entityDB.SystemUsersOrganizations.Where(sysUserOrg => sysUserOrg.OrganizationId == localEmailSetupNot.OrganizationId && sysUserOrg.PrimaryUser == false).Select(rec => rec.UserId);
            
            // Kiran on 03/18/2015
            var additionalClientRecipients = (
                                              from systemUser in entityDB.SystemUsers.Where(rec => SysUserOrgUserIdsForNonPrimary.Contains(rec.UserId)).ToList()
                                              select new SelectListItem
                                              {
                                                  Text = systemUser.Contacts.FirstOrDefault() != null ? systemUser.Contacts[0].LastName + ", " + systemUser.Contacts[0].FirstName : systemUser.Email,
                                                  Value = systemUser.UserId.ToString()
                                              }).ToList();
            // Kiran on 03/18/2015 Ends<<<

            additionalClientRecipients.Add(defaultItem);
            ViewBag.VBadditionalClientRecipients = additionalClientRecipients;
            // Ends<<<

            var emailTemplates = (from template in entityDB.EmailTemplates.Where(rec => rec.EmailUsedFor == 0).OrderBy(row => row.Name).ToList()
                                  select new SelectListItem
                                  {
                                      Text = template.Name,
                                      Value = template.EmailTemplateID + ""
                                  }).ToList();
            ViewBag.VBEmailTemplates = emailTemplates;
        
            ViewBag.VBsysUserOrg_Users = sysUserOrg_users;
                        
            var temp = "";
            //if (localEmailSetupNot.SendNotificationToClient == true || localEmailSetupNot.SendNotificationToFirstVerify == true || localEmailSetupNot.SendNotificationToSiteContacts == true) // Kiran on 12/13/2013
            //{
            //    ViewBag.sendNotification = true;
            //}
            //else
            //{
            //    ViewBag.sendNotification = false;
            //    return View(localEmailSetupNot);
            //}

            // To save the organization emailsetupnotification to Database while adding notification 
            // Kiran on 9/25/2014
            if ((localEmailSetupNot.SendNotificationToClient == true && localEmailSetupNot.AddClientRecipients == true && localEmailSetupNot.AdditionalClientRecipients == null) || (localEmailSetupNot.SendNotificationToClient == true && localEmailSetupNot.AddClientRecipients == true && localEmailSetupNot.AdditionalClientRecipients.Count == 0))
            {
                ModelState.AddModelError("AdditionalClientRecipients", "Please Select atleast one additional user");
                return View(localEmailSetupNot);
            }

            OrganizationsEmailSetupNotifications orgEmailSetupNotification = new OrganizationsEmailSetupNotifications();

            if (localEmailSetupNot.isInsert == true)
            {
                // Kiran on 12/04/2013
                var selectedSetupTypeId = Convert.ToInt64(localEmailSetupNot.SetupTypeId); // Kiran on 12/13/2013
                var NotificationRecord = entityDB.OrganizationsEmailSetupNotifications.FirstOrDefault(record => record.OrganizationId == localEmailSetupNot.OrganizationId && record.SetupType == localEmailSetupNot.SetupType && record.SetupTypeId == selectedSetupTypeId); // Kiran on 12/13/2013
                if (NotificationRecord != null && NotificationRecord.Status == false)
                {

                    orgEmailSetupNotification = NotificationRecord;
                    orgEmailSetupNotification.Status = null;
                    //ViewBag.NotificationExists = true;
                    //return View(localEmailSetupNot);
                }
                else if (NotificationRecord != null)
                {
                    ViewBag.NotificationExists = true;
                    return View(localEmailSetupNot);
                }
                // Ends<<<

                orgEmailSetupNotification.OrganizationId = localEmailSetupNot.OrganizationId;
                orgEmailSetupNotification.SetupType = localEmailSetupNot.SetupType;
                orgEmailSetupNotification.SetupTypeId = Convert.ToInt64(localEmailSetupNot.SetupTypeId);
                orgEmailSetupNotification.FirstNotification = (int)localEmailSetupNot.FirstNotification;
                orgEmailSetupNotification.SecondNotification = localEmailSetupNot.SecondNotification;
                orgEmailSetupNotification.ThirdNotification = localEmailSetupNot.ThirdNotification;
                orgEmailSetupNotification.EmailTemplateType = localEmailSetupNot.EmailTemplateType;
                if (localEmailSetupNot.EmailTemplateType == 1)
                orgEmailSetupNotification.EmailTemplateID = localEmailSetupNot.EmailTemplateID;
                    
                if (localEmailSetupNot.SendNotificationToClient == true)
                {
                    temp = "Client,";
                }

                if (localEmailSetupNot.SendNotificationToVendor == true)
                {
                    temp = temp + "Vendor,";
                }
                if (localEmailSetupNot.SendNotificationToFirstVerify == true)
                {
                    temp = temp + "First Verify,";
                }

                if (localEmailSetupNot.SendNotificationToSiteContacts == true)
                {
                    temp = temp + "Site Contacts,";
                }
                // Kiran on 9/25/2014
                if (localEmailSetupNot.SendNotificationToClient == true && localEmailSetupNot.AddClientRecipients == true)
                {
                    if (localEmailSetupNot.AdditionalClientRecipients.Count != 0 || localEmailSetupNot.AdditionalClientRecipients != null)
                    {
                        foreach (var user in localEmailSetupNot.AdditionalClientRecipients)
                        {
                            temp = temp + user + ",";
                        }
                    }
                }
                if (localEmailSetupNot.SendNotificationToClient == true || localEmailSetupNot.SendNotificationToFirstVerify == true || localEmailSetupNot.SendNotificationToSiteContacts == true || localEmailSetupNot.AddClientRecipients == true) // Kiran on 2/26/2014
                {
                    orgEmailSetupNotification.SendNotificationTo = temp; // Kiran on 9/22/2014
                }
                // Ends<<<
                else
                {
                    orgEmailSetupNotification.SendNotificationTo = "";
                }
                // To enable receivenotifications for the current organization site contacts
                if (localEmailSetupNot.SendNotificationToSiteContacts == true)
                {
                    var currentOrg_busUnits = entityDB.ClientBusinessUnits.Where(busUnits=>busUnits.ClientId == localEmailSetupNot.OrganizationId).ToList();

                    foreach (var unit in currentOrg_busUnits)
                    {
                        var currentOrg_busUnitSites = unit.ClientBusinessUnitSites;

                        foreach (var busUnitSite in currentOrg_busUnitSites)
                        {
                            var currentOrg_SiteContacts = busUnitSite.ClientBusinessUnitSiteRepresentatives;

                            foreach (var siteContact in currentOrg_SiteContacts)
                            {
                                siteContact.ReceiveNotifications = true;
                            }
                        }
                    }
                }
                // Kiran on 4/3/2014
                var currentNotification_oldPrimaryUser_record = entityDB.SystemUsersOrganizations.FirstOrDefault(record => record.PrimaryUser == true && record.OrganizationId == localEmailSetupNot.OrganizationId);
                if (currentNotification_oldPrimaryUser_record != null)
                    currentNotification_oldPrimaryUser_record.PrimaryUser = false;

                //sandhya on 08/10/2015
                if (NotificationRecord != null)
                    entityDB.Entry(orgEmailSetupNotification).State = EntityState.Modified;
                else
                    entityDB.OrganizationsEmailSetupNotifications.Add(orgEmailSetupNotification);
                //Ends>>

                // To enable the primary user of current organization                    
                var primaryUser_sysUserOrg = entityDB.SystemUsersOrganizations.FirstOrDefault(m => m.UserId == localEmailSetupNot.PrimaryUserId);
                if (primaryUser_sysUserOrg != null)
                {
                    primaryUser_sysUserOrg.PrimaryUser = true;
                    if (currentNotification_oldPrimaryUser_record != null)
                        entityDB.Entry(currentNotification_oldPrimaryUser_record).State = EntityState.Modified;
                }

                if (primaryUser_sysUserOrg != null)
                entityDB.Entry(primaryUser_sysUserOrg).State = EntityState.Modified;
                entityDB.SaveChanges();
                return RedirectToAction("ClosePopup", localEmailSetupNot);
                // Ends<<<
            }
            else   // To update the organization emailsetupnotification to Database while updating  
            {
                // Kiran on 12/04/2013
                var selectedSetupTypeId = Convert.ToInt64(localEmailSetupNot.SetupTypeId); // Kiran on 12/13/2013
                var NotificationRecord = entityDB.OrganizationsEmailSetupNotifications.FirstOrDefault(record => record.OrganizationId == localEmailSetupNot.OrganizationId && record.SetupType == localEmailSetupNot.SetupType && record.OrgEmailSetupId != localEmailSetupNot.OrgEmailSetupId && record.SetupTypeId == selectedSetupTypeId); // Kiran on 12/13/2013
                if (NotificationRecord != null)
                {
                    ViewBag.NotificationExists = true;
                    return View(localEmailSetupNot);
                }
                // Ends<<<

                var currentNotification_record = entityDB.OrganizationsEmailSetupNotifications.FirstOrDefault(record => record.OrgEmailSetupId == localEmailSetupNot.OrgEmailSetupId);
                currentNotification_record.OrganizationId = localEmailSetupNot.OrganizationId;
                currentNotification_record.SetupType = localEmailSetupNot.SetupType;
                if (localEmailSetupNot.SetupType == 3)
                    currentNotification_record.SetupTypeId = null;
                else
                    currentNotification_record.SetupTypeId = Convert.ToInt64(localEmailSetupNot.SetupTypeId);
                currentNotification_record.FirstNotification = (int)localEmailSetupNot.FirstNotification;
                currentNotification_record.SecondNotification = localEmailSetupNot.SecondNotification;
                currentNotification_record.ThirdNotification = localEmailSetupNot.ThirdNotification;
                currentNotification_record.EmailTemplateType = localEmailSetupNot.EmailTemplateType;
                if(localEmailSetupNot.EmailTemplateType == 1)
                currentNotification_record.EmailTemplateID = localEmailSetupNot.EmailTemplateID;
                    
                if (localEmailSetupNot.SendNotificationToClient == true)
                {
                    temp = "Client,";
                }
                if (localEmailSetupNot.SendNotificationToVendor == true)
                {
                    temp = temp + "Vendor,";
                }
                if (localEmailSetupNot.SendNotificationToFirstVerify == true)
                {
                    temp = temp + "First Verify,";
                }

                if (localEmailSetupNot.SendNotificationToSiteContacts == true)
                {
                    temp = temp + "Site Contacts,";
                }
                // Kiran on 9/25/2014
                if (localEmailSetupNot.SendNotificationToClient == true && localEmailSetupNot.AddClientRecipients == true)
                {
                    if (localEmailSetupNot.AdditionalClientRecipients.Count != 0 || localEmailSetupNot.AdditionalClientRecipients != null)
                    {
                        foreach (var user in localEmailSetupNot.AdditionalClientRecipients)
                        {
                            temp = temp + user + ",";
                        }
                    }
                }
                if (localEmailSetupNot.SendNotificationToClient == true || localEmailSetupNot.SendNotificationToFirstVerify == true || localEmailSetupNot.SendNotificationToSiteContacts == true || localEmailSetupNot.AddClientRecipients == true) // Kiran on 2/26/2014
                {
                    currentNotification_record.SendNotificationTo = temp; // Kiran on 9/22/2014
                }
                // Ends<<<
                else
                {
                    currentNotification_record.SendNotificationTo = "";
                }

                // To enable receivenotifications for the current organization site contacts
                if (localEmailSetupNot.SendNotificationToSiteContacts == true)
                {
                    var currentOrg_busUnits = entityDB.ClientBusinessUnits.Where(busUnits=>busUnits.ClientId == localEmailSetupNot.OrganizationId).ToList();

                    foreach (var unit in currentOrg_busUnits)
                    {
                        var currentOrg_busUnitSites = unit.ClientBusinessUnitSites;

                        foreach (var busUnitSite in currentOrg_busUnitSites)
                        {
                            var currentOrg_SiteContacts = busUnitSite.ClientBusinessUnitSiteRepresentatives;

                            foreach (var siteContact in currentOrg_SiteContacts)
                            {
                                siteContact.ReceiveNotifications = true;
                            }
                        }
                    }
                }

                // To disable receivenotifications for the current organization site contacts if unchecked the site contacts while updating
                if (localEmailSetupNot.SendNotificationToSiteContacts == false)
                {
                    var currentOrg_busUnits = entityDB.ClientBusinessUnits.Where(busUnits=>busUnits.ClientId == localEmailSetupNot.OrganizationId).ToList();

                    foreach (var unit in currentOrg_busUnits)
                    {
                        var currentOrg_busUnitSites = unit.ClientBusinessUnitSites;

                        foreach (var busUnitSite in currentOrg_busUnitSites)
                        {
                            var currentOrg_SiteContacts = busUnitSite.ClientBusinessUnitSiteRepresentatives;

                            foreach (var siteContact in currentOrg_SiteContacts)
                            {
                                siteContact.ReceiveNotifications = false;
                            }
                        }
                    }
                }

                // Kiran on 4/3/2014
                // to disable the old primary user
                var currentNotification_oldPrimaryUser_record = entityDB.SystemUsersOrganizations.FirstOrDefault(record => record.PrimaryUser == true && record.OrganizationId == localEmailSetupNot.OrganizationId);
                if (currentNotification_oldPrimaryUser_record != null)                
                currentNotification_oldPrimaryUser_record.PrimaryUser = false;

                // To enable the primary user of current organization                    
                var primaryUser_sysUserOrg = entityDB.SystemUsersOrganizations.FirstOrDefault(m => m.UserId == localEmailSetupNot.PrimaryUserId);
                if (primaryUser_sysUserOrg != null)
                {
                    primaryUser_sysUserOrg.PrimaryUser = true;
                    if (currentNotification_oldPrimaryUser_record != null)
                        entityDB.Entry(currentNotification_oldPrimaryUser_record).State = EntityState.Modified;
                }
                entityDB.Entry(currentNotification_record).State = EntityState.Modified;
                
                if (primaryUser_sysUserOrg != null)
                entityDB.Entry(primaryUser_sysUserOrg).State = EntityState.Modified;
                entityDB.SaveChanges();
                return RedirectToAction("ClosePopup", localEmailSetupNot);
                // Ends<<<
            }            

        }

        public ActionResult ClosePopup(LocalOrganizationsEmailSetupNotifications addEmailSetupNotification) // Passing clientId via addEmailSetupNotification object
        {
            return View(addEmailSetupNotification);
        }

        // Kiran on 9/22/2014
        public ActionResult GetAdditionalClientRecipients(long ClientId, Guid PrimaryUserId)
        {

            //var additionalClientUsers = (from sysUserOrg in entityDB.SystemUsersOrganizations.ToList()
            //                             from contact in entityDB.Contact.ToList()
            //                             where sysUserOrg.OrganizationId == ClientId && contact.UserId == sysUserOrg.UserId && sysUserOrg.UserId != PrimaryUserId
            //                             select new SelectListItem
            //                             {
            //                                 Text = contact.FirstName + "," + contact.LastName,
            //                                 Value = sysUserOrg.SystemUsers.Email.ToString()
            //                             }).ToList();

            var SysUserOrgUserIdsForNonPrimary = entityDB.SystemUsersOrganizations.Where(sysUserOrg => sysUserOrg.OrganizationId == ClientId && sysUserOrg.UserId != PrimaryUserId).Select(rec => rec.UserId);
            
            // Kiran on 03/18/2015
            var additionalClientUsers = (
                                              from systemUser in entityDB.SystemUsers.Where(rec => SysUserOrgUserIdsForNonPrimary.Contains(rec.UserId)).ToList()
                                              select new SelectListItem
                                              {
                                                  Text = systemUser.Contacts.FirstOrDefault() != null ? systemUser.Contacts[0].LastName + ", " + systemUser.Contacts[0].FirstName : systemUser.Email,
                                                  Value = systemUser.UserId.ToString()
                                              }).ToList();
            // Kiran on 03/18/2015 Ends<<<

            return Json(additionalClientUsers);
        }

        // Kiran on 01/19/2015
        public ActionResult DeleteNotification(long OrgEmailSetupId)
        {
            var notificationSetUpRecord = entityDB.OrganizationsEmailSetupNotifications.FirstOrDefault(rec => rec.OrgEmailSetupId == OrgEmailSetupId);

            notificationSetUpRecord.Status = false;

            entityDB.Entry(notificationSetUpRecord).State = EntityState.Modified;
            entityDB.SaveChanges();

            return Redirect("../EmailSetupNotifications/PartialManageNotificationsList?clientId=" + notificationSetUpRecord.OrganizationId);
        }
        // Ends<<
    }
}
