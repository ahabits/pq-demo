﻿//
// NinjectControllerFactory.cs ver 1.0 - Created by AhaApps 
// Author: Satish Reddy
//

using System;
using System.Web.Mvc;
using System.Web.Routing;
using Ninject;
using FVGen3.Domain.Entities;
using FVGen3.Domain.Abstract;
using System.Collections.Generic;
using System.Linq;
using FVGen3.BusinessLogic;
using FVGen3.BusinessLogic.Interfaces;
using Moq;
using FVGen3.Domain.Concrete;
using System.Web.Http;
using MyApplication.App_Start;

namespace FVGen3.WebUI.Infrastructure 
{
    public class NinjectControllerFactory : DefaultControllerFactory 
    {
        private IKernel ninjectKernel;
        public NinjectControllerFactory() 
        {
            ninjectKernel = new StandardKernel();
            AddBindings();

            GlobalConfiguration.Configuration.DependencyResolver = new NinjectDependencyResolver(ninjectKernel);
        }

        protected override IController GetControllerInstance(RequestContext requestContext, Type controllerType) 
        {
            return controllerType == null ? null : (IController)ninjectKernel.Get(controllerType);
        }

        private void AddBindings() 
        {
        // put bindings here
            /* Mock object
            Mock<IContactsRepository> mock = new Mock<IContactsRepository>();
            mock.Setup(m => m.Contact).Returns(new List<Contact> {
            new Contact { ContactId = 1, OrganizationId = 25, ContactName = "Satish1", ContactTitle = "ABC", FirstName = "Satish", LastName = "Reddy", ListOrder = 1, MiddleName = "G" },
            new Contact { ContactId = 2, OrganizationId = 25, ContactName = "Satish2", ContactTitle = "ABC", FirstName = "Satish", LastName = "Reddy", ListOrder = 1, MiddleName = "G" },
            new Contact { ContactId = 3, OrganizationId = 25, ContactName = "Satish3", ContactTitle = "ABC", FirstName = "Satish", LastName = "Reddy", ListOrder = 1, MiddleName = "G" }
            }.AsQueryable());
            ninjectKernel.Bind<IContactsRepository>().ToConstant(mock.Object);
             * */
            ninjectKernel.Bind<IContactsRepository>().To<EFContactRepository>();
            ninjectKernel.Bind<IOrganizationRepository>().To<EFOrganizationRepository>();
            ninjectKernel.Bind<ISystemUsersRepository>().To<EFSystemUsersRepository>();
            ninjectKernel.Bind<ISystemUserLogsRepository>().To<EFSystemUserLogsRepository>();
            ninjectKernel.Bind<IUtilityBusiness>().To<UtilityBusiness>(); 
            ninjectKernel.Bind<IBiddingInterestsBusiness>().To<BiddingInterestsBusiness>().WithConstructorArgument("httpContext", a => System.Web.HttpContext.Current);
            ninjectKernel.Bind<IDocumentBusiness>().To<DocumentBusiness>().WithConstructorArgument("httpContext", a => System.Web.HttpContext.Current);
            ninjectKernel.Bind<IProficiencyBusiness>().To<ProficiencyBusiness>().WithConstructorArgument("httpContext", a => System.Web.HttpContext.Current);
            ninjectKernel.Bind<IVendorBusiness>().To<VendorBusiness>().WithConstructorArgument("httpContext", a => System.Web.HttpContext.Current);
            ninjectKernel.Bind<IOrganizationBusiness>().To<OrganizationBusiness>().WithConstructorArgument("httpContext", a => System.Web.HttpContext.Current);
            ninjectKernel.Bind<IPrequalificationBusiness>().To<PrequalificationBusiness>().WithConstructorArgument("httpContext", a => System.Web.HttpContext.Current);
            ninjectKernel.Bind<IOnSiteOffSiteBusiness>().To<OnSiteOffSiteBusiness>().WithConstructorArgument("httpContext", a => System.Web.HttpContext.Current);
            ninjectKernel.Bind<ISystemUserBusiness>().To<SystemUserBusiness>().WithConstructorArgument("httpContext", a => System.Web.HttpContext.Current);
            ninjectKernel.Bind<IEmailTemplateBusiness>().To<EmailTemplateBusiness>().WithConstructorArgument("httpContext", a => System.Web.HttpContext.Current); 
            ninjectKernel.Bind<ITemplateToolBusiness>().To<TemplateToolBusiness>().WithConstructorArgument("httpContext", a => System.Web.HttpContext.Current);
            ninjectKernel.Bind<IClientBusiness>().To<ClientBusiness>().WithConstructorArgument("httpContext", a => System.Web.HttpContext.Current);
            ninjectKernel.Bind<IEmployeeBusiness>().To<EmployeeBusiness>();
            ninjectKernel.Bind<IReportsBusiness>().To<ReportsBusiness>();
            ninjectKernel.Bind<IPrequalificationSitesBusiness>().To<PrequalificationSitesBusiness>().WithConstructorArgument("httpContext", a=>System.Web.HttpContext.Current);
            ninjectKernel.Bind<IMigrationBusiness>().To<MigrationBusiness>().WithConstructorArgument("httpContext", a => System.Web.HttpContext.Current);
            ninjectKernel.Bind<IQuizBusiness>().To<QuizBusiness>();
        }
    }
}