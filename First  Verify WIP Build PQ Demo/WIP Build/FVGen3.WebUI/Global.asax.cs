﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Moq;
using System.Data.Entity;
using FVGen3.WebUI.Infrastructure;
using FVGen3.Domain.Concrete;
using System.Configuration;
using System.Net;
using AhaApps.Libraries.Logging;
using AhaApps.Libraries.Extensions;
using FVGen3.BusinessLogic;
using FVGen3.WebUI.Utilities.ErrorLogs;

namespace FVGen3.WebUI
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            // Set this to avoid the runtime error for Db changes
            Database.SetInitializer<EFDbContext>(null);
            BundleTable.EnableOptimizations = true;
            ControllerBuilder.Current.SetControllerFactory(new NinjectControllerFactory());
            
        }
        protected void Application_Error(object sender, EventArgs e)//golbal.asax.cs
        {

            var httpContext = ((MvcApplication)sender).Context;
            Exception exception = HttpContext.Current.Server.GetLastError();
            if (Response.StatusCode != 404 && Response.StatusCode != 302)
            {
                
                //try
                //{
                //    var pb = new UtilityBusiness();
                //    var errorModelWithSession = new ErrorLogUtility(System.Web.HttpContext.Current.Request)
                //                                    .GetErrorModel(exception, Session["UserId"]);
                //    pb.RecordApplicationError(errorModelWithSession);
                //}
                //catch { }
                Logger.Initialize();
                Logger.Log(LoggingLevel.Error, exception.FormatError(), exception);
                //var errorModelUsingRequestBase = new ErrorLogUtility(httpContext.Request).GetErrorModel(exception);
                //var errorModelUsingRequest = new ErrorLogUtility
                //(System.Web.HttpContext.Current.Request).GetErrorModel(exception);
            }
            //include session in error model

        }
        //protected void Application_Error(object sender, EventArgs e)
        //{
        //    if (Server != null)
        //    {
        //        Exception ex = Server.GetLastError();

        //        if (Response.StatusCode != 404) ;// && Response.StatusCode != 302)
        //        {
        //            Logger.Initialize();
        //            Logger.Log(LoggingLevel.Error, ex.FormatError(), ex);
        //        }
        //    }
        //}
    }
}