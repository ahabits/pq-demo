﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FVGen3.WebUI.Messages
{
    public class Messages
    {
        //Controller :- SystemUsers
        public string strCurrentPasswordRequired = "Current password required";
        public string strNewPasswordRequired = "New password required";
        public string strInvalidPassword = "Invalid Password";
        public string strNewPasswordlength = "The new password must be at least 10 characters long.";
        public string strNewAndOldMismatch = "The new password and confirmation password do not match.";
        public string strNewpasswordRequirements = "New password need atleast 1 upper character,1 lower character and 1 digit";//"Does not meet requirements";

        public string strinvalidUserName = "Invalid User Name or Password";
        public string strUserName = "Enter User Name";
        public string strPassword = "Enter Password";
        public string strEmptyFields = "Please Enter UserName and Password";
        public string strFirstNameRequired = "First Name is required!";
        public string strOrganizationNotExist = "Organization not exist for this user.";
        public string strLastNameRequired = "Last Name is required!";
        public string strTitleRequired = "Title is required!";
        public string strPhoneNumberRequired = "Phone Number is required!";
        public string strInvalidPhoneNumber = "Phone number seems to be invalid, Please enter correct number";
        public string strEmailRequired = "Email is required!";
        public string strInvalidEmail = "Email seems to be invalid, please enter valid email";

        public string strCouldNotContact = "Could not contact server. Please contact sytem admin!";
        //Controller :- AdminVendorsController
        public string strVendorRequired = "Vendor Name is required!";

        public string strCannotCreateAccount = "No Application Id is assigned for you.Please Contact System Administrator.";
        

    }
}