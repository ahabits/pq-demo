﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Resources;
using System.Web;

namespace Resources
{
    public static class LocalConstants
    {
        public static string GetTranslatedResource(string resource, string languageCode)
        {
            System.Globalization.CultureInfo currentCulture = new CultureInfo(languageCode);
            string translatedResource = Resources.ResourceManager.GetString(resource, currentCulture);

            return translatedResource;
        }
        public const long RESTRICTED_ACCESS_2_VIEW_ID = 1002;
        public const string RESTRICTED_ACCESS_2_VIEW_NAME = "Restricted Section Access2";
        public const long Clayco_ClientId = 58;
        public const string Clayco_Loc_SubsecName = "Field Orientation";
        public const long Login_As_Any_User_VIEW_ID = 1003;
        public const string Login_As_Any_User_VIEW_NAME = "Login As Any User";
        public const long Eri_User_View_Id = 1004;
        public const string Eri_User_View_Name = "Eri User";
        public const string Client = "Client";
        public const string Vendor = "Vendor";
        public const string Admin = "Admin";
        public const string SuperClient = "Super Client";
        public const string SuperAdmin = "Super Admin";
        public const string Employees = "employees";
        public const long Financial_Analytics_View_Id = 1005;
        public const string Financial_Analytics_View_Name = "Financial Analytics";
        public const long Financial_Analytics_Ratios_View_Id = 1006;
        public const string Financial_Analytics_Ratios_View_Name = "Financial Analytics PM Dashboard Ratios";
        public const long Supporting_Documents_View_Id = 1007;
        public const string Supporting_Documents_View_Name = "Supporting Documents";
        public const long Tag_Number_View_Id = 1008;
        public const string Tag_Number_View__Name = "Tag Number";
        public const string Employee_Traning = "Employee Training";
        public const string BusinessUnit = "Business Unit";
        public const string Vendor_Number_Assigned = "vendor number assigned:";
        public const string Default_Value = "Default";
        public const long Badge_Number_View_Id = 1009;
        public const string Badge_Number_View__Name = "Badge Number";
        public const long ClientID = 58;
        public const long OrmatID = 13159;
        public const string Geographicarea = "All U.S.";
        public const string VendorSuperUser= "A70DCA84-DECC-4909-8AB6-46F6E10B8BA2";
        public const long ClientEmployeeId = 1010;
        public const string ClientEmployee_View__Name = "Client Employee";
        public const long SineproId= 1011;
        public const string Sinepro_View_Name = "Sine Pro Checkin/Checkout";
        public const long PQComments_ID = 1012;
        public const string PQComments_View_Name = "Comment in Questionnaire Banner";
        public const string Allstr = "All";
        public const long Checkout = 13;
        public const long OrmatCheckout = 16;
        public const int New_Invitation = 0;
        public const int All = -1;
        public const int PQInvitationId = 2;
        public static readonly Guid ClaycotrainingTemplateID = new Guid("F11AD79E-B3E3-41BA-AB02-E1677AF92AAE");
        public static class SectionPermissionType
        {
            public const int RESTRICTED_ACCESS_2 = 5;
        }
        public static class SubSectionTypeCondition
        {
            public const string ERIClientSelection = "ERIClientSelection";
            public const string LocationApproval = "LocationApproval";
            public const string PQDocuments2 = "PQDocuments2";
        }
        //public const long ERI = -1;
        public const long PQDocuments2 = 12;
        public static class ReportTypes
        {
            public const string SubSectionReport = "SubSectionReport";
            public const string BiddingReporting = "BiddingReporting";
            public const string ClaycoBiddingReporting = "ClaycoBiddingReporting";
        }
        public const string PQ_DEFAULT_BG_COLOR = "#75787E";//e5e6e8
        public const string EMAIL_FORMAT =
            @"^([\w\-\.\']+)@((\[([0-9]{1,3}\.){3}[0-9]{1,3}\])|(([\w\-]+\.)+)([a-zA-Z]{2,8}))$";
       
    }
   public enum DocumentModificationType
    {
        Modified,
        Deleted,
        Replaced,
        StatusChanged,
        Added
    }
    public enum ClientNotificationType
    {
       
        Inviatation=1,
        Cronjob=2

    }
    public enum QuizLegends
    {
        quizfailed, quizpassed, ExpireColor, quizrenewal, quiznottaken, quizinprocess, mixed
    }
    public enum InvitationTypes
    {
        NewInvitation,ExpiredInvitation,OutSideInvitation

    }
    public class Languages
    {
        public const long English = 1;
        public const long Spanish = 2;
    }

    public class PQDocumentStatus
    {
        public const int Visible = 0;
        public const int Hide = 1;
        public const int Delete = 2;
    }

    public class SectionTypes
    {
        public const int IsProfile = 0;
        public const int IsGeneral = 1;
        public const int IsPayment = 3;
        public const int IsPrequalificationDocument = 4;
        public const int IsProficiencyCapability = 5;
        public const int IsBidding = 6;
        public const int IsBuSites = 7;
        public const int IsPrequalificationRequirements = 31;

        public const int IsLast = 99;
        public const int ExceptionProbation = 34;
        public const int IsMasterPurchaseAgreement = 33;
    }

    public class SubSectionTypeCondition
    {
        public const string PrequalComments = "PrequalComments";
        public const string SupportingDocument = "SupportingDocument";
        public const string BiddingReporting = "BiddingReporting";
        public const string ClaycoBiddingReporting = "ClaycoBiddingReporting";
        public const string ProficiencyReporting = "ProficiencyReporting";
        public const string ProductCategoriesReporting = "ProductCategoriesReporting";
        public const string SubSectionReport = "SubSectionReport";
        public const string PrequalificationPayment = "PrequalificationPayment";
        public const string ClientSign = "ClientSign";
        public const string VendorSign = "VendorSign";
        public const string YearWiseStats = "YearWiseStats";
        public const string SitePrequalification = "SitePrequalification";
    }

    public class TableReference
    {
        public const string UnitedStates = "UnitedStates";
        public const string ClientContractTypes = "clientcontracttypes";
    }
    public enum PermissionType
    {
        All,
        Admin,
        Client,
        Vendor
    }
    public enum PowerBiReportKeywords
    {
        LoginUserId,
        OrganizationId
        
    }
}