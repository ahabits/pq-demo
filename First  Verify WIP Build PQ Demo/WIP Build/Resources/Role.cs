﻿

using System;
using System.Collections.Generic;

namespace FVGen3.WebUI.Constants
{
    public class OrganizationType
    {
        public const string Admin = "Admin";
        public const string Client = "Client";
        public const string Vendor = "Vendor";
        public const string SuperClient = "Super Client";
    }
    public class PQSubSectionType
    {
        public const string VendorSign = "VendorSign";
        public const string ClientSign = "ClientSign";
        public const string Addendum = "Addendum"; 
    }
    public class EmployeeRole
    {
       public static List<string> EmployeeRoleId =new List<string>() { "A4D58875-8EFC-4300-8B54-D0A9E732B891", "67DCC9B4-B069-4CCA-93DC-CD8D02EF8E62" };
        public static string VendorEmpRoleId = "A4D58875-8EFC-4300-8B54-D0A9E732B891";
        public static string ClientEmployeeID = "67DCC9B4-B069-4CCA-93DC-CD8D02EF8E62";
    }
} 