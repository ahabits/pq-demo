﻿/*
Page Created Date:  04/10/2017
Created By: Rajesh Pendela
Purpose: All helper operations related to prequalification module will be performed here
Version: 1.0
****************************************************
*/

using FVGen3.DataLayer;
using FVGen3.DataLayer.DTO;
using FVGen3.Domain.Concrete;
using FVGen3.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Data.Entity.SqlServer;
using FVGen3.Common;

namespace FVGen3.BusinessLogic
{
    public class PrequalificationHelper
    {
        /// <summary>
        /// To get the question column data
        /// </summary>
        /// <param name="preQualificationId"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public List<QuestionColumnData> GetQuestionColumnData(long preQualificationId, string key)
        {
            string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["EFDbContext"].ConnectionString;
            var Dal = new PrequalificationDAL(connectionString);
            return Dal.GetQuestionColumnData(preQualificationId, key);
        }
        /// <summary>
        /// To get the specific prequalification contact information
        /// </summary>
        /// <param name="pqId"></param>
        /// <returns></returns>
        public Object PQVendorDetails(long pqId)
        {
            using (var context = new EFDbContext())
            {
                var result = (from pq in context.Prequalification.Where(rec => rec.PrequalificationId == pqId)
                              join pqu in context.PrequalificationUserInput on pq.PrequalificationId equals pqu.PreQualificationId
                              join qcd in context.QuestionColumnDetails on pqu.QuestionColumnId equals qcd.QuestionColumnId
                              join q in context.Questions on qcd.QuestionId equals q.QuestionID
                              join tss in context.TemplateSubSections on q.SubSectionId equals tss.SubSectionID
                              where q.ReportForKeyword == "VendorDetails"
                              select new { tss.SubSectionID, tss.SubSectionName, q.QuestionText, pqu.UserInput }).OrderBy(rec => rec.SubSectionID).ToList();
                return result;
            }
        }
        /// <summary>
        /// To get the vendor prequalification payment summary
        /// </summary>
        /// <param name="vendornumber"></param>
        /// <returns></returns>
        public List<PQPaymentDetails> GetPrequalificationPayment(long vendornumber)
        {
            using (var entity = new EFDbContext())
            {
                //var res = from pp in entityDB.PrequalificationPayments
                //          join p in entityDB.Prequalification on pp.PrequalificationId equals p.PrequalificationId
                //          join c in entityDB.Organizations on p.ClientId equals c.OrganizationID
                //          join ct in entityDB.ClientTemplates on p.ClientTemplateId equals ct.ClientTemplateID
                //          join t in entityDB.Templates on ct.TemplateID equals t.TemplateID
                //          join psite in entityDB.PrequalificationSites.GroupBy(r => new { r.PrequalificationId, r.PrequalificationPaymentsId })
                //          .Select(r => new { r.Key.PrequalificationId, r.Key.PrequalificationPaymentsId, Name = r.Select(r1 => r1.Client.Name) })
                //          on new { Key1 = p.PrequalificationId, Key2 = pp.PrequalificationPaymentsId }
                //          equals new { Key1 = psite.PrequalificationId ?? 0, Key2 = psite.PrequalificationPaymentsId ?? 0 }
                //          into gj
                //          from psites in gj.DefaultIfEmpty()

                //where p.VendorId == vendornumber
                //          select new PQPaymentDetails
                //          { InvoiceAmount = pp.InvoiceAmount,
                //              PaymentReceivedAmount = pp.PaymentReceivedAmount,
                //              PaymentReceivedDate = pp.PaymentReceivedDate,
                //              PaymentTransactionId = pp.PaymentTransactionId ?? "",
                //              Name = c.Name,
                //              TemplateName = t.TemplateName + (pp.PaymentCategory == 1 ? "(0)" : "") + (t.TemplateCode != null ? " ( " + (t.TemplateCode) + " )" : ""),
                //              PaymentCategory = pp.PaymentCategory ?? 0,
                //              ClientNames = psites.Name
                //          };
                //var result = res.OrderByDescending(rec => rec.PaymentReceivedDate).ToList();
                //foreach (var r in result.Where(r => r.PaymentCategory == 1))
                //{
                //    var clientNames = string.Join(",", r.ClientNames);
                //    if (r.PaymentCategory == 1)
                //        r.TemplateName = r.TemplateName.Replace("0", clientNames);
                //}

                //return result;
                var VendorSearchSQL = "with cte as" +
 "(select  Distinct psites.PrequalificationId,psites.ClientId,client.Name from  PrequalificationSites as psites " +
"join Organizations as client on psites.ClientId = client.OrganizationID)" +

",DATA AS (SELECT PrequalificationId, ClientNames= STUFF(( SELECT ',' + Name from CTE AS T1" +
                              " where T1.PrequalificationId = T2.PrequalificationId FOR XML PATH('')), 1, 1, '') from CTE AS T2 group by PrequalificationId)" +




                    "select op.periodstatus, pp.PaymentReceivedAmount,pp.PaymentReceivedDate,iif(pp.PaymentTransactionId is null,'',pp.PaymentTransactionId) as PaymentTransactionId,o.name," +
       "t.TemplateName+ IIf(o.organizationtype='Super Client'and pp.PaymentReceivedAmount !=0 , '('+psites.ClientNames+')','')+iif(t.TemplateCode is not null,'('+t.TemplateCode+')','') as templatename," +
       "pp.PaymentCategory,IIf((convert (nvarchar(500),op.PrequalificationPeriodStart,101) +'-'+convert (nvarchar(500),op.PrequalificationPeriodClose,101)) is null,IIF((select  PrequalificationPeriodStart  from OrganizationsPrequalificationOpenPeriod where PrequalificationPeriodStart<=pp.PaymentReceivedDate and PrequalificationPeriodClose>=pp.PaymentReceivedDate and  periodstatus= 0 and vendorid=" + vendornumber + ") is null,'Final status not assigned','Open Amount'),(convert (nvarchar(500),op.PrequalificationPeriodStart,101) +'-'+convert (nvarchar(500),op.PrequalificationPeriodClose,101))) as FeeCapPeriod " +
        " from PrequalificationPayments as pp" +
       " join Prequalification as p on p.PrequalificationId = pp.PrequalificationId" +
       " join Organizations as o on o.OrganizationID = p.ClientId" +
       " join ClientTemplates as ct on ct.ClientTemplateID = p.ClientTemplateId" +
       " join templates as t on t.TemplateID = ct.TemplateID" +
       " left join DATA AS Psites on p.PrequalificationId=psites.PrequalificationId" +
       " left join FeeCapLog as f on f.PrequalificationId = p.PrequalificationId" +
       " left join OrganizationsPrequalificationOpenPeriod as op on op.OrgPrequalificationPeriodId = f.FeecapPeriodId" +
       " where p.VendorId =" + vendornumber +" and(o.OrganizationType <> 'client' OR pp.PaymentReceivedAmount <> 0 OR PaymentCategory <> 1) order by PaymentReceivedDate desc";
                var resultList = entity.Database.SqlQuery<PQPaymentDetails>(VendorSearchSQL, new SqlParameter("@vendornumber", vendornumber));
                 return resultList.ToList();

                
            }
        }
        
        /// <summary>
        /// To get the vendor employees payment summary
        /// </summary>
        /// <param name="vendornumber"></param>
        /// <returns></returns>
        public List<QuizPaymentDetails> GetQuizPayments(long vendornumber)
        {
            using (var entityDB = new EFDbContext())
            {
                var res = (from qp in entityDB.QuizPayments
                           join suoq in entityDB.SystemUsersOrganizationsQuizzes on qp.SysUserOrgQuizId equals suoq.SysUserOrgQuizId
                           join suo in entityDB.SystemUsersOrganizations on suoq.SysUserOrgId equals suo.SysUserOrganizationId
                           join c in entityDB.Contact on suo.UserId equals c.UserId
                           join ct in entityDB.ClientTemplates on suoq.ClientTemplateId equals ct.ClientTemplateID
                           join t in entityDB.Templates on ct.TemplateID equals t.TemplateID
                           where suoq.VendorId == vendornumber && qp.PaymentReceivedAmount != 0
                           //join p in entityDB.LatestPrequalification on new { ct.ClientID, suoq.VendorId } equals new { p.ClientId, p.VendorId }
                           select new QuizPaymentDetails
                           {
                               PaymentReceivedAmount = qp.PaymentReceivedAmount,
                               QuizName = t.TemplateName,
                               PaymentReceivedDate = qp.PaymentReceivedDate,
                               PaymentTransactionId = qp.PaymentTransactionId ?? "",
                               EmployeeName = c.FirstName + " " + c.LastName,
                               TransactionMetaData = qp.TransactionMetaData,
                               ClientName = suoq.Client.Name,
                             //  Feeperiod = (SqlFunctions.DateName("day", p.PrequalificationStart) + "/" + SqlFunctions.DateName("month", p.PrequalificationStart) + "/" + SqlFunctions.DateName("year", p.PrequalificationStart)) + "-" + (SqlFunctions.DateName("day", p.PrequalificationFinish) + "/" + SqlFunctions.DateName("month", p.PrequalificationFinish) + "/" + SqlFunctions.DateName("year", p.PrequalificationFinish))

                           }).ToList();

                var annuals = from pretrainingfee in entityDB.PrequalificationTrainingAnnualFees
                              join p in entityDB.LatestPrequalification on pretrainingfee.PrequalificationId equals p.PrequalificationId
                              where p.VendorId == vendornumber && pretrainingfee.AnnualFeeAmount != 0
                              select new QuizPaymentDetails
                              {
                                  PaymentReceivedAmount = pretrainingfee.AnnualFeeAmount,
                                  QuizName = "PQAnnualFee",
                                  PaymentReceivedDate = pretrainingfee.FeeReceivedDate,
                                  PaymentTransactionId = pretrainingfee.FeeTransactionId ?? "",
                                  EmployeeName = Resources.Resources.Payment_AnnualFee,
                                  TransactionMetaData = SqlFunctions.StringConvert((decimal)pretrainingfee.AnnualFeeAmount),
                                  ClientName = p.Client.Name,
                                  //Feeperiod = (SqlFunctions.DateName("day", p.PrequalificationStart) + "/" + SqlFunctions.DateName("month", p.PrequalificationStart) + "/" + SqlFunctions.DateName("year", p.PrequalificationStart)) + "-" + (SqlFunctions.DateName("day", p.PrequalificationFinish) + "/" + SqlFunctions.DateName("month", p.PrequalificationFinish) + "/" + SqlFunctions.DateName("year", p.PrequalificationFinish))
                              };
                res.AddRange(annuals);
                var x = res.OrderByDescending(rec => rec.PaymentReceivedDate).ToList();
                return x;
             
            }
        }
        /// <summary>
        /// To log the PQ fee cap payment
        /// </summary>
        /// <param name="preQualificationId"></param>
        /// <param name="amountPaid"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        public bool RecordFeecapLog(long preQualificationId, decimal amountPaid, EFDbContext entity)
        {
            if (!entity.FeeCapLog.Any(rec => rec.PrequalificationId == preQualificationId))
            {
                var feecapLog = new FeeCapLog();
                feecapLog.CreatedDate = DateTime.Now;
                feecapLog.HasAccumulated = false;
                feecapLog.PQAmount = amountPaid;
                feecapLog.PrequalificationId = preQualificationId;
                entity.FeeCapLog.Add(feecapLog);
            }
            return true;
        }
        /// <summary>
        /// To get the vendor open payment summary
        /// </summary>
        /// <param name="vendorId"></param>
        /// <returns></returns>
        public List<VendorOpenPeriod> GetVendorOpenPeriods(long vendorId)
        {
            using (var entityDB = new EFDbContext())
            {
                return entityDB.OrganizationsPrequalificationOpenPeriod.Where(rec => rec.VendorId == vendorId && rec.PeriodStatus == 0).Select(rec => new VendorOpenPeriod() { PeriodStart = rec.PrequalificationPeriodStart, PeriodClose = rec.PrequalificationPeriodClose, PeriodStatus = rec.PeriodStatus, TotalPaymentRecieved = rec.TotalPaymentRecieved }).OrderBy(rec => rec.PeriodStart).ToList();
            }
        }
        /// <summary>
        /// To get the vendor total fee liable to pay
        /// </summary>
        /// <param name="vendorId"></param>
        /// <returns></returns>
        public List<FeecapOpenAmount> GetVendorFeecapOpenAmount(long vendorId)
        {
            using (var entityDB = new EFDbContext())
            {
               
                return entityDB.FeeCapLog.Where(rec => rec.Prequalification.VendorId == vendorId && rec.PQAmount != 0 && !rec.HasAccumulated).
                    Select(rec => new FeecapOpenAmount()
                    {
                        TemplateName = rec.Prequalification.ClientTemplates.Templates.TemplateName + " ( " +
                            (rec.Prequalification.ClientTemplates.Templates.TemplateCode ?? "") + " )",
                        Amount = rec.Prequalification.TotalPaymentReceivedAmount.Value
                    }).ToList();
            }
        }
        /// <summary>
        /// To update the PQ fee as accumulated
        /// </summary>
        /// <param name="PrequalificationId"></param>
        /// <param name="entityDB"></param>
        /// <param name="paymentType"></param>
        /// <param name="amount"></param>
        /// <returns></returns>
        public bool AccumulateFeeCap(long PrequalificationId, EFDbContext entityDB, int paymentType = 0, decimal amount = 0)
        {
            var log = entityDB.FeeCapLog.FirstOrDefault(rec => rec.PrequalificationId == PrequalificationId && (paymentType == 1 || rec.HasAccumulated == false));
            if (log != null)
            {
                var prequalification_loc = entityDB.Prequalification.Find(PrequalificationId);

                var recievedPreqPrice = amount;
                if (amount == 0 && paymentType == 0)
                    recievedPreqPrice = entityDB.PrequalificationPayments
                        .FirstOrDefault(rec =>
                            rec.PrequalificationId == PrequalificationId &&
                            rec.PaymentCategory == 0).PaymentReceivedAmount ?? 0;

                if (prequalification_loc != null)
                {
                    var currentDate = prequalification_loc.PrequalificationStart;//This is nothing but period start. Currently, status 9 started date.
                    if (paymentType != 0)
                        currentDate = DateTime.Now;
                    var oPeriod = entityDB.OrganizationsPrequalificationOpenPeriod.FirstOrDefault(rec => rec.VendorId == prequalification_loc.VendorId && rec.PrequalificationPeriodStart <= currentDate && rec.PrequalificationPeriodClose >= currentDate);
                    if (oPeriod == null)
                    {
                        
                        var month=Int32.Parse(ConfigurationManager.AppSettings["openPeriodMonth"]);
                        var date = Int32.Parse(ConfigurationManager.AppSettings["openPeriodDay"]);
                        var openPeriod = new DateTime(currentDate.Year,month,date);
                        oPeriod = new OrganizationsPrequalificationOpenPeriod
                        {
                            VendorId = prequalification_loc.VendorId,
                            PrequalificationPeriodStart = openPeriod,
                            PrequalificationPeriodClose = openPeriod.AddYears(1).AddDays(-1),
                            PeriodStatus = 0,
                            TotalPaymentRecieved = recievedPreqPrice
                        };
                        
                        entityDB.OrganizationsPrequalificationOpenPeriod.Add(oPeriod);
                        log.Notes = "New period started";
                    }
                    else
                    {
                        oPeriod.TotalPaymentRecieved += recievedPreqPrice;
                        log.Notes = "";
                        entityDB.Entry(oPeriod).State = EntityState.Modified;
                    }
                    log.Notes += "Total Payment Received :" + oPeriod.TotalPaymentRecieved;
                    if (oPeriod.TotalPaymentRecieved > decimal.Parse(ConfigurationManager.AppSettings["PrequalificationAnnualPeriodAmount"]))
                    {
                        SendMail.sendMail(ConfigurationManager.AppSettings["FeeCapExceedMailTo"],
                            $"Fee cap exceeded to {prequalification_loc.Vendor.Name} for the period of {oPeriod.PrequalificationPeriodStart} - {oPeriod.PrequalificationPeriodClose} and period id is {oPeriod.OrgPrequalificationPeriodId} ", $"Fee cap exceeded to {prequalification_loc.Vendor.Name}");
                    }
                    //To Close all OrganizationsPrequalificationOpenPeriod

                    foreach (var openPeriod in entityDB.OrganizationsPrequalificationOpenPeriod.Where(rec =>(rec.PrequalificationPeriodClose < currentDate && rec.PeriodStatus == 0 && rec.VendorId==prequalification_loc.VendorId)||(rec.PeriodStatus==0 && rec.PrequalificationPeriodClose<DateTime.Now)).ToList())
                    {
                        openPeriod.PeriodStatus = 1;
                        entityDB.Entry(openPeriod).State = EntityState.Modified;
                    }
                    log.FeecapPeriodId = oPeriod.OrgPrequalificationPeriodId;
                }

                log.UpdatedDate = DateTime.Now;
                log.HasAccumulated = true;
            }
            return true;
        }
        /// <summary>
        /// To get the PQ status information for specific vendor and client
        /// </summary>
        /// <param name="clientid"></param>
        /// <param name="vendorid"></param>
        /// <returns></returns>
        public PQData GetLatestPrequalification(long clientid, long vendorid)
        {
            var entityDB = new EFDbContext();
            PQData pqstatus = new PQData();
            var pqnewlist = entityDB.LatestPrequalification.FirstOrDefault(rec => rec.ClientId == clientid && rec.VendorId == vendorid);

            if (pqnewlist != null)
            {
                pqstatus.PrequalificationStatusName = pqnewlist.PrequalificationStatus.PrequalificationStatusName;
                pqstatus.PrequalificationStatusId = pqnewlist.PrequalificationStatusId;
            }

            return pqstatus;
        }
        /// <summary>
        /// To get the specifc field value
        /// </summary>
        /// <param name="tableRef"></param>
        /// <param name="tableReferenceField"></param>
        /// <param name="TableRefFieldValueDisplay"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        public string GetUserInputAnswer(string tableRef, string tableReferenceField, string TableRefFieldValueDisplay, string filter)
        {
            if (string.IsNullOrEmpty(filter))
                return "";
            string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["EFDbContext"].ConnectionString;
            string sql = "select " + TableRefFieldValueDisplay + " from " + tableRef + " where " + tableReferenceField + " =" + filter;
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                var cmd = new SqlCommand(sql, conn);
                return cmd.ExecuteScalar() + "";
            }
        }

        public long? GetBuPaymentId(long buId, long pqId)
        {
            using (var entity = new EFDbContext())
            {
                var pq = entity.Prequalification.Find(pqId);
                if (pq != null)
                {
                    var pqClientTemplate = pq.ClientTemplates.Templates.ClientTemplates.Select(r => r.ClientTemplateID)
                        .ToList();
                    var clientTemplateBuGroupId = entity.ClientTemplatesForBU.FirstOrDefault(r =>
                            r.ClientBusinessUnitId == buId && pqClientTemplate.Contains((Guid) r.ClientTemplateId))
                        .ClientTemplateBUGroupId;
                    var buIds = entity.ClientTemplatesForBU
                        .Where(r => r.ClientTemplateBUGroupId == clientTemplateBuGroupId)
                        .Select(r => r.ClientBusinessUnitId);
                    var pqSite = entity.PrequalificationSites.FirstOrDefault(r =>
                        buIds.Contains(r.ClientBusinessUnitId) && r.PrequalificationId == pqId &&
                        r.PrequalificationPaymentsId != null);
                    if (pqSite != null)
                        return pqSite.PrequalificationPaymentsId;
                }
            }
            return null;
        }
        //public string GetUserInputAnswer(string tableRef, string tableReferenceField, string TableRefFieldValueDisplay, string filter)
        //{
        //    if (string.IsNullOrEmpty(filter))
        //        return "";
        //    string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["EFDbContext"].ConnectionString;
        //    string sql = "select " + TableRefFieldValueDisplay + " from " + tableRef + " where " + tableReferenceField + " =" + filter;
        //    using (SqlConnection conn = new SqlConnection(connectionString))
        //    {
        //        conn.Open();

        //        var cmd = new SqlCommand(sql, conn);
        //        return cmd.ExecuteScalar() + "";
        //    }
        //}
    }
}
