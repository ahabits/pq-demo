﻿
/*
Page Created Date:  06/30/2017
Created By: Rajesh Pendela
Purpose: All Business operations related to Templates will be performed here
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using FVGen3.BusinessLogic.Interfaces;
using FVGen3.DataLayer.DTO;
using FVGen3.Domain.Concrete;
using FVGen3.Domain.Entities;
using AutoMapper;
using System.Web;
using FVGen3.Domain.LocalModels;
using System.Threading.Tasks;

namespace FVGen3.BusinessLogic
{
    public class TemplateToolBusiness : BaseBusinessClass, ITemplateToolBusiness
    {
        /// <summary>
        /// To configure the agreement subsection
        /// </summary>
        /// <param name="agreement"></param>
        public void ConfigureAgreementEmail(AgreementSubsectionModel agreement)
        {
            using (var context = new EFDbContext())
            {
                var config =
                    context.AgreementSubSectionConfiguration.FirstOrDefault(
                        r => r.SubSectionId == agreement.SubSectionId) ?? new AgreementSubSectionConfiguration
                        {
                            CreatedBy = agreement.LoginUser,
                            CreatedDate = DateTime.Now
                        };
                config.EmailTemplateId = agreement.EmailTemplateId;
                var notificationSendTo = "Vendor,";
                if (agreement.IsClient)
                    notificationSendTo += "Client,";
                if (agreement.IsAdmin)
                    notificationSendTo += "Admin,";
                notificationSendTo = notificationSendTo.Substring(0, notificationSendTo.Length - 1);
                config.SendNotificationTo = notificationSendTo;
                config.SubSectionId = agreement.SubSectionId;
                config.UpdatedBy = agreement.LoginUser;
                config.UpdatedDate = DateTime.Now;
                if (config.Id == Guid.Empty)
                    context.AgreementSubSectionConfiguration.Add(config);
                else
                    context.Entry(config).State = EntityState.Modified;
                context.SaveChanges();
            }
        }

        /// <summary>
        /// To get the specifc subsection configuration
        /// </summary>
        /// <param name="subSectionId"></param>
        /// <returns></returns>
        public AgreementSubsectionModel GetAgreementConfiguration(long subSectionId)
        {
            using (var context = new EFDbContext())
            {
                var config =
                    context.AgreementSubSectionConfiguration.FirstOrDefault(
                        r => r.SubSectionId == subSectionId);
                if (config == null) return new AgreementSubsectionModel();
                var result = new AgreementSubsectionModel
                {
                    IsAdmin = config.SendNotificationTo.Contains("Admin"),
                    IsClient = config.SendNotificationTo.Contains("Client"),
                    IsVendor = config.SendNotificationTo.Contains("Vendor"),
                    EmailTemplateId = config.EmailTemplateId ?? -1,
                    SubSectionId = subSectionId
                };
                return result;
            }
        }
        /// <summary>
        /// To log the agreement subsection
        /// </summary>
        /// <param name="emailLog"></param>
        /// <returns></returns>
        public AgreementSubSectionLog CreateAgreementSubsectionLog(LocalEmailComposeModel emailLog)
        {
            using (var context = new EFDbContext())
            {
                var agreementSubSectionConfiguration = context.AgreementSubSectionConfiguration.FirstOrDefault(r => r.SubSectionId == emailLog.SubSectionId);
                if (agreementSubSectionConfiguration != null)
                {
                    var agreementLog = new AgreementSubSectionLog()
                    {
                        ToAddress = emailLog.ToAddress,
                        CCAddress = emailLog.CCAddress,
                        PQId = emailLog.PrequalificationId,
                        AgreementSubSecId = agreementSubSectionConfiguration.Id,
                        SentBy = emailLog.LoginUser,
                        SentDate = DateTime.Now
                    };
                    context.AgreementSubSectionLog.Add(agreementLog);
                    context.SaveChanges();
                    return agreementLog;
                }
            }
            return null;
        }
        /// <summary>
        /// To get the client signature
        /// </summary>
        /// <param name="subSectionId"></param>
        /// <returns></returns>
        public ClientSignature GetClientSignature(long subSectionId)
        {
            using (var context = new EFDbContext())
            {
                var subSec = context.TemplateSubSections.Find(subSectionId);
                var data = subSec.Questions.SelectMany(r => r.QuestionColumnDetails).Select(r => r.QuestionColumnId);
                var signInfo = context.ClientUserSignature.Where(r => data.Contains(r.QuestionColumnId)).ToList();

                var sign = new ClientSignature
                {
                    Sign = signInfo.FirstOrDefault(r => r.QuestionText == "Signed:") != null ? signInfo.FirstOrDefault(r => r.QuestionText == "Signed:").QuestionColumnIdValue : "",
                    Title = signInfo.FirstOrDefault(r => r.QuestionText == "Title:") != null ? signInfo.FirstOrDefault(r => r.QuestionText == "Title:").QuestionColumnIdValue : ""
                };
                sign.Sign = sign.Sign ?? "";
                sign.Title = sign.Title ?? "";
                return sign;
            }
        }
        /// <summary>
        /// To update the client signature
        /// </summary>
        /// <param name="sign"></param>
        /// <returns></returns>
        public string UpdateSign(ClientSignature sign)
        {
            using (var context = new EFDbContext())
            {
                var subSec = context.TemplateSubSections.Find(sign.SubSectionId);
                var data = subSec.Questions.SelectMany(r => r.QuestionColumnDetails).Select(r => r.QuestionColumnId);
                var signInfo = context.ClientUserSignature.Where(r => data.Contains(r.QuestionColumnId)).ToList();
                var signdata = signInfo.FirstOrDefault(r => r.QuestionText == "Signed:");
                if (signdata != null)
                {
                    signdata.QuestionColumnIdValue = sign.Sign;
                    context.Entry(signdata).State = EntityState.Modified;
                }
                var titledata = signInfo.FirstOrDefault(r => r.QuestionText == "Title:");
                if (titledata != null)
                {
                    titledata.QuestionColumnIdValue = sign.Title;
                    context.Entry(titledata).State = EntityState.Modified;
                }
                context.SaveChanges();
                return "Success";
            }
        }
        /// <summary>
        /// To get the specific template questions
        /// </summary>
        /// <param name="Templateid"></param>
        /// <returns></returns>
        public List<LocQuestions> GetTemplateQuestins(Guid Templateid)
        {
            using (var context = new EFDbContext())
            {
                var tempsectype = new List<int> { 0, 3, 4, 5, 6, 7, 21, 31, 33, 36, 39 };
                var TemplateSectionID = context.TemplateSections.Where(r => r.TemplateID == Templateid && !tempsectype.Contains(r.TemplateSectionType.Value)).Select(r => r.TemplateSectionID).ToList();
                var qusids = context.VendorDetailQuestions.Where(r => r.TemplateId == Templateid && r.Type == 1).Select(r => r.QuestionId).ToList();


                var questions = context.TemplateSubSections.Where(r => TemplateSectionID.Contains(r.TemplateSectionID)).SelectMany(r => r.Questions).Where(r => r.Visible == true && r.TemplateSubSections.Visible == true).OrderBy(r => r.TemplateSubSections.TemplateSections.DisplayOrder).ThenBy(r => r.TemplateSubSections.DisplayOrder).ThenBy(r => r.DisplayOrder).Select(r => new LocQuestions()
                {
                    QuestionText = r.QuestionText,
                    QuestionId = r.QuestionID,
                    SubSectionName = r.TemplateSubSections.SubSectionName,
                    Value = r.VendorDetailQuestions.FirstOrDefault(r1 => r1.QuestionId == r.QuestionID && r1.Type == 1).Value,
                    IsMax = r.VendorDetailQuestions.FirstOrDefault(r1 => r1.QuestionId == r.QuestionID && r1.Type == 1).IsMax != null ? r.VendorDetailQuestions.FirstOrDefault(r1 => r1.QuestionId == r.QuestionID && r1.Type == 1).IsMax : false

                }).ToList();
                questions.Where(r => qusids.Contains(r.QuestionId)).ToList().ForEach(r => r.IsExist = true);
                return questions;
            }
        }
        /// <summary>
        /// To update the specific subsection name
        /// </summary>
        /// <param name="SubsecId"></param>
        /// <param name="SubsectionName"></param>
        /// <returns></returns>
        public string ChangeSubSectionName(long SubsecId, string SubsectionName)
        {
            using (var context = new EFDbContext())
            {
                var subSection = context.TemplateSubSections.Find(SubsecId);
                subSection.SubSectionName = SubsectionName;

                context.Entry(subSection).State = EntityState.Modified;

                context.SaveChanges();

                return "Ok";
            }
        }
      
        public List<KeyValuePair<long, string>> GetTemplateSections(Guid Template)
        {
            using (var entity = new EFDbContext())
            {
                var TemplateType = new List<int>() { 3, 99, 31, 0, 4, 7 };
                return entity.TemplateSections.Where(r => r.TemplateID == Template && !TemplateType.Contains(r.TemplateSectionType.Value) && r.Visible == true && (r.HideFromPrequalification == false || r.HideFromPrequalification == null))
                    .Select(r => new { r.SectionName, r.TemplateSectionID }).Distinct().OrderBy(r => r.SectionName)
                    .ToList()
                    .Select(r =>
                        new KeyValuePair<long, string>(r.TemplateSectionID, r.SectionName))
                    .ToList();

            }
        }
        //public List<KeyValuePair<long, string>> GetTemplateSubSections(long SectionId)
        //{
        //    using (var entity = new EFDbContext())
        //    {
        //        return entity.TemplateSubSections.Where(r => r.TemplateSectionID == SectionId)
        //            .Select(r => new { r.SubSectionName, r.SubSectionID }).Distinct().OrderBy(r => r.SubSectionName)
        //            .ToList()
        //            .Select(r =>
        //                new KeyValuePair<long, string>(r.SubSectionID, r.SubSectionName))
        //            .ToList();

        //    }
        //}
        /// <summary>
        /// To get the specific template SubSections
        /// </summary>
        /// <param name="SectionId"></param>

        /// <returns></returns>
        public List<KeyValuePair<long, string>> GetTemplateSubSections(long SectionId)
        {
            using (var entity = new EFDbContext())
            {
                return (from Tss in entity.TemplateSubSections.Where(Tss => Tss.TemplateSectionID == SectionId && Tss.Visible == true)
                        join Q in entity.Questions on Tss.SubSectionID equals Q.SubSectionId
                        select new
                        {
                            Tss.SubSectionID,
                            Tss.SubSectionName
                        }).Distinct().OrderBy(tss => tss.SubSectionName).ToList()
                     .Select(r =>
                         new KeyValuePair<long, string>(r.SubSectionID, r.SubSectionName))
                     .ToList();

            }
        }
        public List<Templates> GetTemplates()
        {
            using (var entity = new EFDbContext())
            {
                return entity.Templates.Where(temp => temp.TemplateType == 0).OrderBy(temp => temp.TemplateName).ToList();

            }
        }
        public List<Questions> GetQuestions(long SubSectionId)
        {
            using (var entity = new EFDbContext())
            {
                var x = entity.Questions.Where(r => r.SubSectionId == SubSectionId).ToList();
                return x;
            }
        }
        public LocalMappedQuestions GetMappedQuestions(long SSubsectionId, long DSubsectionId)
        {
            var result = new LocalMappedQuestions();
            result.SDQuestion = new List<SDQuestion>();
            using (var entity = new EFDbContext())
            {
                result.SourceQuestions = entity.Questions.Where(r => r.SubSectionId == SSubsectionId)
                    .Select(r => new QuestionsList()
                    {
                        SourceQuestionId = r.QuestionID,
                        SourceQuestionText = r.QuestionText,
                        SourceBankId = r.QuestionBankId.Value,
                        SourceNoOfColumns = r.NumberOfColumns,
                        SourceDisplayorder = r.DisplayOrder,
                        //SourceQuestionControltype = r.QuestionColumnDetails.Any()?r.QuestionColumnDetails.FirstOrDefault().QuestionControlTypeId:0
                    }).ToList();
                result.DestinationQuestions = entity.Questions.Where(r => r.SubSectionId == DSubsectionId)
                    .Select(r => new QuestionsList()
                    {
                        DestinationQuestionId = r.QuestionID,
                        DestinationQuestionText = r.QuestionText,
                        DestinationBankId = r.QuestionBankId.Value,
                        
                        //DestinationNoOfColumns = r.NumberOfColumns,
                        DestinationDisplayorder = r.DisplayOrder,
                        //SourceQuestionControltype = r.QuestionColumnDetails.Any()?r.QuestionColumnDetails.FirstOrDefault().QuestionControlTypeId:0
                    }).ToList();
                foreach (var Questions in result.SourceQuestions)
                {

                    var MatchedQuestion = result.DestinationQuestions.Where(r => r.DestinationBankId == Questions.SourceBankId).ToList();
                    
                        var DestinationQuestion =
                            //= MatchedQuestion.Count == 1 ? 
                            //MatchedQuestion.FirstOrDefault().DestinationQuestionId : 
                            MatchedQuestion.FirstOrDefault(r => MatchedQuestion.Count==1 || 
                            r.DestinationDisplayorder == Questions.SourceDisplayorder);
                    var mappedQuestion = entity.MappedQuestions.FirstOrDefault(r => r.SourceQuestionId == Questions.SourceQuestionId
                          && r.SourceSubSectionId == SSubsectionId && r.DestinationSubSectionId == DSubsectionId);
                    if (DestinationQuestion == null)
                    {if (mappedQuestion != null)
                        result.SDQuestion.Add(new SDQuestion() { SourceQuestionId = mappedQuestion.SourceQuestionId, DestinationQuestionId = mappedQuestion.DestinationQuestionId });
                        continue;
                    }
                        var DestinationQuestionId =
                           DestinationQuestion.DestinationQuestionId;

                        MappedQuestions CopyQuestionsList = new MappedQuestions();
                        
                        if (mappedQuestion == null)
                        {
                            CopyQuestionsList.SourceQuestionId = Questions.SourceQuestionId;

                            CopyQuestionsList.DestinationQuestionId = DestinationQuestionId;
                            CopyQuestionsList.SourceSubSectionId = SSubsectionId;
                            CopyQuestionsList.DestinationSubSectionId = DSubsectionId;
                            CopyQuestionsList.IsMapped = true;
                            entity.MappedQuestions.Add(CopyQuestionsList);
                           result.SDQuestion.Add(new SDQuestion() { SourceQuestionId = Questions.SourceQuestionId, DestinationQuestionId = DestinationQuestionId });
                        }
                     else   result.SDQuestion.Add(new SDQuestion() { SourceQuestionId = mappedQuestion.SourceQuestionId, DestinationQuestionId = mappedQuestion.DestinationQuestionId });

                   
                    entity.SaveChanges();
                }
            }
            return result;
        }
        public string IsCompatibleQuestion(long DQuestionId, long SQuestionId)
        {
            using (var entity = new EFDbContext())
            {
                if (DQuestionId == 0) { return "true"; }
                var DQuestion = GetQuestion(DQuestionId);
                var SQuestion= GetQuestion(SQuestionId);
                var SControlType = entity.QuestionColumnDetails.FirstOrDefault(r => r.QuestionId == SQuestion.QuestionID) != null ? entity.QuestionColumnDetails.FirstOrDefault(r => r.QuestionId == SQuestion.QuestionID).QuestionControlTypeId : 0;
                var DControlType = entity.QuestionColumnDetails.FirstOrDefault(r => r.QuestionId == DQuestion.QuestionID) != null ? entity.QuestionColumnDetails.FirstOrDefault(r => r.QuestionId == DQuestion.QuestionID).QuestionControlTypeId : 0;
                if (SQuestion.NumberOfColumns == DQuestion.NumberOfColumns && SControlType == DControlType)
                    return "true";
                else
                    return "";
            }
        }

        public void mappingQuestions(LocalCopyQuestions CopyQuestions)
        {
            using (var entity = new EFDbContext())
            {
                foreach (var Questions in CopyQuestions.QuestionsList)
                {
                    MappedQuestions CopyQuestionsList = new MappedQuestions();
                    var HasQuestion = entity.MappedQuestions.FirstOrDefault(r => r.SourceQuestionId == Questions.SourceQuestionId && r.SourceSubSectionId == CopyQuestions.SourceSubSectionId&&r.DestinationSubSectionId==CopyQuestions.DestionationSubSectionId);
                    if (HasQuestion != null)
                    {
                        if (HasQuestion.IsMapped == true)
                        {
                            HasQuestion.DestinationQuestionId = Questions.DestinationQuestionId;
                            entity.Entry(HasQuestion).State = EntityState.Modified;

                        }
                        else entity.MappedQuestions.Remove(HasQuestion);
                        entity.SaveChanges();
                    }
                    var IsExist = entity.MappedQuestions.FirstOrDefault(r => r.DestinationQuestionId == Questions.SourceQuestionId && r.DestinationSubSectionId == CopyQuestions.SourceSubSectionId&&r.SourceSubSectionId==CopyQuestions.DestionationSubSectionId);
                    if (IsExist != null)
                    {
                        if (IsExist.IsMapped == true)
                        {
                            IsExist.DestinationQuestionId = Questions.SourceQuestionId;
                            entity.Entry(IsExist).State = EntityState.Modified;

                        }
                        else entity.MappedQuestions.Remove(IsExist);
                        //CopyQuestionsList.DestinationQuestionId = Questions.SourceQuestionId;
                        entity.SaveChanges();   //entity.Entry(CopyQuestionsList).State = EntityState.Modified;
                    }

                    if (Questions.DestinationQuestionId == 0) continue;

                    //var DestinationControtype = entity.QuestionColumnDetails.FirstOrDefault(r => r.QuestionId == Questions.DestinationQuestionId).QuestionControlTypeId;
                    //if (DesNumberOfColumns == Questions.SourceNoOfColumns && Questions.SourceQuestionControltype == DestinationControtype)
                    //{
                    CopyQuestionsList.SourceQuestionId = Questions.SourceQuestionId;
                    CopyQuestionsList.DestinationQuestionId = Questions.DestinationQuestionId;
                    CopyQuestionsList.SourceSubSectionId = CopyQuestions.SourceSubSectionId;
                    CopyQuestionsList.DestinationSubSectionId = CopyQuestions.DestionationSubSectionId;
                    entity.MappedQuestions.Add(CopyQuestionsList);
                    entity.SaveChanges();
                    CopyQuestionsList.SourceQuestionId = Questions.DestinationQuestionId;
                    CopyQuestionsList.DestinationQuestionId = Questions.SourceQuestionId;
                    CopyQuestionsList.SourceSubSectionId = CopyQuestions.DestionationSubSectionId;
                    CopyQuestionsList.DestinationSubSectionId = CopyQuestions.SourceSubSectionId;
                    entity.MappedQuestions.Add(CopyQuestionsList);
                    entity.SaveChanges();
                    //}
                }

            }
        }
        public Questions GetQuestion(long QuestionId)
        {
            using (var entity = new EFDbContext())
            {
                return entity.Questions.Find(QuestionId);
            }
        }
    
        public void mappingQuestionsTest(string fieldsWithValues)
        { 
            using (var entity = new EFDbContext())
            {
                foreach (var Questions in fieldsWithValues.Split('|'))
                {
                    if (Questions != "")
                    {
                        string[] questionID_Val = Questions.Split(',');
                        var SourceQuestionId = questionID_Val[0] != "" ? Convert.ToInt64(questionID_Val[0]) : 0;
                        var DestinationQuestionId = Convert.ToInt64(questionID_Val[1]);
                        var SourceSubSectionId = Convert.ToInt64(questionID_Val[2]);
                        var DestionationSubSectionId = Convert.ToInt64(questionID_Val[3]);
                        MappedQuestions CopyQuestionsList = new MappedQuestions();
                        var HasQuestion = entity.MappedQuestions.FirstOrDefault(r => r.SourceQuestionId == SourceQuestionId && r.SourceSubSectionId == SourceSubSectionId && r.DestinationSubSectionId == DestionationSubSectionId);
                        if (HasQuestion != null)
                        {
                            if (HasQuestion.IsMapped == true)
                            {
                                HasQuestion.DestinationQuestionId = DestinationQuestionId;
                                entity.Entry(HasQuestion).State = EntityState.Modified;

                            }
                            else
                            
                                entity.MappedQuestions.Remove(HasQuestion);

                           
                           
                        }
                       
                        entity.SaveChanges();
                            var IsExist = entity.MappedQuestions.FirstOrDefault(r => r.DestinationQuestionId == SourceQuestionId && r.DestinationSubSectionId == SourceSubSectionId && r.SourceSubSectionId == DestionationSubSectionId);
                        if (IsExist != null)
                        {
                            if (IsExist.IsMapped == true)
                            {
                                IsExist.DestinationQuestionId = SourceQuestionId;
                               
                                entity.Entry(IsExist).State = EntityState.Modified;

                            }
                            else entity.MappedQuestions.Remove(IsExist);
                        }
                        entity.SaveChanges();
                       if(HasQuestion==null)
                        {
                            if (DestinationQuestionId == 0) continue;
                            CopyQuestionsList.SourceQuestionId = SourceQuestionId;
                            CopyQuestionsList.DestinationQuestionId = DestinationQuestionId;
                            CopyQuestionsList.SourceSubSectionId = SourceSubSectionId;
                            CopyQuestionsList.DestinationSubSectionId = DestionationSubSectionId;
                            entity.MappedQuestions.Add(CopyQuestionsList); entity.SaveChanges();

                            CopyQuestionsList.SourceQuestionId = DestinationQuestionId;
                            CopyQuestionsList.DestinationQuestionId = SourceQuestionId;
                            CopyQuestionsList.SourceSubSectionId = DestionationSubSectionId;
                            CopyQuestionsList.DestinationSubSectionId = SourceSubSectionId;
                            entity.MappedQuestions.Add(CopyQuestionsList);
                            entity.SaveChanges();
                        }

                    }
                   

                   
                 
                 
                }

            }
        }
        public async Task<string> UpdateSubSectionVisiblity(long SubSecId,bool IsShow)
        {
            using (var context = new EFDbContext())
            {
                var Templatesubsection = await  context.TemplateSubSections.FindAsync(SubSecId);
                Templatesubsection.Visible = IsShow;
                context.Entry(Templatesubsection).State = EntityState.Modified;
                context.SaveChanges();
                return "Success";
            }
        }
    }
}