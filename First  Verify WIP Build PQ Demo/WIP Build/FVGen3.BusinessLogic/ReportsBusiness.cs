﻿/*
Page Created Date:  01/27/2019
Created By: Rajesh Pendela
Purpose: All Business operations related to reports module will be performed here
Version: 1.0
****************************************************
*/

using FVGen3.BusinessLogic.Interfaces;
using FVGen3.Domain.Concrete;
using FVGen3.Domain.LocalModels;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace FVGen3.BusinessLogic
{
    public class ReportsBusiness : IReportsBusiness
    {
        /// <summary>
        /// To get the query for Injury Statistics report
        /// </summary>
        /// <param name="templateId"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public string GetInjuryStatisticsQuery(Guid templateId, long clientId)
        {
            using (var context = new EFDbContext())
            {
                var clientTemplate = context.ClientTemplates.FirstOrDefault(r => r.ClientID == clientId && r.TemplateID == templateId);

                var result = context.Database.SqlQuery<InjuryOrSafetyStatisticsModel>(@"select q.QuestionText,qcd.QuestionColumnId,q.DisplayOrder,qcd.ColumnNo from Questions q 
join QuestionColumnDetails qcd on qcd.QuestionId=q.QuestionID 
join TemplateSubSections tsubsec on q.SubSectionId=tsubsec.SubSectionID
join TemplateSections tsec on tsec.TemplateSectionID=tsubsec.TemplateSectionID
join Templates t on t.TemplateID=tsec.TemplateID
where t.TemplateID=@p0 
and q.Visible=1 and tsubsec.Visible=1 and tsec.Visible=1
and QuestionText in('Is your firm self-insured for Workers Compensation claims?','EMR:','EMR effective date:'
,'Recordable Incident Frequency Rate (3 year average):'
,'Has your company been cited by OSHA or the EPA in the past three years?'
,'Has your company been cited by OSHA or the EPA in the past five years?',
'Number of company employees:'
,'Has your company ever had a fatality?')
order by QuestionText,DisplayOrder,ColumnNo
", new SqlParameter("@p0", templateId)).ToList();


                var result1 = context.Database.SqlQuery<InjuryOrSafetyStatisticsModel>(@"select qcd.QuestionColumnId from Questions q 
join QuestionColumnDetails qcd on qcd.QuestionId=q.QuestionID 
join TemplateSubSections tsubsec on q.SubSectionId=tsubsec.SubSectionID
join TemplateSections tsec on tsec.TemplateSectionID=tsubsec.TemplateSectionID
join Templates t on t.TemplateID=tsec.TemplateID
where t.TemplateID=@p0 
and q.Visible=1 and tsubsec.Visible=1 and tsec.Visible=1
and QuestionText in('Number of fatalities: (total from Column G on your OSHA Form)')
", new SqlParameter("@p0", templateId)).ToList();

                var questionColumnIds1 = string.Join(",", result1.Select(r => r.QuestionColumnId));

                var questionColumnIds = string.Join(",", result.GroupBy(r => r.QuestionText).Select(r => r.OrderBy(r1 => r1.DisplayOrder).ThenBy(r1 => r1.ColumnNo).FirstOrDefault().QuestionColumnId));
                //if (!string.IsNullOrEmpty(questioncolumnIds1))
                //    questionColumnIds = questioncolumnIds1 + "," + questionColumnIds;
                if (string.IsNullOrEmpty(questionColumnIds))
                {
                    questionColumnIds = "0";
                }
                if (string.IsNullOrEmpty(questionColumnIds1))
                {
                    questionColumnIds1 = "0";
                }
                return @"select o.Name,o.State,o.City,p.PrequalificationStart, p.PrequalificationFinish, p.PrequalificationStatusId,ui.*,ps.PrequalificationStatusName,EMRs.Numberoffatalities from LatestPrequalification p 
join Organizations o on o.OrganizationID=p.VendorId
join PrequalificationStatus ps on ps.PrequalificationStatusId=p.PrequalificationStatusId
join (SELECT year.PrequalificationId,sum(cast(val.QuestionColumnIdValue as decimal(18,2))) Numberoffatalities  FROM [pqFirstVerifyProduction].[dbo].[PrequalificationEMRStatsValues] val  join PrequalificationEMRStatsYears year on year.PrequalEMRStatsYearId=val.PrequalEMRStatsYearId  where val.QuestionColumnId in(" + questionColumnIds1 + @") and ISNUMERIC(val.QuestionColumnIdValue)=1  group by year.PrequalificationId) EMRs on EMRs.PrequalificationId=p.PrequalificationId
join (
 SELECT PreQualificationId,[Is your firm self-insured for Workers Compensation claims?] isSelfInsured,[EMR:] EMR,[EMR effective date:] EMREffectiveDate
,[Recordable Incident Frequency Rate (3 year average):] RIRAvg
,[Has your company been cited by OSHA or the EPA in the past three years?] isCitedOrEPAThreeYears
,[Has your company been cited by OSHA or the EPA in the past five years?] isCitedOrEPAFiveYears
,[Has your company ever had a fatality?] hadFatality,[Number of company employees:] numberofEmps
      FROM
         (SELECT PreQualificationId, UserInput, QuestionText FROM PrequalificationUserInput ui 
		 join QuestionColumnDetails qcol on qcol.QuestionColumnId =ui.QuestionColumnId
		 join Questions q on q.QuestionID=qcol.QuestionId where qcol.QuestionColumnId in(" + questionColumnIds + @")) I
         PIVOT (max(UserInput) FOR QuestionText IN ([Is your firm self-insured for Workers Compensation claims?],[EMR:],[EMR effective date:]
,[Recordable Incident Frequency Rate (3 year average):],[Number of company employees:]
,[Has your company been cited by OSHA or the EPA in the past three years?]
,[Has your company been cited by OSHA or the EPA in the past five years?]
,[Has your company ever had a fatality?])) P
)ui on ui.PreQualificationId=p.PrequalificationId where p.clientid=" + clientTemplate.ClientID + " and p.PrequalificationStatusId in (8,9,13,24,25,26)" + " order by o.Name";
            }
            return string.Empty;
        }
        /// <summary>
        /// To get the query for Safety Statistics report
        /// </summary>
        /// <returns></returns>
        public string GetSafetyStatisticsQuery()
        {
            using (var context = new EFDbContext())
            {
                var result = context.Database.SqlQuery<InjuryOrSafetyStatisticsModel>(@"select q.QuestionText,qcd.QuestionColumnId,q.DisplayOrder,qcd.ColumnNo from Questions q 
join QuestionColumnDetails qcd on qcd.QuestionId=q.QuestionID 
join TemplateSubSections tsubsec on q.SubSectionId=tsubsec.SubSectionID
join TemplateSections tsec on tsec.TemplateSectionID=tsubsec.TemplateSectionID
join Templates t on t.TemplateID=tsec.TemplateID
where t.TemplateID=@p0 
and q.Visible=1 and tsubsec.Visible=1 and tsec.Visible=1
and QuestionText in('EMR:','Number of fatalities: (total from Column G on your OSHA Form)',
'Number of lost work day cases: (total from Column H on your OSHA Form)',
'Number of job transfer or restricted work day cases: (total from Column I on your OSHA Form)',
'Number of other recordable cases: (total from Column J on your OSHA Form)',
'Number of days away from work: (total from Column K on your OSHA Form)',
'Recordable Incident Frequency Rate: # recordable cases (total from columns G, H, I, J) x 200,000 / Total employee hours worked last year',
'Lost Work Day Case Rate: # lost work day cases (total from column H) x 200,000 / Total employee hours worked last year',
'Days Away, Restrictions or Transfers Rate (DART): # of DART incidents (total from columns H and I) x 200,000 / Total employee hours worked last year')
order by QuestionText,DisplayOrder,ColumnNo
").ToList();

                var questionColumnIds = string.Join(",", result.GroupBy(r => r.QuestionText).Select(r => r.OrderBy(r1 => r1.DisplayOrder).ThenBy(r1 => r1.ColumnNo).FirstOrDefault().QuestionColumnId));
                if (string.IsNullOrEmpty(questionColumnIds))
                {
                    questionColumnIds = "0";
                }
            }
            return "";
        }
        /// <summary>
        /// To get the query for Insurance Information report
        /// </summary>
        /// <param name="templateId"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public string GetInsuranceInformationQuery(Guid templateId, long clientId)
        {
            using (var context = new EFDbContext())
            {
                var clientTemplate = context.ClientTemplates.FirstOrDefault(r => r.ClientID == clientId && r.TemplateID == templateId);

                var result = context.Database.SqlQuery<InjuryOrSafetyStatisticsModel>(@"select q.QuestionText,qcd.QuestionColumnId,q.DisplayOrder,qcd.ColumnNo from Questions q 
join QuestionColumnDetails qcd on qcd.QuestionId=q.QuestionID 
join TemplateSubSections tsubsec on q.SubSectionId=tsubsec.SubSectionID
join TemplateSections tsec on tsec.TemplateSectionID=tsubsec.TemplateSectionID
join Templates t on t.TemplateID=tsec.TemplateID
where t.TemplateID=@p0 
and q.Visible=1 and tsubsec.Visible=1 and tsec.Visible=1
and QuestionText in('Insurance Carrier(s):','General Liability – 
Each Occurrence Limit Amount:',
'General Liability – 
General Aggregate Limit Amount:',
'Automobile Liability – 
Each Accident Limit Amount:',
'Excess/Umbrella Liability – 
Each Occurrence Limit:',
'Workers Comp and Employer''s Liability –  Each Accident Limit:',
'Workers Comp and Employer''s Liability –  Disease Each Employee:',
'Workers Comp and Employer''s Liability –  Disease Policy Limit:')
order by QuestionText,DisplayOrder,ColumnNo
", new SqlParameter("@p0", templateId)).ToList();

                var questionColumnIds = string.Join(",", result.GroupBy(r => r.QuestionText).Select(r => r.OrderBy(r1 => r1.DisplayOrder).ThenBy(r1 => r1.ColumnNo).FirstOrDefault().QuestionColumnId));
                if (string.IsNullOrEmpty(questionColumnIds))
                {
                    questionColumnIds = "0";
                }
                return @"select o.Name,o.State,o.City,p.PrequalificationStart, p.PrequalificationFinish, p.PrequalificationStatusId,ui.*,ps.PrequalificationStatusName from LatestPrequalification p 
join Organizations o on o.OrganizationID=p.VendorId
join PrequalificationStatus ps on ps.PrequalificationStatusId=p.PrequalificationStatusId
join (
 SELECT PreQualificationId,[Insurance Carrier(s):] InsuranceCarriers,[General Liability – 
Each Occurrence Limit Amount:] GLEOL,[General Liability – 
General Aggregate Limit Amount:] GLGALA
,[Automobile Liability – 
Each Accident Limit Amount:] ALEALA
,[Excess/Umbrella Liability – 
Each Occurrence Limit:] EULEOL,[Workers Comp and Employer's Liability –  Each Accident Limit:] WCAEEAL,
[Workers Comp and Employer's Liability –  Disease Each Employee:] WCAELDEE,
[Workers Comp and Employer's Liability –  Disease Policy Limit:] WCAELDPL
      FROM
         (SELECT PreQualificationId, UserInput, QuestionText FROM PrequalificationUserInput ui 
		 join QuestionColumnDetails qcol on qcol.QuestionColumnId =ui.QuestionColumnId
		 join Questions q on q.QuestionID=qcol.QuestionId where qcol.QuestionColumnId in(" + questionColumnIds + @")) I
         PIVOT (max(UserInput) FOR QuestionText IN ([Insurance Carrier(s):],[General Liability – 
Each Occurrence Limit Amount:],[General Liability – 
General Aggregate Limit Amount:]
,[Automobile Liability – 
Each Accident Limit Amount:]
,[Excess/Umbrella Liability – 
Each Occurrence Limit:],[Workers Comp and Employer's Liability –  Each Accident Limit:],
[Workers Comp and Employer's Liability –  Disease Each Employee:],
[Workers Comp and Employer's Liability –  Disease Policy Limit:]
)) P
)ui on ui.PreQualificationId=p.PrequalificationId where p.clientid=" + clientTemplate.ClientID + " and p.PrequalificationStatusId in (9,13,24,25,26)" + " order by o.Name";
            }
            return "";
        }
        /// <summary>
        /// To get the query for Vendor Contract report
        /// </summary>
        /// <param name="templateId"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public string GetContractVendorsQuery(Guid templateId, long clientId)
        {
            using (var context = new EFDbContext())
            {
                var clientTemplate = context.ClientTemplates.FirstOrDefault(r => r.ClientID == clientId);


                List<string> questions = new List<string>();
                questions.Add("Single Project Contract Amount:");
                questions.Add("Aggregate Contract Amount:");
                questions.Add("JDE Vendor Code");
                questions.Add("PMWeb Vendor Code");
                questions.Add("CSI Vendor Code");

                var ids = context.Questions.Where(r => questions.Contains(r.QuestionText) && r.TemplateSubSections.TemplateSections.TemplateID == templateId).SelectMany(r => r.QuestionColumnDetails).Select(r => r.QuestionColumnId);
                var questionColumnIds = string.Join(",", ids.ToArray());
                if (string.IsNullOrEmpty(questionColumnIds))
                {
                    questionColumnIds = "0";
                }


                return @"select o.Name Vendor,ui.*,ps.PrequalificationStatusName as Status from LatestPrequalification p 
join Organizations o on o.OrganizationID=p.VendorId
join PrequalificationStatus ps on ps.PrequalificationStatusId=p.PrequalificationStatusId
join (
 SELECT PreQualificationId,[Single Project Contract Amount:] SingleProjectContractAmount,[Aggregate Contract Amount:] AggregateAmount,
[JDE Vendor Code] JDEVendorCode,
[PMWeb Vendor Code] PMWebVendorCode,
[CSI Vendor Code] CSIVendorCode
      FROM
         (SELECT PreQualificationId, UserInput, QuestionText FROM PrequalificationUserInput ui 
		 join QuestionColumnDetails qcol on qcol.QuestionColumnId =ui.QuestionColumnId
		 join Questions q on q.QuestionID=qcol.QuestionId where qcol.QuestionColumnId in(" + questionColumnIds + @")) I
         PIVOT (Max(UserInput) FOR QuestionText IN ([Single Project Contract Amount:],[Aggregate Contract Amount:],
[JDE Vendor Code], [PMWeb Vendor Code], [CSI Vendor Code])) P
)ui on ui.PreQualificationId=p.PrequalificationId where p.clientid=" + clientTemplate.ClientID + "and p.PrequalificationFinish>DATEADD(Year,-2,GETDATE()) order by Vendor";
            }
            return string.Empty;
        }
        /// <summary>
        /// To show or hide report to specific client
        /// </summary>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public bool ShowOrHideData(long clientId)
        {
            if (clientId == 58)
                return true;
            else
                return false;
        }
        //public List<FeesPaid> GetFee(string fromdate, string todate, Guid templateId, long clientId)
        //{

        //    using (var entityDB = new EFDbContext())
        //    {

        //        DateTime Fromdate = DateTime.Parse(fromdate);
        //        DateTime Todate = DateTime.Parse(todate);

        //        var res = (from qp in entityDB.QuizPayments
        //                   join suoq in entityDB.SystemUsersOrganizationsQuizzes on qp.SysUserOrgQuizId equals suoq.SysUserOrgQuizId
        //                   join suo in entityDB.SystemUsersOrganizations on suoq.SysUserOrgId equals suo.SysUserOrganizationId
        //                   join org in entityDB.Organizations on suo.OrganizationId equals org.OrganizationID

        //                   join pq in entityDB.Prequalification on org.OrganizationID equals pq.VendorId
        //                   join ct in entityDB.ClientTemplates on suoq.ClientTemplateId equals ct.ClientTemplateID
        //                   join t in entityDB.Templates on ct.TemplateID equals t.TemplateID
        //                   where qp.PaymentReceivedDate >= Fromdate && qp.PaymentReceivedDate <= Todate && qp.PaymentReceivedAmount != 0&&t.TemplateID== templateId && pq.ClientId==clientId

        //                   select new FeesPaid()
        //                   {

        //                       ClientName = pq.Client.Name,
        //                       VendorName = pq.Vendor.Name,
        //                       Transactionid = qp.PaymentTransactionId ?? "",
        //                       Fee = qp.PaymentReceivedAmount,
        //                       Templatename = t.TemplateName,
        //                       PaymentReceivedDate = qp.PaymentReceivedDate
        //                   }).Distinct();
        //        var quizpayments = res.OrderBy(r => r.ClientName).ThenBy(r => r.VendorName).ToList();


        //        var result = (from pp in entityDB.PrequalificationPayments
        //                      join p in entityDB.Prequalification on pp.PrequalificationId equals p.PrequalificationId
        //                      //join c in entityDB.Organizations on p.ClientId equals c.OrganizationID
        //                      join ct in entityDB.ClientTemplates on p.ClientTemplateId equals ct.ClientTemplateID
        //                      join t in entityDB.Templates on ct.TemplateID equals t.TemplateID
        //                      where pp.PaymentReceivedDate >= Fromdate && pp.PaymentReceivedDate <= Todate && pp.PaymentReceivedAmount != 0 && t.TemplateID ==templateId && p.ClientId == clientId
        //                      select new FeesPaid()
        //                      {
        //                          IsQuiz = false,
        //                          ClientName = p.Client.Name,
        //                          VendorName = p.Vendor.Name,
        //                          Transactionid = pp.PaymentTransactionId ?? "",
        //                          Fee = pp.PaymentReceivedAmount,
        //                          Templatename = p.ClientTemplates.Templates.TemplateName,
        //                          PaymentReceivedDate = pp.PaymentReceivedDate
        //                      }).Distinct();
        //        var pqpayments = result.OrderBy(r => r.ClientName).ThenBy(r => r.VendorName).ToList();



        //        var trainingfee = (from ptap in entityDB.PrequalificationTrainingAnnualFees
        //                           join pre in entityDB.Prequalification on ptap.PrequalificationId equals pre.PrequalificationId
        //                           join o in entityDB.Organizations on pre.ClientId equals o.OrganizationID
        //                           join ct in entityDB.ClientTemplates on pre.ClientTemplateId equals ct.ClientTemplateID
        //                           join t in entityDB.Templates on ct.TemplateID equals t.TemplateID
        //                           where ptap.FeeReceivedDate >= Fromdate && ptap.FeeReceivedDate <= Todate && ptap.AnnualFeeReceivedAmount != 0&&t.TemplateID==templateId&&pre.ClientId==clientId
        //                           select new FeesPaid()
        //                           {

        //                               ClientName = pre.Client.Name,
        //                               VendorName = pre.Vendor.Name,
        //                               Transactionid = ptap.FeeTransactionId ?? "",
        //                               Fee = ptap.AnnualFeeReceivedAmount,
        //                               Templatename = t.TemplateName,
        //                               PaymentReceivedDate = ptap.FeeReceivedDate
        //                           }).Distinct();


        //        var Annualfee = trainingfee.OrderBy(r => r.ClientName).ThenBy(r => r.VendorName).ToList();

        //        pqpayments.AddRange(quizpayments);
        //        pqpayments.AddRange(Annualfee);




        //        return pqpayments.Distinct().OrderByDescending(r => r.PaymentReceivedDate).ThenBy(r => r.VendorName).ToList();
        //    }
        //}
        /// <summary>
        /// To get the training payments made by vendors specific to the client
        /// </summary>
        /// <param name="fromdate"></param>
        /// <param name="todate"></param>
        /// <param name="templateId"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public List<FeesPaid> GetFee(string fromdate, string todate, Guid templateId, long clientId)
        {

            using (var entityDB = new EFDbContext())
            {

                DateTime Fromdate = DateTime.Parse(fromdate).Date;
                DateTime Todate = DateTime.Parse(todate).Date.AddDays(1);
                //var EriTemplate=entityDB.Templates.FirstOrDefault(r=>r.TemplateID==templateId&&r.IsERI==true);
                // if(EriTemplate!=null)
                // {clientId=-1;}
                string ClientId = clientId.ToString();
                var res = (from qp in entityDB.QuizPayments
                           join suoq in entityDB.SystemUsersOrganizationsQuizzes on qp.SysUserOrgQuizId equals suoq.SysUserOrgQuizId
                           join suo in entityDB.SystemUsersOrganizations on suoq.SysUserOrgId equals suo.SysUserOrganizationId
                           join org in entityDB.Organizations on suo.OrganizationId equals org.OrganizationID

                           //join pq in entityDB.Prequalification on org.OrganizationID equals pq.VendorId
                           join ct in entityDB.ClientTemplates on suoq.ClientTemplateId equals ct.ClientTemplateID
                           join t in entityDB.Templates on ct.TemplateID equals t.TemplateID
                           where qp.PaymentReceivedDate >= Fromdate && qp.PaymentReceivedDate <= Todate
                           && qp.PaymentReceivedAmount != 0 && ct.ClientID == clientId

                           select new FeesPaid()
                           {

                               ClientName = ct.Organizations.Name,
                               VendorName = org.Name,
                               Transactionid = qp.PaymentTransactionId ?? "",
                               Fee = qp.PaymentReceivedAmount,
                               Templatename = t.TemplateName,
                               Templateid = t.TemplateID,
                               PaymentReceivedDate = qp.PaymentReceivedDate
                           }).GroupBy(x => new
                           {
                               x.VendorName,
                               x.Transactionid
                           }).Select(g => g.FirstOrDefault());
                if (templateId != new Guid())
                {
                    res = res.Where(r => r.Templateid == templateId);
                }
                var quizpayments = res.OrderByDescending(r => r.PaymentReceivedDate).ThenBy(r => r.VendorName).ToList();


                var result = (from pp in entityDB.PrequalificationPayments
                              join p in entityDB.Prequalification on pp.PrequalificationId equals p.PrequalificationId
                              //join c in entityDB.Organizations on p.ClientId equals c.OrganizationID
                              //join ct in entityDB.ClientTemplates on p.ClientTemplateId equals ct.ClientTemplateID
                              join t in entityDB.Templates on p.ClientTemplates.TemplateID equals t.TemplateID
                              where pp.PaymentReceivedDate >= Fromdate && pp.PaymentReceivedDate <= Todate
                              && pp.PaymentReceivedAmount != 0 && p.ClientId == clientId && pp.PaymentCategory == 0
                              select new FeesPaid()
                              {

                                  ClientName = p.Client.Name,
                                  VendorName = p.Vendor.Name,
                                  Transactionid = pp.PaymentTransactionId ?? "",
                                  Fee = pp.PaymentReceivedAmount,
                                  Templatename = t.TemplateName,
                                  Templateid = t.TemplateID,
                                  PaymentReceivedDate = pp.PaymentReceivedDate,
                                  IsQuiz = false
                              });

                if (templateId != new Guid())
                {
                    result = result.Where(r => r.Templateid == templateId);
                }
                var pqpayments = result.ToList();

                var bupayments = (from pp in entityDB.PrequalificationPayments
                                  join pre in entityDB.Prequalification on pp.PrequalificationId equals pre.PrequalificationId
                                  //join o in entityDB.Organizations on pre.VendorId equals o.OrganizationID

                                  //join ct in entityDB.ClientTemplates on pre.ClientTemplateId equals ct.ClientTemplateID
                                  join t in entityDB.Templates on pre.ClientTemplates.TemplateID equals t.TemplateID
                                  join prc in entityDB.PrequalificationSites on pre.PrequalificationId equals prc.PrequalificationId
                                  where pp.PaymentReceivedDate >= Fromdate && pp.PaymentReceivedDate <= Todate && pp.PaymentCategory == 1
                                  && pp.PaymentReceivedAmount != 0 && prc.ClientId == clientId && pp.PrequalificationPaymentsId == prc.PrequalificationPaymentsId
                                  select new FeesPaid()
                                  {

                                      ClientName = prc.Client.Name,
                                      VendorName = pre.Vendor.Name,
                                      Transactionid = pp.PaymentTransactionId ?? "",
                                      Fee = pp.PaymentReceivedAmount,
                                      Templatename = t.TemplateName,
                                      Templateid = t.TemplateID,
                                      IsQuiz = false,
                                      PaymentReceivedDate = pp.PaymentReceivedDate
                                  }).Distinct();
                if (templateId != new Guid())
                {
                    bupayments = bupayments.Where(r => r.Templateid == templateId);
                }

                var Bupayments = bupayments.OrderByDescending(r => r.PaymentReceivedDate).ThenBy(r => r.VendorName).ToList();
                var trainingfee = (from ptap in entityDB.PrequalificationTrainingAnnualFees
                                   join pre in entityDB.Prequalification on ptap.PrequalificationId equals pre.PrequalificationId
                                   join o in entityDB.Organizations on pre.ClientId equals o.OrganizationID
                                   join ct in entityDB.ClientTemplates on pre.ClientTemplateId equals ct.ClientTemplateID
                                   join t in entityDB.Templates on ct.TemplateID equals t.TemplateID
                                   where ptap.FeeReceivedDate >= Fromdate && ptap.FeeReceivedDate <= Todate && ptap.AnnualFeeReceivedAmount != 0 && pre.ClientId == clientId
                                   select new FeesPaid()
                                   {

                                       ClientName = pre.Client.Name,
                                       VendorName = pre.Vendor.Name,
                                       Transactionid = ptap.FeeTransactionId ?? "",
                                       Fee = ptap.AnnualFeeReceivedAmount,
                                       Templatename = t.TemplateName,
                                       Templateid = t.TemplateID,
                                       PaymentReceivedDate = ptap.FeeReceivedDate
                                   }).GroupBy(x => new
                                   {
                                       x.VendorName,
                                       x.Transactionid
                                   }).Select(g => g.FirstOrDefault());
                if (templateId != new Guid())
                {
                    trainingfee = trainingfee.Where(r => r.Templateid == templateId);
                }

                var Annualfee = trainingfee.ToList();

                pqpayments.AddRange(quizpayments);
                pqpayments.AddRange(Annualfee);
                pqpayments.AddRange(Bupayments);
                return pqpayments.Distinct().OrderByDescending(r => r.PaymentReceivedDate).ThenBy(r => r.VendorName).ToList();
            }
        }
        public string GetVendorInvitesByStatusQuery(Guid templateId, long clientId,string State)
        {
            using (var context = new EFDbContext())
            {
                var clientTemplate = context.ClientTemplates.FirstOrDefault(r => r.ClientID == clientId && r.TemplateID == templateId);

                var result = context.Database.SqlQuery<InjuryOrSafetyStatisticsModel>(@"select q.QuestionText,qcd.QuestionColumnId,q.DisplayOrder,qcd.ColumnNo from Questions q 
join QuestionColumnDetails qcd on qcd.QuestionId=q.QuestionID 
join TemplateSubSections tsubsec on q.SubSectionId=tsubsec.SubSectionID
join TemplateSections tsec on tsec.TemplateSectionID=tsubsec.TemplateSectionID
join Templates t on t.TemplateID=tsec.TemplateID
where t.TemplateID=@p0 
and q.Visible=1 and tsubsec.Visible=1 and tsec.Visible=1
and QuestionText in('Minority Business:','Business qualifies as a Disadvantaged Business Enterprise (DBE):' ,'Business qualifies as a Minority Business Enterprise (MBE):',
'Business qualifies as a Small Business Enterprise (SBE):'
,'Business qualifies as a Women''s Business Enterprise (WBE):'
,'Financials:'
,'Safety:')
order by QuestionText,DisplayOrder,ColumnNo
", new SqlParameter("@p0", templateId)).ToList();

                var questionColumnIds = string.Join(",", result.GroupBy(r => r.QuestionText).Select(r => r.OrderBy(r1 => r1.DisplayOrder).ThenBy(r1 => r1.ColumnNo).FirstOrDefault().QuestionColumnId));
                if (string.IsNullOrEmpty(questionColumnIds))
                {
                    questionColumnIds = "0";
                }
                var Filters = "";
                if (State != "")
                { Filters = "VendorOrgs.State = '" + State+"' AND vid.ClientId = "+ clientId; }
                else { Filters = "vid.ClientId =" + clientId; }

                return @"select VendorOrgs.Name VendorName, VendorOrgs.City VendorCity, VendorOrgs.State VendorState,vid.InvitationSentByUser as InvitationSentBy, ui.DBE as DBE,ui.Financials as Financials,ui.IsMinority as IsMinority,ui.MBE as MBE,ui.Safety as Safety,ui.SBE as SBE,ui.WBE as WBE
FROM VendorInviteDetails vid join prequalification as p on p.PrequalificationId = vid.PQId
JOIN Organizations org ON vid.ClientId = org.OrganizationID
JOIN Organizations as VendorOrgs ON vid.Vendorid = VendorOrgs.OrganizationID
join(
SELECT PreQualificationId,[Minority Business:] IsMinority,[Business qualifies as a Disadvantaged Business Enterprise (DBE):] DBE,[Business qualifies as a Minority Business Enterprise (MBE):] MBE
,[Business qualifies as a Small Business Enterprise (SBE):] SBE
,[Business qualifies as a Women's Business Enterprise (WBE):] WBE
,[Financials:] Financials
,[Safety:] Safety
FROM
(SELECT PreQualificationId, UserInput, QuestionText FROM PrequalificationUserInput ui

join QuestionColumnDetails qcol on qcol.QuestionColumnId = ui.QuestionColumnId

join Questions q on q.QuestionID = qcol.QuestionId where qcol.QuestionColumnId in(" + questionColumnIds + @")) I
PIVOT(max(UserInput) FOR QuestionText IN([Minority Business:],[Business qualifies as a Disadvantaged Business Enterprise (DBE):],[Business qualifies as a Minority Business Enterprise (MBE):]
,[Business qualifies as a Small Business Enterprise (SBE):],[Business qualifies as a Women's Business Enterprise (WBE):]
,[Financials:]
,[Safety:]
)) P
)ui on p.PrequalificationId = ui.PreQualificationId
where" + Filters;
            }
            return string.Empty;
        }

    }
}
