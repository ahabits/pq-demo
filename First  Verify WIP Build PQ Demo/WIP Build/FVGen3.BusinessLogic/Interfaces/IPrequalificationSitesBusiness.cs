﻿/*
Page Created Date:  04/03/2018
Created By: Rajesh Pendela
Purpose: All Business methods related to prequalification sites will be declared here
Version: 1.0
****************************************************
*/

using FVGen3.Domain.Entities;
using FVGen3.Domain.LocalModels;
using FVGen3.Domain.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FVGen3.BusinessLogic.Interfaces
{
    public interface IPrequalificationSitesBusiness
    {
        bool UpdateSiteStatuses(List<SiteStatus> Sites,Guid userId);
        SiteStatus GetSiteStatuses(long PqSiteId);

        List<PQBusinessUnitSites> GetPQSites(long pqId, Guid userId);
        PrequalificationSites GetPqSite(long? pqSiteId, long? buId, long? buSiteId, long? pqId);
        List<KeyValuePair<string, string>> GetBusinessUnitSites(long? clientBuId);
        void PostBuSites(LocalAddPrequalificationSite buSites);
    }
}
