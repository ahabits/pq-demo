﻿/*
Page Created Date:  05/18/2017
Created By: Rajesh Pendela
Purpose: All Business methods related to Proficiency Capabilitis module will be declared here
Version: 1.0
****************************************************
*/

using System.Collections.Generic;
using FVGen3.DataLayer.DTO;

namespace FVGen3.BusinessLogic.Interfaces
{
    public interface IProficiencyBusiness
    {
        List<ProficiencyData> GetProficiencies();
    }
}
