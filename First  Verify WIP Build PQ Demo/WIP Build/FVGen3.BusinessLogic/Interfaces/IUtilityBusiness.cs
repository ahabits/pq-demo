﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FVGen3.Domain.ViewModels;

namespace FVGen3.BusinessLogic.Interfaces
{
   public interface IUtilityBusiness
    {
        void Info(string message);
        void Info(string message, string param, string screen);
        List<StatusesDescription> GetStatusDescriptions();
        
    }
}
