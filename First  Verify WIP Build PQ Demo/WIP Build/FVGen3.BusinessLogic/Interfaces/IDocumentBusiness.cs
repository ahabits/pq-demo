﻿using System;
using System.Collections.Generic;
using FVGen3.Domain.LocalModels;
using FVGen3.Domain.Entities;
using FVGen3.DataLayer.DTO;
using System.Web;

namespace FVGen3.BusinessLogic.Interfaces
{
    public interface IDocumentBusiness
    {
        bool RecordLog(Guid documentId, Guid UserId, string ModificationType);
        List<DocumentInfo> GetLogs(Guid documentId);
        DataSourceResponse GetRecentDocs(string filters, DataSourceRequest request, string Role);
        LocalSupportedDocument GetSupportingDocument(long subSectionId, Guid? documentId);
        void PostDocumentBusiness(LocalSupportedDocument form, string role, Guid userId, HttpPostedFileBase file, Document doc);
    }
} 
