﻿/*
Page Created Date:  05/18/2017
Created By: Rajesh Pendela
Purpose: All Business methods related to user will be declared here
Version: 1.0
****************************************************
*/

using System;
using FVGen3.DataLayer.DTO;
using System.Collections.Generic;
using FVGen3.Domain.Entities;
using FVGen3.Domain.LocalModels;
using System.Windows.Forms;
using FVGen3.Domain.ViewModels;

namespace FVGen3.BusinessLogic.Interfaces
{
    public interface ISystemUserBusiness
    {
        long ErrorHandler(ErrorModel error,string params1);
        UserInfo GetUserInfo(Guid UserId);
        UserInfo GetUserInfo(string email); 
        UserInfo GetSuperUserInfo(long orgId);
        bool UpdateUserLog(Guid userId);
        bool CheckUserCanLoginAsAnyUser(Guid userId);
        bool IsERIUser(Guid UserId);
        bool IsUserHasPermission(Guid UserId, long PermissionId);
        List<UserInfo> GetUsers(string role);
        bool HasScreenPermission(string headerName, string sideMenuName, string ViewName, List<LocalHeaderMenuModel> headerMenu);

        /// <summary>
        /// Created By Vasavi
        /// 
        /// </summary>
        SystemUsers FindUser(Guid userID);
        /// <summary>
        /// Created By Vasavi
        /// 
        /// </summary>
        void UpdateUserPassword(string encrptNewPassword, Guid userId);
        SystemUsers GetUserAccount(Guid userId);
      string GetUserOfficeName(Guid userId);
        IEnumerable<ClientBusinessUnits> GetClientBuUnits(long clientId);
        void RemoveClientBusinessUnitSiteRepresentatives(long oldSiteId);
        ClientBusinessUnitSites GetClientBusinessUnitSites(long currentSiteId);
        List<ClientBusinessUnitSiteRepresentatives> GetClientBuSiteRepresentatives(Guid userId);
        void InsertClientBuSiteRepresentatives(long ClientBusinessUnitSiteId, Guid userId);
        ClientBusinessUnits GetClientBusinessUnits(long busUnitId);
        Prequalification FindPq(long prequlificationId);
        PrequalificationClient GetPQClients(long prequlificationId);
        List<long> GetClientBusinessUnitSiteIdsInPq(long prequlificationId);
        List<string> GetStates(IEnumerable<long> selectedRegions);
        SystemUsersOrganizations GetSystemUsersOrgs(Guid userId);
        Organizations GetOrgs(long OrgId);
        string GetAdminSuperUserEmail();
        void AddArchiveOrgDetails(long OrganizationID, Guid userId);
        void UpdateSystemUsersAccount(LocalMyAccountModel form);
        void UpdateUserOrgnizationdata(string RoleName, Organizations userOrganizationData, LocalMyAccountModel form);
        SystemUsers GetSystemUser(string userName);
        SystemUsersOrganizations GetSystemUserOrganizations(Guid userId);
        void UpdateWrongPasswordAttempts(SystemUsers varlogMail);
        void UpdateSystemUserLogs(Guid userId);
        SystemUsers GetUser(string UserName, string PassWord);
        void UpdateLoginDate(Guid Userid);
        Contact GetName(Guid UserId);
        string LoginUserName(SystemUsers varlogMail);
        LocalMyAccountModel GetAccount(Guid guidRecordId);
        FinancialAnalyticsModel IsUserHasFinancialAnalytics(long OrganizationID);
        bool HasFinancialData(long OrganizationId);
        Organizations OrganizationWithTaxId(string TaxId, long OrganizationId);
        Organizations OrgsInUserOrganization(Guid userId);
        void AddOrganizations(LocalMyCompanyDetails orgForm, SystemUsersOrganizations varSysUserOrg);
        void UpdateOrganizations(LocalMyCompanyDetails orgForm, Organizations org_updata);
        void AddArchiveVendorData(LocalMyCompanyDetails orgForm, Guid userId, OrganizationData org);
        SystemUsers GetSystemUserInfo(string Email);
        void ResetPassword(Guid UserID, string PassWord);
        List<KeyValuePair<string, long>> GetOrganizations(long currentUserOrgId);
        LocalNewUserContactDetails AddNewUser(LocalAddNewUser form, string PassWord);
         List<Organizations> OrganizationsList(string OrganizationType);
        List<KeyValuePair<string, long>> GetOrganizationsInSystemUserOrgs(Guid currentUserId);
        SystemUsersOrganizations FindUserOrgs(long userOrgID);
        List<SystemUsersOrganizationsRoles> GetUserOrganizationRoles(long userOrgID);
        SystemUsersOrganizations UserOrganizations(long OrganizationId, Guid UserId);
        void SaveOrganizationRoles(LocalCompaniesList form);
        List<SystemRoles> GetRoles(string OrgType);
        void RemoveOldOrganizationRoles(long oldOrgId);
        void UpdateSelectedUserCompanyAccess(LocalCompaniesList form);
        string SuperUserRoleId(string currentUser_OrgType);
        bool isSuperUser(long currUserSysUserOrgId, string superUserRoleId);
        List<Contact> UserContacts(Guid userId);
        SystemUsers emailExists(string Email, Guid userId);
        void UpdateNewUserContactDetails(LocalNewUserContactDetails local);
        SystemUsersBUSites UserBuSites(Guid userId);
        UserDepartments GetUserDepartment(Guid UserId);
        string UpdateUser(Guid NewUserId, Guid CurrentuserId, bool Isdelete);
        string DeleteUser(Guid UserId);
        List<SystemUsersOrganizations> userOrganizationsList(Guid UserId);
        DataSourceResponse UsersList(bool searchType,long SuperClientId, string fname, string lname, string email, bool userStatus_Active, bool userStatus_InActive, long clientId, long vendorId, long adminId, DataSourceRequest request);
       
        List<BiddingInterests> GetBiddings();
        List<ClaycoBiddingInterests> GetClaycoBiddingsList();
        List<Language> GetLanguages();
        ClaycoBiddingInterests GetFirstClaycoBiddings(long biddingId);
       void AddBiddings(LocalAddBiddingInterest bidding);
        void AddClaycoBidddings(LocalAddClaycoBiddingInterest bidding);
        void DeleteBidding(long BiddingId);
        void DeleteTemplatePrice(long TemplatePriceId);
        List<LocalHeaderMenuModel> setUpMenus(string role, Guid UserId, long CurrentOrgId);
        BiddingInterests GetFirstBiddings(long biddingId);
        List<ProficiencyCapabilities> GetProficiencyCategory();
        ProficiencyCapabilities FindProficiencyCategory(long ProficiencyId);
        bool HasPrequalificationReportingData(long ProficiencyId);
        List<string> GetProficiencyCategoryNames(string ProficiencyCategory);
        string AddProficiency(LocalAddProficiencyCapability proficiency);
        List<string> GetProficiencySubCategoryNames(long ProficiencyId);
        List<ProficiancyWithIsEdit> GetProficiencyData();
        List<string> GetProficiencySubCategoryNamesWithCategory(string Category);
        List<PrequalificationReportingData> GetPrequalificationReportingData(long proficiencyId);
        string EditProficiency(long proficiencyId, string category, string SubCategroy);
        List<DocumentType> GetDocuments();
        LocalAddDocumentType GetDocumentTypeBasedOnId(long DocumentTypeId);
        bool HasDocument(string DocumentTypeName, long DocumentTypeId);
        void AddDocumentType(LocalAddDocumentType addDocumentType);
        long AssignUserRoles(List<LocalAssignRoles> UserRoles);
        List<LocalAssignRoles> UserWithPermissions(long OrgId);
        List<ClientBusinessUnitSites> GetClientBusinessUnitSitesList(long busUnitId);
        List<Organizations> GetClientsList(string OrganizationType);
        List<CompanyAccessRoles> GetUserRoles(Guid UserId);
        List<SiteRepresents> GetSiteRepresents(Guid registeredUserId);
        string UpdateUserEmail(Guid UserId, string Email);
        List<KeyValuePair<string, int>> GetDepartments();
        string UserInsertion(string file, long OrgId, string RoleId, int DepartmentId);
        bool CheckIsOrmatUser(Guid UserId);
        DataSourceResponse GetAllDepartments(DataSourceRequest request, string filters);
        string UpdateDepartmentName(int id, string Name);
        string AddDepartment(string Name, long ClientID);
        List<KeyValuePair<string, string>> GetAllEmailTemplates();
        DataSourceResponse GetEmailtemplates(DataSourceRequest request,string filters);
        string UpdateEmailTemplate(int Id, long ClientId, int TemplateId, int? Type);
        string DeleteEmailTemplate(int ID);
        string AddEmailTemplate(long ClientId, int TemplateId, int? Type);
        List<LocalUsersList> GetOrganizationUsers(Guid UserId);
        List<KeyValuePair<string, string>> GetClientDepartments(long ClientId);
        List<KeyValuePair<string, string>> GetSineproSites();
        int CheckOrgUsersCount(Guid UserId);
        List<Dropdown> GetClientUserEmails(long ClientId, string name);
        string SaveClientInvitaionEmails(string UserEmails, string UserIds, long ClientId, long? Id,int Type);
        DataSourceResponse GetInviteUsers(DataSourceRequest request, string filters,int Type);
        List<long> GetClients();
        ClientNotifications CheckClientUsersforInvitation(long ClientId);
        bool CheckInviteClients(long ClientId);
        List<long> NotificationClients();
        string DeleteClientNotification(long Id);
        PqBannerComments GetPQcomments(long pqId);
        void UpdatePqComments(long pqId, string Comment, Guid UserId, bool IsChecked);
        long GetSuperClientId(long ClientId);
        DataSourceResponse GetAllPowerBiUserPermissions(DataSourceRequest request, string filters);
        List<KeyValuePair<string, long>> GetPowerBiReportNames();
        List<KeyValuePair<string, string>> GetOrganizations(int Type);
        string PostPowerBiUsersData(string UserIds, long? OrgId, long? Id, int Type, long ReportId);
        string DeletePowerBiUserpermission(long Id);
        List<LocalPowerBiReports> GetUserReports(long OrgId,Guid UserId,string Role);
        void UsersInInvitation(Guid InviatationId, string Bcc, string Cc);

    }
}

