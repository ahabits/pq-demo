﻿/*
Page Created Date:  11/17/2017
Created By: Rajesh Pendela
Purpose: All Business methods related to client will be declared here
Version: 1.0
****************************************************
*/

using FVGen3.DataLayer.DTO;
using FVGen3.Domain.Entities;
using FVGen3.Domain.LocalModels;
using FVGen3.Domain.ViewModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FVGen3.BusinessLogic.Interfaces
{
    public interface IClientBusiness
    {

        List<KeyValuePair<string, string>> GetClientContractTypes(long clientId);
        List<KeyValuePair<string, string>> GetClients();
        List<KeyValuePair<string, string>> GetAllClients();
        List<KeyValuePair<string, string>> GetERIClientsByRegion(long pqId,string regions);
        List<KeyValuePair<string, string>> GetERIsRegion(long pqId);
        bool CheckTemplateIsERI(long pqId);
        List<KeyValuePair<string, string>> GetClients(IEnumerable<long> clientIds);
        List<KeyValuePair<string, string>> GetClientSupportingDocs(long clientId, long prequalificationId,long SectionId);
        bool isDocumentExpire(long preQualification, long docTypeId, long clientId);
        List<KeyValuePair<string, string>> GetClientBusinessUnits(long clientId);
        IEnumerable<KeyValuePair<string, string>> GetClientBUSitesByBU(string buIds);
        IEnumerable<KeyValuePair<long, string>> GetPQClientsInSites(long pqId);
        IEnumerable<KeyValuePair<string, string>> GetEriClients();
        List<long> GetRestrictedClients(long superClient);
        bool CheckClientIsInERIClients(long clientId);
        List<PQSites> GetPQClientLocations(long pqId, long clientId);
        //List<ClientRegionsDTO> GetPQClientRegions(long pqId, List<long> clientIds);

        IEnumerable<KeyValuePair<string, string>> GetPQStatuses();
        bool UpdateLocationStatus(long pqSiteId, long pqSiteStatusId);
        bool LogSiteStatusChange(AssignLocationStatus assignStatus);
        List<KeyValuePair<string, string>> GetFeeTemplates(long clientid);
        List<KeyValuePair<string, string>> GetClientTemplates(long clientId);
        string UpdateKey(long organizationId, string key);
        string UpdateTemplateKey(long organizationId, string key,Guid TemplateId);
        List<KeyValuePair<string, string>> GetSubClients(long orgId,bool includeSperClient=false);
        List<LocalBusinessSites> GetClientBusinessUnitSites(long ClientId);

        PQBusinessUnitDocments GetPQBusinessUnitDocuments(long pqid, string Role, Guid UserId);

        List<LocalinviteVendor> GetInvitedVendors(long clientId,string SearchEmail,int? InvitationType, bool HasInvitation);
        List<KeyValuePair<string, string>> GetClientsExcludeSubClients();
        Task<List<KeyValuePair<string, string>>> GetLanguagesAsync();
        List<KeyValuePair<string, string>> GetLanguages();
        string DeleteTemplateKey(long organizationid, Guid TemplateId);
        string GetClientTemplateKey(long ClientId, Guid TemplateId);
        List<VendorPQsSpecificToClient> GetClientUserAccessiblePqs(long clientId, long vendorId, Guid userId);
        bool IsClientPartOfEri(long clientId);
        InviteTemplateConfiguration GetClientConfiguredInvitationTemplate(long clientId, int? invitationType);
        List<VendorReviewInputViewModel> GetContractorEvaluations(long pqId, long orgId, string role);
        LocalScoreCard GetVendorReviewInput(long pqId, long vendorId, long? buId, long? vendorReviewInputId, long orgId, Guid userId);
        void PostVendorReviewInput(LocalScoreCard scoreCard, long orgId, Guid userId);
        void PostClientBusAndSites(long clientId, string clientName);
        List<string> GetClientsFromClientTemplates(List<long> clientIds);
        string GetPowerBiReportByName(long ClientId, Guid UserId,string Role,string ReportName);
        string GetReportFilter(string ReportName);


    }
}
