﻿/*
Page Created Date:  04/04/2017
Created By: Rajesh Pendela
Purpose: All Business methods related to bidding interests will be declared here
Version: 1.0
****************************************************
*/

using System.Collections.Generic;

namespace FVGen3.BusinessLogic.Interfaces
{
    public interface IBiddingInterestsBusiness
    {
        List<KeyValuePair<long, string>> GetBiddings();
    }
}
