﻿/*
Page Created Date:  11/14/2019
Created By: Kiran Talluri
Purpose: All Business methods related to Quiz will be declared here
Version: 1.0
****************************************************
*/ 

using FVGen3.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FVGen3.BusinessLogic.Interfaces
{
    public interface IQuizBusiness
    {
        QuizDetails getQuizDetails(long quizId);

        QuizWaiverFormCompletionLog getWaiverFormLog(long quizId, DateTime? submittedDate);
    }
}
