﻿/*
Page Created Date:  06/30/2017
Created By: Rajesh Pendela
Purpose: All Business methods related to email templates will be declared here
Version: 1.0
****************************************************
*/


using System;
using FVGen3.Domain.Entities;

using System.Collections.Generic;

namespace FVGen3.BusinessLogic.Interfaces
{
    public interface IEmailTemplateBusiness
    {
        List<KeyValuePair<int, string>> GetGenericEmailTemplates();
        List<EmailTemplates> GetClientEmailTemplates(List<long?> clientIds);
        string SaveLanguageEmailTemplate(int EmailTemplateId, long languageId, string EmailSubject, string EmailBody,string comments);
        EmailTemplates GetEmailData(int EmailTemplateId, string LanguageID);
        long? GetEmailTemplateClientId(int emailTemplateId);
        LocalEmailComposeModel GetEmailTemplateContent(int templateId, long pqId, long? selectedSubClientId, Guid userId);
    }
}
