﻿/*
Page Created Date:  01/27/2019
Created By: Rajesh Pendela
Purpose: All Business methods related to reports module will be declared here
Version: 1.0
****************************************************
*/

using FVGen3.Domain.LocalModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FVGen3.BusinessLogic.Interfaces
{
    public interface IReportsBusiness
    {
        string GetInjuryStatisticsQuery(Guid templateId, long clientId);
        string GetContractVendorsQuery(Guid templateId, long clientId);
        string GetSafetyStatisticsQuery();
        string GetInsuranceInformationQuery(Guid templateId, long clientId);
     bool ShowOrHideData(long clientId);
        string GetVendorInvitesByStatusQuery(Guid templateId, long clientId,string State);
        //List<FeesPaid> GetFee(string fromdate, string todate);
        List<FeesPaid> GetFee(string fromdate, string todate, Guid templateId, long clientId);
    } 
}
