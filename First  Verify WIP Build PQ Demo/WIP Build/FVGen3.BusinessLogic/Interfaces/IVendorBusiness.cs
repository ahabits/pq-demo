﻿/*
Page Created Date:  04/04/2017
Created By: Rajesh Pendela
Purpose: All Business methods related to Vendor will be declared here
Version: 1.0
****************************************************
*/

using System.Collections.Generic;
using FVGen3.DataLayer.DTO;
using FVGen3.Domain.Entities;
using System;
using FVGen3.Domain.LocalModels;
using FVGen3.Domain.ViewModels;

namespace FVGen3.BusinessLogic.Interfaces
{
    public interface IVendorBusiness
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="vendorName"></param>
        /// <param name="biddings"></param>
        /// <param name="city"></param>
        /// <param name="state"></param>
        /// <param name="exceptThisClient"></param>
        /// <param name="pqStatusFilter">
        /// 0-No filter
        /// 1- +Ve Status
        /// 2- -Ve Status
        /// </param>
        /// <returns></returns>
        List<VendorData> GetClientVendors(Guid UserId,bool ERIOnly, bool SingleClient, int orderBy,int status,bool isAsc,out int recordsFiltered,int draw, int start, int length, long clientId, string vendorName,
            List<long> biddings, List<long> claycoBiddings, List<long> proficiency,
            string city, string state,string Country, bool exceptThisClient = false, int pqStatusFilter = 1);

        VendorSummary GetVendorSummary(long vendorId,Guid UserId,long clientId);
        VendorSummary GetERIVendorSummary(long vendorId);
        List<KeyValuePair<string, string>> GetAllVendors();
        List<string> GetVendors(string prefix, int count);
        List<KeyValuePair<long, string>> GetVendorOrganizationsByClientId(long clientId);
        bool AddVendorLog(LocalVendorEmail data);
        List<Dropdown> GetOrganizations(string name, string usertype);
        List<Dropdown> GetVendors(string name, long ClientId);
        List<LocalPrequalificationAdvanced> VendorEmailSearch(string searchType, long clientId, long vendorId,
            string prequalStatus);

        List<VendorData> GetERIClientVendors(int orderBy, int status, bool isAsc, out int recordsFiltered, int draw, int start, int length, long clientId);

        List<LocalinviteVendor> GetInvitedVendors(int pageNumber, string SearchEmail, int InvitationType);
        string GetPQInvitedByForDashboard(string vendorEmail, long clientId, Guid invitationId);
        DataSourceResponse GetGroupedPrequalifications(long ClientId,string searchType, string SortType, string firstLetterVal, string period, Guid UserId, string Role, string Vendorname,bool HasAnalyticsPermission, long organizationid, DataSourceRequest request);
        DataSourceResponse GetVendorsForAdvancedSearch(DataSourceRequest request, string Subsite, string vendorCity, string Country, string vendorState, string biddingIdList, string statusIdList, string proficiencyIdList, string sortType, long clientId, Guid? clientTemplateid, string Site, string claycoBiddingIdList, string clientContractId, string productCategoryList, string GeographicArea, bool resubOnly, bool HasAnalyticsPermission, string venname, string period, string clientrefid, long varclientId, string role, long orgId, Guid userId);
        bool PostVendorInvite(LocalinviteVendor inviteVendor, Guid userId);
        string RiskFee(long client, string riskLevel, bool isERI);

        string Location(List<VendorInviteLocations> InviteVen);
        VendorInviteDetails GetInviteInfo(Guid invitationId);
        void UpdateInviteComments(LocalinviteVendor inviteVendor,Guid userId);
        string SendNewInvitation(Guid invitationId, Guid userid);

        string SendOutsideInvitation(Guid invitationId, Guid userid);
    }
}
