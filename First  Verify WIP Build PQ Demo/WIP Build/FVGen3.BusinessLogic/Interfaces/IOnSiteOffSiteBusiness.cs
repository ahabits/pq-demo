/*
Page Created Date:  04/12/2017
Created By: Rajesh Pendela
Purpose: All Business methods related to Onsite/Offsite module will be declared here
Version: 1.0
****************************************************
*/

using System.Collections.Generic;
using FVGen3.DataLayer.DTO;
using System;
using FVGen3.Domain.ViewModels;

namespace FVGen3.BusinessLogic.Interfaces
{
    public interface IOnSiteOffSiteBusiness
    {
        List<OnSiteOffSiteCheckin> GetCheckins(string location, int workingOffset,Guid UserId,long OrgId);
        string ResetTagorBadgeNumber(Guid EmployeeId,bool IsTagNumber,Guid OnsiteOffsiteID, bool IsVisitor);
        List<CurrentlyOnsite> GetSineProOnsiteData(long clientID);
    }
}
