﻿/*
Page Created Date:  04/10/2017
Created By: Rajesh Pendela
Purpose: All Business methods related to prequalification module will be declared here
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using FVGen3.DataLayer.DTO;
using FVGen3.Domain.Entities;
using System.Data.Entity;
using FVGen3.Domain.LocalModels;
using FVGen3.Domain.Concrete;
using System.Globalization;
using FVGen3.Domain.ViewModels;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace FVGen3.BusinessLogic.Interfaces
{
    public interface IPrequalificationBusiness
    {
        // ReSharper disable once InconsistentNaming
        string UnlockPQDocs(long PQId);
        List<KeyValuePair<long, string>> GetPQStatuses();
        KeyValuePair<long, string> GetPQStatusForBanner(long pqId, string defaultStatus,long clientId, Guid UserId);
        KeyValuePair<long, string> GetPQStatusVendorIndicationText(long pqId, string defaultStatus, long clientId, Guid UserId);
        long GetLocationSectionId(long PQId);
        List<UserInfo> GetPQVendorUser(long PQId);
        IEnumerable<UserInfo> GetPQVendorUser(List<long> PQId);
        LatestPrequalification GetLatestPQ(long clientId, long vendorId);
        List<long> GetVendorClients(long vendorId);
        AddendumModel AddAddendum(AddendumModel addendum);
        //long? GetBUPaymentId(long buId, long pqId);
        string HandleInvitation(long vendorId, string code,Guid loginUser,string comments,out long pqId);
        List<AddendumDTO> GetAddendums(long pqId,long subSecId);
        AddendumModel GetAddendum(long pqSubSectionId);
        bool ToggleAddendumVendorDisply(long pqSubSectionId);
        Prequalification GetPrequalification(long PqId);
        List<KeyValuePair<string, string>> GetPQTemplates(long PQID);
        Templates GetPQTemplate(long PqId);
        string ChangePQTemplate(long PQID,Guid clientTemplateId);
        string GetLockTemplateComments(Guid clientTemplateId);
        bool CheckClientHasTrainingTemplates(long clientId,long PQId);
        List<OrganizationData> GetTemplateOrganizations(long prequalificationId);
        PrequalificationClient GetPrequalificationClient(long pqId);
        List<long> Finalstatus();
        DataSet getERIPrequalifications(Guid? templateId);
        string SubSectionInterimSave(List<PQUserInput> userInputs, long prequalificationId);
        string HideOrUnhidePrequalificationSections(LocalChangeStatus formStatus, Prequalification prequal);
        bool checkSectionHasInterimSubSections(long templateSectionId);
        FaGraphDetails GetGraphData(long PQId, string field);
        List<FinancialAnalyticsModel> GetAnalytics(long VendorId, long ClientId,long PQId);
        List<FinancialAnalyticsModel> GetAnalytics(long PqId);
        string ChangeDocumentName(Guid DocumentId, string DocName);
        Statistics GetStatistics(long VendorId, long ClientId);
        List<KeyValuePair<long, string>> GetParentColor(long pqId, List<long> questionId);
        List<KeyValuePair<long,string>> GetQuestionColor(long PqId, List<long> QuestionId);
        List<FinancialAnalyticsLog> CopyAnalytics(long? oldPreQualificationId, EFDbContext context);
        bool HasFinancialData(long prequalificationid);
        bool HasFinancialAnalytics(long SectionId, Guid userId);
        void ZeroPayments(long pqid);
        IEnumerable<StatusHistoryLog> StatusHistory(long PrequalificationId);
        Document getDocument(Guid? docId);
        CultureInfo SetCulture(string languageCode);
        void MoveAnswers(long PQID, Guid SourceTemplate, Guid DestinationTemplate);
        void MoveEmRValues(long PQID); 
        //void MoveDocuments(long SourcePQID, Guid SourceTemplate, Guid DestinationTemplate);
        void CopyAnswers(long Sourcepqid, long Destpqid, Guid SourceTemplate, Guid DestinationTemplate);
        void ClearPayment(long PQID);
        LocalVendorInviteDetails PQInviteInfo(long pqId);
        LocalVendorInviteDetails GetPQInviteDetails(long pqId);
        LocalVendorInviteDetails AddInvitaionDetailsIntoPq(long ClientId, long VendorId);
        List<KeyValuePair<string, string>> GetInvitationFromOptions(long clientId);

        string UpdateInvitationFromInfo(long pqId, string InvitationFrom);
        List<LocalPrequalification> GetPrequalificationsforAdminDashboard(int pageNumber, int? type);
        List<DocumentsList> GetBUDocs(long BuId);
        long GetPQStatusId(long pqId);
        VendorDetailsViewModel GetPQDetails(long pqId);
        decimal GetVendorReviewAverageSpecificToClient(long vendorId, long clientId);
        long? GetTemplateVendorNumberAssignedQuestion(long pqId);
        List<QuestionAndAnswers> GetVendorDetailsQSections(long pqId);
        bool CheckEvalationExpiration(Guid UserId, long VendorId, long ClientId);
        List<QuestionAndAnswers> GetVendorDetailERIQSections(long pqId);
        string GetPQStatusName(long pqId);
        string GetNotificationComments(long EmailRecordId);
        void AddorUpdateComment(LocalComments comment, Guid userId);
        void DeleteComment(long emailRecordId);
        List<long> GetClientNotificationTypes(long pqId);
        IEnumerable<PrequalificationCommentsViewModel> GetPQComments(long pqId);
        List<TemplateSections> GetTemplateSections(Guid templateId);
        int? GetTemplateSectionType(long secId);
        StatusBannerColor GetPQPreviousStatusAndBannerColor(long pqId, long pqStatusId);
        string GetPQStatusBannerColor(long pqStatusId, string role, int? mode, string statusName, string screenName);

        List<VendorPQsSpecificToClient> GetVendorPqsSpecificToClient(long clientId, long vendorId);
        PQPaymentInfo GetPQPaymemtInfo(long pqId);
        List<LocalTemplatesSideMenuModel> GetPQSideMenusForVendorDetails(long pqId, ref long TemplateSectionID, int? mode, Guid userId, Guid templateId, string role);
        LocalChangeStatus GetPQStatusInfoForChangeStatus(long pqId, Guid templateId);
        List<long> GetPQSectionsToDisplay(long pqId);
        List<LocalTemplatesSideMenuModel> GetPQSideMenus(long pqId, int? mode, Guid templateId, string role, string screenName);
        VendorDetailsViewModel GetPQCommonData(long pqId);
        List<KeyValuePair<string, string>> GetAdminUsers();
        void PostPqStatusChange(LocalChangeStatus formData, string userId);
        TemplateSectionViewModel GetTemplateSection(long pqId, long templateSectionId, string role, Guid userId);
        void PostPqAssignerAndExceptionStatus(LocalChangeStatus formData);
        TemplateSubSections GetTemplateSubSection(long subSectionId);
        List<IGrouping<long?, SupportingDocumentsViewModel>> GetPqSupportingDocs(long sectionId, long pqId, string role, long orgId);
        List<PqReferenceDocumentsViewModel> GetReferenceDocs(string refType, long refRecId, long? docTypeId, long? clientId, bool? orderBy);
        decimal GetPqPriceToPay(long vendorId, Guid? clientTemplateId, long preQualificationId);
        Task<PqPaymentsViewModel> GetPaymentData(long preQualificationId);
        decimal ApplyFeeCap(long vendorId, DateTime currentDate, long pqId, decimal totalAdditionalFee);
        PqPaymentCommonViewModel GetPqPaymentCommonData(long pqId, long vendorId, Guid? clientTemplateId);
        PqViewModel GetPqInfo(long pqId, long vendorId, Guid? clientTemplateId);
        long? GetTemplateLanguage(Guid templateId);
        long GetPqTemplateSectionId(long pqId, long templateSectionId, string role );
        bool PqHasCompletedSection(long pqId, long templateSectionId);
        List<LocalTemplatesSideMenuModel> GetPqSideMenusForQuestionnaire(long pqId, int? mode, Guid templateId, string role, Guid userId, ref long templateSectionId);
        StatusBannerColor GetBuStatusBannerColor(long pqId, long? buId, string role);
        void LogEmailNotification(LocalEmailComposeModel email, Guid userId);
        //bool CheckReviewDate(long ClientId, long VendorId, Guid UserId);
    }
}

