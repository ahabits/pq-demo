﻿/*
Page Created Date:  07/09/2018
Created By: Kiran Talluri
Purpose: All Business methods related to employees will be declared here
Version: 1.0
****************************************************
*/

using FVGen3.DataLayer.DTO;
using FVGen3.Domain.Entities;
using FVGen3.Domain.LocalModels;
using FVGen3.Domain.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FVGen3.BusinessLogic.Interfaces
{
    public interface IEmployeeBusiness
    {
        string DeleteTrainings(long SysUserOrgQuizId);
        string UpdateEmployeeDetailsAndAcknowledge(LocalEmployeeDetails empData);
        string AcknowledgeEmployee(Guid empId);
        List<VendorEmployeeTraining> VendorEmployeeTrainings(long clientid);
        string deleteEmployee(long sysUserOrgId);
        string AddEmployee(EmpRegistrationLocalModel emp);
        LocalEmployeeQuizzes EmployeeQuizs(Guid userId, int? Type,Guid Loginuserid, List<long> clients);
        List<LocalAddCategories> GetTrainingCategories(long quizId, string role);
        void PostEmployeeQuizCategories(List<LocalAddCategories> empQuizCategories, Guid userId);
        List<LocalAddCategories> GetEmployeeCategories(long sysUserOrgId, string role);
        void PostEmployeeCategories(List<LocalAddCategories> empCategories, Guid userId);
        List<OrganizationUsers> GetEmployees(long pqId, List<long> clientIds);
        bool IsZeroFeeTraining(Guid? clientTemplateId);
        bool IsVendorReachedTrainingFeeCap(long? vendorId, long? clientId);
        string SaveSignProCheckinCheckOut(SineProCheckinCheckout info);
    }
}
