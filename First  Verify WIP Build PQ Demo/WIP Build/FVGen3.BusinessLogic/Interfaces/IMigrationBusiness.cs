﻿/*
Page Created Date:  03/01/2019
Created By: Kiran Talluri
Purpose: All Business methods related to vendor migration from single client to multi client will be declared here
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using FVGen3.DataLayer.DTO;
using FVGen3.Domain.Entities;
using System.Data.Entity;
using FVGen3.Domain.LocalModels;
using FVGen3.Domain.Concrete;

namespace FVGen3.BusinessLogic.Interfaces
{
    public interface IMigrationBusiness
    {
        void CopyEmRValues(long sourcepqid, long destinationpqid, long Vendorid, List<QuestionsForMigration> Data);
        void CopyDocuments(long sourcepqid, long destinationpqid, long Vendorid, List<QuestionsForMigration> Data);
        void CopySites(long sourcepqid, long destinationpqid);
        void CopyAnswers(long Sourcepqid, long Destpqid, List<QuestionsForMigration> Data);
        void AddPQClient(long destinationpqid, long clientid);
        void RecordPayment(long Sourcepqid, long Destpqid, List<QuestionsForMigration> Data);
        void MoveComments(long sourcePQ, long destPQ, List<QuestionsForMigration> Data);
        void MoveReportingData(long sourcePQ, long destPQ);
        void PQCompletedSections(long destinationpqid, long Sourcepqid, List<QuestionsForMigration> Data);
        void CopyVendorDetails(long Sourcepqid, long Destpqid);
        void NotificationEmailComments(long sourcePQ, long destPQ);
        void SectionstoDisplay(long Sourcepqid, long destinationpqid, List<QuestionsForMigration> Data);
        void deletePrequalification(long prequalId);
    }
}
 