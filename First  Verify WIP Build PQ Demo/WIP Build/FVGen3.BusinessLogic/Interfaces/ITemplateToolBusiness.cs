﻿/*
Page Created Date:  06/30/2017
Created By: Rajesh Pendela
Purpose: All Business methods related to Templates will be declared here
Version: 1.0
****************************************************
*/

using FVGen3.DataLayer.DTO;
using FVGen3.Domain.Entities;
using FVGen3.Domain.LocalModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FVGen3.BusinessLogic.Interfaces
{

    public interface ITemplateToolBusiness
    {
        void ConfigureAgreementEmail(AgreementSubsectionModel agreement);
        AgreementSubsectionModel GetAgreementConfiguration(long subSectionId);
        AgreementSubSectionLog CreateAgreementSubsectionLog(LocalEmailComposeModel emailLog);
        ClientSignature GetClientSignature(long subSectionId);
        List<LocQuestions> GetTemplateQuestins(Guid Templateid);
        string UpdateSign(ClientSignature sign);
        List<KeyValuePair<long, string>> GetTemplateSubSections(long SectionId);
        string ChangeSubSectionName(long SubsecId, string SubsectionName);
        List<Templates> GetTemplates();
        List<KeyValuePair<long, string>> GetTemplateSections(Guid Template);
        List<Questions> GetQuestions(long SubSectionId);
        void mappingQuestions(LocalCopyQuestions CopyQuestions);
        Questions GetQuestion(long QuestionId);
        LocalMappedQuestions GetMappedQuestions(long SSubsectionId, long DSubsectionId);
        string IsCompatibleQuestion(long DestinationquestionId, long SQuestionId);
        void mappingQuestionsTest(string fieldsWithValues);

        Task<string> UpdateSubSectionVisiblity(long SubSecId, bool IsShow);
    }
}
