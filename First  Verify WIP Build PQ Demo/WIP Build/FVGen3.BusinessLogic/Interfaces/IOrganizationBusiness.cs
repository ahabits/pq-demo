﻿/*
Page Created Date:  04/04/2017
Created By: Rajesh Pendela
Purpose: All Business methods related to an organization will be declared here
Version: 1.0
****************************************************
*/

using System.Collections.Generic;
using FVGen3.DataLayer.DTO;
using System;
using FVGen3.Domain.Entities;
using FVGen3.Domain.ViewModels;

namespace FVGen3.BusinessLogic.Interfaces
{
    public interface IOrganizationBusiness
    {
        OrganizationData GetOrganization(long orgId);
        List<UserInfo> GetOrganizationUsers(long orgId);
        UserInfo GetSuperuser(long orgId);
        OrganizationData GetOrganization(string ClientSecret);
        List<LocOrganizationRoles> GetOrganizationsWithPermission(string RoleId);
        string AssignOrgRoles(string roleId, List<long> orgIds);
        List<LocOrganizationRoles> GetOrganizationsWithRole(string RoleId);
        bool HasOrgRoles(string RoleId);
        List<KeyValuePair<string, string>> GetCountries();
        List<KeyValuePair<string, string>> GetRegions();
        bool IsSubClient(long orgId);
        bool IsTemplateLocked(string invitationCode);
        List<KeyValuePair<string,string>>GetCountryStates(string country);
        List<KeyValuePair<String, string>> GetCountryLanguages(string country);
        string GetLanguageCode(long languageId);
        OrganizationData GetSuperClient(long orgId);
        string BulkLocationInsertion(string file, Guid templateId, Guid userId);
        string BulkVendorInsertion(string file, Guid TemplateId, Guid UserId);
        List<KeyValuePair<string, string>> GetOrgUsers(long orgId);
        List<KeyValuePair<string, string>> GetClientOrganizations();
        List<OrganizationData> GetOrganizations(string companyName, string typeClient, string typeVendor, string typeAdmin);
        long PostOrganization(OrganizationData newOrganization, long orgId);
        string CheckAnyOrganizationHasTaxId(string taxId, long? organizationId);
        void PostSubClient(long orgId, long subClientId);
        bool IsVendorHasFinancialData(long vendorId);
        List<ArchiveOrganizationDetailsViewModel> GetArchivedArchiveOrganizationDetails(long vendorId);
    }
}
