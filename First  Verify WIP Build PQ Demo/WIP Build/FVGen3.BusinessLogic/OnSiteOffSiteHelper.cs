/*
Page Created Date:  04/12/2017
Created By: Rajesh Pendela
Purpose: All helper operations related to Onsite/Offsite module will be performed here
Version: 1.0
****************************************************
*/


using FVGen3.Domain.ViewModels;
using System.Data.Common;

namespace FVGen3.BusinessLogic
{
    public class OnSiteOffSiteHelper
    {
        /// <summary>
        /// Query to get the checkin history
        /// </summary>
        /// <param name="workingOffset"></param>
        /// <param name="location"></param>
        /// <returns></returns>
        public string GetCheckinsQuery(int workingOffset, string location,long checkoutTime,long OrgId)
        {
            string workingOffsetString = (workingOffset * -1).ToString();
            var data = "";
            data = @"
select sites.SiteName, o.Name as VendorName, (c.LastName + ', ' + c.FirstName )as VisitorName, (c1.LastName + ', ' + c1.FirstName )as SecurityNameCheckin,
                      dateadd(HOUR, " + workingOffsetString + @" ,oo.Checkin) as checkin ,bus.BusinessUnitName as [BUName],null as CompanyName,null as SiteContact,
                      oo.TagNumber as TagNumber, oo.BadgeNumber as BadgeNumber,oo.VendorEmployeeID as EmployeeOrVisitorID,oo.VendorID as VendorID,oo.OnsiteOffsiteID as OnsiteOffsiteID,oo.Usertype
                      from
                      dbo.OnsiteOffsite oo
                      join Organizations o on o.OrganizationID = oo.VendorID
                      join Contact c on c.UserID  = oo.VendorEmployeeID
                      left outer join Contact c1 on c1.UserID  = oo.CheckinClientUser                      
                      join ClientBusinessUnits bus on bus.ClientBusinessUnitId = oo.ClientBusinessUnitId
                      join ClientBusinessUnitSites sites on sites.ClientBusinessUnitSiteId = oo.ClientBusinessUnitSiteId
 join
 (select Max(Checkin) as checkinMax, max(Checkout) as checkoutMax ,DATEDIFF(hour, Max(Checkin), Max(GETDATE())) as timediff,
                    VendorEmployeeID, VendorID,ClientID,ClientBusinessUnitSiteID,ClientBusinessUnitId from OnsiteOffsite
Group By VendorEmployeeID, VendorID,ClientID,ClientBusinessUnitSiteID,ClientBusinessUnitId
                     having DATEDIFF(hour, Max(Checkin), Max(GETDATE())) < "+ checkoutTime + @"
                     and (max(Checkout) is null or max(Checkout) < max(Checkin))) ag on
                   oo.VendorEmployeeID = ag.VendorEmployeeID and
                   oo.VendorID = ag.VendorID and
                   oo.ClientID = ag.ClientID and
                   oo.ClientBusinessUnitID = ag.ClientBusinessUnitID and
                   oo.ClientBusinessUnitSiteID = ag.ClientBusinessUnitSiteID and
                   oo.Checkin = ag.checkinMax ";
            if (location != "-1")
            {
                data = data + " and (@location is null  or @location = oo.ClientBusinessUnitSiteId) ";
            }

            data = data + @" UNION
                        select sites.SiteName, null as VendorName, (v.LastName + ', ' + v.FirstName )as VisitorName, (c1.LastName + ', ' + c1.FirstName )as SecurityNameCheckin,
                      dateadd(HOUR, " + workingOffsetString + @",oo.Checkin) as checkin,bus.BusinessUnitName as [BUName],v.Company as CompanyName,v.SiteContact as SiteContact,
                        oo.TagNumber as TagNumber, oo.BadgeNumber as BadgeNumber,oo.VisitorId as EmployeeOrVisitorID,null as VendorID,oo.OnsiteOffsiteID as OnsiteOffsiteID,0 as Usertype
                       from
                      dbo.[OnsiteOffsiteVisitor] oo                      
                      join Visitors v on v.VisitorId  = oo.VisitorId
                      left outer join Contact c1 on c1.UserID  = oo.CheckinClientUser                      
                      join ClientBusinessUnits bus on bus.ClientBusinessUnitId = oo.ClientBusinessUnitId
                      join ClientBusinessUnitSites sites on sites.ClientBusinessUnitSiteId = oo.ClientBusinessUnitSiteId
  join
 (select Max(Checkin) as checkinMax, max(Checkout) as checkoutMax ,DATEDIFF(hour, Max(Checkin), Max(GETDATE())) as timediff,
                    VisitorID, ClientID,ClientBusinessUnitSiteID,ClientBusinessUnitId from OnsiteOffsiteVisitor
Group By VisitorID, ClientID,ClientBusinessUnitSiteID,ClientBusinessUnitId
                     having DATEDIFF(hour, Max(Checkin), Max(GETDATE())) < " + checkoutTime + @"
                     and (max(Checkout) is null or max(Checkout) < max(Checkin))) ag on
                   oo.VisitorID = ag.VisitorID and                    
                   oo.ClientID = ag.ClientID and
                   oo.ClientBusinessUnitID = ag.ClientBusinessUnitID and
                   oo.ClientBusinessUnitSiteID = ag.ClientBusinessUnitSiteID and
                   oo.Checkin = ag.checkinMax

            ";
            if (location != "-1")
            {
                data = data + " and (@location is null  or @location = oo.ClientBusinessUnitSiteId) ";
            }
          
            data = data + @" union all

             select* from(
select null as siteName, o.Name as VendorName, (c.LastName + ', ' + c.FirstName) as VisitorName, (c1.LastName + ', ' + c1.FirstName) as SecurityNameCheckin,
                      dateadd(HOUR, "+workingOffset+@", oo.Checkin) as checkin, null as [BUName], null as CompanyName, null as SiteContact,
                      oo.TagNumber as TagNumber, oo.BadgeNumber as BadgeNumber, oo.VendorEmployeeID as EmployeeOrVisitorID, oo.VendorID as VendorID, oo.OnsiteOffsiteID as OnsiteOffsiteID, oo.Usertype
                      from
                      dbo.OnsiteOffsite oo
                      join Organizations o on o.OrganizationID = oo.Clientid
                      join Contact c on c.UserID = oo.VendorEmployeeID
                      left outer join Contact c1 on c1.UserID = oo.CheckinClientUser


 join
 (select Max(Checkin) as checkinMax, max(Checkout) as checkoutMax, DATEDIFF(hour, Max(Checkin), Max(GETDATE())) as timediff,
                    VendorEmployeeID, VendorID, ClientID from OnsiteOffsite
 Group By VendorEmployeeID, VendorID, ClientID
                     having DATEDIFF(hour, Max(Checkin), Max(GETDATE())) < " + checkoutTime + @"
                     and(max(Checkout) is null or max(Checkout) < max(Checkin))) ag on
                   oo.VendorEmployeeID = ag.VendorEmployeeID and
                   oo.VendorID = ag.VendorID and
                   oo.ClientID = ag.ClientID and


                   oo.Checkin = ag.checkinMax ) as client

                    where client.UserType = 2 and client.VendorID="+OrgId;
           
            return data;
        }
        public string GetClientCheckinQuery(int workingOffset,long checkoutTime)
        {
            string workingOffsetString = (workingOffset * -1).ToString();

            
            var data = @"
             select* from(
select '' as siteName, o.Name as VendorName, (c.LastName + ', ' + c.FirstName) as VisitorName, (c1.LastName + ', ' + c1.FirstName) as SecurityNameCheckin,
                      dateadd(HOUR,"+ workingOffsetString + @", oo.Checkin) as checkin, d.DepartmentName as [BUName], null as CompanyName, '' as SiteContact,
                      oo.TagNumber as TagNumber, oo.BadgeNumber as BadgeNumber, oo.VendorEmployeeID as EmployeeOrVisitorID, oo.VendorID as VendorID, oo.OnsiteOffsiteID as OnsiteOffsiteID, oo.Usertype
                      from
                      dbo.OnsiteOffsite oo
                      join Organizations o on o.OrganizationID = oo.VendorID
                      left join Department d on d.ID=oo.DepartmentId
                      join Contact c on c.UserID = oo.VendorEmployeeID
                      left outer join Contact c1 on c1.UserID = oo.CheckinClientUser


 join
 (select Max(Checkin) as checkinMax, max(Checkout) as checkoutMax, DATEDIFF(hour, Max(Checkin), Max(GETDATE())) as timediff,
                    VendorEmployeeID, VendorID, ClientID from OnsiteOffsite
 Group By VendorEmployeeID, VendorID, ClientID
                     having DATEDIFF(hour, Max(Checkin), Max(GETDATE())) < " + checkoutTime + @"
                     and(max(Checkout) is null or max(Checkout) < max(Checkin))) ag on
                   oo.VendorEmployeeID = ag.VendorEmployeeID and
                   oo.VendorID = ag.VendorID and
                   oo.ClientID = ag.ClientID and


                   oo.Checkin = ag.checkinMax  ) as client

                    where client.UserType = 2";
            return data;
        }
        public string GetVendorCheckinsQuery(int workingOffset, string location, long checkoutTime)
        {
            string workingOffsetString = (workingOffset * -1).ToString();

            var data = @"
select '' SiteName, o.Name as VendorName, (c.LastName + ', ' + c.FirstName )as VisitorName, (c1.LastName + ', ' + c1.FirstName )as SecurityNameCheckin,
                      dateadd(HOUR, 0 ,oo.Checkin) as checkin ,'' as [BUName],null as CompanyName,null as SiteContact,
                      oo.TagNumber as TagNumber, oo.BadgeNumber as BadgeNumber,oo.VendorEmployeeID as EmployeeOrVisitorID,oo.VendorID as VendorID,oo.OnsiteOffsiteID as OnsiteOffsiteID,oo.Usertype
                      from
                      dbo.OnsiteOffsite oo
                      join Organizations o on o.OrganizationID = oo.VendorID
                      join Contact c on c.UserID  = oo.VendorEmployeeID
                      left outer join Contact c1 on c1.UserID  = oo.CheckinClientUser                      
                      

 join
 (select Max(Checkin) as checkinMax, max(Checkout) as checkoutMax ,DATEDIFF(hour, Max(Checkin), Max(GETDATE())) as timediff,
                    VendorEmployeeID, VendorID,ClientID,ClientBusinessUnitSiteID,ClientBusinessUnitId from OnsiteOffsite
Group By VendorEmployeeID, VendorID,ClientID,ClientBusinessUnitSiteID,ClientBusinessUnitId
                     having DATEDIFF(hour, Max(Checkin), Max(GETDATE())) < 13
                     and (max(Checkout) is null or max(Checkout) < max(Checkin))) ag on
                   oo.VendorEmployeeID = ag.VendorEmployeeID and
                   oo.VendorID = ag.VendorID and
                   oo.ClientID = ag.ClientID and
                   
                   oo.Checkin = ag.checkinMax and UserType=2
				   union

select sites.SiteName, o.Name as VendorName, (c.LastName + ', ' + c.FirstName )as VisitorName, (c1.LastName + ', ' + c1.FirstName )as SecurityNameCheckin,
                      dateadd(HOUR, " + workingOffsetString + @" ,oo.Checkin) as checkin ,bus.BusinessUnitName as [BUName],null as CompanyName,null as SiteContact,
                      oo.TagNumber as TagNumber, oo.BadgeNumber as BadgeNumber,oo.VendorEmployeeID as EmployeeOrVisitorID,oo.VendorID as VendorID,oo.OnsiteOffsiteID as OnsiteOffsiteID,oo.Usertype
                      from
                      dbo.OnsiteOffsite oo
                      join Organizations o on o.OrganizationID = oo.VendorID
                      join Contact c on c.UserID  = oo.VendorEmployeeID
                      left outer join Contact c1 on c1.UserID  = oo.CheckinClientUser                      
                      join ClientBusinessUnits bus on bus.ClientBusinessUnitId = oo.ClientBusinessUnitId
                      join ClientBusinessUnitSites sites on sites.ClientBusinessUnitSiteId = oo.ClientBusinessUnitSiteId
 join
 (select Max(Checkin) as checkinMax, max(Checkout) as checkoutMax ,DATEDIFF(hour, Max(Checkin), Max(GETDATE())) as timediff,
                    VendorEmployeeID, VendorID,ClientID,ClientBusinessUnitSiteID,ClientBusinessUnitId from OnsiteOffsite
Group By VendorEmployeeID, VendorID,ClientID,ClientBusinessUnitSiteID,ClientBusinessUnitId
                     having DATEDIFF(hour, Max(Checkin), Max(GETDATE())) < " + checkoutTime + @"
                     and (max(Checkout) is null or max(Checkout) < max(Checkin))) ag on
                   oo.VendorEmployeeID = ag.VendorEmployeeID and
                   oo.VendorID = ag.VendorID and
                   oo.ClientID = ag.ClientID and
                   oo.ClientBusinessUnitID = ag.ClientBusinessUnitID and
                   oo.ClientBusinessUnitSiteID = ag.ClientBusinessUnitSiteID and
                   oo.Checkin = ag.checkinMax ";
            if (location != "-1")
            {
                data = data + " and (@location is null  or @location = oo.ClientBusinessUnitSiteId) ";
            }

            data = data + @" UNION
                        select sites.SiteName, null as VendorName, (v.LastName + ', ' + v.FirstName )as VisitorName, (c1.LastName + ', ' + c1.FirstName )as SecurityNameCheckin,
                      dateadd(HOUR, " + workingOffsetString + @",oo.Checkin) as checkin,bus.BusinessUnitName as [BUName],v.Company as CompanyName,v.SiteContact as SiteContact,
                        oo.TagNumber as TagNumber, oo.BadgeNumber as BadgeNumber,oo.VisitorId as EmployeeOrVisitorID,null as VendorID,oo.OnsiteOffsiteID as OnsiteOffsiteID,0 as Usertype
                       from
                      dbo.[OnsiteOffsiteVisitor] oo                      
                      join Visitors v on v.VisitorId  = oo.VisitorId
                      left outer join Contact c1 on c1.UserID  = oo.CheckinClientUser                      
                      join ClientBusinessUnits bus on bus.ClientBusinessUnitId = oo.ClientBusinessUnitId
                      join ClientBusinessUnitSites sites on sites.ClientBusinessUnitSiteId = oo.ClientBusinessUnitSiteId
  join
 (select Max(Checkin) as checkinMax, max(Checkout) as checkoutMax ,DATEDIFF(hour, Max(Checkin), Max(GETDATE())) as timediff,
                    VisitorID, ClientID,ClientBusinessUnitSiteID,ClientBusinessUnitId from OnsiteOffsiteVisitor
Group By VisitorID, ClientID,ClientBusinessUnitSiteID,ClientBusinessUnitId
                     having DATEDIFF(hour, Max(Checkin), Max(GETDATE())) < " + checkoutTime + @"
                     and (max(Checkout) is null or max(Checkout) < max(Checkin))) ag on
                   oo.VisitorID = ag.VisitorID and                    
                   oo.ClientID = ag.ClientID and
                   oo.ClientBusinessUnitID = ag.ClientBusinessUnitID and
                   oo.ClientBusinessUnitSiteID = ag.ClientBusinessUnitSiteID and
                   oo.Checkin = ag.checkinMax
            ";
            if (location != "-1")
            {
                data = data + " and (@location is null  or @location = oo.ClientBusinessUnitSiteId) ";
            }


            data = data + " order by checkin desc";

            return data;
        }
        public string GetSineProCheckin(long clientID)
        {
            var data = @"
             
select LastName+','+Firstname as VisitorName,CompanyName ,  convert(nvarchar(50), CheckinTime,101)+' ' + convert(nvarchar(50), CheckinTime,108)    as Checkin,sitename  as SiteName,SiteId,CheckinUser from [dbo].[SineProCheckinCheckout] where clientid=" + clientID + @" and checkouttime is null 
 ";
            return data;
        }
    }
}
