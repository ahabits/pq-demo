﻿/*
Page Created Date:  05/18/2017
Created By: Rajesh Pendela
Purpose: All Business operations related to Proficiency Capabilitis module will be performed here
Version: 1.0
****************************************************
*/

using System.Collections.Generic;
using System.Linq;
using System.Web;
using FVGen3.BusinessLogic.Interfaces;
using FVGen3.DataLayer.DTO;
using FVGen3.Domain.Concrete;

namespace FVGen3.BusinessLogic
{
    public class ProficiencyBusiness: BaseBusinessClass, IProficiencyBusiness
    {
        //public ProficiencyBusiness(HttpContext httpContext) : base(httpContext)
        //{
        //}
        /// <summary>
        /// To get the proficiency capabilities 
        /// </summary>
        /// <returns></returns>
        public List<ProficiencyData> GetProficiencies()
        {
            using (var entity =new EFDbContext())
            {
                return entity.ProficiencyCapabilities.Where(rec => rec.Status == null || rec.Status == 1).Select(r => new ProficiencyData
                {
                    ProficiencyName = r.ProficiencyName,
                    Id = r.ProficiencyId,
                    Category = r.ProficiencyCategory,
                    SubCategory = r.ProficiencySubCategroy
                }).ToList();
            }
        }
    }
}
