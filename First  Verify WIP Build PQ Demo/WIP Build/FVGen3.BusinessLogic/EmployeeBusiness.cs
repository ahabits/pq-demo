﻿/*
Page Created Date:  07/09/2018
Created By: Kiran Talluri
Purpose: All Business operations related to employees will be performed here
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FVGen3.BusinessLogic.Interfaces;
using FVGen3.Domain.Concrete;

using FVGen3.Domain.Entities;
using FVGen3.DataLayer.DTO;

using System.Data.SqlClient;
using FVGen3.Domain.LocalModels;
using System.IO;
using System.Configuration;
using FVGen3.WebUI.Constants;
using System.Web;
using Resources;
using FVGen3.WebUI.Constants;
using System.Data.Entity;
using FVGen3.Domain.ViewModels;

namespace FVGen3.BusinessLogic
{
    public class EmployeeBusiness : IEmployeeBusiness
    {
        /// <summary>
        /// To delete the specif employee quiz
        /// </summary>
        /// <param name="SysUserOrgQuizId"></param>
        /// <returns></returns>
        public string DeleteTrainings(long SysUserOrgQuizId)
        {
            //using (var deleteTable = new EFDbContext(LocalConstants.DeleteDBContext))

            using (var context = new EFDbContext())
            {
                var employee = context.SystemUsersOrganizationsQuizzes.Where(r => r.SysUserOrgQuizId == SysUserOrgQuizId).ToList();

                foreach (var emplist in employee)
                {
                    var Emplist = context.EmployeeQuizUserInput.Where(r => r.SysUserOrgQuizId == SysUserOrgQuizId).ToList();
                    foreach (var Emp in Emplist)
                    {
                        //EmployeeQuizUserInput input = new EmployeeQuizUserInput();
                        //input.EmpQuizRecId = Emp.EmpQuizRecId;
                        //input.QuestionColumnId = Emp.QuestionColumnId;
                        //input.UserInput = Emp.UserInput;
                        //input.SysUserOrgQuizId = Emp.SysUserOrgQuizId;
                        //deleteTable.EmployeeQuizUserInput.Add(input);
                        context.EmployeeQuizUserInput.Remove(Emp);
                    }

                    foreach (var quizes in emplist.QuizzesCompletedSections.ToList())
                    {
                        context.QuizzesCompletedSections.Remove(quizes);
                    }
                    foreach (var lists in emplist.QuizPayments.ToList())
                    {
                        foreach (var items in lists.QuizPaymentsItems.ToList())
                        {
                            context.QuizPaymentsItems.Remove(items);
                        }
                        context.QuizPayments.Remove(lists);
                    }
                    foreach (var category in emplist.SystemUsersOrganizationsQuizzesCategories.ToList())
                    {
                        context.SystemUsersOrganizationsQuizzesCategories.Remove(category);
                    }
                    foreach (var quizHistory in emplist.SysUserOrgQuizHistory.ToList())
                    {
                        context.SysUserOrgQuizHistory.Remove(quizHistory);
                    }

                    context.SystemUsersOrganizationsQuizzes.Remove(emplist);
                }

                context.SaveChanges();
                //deleteTable.SaveChanges();
            }
            return "success";
        }
        /// <summary>
        /// To get the vendor employee training summary for specifc client
        /// </summary>
        /// <param name="clientid"></param>
        /// <returns></returns>
        public List<VendorEmployeeTraining> VendorEmployeeTrainings(long clientid)
        {
            using (var context = new EFDbContext())
            {

                var RoleIds = EmployeeRole.VendorEmpRoleId;

                var triningsNames = context.Database.SqlQuery<VendorEmployeeTraining>(@"select ven.Name as Vendor,
        ps.PrequalificationStatusName VendorStatus,
c.FirstName+' '+c.LastName as EmpName,t.TemplateName QuizName,
case when t.TemplateName is NULL then '' when suoq.QuizResult=1 then 'Passed' when suoq.QuizResult=0 then 'Failed' else 'Not Attended'  end as QuizResult,suoq.QuizExpirationDate as  QuizExpireDate from LatestPrequalification P 
join PrequalificationStatus PS
 on p.PrequalificationStatusId=ps.PrequalificationStatusId 
 Join Organizations ven on ven.OrganizationID=p.VendorId
 left join SystemUsersOrganizations suo on suo.OrganizationId=ven.OrganizationID 
 left join SystemUsersOrganizationsRoles suor on suo.SysUserOrganizationId =suor.SysUserOrgId
 left join SystemUsersOrganizationsQuizzes suoq on suoq.SysUserOrgId=suo.SysUserOrganizationId 
 join SystemUsers su on su.UserId=suo.UserId
 join Contact c on c.UserId=su.UserId
 left join ClientTemplates ct on ct.ClientTemplateID=suoq.ClientTemplateId
 left join Templates t on t.TemplateID=ct.TemplateID
 where p.ClientId=@clientidVar and ct.ClientId=@clientidVar and suor.SysRoleId in (@RoleId) 
 
 union
 
 (select ven.Name as Vendor,
        ps.PrequalificationStatusName VendorStatus,
'' as EmpName,'' QuizName,'' QuizResult,
null as  QuizExpireDate from LatestPrequalification P 
join PrequalificationStatus PS
 on p.PrequalificationStatusId=ps.PrequalificationStatusId 
 Join Organizations ven on ven.OrganizationID=p.VendorId  where p.ClientId=@clientidVar and p.VendorId not in(select p.VendorId  from LatestPrequalification P 
join PrequalificationStatus PS
 on p.PrequalificationStatusId=ps.PrequalificationStatusId 
 Join Organizations ven on ven.OrganizationID=p.VendorId
 left join SystemUsersOrganizations suo on suo.OrganizationId=ven.OrganizationID 
 left join SystemUsersOrganizationsRoles suor on suo.SysUserOrganizationId =suor.SysUserOrgId
 left join SystemUsersOrganizationsQuizzes suoq on suoq.SysUserOrgId=suo.SysUserOrganizationId 
 join SystemUsers su on su.UserId=suo.UserId
 join Contact c on c.UserId=su.UserId
 left join ClientTemplates ct on ct.ClientTemplateID=suoq.ClientTemplateId
 left join Templates t on t.TemplateID=ct.TemplateID
 where p.ClientId=@clientidVar and ct.ClientId=@clientidVar and suor.SysRoleId in (@RoleId)))


 order by ven.Name,ps.PrequalificationStatusName,c.FirstName+' '+c.LastName,suoq.QuizExpirationDate", new SqlParameter("@clientidVar", clientid), new SqlParameter("@RoleId", RoleIds)).ToList();


                //var existingVendors = triningsNames.Select(r => r.Vendor).Distinct();
                //var temp = context.LatestPrequalification.Where(r => r.ClientId == clientid &&!existingVendors.Contains(r.Vendor.Name)).Select(r=>new VendorEmployeeTraining() {Vendor=r.Vendor.Name,VendorStatus=r.PrequalificationStatus.PrequalificationStatusName }).ToList();
                //triningsNames.AddRange(temp);
                //return triningsNames.OrderBy(r=>r.Vendor).ThenBy(r=>r.VendorStatus).ThenBy(r=>r.EmpName).ThenBy(r=>r.QuizExpireDate).ToList();
                return triningsNames;

            }

        }

        public string UpdateEmployeeDetailsAndAcknowledge(LocalEmployeeDetails empData)
        {
            using (var entity = new EFDbContext())
            {
                var empContact = entity.Contact.FirstOrDefault(rec => rec.UserId == empData.UserId);
                //empContact.PhoneNumber = empData.PhoneNumber;
                if (empData.DocumentType == 1)
                {
                    empContact.DocumentType = empData.DocumentType;
                    empContact.DriversLicenseLast4Digits = empData.Last4Digits;
                }
                else if (empData.DocumentType == 2)
                {
                    empContact.DocumentType = empData.DocumentType;
                    empContact.GreenCardLast4Digits = empData.Last4Digits;
                }
                else if (empData.DocumentType == 3)
                {
                    empContact.DocumentType = empData.DocumentType;
                    empContact.SocialSecurityNumberLast4Digits = empData.Last4Digits;
                }
                else if (empData.DocumentType == 4)
                {
                    empContact.DocumentType = empData.DocumentType;
                    empContact.PhoneNumberLast4Digits = empData.Last4Digits;
                }
                entity.Entry(empContact).State = EntityState.Modified;
                entity.SaveChanges();
            }
            return "success";
        }

        public string AcknowledgeEmployee(Guid empId)
        {
            using (var entity = new EFDbContext())
            {
                UserAcknowledgement userAck = new UserAcknowledgement();
                userAck.Acknowledgement = true;
                userAck.SubmittedOn = DateTime.Now;
                userAck.SubmittedBy = empId;

                entity.Entry(userAck).State = EntityState.Added;
                entity.SaveChanges();
            }
            return "success";
        }
        public string AddEmployee(EmpRegistrationLocalModel employee)
        {
            using (var entityDB = new EFDbContext())
            {
                var quizIdsList = new List<string>();
                var isUserExist = entityDB.SystemUsers.Any(r => r.UserName == employee.MobileNumber);
                if (isUserExist)
                    return "User ID already exists. Please use another User ID (mobile number or email) for registration.";
                SystemApplicationId sysAppId = entityDB.SystemApplicationId.FirstOrDefault();

                SystemUsersOrganizations currenrtUser_sysUserOrg = new SystemUsersOrganizations();

                SystemUsersOrganizationsRoles currentUser_sysUserOrgRole = new SystemUsersOrganizationsRoles();

                currenrtUser_sysUserOrg.OrganizationId = employee.VendorId;

                currenrtUser_sysUserOrg.SystemUsers = new SystemUsers();

                currenrtUser_sysUserOrg.SystemUsers.ApplicationId = sysAppId.ApplicationId;
                currenrtUser_sysUserOrg.SystemUsers.UserName = employee.MobileNumber;
                //var fourLetterUniqueKeyForEmpployee = Utilities.EncryptionDecryption.GetUniquePasswordForEmployee();
                currenrtUser_sysUserOrg.SystemUsers.Password = employee.Password;//Utilities.EncryptionDecryption.EncryptData(fourLetterUniqueKeyForEmpployee);
                //if (employee.UserHasEmail)
                //{
                //    currenrtUser_sysUserOrg.SystemUsers.Email = employee.MobileNumber;
                //}
                currenrtUser_sysUserOrg.SystemUsers.CreateDate = DateTime.Now;
                currenrtUser_sysUserOrg.SystemUsers.LastLoginDate = DateTime.Now;
                currenrtUser_sysUserOrg.SystemUsers.UserStatus = true;
                currenrtUser_sysUserOrg.SystemUsers.LanguageId = employee.LanguageId.Value;
                Contact currentUser_Contact = new Contact();

                currentUser_Contact.FirstName = employee.FirstName;
                currentUser_Contact.LastName = employee.LastName;
                //currentUser_Contact.ContactTitle = employee.Title;
                currentUser_Contact.PhoneNumber = employee.MobileNumber;
                //if (employee.DocumentType == 1)
                //{
                //    currentUser_Contact.DocumentType = employee.DocumentType;
                //    currentUser_Contact.DriversLicenseLast4Digits = employee.Last4Digits;
                //}
                //else if (employee.DocumentType == 2)
                //{
                //    currentUser_Contact.DocumentType = employee.DocumentType;
                //    currentUser_Contact.GreenCardLast4Digits = employee.Last4Digits;
                //}
                //else if (employee.DocumentType == 3)
                //{
                //    currentUser_Contact.DocumentType = employee.DocumentType;
                //    currentUser_Contact.SocialSecurityNumberLast4Digits = employee.Last4Digits;
                //}
                //else if (employee.DocumentType == 4)
                //{
                //    currentUser_Contact.DocumentType = employee.DocumentType;
                //    currentUser_Contact.PhoneNumberLast4Digits = employee.Last4Digits;
                //}

                //To avoid the acknowldgement
                //currentUser_Contact.DocumentType = 1;
                //currentUser_Contact.DriversLicenseLast4Digits = "  ";


                entityDB.SystemUsers.Add(currenrtUser_sysUserOrg.SystemUsers);
                entityDB.Contact.Add(currentUser_Contact);
                entityDB.SystemUsersOrganizations.Add(currenrtUser_sysUserOrg);

                currentUser_sysUserOrgRole.SysUserOrgId = currenrtUser_sysUserOrg.SysUserOrganizationId;
                currentUser_sysUserOrgRole.SysRoleId = entityDB.SystemRoles.FirstOrDefault(record => record.RoleName == "Employee Training").RoleId;

                entityDB.SystemUsersOrganizationsRoles.Add(currentUser_sysUserOrgRole);
                #region Assign Trainings
                // Get Client Trainings
                var trainings = entityDB.ClientTemplates.Where(r => r.ClientID == 58 && r.Templates.TemplateType == 1);
                var clienttrainings = trainings.Select(r => new { r.ClientTemplateID, r.ClientID }).ToList();
                var LanguageTrainings = entityDB.ClientTemplates.Any(r => r.ClientID == LocalConstants.Clayco_ClientId && r.Templates.TemplateType == 1 && r.Templates.LanguageId == currenrtUser_sysUserOrg.SystemUsers.LanguageId);
                if (LanguageTrainings)
                    clienttrainings = trainings.Where(r => r.Templates.LanguageId == currenrtUser_sysUserOrg.SystemUsers.LanguageId).Select(r => new { r.ClientTemplateID, r.ClientID }).ToList();
                else
                    clienttrainings = trainings.Where(r => r.Templates.LanguageId == 1).Select(r => new { r.ClientTemplateID, r.ClientID }).ToList();
                foreach (var train in clienttrainings)
                {
                    var assignTrainingToEmployee = new SystemUsersOrganizationsQuizzes();
                    assignTrainingToEmployee.ClientTemplateId = train.ClientTemplateID;
                    assignTrainingToEmployee.VendorId = employee.VendorId;
                    assignTrainingToEmployee.ClientId = train.ClientID;
                    assignTrainingToEmployee.SysUserOrgId = currenrtUser_sysUserOrg.SysUserOrganizationId;
                    entityDB.SystemUsersOrganizationsQuizzes.Add(assignTrainingToEmployee);
                    RecordZeroPayments(assignTrainingToEmployee, entityDB);
                }


                #endregion

                entityDB.SaveChanges();
            }

            return "Registration has been completed successfully";
        }
        private void RecordZeroPayments(SystemUsersOrganizationsQuizzes quiz, EFDbContext entityDB)
        {
            //var quizIds = quizIdsStr.Split(',').Select(long.Parse);
            //var quizes = entityDB.SystemUsersOrganizationsQuizzes.Where(r => quizIds.Contains(r.SysUserOrgQuizId)).ToList();
            //foreach (var quiz in quizes)
            //{
            var quizCheck = entityDB.QuizPayments.FirstOrDefault(rec => rec.SysUserOrgQuizId == quiz.SysUserOrgQuizId && rec.IsOpen != null && rec.IsOpen.Value);
            if (quizCheck != null)
                return;
            //if (!quiz.QuizPayments.Any())
            quiz.QuizPayments = new List<QuizPayments>();

            var userQuiz = new QuizPayments()
            {
                ExpirationDate = quiz.QuizExpirationDate,
                IsOpen = true,
                PaymentReceivedAmount = 0,
                PaymentReceivedDate = DateTime.Now,
                QuizPaymentsItems = new List<QuizPaymentsItems>(),
                SubmittedDate = quiz.QuizDateSubmitted,
                SysUserOrgQuizId = quiz.SysUserOrgQuizId,
                IsZeroFeeTraining = IsZeroFeeTraining(quiz.ClientTemplateId),
                IsFeeCapReached = IsVendorReachedTrainingFeeCap(quiz.VendorId, quiz.ClientId)
            };
            quiz.QuizPayments.Add(userQuiz);
            //foreach (var userQuiz in quiz.QuizPayments)
            //{
            var clientTemplateForBU = entityDB.ClientTemplatesForBU.Where(rec => rec.ClientTemplateId == quiz.ClientTemplateId && rec.ClientTemplatesBUGroupPrice.GroupingFor == 2).Select(row => row).ToList();
            foreach (var businessUnit in clientTemplateForBU)
            {
                var clientTemplatesForBUSites = entityDB.ClientTemplatesForBUSites.Where(rec => rec.ClientTemplateForBUId == businessUnit.ClientTemplateForBUId).Select(row => row).ToList();
                foreach (var businessUniSite in clientTemplatesForBUSites)
                {
                    QuizPaymentsItems addQuizPaymentItem = new QuizPaymentsItems();
                    addQuizPaymentItem.ClientTemplateForBUId = businessUnit.ClientTemplateForBUId;
                    addQuizPaymentItem.ClientTemplateForBUSitesId = businessUniSite.ClientTemplateForBUSitesId;
                    addQuizPaymentItem.AsOfClientTemplateBUGroupId = businessUnit.ClientTemplateBUGroupId;
                    addQuizPaymentItem.QuizPaymentId = userQuiz.QuizPaymentId;
                    entityDB.QuizPaymentsItems.Add(addQuizPaymentItem);

                }
            }
            //}
            //entityDB.SaveChanges();
            //}

        }
        public string deleteEmployee(long sysUserOrgId)
        {
            using (var entity = new EFDbContext())
            {
                try
                {
                    Guid empUserId = entity.SystemUsersOrganizations.FirstOrDefault(rec => rec.SysUserOrganizationId == sysUserOrgId).UserId;
                    List<SystemUsersOrganizationsQuizzes> employeeQuizAssignedRecords = entity.SystemUsersOrganizationsQuizzes.Where(rec => rec.SysUserOrgId == sysUserOrgId).ToList();


                    // Deletion of EmployeeAssignedQuizzes
                    foreach (var assignedquiz in employeeQuizAssignedRecords)
                    {
                        // Kiran on 02/23/215 according to new design
                        List<SystemUsersOrganizationsQuizzesCategories> sysUserOrgQuizCategories = entity.SystemUsersOrganizationsQuizzesCategories.Where(rec => rec.SysUserOrgQuizId == assignedquiz.SysUserOrgQuizId).ToList();
                        foreach (var quizCategory in sysUserOrgQuizCategories)
                        {
                            entity.SystemUsersOrganizationsQuizzesCategories.Remove(quizCategory);
                        }
                        foreach (var userInput in assignedquiz.EmployeeQuizUserInput.ToList())
                        {
                            entity.EmployeeQuizUserInput.Remove(userInput);
                        }
                        foreach (var quizHistory in assignedquiz.SysUserOrgQuizHistory.ToList())
                        {
                            entity.SysUserOrgQuizHistory.Remove(quizHistory);
                        }
                        foreach (var quizCompleted in assignedquiz.QuizzesCompletedSections.ToList())
                        {
                            entity.QuizzesCompletedSections.Remove(quizCompleted);
                        }
                        foreach (var quizPay in assignedquiz.QuizPayments.ToList())
                        {
                            foreach (var quizItem in quizPay.QuizPaymentsItems.ToList())
                            {
                                entity.QuizPaymentsItems.Remove(quizItem);
                            }
                            entity.QuizPayments.Remove(quizPay);
                        }
                        // Ends<<<
                        entity.SystemUsersOrganizationsQuizzes.Remove(assignedquiz);
                    }

                    SystemUsers sysUserRecord = entity.SystemUsers.FirstOrDefault(rec => rec.UserId == empUserId);
                    if (sysUserRecord != null)
                    {
                        entity.Contact.Remove(sysUserRecord.Contacts[0]);
                        entity.SystemUsers.Remove(sysUserRecord);
                    }
                    SystemUsersOrganizations sysUserOrgRecord = entity.SystemUsersOrganizations.FirstOrDefault(rec => rec.UserId == empUserId);
                    List<SystemUsersOrganizationsRoles> userRolesRecords = entity.SystemUsersOrganizationsRoles.Where(rec => rec.SysUserOrgId == sysUserOrgRecord.SysUserOrganizationId).ToList();
                    foreach (var role in userRolesRecords)
                    {
                        entity.SystemUsersOrganizationsRoles.Remove(role);
                    }

                    // Kiran on 02/23/2015 according to new design
                    List<SystemUsersOrganizationsCategories> sysUserOrgCategories = entity.SystemUsersOrganizationsCategories.Where(rec => rec.SysUserOrganizationId == sysUserOrgId).ToList();
                    foreach (var sysuserOrgCategory in sysUserOrgCategories)
                    {
                        entity.SystemUsersOrganizationsCategories.Remove(sysuserOrgCategory);
                    }

                    entity.SystemUsersOrganizations.Remove(sysUserOrgRecord);

                    var userLogRecords = entity.SystemUserLogs.Where(rec => rec.SystemUserId == empUserId);
                    foreach (var log in userLogRecords)
                    {
                        entity.SystemUserLogs.Remove(log);
                    }
                    var userAcknowledgement = entity.UserAcknowledgement.Where(rec => rec.SubmittedBy == empUserId).ToList();
                    foreach (var userAck in userAcknowledgement)
                    {
                        entity.UserAcknowledgement.Remove(userAck);
                    }
                    entity.SaveChanges();

                    return "EmployeeDeleted";
                }
                catch (Exception e)
                {
                    //entityDB = new EFDbContext();
                    //var empUserId = entityDB.SystemUsersOrganizations.FirstOrDefault(rec => rec.SysUserOrganizationId == sysUserOrgId).UserId;
                    //var empUserName = entityDB.Contact.FirstOrDefault(rec => rec.UserId == empUserId).LastName + ", " + entityDB.Contact.FirstOrDefault(rec => rec.UserId == empUserId).FirstName;
                    //// Kiran on 01/06/2015
                    //var empSysteUserRecord = entityDB.SystemUsers.Find(empUserId);
                    //empSysteUserRecord.UserStatus = false;
                    //entityDB.Entry(empSysteUserRecord).State = EntityState.Modified;
                    //entityDB.SaveChanges();
                    ////return empUserName + " started the training process, can't be deleted.";
                    //// Ends<<<
                    return "Unable To Delete. Please contact system Admin";
                }

            }
        }
        public LocalEmployeeQuizzes EmployeeQuizs(Guid userId, int? Type, Guid Loginuserid, List<long> clientIds)
        {
            using (var context = new EFDbContext())
            {
                LocalEmployeeQuizzes LocalEmployeeQuizzes = new LocalEmployeeQuizzes();
                LocalEmployeeQuizzes.ClientQuizzes = new List<EmployeeQuizzesWithCategory>();
                LocalEmployeeQuizzes.LocalEmployeeGuideDocumentsGroupedByClient = new List<LocalEmployeeGuideDocumentsGrouped>();

                var userSysUserOrg = context.SystemUsersOrganizations.FirstOrDefault(record => record.UserId == userId); // Kiran on 02/03/2015
                var currentUserSysUserOrgId = context.SystemUsersOrganizations.FirstOrDefault(record => record.UserId == userId).SysUserOrganizationId;
                var currentUserSysUserOrgQuiz = context.SystemUsersOrganizationsQuizzes.Where(record => record.SysUserOrgId == currentUserSysUserOrgId && (record.QuizRemoved == false || record.QuizRemoved == null)).ToList(); // Kiran on 11/18/2014

                long clientID = -1;
                if (clientIds != null)
                    currentUserSysUserOrgQuiz = currentUserSysUserOrgQuiz.Where(rec => rec.ClientTemplates != null && clientIds.Contains(rec.ClientTemplates.ClientID)).ToList();
                var sysUserOrgQuizGroupByClient = currentUserSysUserOrgQuiz.GroupBy(record => record.ClientTemplates.Organizations.Name);
                //ViewBag.VBsysUserOrgQuizGroupByClient = sysUserOrgQuizGroupByClient.ToList();

                foreach (var client in sysUserOrgQuizGroupByClient)
                {
                    LocalEmployeeGuideDocumentsGrouped employeeGuide = new LocalEmployeeGuideDocumentsGrouped();

                    employeeGuide.key = client.Key;
                    employeeGuide.OrganizationId = client.FirstOrDefault().ClientId;
                    clientID = client.FirstOrDefault().ClientId.Value;

                    var vendorLatestPrequalification = context.Prequalification.Where(rec => rec.ClientId == employeeGuide.OrganizationId && rec.VendorId == userSysUserOrg.OrganizationId).OrderByDescending(rec => rec.PrequalificationStart).ToList();
                    if (!vendorLatestPrequalification.Any())
                    {
                        vendorLatestPrequalification = context.Prequalification.SqlQuery(@"select lp.* from Prequalification lp 
join ClientTemplates ct on ct.ClientTemplateID=lp.ClientTemplateId
join PrequalificationClient pc on pc.PQId=lp.PrequalificationId
where ','+pc.ClientIds+',' like @clientID and VendorId=@vendorid order by PrequalificationStart",
                            new SqlParameter("@clientID", "%," + employeeGuide.OrganizationId + ",%"),
                        new SqlParameter("@vendorid", userSysUserOrg.OrganizationId)
                            ).ToList();
                        if (!vendorLatestPrequalification.Any())
                            continue;
                    }
                    employeeGuide.prequalifcation = vendorLatestPrequalification.FirstOrDefault();
                    List<ClientTemplatesForBU> AnnualBus = new List<ClientTemplatesForBU>();
                    IEnumerable<long?> prequalBusinessUnits = new List<long?>();
                    decimal templateAnnualPrice = 0;
                    if (employeeGuide.prequalifcation != null && employeeGuide.prequalifcation.ClientTemplates != null)
                    {
                        AnnualBus = employeeGuide.prequalifcation.ClientTemplates.ClientTemplatesForBU.Where(rec => rec.ClientTemplateBUGroupId != null && rec.ClientTemplatesBUGroupPrice.GroupingFor == 1 && rec.ClientTemplatesBUGroupPrice.GroupPriceType != -1 && rec.ClientTemplatesBUGroupPrice.GroupPrice != 0).ToList(); // Kiran on 11/25/2015 for FV-232                    
                        prequalBusinessUnits = employeeGuide.prequalifcation.PrequalificationSites.Select(rec => rec.ClientBusinessUnitId).Distinct().ToList();
                    }
                    foreach (var AnnualBu in AnnualBus)
                    {
                        if (prequalBusinessUnits.Contains(AnnualBu.ClientBusinessUnitId))
                        {
                            templateAnnualPrice = templateAnnualPrice + (decimal)AnnualBu.ClientTemplatesBUGroupPrice.GroupPrice;
                        }
                    }
                    employeeGuide.templateAnnualPrice = templateAnnualPrice;
                    if (vendorLatestPrequalification.Any())
                        employeeGuide.PqTrainingAnnualFeeCount = vendorLatestPrequalification.FirstOrDefault().PrequalificationTrainingAnnualFees.Count();
                    if (vendorLatestPrequalification.Any() && vendorLatestPrequalification.FirstOrDefault().PrequalificationStatusId != 14 && vendorLatestPrequalification.FirstOrDefault().PrequalificationFinish > DateTime.Now) // Kiran on 02/05/2015 // Kiran on 02/12/2015
                                                                                                                                                                                                                                   //if ((vendorLatestPrequalification.FirstOrDefault().PrequalificationStatusId == 9) && vendorLatestPrequalification.FirstOrDefault().PrequalificationFinish > DateTime.Now)
                    {
                        employeeGuide.qualified = true;
                    }
                    if (employeeGuide.qualified == null || employeeGuide.qualified == false)
                    {
                        vendorLatestPrequalification = context.Prequalification.Where(rec => rec.Client.OrganizationType == OrganizationType.SuperClient && rec.VendorId == userSysUserOrg.OrganizationId).OrderByDescending(rec => rec.PrequalificationStart).ToList();
                        if (vendorLatestPrequalification.Any())
                        {
                            employeeGuide.prequalifcation = vendorLatestPrequalification.FirstOrDefault();

                            if (vendorLatestPrequalification.FirstOrDefault().PrequalificationStatusId != 14 && vendorLatestPrequalification.FirstOrDefault().PrequalificationFinish > DateTime.Now) // Kiran on 02/05/2015 // Kiran on 02/12/2015
                                                                                                                                                                                                     //if ((vendorLatestPrequalification.FirstOrDefault().PrequalificationStatusId == 9) && vendorLatestPrequalification.FirstOrDefault().PrequalificationFinish > DateTime.Now)
                            {
                                employeeGuide.qualified = true;
                            }
                        }
                    }

                    // Ends<<
                    LocalEmployeeQuizzes.LocalEmployeeGuideDocumentsGroupedByClient.Add(employeeGuide);
                }
                foreach (var quiz in currentUserSysUserOrgQuiz.OrderBy(r => r.ClientTemplates.Templates.TemplateName).ToList())
                {
                    var vendorLatestPrequalification = context.Prequalification.Where(rec => rec.ClientId == quiz.ClientId && rec.VendorId == userSysUserOrg.OrganizationId).OrderByDescending(rec => rec.PrequalificationStart).FirstOrDefault();
                    List<ClientTemplatesForBU> AnnualBus = new List<ClientTemplatesForBU>();
                    List<long?> prequalBusinessUnits = new List<long?>();
                    LatestPrequalification vendorLatestPQ = null;
                    if (vendorLatestPrequalification == null)
                    {
                        vendorLatestPQ = context.LatestPrequalification.SqlQuery(
                            @"select lp.* from Prequalification lp 
join ClientTemplates ct on ct.ClientTemplateID=lp.ClientTemplateId
join PrequalificationClient pc on pc.PQId=lp.PrequalificationId
where ','+pc.ClientIds+',' like @clientID and VendorId=@vendorid order by PrequalificationStart",
                            new SqlParameter("@clientID", "%," + quiz.ClientId + ",%"),
                            new SqlParameter("@vendorid", userSysUserOrg.OrganizationId)
                        ).FirstOrDefault();
                        if (vendorLatestPQ != null)
                        {
                            AnnualBus = vendorLatestPQ.ClientTemplates.ClientTemplatesForBU.Where(rec => rec.ClientTemplateBUGroupId != null && rec.ClientTemplatesBUGroupPrice.GroupingFor == 1 && rec.ClientTemplatesBUGroupPrice.GroupPriceType != -1 && rec.ClientTemplatesBUGroupPrice.GroupPrice != 0).ToList();
                            prequalBusinessUnits = vendorLatestPQ.PrequalificationSites.Select(rec => rec.ClientBusinessUnitId).Distinct().ToList();
                        }
                        if (vendorLatestPQ == null) continue;
                    }
                    if (vendorLatestPrequalification != null && vendorLatestPrequalification.ClientTemplates != null)
                    {
                        AnnualBus = vendorLatestPrequalification.ClientTemplates.ClientTemplatesForBU.Where(rec => rec.ClientTemplateBUGroupId != null && rec.ClientTemplatesBUGroupPrice.GroupingFor == 1 && rec.ClientTemplatesBUGroupPrice.GroupPriceType != -1 && rec.ClientTemplatesBUGroupPrice.GroupPrice != 0).ToList();
                        prequalBusinessUnits = vendorLatestPrequalification.PrequalificationSites.Select(rec => rec.ClientBusinessUnitId).Distinct().ToList();
                    }
                    //var AnnualBus = vendorLatestPrequalification.ClientTemplates.ClientTemplatesForBU.Where(rec => rec.ClientTemplateBUGroupId != null && rec.ClientTemplatesBUGroupPrice.GroupingFor == 1 && rec.ClientTemplatesBUGroupPrice.GroupPriceType != -1 && rec.ClientTemplatesBUGroupPrice.GroupPrice != 0).ToList();
                    //var prequalBusinessUnits = vendorLatestPrequalification.PrequalificationSites.Select(rec => rec.ClientBusinessUnitId).Distinct().ToList();

                    EmployeeQuizzesWithCategory EmpQuiz = new EmployeeQuizzesWithCategory();
                    EmpQuiz.ClientID = quiz.ClientId;
                    EmpQuiz.ClientName = quiz.Client.Name;
                    EmpQuiz.ClientTemplateId = quiz.ClientTemplateId;
                    EmpQuiz.TemplateName = quiz.ClientTemplates.Templates.TemplateName;
                    EmpQuiz.SysUserOrgQuizId = quiz.SysUserOrgQuizId;
                    EmpQuiz.QuizExpirationDate = quiz.QuizExpirationDate;
                    EmpQuiz.QuizDateSubmitted = quiz.QuizDateSubmitted;
                    EmpQuiz.QuizResult = quiz.QuizResult;
                    EmpQuiz.HasQuizPayments = quiz.QuizPayments.Any(rec => rec.IsOpen != null && rec.IsOpen.Value);
                    var Status = QuizLegends.quizfailed.ToString();
                    //if (quiz.QuizResult == true)
                    //{var Status= QuizLegends.quizfailed.ToString();

                    //}

                    if (quiz.QuizResult == true && quiz.QuizExpirationDate != null && quiz.QuizExpirationDate > DateTime.Now) // Kiran on 3/24/2015
                    {
                        Status = QuizLegends.quizpassed.ToString();

                    }

                    else if (quiz.QuizExpirationDate != null && quiz.QuizExpirationDate < DateTime.Now && quiz.QuizResult == true)
                    {
                        Status = QuizLegends.ExpireColor.ToString();
                    }

                    else if (quiz.QuizDateStart == null)
                    {
                        if (quiz.PreviousLastQuizSubmittedDate != null)
                        {
                            var expiration = quiz.PreviousLastQuizSubmittedDate.Value.AddYears(1);
                            if (DateTime.Now < expiration)
                            {
                                Status = QuizLegends.quizrenewal.ToString();

                            }
                            else
                            {
                                Status = QuizLegends.quiznottaken.ToString();

                            }
                        }
                        else
                        {
                            Status = QuizLegends.quiznottaken.ToString();

                        }
                    }

                    else if (quiz.QuizResult == null)
                    {
                        Status = QuizLegends.quizinprocess.ToString();

                    }
                    EmpQuiz.QuizStatus = Status + ".png";
                    EmpQuiz.HasTrainingCertificate = false;
                    var HasTrainingCertificate = context.Organizations.FirstOrDefault(r => r.OrganizationID == quiz.ClientId).TrainingCertificate;
                    if (quiz.QuizResult == true && quiz.QuizExpirationDate > DateTime.Now && HasTrainingCertificate.HasValue && HasTrainingCertificate.Value)
                    {
                        EmpQuiz.HasTrainingCertificate = true;
                    }
                    int periodLength = quiz.ClientTemplates.ClientTemplatesQuizDetails.FirstOrDefault().PeriodLength.Value;
                    EmpQuiz.QuizExpired = quiz.QuizExpirationDate != null ? quiz.QuizExpirationDate.Value.Date.ToShortDateString() : (quiz.QuizDateStart == null && quiz.PreviousLastQuizSubmittedDate != null && quiz.PreviousLastQuizSubmittedDate < DateTime.Now) ? quiz.PreviousLastQuizSubmittedDate.Value.Date.AddYears(periodLength).ToShortDateString() : "";
                    EmpQuiz.QuizDateStart = quiz.QuizDateStart;
                    EmpQuiz.waiverFormConfigured = quiz.ClientTemplates.ClientTemplatesQuizDetails.FirstOrDefault().WaiverForm;
                    EmpQuiz.quizWaiverFormCompletionLog = quiz.QuizWaiverFormCompletionLog.Count();

                    EmpQuiz.QuizzesCategories = new List<QuizzesCategories>();
                    foreach (var QuizCateg in quiz.SystemUsersOrganizationsQuizzesCategories)
                    {
                        QuizzesCategories categ = new QuizzesCategories();
                        categ.CategoryName = QuizCateg.EmployeeQuizCategories.CategoryName;
                        categ.Email = QuizCateg.SystemUsers != null ? QuizCateg.SystemUsers.Email : "";
                        categ.UserName = QuizCateg.SystemUsers != null ? QuizCateg.SystemUsers.Contacts.Count!=0 ? QuizCateg.SystemUsers.Contacts[0].LastName + " " + QuizCateg.SystemUsers.Contacts[0].FirstName : "":"";
                        categ.SubCategoryEnteredDate = QuizCateg.SubCategoryEnteredDate.ToString();
                        categ.SubCategoryInputValue = QuizCateg.SubCategoryInputValue;
                        EmpQuiz.QuizzesCategories.Add(categ);
                    }
                    var clientTemplatesForBU = new List<ClientTemplatesForBU>();
                    if (quiz.ClientTemplates.ClientTemplatesForBU != null)
                        clientTemplatesForBU = quiz.ClientTemplates.ClientTemplatesForBU.Where(rec => rec.ClientTemplateBUGroupId != null && rec.ClientTemplatesBUGroupPrice.GroupPriceType != -1 && rec.ClientTemplatesBUGroupPrice.GroupingFor == 2).ToList();
                    decimal templateTotalPrice = 0;
                    if (quiz.PreviousLastQuizSubmittedDate != null)
                    {
                        EmpQuiz.PreviousLastQuizTakenStatus = (quiz.PreviousLastQuizTakenStatus == true ? "Yes" : "No") + " - " + quiz.PreviousLastQuizSubmittedDate.Value.Date.ToShortDateString();


                    }
                    else
                    {
                        if (quiz.PreviousLastQuizTakenStatus != null)
                        {
                            EmpQuiz.PreviousLastQuizTakenStatus = quiz.PreviousLastQuizTakenStatus == true ? "Yes" : "No";
                        }


                    }

                    foreach (var clientTemplateForBU in clientTemplatesForBU)
                    {
                        var annualFeeBUs = AnnualBus.Select(rec => rec.ClientBusinessUnitId).ToList();
                        if (!annualFeeBUs.Contains(clientTemplateForBU.ClientBusinessUnitId) && prequalBusinessUnits.Contains(clientTemplateForBU.ClientBusinessUnitId)) // Sandhya on 08/20/2015
                        {
                            templateTotalPrice = templateTotalPrice + (decimal)clientTemplateForBU.ClientTemplatesBUGroupPrice.GroupPrice;
                        }
                    }
                    EmpQuiz.templateTotalPrice = templateTotalPrice;
                    LocalEmployeeQuizzes.ClientQuizzes.Add(EmpQuiz);
                }


                var userSysOrgId = context.SystemUsersOrganizations.FirstOrDefault(rec => rec.UserId == Loginuserid).SysUserOrganizationId;
                var employeeTrainingRoleId = context.SystemRoles.FirstOrDefault(rec => rec.RoleName == "Employee Training").RoleId;
                var userRoles = context.SystemUsersOrganizationsRoles.FirstOrDefault(rec => rec.SysUserOrgId == userSysOrgId && rec.SysRoleId == employeeTrainingRoleId);
                LocalEmployeeQuizzes.Employee = false;
                if (userRoles != null)
                {
                    LocalEmployeeQuizzes.Employee = true;
                }

                LocalEmployeeQuizzes.vendorIsExemptFromTrainingFee = false;
                var props = context.ClientVendorProperties.FirstOrDefault(r => r.ClientID == clientID && r.VendorID == userSysUserOrg.OrganizationId);
                if (props != null)
                {
                    if (props.TrainingPaymentExempt != null && props.TrainingPaymentExempt == true)
                    {
                        LocalEmployeeQuizzes.vendorIsExemptFromTrainingFee = true;
                    }

                }

                return LocalEmployeeQuizzes;

            }
        }
        public List<LocalAddCategories> GetTrainingCategories(long quizId, string role)
        {
            using (var entity = new EFDbContext())
            {
                List<EmployeeQuizCategories> trainingCategories = entity.EmployeeQuizCategories.Where(rec => rec.CategoryType == 2).ToList();

                if (role.Equals(LocalConstants.Vendor))
                {
                    trainingCategories = trainingCategories.Where(rec => rec.AccessType == 2).ToList();
                }

                List<LocalAddCategories> trainingCategoriesList = new List<LocalAddCategories>();
                foreach (var category in trainingCategories)
                {
                    LocalAddCategories trainingCategory = new LocalAddCategories();
                    trainingCategory.EmpQuizCategoryId = category.EmpQuizCategoryId;
                    trainingCategory.CategoryName = category.CategoryName;
                    trainingCategory.SubCategoryControlType = category.SubCategoryControlType;
                    trainingCategory.SubCategoriesValue = category.SubCategoriesValue;
                    trainingCategory.SysUserOrgQuizId = quizId;

                    var sysUserOrgQuizCategory = entity.SystemUsersOrganizationsQuizzesCategories.FirstOrDefault(rec => rec.EmpQuizCategoryId == category.EmpQuizCategoryId && rec.SysUserOrgQuizId == quizId);
                    if (sysUserOrgQuizCategory != null)
                    {
                        trainingCategory.SubCategorySelectedValue = sysUserOrgQuizCategory.SubCategoryInputValue;
                        trainingCategory.SubCategoryEnteredDate = sysUserOrgQuizCategory.SubCategoryEnteredDate;
                    }

                    trainingCategoriesList.Add(trainingCategory);
                }
                return trainingCategoriesList;
            }
        }

        public void PostEmployeeQuizCategories(List<LocalAddCategories> empQuizCategories, Guid userId)
        {
            using (var entity = new EFDbContext())
            {
                foreach (var category in empQuizCategories)
                {
                    var sysUserOrgQuizCategory = entity.SystemUsersOrganizationsQuizzesCategories.FirstOrDefault(rec => rec.EmpQuizCategoryId == category.EmpQuizCategoryId && rec.SysUserOrgQuizId == category.SysUserOrgQuizId);
                    if (sysUserOrgQuizCategory == null)
                    {
                        if (!category.SubCategorySelectedValue.Equals("N/A"))
                        {
                            sysUserOrgQuizCategory = new SystemUsersOrganizationsQuizzesCategories();
                            sysUserOrgQuizCategory.SubCategoryInputValue = category.SubCategorySelectedValue;
                            sysUserOrgQuizCategory.SysUserOrgQuizId = category.SysUserOrgQuizId;
                            sysUserOrgQuizCategory.SubCategoryEnteredDate = category.SubCategoryEnteredDate;
                            sysUserOrgQuizCategory.EmpQuizCategoryId = category.EmpQuizCategoryId;
                            sysUserOrgQuizCategory.CategoryAssignedBy = new Guid(userId.ToString());
                            entity.SystemUsersOrganizationsQuizzesCategories.Add(sysUserOrgQuizCategory);
                        }
                    }
                    else
                    {
                        if (!category.SubCategorySelectedValue.Equals("N/A"))
                        {
                            sysUserOrgQuizCategory.SubCategoryInputValue = category.SubCategorySelectedValue;
                            sysUserOrgQuizCategory.SubCategoryEnteredDate = category.SubCategoryEnteredDate;
                            sysUserOrgQuizCategory.CategoryAssignedBy = new Guid(userId.ToString());
                            entity.Entry(sysUserOrgQuizCategory).State = EntityState.Modified;
                        }
                        else
                        {
                            entity.Entry(sysUserOrgQuizCategory).State = EntityState.Deleted;
                        }
                    }

                    if (!category.SubCategorySelectedValue.Equals("N/A") && category.EmpQuizCategoryId == 2)
                    {
                        var sysUserOrgQuizRec = entity.SystemUsersOrganizationsQuizzes.Find(category.SysUserOrgQuizId);
                        if (category.SubCategorySelectedValue.Equals("Pass"))
                        {
                            sysUserOrgQuizRec.QuizResult = true;
                            DateTime submittedDate = (DateTime)category.SubCategoryEnteredDate;
                            sysUserOrgQuizRec.QuizDateStart = submittedDate;
                            sysUserOrgQuizRec.QuizDateSubmitted = submittedDate;
                            sysUserOrgQuizRec.QuizExpirationDate = submittedDate.AddYears(1);
                        }
                        else
                        {
                            sysUserOrgQuizRec.QuizResult = false;
                            DateTime submittedDate = (DateTime)category.SubCategoryEnteredDate;
                            sysUserOrgQuizRec.QuizDateStart = submittedDate;
                            sysUserOrgQuizRec.QuizDateSubmitted = submittedDate;
                        }

                        entity.Entry(sysUserOrgQuizRec).State = EntityState.Modified;
                    }

                    entity.SaveChanges();

                }
            }
        }

        public List<LocalAddCategories> GetEmployeeCategories(long sysUserOrgId, string role)
        {
            using (var entity = new EFDbContext())
            {
                var empCategories = entity.EmployeeQuizCategories.Where(rec => rec.CategoryType == 1).ToList();
                if (role.Equals(LocalConstants.Vendor))
                {
                    empCategories = empCategories.Where(rec => rec.AccessType == 2).ToList();
                }
                List<LocalAddCategories> empCategoriesList = new List<LocalAddCategories>();
                foreach (var category in empCategories)
                {
                    LocalAddCategories empCategory = new LocalAddCategories();
                    empCategory.CategoryName = category.CategoryName;
                    empCategory.SubCategoryControlType = category.SubCategoryControlType;
                    empCategory.SubCategoriesValue = category.SubCategoriesValue;
                    empCategory.SysUserOrgId = sysUserOrgId;
                    empCategory.EmpQuizCategoryId = category.EmpQuizCategoryId;
                    var category_Loc = entity.SystemUsersOrganizationsCategories.FirstOrDefault(rec => rec.SysUserOrganizationId == empCategory.SysUserOrgId && rec.EmpQuizCategoryId == empCategory.EmpQuizCategoryId);
                    if (category_Loc != null)
                    {
                        empCategory.SubCategorySelectedValue = category_Loc.SubCategoryInputValue;
                        empCategory.SubCategoryEnteredDate = category_Loc.SubCategoryEnteredDate;
                    }
                    empCategoriesList.Add(empCategory);
                }
                return empCategoriesList;
            }
        }

        public void PostEmployeeCategories(List<LocalAddCategories> empCategories, Guid userId)
        {
            using (var entity = new EFDbContext())
            {
                foreach (var Category in empCategories.ToList())
                {
                    var category_Loc = entity.SystemUsersOrganizationsCategories.FirstOrDefault(rec => rec.SysUserOrganizationId == Category.SysUserOrgId && rec.EmpQuizCategoryId == Category.EmpQuizCategoryId);
                    if (category_Loc == null)
                    {
                        if (!Category.SubCategorySelectedValue.Equals("N/A"))
                        {
                            category_Loc = new SystemUsersOrganizationsCategories();
                            category_Loc.EmpQuizCategoryId = Category.EmpQuizCategoryId;
                            category_Loc.SubCategoryEnteredDate = Category.SubCategoryEnteredDate;
                            category_Loc.SubCategoryInputValue = Category.SubCategorySelectedValue;
                            category_Loc.SysUserOrganizationId = Category.SysUserOrgId;
                            category_Loc.CategoryAssignedBy = new Guid(userId.ToString());
                            entity.SystemUsersOrganizationsCategories.Add(category_Loc);
                        }
                    }
                    else
                    {
                        if (!Category.SubCategorySelectedValue.Equals("N/A"))
                        {
                            category_Loc.SubCategoryInputValue = Category.SubCategorySelectedValue;
                            category_Loc.SubCategoryEnteredDate = Category.SubCategoryEnteredDate;
                            category_Loc.CategoryAssignedBy = new Guid(userId.ToString());
                            entity.Entry(category_Loc).State = EntityState.Modified;
                        }
                        else
                        {
                            entity.Entry(category_Loc).State = EntityState.Deleted;
                        }
                    }
                    entity.SaveChanges();
                }
            }
        }

        public string SaveSignProCheckinCheckOut(SineProCheckinCheckout info)
        {
            using (var entity = new EFDbContext())
            {
                var rec = entity.SignProCheckinCheckout.FirstOrDefault(r => r.RecId == info.RecId);
                if (rec == null)
                    entity.SignProCheckinCheckout.Add(info);
                else
                {
                    rec.CheckOutUser = info.CheckOutUser;
                    rec.CheckoutTime = info.CheckoutTime;
                    entity.Entry(rec).State = EntityState.Modified;
                }
                entity.SaveChanges();
            }

            return "Success";
        }
        public List<OrganizationUsers> GetEmployees(long pqId, List<long> clientIds)
        {
            using (var entity = new EFDbContext())
            {
                Prequalification pq = entity.Prequalification.Find(pqId);

                List<SystemUsersOrganizations> vendorsusers = entity.SystemUsersOrganizationsQuizzes.Where(rec => rec.SystemUsersOrganizations.OrganizationId == pq.VendorId).Select(rec => rec.SystemUsersOrganizations).Distinct().OrderBy(rec => rec.SystemUsers.Contacts.FirstOrDefault().LastName).ToList();

                List<OrganizationUsers> vendorEmployees = new List<OrganizationUsers>();

                foreach (var user in vendorsusers)
                {
                    OrganizationUsers employee = new OrganizationUsers();
                    var empStatus = "";
                    var clientId = new List<long>() { pq.ClientId };

                    if (clientIds != null && pq.Client.OrganizationType == FVGen3.WebUI.Constants.OrganizationType.SuperClient)
                    {
                        clientId = clientIds;
                    }
                    List<SystemUsersOrganizationsQuizzes> empSystemUserOrgQuizzes = user.SystemUsersOrganizationsQuizzes.Where(rec => rec.QuizRemoved == null || rec.QuizRemoved == false).ToList();
                    var empQuizzesPassedCount = 0;
                    var empQuizzesFailedCount = 0;
                    var empQuizzesNotAttended = 0;
                    var empQuizzesInProcess = 0;
                    var isPassFailAttended = false;
                    var isPass = false;
                    var Inprocessquiz = false;
                    var empexpired = 0;

                    foreach (var quiz in empSystemUserOrgQuizzes.Where(r => r.ClientTemplates != null && clientId.Contains(r.ClientTemplates.ClientID)))
                    {
                        var empQuiz = quiz;
                        if (@empQuiz.QuizExpirationDate != null && @empQuiz.QuizExpirationDate < DateTime.Now && @empQuiz.QuizResult == true)
                        {
                            empexpired++;
                        }
                        else if (empQuiz.QuizDateStart == null)
                        {
                            empQuizzesNotAttended++;
                        }

                        else if (empQuiz.QuizResult == true && @empQuiz.QuizExpirationDate != null && @empQuiz.QuizExpirationDate > DateTime.Now) // Kiran on 3/30/2015
                        {
                            empQuizzesPassedCount++;
                        }
                        else if (empQuiz.QuizResult == false)
                        {
                            empQuizzesFailedCount++;
                        }
                        else if (empQuiz.QuizResult == null)
                        {
                            Inprocessquiz = true;
                            empQuizzesInProcess++;
                        }

                    }

                    if (empQuizzesFailedCount > 0 || empQuizzesNotAttended > 0)
                    {
                        isPassFailAttended = true;
                    }
                    if (empQuizzesPassedCount > 0)
                    {
                        isPass = true;
                    }

                    var numberofcombination = 0;
                    if (empQuizzesNotAttended > 0) { numberofcombination++; }
                    if (empQuizzesFailedCount > 0) { numberofcombination++; }
                    if (empQuizzesPassedCount > 0) { numberofcombination++; }
                    if (empexpired > 0) { numberofcombination++; }
                    if (empQuizzesInProcess > 0) { numberofcombination++; }

                    if (numberofcombination > 1)
                    {
                        empStatus = QuizLegends.mixed.ToString();
                    }
                    else if (empexpired > 0)
                    {
                        empStatus = QuizLegends.ExpireColor.ToString();
                    }
                    else if (empQuizzesNotAttended > 0 && empQuizzesFailedCount == 0 && empQuizzesPassedCount == 0)
                    {
                        empStatus = QuizLegends.quiznottaken.ToString();
                    }
                    else if (isPass == true && isPassFailAttended == false)
                    {
                        empStatus = QuizLegends.quizpassed.ToString();
                    }
                    else if (isPassFailAttended == true && isPass == true)
                    {
                        empStatus = QuizLegends.quiznottaken.ToString();
                    }
                    else if (isPassFailAttended == true && isPass == false)
                    {
                        empStatus = QuizLegends.quizfailed.ToString();
                    }
                    else if (Inprocessquiz == true)
                    {
                        empStatus = QuizLegends.quizinprocess.ToString();
                    }

                    SystemUsers systemusers = user.SystemUsers;
                    Contact userContact = user.SystemUsers.Contacts.FirstOrDefault();
                    employee.UserId = user.UserId;
                    employee.UserStatus = systemusers.UserStatus;
                    employee.SysUserOrganizationId = user.SysUserOrganizationId;
                    if (!string.IsNullOrEmpty(empStatus))
                    {
                        employee.EmployeeStatus = empStatus + ".png";
                    }
                    if (userContact != null)
                    {
                        employee.EmployeeFullName = userContact.FullName;
                        employee.EmployeeTitle = userContact.ContactTitle;
                        if (userContact.DocumentType == 1)
                        {
                            employee.DocumentType = "DL";
                        }
                        else if (userContact.DocumentType == 2)
                        {
                            employee.DocumentType = "GC";
                        }
                        else if (userContact.DocumentType == 3)
                        {
                            employee.DocumentType = "SSN";
                        }
                        else if (userContact.DocumentType == 4)
                        {
                            employee.DocumentType = "Phone#";
                        }

                        if (!string.IsNullOrEmpty(userContact.DriversLicenseLast4Digits))
                        {
                            employee.DocumentTypeValue = userContact.DriversLicenseLast4Digits;
                        }
                        else if (!string.IsNullOrEmpty(userContact.GreenCardLast4Digits))
                        {
                            employee.DocumentTypeValue = userContact.GreenCardLast4Digits;
                        }
                        else if (!string.IsNullOrEmpty(userContact.SocialSecurityNumberLast4Digits))
                        {
                            employee.DocumentTypeValue = userContact.SocialSecurityNumberLast4Digits;
                        }
                        else if (!string.IsNullOrEmpty(userContact.PhoneNumberLast4Digits))
                        {
                            employee.DocumentTypeValue = userContact.PhoneNumberLast4Digits;
                        }
                    }

                    employee.employeesCategories = new List<EmployeesCategories>();
                    foreach (var empCategory in user.SystemUsersOrganizationsCategories)
                    {
                        EmployeesCategories employeeCategory = new EmployeesCategories();
                        employeeCategory.CategoryName = empCategory.EmployeeQuizCategories.CategoryName;
                        employeeCategory.SubCategoryInputValue = empCategory.SubCategoryInputValue;
                        employeeCategory.SubCategoryEnteredDate = empCategory.SubCategoryEnteredDate != null ? empCategory.SubCategoryEnteredDate.Value.Date.ToShortDateString() : "";
                        employeeCategory.UserName = userContact != null ? userContact.FullName : systemusers.Email;

                        employee.employeesCategories.Add(employeeCategory);
                    }
                    vendorEmployees.Add(employee);
                }
                return vendorEmployees;
            }
        }

        public bool IsZeroFeeTraining(Guid? clientTemplateId)
        {
            using (var entity = new EFDbContext())
            {
                bool isZeroFeeTraining = false;
                ClientTemplates trainingTemplate = entity.ClientTemplates.FirstOrDefault(r => r.ClientTemplateID == clientTemplateId);
                if (trainingTemplate != null)
                {
                    isZeroFeeTraining = trainingTemplate.ClientTemplatesBUGroupPrice.Any(rec =>
                        rec.GroupingFor == 2 && rec.GroupPriceType == 0 && rec.GroupPrice == 0);
                }

                return isZeroFeeTraining;
            }
        }

       public bool IsVendorReachedTrainingFeeCap(long? vendorId, long? clientId)
       {
           using (var entity = new EFDbContext())
           {
               var trainingFeeCap = entity.Organizations.FirstOrDefault(rec => rec.OrganizationID == clientId)?.TrainingFeeCap;
               
               decimal? trainingFeePaidSoFar = 0.0M;
               DateTime currentDate = DateTime.Now;
               
               var activePq = entity.Prequalification.OrderByDescending(rec => rec.ClientId == clientId && rec.VendorId == vendorId && currentDate >= rec.PrequalificationStart && currentDate < rec.PrequalificationFinish).FirstOrDefault();

               if (trainingFeeCap.HasValue && trainingFeeCap.Value > 0)
               {
                   var quizPayments = from quiz in entity.QuizPayments
                       join suoq in entity.SystemUsersOrganizationsQuizzes on quiz.SysUserOrgQuizId equals suoq.SysUserOrgQuizId
                       join p in entity.Prequalification on suoq.ClientTemplates.ClientID equals p.ClientTemplates.ClientID
                       where p.PrequalificationId == activePq.PrequalificationId && suoq.VendorId == p.VendorId
                             && p.PrequalificationStart <= quiz.PaymentReceivedDate && p.PrequalificationFinish >= quiz.PaymentReceivedDate
                       select quiz;
                   trainingFeePaidSoFar += quizPayments.Select(r => r.PaymentReceivedAmount).Sum();
               }

               if (trainingFeeCap.HasValue && trainingFeePaidSoFar > trainingFeeCap.Value)
               {
                   return true;
               }
               else
               {
                   return false;
               }
            }
        }
    }
}
