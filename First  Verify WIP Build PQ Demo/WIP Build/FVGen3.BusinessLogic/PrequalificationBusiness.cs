﻿/*
Page Created Date:  04/10/2017
Created By: Rajesh Pendela
Purpose: All Business operations related to prequalification module will be performed here
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using FVGen3.BusinessLogic.Interfaces;
using FVGen3.DataLayer.DTO;
using FVGen3.Domain.Concrete;
using FVGen3.Domain.Entities;

using System.Web;

using FVGen3.Domain.LocalModels;
using AutoMapper;
using System.Data.SqlClient;
using System.IO;
using System.Configuration;
using AhaApps.Libraries.Extensions;
using AhaApps.Libraries.IO;
using System.Globalization;
using System.Threading;
using FVGen3.Domain.ViewModels;
using Resources;
using System.Data;
using FVGen3.WebUI.Constants;
using System.Threading.Tasks;

namespace FVGen3.BusinessLogic
{
    public class PrequalificationBusiness : BaseBusinessClass, IPrequalificationBusiness
    {
        IMapper mapper;
        IOrganizationBusiness _orgBusiness;
        IClientBusiness _clientBusiness;
        private IUtilityBusiness logs = new UtilityBusiness();

        public PrequalificationBusiness(IOrganizationBusiness _orgBusiness, IClientBusiness _clientBusiness)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Organizations, OrganizationData>();
                cfg.CreateMap<FinancialAnalyticsModel, FinancialAnalyticsLog>(); cfg.CreateMap<FinancialAnalyticsLog, FinancialAnalyticsModel>(); cfg.CreateMap<List<FinancialAnalyticsModel>, List<FinancialAnalyticsLog>>();
                cfg.CreateMap<OrganizationData, Organizations>();
            });
            mapper = config.CreateMapper();
            this._orgBusiness = _orgBusiness;
            this._clientBusiness = _clientBusiness;
        }
        /// <summary>
        /// To get the different prequalification statuses available in the system
        /// </summary>
        /// <returns></returns>
        public List<KeyValuePair<long, string>> GetPQStatuses()
        {
            using (var entity = new EFDbContext())
            {
                return entity.PrequalificationStatus.Where(rec => rec.ShowInDashBoard.Value).OrderBy(r => r.PrequalificationStatusName).AsEnumerable()
                    .Select(rec => new KeyValuePair<long, string>(rec.PrequalificationStatusId, rec.PrequalificationStatusName)).ToList();
            }
        }
        /// <summary>
        /// To get the prequalification status of specifc prequalification so as to display it in the banner
        /// </summary>
        /// <param name="pqId"></param>
        /// <param name="defaultStatus"></param>
        /// <param name="clientId"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public KeyValuePair<long, string> GetPQStatusForBanner(long pqId, string defaultStatus, long clientId, Guid UserId)
        {

            //var clientId = 0L;
            //if (SSession.Role.Equals(Role.Client))
            //    clientId = SSession.OrganizationId;
            using (var entity = new EFDbContext())
            {
                var clientLocations = _clientBusiness.GetPQClientLocations(pqId, clientId) as IEnumerable<PQSites>;

                //Sites accessble to the user
                var userSites = entity.SystemUsersBUSites.FirstOrDefault(rec => rec.SystemUserId == UserId);


                if (userSites != null && userSites.BUSiteIDsList.Count() > 0)
                {
                    var SiteIds = userSites.BUSiteIDsList;
                    clientLocations = clientLocations.Where(r => SiteIds.Contains((long)r.BUSiteId));
                }

                var siteStatus = clientLocations.Select(row => new { row.PQSiteStatusId, row.PQSiteStatusName }).Distinct().ToList();

                if (siteStatus.Count == 0)
                    return new KeyValuePair<long, string>(entity.Prequalification.Find(pqId).PrequalificationStatusId, defaultStatus);

                if (siteStatus.Count == 1)

                    return new KeyValuePair<long, string>(siteStatus.FirstOrDefault().PQSiteStatusId.Value, siteStatus.FirstOrDefault().PQSiteStatusName);
                return new KeyValuePair<long, string>(-1, "Multiple");
            }

            //var clientLocations = _clientBusiness.GetPQClientLocations(pqId, clientId);

            //var siteStatus = clientLocations.Select(row => new { row.PQSiteStatusId, row.PQSiteStatusName }).Distinct().ToList();
            //if (siteStatus.Count == 0)
            //    return defaultStatus;
            //else if (siteStatus.Count == 1)
            //    return siteStatus.FirstOrDefault().PQSiteStatusName;
            //else
            //    return "Multiple";

        }
        /// <summary>
        /// To get the prequalification location section
        /// </summary>
        /// <param name="PQId"></param>
        /// <returns></returns>
        public long GetLocationSectionId(long PQId)
        {
            using (var context = new EFDbContext())
            {
                try { return context.Prequalification.Find(PQId).ClientTemplates.Templates.TemplateSections.FirstOrDefault(r => r.TemplateSectionType == 7).TemplateSectionID; } catch { }
            }
            return -1;
        }
        /// <summary>
        /// To get the PQ status for Indication text
        /// </summary>
        /// <param name="pqId"></param>
        /// <param name="defaultStatus"></param>
        /// <param name="clientId"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public KeyValuePair<long, string> GetPQStatusVendorIndicationText(long pqId, string defaultStatus, long clientId, Guid UserId)
        {

            //var clientId = 0L;
            //if (SSession.Role.Equals(Role.Client))
            //    clientId = SSession.OrganizationId;
            using (var entity = new EFDbContext())
            {
                var clientLocations = _clientBusiness.GetPQClientLocations(pqId, clientId) as IEnumerable<PQSites>;

                //Sites accessble to the user
                var userSites = entity.SystemUsersBUSites.FirstOrDefault(rec => rec.SystemUserId == UserId);


                if (userSites != null && userSites.BUSiteIDsList.Count() > 0)
                {
                    var SiteIds = userSites.BUSiteIDsList;
                    clientLocations = clientLocations.Where(r => SiteIds.Contains((long)r.BUSiteId));
                }

                var siteStatus = clientLocations.Select(row => new { row.PQSiteStatusId, row.VendorIndicationText }).Distinct().ToList();

                if (siteStatus.Count == 0)
                    return new KeyValuePair<long, string>(entity.Prequalification.Find(pqId).PrequalificationStatusId, defaultStatus);

                else if (siteStatus.Count == 1)

                    return new KeyValuePair<long, string>(siteStatus.FirstOrDefault().PQSiteStatusId.Value, siteStatus.FirstOrDefault().VendorIndicationText);
                else
                    return new KeyValuePair<long, string>(-1, "Multiple");
            }

            //var clientLocations = _clientBusiness.GetPQClientLocations(pqId, clientId);

            //var siteStatus = clientLocations.Select(row => new { row.PQSiteStatusId, row.PQSiteStatusName }).Distinct().ToList();
            //if (siteStatus.Count == 0)
            //    return defaultStatus;
            //else if (siteStatus.Count == 1)
            //    return siteStatus.FirstOrDefault().PQSiteStatusName;
            //else
            //    return "Multiple";

        }
        /// <summary>
        /// To get the client organizations of specific prequalification
        /// </summary>
        /// <param name="prequalificationId"></param>
        /// <returns></returns>
        public List<OrganizationData> GetTemplateOrganizations(long prequalificationId)
        {
            using (var context = new EFDbContext())
            {
                var orgs = context.Prequalification.Find(prequalificationId).ClientTemplates.Templates.ClientTemplates.Select(r => r.Organizations).ToList();
                return mapper.Map<List<OrganizationData>>(orgs);
            }
        }
        /// <summary>
        /// To get the clients selected in specific prequalification
        /// </summary>
        /// <param name="pqId"></param>
        /// <returns></returns>
        public PrequalificationClient GetPrequalificationClient(long pqId)
        {
            using (var context = new EFDbContext())
            {
                var pqClients = context.PrequalificationClient.FirstOrDefault(r => r.PQId == pqId);
                return pqClients;
            }
        }
        /// <summary>
        /// To get the contact information(general information) of specifc prequalification
        /// </summary>
        /// <param name="pqId"></param>
        /// <returns></returns>
        public List<UserInfo> GetPQVendorUser(long pqId)
        {
            using (var entity = new EFDbContext())
            {
                return entity.VendorDetails.Where(rec => rec.PrequalificationId == pqId && rec.EmailAddress != null)
                    .Select(rec => new UserInfo() { Email = rec.EmailAddress, FirstName = rec.FullName, LastName = "" })
                    .ToList();
            }
        }
        /// <summary>
        /// To get the contact information(general information) for the list of prequalifications
        /// </summary>
        /// <param name="PQId"></param>
        /// <returns></returns>
        public IEnumerable<UserInfo> GetPQVendorUser(List<long> PQId)
        {
            using (var entity = new EFDbContext())
            {
                return entity.VendorDetails.Where(rec => PQId.Contains(rec.PrequalificationId) &&
                                                         rec.EmailAddress != null)
                    .Select(rec => new UserInfo() { Email = rec.EmailAddress, FirstName = rec.FullName, LastName = "" })
                    .ToList();
            }
        }
        /// <summary>
        /// To get the specifc PQ template
        /// </summary>
        /// <param name="PqId"></param>
        /// <returns></returns>
        public Templates GetPQTemplate(long PqId)
        {
            using (var context = new EFDbContext())
            {
                return context.Prequalification.Find(PqId).ClientTemplates.Templates;
            }
        }
        /// <summary>
        /// To get the specific PQ template name
        /// </summary>
        /// <param name="PQID"></param>
        /// <returns></returns>
        public List<KeyValuePair<string, string>> GetPQTemplates(long PQID)
        {
            using (var context = new EFDbContext())
            {
                var clientId = context.Prequalification.Find(PQID).ClientId;
                return
                    context.ClientTemplates.Where(r => r.ClientID == clientId).ToList()
                        .Select(
                            r =>
                                new KeyValuePair<string, string>(r.ClientTemplateID.ToString(), r.Templates.TemplateName)).ToList();
            }
        }
        /// <summary>
        /// To get the latest prequalification for the specific vendor and client
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="vendorId"></param>
        /// <returns></returns>
        public LatestPrequalification GetLatestPQ(long clientId, long vendorId)
        {
            using (var entity = new EFDbContext())
            {
                return
                    entity.LatestPrequalification.FirstOrDefault(r => r.ClientId == clientId && r.VendorId == vendorId);
            }
        }
        /// <summary>
        /// To get the client of specific PQ
        /// </summary>
        /// <param name="vendorId"></param>
        /// <returns></returns>
        public List<long> GetVendorClients(long vendorId)
        {
            using (var entity = new EFDbContext())
            {
                return
                    entity.LatestPrequalification.Where(r => r.VendorId == vendorId)
                        .OrderBy(r => r.Client.Name)
                        .Select(r => r.ClientId)
                        .ToList();
            }
        }
        /// <summary>
        /// To get the specif PQ
        /// </summary>
        /// <param name="pqId"></param>
        /// <returns></returns>
        public Prequalification GetPrequalification(long pqId)
        {
            using (var entity = new EFDbContext())
            {
                return
                    entity.Prequalification.Find(pqId);
            }
        }

        /// <summary>
        /// To display toggle the specifc subsection to vendor
        /// </summary>
        /// <param name="pqSubSectionId"></param>
        /// <returns></returns>
        public bool ToggleAddendumVendorDisply(long pqSubSectionId)
        {
            using (var context = new EFDbContext())
            {
                var pqSubSec = context.PrequalificationSubsection.Find(pqSubSectionId);
                if (pqSubSec == null) return false;
                pqSubSec.VisibleToVendor = !pqSubSec.VisibleToVendor;
                context.Entry(pqSubSec).State = EntityState.Modified;
                context.SaveChanges();
                return true;
            }
        }
        /// <summary>
        /// To create/update the specific subsection
        /// </summary>
        /// <param name="addendum"></param>
        /// <returns></returns>
        public AddendumModel AddAddendum(AddendumModel addendum)
        {
            using (var context = new EFDbContext())
            {
                var q = context.PrequalificationSubsection;
                var latestGroup = q.Any() ? q.Max(r => r.GroupNumber) : 0;
                latestGroup += 1;
                var order = 1;
                var subsection = addendum.PQSubSectionId == -1 ? new PrequalificationSubsection() : context.PrequalificationSubsection.Find(addendum.PQSubSectionId);
                if (subsection != null)
                {
                    subsection.PQId = addendum.PQId;
                    //subsection.SubSectionFooter = addendum.Footer;
                    subsection.SubSectionHeader = addendum.Header;
                    subsection.SubSectionName = addendum.SectionName;
                    subsection.SubSectionId = addendum.SubSectionId;


                    if (addendum.PQSubSectionId == -1)
                    {
                        subsection.Order = order++;
                        subsection.CreatedBy = addendum.UserId;
                        subsection.CreatedDate = DateTime.Now;
                        subsection.GroupNumber = latestGroup;
                        subsection.SubSectionType = "Addendum";
                        context.PrequalificationSubsection.Add(subsection);
                        var vendorSign = CreateSign(addendum, latestGroup, "Vendor Signature", order++);
                        vendorSign.SubSectionType = "VendorSign";
                        context.PrequalificationSubsection.Add(vendorSign);
                        var clientSign = CreateSign(addendum, latestGroup, "Client Signature", order);
                        clientSign.SubSectionType = "ClientSign";
                        context.PrequalificationSubsection.Add(clientSign);
                    }
                    else
                    {
                        context.Entry(subsection).State = EntityState.Modified;
                    }
                    addendum.PQSubSectionId = subsection.Id;
                }
                context.SaveChanges();
            }

            return addendum;
        }
        /// <summary>
        /// To create the signature subsection
        /// </summary>
        /// <param name="addendum"></param>
        /// <param name="latestGroup"></param>
        /// <param name="sectionName"></param>
        /// <param name="order"></param>
        /// <returns></returns>
        private PrequalificationSubsection CreateSign(AddendumModel addendum, long latestGroup, string sectionName, int order)
        {
            var sign = new PrequalificationSubsection()
            {
                CreatedDate = DateTime.Now,
                CreatedBy = addendum.UserId,
                GroupNumber = latestGroup,
                PQId = addendum.PQId,
                SubSectionName = sectionName,
                Order = order,
                SubSectionHeader =
                    "In accordance with the Electronic Signatures in Global and National Commerce Act (ESIGN Act, 15 U.S.C.§§ 7001-7031), entering your full name below is a signature with full legal force and effect",
                SubSectionId = addendum.SubSectionId,
                PrequalificationQuestion = new List<PrequalificationQuestion>()
                {
                    new PrequalificationQuestion()
                    {
                        PQId = addendum.PQId,
                        QuestionType = 2,
                        QuestionText = "Signed:"
                    },
                    new PrequalificationQuestion()
                    {
                        PQId = addendum.PQId,
                        QuestionType = 2,
                        QuestionText = "Title:"
                    },
                    new PrequalificationQuestion()
                    {
                        PQId = addendum.PQId,
                        QuestionType = 11,
                        QuestionText = "Date:"
                    },
                }
            };
            return sign;
        }
        /// <summary>
        /// To get the specific subsection
        /// </summary>
        /// <param name="pqSubSectionId"></param>
        /// <returns></returns>
        public AddendumModel GetAddendum(long pqSubSectionId)
        {
            using (var context = new EFDbContext())
            {
                var pqSubSec = context.PrequalificationSubsection.Find(pqSubSectionId);
                if (pqSubSec != null)
                {
                    var result = new AddendumModel()
                    {
                        Footer = pqSubSec.SubSectionFooter,
                        Header = pqSubSec.SubSectionHeader,
                        PQId = pqSubSec.PQId,
                        PQSubSectionId = pqSubSec.Id,
                        SectionName = pqSubSec.SubSectionName,
                        SubSectionId = pqSubSec.SubSectionId
                    };
                    return result;
                }
            }
            return null;
        }
        /// <summary>
        /// To get the list of questions related to specifc PQ subsection
        /// </summary>
        /// <param name="pqId"></param>
        /// <param name="subSecId"></param>
        /// <returns></returns>
        public List<AddendumDTO> GetAddendums(long pqId, long subSecId)
        {
            var result = new List<AddendumDTO>();
            using (var context = new EFDbContext())
            {
                var pqSubSecs =
                    context.PrequalificationSubsection.Where(r => r.PQId == pqId && r.SubSectionId == subSecId).OrderBy(r => r.GroupNumber).ThenBy(r => r.Order);
                foreach (var pqSubSec in pqSubSecs.ToList())
                {
                    var addendum = new AddendumDTO
                    {
                        PQSubSectionId = pqSubSec.Id,
                        Footer = pqSubSec.SubSectionFooter,
                        Header = pqSubSec.SubSectionHeader,
                        PqId = pqSubSec.PQId,
                        SubSecId = subSecId,
                        SubSectionName = pqSubSec.SubSectionName,
                        VisibleToVendor = pqSubSec.VisibleToVendor,
                        SubSectionType = pqSubSec.SubSectionType
                    };
                    foreach (var questions in pqSubSec.PrequalificationQuestion.ToList())
                    {
                        addendum.Questions.Add(new AddendumQuestions()
                        {
                            PQQuestionId = questions.Id,
                            QuestionType = questions.QuestionType,
                            InputVal = questions.QuestionInputValue,
                            QuestionText = questions.QuestionText,

                        });
                    }
                    result.Add(addendum);
                }

            }
            return result;
        }
        /// <summary>
        /// To change the PQ template
        /// </summary>
        /// <param name="PQID"></param>
        /// <param name="clientTemplateId"></param>
        /// <returns></returns>
        public string ChangePQTemplate(long PQID, Guid clientTemplateId)
        {
            using (var context = new EFDbContext())
            {
                var pq = context.Prequalification.Find(PQID);

                pq.ClientTemplateId = clientTemplateId;
                //if (pq.PaymentReceived != null && pq.PaymentReceived == true)
                //{
                //    long newTemplatePaymentSectionId = context.ClientTemplates.Find(clientTemplateId)
                //        .Templates.TemplateSections.FirstOrDefault(section => section.TemplateSectionType == 3)
                //        .TemplateSectionID;
                //    PrequalificationCompletedSections completePaymentSection = new PrequalificationCompletedSections();
                //    completePaymentSection.PrequalificationId = pq.PrequalificationId;
                //    completePaymentSection.TemplateSectionId = newTemplatePaymentSectionId;
                //    completePaymentSection.SubmittedDate = DateTime.Now;

                //    context.PrequalificationCompletedSections.Add(completePaymentSection);
                //    context.SaveChanges();
                //}

                context.Entry(pq).State = EntityState.Modified;

                context.SaveChanges();
            }
            return "Success";
        }
        public void MoveAnswers(long PQID, Guid SourceTemplate, Guid DestinationTemplate)
        {
            using (var context = new EFDbContext())
            {
                var SubSections = context.TemplateSections.Where(r => r.TemplateID == SourceTemplate).SelectMany(r => r.TemplateSubSections).Select(r => r.SubSectionID).ToList();
                var DestinationSections = context.TemplateSections.Where(r => r.TemplateID == DestinationTemplate).SelectMany(r => r.TemplateSubSections).Select(r => r.SubSectionID).ToList();
                var FinalSections = context.MappedQuestions.Where(r => DestinationSections.Contains(r.DestinationSubSectionId.Value)).Select(r => r.DestinationSubSectionId).Distinct().ToList();
                foreach (var SubSectons in FinalSections)
                {
                    var SourceQuestionId = context.Questions.Where(r => r.SubSectionId == SubSectons).Select(r => r.QuestionID).ToList();
                    var FinalQuestions = context.MappedQuestions.Where(r => SourceQuestionId.Contains(r.DestinationQuestionId)).ToList();
                    foreach (var Questions in FinalQuestions)
                    {
                        var QuestionColumnId = context.QuestionColumnDetails.Where(r => r.QuestionId == Questions.SourceQuestionId).Select(r => r.QuestionColumnId);

                        var Userinput = context.PrequalificationUserInput.Where(r => QuestionColumnId.Contains(r.QuestionColumnId) && r.PreQualificationId == PQID).Select(r => r.PreQualRecId).ToList();
                        foreach (var Users in Userinput)
                        {

                            var preuserinput = context.PrequalificationUserInput.Find(Users);
                            var QuestionColumn = context.QuestionColumnDetails.FirstOrDefault(r => r.QuestionColumnId == preuserinput.QuestionColumnId);
                            var SourceQuestionID = QuestionColumn.Questions.QuestionID;
                            var DisplayOrder = QuestionColumn.ColumnNo;
                            var DestinationQuestionId = context.MappedQuestions.FirstOrDefault(r => r.SourceQuestionId == SourceQuestionID).DestinationQuestionId;
                            if (DestinationQuestionId != 0)
                            {
                                PrequalificationUserInput userinput = new PrequalificationUserInput();
                                userinput.PreQualificationId = preuserinput.PreQualificationId;

                                userinput.QuestionColumnId = context.QuestionColumnDetails.FirstOrDefault(r => r.QuestionId == DestinationQuestionId && r.ColumnNo == DisplayOrder).QuestionColumnId;
                                userinput.UserInput = preuserinput.UserInput;
                                context.PrequalificationUserInput.Add(userinput);
                                // context.Entry(preuserinput).State = EntityState.Modified;
                                context.SaveChanges();
                            }
                        }
                    }

                }
            }

        }
        public void ClearPayment(long PQID)
        {
            using (var context = new EFDbContext())
            {
                var pqpayment = context.LatestPrequalification.Find(PQID);
                if (pqpayment.PaymentReceived == true)
                {
                    var PQPayment = context.PrequalificationPayments.Where(r => r.PrequalificationId == PQID).ToList();
                    foreach (var Payment in PQPayment)
                    {
                        var Sites = context.PrequalificationSites.Where(r => r.PrequalificationId == PQID && Payment.PrequalificationPaymentsId == r.PrequalificationPaymentsId).ToList();
                        foreach (var sitepayment in Sites)
                        {
                            sitepayment.PrequalificationPaymentsId = null;
                            context.Entry(sitepayment).State = EntityState.Modified;
                        }
                        context.PrequalificationPayments.Remove(Payment);
                    }
                    var PaymentLog = context.PrequalificationTrainingAnnualFees.Find(PQID);
                    if (PaymentLog != null)
                        context.PrequalificationTrainingAnnualFees.Remove(PaymentLog);
                    pqpayment.PaymentReceived = false;
                    context.Entry(pqpayment).State = EntityState.Modified;
                    context.SaveChanges();
                }
            }

        }
        public void MoveEmRValues(long PQID)
        {
            using (var entity = new EFDbContext())
            {
                var emr = entity.PrequalificationEMRStatsYears.Where(r => r.PrequalificationId == PQID).ToList();

                foreach (var pq in emr)
                {
                    var sourceEmrValues = entity.PrequalificationEMRStatsYears.FirstOrDefault(r => r.PrequalificationId == PQID && r.EMRStatsYear == pq.EMRStatsYear).PrequalificationEMRStatsValues.ToList();
                    foreach (var emrvalues in sourceEmrValues)
                    {

                        var Emrvalues = entity.PrequalificationEMRStatsValues.Find(emrvalues.PrequalEMRStatsValuesId);
                        var HasQuestion = entity.MappedQuestions.FirstOrDefault(r => r.SourceQuestionId == emrvalues.QuestionId);
                        if (HasQuestion != null && HasQuestion.DestinationQuestionId != 0)
                        {
                            var DestinationQuestionID = HasQuestion.DestinationQuestionId;
                            Emrvalues.QuestionColumnId = entity.QuestionColumnDetails.FirstOrDefault(r => r.QuestionId == DestinationQuestionID).QuestionColumnId;
                            Emrvalues.QuestionId = DestinationQuestionID;

                            entity.Entry(Emrvalues).State = EntityState.Modified;

                        }

                    }
                    entity.SaveChanges();


                }
            }
        }
        public void CopyAnswers(long Sourcepqid, long Destpqid, Guid SourceTemplate, Guid DestinationTemplate)
        {
            using (var context = new EFDbContext())
            {
                var SubSections = context.TemplateSections.Where(r => r.TemplateID == SourceTemplate).SelectMany(r => r.TemplateSubSections).Select(r => r.SubSectionID).ToList();
                var DestinationSections = context.TemplateSections.Where(r => r.TemplateID == DestinationTemplate).SelectMany(r => r.TemplateSubSections).Select(r => r.SubSectionID).ToList();
                var FinalSections = context.MappedQuestions.Where(r => SubSections.Contains(r.SourceSubSectionId.Value) && DestinationSections.Contains(r.DestinationSubSectionId.Value)).Select(r => r.SourceSubSectionId).Distinct().ToList();
                foreach (var SubSectons in FinalSections)
                {
                    var SourceQuestionId = context.Questions.Where(r => r.SubSectionId == SubSectons).Select(r => r.QuestionID).ToList();
                    var FinalQuestions = context.MappedQuestions.Where(r => SourceQuestionId.Contains(r.SourceQuestionId)).ToList();
                    foreach (var Questions in FinalQuestions)
                    {
                        var QuestionColumnId = context.QuestionColumnDetails.Where(r => r.QuestionId == Questions.SourceQuestionId).Select(r => r.QuestionColumnId);

                        var Userinput = context.PrequalificationUserInput.Where(r => QuestionColumnId.Contains(r.QuestionColumnId) && r.PreQualificationId == Sourcepqid).Select(r => r.PreQualRecId).ToList();
                        foreach (var Users in Userinput)
                        {

                            var userinput = context.PrequalificationUserInput.Find(Users);
                            var QuestionColumn = context.QuestionColumnDetails.FirstOrDefault(r => r.QuestionColumnId == userinput.QuestionColumnId);
                            var SourceQuestionID = QuestionColumn.Questions.QuestionID;
                            var DisplayOrder = QuestionColumn.ColumnNo;
                            var DestinationQuestionId = context.MappedQuestions.FirstOrDefault(r => r.SourceQuestionId == SourceQuestionID).DestinationQuestionId;
                            PrequalificationUserInput preuserinput = new PrequalificationUserInput();
                            preuserinput.PreQualificationId = Destpqid;
                            preuserinput.QuestionColumnId = context.QuestionColumnDetails.FirstOrDefault(r => r.QuestionId == DestinationQuestionId && r.ColumnNo == DisplayOrder).QuestionColumnId;
                            preuserinput.UserInput = userinput.UserInput;
                            context.PrequalificationUserInput.Add(preuserinput);






                        }
                        context.SaveChanges();

                    }

                }
            }
        }
        //public void MoveDocuments(long SourcePQID, Guid SourceTemplate, Guid DestinationTemplate)
        //{
        //    using (var entity = new EFDbContext())
        //    {
        //        var docs = entity.Document.Where(r => r.ReferenceRecordID == SourcePQID && r.ReferenceType == "PreQualification").ToList();
        //        var sourcePQ = entity.Prequalification.Find(SourcePQID);
        //        if (docs != null)
        //            foreach (var Doc in docs)
        //            {


        //                //    Doc.ClientId = (Doc.DocumentTypeId == 2 || Doc.DocumentTypeId == 14) ? sourcePQ.ClientId : -1;
        //                Doc.SectionId = entity.TemplateSections.FirstOrDefault(r => r.TemplateSectionType == 4).TemplateSectionID;
        //                entity.Entry(Doc).State = EntityState.Modified;
        //            }
        //        entity.SaveChanges();
        //    }

        //    }
        /// <summary>
        /// To get the specific locked template comments
        /// </summary>
        /// <param name="clientTemplateId"></param>
        /// <returns></returns>
        public string GetLockTemplateComments(Guid clientTemplateId)
        {
            using (var context = new EFDbContext())
            {
                var template = context.ClientTemplates.Find(clientTemplateId).Templates;
                return template.LockComments;
            }
        }
        /// <summary>
        /// To check whether the specified client has training templates
        /// </summary>
        /// <param name="ClientId"></param>
        /// <param name="PQId"></param>
        /// <returns></returns>
        public bool CheckClientHasTrainingTemplates(long ClientId, long PQId)
        {
            var currentClient = _orgBusiness.GetOrganization(ClientId);
            var clientIds = new List<long>() { ClientId };
            if (currentClient.OrganizationType == "Super Client")
            {
                var pqClient = GetPrequalificationClient(PQId);
                if (pqClient != null)
                    clientIds = pqClient.ClientIdsList.ToList();
            }
            using (var context = new EFDbContext())
            {
                var clientTemplates =
                    context.ClientTemplates.Where(row => clientIds.Contains(row.ClientID) && row.Templates.TemplateType == 1)
                        .ToList();
                if (clientTemplates != null && clientTemplates.Count != 0)
                    return true;
                else
                    return false;
            }
        }
        /// <summary>
        /// To get the specific vendor financial analytics history in the form of graph
        /// </summary>
        /// <param name="PQId"></param>
        /// <param name="field"></param>
        /// <returns></returns>
        public FaGraphDetails GetGraphData(long PQId, string field)
        {
            using (var context = new EFDbContext())
            {
                //var fieldName = context.FinancialAnalyticsModel.Find(field);
                var currentPQ = context.Prequalification.Find(PQId);
                //var Reportingtype = context.PrequalificationReportingData.FirstOrDefault(r => r.ClientId == currentPQ.ClientId).ReportingType;
                //if (currentPQ.ClientId == 58)
                //{
                //    Reportingtype = 3;
                //}


                var returnVal = new FaGraphDetails();
                var Series = new List<Series>();
                var info1 = context.FinancialAnalyticsModel.Where(r => r.ClientId == currentPQ.ClientId && r.VendorId == currentPQ.VendorId && r.DateOfFinancialStatements.HasValue).OrderByDescending(r => r.DateOfFinancialStatements).Take(4).ToList();
                var faData = info1.Select(r => new { text = (decimal)r.GetType().GetProperty(field).GetValue(r) }).ToList();

                if (faData.Any())
                {

                    var serices = new Series() { data = faData.Select(r => r.text).ToList(), name = currentPQ.Vendor.Name };

                    for (int i = serices.data.Count; i < 4; i++)
                        serices.data.Add(0);
                    Series.Add(serices);
                }

                var biddings = context.PrequalificationReportingData.Where(r => r.PrequalificationId == PQId).Select(r => new { r.ReportingDataId, r.ReportingType }).ToList();
                //var latestPQs = context.LatestPrequalification.Where(r => r.ClientId == currentPQ.ClientId && r.PrequalificationId != currentPQ.PrequalificationId)
                //    .Select(r => r.PrequalificationId).ToList();
                foreach (var bidd in biddings)
                {
                    //var pqs = (context.PrequalificationReportingData.Where(r => latestPQs.Contains((long)r.PrequalificationId) && r.ReportingDataId == bidd && r.ReportingType == Reportingtype )
                    //    .Select(r => r.PrequalificationId).ToList());
                    var biddingName = "";//context.BiddingInterests.Find(bidd).BiddingInterestName;
                    if (bidd.ReportingType == 3)
                    {
                        biddingName = context.ClaycoBiddingInterests.Find(bidd.ReportingDataId).BiddingInterestName;
                    }
                    else
                    {
                        biddingName = context.BiddingInterests.Find(bidd.ReportingDataId).BiddingInterestName;
                    }
                    var serices = new Series() { data = new List<decimal>(), name = biddingName };
                    var baseinfo = from f in context.FinancialAnalyticsModel
                                   join pr in context.PrequalificationReportingData.

                                   Where(r => r.ReportingDataId == bidd.ReportingDataId && r.ReportingType == bidd.ReportingType)
                                   on new { f.ClientId, f.VendorId } equals
                                    new { pr.ClientId, pr.VendorId }
                                   join lp in context.LatestPrequalification.Where(r => r.ClientId == currentPQ.ClientId && r.PrequalificationId != currentPQ.PrequalificationId)
                                   on pr.PrequalificationId equals lp.PrequalificationId
                                   select f;
                    var groupedData = baseinfo.GroupBy(r => new { r.ClientId, r.VendorId }).Select(r => r.Take(4)).ToList();
                    for (int i = 0; i < 4; i++)
                    {
                        var info = 0m;
                        var data = groupedData.Select(g => g.OrderByDescending(r1 => r1.DateOfFinancialStatements).Skip(i).FirstOrDefault()).Where(r => r != null).ToList();
                        if (data == null || !data.Any())
                        {
                            info = 0m;
                        }
                        else
                        {
                            info = data.Average(r => (decimal)r.GetType().GetProperty(field).GetValue(r));
                        }
                        serices.data.Add(info);
                        //var x=context.FinancialAnalyticsModel.Where(r=>latestPQs.Contains(r.PrequalificationId) && r.DateOfFinancialStatements!=null && r.DateOfFinancialStatements.Value.Date==dat)
                    }
                    Series.Add(serices);
                }
                var dates = new List<string> { "Current Year", "Previous Year", "2 Years Previous", "3 Years Previous" };
                returnVal.CategoryAxis = dates;
                returnVal.Series = Series.OrderBy(r => r.name != currentPQ.Vendor.Name).ThenBy(r => r.name).ToList();

                return returnVal;
            }

        }
        /// <summary>
        /// To save the interim subsection
        /// </summary>
        /// <param name="userInputs"></param>
        /// <param name="prequalId"></param>
        /// <returns></returns>
        public string SubSectionInterimSave(List<PQUserInput> userInputs, long prequalId)
        {
            using (var context = new EFDbContext())
            {
                foreach (PQUserInput userInput in userInputs)
                {
                    var isInsert = false;
                    PrequalificationUserInput preUserInput = context.PrequalificationUserInput.FirstOrDefault(rec => rec.QuestionColumnId == userInput.QuestionColumnId && rec.PreQualificationId == prequalId);

                    //if no record found
                    if (preUserInput == null)
                    {
                        isInsert = true;
                        preUserInput = new PrequalificationUserInput();
                    }

                    preUserInput.PreQualificationId = prequalId;
                    preUserInput.QuestionColumnId = userInput.QuestionColumnId;
                    preUserInput.UserInput = userInput.UserInput;

                    if (isInsert)
                    {
                        if (String.IsNullOrWhiteSpace(userInput.UserInput))
                            continue;
                        context.PrequalificationUserInput.Add(preUserInput);
                    }
                    else
                        context.Entry(preUserInput).State = EntityState.Modified;

                    context.SaveChanges();
                }
            }
            return "Saved";
        }
        /// <summary>
        /// To update the toggle display of specific PQ subsections
        /// </summary>
        /// <param name="formStatus"></param>
        /// <param name="prequal"></param>
        /// <returns></returns>
        public string HideOrUnhidePrequalificationSections(LocalChangeStatus formStatus, Prequalification prequal)
        {
            using (var context = new EFDbContext())
            {
                foreach (var section in formStatus.displayTemplateSections)
                {
                    var isInsert = false;
                    PrequalificationSectionsToDisplay sectionToDisplay =
                        context.PrequalificationSectionsToDisplay.FirstOrDefault(
                            rec =>
                                rec.PrequalificationId == prequal.PrequalificationId &&
                                rec.TemplateSectionId == section.TemplateSectionId);
                    if (sectionToDisplay == null)
                    {
                        isInsert = true;
                        sectionToDisplay = new PrequalificationSectionsToDisplay();
                    }
                    sectionToDisplay.PrequalificationId = prequal.PrequalificationId;
                    sectionToDisplay.TemplateSectionId = section.TemplateSectionId;

                    if (section.Status)
                    {
                        sectionToDisplay.Visible = true;
                        if (isInsert)
                            context.Entry(sectionToDisplay).State = EntityState.Added;

                        else
                            context.Entry(sectionToDisplay).State = EntityState.Modified;
                    }
                    else
                    {
                        sectionToDisplay.Visible = false;
                        if (isInsert)
                            context.Entry(sectionToDisplay).State = EntityState.Added;

                        else
                            context.Entry(sectionToDisplay).State = EntityState.Modified;
                    }
                    context.SaveChanges();
                }
            }
            return "Saved";
        }
        /// <summary>
        /// To check whether the specific section has interim subsections
        /// </summary>
        /// <param name="templateSectionId"></param>
        /// <returns></returns>
        public bool checkSectionHasInterimSubSections(long templateSectionId)
        {
            bool hasInterimSubSection = false;
            using (var context = new EFDbContext())
            {
                TemplateSections templateSection = context.TemplateSections.Find(templateSectionId);
                TemplateSectionsPermission templateSectionPermission = new TemplateSectionsPermission();
                ;
                if (templateSection != null)
                    templateSectionPermission = templateSection.TemplateSectionsPermission.FirstOrDefault();
                if (templateSection != null && templateSectionPermission != null && templateSectionPermission.VisibleTo == 2)
                {
                    hasInterimSubSection = templateSection
                        .TemplateSubSections.Any(
                            r =>
                                r.Questions.Any(rec =>
                                    rec.QuestionColumnDetails.Any(row => row.QuestionControlTypeId == 12)));
                }
            }
            return hasInterimSubSection;
        }
        /// <summary>
        /// To get the analytics information of specific PQ
        /// </summary>
        /// <param name="PqId"></param>
        /// <returns></returns>
        public List<FinancialAnalyticsModel> GetAnalytics(long PqId)
        {
            var VendorId = 0L;
            var ClientId = 0L;
            using (var entityDB = new EFDbContext())
            {
                var PQ = entityDB.Prequalification.Find(PqId);
                VendorId = PQ.VendorId;
                ClientId = PQ.ClientId;
            }
            return GetAnalytics(VendorId, ClientId, PqId);
        }
        /// <summary>
        /// To get the status history of specific PQ
        /// </summary>
        /// <param name="PrequalificationId"></param>
        /// <returns></returns>
        public IEnumerable<StatusHistoryLog> StatusHistory(long PrequalificationId)
        {
            using (var entity = new EFDbContext())
            {
                //  var statusHistory = entity.PrequalificationStatusChangeLog.Where(rec => rec.PrequalificationId == PrequalificationId).OrderByDescending(rec => rec.StatusChangeDate).ToList();
                var pqStatuses = entity.PrequalificationStatusChangeLog.Where(r => r.PrequalificationId == PrequalificationId).Select(r => new StatusHistoryLog { PQSiteid = -1, SiteName = "", PrequalificationStatusName = r.PrequalificationStatus.PrequalificationStatusName, StatusChangeDate = r.StatusChangeDate, ChangedBy = r.StatusChangedBySystemUser.Contacts.FirstOrDefault() == null ? r.StatusChangedBySystemUser.Email : r.StatusChangedBySystemUser.Contacts.FirstOrDefault().LastName + ", " + r.StatusChangedBySystemUser.Contacts.FirstOrDefault().FirstName, CreatedDate = r.CreatedDate }).ToList();

                var siteIds = entity.Prequalification.Find(PrequalificationId).PrequalificationSites.Select(r => r.PrequalificationSitesId).ToList();
                var SitesHistorylog = entity.LocationStatusLog.Where(r => siteIds.Contains(r.pqSiteId)).Select(r => new StatusHistoryLog { PQSiteid = -1, SiteName = r.PrequalificationSites.ClientBusinessUnitSites.SiteName, PrequalificationStatusName = r.PrequalificationStatus.PrequalificationStatusName, StatusChangeDate = r.StatusChangeDate, ChangedBy = r.SystemUsers.Contacts.FirstOrDefault() == null ? r.SystemUsers.Email : r.SystemUsers.Contacts.FirstOrDefault().LastName + ", " + r.SystemUsers.Contacts.FirstOrDefault().FirstName }).ToList();

                pqStatuses.AddRange(SitesHistorylog);

                return pqStatuses.OrderByDescending(r => r.CreatedDate).ThenByDescending(r => r.StatusChangeDate);
            }

        }
        /// <summary>
        /// To get the analytics history of specific client, vendor and prequalification
        /// </summary>
        /// <param name="VendorId"></param>
        /// <param name="ClientId"></param>
        /// <param name="PQId"></param>
        /// <returns></returns>
        public List<FinancialAnalyticsModel> GetAnalytics(long VendorId, long ClientId, long PQId)
        {
            var vendorFinancialData = new List<FinancialAnalyticsModel>();
            if (VendorId != 0)
                using (var entityDB = new EFDbContext())
                {

                    var vendorOrg = entityDB.Organizations.Find(VendorId);
                    if (vendorOrg.FinancialMonth == null || vendorOrg.FinancialDate == null)
                        return vendorFinancialData;
                    //DateTime todaysDate = DateTime.Now.Date;
                    //int Month = todaysDate.Month;
                    //int day = todaysDate.Day;

                    //var date = new DateTime(DateTime.Now.Year, vendorOrg.FinancialMonth ?? 1, vendorOrg.FinancialDate ?? 1);
                    //if (((vendorOrg.FinancialMonth??0) > Month) || ((vendorOrg.FinancialMonth??0) == Month && (vendorOrg.FinancialDate??0) <= day))
                    //{
                    //    date = new DateTime(DateTime.Now.Year - 1, vendorOrg.FinancialMonth ?? 1, vendorOrg.FinancialDate ?? 1);

                    //}

                    //TimeSpan days = todaysDate - date;
                    //int Days = Math.Abs(days.Days);


                    //if (Days > 180)
                    //{
                    //    vendorFinancialData = entityDB.FinancialAnalyticsModel.Where(x => x.VendorId == VendorId && x.ClientId == ClientId).OrderByDescending(y => y.DateOfFinancialStatements).Take(4).ToList();
                    //}
                    //else
                    //{
                    //    vendorFinancialData = entityDB.FinancialAnalyticsModel.Where(x => x.VendorId == VendorId && x.ClientId == ClientId).OrderByDescending(x => x.DateOfFinancialStatements).Skip(1).Take(4).ToList();
                    //}
                    var latestPQ = entityDB.LatestPrequalification.Any(r => r.PrequalificationId == PQId);
                    if (latestPQ || PQId == 0)
                        vendorFinancialData = entityDB.FinancialAnalyticsModel.Where(x => x.VendorId == VendorId && x.ClientId == ClientId).OrderByDescending(y => y.DateOfFinancialStatements).Take(4).ToList();
                    else
                        vendorFinancialData = mapper.Map<List<FinancialAnalyticsModel>>(entityDB.FinancialAnalyticsLog.Where(x => x.PrequalificationId == PQId).OrderByDescending(y => y.DateOfFinancialStatements).Take(4).ToList());
                }
            var temp = new List<FinancialAnalyticsModel>();
            if (vendorFinancialData.Any())
            {
                var financialDate = vendorFinancialData.FirstOrDefault().DateOfFinancialStatements;
                for (int i = 0; i < 4; i++)
                {
                    var locfinancialDate = financialDate.Value.AddYears(-i);
                    temp.Add(vendorFinancialData.FirstOrDefault(r => r.DateOfFinancialStatements == locfinancialDate) ?? new FinancialAnalyticsModel() { VendorId = VendorId, ClientId = ClientId, DateOfFinancialStatements = locfinancialDate }
                        );
                }
            }
            return temp;

        }
        public List<KeyValuePair<long, string>> GetParentColor(long pqId, List<long> questionId)
        {
            var result = new List<KeyValuePair<long, string>>();
            if (questionId == null || !questionId.Any())
            {
                return result;
            }
            using (var context = new EFDbContext())
            {
                var parentQuestions = context.QuestionsDependants.Where(r => questionId.Contains(r.DependantId)).ToList();
                foreach (var parentQuestion in parentQuestions)
                {
                    var questionColumn = parentQuestion?.Questions.QuestionColumnDetails.FirstOrDefault();
                    if (questionColumn == null)
                        continue;
                    var answer = context.PrequalificationUserInput.FirstOrDefault(r => r.PreQualificationId == pqId && r.QuestionColumnId == questionColumn.QuestionColumnId);
                    if (answer == null) continue;
                    var hasColor = context.QuestionColors.FirstOrDefault(r => r.QuestionID == parentQuestion.QuestionID && r.Answer == answer.UserInput);
                    if (hasColor != null)
                    {
                        result.Add(new KeyValuePair<long, string>(parentQuestion.DependantId, hasColor.Color));
                    }
                }
                return result;
            }
        }
        public List<KeyValuePair<long, string>> GetQuestionColor(long pqId, List<long> questionId) // This is to get the child colors. Question color was handled in View.
        {
            var result = new List<KeyValuePair<long, string>>();

            if (questionId == null || !questionId.Any())
                return result;
            using (var context = new EFDbContext())
            {


                var qColList = context.QuestionColumnDetails.Where(r => questionId.Contains(r.QuestionId));
                foreach (var qCol in qColList.ToList())
                {
                    var dependentQuestions = context.QuestionsDependants.Where(r => qCol.QuestionId == r.QuestionID)
                        .Select(r => r.DependantId);
                    //  var HasColorQuestionIDs=DependentQuestions.Where(r => );
                    if (!dependentQuestions.Any()) continue;

                    var clr = new List<string>();
                    //if (qCol == null)
                    //    continue;
                    var questionColumnId = qCol.QuestionColumnId;
                    var answer = context.PrequalificationUserInput.FirstOrDefault(r => r.PreQualificationId == pqId && questionColumnId == r.QuestionColumnId);
                    if (answer == null) continue;
                    var hasColor = context.QuestionColors.FirstOrDefault(r => r.QuestionID == qCol.QuestionId && r.Answer == answer.UserInput);
                    if (hasColor != null)
                    {

                        clr.Add(hasColor.Color);
                    }


                    foreach (var dQuestion in dependentQuestions.ToList())
                    {
                        var qColumns = context.QuestionColumnDetails.FirstOrDefault(r => r.QuestionId == dQuestion);
                        if (qColumns == null)
                            continue;
                        var dQuestionColumnId = qColumns.QuestionColumnId;
                        var answerRec = context.PrequalificationUserInput.FirstOrDefault(r => r.PreQualificationId == pqId && dQuestionColumnId == r.QuestionColumnId);
                        if (answerRec == null) continue;
                        var qColor = context.QuestionColors.FirstOrDefault(r => r.QuestionID == dQuestion && r.Answer == answerRec.UserInput);
                        if (qColor != null && !clr.Contains(qColor.Color))
                        {

                            clr.Add(qColor.Color);
                        }
                    }
                    if (!clr.Any())
                        continue;
                    if (clr.Count > 1)
                    {
                        result.Add(new KeyValuePair<long, string>(qCol.QuestionId, "Multiple")); ;
                    }
                    else result.Add(new KeyValuePair<long, string>(qCol.QuestionId, clr.FirstOrDefault()));
                }

            }

            return result;
        }
        /// <summary>
        /// To get the statistics of specific client and vendor
        /// </summary>
        /// <param name="VendorId"></param>
        /// <param name="ClientId"></param>
        /// <returns></returns>
        public Statistics GetStatistics(long VendorId, long ClientId)
        {
            var returnVal = new Statistics();
            //var result = new List<String>();
            using (var context = new EFDbContext())
            {
                var latestPq = context.LatestPrequalification.FirstOrDefault(r => r.VendorId == VendorId && r.ClientId == ClientId);
                var query = (from PUI in context.PrequalificationUserInput.Where(PUI => PUI.PreQualificationId == latestPq.PrequalificationId)
                             join qcd in context.QuestionColumnDetails
                          on PUI.QuestionColumnId equals qcd.QuestionColumnId
                             join Q in context.Questions on qcd.QuestionId equals Q.QuestionID
                             //join tss in context.TemplateSubSections on Q.SubSectionId equals tss.SubSectionID
                             //join t in context.TemplateSections on tss.TemplateSectionID equals t.TemplateSectionID

                             select new
                             {
                                 PUI.UserInput,
                                 Q.QuestionText
                             });
                var EMR = query.Where(r => r.QuestionText == "EMR:").Select(r => r.UserInput.Replace(",", "")).ToList();
                var RIR = query.Where(r => r.QuestionText == "Recordable Incident Frequency Rate: # recordable cases (total from columns G, H, I, J) x 200,000 / Total employee hours worked last year").Select(r => r.UserInput.Replace(",", "")).ToList();
                var DART = query.Where(r => r.QuestionText == "Days Away, Restrictions or Transfers Rate (DART): # of DART incidents (total from columns H and I) x 200,000 / Total employee hours worked last year").Select(r => r.UserInput.Replace(",", "")).ToList();

                string Emr = string.Join(",", EMR);
                string Rir = string.Join(",", RIR);
                string Dart = string.Join(",", DART);
                returnVal.EMR = Emr;
                returnVal.RIR = Rir;
                returnVal.DART = Dart;
            }
            //var da = returnVal.EMR.Split().ToList();
            //    var rir = returnVal.RIR.Split().ToList();
            //    var dart = returnVal.DART.Split().ToList();

            return returnVal;


        }
        /// <summary>
        /// To update the specific document name
        /// </summary>
        /// <param name="DocumentId"></param>
        /// <param name="DocName"></param>
        /// <returns></returns>
        public string ChangeDocumentName(Guid DocumentId, string DocName)
        {
            using (var entityDB = new EFDbContext())
            {
                var name = DocName;
                if (!name.EndsWith(".pdf", StringComparison.CurrentCultureIgnoreCase))
                {
                    name += ".pdf";
                }
                var doc = entityDB.Document.Find(DocumentId);
                doc.DocumentName = name;
                entityDB.Entry(doc).State = EntityState.Modified;
                entityDB.SaveChanges();
            }
            return "Ok";
        }
        /// <summary>
        /// To get the financial analytics data
        /// </summary>
        /// <param name="oldPreQualificationId"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public List<FinancialAnalyticsLog> CopyAnalytics(long? oldPreQualificationId, EFDbContext context)
        {
            //using (var context = new EFDbContext())
            {
                var pq = context.Prequalification.Find(oldPreQualificationId);
                var a = mapper.Map<List<FinancialAnalyticsLog>>(context.FinancialAnalyticsModel.Where(r => r.ClientId == pq.ClientId && r.VendorId == pq.VendorId));
                a.ForEach(r => r.PrequalificationId = oldPreQualificationId);

                var logs = context.FinancialAnalyticsLog.Where(r => r.PrequalificationId == oldPreQualificationId);
                foreach (var log in logs)
                {
                    context.FinancialAnalyticsLog.Remove(log);
                }
                return a;
            }

        }
        /// <summary>
        /// To check whether the specific PQ has financial data
        /// </summary>
        /// <param name="prequalificationid"></param>
        /// <returns></returns>
        public bool HasFinancialData(long prequalificationid)
        {

            bool dataexists = false;
            using (var context = new EFDbContext())
            {
                var vendorid = context.Prequalification.Find(prequalificationid).VendorId;
                var finance = context.Organizations.Find(vendorid);


                var currentDate = DateTime.Now;
                var financial = new DateTime(currentDate.Year, finance.FinancialMonth ?? 1, finance.FinancialDate ?? 1).Date;
                if (financial.Date > currentDate.Date)
                    financial = financial.AddYears(-1);
                var data = context.FinancialAnalyticsModel.Where(r => r.VendorId == vendorid && r.DateOfFinancialStatements.Value == financial).ToList();
                if (data.Count != 0)
                {
                    dataexists = true;
                }
            }
            return dataexists;
        }
        /// <summary>
        /// To check whether the specific user has permissions to enter financial data
        /// </summary>
        /// <param name="SectionId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public bool HasFinancialAnalytics(long SectionId, Guid userId)
        {
            bool financialdata = false;
            using (var context = new EFDbContext())
            {
                var text = context.Database.SqlQuery<int>(@"select COUNT(qcd.ButtonTypeReferenceValue) from TemplateSections  as t
                          join [dbo].[templateSubsections] as ts
                          on t.TemplateSectionID=ts.TemplateSectionID
                             join Questions as q 
                           on ts.SubSectionID=q.SubSectionId
                             join questioncolumndetails as qcd 
                           on qcd.QuestionId=q.QuestionID
						   join QuestionControlType as qc
						   on qcd.QuestionControlTypeId=qc.QuestionTypeID
                            where t.templatesectionId=@SectionIdVar and qc.QuestionType='CtrlButton' and qcd.ButtonTypeReferenceValue=2", new SqlParameter("@SectionIdVar", SectionId)).ToList();
                var sysUserBusiness = new SystemUserBusiness();
                if (text.Any(r => r != 0) && sysUserBusiness.IsUserHasPermission(userId, 1005))
                {

                    financialdata = true;

                }

            }
            return financialdata;
        }
        /// <summary>
        /// To record zero payments
        /// </summary>
        /// <param name="pqid"></param>
        public void ZeroPayments(long pqid)
        {
            using (var context = new EFDbContext())
            {
                var predetails = context.Prequalification.Find(pqid);
                if (predetails == null)
                    return;
                if (predetails.ClientTemplates == null)
                    return;
                var paymentTemplate = predetails.ClientTemplates.Templates.TemplateSections.FirstOrDefault(r => r.TemplateSectionType == 3);
                var Preq = context.ClientTemplatesBUGroupPrice.FirstOrDefault(
                      rec =>
                          rec.ClientTemplateID == predetails.ClientTemplateId && rec.GroupingFor == 0 &&
                          rec.GroupPriceType == 0);
                var preqprice = 0m;
                if (Preq != null)
                {
                    preqprice = Preq.GroupPrice.Value;
                }



                if (preqprice == 0 && !paymentTemplate.Visible)
                {
                    predetails.PaymentReceived = true;


                    PrequalificationPayments paymentLog =
                                    context.PrequalificationPayments.FirstOrDefault(
                                        rec => rec.PrequalificationId == pqid && rec.PaymentCategory == 0);
                    if (paymentLog == null)
                    {
                        paymentLog = new PrequalificationPayments();
                        paymentLog.PaymentReceivedAmount = preqprice;
                        paymentLog.PaymentReceivedDate = DateTime.Now;
                        paymentLog.PaymentCategory = 0;
                        paymentLog.InvoiceAmount = preqprice;
                        paymentLog.PrequalificationId = pqid;
                        predetails.TotalPaymentReceivedAmount = preqprice;
                        context.Entry(paymentLog).State = EntityState.Added;
                        context.Entry(predetails).State = EntityState.Modified;
                        //var TemplateSectionIds = context.TemplateSections.FirstOrDefault(r => r.TemplateID == TemplateID && r.Visible == false).Select(r => r.TemplateSectionID).ToList();
                        ////    List<PrequalificationCompletedSections> PrequalificationCompletedSections = new List<PrequalificationCompletedSections>();

                        //foreach (var id in TemplateSectionIds)
                        //{
                        //    PrequalificationCompletedSections compsections = new PrequalificationCompletedSections();
                        //    compsections.PrequalificationId = pqid;
                        //    compsections.SubmittedDate = DateTime.Now;
                        //    compsections.TemplateSectionId = id;
                        //    context.Entry(compsections).State = EntityState.Added;
                        //}
                    }
                    context.SaveChanges();
                }

            }
        }
        /// <summary>
        /// To unlock the specific PQ documents
        /// </summary>
        /// <param name="PQId"></param>
        /// <returns></returns>
        public string UnlockPQDocs(long PQId)
        {

            using (var entity = new EFDbContext())
            {
                var pqDetails = entity.Prequalification.Find(PQId);
                pqDetails.BlockClientDocsSelection = !pqDetails.BlockClientDocsSelection;
                entity.Entry(pqDetails).State = EntityState.Modified;
                entity.SaveChanges();
            }
            return "Success";
        }
        /// <summary>
        /// To get the prequalifications of specific template
        /// </summary>
        /// <param name="templateId"></param>
        /// <returns></returns>
        public DataSet getERIPrequalifications(Guid? templateId)
        {
            using (var entity = new EFDbContext())
            {
                string superUserRoleId = entity.SystemRoles.FirstOrDefault(row => row.RoleName == "Super User" && row.OrganizationType == "Vendor").RoleId;
                DataSet eriPrequalifications = new DataSet();
                eriPrequalifications.Tables.Add(new DataTable());
                var prequalifications = new List<Prequalification>();
                if (templateId == null)
                    prequalifications = entity.Prequalification.Where(row => row.Client.OrganizationType == "Super Client").ToList();
                else
                    prequalifications = entity.Prequalification.Where(row => row.ClientTemplates.TemplateID == templateId).ToList();

                foreach (var prequalification in prequalifications)
                {
                    var superUserDetails = _orgBusiness.GetSuperuser(prequalification.VendorId);
                    var pqClients = prequalification.PrequalificationClient.ClientIdsList;

                    var clientNames = string.Join(",", entity.Organizations.Where(rec => pqClients.Contains(rec.OrganizationID)).Select(row => row.Name));

                    DataRow dr = eriPrequalifications.Tables[0].NewRow();
                    dr["Name"] = prequalification.Vendor.Name;
                    dr["Address1"] = prequalification.Vendor.Address1;
                    dr["Address2"] = prequalification.Vendor.Address2;
                    dr["City"] = prequalification.Vendor.City;
                    dr["State"] = prequalification.Vendor.State;
                    dr["Country"] = prequalification.Vendor.Country;
                    dr["Zip"] = prequalification.Vendor.Zip;
                    dr["FirstName"] = superUserDetails.FirstName;
                    dr["LastName"] = superUserDetails.LastName;
                    dr["MobileNumber"] = superUserDetails.MobileNumber;
                    dr["Email"] = superUserDetails.Email;
                    dr["TemplateCode"] = prequalification.ClientTemplates.Templates.TemplateCode;
                    dr["PrequalificationStatusName"] = prequalification.PrequalificationStatus.PrequalificationStatusName;
                    dr["PrequalificationStart"] = prequalification.PrequalificationStart.ToString("MM/dd/yyyy");
                    dr["PrequalificationFinish"] = prequalification.PrequalificationFinish.ToString("MM/dd/yyyy");
                    dr["VendorId"] = prequalification.VendorId;
                    dr["Clients"] = clientNames;

                    eriPrequalifications.Tables[0].Rows.Add(dr);
                }
                return eriPrequalifications;
            }
        }
        /// <summary>
        /// To get the final statuses
        /// </summary>
        /// <returns></returns>
        public List<long> Finalstatus()
        {
            var finalStatuses = new List<long> { 9, 24, 25, 26, 13, 8, -1 };// removed 7 by rajesh
            return finalStatuses;
        }
        /// <summary>
        /// To get the specific business unit payment data
        /// </summary>
        /// <param name="buId"></param>
        /// <param name="pqId"></param>
        /// <returns></returns>

        public string HandleInvitation(long vendorId, string code, Guid loginUser, string comments, out long pqId)
        {
            pqId = 0;
            if (string.IsNullOrEmpty(code))
                return "";
            using (var entity = new EFDbContext())
            {
                var invitation = entity.VendorInviteDetails.FirstOrDefault(r => r.InvitationCode.ToLower() == code.ToLower().Trim());
                if (invitation == null)
                    return "Invalid code";
                if (entity.Prequalification.Any(r => r.ClientId == invitation.ClientId && r.VendorId == vendorId))
                    return "";
                var vendorPrequalification_ToInsert = new Prequalification();
                if (_clientBusiness.CheckClientIsInERIClients(invitation.ClientId))
                {
                    var parentClient = entity.SubClients.FirstOrDefault(r => r.ClientId == invitation.ClientId).SuperClientOrganization;
                    if (parentClient == null)
                    {
                        return "Unable to process the request";
                    }

                    int[] allowedSections = { 1, 2 };
                    int[] InprocessPrequalifications = { 9, 13, 14, 3, 4, 5, 7, 8, 15 };
                    var pq = entity.LatestPrequalification.FirstOrDefault(r => r.ClientId == parentClient.OrganizationID && r.VendorId == vendorId);
                    if (pq != null)
                    {
                        if (Array.IndexOf(InprocessPrequalifications, pq.PrequalificationStatusId) != -1)
                            return "This questionnaire has already been selected";
                        else if (pq.PrequalificationStatusId == 10)
                            return "../Prequalification/TemplatesList?PreQualificationId=-1&oldPreQualificationId=" + pq.PrequalificationId;
                        else return "../Prequalification/TemplatesList?PreQualificationId=" + pq.PrequalificationId;
                    }
                    else
                    {
                        vendorPrequalification_ToInsert = new Prequalification();
                        vendorPrequalification_ToInsert.VendorId = vendorId;
                        vendorPrequalification_ToInsert.ClientId = parentClient.OrganizationID;
                        vendorPrequalification_ToInsert.BlockClientDocsSelection = false;
                        vendorPrequalification_ToInsert.PrequalificationCreate = DateTime.Now;
                        //vendorPrequalification_ToInsert.PrequalificationStart = DateTime.Now;
                        //vendorPrequalification_ToInsert.PrequalificationFinish = DateTime.Now;
                        //vendorPrequalification_ToInsert.PrequalificationSubmit = DateTime.Now;
                        vendorPrequalification_ToInsert.PrequalificationStatusId = 2;
                        if (invitation.InvitationSentFrom == 0)
                            vendorPrequalification_ToInsert.InvitationFrom = LocalConstants.PQInvitationId;
                        else
                            vendorPrequalification_ToInsert.InvitationFrom = invitation.InvitationSentFrom;
                        vendorPrequalification_ToInsert.InvitationSentByUser = invitation.InvitationSentByUser;
                        vendorPrequalification_ToInsert.InvitationId = invitation.InvitationId;
                        #region Client templates Handling
                        var clienttemplate = entity.ClientTemplates.FirstOrDefault(r => r.ClientID == parentClient.OrganizationID && r.Templates.RiskLevel == invitation.RiskLevel);
                        if (clienttemplate != null)
                        {
                            vendorPrequalification_ToInsert.PrequalificationStatusId = 3;
                            vendorPrequalification_ToInsert.ClientTemplateId = clienttemplate.ClientTemplateID;
                            vendorPrequalification_ToInsert.locked = clienttemplate.Templates.LockedByDefault;
                            vendorPrequalification_ToInsert.Comments = comments;
                            #region Adding PQ Clients
                            entity.PrequalificationClient.Add(new PrequalificationClient()
                            {
                                ClientIds = invitation.ClientId + "",
                                PQId = vendorPrequalification_ToInsert.PrequalificationId,
                                Region = "1"
                            });
                            #endregion
                            #region Adding locations

                            foreach (var location in invitation.VendorInviteLocations)
                            {
                                var pqSites = new PrequalificationSites();
                                pqSites.ClientId = invitation.ClientId;
                                pqSites.VendorId = vendorId;
                                pqSites.PrequalificationId = vendorPrequalification_ToInsert.PrequalificationId;
                                pqSites.ClientBULocation = location.ClientBusinessUnitSites.BusinessUnitLocation;
                                pqSites.ClientBusinessUnitId = location.ClientBusinessUnitSites.ClientBusinessUnitId;
                                pqSites.ClientBusinessUnitSiteId = location.ClientBusinessUnitSiteID;
                                entity.PrequalificationSites.Add(pqSites);
                            }

                            #endregion

                        }
                        #endregion

                        //Rajesh DT:-8/7/2013>>>>
                        vendorPrequalification_ToInsert.PrequalificationStart = DateTime.Now;
                        DateTime FinishDateTime = DateTime.Now;
                        var preStatus = entity.PrequalificationStatus.FirstOrDefault(rec => rec.PrequalificationStatusId == vendorPrequalification_ToInsert.PrequalificationStatusId);
                        if (preStatus != null)
                        {
                            if (preStatus.PeriodLengthUnit.ToLower().Equals("d"))
                                FinishDateTime = DateTime.Now.AddDays(preStatus.PeriodLength);
                            else if (preStatus.PeriodLengthUnit.ToLower().Equals("m"))
                                FinishDateTime = DateTime.Now.AddMonths(preStatus.PeriodLength);
                            else if (preStatus.PeriodLengthUnit.ToLower().Equals("y"))
                                FinishDateTime = DateTime.Now.AddYears(preStatus.PeriodLength);
                        }
                        vendorPrequalification_ToInsert.PrequalificationFinish = FinishDateTime;
                        // Ends <<<<<

                        vendorPrequalification_ToInsert.UserIdAsCompleter = loginUser;
                        entity.Prequalification.Add(vendorPrequalification_ToInsert);
                        logs.Info("Pq Added Successfully " + vendorPrequalification_ToInsert.PrequalificationId, "vendorId=" + vendorId + "&code=" + code + "&loginUser=" + loginUser + "&comments=" + comments + "&pqId=" + pqId, "Vendorcontroller/StartPrequalification/HandleInvitation");

                        //To add Prequalification Logs
                        PrequalificationStatusChangeLog preQualificationLog = new PrequalificationStatusChangeLog();
                        preQualificationLog.PrequalificationId = vendorPrequalification_ToInsert.PrequalificationId;
                        preQualificationLog.PrequalificationStatusId = vendorPrequalification_ToInsert.PrequalificationStatusId;
                        preQualificationLog.StatusChangeDate = DateTime.Now;
                        preQualificationLog.StatusChangedByUser = loginUser;
                        preQualificationLog.PrequalificationStart = vendorPrequalification_ToInsert.PrequalificationStart;
                        preQualificationLog.PrequalificationFinish = vendorPrequalification_ToInsert.PrequalificationFinish;
                        preQualificationLog.CreatedDate = DateTime.Now;
                        entity.PrequalificationStatusChangeLog.Add(preQualificationLog);

                    }
                    //return "ERI Client - " + invitation.Organization.Name;

                }

                invitation.VendorValidated = true;
                entity.Entry(invitation).State = EntityState.Modified;
                entity.SaveChanges();
                pqId = vendorPrequalification_ToInsert.PrequalificationId;
                return "../Prequalification/TemplatesList?PreQualificationId=" + vendorPrequalification_ToInsert.PrequalificationId;
            }
            return "";
        }
        /// <summary>
        /// To get the specific document
        /// </summary>
        /// <param name="docId"></param>
        /// <returns></returns>
        public Document getDocument(Guid? docId)
        {
            using (var entity = new EFDbContext())
            {
                return
                    entity.Document.Find(docId);
            }
        }

        public CultureInfo SetCulture(string LanguageCode)
        {
            Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(LanguageCode);
            Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator = ".";
            Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;
            return Thread.CurrentThread.CurrentUICulture;
        }
        public LocalVendorInviteDetails PQInviteInfo(long pqId)
        {
            using (var entity = new EFDbContext())
            {
                var prequalification = entity.Prequalification.Find(pqId);
                LocalVendorInviteDetails inviteDetails = new LocalVendorInviteDetails();
                if (prequalification.InvitationFrom == 0 || prequalification.InvitationFrom == null)
                {
                    inviteDetails.InvitationSentBy = Resources.Resources.PQBanner_Label_WithoutInvitation;
                }
                else if (prequalification.InvitationFrom == 1)
                {
                    inviteDetails.InvitationSentBy = Resources.Resources.PQBanner_Label_InvitationFromFirstVerify;
                }
                else if (prequalification.InvitationFrom == 2)
                {
                    inviteDetails.InvitationSentBy = Resources.Resources.PQBanner_Label_InvitationFrom;
                    inviteDetails.InvitationSentByUser = prequalification.InvitedByUser.Contacts.FirstOrDefault().LastName + ", " + prequalification.InvitedByUser.Contacts.FirstOrDefault().FirstName;
                }
                return inviteDetails;
            }

        }
        public LocalVendorInviteDetails GetPQInviteDetails(long pqId)
        {
            using (var entity = new EFDbContext())
            {
                var prequalification = entity.Prequalification.Find(pqId);
                LocalVendorInviteDetails inviteDetails = new LocalVendorInviteDetails();
                if (prequalification.InvitationFrom == null)
                {

                    var VendorEmails = prequalification.Vendor.SystemUsersOrganizations.Select(rec => rec.SystemUsers.UserName).ToList();
                    var InvitationDetails = entity.VendorInviteDetails.FirstOrDefault(rec => VendorEmails.Contains(rec.VendorEmail) && rec.ClientId == prequalification.ClientId);

                    if (InvitationDetails == null)

                        inviteDetails.InvitationSentBy = Resources.Resources.PQBanner_Label_WithoutInvitation;
                    else if (InvitationDetails.InvitationSentFrom == 0)
                        try
                        {
                            inviteDetails.InvitationFrom = InvitationDetails.InvitationSentFrom;
                            inviteDetails.InvitationSentByUserId = InvitationDetails.InvitationSentByUser;
                            inviteDetails.InvitationSentBy = Resources.Resources.PQBanner_Label_InvitationFrom;
                            inviteDetails.InvitationSentByUser = InvitationDetails.SystemUsers.Contacts.Any() ? (InvitationDetails.SystemUsers.Contacts.FirstOrDefault().LastName + ", " + InvitationDetails.SystemUsers.Contacts.FirstOrDefault().FirstName) : "";
                        }
                        catch { }
                    else
                    {
                        inviteDetails.InvitationSentBy = Resources.Resources.PQBanner_Label_InvitationFromFirstVerify;
                        inviteDetails.InvitationFrom = InvitationDetails.InvitationSentFrom;
                    }
                }
                else
                {
                    inviteDetails.InvitationFrom = prequalification.InvitationFrom;
                    //if (prequalification.InvitationFrom == 0)
                    //{
                    //    inviteDetails.InvitationSentBy = Resources.Resources.PQBanner_Label_WithoutInvitation;
                    //}
                    //else if (prequalification.InvitationFrom == 1)
                    //{
                    //    inviteDetails.InvitationSentBy = Resources.Resources.PQBanner_Label_InvitationFromFirstVerify;
                    //}
                    //else if (prequalification.InvitationFrom == 2)
                    //{
                    //    inviteDetails.InvitationSentBy = Resources.Resources.PQBanner_Label_InvitationFrom;
                    //    inviteDetails.InvitationSentByUser = prequalification.InvitedByUser.Contacts.FirstOrDefault().LastName + ", " + prequalification.InvitedByUser.Contacts.FirstOrDefault().FirstName;
                    //}
                    if (prequalification.InvitationFrom == 0)
                    {
                        inviteDetails.InvitationSentBy = Resources.Resources.Common_label_None;
                    }

                    else if (prequalification.InvitationFrom == 1)
                    {
                        inviteDetails.InvitationSentBy = Resources.Resources.Common_label_FirstVerify;
                    }
                    else if (prequalification.InvitationFrom == 2)
                    {
                        inviteDetails.InvitationSentByUserId = prequalification.InvitationSentByUser;
                        inviteDetails.InvitationSentBy = Resources.Resources.Common_label_ClientUser;
                        inviteDetails.InvitationSentByUser = prequalification.InvitedByUser.Contacts.Any() ? (prequalification.InvitedByUser.Contacts.FirstOrDefault().LastName + ", " + prequalification.InvitedByUser.Contacts.FirstOrDefault().FirstName) : "";
                    }
                }
                return inviteDetails;
            }
        }
        public LocalVendorInviteDetails AddInvitaionDetailsIntoPq(long ClientId, long VendorId)
        {
            using (var entity = new EFDbContext())
            {
                LocalVendorInviteDetails inviteDetails = new LocalVendorInviteDetails();


                var VendorEmails = entity.SystemUsersOrganizations.Where(r => r.OrganizationId == VendorId).Select(rec => rec.SystemUsers.UserName).ToList();
                var InvitationDetails = entity.VendorInviteDetails.FirstOrDefault(rec => VendorEmails.Contains(rec.VendorEmail) && rec.ClientId == ClientId);
                if (InvitationDetails != null)
                {
                    if (InvitationDetails.InvitationSentFrom == 0)
                    {
                        inviteDetails.InvitationFrom = 2;//invitationfrom 0 is stored as 2 in prequalification table
                        inviteDetails.InvitationSentByUserId = InvitationDetails.InvitationSentByUser;
                    }
                    else
                        inviteDetails.InvitationFrom = InvitationDetails.InvitationSentFrom;
                    inviteDetails.InvitationId = InvitationDetails.InvitationId;
                }

                return inviteDetails;
            }

        }

        public List<KeyValuePair<string, string>> GetInvitationFromOptions(long clientId)
        {
            using (var entity = new EFDbContext())
            {
                KeyValuePair<string, string> withoutInvitation = new KeyValuePair<string, string>(Resources.Resources.PQBanner_Label_WithoutInvitation, "0");
                KeyValuePair<string, string> invitationFromFirstVerify = new KeyValuePair<string, string>("Invitation from  FIRST, VERIFY.", "1");
                var invitationFromOptions = entity.SystemUsersOrganizations.Where(row => row.OrganizationId == clientId).AsEnumerable().ToList()
                                            .Select(rec => new KeyValuePair<string, string>(rec.SystemUsers.Contacts.Any() ? rec.SystemUsers.Contacts.FirstOrDefault().LastName + ", " + rec.SystemUsers.Contacts.FirstOrDefault().FirstName : rec.SystemUsers.Email, rec.UserId.ToString())).ToList();
                invitationFromOptions = invitationFromOptions.OrderBy(rec => rec.Key).ToList();
                invitationFromOptions.Insert(0, withoutInvitation);
                invitationFromOptions.Insert(1, invitationFromFirstVerify);

                return invitationFromOptions;
            }
        }

        public string UpdateInvitationFromInfo(long pqId, string InvitationFrom)
        {
            using (var entity = new EFDbContext())
            {
                var pq = entity.Prequalification.Find(pqId);
                if (InvitationFrom.Equals("0"))
                    pq.InvitationFrom = Convert.ToInt32(InvitationFrom);
                else if (InvitationFrom.Equals("1"))
                    pq.InvitationFrom = Convert.ToInt32(InvitationFrom);

                else
                {
                    pq.InvitationFrom = 2;
                    pq.InvitationSentByUser = Guid.Parse(InvitationFrom);
                }
                entity.Entry(pq).State = EntityState.Modified;
                entity.SaveChanges();

                return "Success";
            }
        }
        public List<LocalPrequalification> GetPrequalificationsforAdminDashboard(int pageNumber, int? type)//Page Number =-10 means All Data.
        {
            using (var entityDB = new EFDbContext())
            {


                var statusId = 16;
                if (type == null || type == 0)
                    statusId = 5;
                if (type == 1)
                    statusId = 7;
                var HasNextQuery = entityDB.Prequalification.Where(m => m.PrequalificationStatus.PrequalificationStatusId == statusId);
                if (type == 2)
                    HasNextQuery.Where(m => !m.PrequalificationSites.Any(r1 => r1.PQSiteStatus != null));
                var HasNext = true;

                if (pageNumber >= 0) HasNext = HasNextQuery.OrderByDescending(rec => rec.PrequalificationSubmit).Skip(pageNumber * 30).Count() > 30; // Rajesh on 6/5/2014;
                var HasPrevious = pageNumber != 0;
                var sql = @"select " +
                          (pageNumber == -10 ? "" : "@HasNext as HasNext,@HasPrevious as HasPrevious,@pageNumber as pageNumber,")
                                              + @" pstatus.IndicationTextClient as IndicationTextClient,
lp.PrequalificationCreate PrequalificationCreate,
lp.PrequalificationFinish PrequalificationFinish,
lp.prequalificationid PrequalificationId,
lp.PrequalificationStart PrequalificationStart,
lp.PrequalificationSubmit PrequalificationSubmit,
vendor.Name as VendorName,
vendor.OrganizationID as VendorID,
client.name as ClientName,
iif(lp.UserIdAsSigner is not null, (select SUBSTRING(firstname, 1, 1) + '' + SUBSTRING(lastname, 1, 1) from contact where userid = lp.UserIdAsSigner),'')as Initials ,
CAST((select count(*) from PrequalificationCompletedSections as pcomplete join TemplateSections as ts on ts.TemplateSectionID = pcomplete.TemplateSectionId where ts.Visible = 1 and ts.SectionName like'%approval' and pcomplete.PrequalificationId = lp.prequalificationid)AS BIT) as isClientApproval,
iif((select count(*) from PrequalificationStatusChangeLog where PrequalificationId = lp.prequalificationid and PrequalificationStatusId = 5) > 1,CAST(1 AS BIT),CAST(0 AS BIT))
as isReSubmitted
from Prequalification as lp
join Organizations as vendor on vendor.OrganizationID = lp.VendorId
join Organizations as client on client.OrganizationID = lp.ClientId
join PrequalificationStatus as pstatus on pstatus.PrequalificationStatusId = lp.PrequalificationStatusId
where lp.PrequalificationStatusId = " + statusId;
                if (type == 2)
                    sql += @" and lp.PrequalificationId not in(
select prequalificationid from
PrequalificationSites where  prequalificationid=lp.PrequalificationId and PQSiteStatus is  not null
)";
                sql += " order by lp.PrequalificationSubmit desc";
                if (pageNumber != -10)
                    sql += " OFFSET @pageNumber*30 ROWS FETCH NEXT 30 ROWS ONLY";
                var resultList = entityDB.Database.SqlQuery<LocalPrequalification>(sql, new SqlParameter("@pageNumber", pageNumber),
                       new SqlParameter("@HasNext", HasNext), new SqlParameter("@HasPrevious", HasPrevious));

                //ViewBag.VBPrequalificationList = resultList.ToList();

                return resultList.ToList();
            }
        }


        public List<DocumentsList> GetBUDocs(long BuId)
        {
            using (var entity = new EFDbContext())
            {

               return entity.Document.Where(rec => rec.ReferenceRecordID == BuId && rec.ReferenceType == Resources.LocalConstants.BusinessUnit).
                   Select(rec => new DocumentsList { DocumentId = rec.DocumentId, DocumentName = rec.DocumentName, ClientId = rec.OrganizationId }).ToList();
            }
        }

        public long GetPQStatusId(long pqId)
        {
            using (var entity = new EFDbContext())
            {
                return entity.Prequalification.Find(pqId).PrequalificationStatusId;
            }
        }

        public VendorDetailsViewModel GetPQDetails(long pqId)
        {
            using (var entity = new EFDbContext())
            {
                VendorDetailsViewModel pqVendorDetails = new VendorDetailsViewModel();

                Prequalification pqDetails = entity.Prequalification.Find(pqId);
                Organizations vendorOrgDetails = entity.Organizations.Find(pqDetails.VendorId);
                pqVendorDetails.ClientId = pqDetails.ClientId;
                pqVendorDetails.ClientName = pqDetails.Client.Name;
                pqVendorDetails.VendorName = pqDetails.Vendor.Name;
                pqVendorDetails.IsERI = pqDetails.ClientTemplates.Templates.IsERI;
                pqVendorDetails.ClientOrganizationType = pqDetails.Client.OrganizationType;
                pqVendorDetails.PaymentReceived = pqDetails.PaymentReceived;
                pqVendorDetails.PrequalificationCreate = pqDetails.PrequalificationCreate;
                pqVendorDetails.PrequalificationStart = pqDetails.PrequalificationStart;
                pqVendorDetails.PrequalificationFinish = pqDetails.PrequalificationFinish;
                pqVendorDetails.PrequalificationStatusName = pqDetails.PrequalificationStatus.PrequalificationStatusName;
                pqVendorDetails.TemplateID = pqDetails.ClientTemplates.Templates.TemplateID;
                pqVendorDetails.PrequalificationStatusId = pqDetails.PrequalificationStatusId;
                pqVendorDetails.HasVendorDetailLocation = pqDetails.ClientTemplates.Templates.HasVendorDetailLocation;
                pqVendorDetails.vendorContacts = entity.VendorDetails.Where(m => m.PrequalificationId == pqId).Select(r => new VendorContact
                {
                    GroupNumber = r.GroupNumber,
                    FullName = r.FullName,
                    Title = r.Title,
                    EmailAddress = r.EmailAddress,
                    TelephoneNumber = r.TelephoneNumber
                }).Distinct().OrderBy(r => r.GroupNumber).ToList();

                var subsectionid = pqDetails.ClientTemplates.Templates.TemplateSections.Where(r => r.TemplateSectionType == 32).
                        SelectMany(r => r.TemplateSubSections).Where(r => r.Visible == true).Select(r => r.SubSectionID).ToList();


                pqVendorDetails.subSectionNames = entity.Questions.Where(r => r.GroupAsOneRecord != null && r.Visible == true && subsectionid.Contains(r.SubSectionId)).Select(r => new { r.TemplateSubSections.SubSectionName, r.GroupAsOneRecord })
                    .Distinct().OrderBy(r => r.GroupAsOneRecord).Select(r => r.SubSectionName).ToList();

                pqVendorDetails.vendorOrgDetails = new VendorOrgDetails();
                pqVendorDetails.vendorOrgDetails.Address1 = vendorOrgDetails.Address1;
                pqVendorDetails.vendorOrgDetails.Address2 = vendorOrgDetails.Address2;
                pqVendorDetails.vendorOrgDetails.City = vendorOrgDetails.City;
                pqVendorDetails.vendorOrgDetails.State = vendorOrgDetails.State;
                pqVendorDetails.vendorOrgDetails.Zip = vendorOrgDetails.Zip;
                pqVendorDetails.vendorOrgDetails.PhoneNumber = vendorOrgDetails.PhoneNumber;
                pqVendorDetails.vendorOrgDetails.FaxNumber = vendorOrgDetails.FaxNumber;
                pqVendorDetails.vendorOrgDetails.WebsiteURL = vendorOrgDetails.WebsiteURL;
                pqVendorDetails.vendorOrgDetails.TaxID = vendorOrgDetails.TaxID;

                bool pqUserExists = pqDetails.LoggedInUserForPreQualification != null;
                if (pqUserExists)
                //if (pqVendorDetails.pqUserIdLoggedInDetails.LoggedInUserForPreQualification)
                {
                    pqVendorDetails.pqUserIdLoggedInDetails = new LocalVendorDetails();
                    pqVendorDetails.pqUserIdLoggedInDetails.LoggedInUserForPreQualification = pqUserExists;
                    pqVendorDetails.pqUserIdLoggedInDetails.FullName = pqDetails.LoggedInUserForPreQualification.Email;
                    if (pqDetails.LoggedInUserForPreQualification.Contacts != null && pqDetails.LoggedInUserForPreQualification.Contacts.Count() != 0)
                    {
                        var contact = pqDetails.LoggedInUserForPreQualification.Contacts[0];
                        pqVendorDetails.pqUserIdLoggedInDetails.FullName = contact.FullName;
                        pqVendorDetails.pqUserIdLoggedInDetails.ContactTile = contact.ContactTitle;
                    }
                    pqVendorDetails.pqUserIdLoggedInDetails.PrequalificationSubmit = pqDetails.PrequalificationSubmit;
                    pqVendorDetails.pqUserIdLoggedInDetails.Email = pqDetails.LoggedInUserForPreQualification.Email;
                }

                var pqSubmittedDetails = entity.PrequalificationStatusChangeLog.Where(rec => rec.PrequalificationId == pqId && rec.PrequalificationStatusId == 5).OrderByDescending(rec => rec.StatusChangeDate).ToList();
                if (pqSubmittedDetails != null)
                {
                    pqVendorDetails.pqSubmittedDetails = new List<LocalVendorDetails>();
                    foreach (var subdetails in pqSubmittedDetails)
                    {
                        LocalVendorDetails pqSubmittedDetailsFromLog = new LocalVendorDetails();
                        pqSubmittedDetailsFromLog.FullName = subdetails.StatusChangedBySystemUser.Email;
                        if (subdetails.StatusChangedBySystemUser.Contacts != null && subdetails.StatusChangedBySystemUser.Contacts.Count() != 0)
                        {
                            pqSubmittedDetailsFromLog.FullName = subdetails.StatusChangedBySystemUser.Contacts[0].FirstName + " " + subdetails.StatusChangedBySystemUser.Contacts[0].LastName;
                            pqSubmittedDetailsFromLog.ContactTile = subdetails.StatusChangedBySystemUser.Contacts[0].ContactTitle;
                        }
                        pqSubmittedDetailsFromLog.StatusChangeDate = subdetails.StatusChangeDate;
                        pqSubmittedDetailsFromLog.Email = subdetails.StatusChangedBySystemUser.Email;
                        pqVendorDetails.pqSubmittedDetails.Add(pqSubmittedDetailsFromLog);
                    }
                }
                pqVendorDetails.ClientVendorRefId = GetClientVendorReferenceId(pqId, pqDetails.ClientId, pqDetails.VendorId);

                List<long> clientIds = pqDetails.ClientTemplates.Templates.ClientTemplates.Select(r => r.ClientID).ToList();
                pqVendorDetails.ShowScorecard = entity.VendorReviewQuestions.Any(rec => clientIds.Contains(rec.ClientID));

                pqVendorDetails.RecommendedCount = entity.VendorReviewInput
                        .Where(rec => rec.VendorId == pqDetails.VendorId)
                        .SelectMany(rec => rec.VendorReviewInputSection3Ans)
                        .Count(rec => rec.VendorReviewQuestionsForSection3.IsRating.Value && rec.Answer == "1");

                pqVendorDetails.NotRecommendedCount = entity.VendorReviewInput
                        .Where(rec => rec.VendorId == pqDetails.VendorId)
                        .SelectMany(rec => rec.VendorReviewInputSection3Ans)
                        .Count(rec => rec.VendorReviewQuestionsForSection3.IsRating.Value && rec.Answer == "0");

                Templates pqTemplate = pqDetails.ClientTemplates.Templates;

                pqVendorDetails.DefaultTemplateSectionId = pqTemplate.TemplateSections.Where(rec => rec.Visible == true).ToList().OrderBy(rec => rec.DisplayOrder).FirstOrDefault().TemplateSectionID;

                pqVendorDetails.TemplateCode = pqTemplate.TemplateCode;

                PQPaymentInfo pqPaymentInfo = GetPQPaymemtInfo(pqId);

                pqVendorDetails.TemplateHasPaymentSection = pqPaymentInfo.HasPaymentSection;
                pqVendorDetails.PaymentSectionId = pqPaymentInfo.PaymentSectionId;

                //pqVendorDetails.TemplateHasPaymentSection = pqTemplate.TemplateSections.FirstOrDefault(rec => rec.TemplateSectionType == 3) != null;
                //if(pqVendorDetails.TemplateHasPaymentSection)
                //    pqVendorDetails.PaymentSectionId = pqTemplate.TemplateSections.FirstOrDefault(rec => rec.TemplateSectionType == 3).TemplateSectionID;

                return pqVendorDetails;
            }
        }

        public PQPaymentInfo GetPQPaymemtInfo(long pqId)
        {
            using (var entity = new EFDbContext())
            {
                Templates pqTemplate = entity.Prequalification.Find(pqId).ClientTemplates.Templates;
                PQPaymentInfo pqPayment = new PQPaymentInfo();
                pqPayment.HasPaymentSection = pqTemplate.TemplateSections.FirstOrDefault(rec => rec.TemplateSectionType == 3) != null;
                if (pqPayment.HasPaymentSection)
                    pqPayment.PaymentSectionId = pqTemplate.TemplateSections.FirstOrDefault(rec => rec.TemplateSectionType == 3).TemplateSectionID;

                return pqPayment;
            }
        }

        private string GetClientVendorReferenceId(long pqId, long clientId, long vendorId)
        {
            using (var entity = new EFDbContext())
            {
                string ClientVendorRefId = "";

                string ClientRefval = "";
                long? questionId = GetTemplateVendorNumberAssignedQuestion(pqId);
                try
                {
                    long questionColumnId = entity.Questions.Find(questionId).QuestionColumnDetails.FirstOrDefault().QuestionColumnId;

                    ClientRefval = entity.PrequalificationUserInput.FirstOrDefault(rec => rec.PreQualificationId == pqId && rec.QuestionColumnId == questionColumnId).UserInput;
                }
                catch
                {
                    ClientVendorRefId = "";
                }
                if (ClientRefval == "")
                {
                    ClientVendorReferenceIds clientVendorRef = entity.ClientVendorReferenceIds.FirstOrDefault(rec => rec.VendorId == vendorId && rec.ClientId == clientId);
                    if (clientVendorRef != null)
                    {
                        ClientVendorRefId = clientVendorRef.ClientVendorReferenceId;
                    }
                }
                else
                {
                    ClientVendorRefId = ClientRefval;
                }
                return ClientVendorRefId;
            }
        }

        public long? GetTemplateVendorNumberAssignedQuestion(long pqId)
        {
            using (var entity = new EFDbContext())
            {
                var pq = entity.Prequalification.Find(pqId);
                string questionText = Resources.LocalConstants.Vendor_Number_Assigned;
                Questions question = entity.Questions.FirstOrDefault(r => r.TemplateSubSections.TemplateSections.TemplateID == pq.ClientTemplates.TemplateID && r.QuestionText.ToLower().Trim().Equals(questionText));
                if (question != null)
                    return question.QuestionID;
                else
                    return null;
            }
        }

        public decimal GetVendorReviewAverageSpecificToClient(long vendorId, long clientId)
        {
            using (var entity = new EFDbContext())
            {
                decimal reviewAverage = 0;
                var userReview = entity.VendorReviewInput.Where(rec => rec.VendorId == vendorId && rec.ClientId == clientId).ToList();
                if (userReview != null && userReview.Any())
                {
                    reviewAverage = Math.Round(userReview.Average(rec => rec.UserRating), 2);
                }
                return reviewAverage;
            }
        }
        public List<QuestionAndAnswers> GetVendorDetailsQSections(long pqId)
        {
            using (var entity = new EFDbContext())
            {
                var qAndA = new List<QuestionAndAnswers>();
                var templateid = entity.Prequalification.Find(pqId).ClientTemplates.TemplateID;
                var Questions = entity.VendorDetailQuestions.Where(r => r.TemplateId == templateid && r.Type == 0).Select(r => r.Questions).OrderBy(r => r.QuestionID).Select(r => new
                {
                    QText = r.QuestionText,
                    QCids = r.QuestionColumnDetails.Select(r1 => r1.QuestionColumnId)

                }).ToList();
                foreach (var question in Questions)
                {
                    var a = new QuestionAndAnswers();
                    a.QuestionName = question.QText;
                    a.Answers = new List<string>();
                    var QCols = entity.PrequalificationUserInput.Where(r => r.PreQualificationId == pqId && question.QCids.Contains(r.QuestionColumnId)).Select(r => new { r.UserInput, r.QuestionColumnDetails.ColumnValues, r.QuestionColumnDetails.TableReference, r.QuestionColumnDetails.TableReferenceField, r.QuestionColumnDetails.TableRefFieldValueDisplay, r.QuestionColumnDetails.QuestionControlTypeId }).ToList();
                    foreach (var Qcol in QCols)
                    {
                        if (string.IsNullOrEmpty(Qcol.TableReference))
                        {
                            if (Qcol.QuestionControlTypeId == 8)
                            {
                                if (Qcol.UserInput.Equals("true", StringComparison.CurrentCultureIgnoreCase))
                                    a.Answers.Add(Qcol.ColumnValues);
                            }
                            else
                                a.Answers.Add(Qcol.UserInput);
                        }
                        else
                        {
                            var helper = new PrequalificationHelper();
                            a.Answers.Add(helper.GetUserInputAnswer(Qcol.TableReference, Qcol.TableReferenceField, Qcol.TableRefFieldValueDisplay, Qcol.UserInput));
                        }
                    }
                    qAndA.Add(a);
                }
                return qAndA;
            }
        }

        public string GetPQStatusName(long pqId)
        {
            using (var entity = new EFDbContext())
            {
                return entity.Prequalification.Find(pqId).PrequalificationStatus.PrequalificationStatusName;
            }
        }
        public bool CheckEvalationExpiration(Guid UserId,long VendorId,long ClientId)
        {
            using (var context = new EFDbContext())
            {
                var Expiryperiod = context.Organizations.FirstOrDefault(r => r.OrganizationID == ClientId).ContractorEvaluationExpirationPeriod;
                if (Expiryperiod == null) return false;
                else
                {
                  var HasReview= context.VendorReviewInput.FirstOrDefault(r => r.VendorId == VendorId && r.ClientId == ClientId && r.InputUserId == UserId);
                    return HasReview !=null&& HasReview.ReviewDate!=null&& DateTime.Now<HasReview.ReviewDate.Value.AddYears(Expiryperiod.Value)? true:false;

                }
            }
        }

        public List<QuestionAndAnswers> GetVendorDetailERIQSections(long pqId)
        {
            using (var entity = new EFDbContext())
            {
                var qAndA = new List<QuestionAndAnswers>();

                var templateid = entity.Prequalification.Find(pqId).ClientTemplates.TemplateID;
                var Questions = entity.VendorDetailQuestions.Where(r => r.TemplateId == templateid && r.Type == 1).Select(r => r.Questions).OrderBy(r => r.QuestionID).Select(r => new
                {
                    QText = r.QuestionText,
                    QCids = r.QuestionColumnDetails.Select(r1 => r1.QuestionColumnId),
                    Qid = r.QuestionID

                }).ToList();

                foreach (var question in Questions)
                {
                    var a = new QuestionAndAnswers();
                    a.QuestionName = question.QText;
                    a.Answers = new List<string>();
                    var max = entity.Questions.Find(question.Qid);//Where(r => r.QuestionText == a.QuestionName).Select(r => r.QuestionID);
                    var MaxValue = entity.VendorDetailQuestions.FirstOrDefault(r => r.QuestionId == max.QuestionID && r.Type == 1);
                    a.Value = MaxValue.Value;
                    a.IsMax = MaxValue.IsMax;
                    var QCols = entity.PrequalificationUserInput.Where(r => r.PreQualificationId == pqId && question.QCids.Contains(r.QuestionColumnId)).Take(3).Select(r => new
                    {
                        r.UserInput,
                        r.QuestionColumnDetails.ColumnValues,
                        r.QuestionColumnDetails.TableReference,
                        r.QuestionColumnDetails.TableReferenceField,
                        r.QuestionColumnDetails.TableRefFieldValueDisplay,
                        r.QuestionColumnDetails.QuestionControlTypeId
                    }).ToList();
                    foreach (var Qcol in QCols)
                    {
                        if (string.IsNullOrEmpty(Qcol.TableReference))
                        {
                            if (Qcol.QuestionControlTypeId == 8)
                            {
                                if (Qcol.UserInput.Equals("true", StringComparison.CurrentCultureIgnoreCase))
                                    a.Answers.Add(Qcol.ColumnValues);
                            }
                            else
                                a.Answers.Add(Qcol.UserInput);
                        }
                        else
                        {
                            var helper = new PrequalificationHelper();
                            a.Answers.Add(helper.GetUserInputAnswer(Qcol.TableReference, Qcol.TableReferenceField, Qcol.TableRefFieldValueDisplay, Qcol.UserInput));
                        }
                    }

                    qAndA.Add(a);

                }
                return qAndA;
            }
        }

        public string GetNotificationComments(long EmailRecordId)
        {
            using (var entity = new EFDbContext())
            {
                return entity.OrganizationsNotificationsEmailLogs.Find(EmailRecordId).Comments;
            }
        }

        public void AddorUpdateComment(LocalComments comments, Guid userId)
        {
            using (var entity = new EFDbContext())
            {
                var pq = entity.Prequalification.Find(comments.prequalificationId);
                var MaxNotificationNo = 0;
                OrganizationsNotificationsEmailLogs pqEmailLog = entity.OrganizationsNotificationsEmailLogs.FirstOrDefault(record => record.PrequalificationId == pq.PrequalificationId && record.NotificationType == 1);
                var currentCommentRecord = entity.OrganizationsNotificationsEmailLogs.Find(comments.EmailRecordId);
                if (pqEmailLog != null)
                    MaxNotificationNo = entity.OrganizationsNotificationsEmailLogs.Where(record => record.PrequalificationId == pq.PrequalificationId && record.NotificationType == 1).Max(record => record.NotificationNo);
                long NotificationId = 0;
                var commentNotificationSentRecord = entity.OrganizationsNotificationsSent.FirstOrDefault(record => record.SetupType == 2 && record.ClientId == pq.ClientId);
                if (commentNotificationSentRecord == null)
                {
                    OrganizationsNotificationsSent addNotificationForComment = new OrganizationsNotificationsSent();
                    addNotificationForComment.ClientId = pq.ClientId;
                    addNotificationForComment.SetupType = 2;
                    entity.Entry(addNotificationForComment).State = EntityState.Added;
                    entity.SaveChanges();
                    NotificationId = entity.OrganizationsNotificationsSent.FirstOrDefault(record => record.SetupType == 2 && record.ClientId == pq.ClientId).NotificationId;
                }
                else
                {
                    NotificationId = commentNotificationSentRecord.NotificationId;
                }
                if (currentCommentRecord == null)
                {
                    currentCommentRecord = new OrganizationsNotificationsEmailLogs();

                    currentCommentRecord.NotificationId = NotificationId;
                    currentCommentRecord.NotificationType = 1;
                    currentCommentRecord.NotificationNo = MaxNotificationNo + 1;
                    currentCommentRecord.VendorId = pq.VendorId;
                    currentCommentRecord.PrequalificationId = pq.PrequalificationId;
                    currentCommentRecord.EmailSentDateTime = DateTime.Now;
                    currentCommentRecord.ExpiringDate = pq.PrequalificationFinish;
                    currentCommentRecord.Comments = comments.comment;
                    currentCommentRecord.MailSentBy = userId;
                    entity.Entry(currentCommentRecord).State = EntityState.Added;
                    entity.SaveChanges();
                }
                else
                {
                    currentCommentRecord.Comments = comments.comment;
                    currentCommentRecord.CommentsUpdateDateTime = DateTime.Now;

                    entity.Entry(currentCommentRecord).State = EntityState.Modified;
                    entity.SaveChanges();
                }
            }
        }

        public void DeleteComment(long emailRecordId)
        {
            using (var entity = new EFDbContext())
            {
                var currentCommentrecord = entity.OrganizationsNotificationsEmailLogs.Find(emailRecordId);
                entity.OrganizationsNotificationsEmailLogs.Remove(currentCommentrecord);
                entity.SaveChanges();
            }
        }

        public List<long> GetClientNotificationTypes(long pqId)
        {
            using (var entity = new EFDbContext())
            {
                long clientId = entity.Prequalification.Find(pqId).ClientId;

                return entity.OrganizationsNotificationsSent.Where(m => (m.SetupType == 1 || m.SetupType == 2 || m.SetupType == 5 || m.SetupType == 6) && m.ClientId == clientId).Select(m => m.NotificationId).ToList();
            }
        }

        public IEnumerable<PrequalificationCommentsViewModel> GetPQComments(long pqId)
        {
            using (var entity = new EFDbContext())
            {
                List<long> varOrgNotificationSent = GetClientNotificationTypes(pqId);

                List<PrequalificationCommentsViewModel> notifications =

                    entity.OrganizationsNotificationsEmailLogs.Where(
                            rec =>
                                rec.PrequalificationId == pqId &&
                                varOrgNotificationSent.Contains(rec.NotificationId)).ToList()
                        .Select(
                            r =>
                                new PrequalificationCommentsViewModel()
                                {
                                    EmailSentDateTime = r.EmailSentDateTime,
                                    Name =
                                        r.SystemUsers.Contacts.Any()
                                            ? r.SystemUsers.Contacts.FirstOrDefault().FirstName + ' ' +
                                              r.SystemUsers.Contacts.FirstOrDefault().LastName
                                            : r.SystemUsers.Email,
                                    Comments = r.Comments,
                                    SetupType = r.OrganizationsNotificationsSent.SetupType,
                                    NotificationType = r.NotificationType,
                                    PrequalificationId = r.PrequalificationId,
                                    EmailRecordId = r.EmailRecordId,
                                    UserEmail = r.SystemUsers.Email,
                                    UserContact = r.SystemUsers.Contacts.Any()
                                            ? r.SystemUsers.Contacts.FirstOrDefault().PhoneNumber
                                            : String.Empty
                                }).ToList();

                //List<PrequalificationCommentsViewModel> notifications = (from emaillogs in entity.OrganizationsNotificationsEmailLogs.ToList()
                //                                                         join ons in entity.OrganizationsNotificationsSent on emaillogs.NotificationId equals ons.NotificationId
                //                                                         join sysUsers in entity.SystemUsers on emaillogs.MailSentBy equals sysUsers.UserId
                //                                                         where emaillogs.PrequalificationId == pqId && varOrgNotificationSent.Contains(emaillogs.OrganizationsNotificationsSent.NotificationId)
                //                                                         select new PrequalificationCommentsViewModel
                //                                                         {
                //                                                             EmailSentDateTime = emaillogs.EmailSentDateTime,
                //                                                             Name = emaillogs.SystemUsers.Contacts.Any()
                //                                                                    ? emaillogs.SystemUsers.Contacts.FirstOrDefault().FirstName + ' ' +
                //                                                                      emaillogs.SystemUsers.Contacts.FirstOrDefault().LastName
                //                                                                    : emaillogs.SystemUsers.Email,
                //                                                             Comments = emaillogs.Comments,
                //                                                             SetupType = emaillogs.OrganizationsNotificationsSent.SetupType,
                //                                                             NotificationType = emaillogs.NotificationType,
                //                                                             PrequalificationId = emaillogs.PrequalificationId,
                //                                                             EmailRecordId = emaillogs.EmailRecordId,
                //                                                             UserEmail = emaillogs.SystemUsers.Email,
                //                                                             UserContact = emaillogs.SystemUsers.Contacts.Any()
                //                                                                                         ? emaillogs.SystemUsers.Contacts.FirstOrDefault().PhoneNumber
                //                                                                                         : String.Empty
                //                                                         }).ToList();

                List<PrequalificationCommentsViewModel> expiredInvitaions =
                    entity.VendorInviteDetails.Where(r => r.PQId == pqId).ToList()
                        .Select(r => new PrequalificationCommentsViewModel()
                        {
                            EmailSentDateTime = r.InvitationDate,
                            Name = r.SystemUsers.Contacts.Any()
                                ? r.SystemUsers.Contacts.FirstOrDefault().LastName + ' ' +
                                  r.SystemUsers.Contacts.FirstOrDefault().FirstName
                                : r.SystemUsers.Email,
                            Comments = "Invitation sent to renew prequalification.",
                            SetupType = -1,
                            NotificationType = -1,
                            PrequalificationId = r.PQId,
                            EmailRecordId = -1,
                            UserEmail = r.SystemUsers.Email,
                            UserContact = r.SystemUsers.Contacts.Any()
                                    ? r.SystemUsers.Contacts.FirstOrDefault().PhoneNumber
                                    : String.Empty
                        }).GroupBy(r => new { r.PrequalificationId, r.EmailSentDateTime.Date }).Select(r => r.FirstOrDefault()).ToList();

                List<PrequalificationCommentsViewModel> AgreementLogs =
                    entity.AgreementSubSectionLog.Where(r => r.PQId == pqId).ToList()
                        .Select(r => new PrequalificationCommentsViewModel()
                        {
                            EmailSentDateTime = r.SentDate,
                            Name = r.SystemUsers.Contacts.Any()
                                ? r.SystemUsers.Contacts.FirstOrDefault().LastName + ' ' +
                                  r.SystemUsers.Contacts.FirstOrDefault().FirstName
                                : r.SystemUsers.Email,
                            Comments = r.AgreementSubSectionConfiguration.EmailTemplates.CommentText,
                            SetupType = -1,
                            NotificationType = -1,
                            PrequalificationId = r.PQId,
                            EmailRecordId = -1,
                            UserEmail = r.SystemUsers.Email,
                            UserContact = r.SystemUsers.Contacts.Any()
                                    ? r.SystemUsers.Contacts.FirstOrDefault().PhoneNumber
                                    : String.Empty
                        }).ToList();

                List<PrequalificationCommentsViewModel> VendorEmailLog =
                    entity.VendorEmailLog.Where(r => r.PQId == pqId && r.EmailStatus).ToList()
                        .Select(r => new PrequalificationCommentsViewModel()
                        {
                            EmailSentDateTime = r.SendDate,
                            Name = r.SystemUsers.Contacts.Any()
                                ? r.SystemUsers.Contacts.FirstOrDefault().LastName + ' ' +
                                  r.SystemUsers.Contacts.FirstOrDefault().FirstName
                                : r.SystemUsers.Email,
                            Comments = "Email message sent by client " + r.Prequalification.Client.Name,
                            SetupType = -1,
                            NotificationType = -1,
                            PrequalificationId = r.PQId,
                            EmailRecordId = -1,
                            UserEmail = r.SystemUsers.Email,
                            UserContact = r.SystemUsers.Contacts.Any()
                                ? r.SystemUsers.Contacts.FirstOrDefault().PhoneNumber
                                : String.Empty
                        }).ToList();

                return notifications.Union(VendorEmailLog).Union(expiredInvitaions).Union(AgreementLogs).OrderByDescending(r => r.EmailSentDateTime);
            }
        }

        public List<TemplateSections> GetTemplateSections(Guid templateId)
        {
            using (var entity = new EFDbContext())
            {
                return entity.TemplateSections.Where(r => r.TemplateID == templateId).Where(rec => rec.Visible == true).OrderBy(x => x.DisplayOrder).ToList();
            }
        }

        public int? GetTemplateSectionType(long secId)
        {
            using (var entity = new EFDbContext())
            {
                return entity.TemplateSections.Find(secId).TemplateSectionType;
            }
        }

        public StatusBannerColor GetPQPreviousStatusAndBannerColor(long pqId, long pqStatusId)
        {
            using (var entity = new EFDbContext())
            {
                StatusBannerColor previousStatusAndBannerColor = new StatusBannerColor();

                var previousPQStatusRecords = entity.PrequalificationStatusChangeLog.Where(rec => rec.PrequalificationId == pqId && rec.PrequalificationStatusId != pqStatusId).OrderByDescending(record => record.PrequalStatusLogId).ToList();
                if (previousPQStatusRecords != null && previousPQStatusRecords.Any())
                {
                    PrequalificationStatusChangeLog pqStatusChangeRec = previousPQStatusRecords.FirstOrDefault();
                    previousStatusAndBannerColor.BannerColor = pqStatusChangeRec.PrequalificationStatus.BannerColor;
                    previousStatusAndBannerColor.StatusName = pqStatusChangeRec.PrequalificationStatus.PrequalificationStatusName;
                }
                return previousStatusAndBannerColor;
            }
        }

        public string GetPQStatusBannerColor(long pqStatusId, string role, int? mode, string statusName, string screenName)
        {
            using (var entity = new EFDbContext())
            {
                string statusColor = "";
                if (role.ToString().Equals(LocalConstants.Admin) || (role.ToString().Equals(LocalConstants.Client) && (mode == 1 || screenName.ToString().ToLower().Equals(LocalConstants.Employees))) || (role.ToString().Equals(LocalConstants.Vendor) && mode == 1))
                {
                    statusColor = entity.PrequalificationStatus.Find(pqStatusId).BannerColor;
                }
                else if ((mode == null || mode == 0 && role.ToString().Equals(LocalConstants.Client)) || (mode == null || mode == 0 && role.ToString().Equals(LocalConstants.Vendor)))
                {
                    statusColor = LocalConstants.PQ_DEFAULT_BG_COLOR;
                }

                bool siteStatus = statusName == "Multiple";
                try
                {
                    if (siteStatus)
                        statusColor = "#1475ad";
                    else
                        statusColor = entity.PrequalificationStatus.FirstOrDefault(r => r.PrequalificationStatusName == statusName).BannerColor;
                }
                catch (Exception e) { }
                return statusColor;
            }
        }

        public List<VendorPQsSpecificToClient> GetVendorPqsSpecificToClient(long clientId, long vendorId)
        {
            using (var entity = new EFDbContext())
            {
                return entity.Prequalification.Where(rec => rec.ClientId == clientId && rec.VendorId == vendorId).OrderByDescending(rec => rec.PrequalificationStart).
                        Select(r => new VendorPQsSpecificToClient
                        {
                            PrequalificationId = r.PrequalificationId,
                            PrequalificationStart = r.PrequalificationStart,
                            ClientName = r.Client.Name,
                            PrequalificationFinish = r.PrequalificationFinish,
                            PrequalificationStatusId = r.PrequalificationStatusId
                        }).ToList();
            }
        }

        public List<LocalTemplatesSideMenuModel> GetPQSideMenusForVendorDetails(long pqId, ref long TemplateSectionID, int? mode, Guid userId, Guid templateId, string role)
        {
            using (var entity = new EFDbContext())
            {
                Prequalification preQualificationDetails = entity.Prequalification.Find(pqId);
                long templateSectionId = TemplateSectionID;

                List<LocalTemplatesSideMenuModel> sideMenus = new List<LocalTemplatesSideMenuModel>();
                List<TemplateSections> sideMenuSections = GetTemplateSections(preQualificationDetails.ClientTemplates.TemplateID);
                List<long> templateSections = sideMenuSections.Select(r => r.TemplateSectionID).ToList();

                List<TemplateSectionsPermission> allVisibleTemplateSectionsFromPermissions = entity.TemplateSectionsPermission.Where(rec => templateSections.Contains(rec.TemplateSectionID)).ToList();

                List<long> allVisibleTemplateSectionsForAdmin = allVisibleTemplateSectionsFromPermissions.Where(rec => rec.VisibleTo != 4).Select(rec => rec.TemplateSectionID).ToList();

                if (role.Equals(LocalConstants.Client) || role.Equals(LocalConstants.Admin))
                {
                    allVisibleTemplateSectionsForAdmin.AddRange(allVisibleTemplateSectionsFromPermissions.Where(rec => rec.VisibleTo == 4).Select(rec => rec.TemplateSectionID).ToList());
                }
                else//Vendor have the access of access level 2 section.
                {
                    allVisibleTemplateSectionsForAdmin.AddRange(allVisibleTemplateSectionsFromPermissions.Where(rec => rec.VisibleTo == LocalConstants.SectionPermissionType.RESTRICTED_ACCESS_2).Select(rec => rec.TemplateSectionID).ToList());
                }
                sideMenuSections = sideMenuSections.Where(rec => allVisibleTemplateSectionsForAdmin.Contains(rec.TemplateSectionID)).ToList();

                if (role.Equals(LocalConstants.Client) || role.Equals(LocalConstants.SuperClient))
                {
                    long[] prequalificationStatus = { };//Requested on FV-1012
                    int status = Array.IndexOf(prequalificationStatus, preQualificationDetails.PrequalificationStatusId);
                    if (status != -1)
                        sideMenuSections = sideMenuSections.Where(r => r.VisibleToClient == true).OrderBy(x => x.DisplayOrder).ToList();

                    List<long> allVisibleTemplateSectionsForClient = allVisibleTemplateSectionsFromPermissions.Where(rec => rec.VisibleTo == 2 || rec.VisibleTo == 3 || rec.VisibleTo == 4).Select(rec => rec.TemplateSectionID).ToList();

                    //allVisibleTemplateSectionsForClient.AddRange(entity.TemplateSectionsPermission.Where(rec => rec.VisibleTo == 4).Select(rec => rec.TemplateSectionID).ToList());
                    var hasAccess2 = FVGen3.BusinessLogic.RolesHelper.IsUserInRole(userId, LocalConstants.RESTRICTED_ACCESS_2_VIEW_ID, "read", preQualificationDetails.ClientId, preQualificationDetails.VendorId) || role.Equals(LocalConstants.SuperAdmin);
                    if (hasAccess2)
                        allVisibleTemplateSectionsForClient.AddRange(allVisibleTemplateSectionsFromPermissions.Where(rec => rec.VisibleTo == LocalConstants.SectionPermissionType.RESTRICTED_ACCESS_2).Select(rec => rec.TemplateSectionID).ToList());

                    sideMenuSections = sideMenuSections.Where(rec => allVisibleTemplateSectionsForClient.Contains(rec.TemplateSectionID)).ToList();
                    try
                    {
                        if (sideMenuSections.FirstOrDefault(rec => rec.TemplateSectionID == templateSectionId) == null)
                            TemplateSectionID = sideMenuSections.FirstOrDefault().TemplateSectionID;
                    }
                    catch
                    {
                        TemplateSectionID = -1;
                    }

                }
                var initial = true;
                // Kiran on 6/24/2014
                if (role != LocalConstants.Vendor)
                {
                    LocalTemplatesSideMenuModel vendorDetailsMenu = new LocalTemplatesSideMenuModel("Vendor Details", "VendorDetails", "AdminVendors", false, "SelectedVendorId=" + preQualificationDetails.VendorId + "&PreQualificationId=" + pqId + "&TemplateSectionID=-1" + "&mode=" + mode);
                    vendorDetailsMenu.isVisible = true;
                    vendorDetailsMenu.isSectionCompleted = null;
                    sideMenus.Add(vendorDetailsMenu);
                    // Ends<<<
                }
                foreach (TemplateSections section in sideMenuSections)
                {
                    if (initial && role == LocalConstants.Admin && mode != 1)//Rajesh On 8-31-2013 
                    {
                        LocalTemplatesSideMenuModel menu1 = new LocalTemplatesSideMenuModel("Change Status", "ChangeStatus", "Prequalification", false, "templateId=" + section.TemplateID + "&PreQualificationId=" + pqId + "&mode=" + mode);
                        menu1.isVisible = true;
                        menu1.isSectionCompleted = null;
                        sideMenus.Add(menu1);
                        initial = false;
                    }

                    LocalTemplatesSideMenuModel menu = new LocalTemplatesSideMenuModel(section.SectionName, "TemplateSectionsList", "Prequalification", false, "templateId=" + section.TemplateID + "&TemplateSectionID=" + section.TemplateSectionID + "&PreQualificationId=" + pqId + "&mode=" + mode);

                    //Rajesh on 10-01-2013
                    if (role == LocalConstants.Vendor && (preQualificationDetails.PaymentReceived == null || preQualificationDetails.PaymentReceived == false))
                    {
                        int[] userPermissionSections = { 0, 3 };//isprofile and payment
                        if (Array.IndexOf(userPermissionSections, section.TemplateSectionType) == -1)
                            menu.isVendorNoPaymentNoAccess = false;
                    }
                    menu.isSectionCompleted = (entity.PrequalificationCompletedSections.FirstOrDefault(rec => rec.PrequalificationId == pqId && rec.TemplateSectionId == section.TemplateSectionID) != null);//Rajesh on 8/26/2013
                                                                                                                                                                                                             //Rajesh on 1/11/2014
                    menu.isVisible = true;
                    if (section.TemplateSectionType == 34)
                    {
                        if (preQualificationDetails.RequestForMoreInfo == null || preQualificationDetails.RequestForMoreInfo == false)
                            menu.isVisible = false;
                    }
                    if (section.HideFromPrequalification != null && section.HideFromPrequalification == true)
                    {
                        var sectionToDisplay = entity.PrequalificationSectionsToDisplay.FirstOrDefault(
                            rec =>
                                rec.PrequalificationId == preQualificationDetails.PrequalificationId &&
                                rec.TemplateSectionId == section.TemplateSectionID && rec.Visible == true);
                        if (sectionToDisplay == null)
                            menu.isVisible = false;
                    }
                    //Ends<<<
                    //Rajesh on 1/21/2014
                    if (section.TemplateSectionType == 35 && role.Equals(LocalConstants.Vendor))//If Vendor Login Don't Client Data menu
                    {
                        //Do nothing
                    }
                    else
                        sideMenus.Add(menu);
                    //Ends<<<

                }
                bool hasTrainingTemplates = CheckClientHasTrainingTemplates(preQualificationDetails.ClientId, preQualificationDetails.PrequalificationId);
                if (hasTrainingTemplates)
                {
                    if (role == LocalConstants.Admin || role == LocalConstants.Client)
                    {
                        var menu2 = new LocalTemplatesSideMenuModel("Employees", "Employees", "Prequalification", false,
                            "templateId=" + templateId + "&PreQualificationId=" + pqId);
                        menu2.isVisible = true;
                        menu2.isSectionCompleted = null;
                        initial = false;
                        //sideMenus.Add(menu2);
                        if (role == LocalConstants.Admin && mode != 1)
                        {
                            sideMenus.Insert(2, menu2);
                        }
                        else
                        {
                            sideMenus.Insert(1, menu2);
                        }
                    }
                }
                return sideMenus;
            }
        }

        public LocalChangeStatus GetPQStatusInfoForChangeStatus(long pqId, Guid templateId)
        {
            using (var entity = new EFDbContext())
            {
                Prequalification pq = entity.Prequalification.Find(pqId);
                Templates pqTemplate = entity.Templates.Find(templateId);

                LocalChangeStatus pqStatus = new LocalChangeStatus();

                pqStatus.CurrentStatus = pq.PrequalificationStatus.PrequalificationStatusName;
                pqStatus.currentStatusPeriod = pq.PrequalificationStart.ToString("MM/dd/yyyy");

                var hideSectionsToDisplay = entity.PrequalificationSectionsToDisplay.Where(r => r.PrequalificationId == pqId && r.Visible == true).Select(r => r.TemplateSectionId);
                pqStatus.templateId = templateId;
                pqStatus.PrequalificationId = pqId;
                pqStatus.NotCompletedSection = pqTemplate.TemplateSections.Where(rec => rec.Visible == true && (rec.HideFromPrequalification != true || hideSectionsToDisplay.Contains(rec.TemplateSectionID))).OrderBy(x => x.DisplayOrder).ToList();
                pqStatus.pqStatusId = pq.PrequalificationStatusId;

                pqStatus.displayTemplateSections = (from templateSections in
                                                       pqTemplate.TemplateSections.Where(rec => rec.HideFromPrequalification == true)
                                                    select new TemplateSectionsStatus
                                                    {
                                                        TemplateSectionId = templateSections.TemplateSectionID,
                                                        SectionName = templateSections.SectionName,
                                                        Visible = (entity.PrequalificationSectionsToDisplay.FirstOrDefault(rec => rec.PrequalificationId == pq.PrequalificationId && rec.TemplateSectionId == templateSections.TemplateSectionID && rec.Visible == true) != null)
                                                    }).ToList();
                pqStatus.NewPayment = 100;
                pqStatus.RequestForMoreInfo = pq.RequestForMoreInfo == null ? false : (bool)pq.RequestForMoreInfo;

                pqStatus.userIdAsSigner = pq.UserIdAsSigner;
                pqStatus.adminUsers = GetAdminUsers();

                return pqStatus;

            }
        }

        public List<KeyValuePair<string, string>> GetAdminUsers()
        {
            using (var entity = new EFDbContext())
            {
                var adminOrgId = entity.Organizations.FirstOrDefault(record => record.OrganizationType == "Admin").OrganizationID;
                return entity.SystemUsersOrganizations.Where(rec => rec.OrganizationId == adminOrgId && rec.SystemUsers.UserStatus == true).ToList()
                                        .Select(rec => new KeyValuePair<string, string>(rec.SystemUsers.Contacts.Count() != 0 ? rec.SystemUsers.Contacts.FirstOrDefault().FullName : rec.SystemUsers.Email, rec.UserId.ToString())).ToList();
            }
        }

        public List<long> GetPQSectionsToDisplay(long pqId)
        {
            using (var entity = new EFDbContext())
            {
                return entity.PrequalificationSectionsToDisplay.Where(r => r.PrequalificationId == pqId && r.Visible == true).Select(r => r.TemplateSectionId).ToList();
            }
        }

        public List<LocalTemplatesSideMenuModel> GetPQSideMenus(long pqId, int? mode, Guid templateId, string role, string screenName)
        {
            using (var entity = new EFDbContext())
            {
                List<LocalTemplatesSideMenuModel> sideMenus = new List<LocalTemplatesSideMenuModel>();

                var initial = true;
                Prequalification pqDetails = entity.Prequalification.Find(pqId);

                if (!role.Equals(LocalConstants.Vendor))
                {
                    LocalTemplatesSideMenuModel vendorDetailsMenu = new LocalTemplatesSideMenuModel("Vendor Details",
                        "VendorDetails", "AdminVendors", false,
                        "SelectedVendorId=" + pqDetails.VendorId + "&PreQualificationId=" + pqId +
                        "&TemplateSectionID=-1" + "&mode=" + 0);
                    vendorDetailsMenu.isVisible = true;
                    vendorDetailsMenu.isSectionCompleted = null;
                    sideMenus.Add(vendorDetailsMenu);
                }

                List<long> hideSectionsToDisplay = GetPQSectionsToDisplay(pqId);

                foreach (
                    TemplateSections section in
                    GetTemplateSections(templateId).Where(rec => (rec.HideFromPrequalification != true || hideSectionsToDisplay.Contains(rec.TemplateSectionID)))
                        .OrderBy(x => x.DisplayOrder)
                        .ToList())
                {
                    if (initial && role.Equals(LocalConstants.Admin))
                    {
                        LocalTemplatesSideMenuModel menu1 = new LocalTemplatesSideMenuModel("Change Status", "ChangeStatus",
                            "Prequalification", "ChangeStatus".Equals(screenName),
                            "templateId=" + section.TemplateID + "&PreQualificationId=" + pqId);
                        menu1.isVisible = true;
                        menu1.isSectionCompleted = null;
                        initial = false;
                        sideMenus.Add(menu1);
                    }
                    LocalTemplatesSideMenuModel menu = new LocalTemplatesSideMenuModel(section.SectionName,
                        "TemplateSectionsList", "Prequalification", false,
                        "templateId=" + section.TemplateID + "&TemplateSectionID=" + section.TemplateSectionID +
                        "&PreQualificationId=" + pqId);

                    menu.isSectionCompleted =
                    (entity.PrequalificationCompletedSections.FirstOrDefault(
                         rec =>
                             rec.PrequalificationId == pqId &&
                             rec.TemplateSectionId == section.TemplateSectionID) != null); //Rajesh on 8/26/2013
                                                                                           //Rajesh on 1/11/2014
                    menu.isVisible = true;
                    if (section.TemplateSectionType == 34)
                    {
                        if (pqDetails.RequestForMoreInfo == null ||
                            pqDetails.RequestForMoreInfo == false)
                            menu.isVisible = false;
                    }
                    if (section.HideFromPrequalification != null && section.HideFromPrequalification == true)
                    {
                        var sectionToDisplay = entity.PrequalificationSectionsToDisplay.FirstOrDefault(
                            rec =>
                                rec.PrequalificationId == pqId &&
                                rec.TemplateSectionId == section.TemplateSectionID && rec.Visible == true);
                        if (sectionToDisplay == null)
                            menu.isVisible = false;
                    }//Ends<<<
                     //Rajesh on 1/21/2014
                    if (section.TemplateSectionType == 35 && role.Equals(LocalConstants.Vendor))//If Vendor Login Don't Client Data menu
                    {
                        //Do nothing
                    }
                    else
                        sideMenus.Add(menu);
                    //Ends<<<
                }
                bool hasTrainingTemplates = CheckClientHasTrainingTemplates(pqDetails.ClientId, pqDetails.PrequalificationId);
                if (hasTrainingTemplates)
                {
                    if (role.Equals(LocalConstants.Admin) || role.Equals(LocalConstants.Client))
                    {
                        var menu2 = new LocalTemplatesSideMenuModel("Employees", "Employees", "Prequalification",
                            "Employees".Equals(screenName),
                            "templateId=" + templateId + "&PreQualificationId=" + pqId);
                        menu2.isVisible = true;
                        menu2.isSectionCompleted = null;
                        initial = false;

                        if (role.Equals(LocalConstants.Admin))
                        {
                            sideMenus.Insert(2, menu2);
                        }
                        else
                        {
                            sideMenus.Insert(1, menu2);
                        }
                    }
                }
                return sideMenus;
            }
        }

        public VendorDetailsViewModel GetPQCommonData(long pqId)
        {
            using (var entity = new EFDbContext())
            {
                Prequalification pq = entity.Prequalification.Find(pqId);
                Templates pqTemplate = pq.ClientTemplates.Templates;

                VendorDetailsViewModel pqVendorDetails = new VendorDetailsViewModel();

                pqVendorDetails.ClientId = pq.ClientId;
                pqVendorDetails.VendorId = pq.VendorId;
                pqVendorDetails.ClientName = pq.Client.Name;
                pqVendorDetails.VendorName = pq.Vendor.Name;
                pqVendorDetails.TemplateCode = pqTemplate.TemplateCode;
                pqVendorDetails.IsERI = pqTemplate.IsERI;
                pqVendorDetails.ClientOrganizationType = pq.Client.OrganizationType;
                pqVendorDetails.PrequalificationStart = pq.PrequalificationStart;
                pqVendorDetails.PrequalificationFinish = pq.PrequalificationFinish;
                pqVendorDetails.PrequalificationStatusId = pq.PrequalificationStatusId;
                pqVendorDetails.PrequalificationStatusName = pq.PrequalificationStatus.PrequalificationStatusName;
                pqVendorDetails.ClientTemplateID = pq.ClientTemplateId;
                pqVendorDetails.TemplateID = pqTemplate.TemplateID;
                pqVendorDetails.Locked = pq.locked;
                pqVendorDetails.RequestForMoreInfo = pq.RequestForMoreInfo;
                pqVendorDetails.PaymentReceived = pq.PaymentReceived;
                pqVendorDetails.UserIdAsCompleter = pq.UserIdAsCompleter;

                PQPaymentInfo pqPaymentInfo = GetPQPaymemtInfo(pqId);

                pqVendorDetails.TemplateHasPaymentSection = pqPaymentInfo.HasPaymentSection;

                return pqVendorDetails;
            }
        }

        public void PostPqStatusChange(LocalChangeStatus form, string userId)
        {
            using (var entity = new EFDbContext())
            {
                Prequalification pq = entity.Prequalification.Find(form.PrequalificationId);

                if (!(pq.PrequalificationStatusId == Convert.ToInt64(form.NewStatus) && pq.PrequalificationStatusId == 7))
                {
                    pq.PrequalificationStatusId = Convert.ToInt64(form.NewStatus);
                    if (Convert.ToInt64(form.NewStatus) != 10)
                    {
                        pq.PrequalificationStart = Convert.ToDateTime(form.NewPeriodStart);
                        if (Convert.ToInt64(form.NewStatus) != 16)
                        {
                            pq.UserIdAsSigner = new Guid(form.adminUserId);
                            pq.RequestForMoreInfo = form.RequestForMoreInfo;
                        }

                        if (form.PeriodLengthUnit.ToLower().Equals("d"))
                            pq.PrequalificationFinish =
                                Convert.ToDateTime(form.NewPeriodStart).AddDays(form.PeriodLength);
                        else if (form.PeriodLengthUnit.ToLower().Equals("m"))
                            pq.PrequalificationFinish =
                                Convert.ToDateTime(form.NewPeriodStart).AddMonths(form.PeriodLength);
                        else if (form.PeriodLengthUnit.ToLower().Equals("y"))
                            pq.PrequalificationFinish =
                                Convert.ToDateTime(form.NewPeriodStart).AddYears(form.PeriodLength);
                    }

                    PrequalificationStatusChangeLog newStatusChangeLog = new PrequalificationStatusChangeLog();
                    newStatusChangeLog.PrequalificationId = form.PrequalificationId;
                    newStatusChangeLog.PrequalificationStatusId = Convert.ToInt64(form.NewStatus);

                    if (Convert.ToInt64(form.NewStatus) != 16 && Convert.ToInt64(form.NewStatus) != 10)
                        newStatusChangeLog.StatusChangeDate =
                            Convert.ToDateTime(form.NewStatusChanged);
                    else
                        newStatusChangeLog.StatusChangeDate = DateTime.Now;
                    newStatusChangeLog.StatusChangedByUser = new Guid(userId);
                    newStatusChangeLog.PrequalificationStart = pq.PrequalificationStart;
                    newStatusChangeLog.PrequalificationFinish = pq.PrequalificationFinish;
                    newStatusChangeLog.CreatedDate = DateTime.Now;
                    entity.PrequalificationStatusChangeLog.Add(newStatusChangeLog);
                    entity.SaveChanges();

                    entity.Entry(pq).State = EntityState.Modified;
                }

                bool removeFinalizeAndSubmitSection = form.NewStatus == "7" ? true : false;

                for (var i = 0; i < form.NotCompletedSection.Count; i++)
                {
                    if (!form.NotCompletedSection_Status[i]) continue;
                    var sectionId = form.NotCompletedSection[i].TemplateSectionID;
                    var pendingSection =
                        entity.PrequalificationCompletedSections.FirstOrDefault(
                            rec =>
                                rec.PrequalificationId == form.PrequalificationId && rec.TemplateSectionId == sectionId);
                    if (pendingSection != null)
                        entity.PrequalificationCompletedSections.Remove(pendingSection);
                }
                //if (form.displayTemplateSections != null)
                //{
                //    string result = HideOrUnhidePrequalificationSections(form,
                //        pq);
                //}

                if (removeFinalizeAndSubmitSection)
                {
                    long sectionForFinalizeAndSubmit = -1;
                    try
                    {
                        TemplateSections finalizeAndSubmitSection =
                            pq.ClientTemplates.Templates.TemplateSections.FirstOrDefault(
                                rec => rec.TemplateSectionType == 99);
                        if (finalizeAndSubmitSection != null)
                        {
                            sectionForFinalizeAndSubmit = finalizeAndSubmitSection
                                .TemplateSectionID;
                        }
                    }
                    catch
                    {
                    }

                    var pendingSection =
                        entity.PrequalificationCompletedSections.FirstOrDefault(
                            rec =>
                                rec.PrequalificationId == form.PrequalificationId &&
                                rec.TemplateSectionId == sectionForFinalizeAndSubmit);
                    if (pendingSection != null)
                        entity.PrequalificationCompletedSections.Remove(pendingSection);
                }

                entity.SaveChanges();

            }
        }

        public TemplateSectionViewModel GetTemplateSection(long pqId, long templateSectionId, string role, Guid userId)
        {
            using (var entity = new EFDbContext())
            {
                TemplateSections templateSection = entity.TemplateSections.Find(templateSectionId);
                Prequalification pq = entity.Prequalification.Find(pqId);

                TemplateSectionViewModel pqTemplateSection = new TemplateSectionViewModel();
                if (pq != null)
                {
                    pqTemplateSection.BlockClientDocsSelection = pq.BlockClientDocsSelection;

                    pqTemplateSection.IsEri = pq.ClientTemplates.Templates.IsERI;
                    pqTemplateSection.PrequalificationStatusId = pq.PrequalificationStatusId;
                    PqPaymentCommonViewModel pqPaymentCommonData =
                        GetPqPaymentCommonData(pqId, pq.VendorId, pq.ClientTemplateId);
                    pqTemplateSection.TotalAdditionalFee = pqPaymentCommonData.TotalAdditionalFee;
                    pqTemplateSection.TotalAnnualFee = pqPaymentCommonData.TotalAnnualFee;
                    pqTemplateSection.AnnualFeeBus = pqPaymentCommonData.AnnualFeeBus;
                    pqTemplateSection.PqSitesBuUnits = pqPaymentCommonData.PqSitesBuUnits;
                    pqTemplateSection.TemplatePrice = GetPqPriceToPay(pq.VendorId, pq.ClientTemplateId, pqId);
                    SystemUsers pqSubmittedBy = pq.LoggedInUserForPreQualification;
                    if (pqSubmittedBy != null)
                    {
                        pqTemplateSection.FinalizedUserEmail = pqSubmittedBy.Email;
                        pqTemplateSection.FinalizedDateTime = pq.PrequalificationSubmit;
                        pqTemplateSection.FinalizedUserName = pqSubmittedBy.UserName;
                    }
                    var hasLocationNotificationPermission = pq.ClientTemplates.Templates.HasLocationNotification;
                    var isLocationSection = entity.TemplateSections.Any(r => r.TemplateSectionID == templateSectionId && r.TemplateSectionType == 7);

                    if (isLocationSection && hasLocationNotificationPermission && pq.PrequalificationSites != null && pq.PrequalificationSites.Count() >= 2)
                    {
                        pqTemplateSection.HasMultipleLocations = true;
                    }
                    string clientRefId = "";
                    try
                    {
                        ClientVendorReferenceIds clientRef = entity.ClientVendorReferenceIds.FirstOrDefault(rec =>
                            rec.ClientId == pq.ClientId && rec.VendorId == pq.VendorId);
                        if (clientRef != null)
                            clientRefId = clientRef.ClientVendorReferenceId;
                    }
                    catch
                    {
                        clientRefId = "";
                    }

                    pqTemplateSection.ClientVendorRefId = clientRefId;
                }

                if (templateSection != null)
                {
                    TemplateSectionsPermission tempSectionPermission = templateSection.TemplateSectionsPermission.FirstOrDefault();
                    if (tempSectionPermission != null)
                        pqTemplateSection.VisibleTo = tempSectionPermission.VisibleTo;
                    PrequalificationCompletedSections pqCompletedSection = entity.PrequalificationCompletedSections
                        .FirstOrDefault(rec =>
                            rec.PrequalificationId == pqId && rec.TemplateSectionId == templateSectionId);
                    if (pqCompletedSection != null)
                    {
                        pqTemplateSection.PqSectionSubmittedDate = Convert.ToDateTime(pqCompletedSection.SubmittedDate);
                        pqTemplateSection.PqSectionCompleted = true;
                    }
                    else
                        pqTemplateSection.PqSectionSubmittedDate = DateTime.Now;

                    pqTemplateSection.TemplateSectionType = templateSection.TemplateSectionType;
                    if (templateSection.TemplateSectionType == SectionTypes.IsProfile ||
                        templateSection.TemplateSectionType == SectionTypes.IsPrequalificationRequirements)
                    {
                        pqTemplateSection.ClientTemplateProfiles = new List<ClientTemplateProfilesViewModel>();

                        List<ClientTemplateProfiles> sectionClientTemplateProfiles =
                            templateSection.ClientTemplateProfiles;
                        foreach (ClientTemplateProfiles clientTemplateProfile in sectionClientTemplateProfiles)
                        {
                            ClientTemplateProfilesViewModel pqSectionClientTemplateProfile = new ClientTemplateProfilesViewModel();

                            pqSectionClientTemplateProfile.ClientId = clientTemplateProfile.ClientId;
                            pqSectionClientTemplateProfile.ClientTemplateProfileId =
                                clientTemplateProfile.ClientTemplateProfileId;
                            pqSectionClientTemplateProfile.AttachmentExist = clientTemplateProfile.AttachmentExist;
                            pqSectionClientTemplateProfile.ContentType = clientTemplateProfile.ContentType;
                            pqSectionClientTemplateProfile.PositionType = clientTemplateProfile.PositionType;
                            pqSectionClientTemplateProfile.PositionTitle = clientTemplateProfile.PositionTitle;
                            pqSectionClientTemplateProfile.PositionContent = clientTemplateProfile.PositionContent;

                            pqTemplateSection.ClientTemplateProfiles.Add(pqSectionClientTemplateProfile);
                        }
                    }

                    List<TemplateSectionsHeaderFooter> templateSectionsHeaderFooters =
                        templateSection.TemplateSectionsHeaderFooter;
                    pqTemplateSection.TemplateSectionsHeaderFooter = new List<TemplateSectionsHeaderFooterViewModel>();
                    foreach (TemplateSectionsHeaderFooter sectionsHeaderFooter in templateSectionsHeaderFooters)
                    {
                        TemplateSectionsHeaderFooterViewModel pqSectionHeaderFooter = new TemplateSectionsHeaderFooterViewModel();

                        pqSectionHeaderFooter.PositionTitle = sectionsHeaderFooter.PositionTitle;
                        pqSectionHeaderFooter.PostionContent = sectionsHeaderFooter.PostionContent;
                        pqSectionHeaderFooter.PositionType = sectionsHeaderFooter.PositionType;
                        pqSectionHeaderFooter.AttachmentExist = sectionsHeaderFooter.AttachmentExist;
                        pqSectionHeaderFooter.HeaderFooterId = sectionsHeaderFooter.HeaderFooterId;

                        pqTemplateSection.TemplateSectionsHeaderFooter.Add(pqSectionHeaderFooter);
                    }
                    pqTemplateSection.TemplateSectionsPermission = new List<TemplateSectionsPermissionViewModel>();

                    List<TemplateSectionsPermission> sectionPermissions =
                        templateSection.TemplateSectionsPermission.ToList();
                    foreach (TemplateSectionsPermission sectionPermission in sectionPermissions)
                    {
                        TemplateSectionsPermissionViewModel tSectionPermission = new TemplateSectionsPermissionViewModel();

                        tSectionPermission.VisibleTo = sectionPermission.VisibleTo;
                        pqTemplateSection.TemplateSectionsPermission.Add(tSectionPermission);
                    }

                    List<TemplateSubSections> subSections = templateSection.TemplateSubSections
                        .Where(rec => rec.Visible == true).OrderBy(x => x.DisplayOrder).ToList();

                    pqTemplateSection.TemplateSubSections = new List<TemplateSectionSubSectionsViewModel>();

                    foreach (TemplateSubSections subSection in subSections)
                    {
                        string userType = role.ToString();
                        TemplateSectionSubSectionsViewModel tSectionSubSection = new TemplateSectionSubSectionsViewModel();

                        tSectionSubSection.SubSectionId = subSection.SubSectionID;
                        tSectionSubSection.ClientId = subSection.ClientId;
                        tSectionSubSection.DisplayOrder = subSection.DisplayOrder;
                        tSectionSubSection.NoOfColumns = subSection.NoOfColumns;
                        tSectionSubSection.SectionNotes = subSection.SectionNotes;
                        tSectionSubSection.SubSectionName = subSection.SubSectionName;
                        tSectionSubSection.SubHeader = subSection.SubHeader;
                        tSectionSubSection.AttachmentExist = subSection.AttachmentExist;
                        tSectionSubSection.SubSectionType = subSection.SubSectionType;
                        tSectionSubSection.SubSectionTypeCondition = subSection.SubSectionTypeCondition;
                        tSectionSubSection.Visible = subSection.Visible;

                        tSectionSubSection.Questions = new List<QuestionsViewModel>();
                        tSectionSubSection.TemplateSubSectionsPermissions = new List<TemplateSubSectionsPermissionsViewModel>();

                        List<TemplateSubSectionsPermissions> subSectionPermissions =
                            subSection.TemplateSubSectionsPermissions.ToList();
                        foreach (TemplateSubSectionsPermissions subSectionPermission in subSectionPermissions)
                        {
                            TemplateSubSectionsPermissionsViewModel tSectionSubSectionPermission = new TemplateSubSectionsPermissionsViewModel();

                            tSectionSubSectionPermission.PermissionFor = subSectionPermission.PermissionFor;
                            tSectionSubSectionPermission.PermissionType = subSectionPermission.PermissionType;

                            tSectionSubSection.TemplateSubSectionsPermissions.Add(tSectionSubSectionPermission);

                            tSectionSubSectionPermission.TemplateSubSectionsUserPermissions = new List<TemplateSubSectionsUserPermissionsViewModel>();

                            List<TemplateSubSectionsUserPermissions> subSectionUserPermissions =
                                subSectionPermission.TemplateSubSectionsUserPermissions.ToList();
                            foreach (TemplateSubSectionsUserPermissions subSectionUserPermission in subSectionUserPermissions)
                            {
                                TemplateSubSectionsUserPermissionsViewModel tSectionSubSectionUserPermission = new TemplateSubSectionsUserPermissionsViewModel();

                                tSectionSubSectionUserPermission.UserId = subSectionUserPermission.UserId;
                                tSectionSubSectionUserPermission.EditAccess = subSectionUserPermission.EditAccess;

                                tSectionSubSectionPermission.TemplateSubSectionsUserPermissions.Add(tSectionSubSectionUserPermission);
                            }
                        }
                        List<Questions> subSectionQuestions = subSection.Questions.Where(rec => rec.Visible == true)
                            .OrderBy(x => x.DisplayOrder).ToList();

                        foreach (Questions question in subSectionQuestions)
                        {
                            QuestionsViewModel tSectionSubSectionQuestion = new QuestionsViewModel();

                            tSectionSubSectionQuestion.SubSectionId = question.SubSectionId;
                            tSectionSubSectionQuestion.QuestionId = question.QuestionID;
                            tSectionSubSectionQuestion.QuestionBankId = question.QuestionBankId;
                            tSectionSubSectionQuestion.NumberOfColumns = question.NumberOfColumns;
                            tSectionSubSectionQuestion.IsMandatory = question.IsMandatory;
                            tSectionSubSectionQuestion.IsBold = question.IsBold;
                            tSectionSubSectionQuestion.QuestionText = question.QuestionText;
                            tSectionSubSectionQuestion.HasDependantQuestions = question.HasDependantQuestions;
                            tSectionSubSectionQuestion.Visible = question.Visible;
                            tSectionSubSectionQuestion.DisplayOrder = question.DisplayOrder;
                            tSectionSubSectionQuestion.QuestionReferenceId = question.QuestionsBank.QuestionReferenceId;
                            tSectionSubSectionQuestion.HasHighlights = question.HasHighlights;

                            tSectionSubSectionQuestion.QuestionColors = new List<QuestionColorsViewModel>();

                            List<QuestionColors> questionColors = question.QuestionColors.ToList();
                            foreach (var questionColor in questionColors)
                            {
                                QuestionColorsViewModel tSectionQuestionColor = new QuestionColorsViewModel();

                                tSectionQuestionColor.Answer = questionColor.Answer;
                                tSectionQuestionColor.Color = questionColor.Color;

                                tSectionSubSectionQuestion.QuestionColors.Add(tSectionQuestionColor);
                            }

                            tSectionSubSectionQuestion.QuestionColumnDetails = new List<QuestionColumnDetailsViewModel>();

                            List<QuestionColumnDetails> questionQuestionColumnDetails =
                                question.QuestionColumnDetails.ToList();

                            foreach (QuestionColumnDetails questionColumn in questionQuestionColumnDetails)
                            {
                                QuestionColumnDetailsViewModel subSectionQuestionQuestionColumn = new QuestionColumnDetailsViewModel();

                                subSectionQuestionQuestionColumn.QuestionColumnId = questionColumn.QuestionColumnId;
                                subSectionQuestionQuestionColumn.QuestionTypeId =
                                    questionColumn.QuestionControlType.QuestionTypeID;
                                subSectionQuestionQuestionColumn.ButtonTypeReferenceValue =
                                    questionColumn.ButtonTypeReferenceValue;
                                subSectionQuestionQuestionColumn.ColumnNo = questionColumn.ColumnNo;
                                subSectionQuestionQuestionColumn.ColumnValues = questionColumn.ColumnValues;
                                subSectionQuestionQuestionColumn.DisplayType = questionColumn.DisplayType;
                                subSectionQuestionQuestionColumn.TableReference = questionColumn.TableReference;
                                subSectionQuestionQuestionColumn.TableReferenceField =
                                    questionColumn.TableReferenceField;
                                subSectionQuestionQuestionColumn.TableRefFieldValueDisplay =
                                    questionColumn.TableRefFieldValueDisplay;
                                subSectionQuestionQuestionColumn.SortReferenceField = questionColumn.SortReferenceField;
                                subSectionQuestionQuestionColumn.QuestionId = questionColumn.QuestionId;
                                subSectionQuestionQuestionColumn.QFormula = questionColumn.QFormula;
                                if (pq.PrequalificationUserInputs.FirstOrDefault(r =>
                                    r.QuestionColumnId == questionColumn.QuestionColumnId) != null)
                                {
                                    subSectionQuestionQuestionColumn.QuestionColumnUserInput = pq
                                        .PrequalificationUserInputs
                                        .FirstOrDefault(rec => rec.QuestionColumnId == questionColumn.QuestionColumnId)
                                        .UserInput;
                                }
                                else
                                {
                                    subSectionQuestionQuestionColumn.QuestionColumnUserInput = "";
                                }

                                if (subSection.SubSectionTypeCondition == SubSectionTypeCondition.ClientSign)
                                {
                                    subSectionQuestionQuestionColumn.MpaSectionSignedQuestionHasUserInput = true;
                                    if (question.QuestionText == "Signed:" &&
                                        questionColumn.PrequalificationUserInputs.Any(r =>
                                            r.PreQualificationId == pqId && !string.IsNullOrEmpty(r.UserInput)))
                                    {
                                        subSectionQuestionQuestionColumn.MpaSectionSignedQuestionHasUserInput = false;
                                    }
                                }

                                tSectionSubSectionQuestion.QuestionColumnDetails.Add(subSectionQuestionQuestionColumn);
                            }
                            tSectionSubSectionQuestion.QuestionsDependants = new List<QuestionsDependantsViewModel>();

                            List<QuestionsDependants> questionDependants = question.QuestionsDependants;

                            foreach (QuestionsDependants dependantQuestion in questionDependants)
                            {
                                QuestionsDependantsViewModel questionDependantQuestion = new QuestionsDependantsViewModel();

                                questionDependantQuestion.DependantId = dependantQuestion.DependantId;
                                questionDependantQuestion.DependantType = dependantQuestion.DependantType;
                                questionDependantQuestion.DependantValue = dependantQuestion.DependantValue;

                                tSectionSubSectionQuestion.QuestionsDependants.Add(questionDependantQuestion);
                            }

                            tSectionSubSection.Questions.Add(tSectionSubSectionQuestion);
                        }
                        pqTemplateSection.TemplateSubSections.Add(tSectionSubSection);
                    }
                }

                return pqTemplateSection;
            }
        }

        public void PostPqAssignerAndExceptionStatus(LocalChangeStatus form)
        {
            using (var entity = new EFDbContext())
            {
                Prequalification pq = entity.Prequalification.Find(form.PrequalificationId);

                if (pq != null)
                {
                    pq.RequestForMoreInfo = form.RequestForMoreInfo;
                    pq.UserIdAsSigner = form.userIdAsSigner;

                    entity.Entry(pq).State = EntityState.Modified;
                    entity.SaveChanges();
                }
            }
        }

        public TemplateSubSections GetTemplateSubSection(long subSectionId)
        {
            using (var entity = new EFDbContext())
            {
                return entity.TemplateSubSections.Find(subSectionId);
            }
        }

        public List<IGrouping<long?, SupportingDocumentsViewModel>> GetPqSupportingDocs(long sectionId, long pqId, string role, long orgId)
        {
            using (var entity = new EFDbContext())
            {
                Prequalification pq = entity.Prequalification.Find(pqId);
                List<IGrouping<long?, SupportingDocumentsViewModel>> supportingDocuments = new List<IGrouping<long?, SupportingDocumentsViewModel>>();
                if (pq != null)
                {
                    List<Guid> clientTemplateIds = entity.ClientTemplates
                        .FirstOrDefault(row => row.ClientTemplateID == pq.ClientTemplateId).Templates.ClientTemplates
                        .Select(rec => rec.ClientTemplateID).ToList();

                    List<long> documentTypeIds = entity.ClientTemplateReportingData
                        .Where(rec => rec.ReportingType == 2 && clientTemplateIds.Contains(rec.ClientTemplateId))
                        .Select(rec => rec.ReportingTypeId).ToList();

                    List<Document> documents = new List<Document>();
                    if (role.Equals(LocalConstants.Admin) || role.Equals(LocalConstants.Vendor))
                    {
                        if (role.Equals(OrganizationType.Admin))
                        {
                            documents = entity.Document.Where(rec =>
                                rec.ReferenceType == "PreQualification" && rec.SectionId == sectionId &&
                                rec.ReferenceRecordID == pqId &&
                                documentTypeIds.Contains(rec.DocumentTypeId)).ToList();
                        }
                        else
                        {
                            documents = entity.Document.Where(rec =>
                                    rec.ReferenceType == "PreQualification" && rec.SectionId == sectionId &&
                                    rec.ReferenceRecordID == pqId &&
                                    (rec.DocumentStatus != "Deleted" || rec.DocumentStatus == null) &&
                                    documentTypeIds.Contains(rec.DocumentTypeId))
                                .ToList();
                        }
                    }

                    else
                    {
                        documents = entity.Document.Where(rec => (rec.ClientId == null || rec.ClientId == orgId || rec.ClientOrganizations.OrganizationType == OrganizationType.SuperClient)
                                                                 && rec.ReferenceType == "PreQualification"
                                                                 && rec.ReferenceRecordID == pqId
                                                                 && rec.SectionId == sectionId
                                                                 && (rec.DocumentStatus == "Approved" || rec.Systemusers.SystemUsersOrganizations.FirstOrDefault().Organizations.OrganizationType == OrganizationType.Client)
                                                                 && documentTypeIds.Contains(rec.DocumentTypeId)).ToList();
                    }
                    List<SupportingDocumentsViewModel> sDocuments = new List<SupportingDocumentsViewModel>();
                    foreach (var doc in documents)
                    {
                        SupportingDocumentsViewModel supportingDoc = new SupportingDocumentsViewModel();
                        DocumentType docType = doc.DocumentType;
                        Contact statusChangedUserContact = null;
                        Contact updatedByUserContact = null;
                        if (doc.StatusChangedBy != null && doc.StatusChangeDate != null && doc.StatusChangedUser != null && doc.StatusChangedUser.Contacts.Any())
                        {
                            statusChangedUserContact = doc.StatusChangedUser.Contacts.FirstOrDefault();
                        }

                        if (doc.UpdatedOn != null && doc.UpdatedUser != null && doc.UpdatedUser.Contacts.Any())
                        {
                            updatedByUserContact = doc.UpdatedUser.Contacts.FirstOrDefault();
                        }

                        Contact uploaderContact = null;
                        ;
                        if (doc.Systemusers != null && doc.Systemusers.Contacts.Any())
                        {
                            uploaderContact = doc.Systemusers.Contacts.FirstOrDefault();
                        }

                        supportingDoc.DocumentId = doc.DocumentId;
                        supportingDoc.DocumentName = doc.DocumentName;
                        supportingDoc.DocumentStatus = doc.DocumentStatus;
                        supportingDoc.DocumentTypeId = doc.DocumentTypeId;
                        supportingDoc.DocumentTypeName = docType.DocumentTypeName;
                        supportingDoc.Expires = doc.Expires;
                        supportingDoc.OrganizationId = doc.OrganizationId;
                        if (doc.ClientId != null)
                        {
                            supportingDoc.OrganizationType = doc.ClientOrganizations.OrganizationType;
                            supportingDoc.ClientName = doc.ClientOrganizations.Name;
                        }

                        supportingDoc.RestrictAccess = docType.RestrictAccess;
                        supportingDoc.StatusChangeDate = doc.StatusChangeDate;
                        if (statusChangedUserContact != null)
                            supportingDoc.StatusChangedUserFullName = statusChangedUserContact.FullName;
                        if (updatedByUserContact != null)
                            supportingDoc.UpdatedByUserFullName = updatedByUserContact.FullName;
                        supportingDoc.UpdatedOn = doc.UpdatedOn;
                        supportingDoc.Uploaded = doc.Uploaded;
                        if (uploaderContact != null)
                            supportingDoc.UploaderFullName = uploaderContact.FullName;
                        supportingDoc.ClientId = doc.ClientId;

                        sDocuments.Add(supportingDoc);
                    }

                    supportingDocuments = sDocuments.GroupBy(rec => rec.ClientId).ToList();
                }
                return supportingDocuments;
            }
        }

        public List<PqReferenceDocumentsViewModel> GetReferenceDocs(string refType, long refRecId, long? docTypeId, long? clientId, bool? orderBy)
        {
            using (var entity = new EFDbContext())
            {
                if (docTypeId != 0 && clientId != 0 && orderBy != false)
                {
                    return entity.Document
                        .Where(rec =>
                            rec.ReferenceType == refType && rec.ReferenceRecordID == refRecId &&
                            rec.DocumentTypeId == docTypeId && rec.OrganizationId == clientId).OrderBy(rec => rec.OrderBy)
                        .Select(rec =>
                            new PqReferenceDocumentsViewModel()
                            {
                                DocumentId = rec.DocumentId,
                                DocumentName = rec.DocumentName,
                                OrganizationId = rec.OrganizationId
                            }).ToList();
                }

                if (docTypeId != 0 && clientId != 0)
                {
                    return entity.Document
                        .Where(rec =>
                            rec.ReferenceType == refType && rec.ReferenceRecordID == refRecId &&
                            rec.DocumentTypeId == docTypeId && rec.OrganizationId == clientId).Select(rec =>
                            new PqReferenceDocumentsViewModel()
                            {
                                DocumentId = rec.DocumentId,
                                DocumentName = rec.DocumentName,
                                OrganizationId = rec.OrganizationId
                            }).ToList();
                }
                else if (orderBy != null)
                {
                    return entity.Document
                        .Where(rec =>
                            rec.ReferenceType == refType && rec.ReferenceRecordID == refRecId
                            ).OrderBy(rec => rec.OrderBy).Select(rec =>
                            new PqReferenceDocumentsViewModel()
                            {
                                DocumentId = rec.DocumentId,
                                DocumentName = rec.DocumentName,
                                OrganizationId = rec.OrganizationId
                            }).ToList();
                }
                else
                {
                    return entity.Document
                        .Where(rec =>
                            rec.ReferenceType == refType && rec.ReferenceRecordID == refRecId
                        ).Select(rec =>
                            new PqReferenceDocumentsViewModel()
                            {
                                DocumentId = rec.DocumentId,
                                DocumentName = rec.DocumentName,
                                OrganizationId = rec.OrganizationId
                            }).ToList();
                }

            }
        }

        public decimal GetPqPriceToPay(long vendorId, Guid? clientTemplateId, long preQualificationId)
        {
            using (var entity = new EFDbContext())
            {
                {
                    Prequalification pq = entity.Prequalification.Find(preQualificationId);
                    decimal price = 0;

                    var templatePrices = entity.ClientTemplatesBUGroupPrice.FirstOrDefault(rec => rec.ClientTemplateID == clientTemplateId && rec.GroupPriceType == 0 && rec.GroupingFor == 0);
                    if (templatePrices == null)
                        return 0;
                    var currentDate = DateTime.Now;

                    OrganizationsPrequalificationOpenPeriod preOpenPeriod = entity.OrganizationsPrequalificationOpenPeriod.FirstOrDefault(rec => rec.VendorId == pq.VendorId && rec.PrequalificationPeriodStart <= currentDate && rec.PrequalificationPeriodClose >= currentDate); ;
                    if (pq != null && pq.PrequalificationStatusId != 15)
                        preOpenPeriod = preOpenPeriod;
                    else if (pq != null && pq.PrequalificationStatusId == 15)
                    {
                        if (preOpenPeriod != null && preOpenPeriod.PrequalificationPeriodClose >= currentDate.AddDays(60))
                            preOpenPeriod = preOpenPeriod;
                        else
                            try
                            {
                                preOpenPeriod = entity.OrganizationsPrequalificationOpenPeriod.FirstOrDefault(rec => rec.VendorId == vendorId && rec.PrequalificationPeriodStart > preOpenPeriod.PrequalificationPeriodClose);
                            }
                            catch
                            {
                                preOpenPeriod = null;
                            }
                    }
                    if (preOpenPeriod == null && pq.PaymentReceived == null)
                        price = Convert.ToDecimal(templatePrices.GroupPrice);

                    else if (preOpenPeriod == null && pq.PaymentReceived == false)
                        price = Convert.ToDecimal(templatePrices.GroupPrice);

                    else if (preOpenPeriod == null && pq.PaymentReceived != null)
                    {
                        price = 0;
                    }
                    else
                    {
                        long annualPeriodMaximumAmount = Convert.ToInt64(ConfigurationManager.AppSettings["PrequalificationAnnualPeriodAmount"]);
                        if (preOpenPeriod != null && pq != null && (annualPeriodMaximumAmount - preOpenPeriod.TotalPaymentRecieved >= templatePrices.GroupPrice && (pq.PaymentReceived == null || pq.PaymentReceived == false)))
                        {
                            price = Convert.ToDecimal(templatePrices.GroupPrice);
                        }
                        else if (preOpenPeriod != null && pq != null && (annualPeriodMaximumAmount - preOpenPeriod.TotalPaymentRecieved <= templatePrices.GroupPrice && (pq.PaymentReceived == null || pq.PaymentReceived == false)))
                        {
                            price = annualPeriodMaximumAmount - preOpenPeriod.TotalPaymentRecieved;
                        }
                        else
                        {
                            price = 0;
                        }
                    }
                    if (price <= 0)
                    {
                        price = 0;
                    }

                    price = Math.Round(price, 2);
                    return price;
                }
            }
        }

        public async Task<PqPaymentsViewModel> GetPaymentData(long preQualificationId)
        {
            using (var entity = new EFDbContext())
            {
                PqPaymentsViewModel paymentData = new PqPaymentsViewModel();

                Prequalification pq = entity.Prequalification.Find(preQualificationId);

                if (pq != null)
                {
                    paymentData.TemplatePrice = GetPqPriceToPay(pq.VendorId, pq.ClientTemplateId, preQualificationId);
                    paymentData.IsEri = pq.ClientTemplates.Templates.IsERI;
                    paymentData.PaymentReceived = pq.PaymentReceived;
                }

                var paidTemplatePrice = entity.PrequalificationPayments.FirstOrDefault(rec => rec.PrequalificationId == preQualificationId && rec.PaymentCategory == 0);
                try
                {
                    if (paidTemplatePrice == null && pq != null)
                    {
                        ClientTemplatesBUGroupPrice buGroupPrice =
                            entity.ClientTemplatesBUGroupPrice.FirstOrDefault(rec =>
                                rec.ClientTemplateID == pq.ClientTemplateId && rec.GroupingFor == 0 &&
                                rec.GroupPriceType == 0);
                        if (buGroupPrice != null)
                            paymentData.PqPrice = buGroupPrice.GroupPrice;
                    }
                    else if (paidTemplatePrice != null && paidTemplatePrice.PaymentReceivedAmount == null)
                    {
                        ClientTemplatesBUGroupPrice buGroupPrice = entity.ClientTemplatesBUGroupPrice.FirstOrDefault(rec =>
                            rec.ClientTemplateID == pq.ClientTemplateId && rec.GroupingFor == 0 &&
                            rec.GroupPriceType == 0);
                        if (buGroupPrice != null)
                            paymentData.PqPrice = buGroupPrice.GroupPrice;
                    }
                    else if (paidTemplatePrice != null)
                        paymentData.PqPrice = paidTemplatePrice.PaymentReceivedAmount;
                }
                catch (Exception e)
                {

                }
                paymentData.BuGroupPrice = new List<IGrouping<int?, ClientTemplatesBuGroupPriceViewModel>>();
                List<ClientTemplatesBUGroupPrice> pqBuSitesGroupPrices = entity.ClientTemplatesBUGroupPrice.Where(rec => rec.ClientTemplateID == pq.ClientTemplateId && rec.GroupingFor == 0).ToList();
                List<ClientTemplatesBuGroupPriceViewModel> buSitesGroupPrices = new List<ClientTemplatesBuGroupPriceViewModel>();
                foreach (ClientTemplatesBUGroupPrice buSitesGroupPrice in pqBuSitesGroupPrices)
                {
                    ClientTemplatesBuGroupPriceViewModel pqSiteGroupPrice = new ClientTemplatesBuGroupPriceViewModel();

                    pqSiteGroupPrice.GroupPrice = buSitesGroupPrice.GroupPrice;
                    pqSiteGroupPrice.GroupPriceType = buSitesGroupPrice.GroupPriceType;

                    pqSiteGroupPrice.ClientTemplatesForBu = new List<ClientTemplatesForBuViewModel>();
                    List<ClientTemplatesForBU> pqClientTemplatesForBus = buSitesGroupPrice.ClientTemplatesForBU;
                    foreach (ClientTemplatesForBU pqClientTemplatesForBu in pqClientTemplatesForBus)
                    {
                        ClientTemplatesForBuViewModel clientTemplatesForBu = new ClientTemplatesForBuViewModel();

                        clientTemplatesForBu.BusinessUnitName =
                            pqClientTemplatesForBu.ClientBusinessUnits.BusinessUnitName;

                        pqSiteGroupPrice.ClientTemplatesForBu.Add(clientTemplatesForBu);
                    }
                    buSitesGroupPrices.Add(pqSiteGroupPrice);
                }

                paymentData.BuGroupPrice = buSitesGroupPrices.GroupBy(r => r.GroupPriceType).ToList();
                List<ClientBusinessUnits> pqSiteBus = entity.PrequalificationSites.Where(rec => rec.PrequalificationPayments.PaymentCategory == 0 && rec.PrequalificationId == preQualificationId).Select(rec => rec.ClientBusinessUnits).Distinct().ToList();

                paymentData.PqBus = new List<ClientBusinessUnitsViewModel>();
                foreach (ClientBusinessUnits siteBu in pqSiteBus)
                {
                    ClientBusinessUnitsViewModel pqBu = new ClientBusinessUnitsViewModel();

                    pqBu.BusinessUnitName = siteBu.BusinessUnitName;

                    paymentData.PqBus.Add(pqBu);
                }

                var paidAdditionalBuPayments = entity.PrequalificationPayments.Where(rec => rec.PrequalificationId == preQualificationId && rec.PaymentCategory == 1).Select(rec => rec.PrequalificationPaymentsId).ToList();
                var paidAdditionalBuSites = entity.PrequalificationSites.Where(rec => paidAdditionalBuPayments.Contains((long)rec.PrequalificationPaymentsId) && rec.PrequalificationId == preQualificationId).ToList();

                paymentData.PaidAdditionalBu = new List<IGrouping<long?, PqSitesViewModel>>();
                List<PqSitesViewModel> pqPaidAdditionalSites = new List<PqSitesViewModel>();
                foreach (PrequalificationSites additionalBuSite in paidAdditionalBuSites)
                {
                    PqSitesViewModel pqPaidAdditionalBu = new PqSitesViewModel();

                    pqPaidAdditionalBu.ClientName = additionalBuSite.Client.Name;
                    pqPaidAdditionalBu.BusinessUnitName = additionalBuSite.ClientBusinessUnits.BusinessUnitName;
                    pqPaidAdditionalBu.ClientBusinessUnitId = additionalBuSite.ClientBusinessUnits.ClientBusinessUnitId;
                    pqPaidAdditionalBu.PaymentCategory = additionalBuSite.PrequalificationPayments.PaymentCategory;
                    pqPaidAdditionalBu.PaymentReceivedAmount =
                        additionalBuSite.PrequalificationPayments.PaymentReceivedAmount;
                    pqPaidAdditionalBu.PrequalificationPaymentsId = additionalBuSite.PrequalificationPaymentsId;

                    pqPaidAdditionalSites.Add(pqPaidAdditionalBu);

                }

                paymentData.PaidAdditionalBu =
                    pqPaidAdditionalSites.GroupBy(r => r.PrequalificationPaymentsId).ToList();

                if (paidAdditionalBuPayments.Count != paidAdditionalBuSites.Count)
                {
                    var paidAdditionalPaymentIds = entity.PrequalificationSites.Where(rec => paidAdditionalBuPayments.Contains((long)rec.PrequalificationPaymentsId) && rec.PrequalificationId == preQualificationId).Select(rec => rec.PrequalificationPaymentsId).Distinct().ToList();
                    var unUsedPaymentIds = entity.PrequalificationPayments.Where(rec => !paidAdditionalPaymentIds.Contains(rec.PrequalificationPaymentsId) && rec.PrequalificationId == preQualificationId && rec.PaymentCategory == 1).Distinct().ToList();

                    paymentData.UnusedPaymentIds = new List<PrequalificationPaymentsViewModel>();
                    foreach (PrequalificationPayments unUsedPayment in unUsedPaymentIds)
                    {
                        PrequalificationPaymentsViewModel pqUnUsedPayment = new PrequalificationPaymentsViewModel();

                        pqUnUsedPayment.PaymentReceivedAmount = unUsedPayment.PaymentReceivedAmount;

                        paymentData.UnusedPaymentIds.Add(pqUnUsedPayment);
                    }
                }
                PqViewModel pqData = GetPqInfo(preQualificationId, pq.VendorId, pq.ClientTemplateId);

                List<long?> preqSiteBus = pqData.PqSiteBus;
                List<long> additionalFeeBus = pqData.AdditionalFeeBus;

                var allBus = entity.ClientTemplatesForBU.Where(rec => preqSiteBus.Contains(rec.ClientBusinessUnitId) && additionalFeeBus.Contains((long)rec.ClientTemplateBUGroupId)).ToList();

                paymentData.UnPaidBus = new List<ClientTemplatesForBuViewModel>();
                foreach (var bu in allBus)
                {
                    ClientTemplatesForBuViewModel unPaidBu = new ClientTemplatesForBuViewModel();

                    unPaidBu.BusinessUnitName = bu.ClientBusinessUnits.BusinessUnitName;
                    unPaidBu.GroupPrice = bu.ClientTemplatesBUGroupPrice.GroupPrice;
                    unPaidBu.ClientTemplateBuGroupId = bu.ClientTemplateBUGroupId;
                    unPaidBu.ClientName = bu.ClientTemplates.Organizations.Name;
                    unPaidBu.ClientBusinessUnitId = bu.ClientBusinessUnitId;

                    paymentData.UnPaidBus.Add(unPaidBu);
                }
                var additionalBus = allBus.Select(rec => rec.ClientBusinessUnitId).ToList();
                var unpaidBus = entity.PrequalificationSites.Where(rec => rec.PrequalificationId == preQualificationId && additionalBus.Contains(rec.ClientBusinessUnitId) && (rec.PrequalificationPaymentsId == null || rec.PrequalificationPayments.PaymentCategory == 0)).Select(rec => rec.ClientBusinessUnitId).Distinct().ToList();//To display one fee bu,to be paid in additional group
                var unpaidGroups = allBus.Where(r => unpaidBus.Contains(r.ClientBusinessUnitId)).Select(r => r.ClientTemplateBUGroupId);
                var paidBus = unpaidBus;

                paymentData.PaidBus = unpaidBus;
                paymentData.UnPaidGroups = unpaidGroups.ToList();
                PqPaymentCommonViewModel pqCommonPaymentData = GetPqPaymentCommonData(preQualificationId, pq.VendorId, pq.ClientTemplateId);
                paymentData.MonthDiff = pqCommonPaymentData.MonthDiff;
                paymentData.TotalAdditionalFee = pqCommonPaymentData.TotalAdditionalFee;


                paymentData.AnnualFeeBus = pqCommonPaymentData.AnnualFeeBus;
                var annualPaidBus = entity.PrequalificationTrainingAnnualFees.Where(rec => rec.PrequalificationId == preQualificationId).ToList();
                paymentData.AnnualPaidBus = new List<IGrouping<long, PrequalificationTrainingAnnualFeesViewModel>>();
                List<PrequalificationTrainingAnnualFeesViewModel> pqAnnualPaidBus = new List<PrequalificationTrainingAnnualFeesViewModel>();

                foreach (var annualPaidBu in annualPaidBus)
                {
                    PrequalificationTrainingAnnualFeesViewModel pqAnnualPaidBu = new PrequalificationTrainingAnnualFeesViewModel();

                    pqAnnualPaidBu.BusinessUnitName =
                        annualPaidBu.ClientTemplatesForBU.ClientBusinessUnits.BusinessUnitName;
                    pqAnnualPaidBu.AnnualFeeReceivedAmount = annualPaidBu.AnnualFeeReceivedAmount;
                    pqAnnualPaidBu.ClientTemplateBuGroupId = annualPaidBu.ClientTemplateBUGroupId;

                    pqAnnualPaidBus.Add(pqAnnualPaidBu);
                }

                paymentData.AnnualPaidBus = pqAnnualPaidBus.GroupBy(r => r.ClientTemplateBuGroupId).ToList();

                paymentData.TotalAnnualFee = pqCommonPaymentData.TotalAnnualFee;

                decimal price = 0;
                if (pq.TotalPaymentReceivedAmount == null)
                {
                    try
                    {
                        var templatePrices = entity.ClientTemplatesBUGroupPrice.FirstOrDefault(rec => rec.ClientTemplateID == pq.ClientTemplateId && rec.GroupPriceType == 0 && rec.GroupingFor == 0).GroupPrice;
                        if (templatePrices != null)
                            price = (decimal)templatePrices;
                    }
                    catch
                    {
                        price = 0;
                    }
                }

                return await Task.FromResult(paymentData);
            }
        }

        public PqPaymentCommonViewModel GetPqPaymentCommonData(long pqId, long vendorId, Guid? clientTemplateId)
        {
            using (var entity = new EFDbContext())
            {
                Prequalification pq = entity.Prequalification.Find(pqId);

                PqPaymentCommonViewModel pqPaymentCommonData = new PqPaymentCommonViewModel();
                decimal totalAdditionalfee = 0;
                var currentDate = DateTime.Now;

                var monthDiff = 0;
                long pqStatusId = 0;
                if (pq != null)
                {
                    pqStatusId = pq.PrequalificationStatusId;
                }
                PqViewModel pqData = GetPqInfo(pqId, vendorId, clientTemplateId);
                List<long?> pqSiteBus = pqData.PqSiteBus;
                List<long> additionalFeeBus = pqData.AdditionalFeeBus;

                List<ClientTemplatesForBU> allBus = entity.ClientTemplatesForBU.Where(rec => pqSiteBus.Contains(rec.ClientBusinessUnitId) && additionalFeeBus.Contains((long)rec.ClientTemplateBUGroupId)).ToList();
                List<long?> additionalBus = allBus.Select(rec => rec.ClientBusinessUnitId).ToList();
                List<long?> unpaidBus = entity.PrequalificationSites.Where(rec => rec.PrequalificationId == pqId && additionalBus.Contains(rec.ClientBusinessUnitId) && (rec.PrequalificationPaymentsId == null || rec.PrequalificationPayments.PaymentCategory == 0)).Select(rec => rec.ClientBusinessUnitId).Distinct().ToList();
                var unpaidGroups = allBus.Where(r => unpaidBus.Contains(r.ClientBusinessUnitId)).Select(r => r.ClientTemplateBUGroupId);
                var finalStatuses = Finalstatus();
                if (!finalStatuses.Any(r => r == pqStatusId))
                {
                    monthDiff = 12;
                }
                else if (currentDate.Year > pq.PrequalificationStart.Year)
                {
                    monthDiff = pq.PrequalificationStart.Month - currentDate.Month;
                }
                else
                {
                    monthDiff = 12 - currentDate.Month + pq.PrequalificationStart.Month;
                }

                pqPaymentCommonData.MonthDiff = monthDiff;
                if (pq != null && pq.ClientTemplates.Templates.IsERI)
                {

                    if (currentDate.Month != pq.PrequalificationStart.Month && DateTime.Now.Day >= 16 &&
                        pqStatusId != 3 && pqStatusId != 15)
                    {
                        monthDiff--;
                    }


                    var priceList = allBus.Select(r => new
                    { r.ClientTemplateBUGroupId, r.ClientTemplatesBUGroupPrice.GroupPrice }).Distinct();
                    foreach (var businessUnit in priceList)
                    {
                        if (unpaidGroups.Contains(businessUnit.ClientTemplateBUGroupId))
                        {
                            totalAdditionalfee += (decimal)businessUnit.GroupPrice * monthDiff / 12;
                        }
                    }

                }
                else
                {
                    foreach (var businessUnit in allBus)
                    {
                        if (unpaidBus.Contains(businessUnit.ClientBusinessUnitId))
                        {
                            totalAdditionalfee += (decimal)businessUnit.ClientTemplatesBUGroupPrice.GroupPrice;
                        }
                    }
                }

                totalAdditionalfee = Math.Round(totalAdditionalfee, 2, MidpointRounding.AwayFromZero);
                totalAdditionalfee = ApplyFeeCap(vendorId, DateTime.Now, pqId, totalAdditionalfee);

                pqPaymentCommonData.TotalAdditionalFee = totalAdditionalfee;
                List<long> paidGroupIds = entity.PrequalificationTrainingAnnualFees.Where(rec => rec.PrequalificationId == pqId).Select(rec => rec.ClientTemplateBUGroupId).ToList();
                List<ClientTemplatesForBU> test = entity.ClientTemplatesForBU.Where(rec => pqSiteBus.Contains(rec.ClientBusinessUnitId) && !paidGroupIds.Contains((long)rec.ClientTemplateBUGroupId) && rec.ClientTemplateId == clientTemplateId && rec.ClientTemplatesBUGroupPrice.GroupingFor == 1 && rec.ClientTemplatesBUGroupPrice.GroupPrice != 0).ToList();

                pqPaymentCommonData.AnnualFeeBus = new List<IGrouping<long?, ClientTemplatesForBuViewModel>>();
                List<ClientTemplatesForBuViewModel> annualFeeBus = new List<ClientTemplatesForBuViewModel>();

                foreach (var bu in test)
                {
                    ClientTemplatesForBuViewModel annualFeeBu = new ClientTemplatesForBuViewModel();

                    annualFeeBu.GroupPrice = bu.ClientTemplatesBUGroupPrice.GroupPrice;
                    annualFeeBu.BusinessUnitName = bu.ClientBusinessUnits.BusinessUnitName;
                    annualFeeBu.ClientTemplateBuGroupId = bu.ClientTemplateBUGroupId;

                    annualFeeBus.Add(annualFeeBu);
                }

                pqPaymentCommonData.AnnualFeeBus = annualFeeBus.GroupBy(r => r.ClientTemplateBuGroupId).ToList();
                List<ClientTemplatesBUGroupPrice> payGroup = test.Select(rec => rec.ClientTemplatesBUGroupPrice).Distinct().ToList();
                decimal totalannualfee = 0;
                for (int i = 0; i < payGroup.Count; i++)
                {
                    totalannualfee = (payGroup[i].GroupPrice.Value * pqPaymentCommonData.MonthDiff / 12) + totalannualfee;
                }

                pqPaymentCommonData.TotalAnnualFee = Math.Round(totalannualfee, 2, MidpointRounding.AwayFromZero);
                pqPaymentCommonData.PqSitesBuUnits = pqData.PqSiteBus;

                return pqPaymentCommonData;
            }
        }

        public PqViewModel GetPqInfo(long pqId, long vendorId, Guid? clientTemplateId)
        {
            using (var entity = new EFDbContext())
            {
                Prequalification pq = entity.Prequalification.Find(pqId);

                var clientIds = new List<long>();
                if (pq != null && pq.ClientTemplates.Templates.IsERI)
                {
                    var clients = GetPrequalificationClient(pqId);
                    if (clients != null)
                        clientIds = clients.ClientIdsList.ToList();
                }
                clientIds.Add(pq.ClientId);
                List<Guid> clientTemplateIds = entity.ClientTemplates.Where(r => clientIds.Contains(r.ClientID) && r.TemplateID == pq.ClientTemplates.TemplateID).Select(r => r.ClientTemplateID).ToList();


                List<long?> pqSiteBus = entity.PrequalificationSites.Where(rec => rec.PrequalificationId == pqId).Select(rec => rec.ClientBusinessUnitId).Distinct().ToList();
                List<long> additionalFeeBus = entity.ClientTemplatesBUGroupPrice.Where(rec => clientTemplateIds.Contains((Guid)rec.ClientTemplateID) && rec.GroupingFor == 0 && rec.GroupPrice != 0 && rec.GroupPriceType != 0).Select(rec => rec.ClientTemplateBUGroupId).ToList();

                //List<long> paidGroupIds = entity.PrequalificationTrainingAnnualFees.Where(rec => rec.PrequalificationId == pqId).Select(rec => rec.ClientTemplateBUGroupId).ToList();
                //List<ClientTemplatesForBU> test = entity.ClientTemplatesForBU.Where(rec => pqSiteBus.Contains(rec.ClientBusinessUnitId) && !paidGroupIds.Contains((long)rec.ClientTemplateBUGroupId) && rec.ClientTemplateId == clientTemplateId && rec.ClientTemplatesBUGroupPrice.GroupingFor == 1 && rec.ClientTemplatesBUGroupPrice.GroupPrice != 0).ToList();


                PqViewModel pqData = new PqViewModel();

                pqData.ClientTemplateIds = clientTemplateIds;
                pqData.PqSiteBus = pqSiteBus;
                pqData.AdditionalFeeBus = additionalFeeBus;
                //pqData.ClientTemplatesForBus = test;

                return pqData;
            }
        }
        public decimal ApplyFeeCap(long vendorId, DateTime currentDate, long pqId, decimal totalAdditionalFee)
        {
            using (var entity = new EFDbContext())
            {
                Prequalification pq = entity.Prequalification.Find(pqId);
                OrganizationsPrequalificationOpenPeriod preOpenPeriod = entity.OrganizationsPrequalificationOpenPeriod.FirstOrDefault(rec => rec.VendorId == pq.VendorId && rec.PrequalificationPeriodStart <= currentDate && rec.PrequalificationPeriodClose >= currentDate);
                if (pq != null && pq.PrequalificationStatusId == 15)
                {
                    if (!(preOpenPeriod != null && preOpenPeriod.PrequalificationPeriodClose >= currentDate.AddDays(60)))

                    {
                        try
                        {
                            preOpenPeriod = entity.OrganizationsPrequalificationOpenPeriod.FirstOrDefault(rec => rec.VendorId == vendorId && rec.PrequalificationPeriodStart > preOpenPeriod.PrequalificationPeriodClose);
                        }
                        catch
                        {
                            preOpenPeriod = null;
                        }
                    }
                }
                long annualPeriodMaximumAmount = Convert.ToInt64(ConfigurationManager.AppSettings["PrequalificationAnnualPeriodAmount"]);

                if (preOpenPeriod != null)
                {


                    if (annualPeriodMaximumAmount - preOpenPeriod.TotalPaymentRecieved <= totalAdditionalFee)
                    {
                        totalAdditionalFee = annualPeriodMaximumAmount - preOpenPeriod.TotalPaymentRecieved;
                    }
                }
                if (totalAdditionalFee > annualPeriodMaximumAmount)
                {
                    totalAdditionalFee = annualPeriodMaximumAmount;
                }
                if (totalAdditionalFee <= 0)
                {
                    totalAdditionalFee = 0;
                }
                return totalAdditionalFee;
            }
        }

        public long? GetTemplateLanguage(Guid templateId)
        {
            using (var entity = new EFDbContext())
            {
                Templates pqTemplate = entity.Templates.Find(templateId);

                if (pqTemplate != null)
                {
                    return pqTemplate.LanguageId;
                }

                return 0;
            }
        }

        public long GetPqTemplateSectionId(long pqId, long templateSectionId, string role)
        {
            using (var entity = new EFDbContext())
            {
                Prequalification pq = entity.Prequalification.Find(pqId);
                if (pq != null)
                {
                    Templates selectedTemplate = pq.ClientTemplates.Templates;
                    List<long> hideSectionsToDisplay = GetPQSectionsToDisplay(pqId);
                    try
                    {
                        var allVisibleTemplateSections = entity.TemplateSectionsPermission
                            .Where(rec =>
                                rec.TemplateSections.TemplateID == selectedTemplate.TemplateID &&
                                rec.VisibleTo == 3 || rec.VisibleTo == 1)
                            .Select(rec => rec.TemplateSectionID).ToList();

                        if (!role.Equals(OrganizationType.Vendor))
                        {
                            allVisibleTemplateSections.AddRange(entity.TemplateSectionsPermission
                                .Where(rec => rec.VisibleTo == 4)
                                .Select(rec => rec.TemplateSectionID).ToList());
                        }
                        else
                        {
                            allVisibleTemplateSections.AddRange(entity.TemplateSectionsPermission
                                .Where(rec => rec.VisibleTo == LocalConstants.SectionPermissionType.RESTRICTED_ACCESS_2)
                                .Select(rec => rec.TemplateSectionID).ToList());
                        }

                        if (templateSectionId == -1)
                        {
                            if (selectedTemplate != null && hideSectionsToDisplay != null)
                            {
                                List<TemplateSections> templateSections = selectedTemplate.TemplateSections;
                                if (templateSections != null)
                                {
                                    TemplateSections templateSection = null;
                                    templateSection = templateSections
                                        .Where(rec =>
                                            rec.Visible == true && (rec.HideFromPrequalification != true ||
                                                                    hideSectionsToDisplay.Contains(
                                                                        rec.TemplateSectionID)))
                                        .OrderBy(rec => rec.DisplayOrder).FirstOrDefault();

                                    if (role.ToString().Equals(OrganizationType.Vendor))
                                    {
                                        templateSection = templateSections.Where(rec =>
                                                rec.Visible == true &&
                                                (rec.HideFromPrequalification != true ||
                                                 hideSectionsToDisplay.Contains(rec.TemplateSectionID)) &&
                                                allVisibleTemplateSections.Contains(rec.TemplateSectionID))
                                            .OrderBy(rec => rec.DisplayOrder)
                                            .FirstOrDefault();
                                    }

                                    if (templateSection != null)
                                        templateSectionId = templateSection.TemplateSectionID;



                                    var completedSections = entity.PrequalificationCompletedSections
                                        .Where(rec => rec.PrequalificationId == pq.PrequalificationId)
                                        .Select(rec => rec.TemplateSectionId).ToList();


                                    TemplateSections templateSectionsList = templateSections
                                        .Where(rec =>
                                            rec.Visible == true && (rec.HideFromPrequalification != true ||
                                                                    hideSectionsToDisplay.Contains(
                                                                        rec.TemplateSectionID)))
                                        .OrderBy(rec => rec.DisplayOrder)
                                        .FirstOrDefault(rec => !completedSections.Contains(rec.TemplateSectionID));


                                    if (role.ToString().Equals(OrganizationType.Vendor)) //If Vendor Login
                                    {
                                        templateSectionsList = templateSections
                                            .Where(rec =>
                                                rec.Visible == true &&
                                                (rec.HideFromPrequalification != true ||
                                                 hideSectionsToDisplay.Contains(rec.TemplateSectionID)) &&
                                                allVisibleTemplateSections.Contains(rec.TemplateSectionID))
                                            .OrderBy(rec => rec.DisplayOrder)
                                            .FirstOrDefault(rec => !completedSections.Contains(rec.TemplateSectionID));
                                    }

                                    if (templateSectionsList != null)
                                    {
                                        if (templateSectionsList.TemplateSectionType == 35)
                                        {
                                            templateSectionsList = templateSections
                                                .Where(rec =>
                                                    rec.Visible == true && (rec.HideFromPrequalification != true ||
                                                                            hideSectionsToDisplay.Contains(
                                                                                rec.TemplateSectionID)))
                                                .OrderBy(rec => rec.DisplayOrder).FirstOrDefault(rec =>
                                                    !completedSections.Contains(rec.TemplateSectionID) &&
                                                    rec.TemplateSectionType != 35);

                                            if (role.ToString().Equals(OrganizationType.Vendor))
                                            {
                                                templateSectionsList = templateSections
                                                    .Where(rec =>
                                                        rec.Visible == true &&
                                                        (rec.HideFromPrequalification != true ||
                                                         hideSectionsToDisplay.Contains(rec.TemplateSectionID)) &&
                                                        allVisibleTemplateSections.Contains(rec.TemplateSectionID))
                                                    .OrderBy(rec => rec.DisplayOrder).FirstOrDefault(rec =>
                                                        !completedSections.Contains(rec.TemplateSectionID) &&
                                                        rec.TemplateSectionType != 35);
                                            }
                                        }

                                        if (pq.RequestForMoreInfo == null ||
                                            pq.RequestForMoreInfo == false)
                                        {
                                            if (templateSectionsList != null &&
                                                templateSectionsList.TemplateSectionType == 34)
                                            {
                                                templateSectionsList = templateSections
                                                    .Where(rec =>
                                                        rec.Visible == true &&
                                                        (rec.HideFromPrequalification != true ||
                                                         hideSectionsToDisplay.Contains(rec.TemplateSectionID)))
                                                    .OrderBy(rec => rec.DisplayOrder).FirstOrDefault(rec =>
                                                        !completedSections.Contains(rec.TemplateSectionID) &&
                                                        rec.TemplateSectionType != 34);

                                                if (role.ToString().Equals(OrganizationType.Vendor))
                                                {
                                                    templateSectionsList = selectedTemplate.TemplateSections
                                                        .Where(rec =>
                                                            rec.Visible == true &&
                                                            (rec.HideFromPrequalification != true ||
                                                             hideSectionsToDisplay.Contains(rec.TemplateSectionID)) &&
                                                            allVisibleTemplateSections.Contains(rec.TemplateSectionID))
                                                        .OrderBy(rec => rec.DisplayOrder).FirstOrDefault(rec =>
                                                            !completedSections.Contains(rec.TemplateSectionID) &&
                                                            rec.TemplateSectionType != 34);
                                                }

                                                //Ends<<
                                            }
                                        }

                                        if (templateSectionsList != null)
                                            templateSectionId = templateSectionsList.TemplateSectionID;
                                    }
                                }
                            }
                        }
                    }

                    catch (Exception ee)
                    {
                    }
                }
                return templateSectionId;
            }
        }

        public bool PqHasCompletedSection(long pqId, long templateSectionId)
        {
            using (var entity = new EFDbContext())
            {
                return entity.PrequalificationCompletedSections.Any(
                    rec =>
                        rec.PrequalificationId == pqId &&
                        rec.TemplateSectionId == templateSectionId);
            }
        }

        public List<LocalTemplatesSideMenuModel> GetPqSideMenusForQuestionnaire(long pqId, int? mode, Guid templateId,
            string role, Guid userId, ref long templateSectionId)
        {
            using (var entity = new EFDbContext())
            {
                List<TemplateSections> pqTemplateSections = GetTemplateSections(templateId);
                List<LocalTemplatesSideMenuModel> sideMenus = new List<LocalTemplatesSideMenuModel>();
                List<long> hideSectionsToDisplay = GetPQSectionsToDisplay(pqId);
                Prequalification pq = entity.Prequalification.Find(pqId);
                long TemplateSectionId = templateSectionId;
                List<TemplateSections> sideMenuSections = pqTemplateSections.Where(rec => rec.Visible == true && (rec.HideFromPrequalification != true || hideSectionsToDisplay.Contains(rec.TemplateSectionID))).OrderBy(x => x.DisplayOrder).ToList();
                if ((role.ToString().Equals("Client") || role.ToString().Equals("Super Client")) && pq != null)
                {
                    long[] prequalificationStatus = { };
                    int status = Array.IndexOf(prequalificationStatus, pq.PrequalificationStatusId);
                    if (status != -1)
                        sideMenuSections = sideMenuSections.Where(rec => rec.VisibleToClient == true && (rec.HideFromPrequalification != true || hideSectionsToDisplay.Contains(rec.TemplateSectionID))).OrderBy(x => x.DisplayOrder).ToList();

                    var allVisibleTemplateSectionsForClient = entity.TemplateSectionsPermission.Where(rec => rec.VisibleTo == 3 || rec.VisibleTo == 2).Select(rec => rec.TemplateSectionID).ToList();

                    allVisibleTemplateSectionsForClient.AddRange(entity.TemplateSectionsPermission.Where(rec => rec.VisibleTo == 4).Select(rec => rec.TemplateSectionID).ToList());
                    var hasAccess2 = FVGen3.BusinessLogic.RolesHelper.IsUserInRole(userId, LocalConstants.RESTRICTED_ACCESS_2_VIEW_ID, "read", pq.ClientId, pq.VendorId) || role.ToString().Equals("Super Admin");
                    if (hasAccess2)
                        allVisibleTemplateSectionsForClient.AddRange(entity.TemplateSectionsPermission.Where(rec => rec.VisibleTo == LocalConstants.SectionPermissionType.RESTRICTED_ACCESS_2).Select(rec => rec.TemplateSectionID).ToList());

                    sideMenuSections = sideMenuSections.Where(rec => allVisibleTemplateSectionsForClient.Contains(rec.TemplateSectionID)).ToList();

                    try
                    {
                        if (sideMenuSections.FirstOrDefault(rec => rec.TemplateSectionID == TemplateSectionId) == null)
                            templateSectionId = sideMenuSections.FirstOrDefault().TemplateSectionID;
                    }
                    catch
                    {
                        templateSectionId = -1;
                    }
                }
                var allVisibleTemplateSections = entity.TemplateSectionsPermission
                    .Where(rec =>
                        rec.TemplateSections.TemplateID == templateId &&
                        rec.VisibleTo == 3 || rec.VisibleTo == 1)
                    .Select(rec => rec.TemplateSectionID).ToList();

                if (!role.Equals(OrganizationType.Vendor))
                {
                    allVisibleTemplateSections.AddRange(entity.TemplateSectionsPermission
                        .Where(rec => rec.VisibleTo == 4)
                        .Select(rec => rec.TemplateSectionID).ToList());
                }
                else
                {
                    allVisibleTemplateSections.AddRange(entity.TemplateSectionsPermission
                        .Where(rec => rec.VisibleTo == LocalConstants.SectionPermissionType.RESTRICTED_ACCESS_2)
                        .Select(rec => rec.TemplateSectionID).ToList());
                }
                if (role.ToString().Equals("Vendor"))
                {
                    sideMenuSections = sideMenuSections.Where(rec => allVisibleTemplateSections.Contains(rec.TemplateSectionID)).ToList();
                }

                var initial = true;

                if (role.ToString() != "Vendor")
                {
                    LocalTemplatesSideMenuModel vendorDetailsMenu = new LocalTemplatesSideMenuModel("Vendor Details", "VendorDetails", "AdminVendors", false, "SelectedVendorId=" + pq.VendorId + "&PreQualificationId=" + pqId + "&TemplateSectionID=-1" + "&mode=" + mode);
                    vendorDetailsMenu.isVisible = true;
                    vendorDetailsMenu.isSectionCompleted = null;
                    sideMenus.Add(vendorDetailsMenu);
                }
                foreach (TemplateSections section in sideMenuSections)
                {
                    if (initial && role.ToString() == "Admin" && mode != 1)
                    {
                        LocalTemplatesSideMenuModel menu1 = new LocalTemplatesSideMenuModel("Change Status", "ChangeStatus", "Prequalification", false, "templateId=" + section.TemplateID + "&PreQualificationId=" + pqId + "&mode=" + mode);
                        menu1.isVisible = true;
                        menu1.isSectionCompleted = null;
                        sideMenus.Add(menu1);
                        initial = false;
                    }

                    if (role.ToString().Equals("Vendor"))
                    {
                        if (pq.PrequalificationStatusId == 7 && !entity.PrequalificationCompletedSections.Any(
                                rec =>
                                    rec.PrequalificationId == pqId &&
                                    rec.TemplateSectionId == section.TemplateSectionID)
                        )
                        {
                            mode = 0;
                        }
                    }
                    LocalTemplatesSideMenuModel menu = new LocalTemplatesSideMenuModel(section.SectionName, "TemplateSectionsList", "Prequalification", templateSectionId == section.TemplateSectionID, "templateId=" + section.TemplateID + "&TemplateSectionID=" + section.TemplateSectionID + "&PreQualificationId=" + pqId + "&mode=" + mode);

                    if (role.ToString() == "Vendor" && (pq.PaymentReceived == null || pq.PaymentReceived == false))
                    {
                        int[] userPermissionSections = { 0, 3, 31, 32, 7, 40 };
                        if (Array.IndexOf(userPermissionSections, section.TemplateSectionType) == -1)
                            menu.isVendorNoPaymentNoAccess = false;
                    }
                    menu.isSectionCompleted = (entity.PrequalificationCompletedSections.FirstOrDefault(rec => rec.PrequalificationId == pqId && rec.TemplateSectionId == section.TemplateSectionID) != null);

                    menu.isVisible = true;
                    if (section.TemplateSectionType == 34)
                    {
                        if (pq.RequestForMoreInfo == null || pq.RequestForMoreInfo == false)
                            menu.isVisible = false;
                    }

                    if (section.HideFromPrequalification != null && section.HideFromPrequalification == true)
                    {
                        var sectionToDisplay = entity.PrequalificationSectionsToDisplay.FirstOrDefault(
                            rec =>
                                rec.PrequalificationId == pq.PrequalificationId &&
                                rec.TemplateSectionId == section.TemplateSectionID && rec.Visible == true);
                        if (sectionToDisplay == null)
                            menu.isVisible = false;
                    }
                    if (section.TemplateSectionType == 35 && role.ToString().Equals("Vendor"))
                    {
                        //Do nothing
                    }
                    else
                        sideMenus.Add(menu);
                }
                bool hasTrainingTemplates = CheckClientHasTrainingTemplates(pq.ClientId, pqId);
                if (hasTrainingTemplates)
                {
                    if (role.ToString() == "Admin" || role.ToString() == "Client")
                    {
                        var menu2 = new LocalTemplatesSideMenuModel("Employees", "Employees", "Prequalification", false,
                            "templateId=" + templateId + "&PreQualificationId=" + pqId);
                        menu2.isVisible = true;
                        menu2.isSectionCompleted = null;
                        initial = false;

                        if (role.ToString() == "Admin" && mode != 1)
                        {
                            sideMenus.Insert(2, menu2);
                        }
                        else
                        {
                            sideMenus.Insert(1, menu2);
                        }

                    }
                }

                return sideMenus;
            }
        }

        public StatusBannerColor GetBuStatusBannerColor(long pqId, long? buId, string role)
        {
            using (var entity = new EFDbContext())
            {
                StatusBannerColor buStatusBannerColor = new StatusBannerColor();
                Prequalification pq = entity.Prequalification.Find(pqId);
                if (pq != null)
                {
                    if (role.ToString() == "Admin" || (role.ToString() == "Client") || (role.ToString() == "Vendor"))
                    {
                        buStatusBannerColor.BannerColor = pq.PrequalificationStatus.BannerColor;
                        if (buId != null)
                        {
                            PrequalificationSites pqSite = entity.PrequalificationSites
                                    .FirstOrDefault(rec =>
                                        rec.ClientBusinessUnitId == buId &&
                                        rec.PrequalificationId == pqId);
                            if (pqSite != null)
                            {
                                buStatusBannerColor.BannerColor =  pqSite.SiteStatus.BannerColor;
                            }
                        }
                    }

                    if (buId != null)
                    {
                        var pqSiteBusinessUnit = entity.PrequalificationSites.FirstOrDefault(rec =>
                            rec.ClientBusinessUnitId == buId && rec.PrequalificationId == pqId);
                        if(pqSiteBusinessUnit != null)
                        {
                            buStatusBannerColor.StatusName = pqSiteBusinessUnit.SiteStatus.PrequalificationStatusName;
                        }
                    }
                }
                return buStatusBannerColor;
            }
        }

        public void LogEmailNotification(LocalEmailComposeModel form, Guid userId)
        {
            using (var entity = new EFDbContext())
            {
                long notificationId = 0;
                
                Prequalification pq = entity.Prequalification.Find(form.PrequalificationId);
                if (pq != null)
                {
                    List<OrganizationsNotificationsSent> orgNotesSent = entity.OrganizationsNotificationsSent
                        .Where(rec => rec.ClientId == pq.ClientId && rec.SetupType == 2).ToList();
                    if (orgNotesSent.Count == 0)
                    {
                        OrganizationsNotificationsSent newOrgNoteSent = new OrganizationsNotificationsSent();
                        newOrgNoteSent.ClientId = pq.ClientId;
                        newOrgNoteSent.SetupType = 2;
                        newOrgNoteSent.SetupTypeId = null;
                        entity.OrganizationsNotificationsSent.Add(newOrgNoteSent);
                        entity.SaveChanges();

                        notificationId = newOrgNoteSent.NotificationId;
                    }
                    else
                    {
                        notificationId = orgNotesSent[0].NotificationId;
                    }

                    var maxNotificationNo = 0;
                    try
                    {
                        maxNotificationNo = entity.OrganizationsNotificationsEmailLogs
                            .Where(record =>
                                record.PrequalificationId == pq.PrequalificationId && record.NotificationType == 1)
                            .Max(record => record.NotificationNo);
                    }
                    catch (Exception e)
                    {
                    }

                    OrganizationsNotificationsEmailLogs notifiLogs = new OrganizationsNotificationsEmailLogs();
                    notifiLogs.NotificationId = notificationId;
                    notifiLogs.NotificationType = 0; 
                    notifiLogs.NotificationNo = maxNotificationNo + 1;
                    notifiLogs.VendorId = pq.VendorId;
                    notifiLogs.PrequalificationId = pq.PrequalificationId;
                    notifiLogs.EmailSentDateTime = DateTime.Now;
                    notifiLogs.MailSentBy = userId;
                    notifiLogs.ReadByAdmin = 0;
                    notifiLogs.ReadByClient = 0;
                    notifiLogs.ReadByVendor = 0;
                    notifiLogs.EmailTemplateId = form.TemplateId;
                    notifiLogs.EmailSentTo = form.ToAddress;
                    notifiLogs.EmailBcc = form.BCCAddress;
                    notifiLogs.EmailCc = form.CCAddress;
                    if (form.EmailComment != null && form.EmailComment.Trim() != "")
                        notifiLogs.Comments = form.EmailComment;
                    entity.OrganizationsNotificationsEmailLogs.Add(notifiLogs);

                    entity.SaveChanges();
                }
            }
        }
        //public bool CheckReviewDate(long ClientId,long VendorId,Guid UserId)
        //{
        //    using (var context = new EFDbContext())
        //    {
        //        var ReviewInput = context.VendorReviewInput.FirstOrDefault(r => r.ClientId == ClientId && r.VendorId == VendorId && r.InputUserId == UserId);
        //        return ReviewInput != null && ReviewInput.ReviewDate != null ? true : false; 
        //    }
        //}
    }
}
