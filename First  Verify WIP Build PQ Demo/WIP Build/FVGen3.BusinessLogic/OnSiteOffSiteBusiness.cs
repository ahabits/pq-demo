/*
Page Created Date:  04/12/2017
Created By: Rajesh Pendela
Purpose: All Business operations related to Onsite/Offsite module will be performed here
Version: 1.0
****************************************************
*/

using FVGen3.DataLayer.DTO;
using FVGen3.Domain.Concrete;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using FVGen3.BusinessLogic.Interfaces;
using System.Web;
using System;
using System.Data.Entity;
using Resources;
using FVGen3.Domain.ViewModels;

namespace FVGen3.BusinessLogic
{
    public class OnSiteOffSiteBusiness: BaseBusinessClass, IOnSiteOffSiteBusiness
    {
        ISystemUserBusiness _SysBusiness;
       
        public OnSiteOffSiteBusiness(ISystemUserBusiness _SysBusiness)
        {
           
          
            this._SysBusiness = _SysBusiness;
           
           
        }
        /// <summary>
        /// To get the checkin history
        /// </summary>
        /// <param name="location"></param>
        /// <param name="workingOffset"></param>
        /// <returns></returns>
        public List<OnSiteOffSiteCheckin> GetCheckins(string location, int workingOffset,Guid UserId,long OrgId)
        {
            using(var entityDB=new EFDbContext())
            {
                bool IsOrmatUser = _SysBusiness.CheckIsOrmatUser(UserId);
                var checkoutTime = LocalConstants.Checkout;
                if (IsOrmatUser) { checkoutTime = LocalConstants.OrmatCheckout; }
                var OnSiteOffSiteHelper = new OnSiteOffSiteHelper();
                
                var data = OnSiteOffSiteHelper.GetCheckinsQuery(workingOffset,location, checkoutTime, OrgId);
                return entityDB.Database.SqlQuery<OnSiteOffSiteCheckin>(data,new SqlParameter("@location", location),new SqlParameter("@workingOffset", workingOffset)).ToList();
                 
            }
            
        }
        public string ResetTagorBadgeNumber(Guid EmployeeId,bool IsTagNumber, Guid OnsiteOffsiteID, bool IsVisitor)
        {
            using (var context = new EFDbContext())
            {
                int? i1 = 0;
                if (!IsVisitor)
                {
                    var Employee = context.SystemUsers.Find(EmployeeId);
                    var OnsiteOffsite = context.OnsiteOffsites.Find(OnsiteOffsiteID);
                    if (IsTagNumber)
                    {
                        Employee.TagNumber = i1;
                        OnsiteOffsite.TagNumber = null;
                    }
                    else
                    {
                        Employee.Badgenumber = i1;
                        OnsiteOffsite.BadgeNumber = null;
                    }
                    context.Entry(Employee).State = EntityState.Modified;
              context.Entry(OnsiteOffsite).State = EntityState.Modified;
                }
                else
                {
                    var onsiteoffsitevisitor = context.OnsiteOffsiteVisitor.Find(OnsiteOffsiteID);
                    if (IsTagNumber)
                    {
                        //Employee.TagNumber = i1;
                        onsiteoffsitevisitor.TagNumber = null;
                    }
                    else
                    {
                        //Employee.Badgenumber = i1;
                        onsiteoffsitevisitor.BadgeNumber = null;
                    }
                    context.Entry(onsiteoffsitevisitor).State = EntityState.Modified;
                }
                context.SaveChanges();
            }
            return "";
        }
        public List<CurrentlyOnsite> GetSineProOnsiteData(long clientID)
        {
            using (var entity = new EFDbContext())
            {
                var OnSiteOffSiteHelper = new OnSiteOffSiteHelper();

                var data = OnSiteOffSiteHelper.GetSineProCheckin(clientID);
                var sineprodata = entity.Database.SqlQuery<CurrentlyOnsite>(data).ToList();
                // var result = new DataSourceResponse() { Total = sineprodata.First(), Data = sineprodata };
              //  sineprodata= sineprodata.ForEach(r=>r.Checkin= )



                return sineprodata;
            }
        }
    }
}
