﻿/*
Page Created Date:  11/17/2017
Created By: Rajesh Pendela
Purpose: All Business operations related to client will be performed here
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using FVGen3.BusinessLogic.Interfaces;
using FVGen3.Domain.Concrete;
using FVGen3.Domain.Entities;
using System.Data.Entity;
using FVGen3.DataLayer.DTO;
using FVGen3.Domain.LocalModels;
using FVGen3.WebUI.Constants;
using FVGen3.Domain.ViewModels;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Resources;

namespace FVGen3.BusinessLogic
{
    public class ClientBusiness : BaseBusinessClass, IClientBusiness
    {

        //IVendorBusiness _vendor;
        IPrequalificationBusiness _preq;
        public ClientBusiness()
        {
            //_vendor = vendor;
            //this._vendor = new VendorBusiness(_preq);
        }


        /// <summary>
        /// To get the Client Contract Types
        /// </summary>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public List<KeyValuePair<string, string>> GetClientContractTypes(long clientId)
        {
            using (EFDbContext context = new EFDbContext())
            {
                return context.ClientContractTypes.Where(rec => rec.ClientId == clientId).ToList().
                 Select(rec =>
                 new KeyValuePair<string, string>(rec.ClientContractId.ToString(), rec.ContractType)).ToList();
            }
        }

        /// <summary>
        /// To get the Client Templates
        /// </summary>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public List<KeyValuePair<string, string>> GetClientTemplates(long clientId)
        {
            using (EFDbContext context = new EFDbContext())
            {
                return context.ClientTemplates.Where(r => r.ClientID == clientId && r.Templates.TemplateType == 0 && r.Templates.TemplateStatus == 1).Select(r => new { r.Templates.TemplateName, r.Templates.TemplateID }).OrderBy(rec => rec.TemplateName).ToList().
                    Select(rec =>
                        new KeyValuePair<string, string>(rec.TemplateID.ToString(), rec.TemplateName)).ToList();
            }
        }
        /// <summary>
        /// To get the Client fee templates
        /// </summary>
        /// <param name="clientid"></param>
        /// <returns></returns>
        public List<KeyValuePair<string, string>> GetFeeTemplates(long clientid)
        {
            using (EFDbContext context = new EFDbContext())
            {
                var templates = context.ClientTemplates.Where(r => r.ClientID == clientid).Select(r => new
                {
                    r.Templates.TemplateName,
                    r.Templates.TemplateID
                }).OrderBy(rec => rec.TemplateName).ToList().
                          Select(rec =>
                              new KeyValuePair<string, string>(rec.TemplateID.ToString(), rec.TemplateName)).ToList();

                templates.Insert(0, new KeyValuePair<string, string>("0", "Any"));
                return templates;
            }
        }

        /// <summary>
        /// To get the restricted client organizations
        /// </summary>
        /// <param name="superClient"></param>
        /// <returns></returns>
        public List<long> GetRestrictedClients(long superClient)
        {
            using (EFDbContext context = new EFDbContext())
            {
                var orgList = context.Organizations.Where(r => r.ClientTemplates.Any(r1 => r1.Templates.IsERI == false && r1.Templates.TemplateType == 0 && r1.Templates.IsHide == false)).Select(r => r.OrganizationID).ToList();
                var orgList1 = context.SubClients.Where(r => r.SuperClientId != superClient).Distinct().Select(r => r.ClientId).ToList();
                orgList.AddRange(orgList1);
                return orgList;
            }
        }

        /// <summary>
        /// To get the client organizations
        /// </summary>
        /// <returns></returns>
        public List<KeyValuePair<string, string>> GetClients()
        {
            using (EFDbContext context = new EFDbContext())
            {
                return context.Organizations.Where(r => r.OrganizationType == "Client").Select(r => new { r.OrganizationID, r.Name }).OrderBy(rec => rec.Name).ToList().
                    Select(rec =>
                        new KeyValuePair<string, string>(rec.OrganizationID.ToString(), rec.Name)).ToList();
            }
        }

        public List<KeyValuePair<string, string>> GetAllClients()
        {
            using (EFDbContext context = new EFDbContext())
            {
                return context.Organizations.Where(r => r.OrganizationType == "Client" || r.OrganizationType == OrganizationType.SuperClient).Select(r => new { r.OrganizationID, r.Name }).OrderBy(rec => rec.Name).ToList().
                    Select(rec =>
                        new KeyValuePair<string, string>(rec.OrganizationID.ToString(), rec.Name)).ToList();
            }
        }

        /// <summary>
        /// To get the specific client organizations
        /// </summary>
        /// <param name="clientIds"></param>
        /// <returns></returns>
        public List<KeyValuePair<string, string>> GetClients(IEnumerable<long> clientIds)
        {
            using (EFDbContext context = new EFDbContext())
            {
                return context.Organizations.Where(row => clientIds.Contains(row.OrganizationID)).Select(r => new { r.OrganizationID, r.Name }).OrderBy(rec => rec.Name).ToList().
                    Select(rec =>
                        new KeyValuePair<string, string>(rec.OrganizationID.ToString(), rec.Name)).ToList();
            }
        }

        /// <summary>
        /// To get the ERI clients by region
        /// </summary>
        /// <param name="pqId"></param>
        /// <param name="regions"></param>
        /// <returns></returns>
        public List<KeyValuePair<string, string>> GetERIClientsByRegion(long pqId, string regions = "1")
        {
            //for now regions are not using...
            //if (string.IsNullOrEmpty(regions))
            //    regions = "1";
            regions = "1";
            using (EFDbContext context = new EFDbContext())
            {
                var regionsArray = regions.Split(',').Select(int.Parse);
                var statesList = context.RegionStates.Where(r => regionsArray.Contains(r.Regionid)).Select(r => r.StateName.ToLower()).ToList();

                if (regionsArray.Contains(RegionsConstant.INTERNATIONAL))
                {
                    var interNationalRegions = context.ClientBusinessUnitSites.Where(r => !context.RegionStates.Any(r1 => r1.StateName == r.BusinessUnitLocation)).Select(r => r.BusinessUnitLocation).Distinct().ToList();
                    statesList.AddRange(interNationalRegions);
                }
                var clients = context.Prequalification.Find(pqId).ClientTemplates.Templates.ClientTemplates.Where(r => r.Visible).Select(r => r.ClientID);
                //return context.ClientBusinessUnitSites.Where(r => statesList.Contains(r.BusinessUnitLocation.ToLower())).
                //        Select(r => r.ClientBusinessUnit.Organization)
                //        .Where(r => clients.Contains(r.OrganizationID) && r.OrganizationID != -1).Distinct().
                //        Select(r => new
                //        {
                //        r.OrganizationID,
                //        r.Name
                //        }).OrderBy(rec => rec.Name).ToList().
                //        Select(rec =>
                //            new KeyValuePair<string, string>(rec.OrganizationID.ToString(), rec.Name)).ToList();
                return context.Organizations
                    .Where(r => clients.Contains(r.OrganizationID) && r.OrganizationType != "Super Client").Distinct().
                    Select(r => new { r.OrganizationID, r.Name }).OrderBy(rec => rec.Name).ToList().
                    Select(rec =>
                        new KeyValuePair<string, string>(rec.OrganizationID.ToString(), rec.Name)).ToList();
            }
        }
        /// <summary>
        /// Will return the data which was involved in PQ Sites
        /// </summary>
        public IEnumerable<KeyValuePair<long, string>> GetPQClientsInSites(long pqId)
        {

            using (EFDbContext context = new EFDbContext())
            {
                return context.Prequalification.Find(pqId).PrequalificationSites.Where(r => r.ClientId != null).Select(r => new { r.ClientId, r.Client.Name }).Distinct().ToList().Select(r => new KeyValuePair<long, string>((long)r.ClientId, r.Name));
            }
        }

        /// <summary>
        /// To get the regions of specific prequalification
        /// </summary>
        /// <param name="pqId"></param>
        /// <returns></returns>
        public List<KeyValuePair<string, string>> GetERIsRegion(long pqId)
        {
            //var regionsArray = regions.Split(',').Select(r => r.ToLower());
            using (EFDbContext context = new EFDbContext())
            {
                //var clientIds = context.Prequalification.Find(pqId).ClientTemplates.Templates.ClientTemplates.Where(r => r.ClientID != -1).Select(r => r.ClientID).ToList();
                //var PQClientSites = context.ClientRegions.Where(rec => clientIds.Contains(rec.Clientid)).Select(r => new { r.Regionid, r.Regions.RegionName }).Distinct().ToList();
                //return PQClientSites.Select(rec => new KeyValuePair<string, string>(rec.Regionid + "", rec.RegionName)).ToList();
                //var PQClientSites = context.Regions.Where(rec => clientIds.Contains(rec.Clientid)).Select(r => new { r.Regionid, r.Regions.RegionName }).Distinct().ToList();
                return context.Regions.Select(r => new { r.RegionId, r.RegionName }).OrderBy(r => r.RegionName).ToList().Select(rec =>
                          new KeyValuePair<string, string>(rec.RegionId + "", rec.RegionName)).ToList();

                //return context.ClientBusinessUnits.Where(r=>clients.Contains(r.Organization.OrganizationID)).SelectMany(r => r.ClientBusinessUnitSites).Select(r => new { r.BusinessUnitLocation }).Distinct().OrderBy(rec => rec.BusinessUnitLocation).ToList().
                //    Select(rec =>
                //        new KeyValuePair<string, string>(rec.BusinessUnitLocation, rec.BusinessUnitLocation)).ToList();
            }
        }

        /// <summary>
        /// To check the prequalification template is ERI
        /// </summary>
        /// <param name="pqId"></param>
        /// <returns></returns>
        public bool CheckTemplateIsERI(long pqId)
        {
            using (EFDbContext context = new EFDbContext())
            {
                return context.Prequalification.Find(pqId).ClientTemplates.Templates.IsERI;
            }
        }
        /// <summary>
        /// To get the documents supported by client specific to the prequalification
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="prequalificationId"></param>
        /// <param name="SectionId"></param>
        /// <returns></returns>
        public List<KeyValuePair<string, string>> GetClientSupportingDocs(long clientId, long prequalificationId, long SectionId)
        {
            using (EFDbContext context = new EFDbContext())
            {
                var templateId = context.Prequalification.Find(prequalificationId).ClientTemplates.TemplateID;
                var clientTemplateId = context.ClientTemplates.FirstOrDefault(rec => rec.ClientID == clientId && rec.TemplateID == templateId).ClientTemplateID;

                var documentTypeIds = context.ClientTemplateReportingData.Where(rec => rec.ReportingType == 2 && rec.ClientTemplateId == clientTemplateId && rec.SectionId == SectionId).Select(rec => rec.ReportingTypeId).ToList();

                return (from DocType in context.DocumentType.Where(rec => rec.PrequalificationUseOnly == true && documentTypeIds.Contains(rec.DocumentTypeId)).OrderBy(rec => rec.DocumentTypeName).ToList()
                        select new KeyValuePair<string, string>(DocType.DocumentTypeId + "", DocType.DocumentTypeName)).ToList();
            }
        }

        /// <summary>
        /// To check whether the specif document is expires
        /// </summary>
        /// <param name="preQualification"></param>
        /// <param name="docTypeId"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public bool isDocumentExpire(long preQualification, long docTypeId, long clientId)
        {
            using (EFDbContext entityDB = new EFDbContext())
            {
                var templateId = entityDB.Prequalification.Find(preQualification).ClientTemplates.TemplateID;
                var clientTemplateId = entityDB.ClientTemplates.FirstOrDefault(row => row.ClientID == clientId && row.TemplateID == templateId).ClientTemplateID;
                try
                {
                    return entityDB.ClientTemplateReportingData.FirstOrDefault(rec => rec.ReportingType == 2 && rec.ReportingTypeId == docTypeId && rec.ClientTemplateId == clientTemplateId).DocumentExpires ?? false;
                }
                catch { }
                return false;
            }
        }
        /// <summary>
        /// To get the client business units
        /// </summary>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public List<KeyValuePair<string, string>> GetClientBusinessUnits(long clientId)
        {
            using (EFDbContext entityDB = new EFDbContext())
            {
                return (from busUnits in entityDB.ClientBusinessUnits.Where(rec => rec.ClientId == clientId).OrderBy(rec => rec.BusinessUnitName).ToList()
                        select new KeyValuePair<string, string>(busUnits.ClientBusinessUnitId.ToString(), busUnits.BusinessUnitName)).ToList();

            }
        }
        /// <summary>
        /// To get the client specified business units sites
        /// </summary>
        /// <param name="buIds"></param>
        /// <returns></returns>
        public IEnumerable<KeyValuePair<string, string>> GetClientBUSitesByBU(string buIds)
        {
            using (EFDbContext entityDB = new EFDbContext())
            {
                var bus = buIds.Split(',').Select(Int64.Parse).ToList();
                return (from buSites in entityDB.ClientBusinessUnitSites.Where(row => bus.Contains(row.ClientBusinessUnitId)).OrderBy(r => r.SiteName).ToList()
                        select new KeyValuePair<string, string>(buSites.ClientBusinessUnitSiteId.ToString(), buSites.SiteName));
            }
        }
        /// <summary>
        /// To check whether the specific client is part of ERI program
        /// </summary>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public bool CheckClientIsInERIClients(long clientId)
        {
            using (EFDbContext entity = new EFDbContext())
            {
                return entity.ClientTemplates.Any(row => row.ClientID == clientId && row.Visible && row.Templates.IsERI == true);
            }
        }

        /// <summary>
        /// To get the specific prequalification sites
        /// </summary>
        /// <param name="pqId"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public List<PQSites> GetPQClientLocations(long pqId, long clientId)
        {
            var PQSites = new List<PQSites>();
            using (EFDbContext entity = new EFDbContext())
            {
                var PQClientSites = entity.PrequalificationSites.Where(rec => rec.PrequalificationId == pqId && (clientId == 0 || rec.ClientId == clientId));
                PQSites = PQClientSites.Select(rec => new PQSites
                {
                    PQSiteId = rec.PrequalificationSitesId,
                    BUSiteId = rec.ClientBusinessUnitSiteId,
                    ClientName = rec.Client.Name,
                    PQSiteName = rec.ClientBusinessUnitSites.SiteName,
                    PQSiteStatusId = rec.PQSiteStatus ?? rec.Prequalification.PrequalificationStatusId,
                    PQSiteStatusName = rec.PrequalificationStatus.PrequalificationStatusName ?? rec.Prequalification.PrequalificationStatus.PrequalificationStatusName,
                    VendorIndicationText = rec.PrequalificationStatus.IndicationTextVendor ?? rec.Prequalification.PrequalificationStatus.IndicationTextVendor,
                    PQSiteClientId = rec.ClientId,
                    NotificationExistForSite = entity.LocationStatusLog.Any(row => row.pqSiteId == rec.PrequalificationSitesId)
                }).ToList();
                return PQSites;
            }
        }
        //public List<ClientRegionsDTO> GetPQClientRegions(long pqId, List<long> clientIds)
        //{
        //    var PQSites = new List<ClientRegionsDTO>();
        //    using (var entity = new EFDbContext())
        //    {
        //        var PQClientSites = entity.ClientRegions.Where(rec => clientIds.Contains(rec.Clientid));
        //        PQSites = PQClientSites.Select(rec => new ClientRegionsDTO {ClientId=rec.Clientid,RegionId=rec.Regionid,RegionName=rec.Region.RegionName }).ToList();
        //        return PQSites;
        //    }
        //}

        /// <summary>
        /// To get the different prequalification statuses available in the system
        /// </summary>
        /// <returns></returns>
        public IEnumerable<KeyValuePair<string, string>> GetPQStatuses()
        {
            using (EFDbContext entity = new EFDbContext())
            {
                return (from pqStatuses in entity.PrequalificationStatus.Where(rec => rec.PrequalificationStatusId > 2).ToList()
                        select new KeyValuePair<string, string>(pqStatuses.PrequalificationStatusId.ToString(), pqStatuses.PrequalificationStatusName));
            }
        }

        /// <summary>
        /// To update the location status
        /// </summary>
        /// <param name="pqSiteId"></param>
        /// <param name="pqSiteStatusId"></param>
        /// <returns></returns>
        public bool UpdateLocationStatus(long pqSiteId, long pqSiteStatusId)
        {
            using (EFDbContext entity = new EFDbContext())
            {
                var pqSite = entity.PrequalificationSites.Find(pqSiteId);
                pqSite.PQSiteStatus = pqSiteStatusId;

                entity.Entry(pqSite).State = System.Data.Entity.EntityState.Modified;
                entity.SaveChanges();
                return true;
            }
        }

        /// <summary>
        /// To log the site status change
        /// </summary>
        /// <param name="assignStatus"></param>
        /// <returns></returns>
        public bool LogSiteStatusChange(AssignLocationStatus assignStatus)
        {
            using (EFDbContext entity = new EFDbContext())
            {
                LocationStatusLog logSiteStatusChange = new LocationStatusLog();
                logSiteStatusChange.pqSiteId = assignStatus.PrequalificationSiteId;
                logSiteStatusChange.pqSiteStatusId = assignStatus.PrequalificationStatusId;
                logSiteStatusChange.EmailSentTo = assignStatus.SendTo;
                logSiteStatusChange.EmailSentCC = assignStatus.SendCC;
                logSiteStatusChange.StatusChangedBy = assignStatus.StatuschangedBy;
                logSiteStatusChange.StatusChangeDate = DateTime.Now;

                entity.LocationStatusLog.Add(logSiteStatusChange);
                entity.SaveChanges();
                return true;
            }
        }
        /// <summary>
        /// To get the clients participating in ERI program
        /// </summary>
        /// <returns></returns>
        public IEnumerable<KeyValuePair<string, string>> GetEriClients()
        {
            using (EFDbContext context = new EFDbContext())
            {
                return context.Templates.Where(r => r.IsERI == true).SelectMany(r => r.ClientTemplates)
                    .Select(r => r.Organizations).Where(r => r.OrganizationID != -1).ToList().Select(r => new KeyValuePair<string, string>(r.OrganizationID.ToString(), r.Name)).Distinct();

            }
        }

        /// <summary>
        /// To get the sub clients of specific organization
        /// </summary>
        /// <param name="orgId"></param>
        /// <returns></returns>
        public List<KeyValuePair<string, string>> GetSubClients(long orgId, bool includeSuperClient = false)
        {
            using (EFDbContext context = new EFDbContext())
            {
                var res = context.SubClients.Where(r => r.SuperClientId == orgId && r.IsActive == true).ToList().Select(r => new KeyValuePair<string, string>(r.ClientId.ToString(), r.SubClientOrganization.Name)).ToList();
                if (includeSuperClient)
                {
                    var superClient = context.Organizations.Find(orgId);
                    res.Add(new KeyValuePair<string, string>(superClient.OrganizationID.ToString(), superClient.Name));
                }
                return res;
            }
        }

        /// <summary>
        /// To update the client secret key
        /// </summary>
        /// <param name="organizationId"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public string UpdateKey(long organizationId, string key)
        {
            using (EFDbContext context = new EFDbContext())
            {
                var org = context.Organizations.Find(organizationId);
                if (org != null)
                {
                    org.ClientSecret = key;
                    context.Entry(org).State = EntityState.Modified;
                    context.SaveChanges();
                }
                return key;
            }
        }
        public string UpdateTemplateKey(long organizationId, string key, Guid TemplateId)
        {
            using (EFDbContext context = new EFDbContext())
            {
                var org = context.MultipleInvitationCode.FirstOrDefault(r => r.ClientId == organizationId && r.TemplateId == TemplateId);
                if (org != null)
                {
                    org.ClientCode = key;
                    context.Entry(org).State = EntityState.Modified;

                }
                else
                {
                    MultipleInvitationCode Code = new MultipleInvitationCode();
                    Code.ClientCode = key;
                    Code.ClientId = organizationId;
                    Code.TemplateId = TemplateId;
                    Code.ClientName = context.Organizations.Find(organizationId).Name;
                    Code.TemplateName = context.Templates.Find(TemplateId).TemplateName;
                    context.MultipleInvitationCode.Add(Code);
                }
                context.SaveChanges();
                return key;
            }
        }
        public List<LocalBusinessSites> GetClientBusinessUnitSites(long ClientId)
        {
            using (EFDbContext context = new EFDbContext())
            {
                var SubClients = GetSubClients(ClientId).Select(r => long.Parse(r.Key)).ToList();
                return context.ClientBusinessUnits.Where(r => r.ClientId == ClientId || SubClients.Contains(r.ClientId)).SelectMany(r => r.ClientBusinessUnitSites).Select(r => new LocalBusinessSites() { BusinessUnitID = r.ClientBusinessUnitId, BusinessUnitName = r.ClientBusinessUnit.BusinessUnitName, BusinessUnitSiteID = r.ClientBusinessUnitSiteId, BusinesUnitsiteName = r.SiteName }).ToList();
            }

        }
        public PQBusinessUnitDocments GetPQBusinessUnitDocuments(long pqid, string Role, Guid UserId)
        {
            using (EFDbContext context = new EFDbContext())
            {

                //var docs = context.Document.Where(r => r.ReferenceType == "ClientBusinessUnitSites").Select(r => r.ReferenceRecordID).ToList();
                //var PQSites = context.PrequalificationSites.Where(r => r.PrequalificationId == pqid);
                //var siteunitIds = PQSites.Select(r => r.ClientBusinessUnitSiteId);
                var sql = @"select ps.* from PrequalificationSites ps join Document d on d.ReferenceRecordID=ps.ClientBusinessUnitSiteId 
and d.ReferenceType = 'ClientBusinessUnitSites' where ps.PrequalificationId = @pqid ";
                var siteIds = "";
                if (Role.Equals(OrganizationType.Client))
                {

                    var sites = context.SystemUsersBUSites.FirstOrDefault(rec => rec.SystemUserId == UserId);

                    //PQSites = PQSites.Where(r => r.ClientBusinessUnitSiteId != null);
                    if (sites != null && sites.BUSiteIDsList.Count() > 0)
                    {
                        siteIds = string.Join(",", sites.BUSiteIDsList);
                        //PQSites = PQSites.Where(r => SiteIds.Contains((long)r.ClientBusinessUnitSiteId));
                        sql += "and ClientBusinessUnitSiteId in(@sites)";
                    }
                }

                var PQSites = context.PrequalificationSites.SqlQuery(sql,
                    new SqlParameter("@sites", siteIds),
                        new SqlParameter("@pqid", pqid)).ToList();
                PQBusinessUnitDocments pqdocs = new PQBusinessUnitDocments();
                pqdocs.BuSites = new List<BuSiteDocs>();
                pqdocs.Bus = new List<BuDocs>();
                foreach (var pqsites in PQSites)
                {
                    List<DocumentsList> docsList = new List<DocumentsList>();
                    BuSiteDocs Busites = new BuSiteDocs();
                    Busites.BusinessUnitId = pqsites.ClientBusinessUnitId.Value;
                    Busites.BusinessUnitName = pqsites.ClientBusinessUnits.BusinessUnitName;
                    Busites.BusinessUnitSiteId = pqsites.ClientBusinessUnitSiteId.Value;
                    Busites.BusinessUnitsiteName = pqsites.ClientBusinessUnitSites.SiteName;
                    Busites.ClientName = pqsites.Client.Name;
                    foreach (var budocsList in context.Document.Where(r => r.ReferenceType == "ClientBusinessUnitSites" && r.ReferenceRecordID == pqsites.ClientBusinessUnitSiteId.Value).ToList())
                    {
                        DocumentsList BuDocs = new DocumentsList();
                        BuDocs.ClientId = pqsites.ClientId ?? pqsites.Prequalification.ClientId;
                        BuDocs.DocumentName = budocsList.DocumentName;
                        BuDocs.DocumentId = budocsList.DocumentId;
                        docsList.Add(BuDocs);
                    }
                    Busites.documents = docsList;
                    pqdocs.BuSites.Add(Busites);

                }
                foreach (var BuId in PQSites.Select(r => r.ClientBusinessUnitId).Distinct())
                {
                    List<DocumentsList> docsList = new List<DocumentsList>();
                    var Docs = context.Document.Where(r => r.ReferenceRecordID == BuId && r.ReferenceType == "Business Unit").ToList();
                    if (Docs.Count() == 0) continue;

                    BuDocs Bu = new BuDocs();
                    Bu.BusinessUnitId = BuId.Value;
                    var BuName = context.ClientBusinessUnits.FirstOrDefault(r => r.ClientBusinessUnitId == BuId).BusinessUnitName;
                    Bu.BusinessUnitName = BuName;
                    var Client = context.PrequalificationSites.FirstOrDefault(r => r.PrequalificationId == pqid && r.ClientBusinessUnitId == BuId).Client;


                    Bu.ClientName = Client.Name;
                    foreach (var budocsList in Docs)
                    {
                        DocumentsList BuDocs = new DocumentsList();
                        BuDocs.ClientId = Client.OrganizationID;
                        BuDocs.DocumentName = budocsList.DocumentName;
                        BuDocs.DocumentId = budocsList.DocumentId;
                        docsList.Add(BuDocs);
                    }
                    Bu.documents = docsList;
                    pqdocs.Bus.Add(Bu);
                }
                return pqdocs;
            }
        }

        public List<LocalinviteVendor> GetInvitedVendors(long clientId, string SearchEmail, int? InvitationType, bool HasInvitation = false)
        {
            using (EFDbContext context = new EFDbContext())
            {
                var Subclients = GetSubClients(clientId).Select(r => long.Parse(r.Key)).ToList();
                var vendors_list = context.VendorInviteDetails.Where(rec => rec.ClientId == clientId || Subclients.Contains(rec.ClientId));
                if (!string.IsNullOrEmpty(SearchEmail))
                    vendors_list = vendors_list.Where(rec => (rec.VendorEmail.Contains(SearchEmail) || rec.VendorCompanyName.Contains(SearchEmail)));
                if (HasInvitation)
                {
                    switch (InvitationType)
                    {
                        case 0:
                            vendors_list = vendors_list.Where(r => r.InvitationType == null || r.InvitationType == 0); break;
                        case 1:
                            vendors_list = vendors_list.Where(r => r.InvitationType == 1);
                            break;
                        case 2:
                            vendors_list = vendors_list.Where(r => r.InvitationType == 2); break;
                    }
                }
                var vendorsCount = context.VendorInviteDetails.Where(rec => rec.ClientId == clientId).ToList().Count();
                // Subclients = GetSubClients(clientId).Select(r => long.Parse(r.Key)).ToList();
                var invitedVendors = (from vendors in vendors_list.ToList().OrderByDescending(rec => rec.InvitationDate).Take(30) // Kiran on 9/29/2014
                                      select new LocalinviteVendor
                                      {
                                          HasNext = vendorsCount > 30,
                                          HasPrevious = false,
                                          PageNumber = 0,
                                          VendorCompanyName = vendors.VendorCompanyName,
                                          FullName = GetPQInvitedByForDashboard(vendors.VendorEmail, vendors.ClientId, vendors.InvitationId),
                                          //LastName = vendors.VendorLastName,
                                          EmailAddress = vendors.VendorEmail,
                                          InvitationDate = vendors.InvitationDate
                                      }).ToList();

                return invitedVendors;
            }
        }
        public LocalVendorInviteDetails GetPQInviteDetails(long pqId)
        {
            using (EFDbContext entity = new EFDbContext())
            {
                var prequalification = entity.Prequalification.Find(pqId);
                LocalVendorInviteDetails inviteDetails = new LocalVendorInviteDetails();
                if (prequalification.InvitationFrom == null)
                {

                    var VendorEmails = prequalification.Vendor.SystemUsersOrganizations.Select(rec => rec.SystemUsers.UserName).ToList();
                    var InvitationDetails = entity.VendorInviteDetails.FirstOrDefault(rec => VendorEmails.Contains(rec.VendorEmail) && rec.ClientId == prequalification.ClientId);
                    if (InvitationDetails == null)
                        inviteDetails.InvitationSentBy = Resources.Resources.PQBanner_Label_WithoutInvitation;
                    else if (InvitationDetails.InvitationSentFrom == 0)
                        try
                        {
                            inviteDetails.InvitationSentBy = Resources.Resources.PQBanner_Label_InvitationFrom;
                            inviteDetails.InvitationSentByUser = InvitationDetails.SystemUsers.Contacts.FirstOrDefault().LastName + ", " + InvitationDetails.SystemUsers.Contacts.FirstOrDefault().FirstName;
                        }
                        catch { }
                    else
                        inviteDetails.InvitationSentBy = Resources.Resources.PQBanner_Label_InvitationFromFirstVerify;
                }
                else
                {

                    if (prequalification.InvitationFrom == 0)
                    {
                        inviteDetails.InvitationSentBy = Resources.Resources.Common_label_None;
                    }
                    else if (prequalification.InvitationFrom == 1)
                    {
                        inviteDetails.InvitationSentBy = Resources.Resources.Common_label_FirstVerify;
                    }
                    else if (prequalification.InvitationFrom == 2)
                    {
                        inviteDetails.InvitationSentBy = Resources.Resources.Common_label_ClientUser;
                        inviteDetails.InvitationSentByUser = prequalification.InvitedByUser.Contacts.FirstOrDefault().LastName + ", " + prequalification.InvitedByUser.Contacts.FirstOrDefault().FirstName;
                    }
                }
                return inviteDetails;
            }
        }
        public List<KeyValuePair<string, string>> GetClientsExcludeSubClients()
        {
            using (EFDbContext entity = new EFDbContext())
            {
                var SubClients = entity.SubClients.Select(r => r.ClientId);
                return entity.Organizations.Where(rec => (rec.OrganizationType == OrganizationType.Client || rec.OrganizationType == OrganizationType.SuperClient) && !SubClients.Contains(rec.OrganizationID)).ToList().OrderBy(rec => rec.Name).Select(r => new KeyValuePair<string, string>(r.OrganizationID.ToString(), r.Name)).ToList();
            }
        }
        public async Task<List<KeyValuePair<string, string>>> GetLanguagesAsync()
        {
            using (EFDbContext entity = new EFDbContext())
            {
                var res = await entity.Language.ToListAsync();
                return res.Select(r => new KeyValuePair<string, string>(r.LanguageId.ToString(), r.LanguageName)).ToList();
            }
        }
        public List<KeyValuePair<string, string>> GetLanguages()
        {
            using (EFDbContext entity = new EFDbContext())
            {
                return entity.Language.ToList().Select(r => new KeyValuePair<string, string>(r.LanguageId.ToString(), r.LanguageName)).ToList();
            }
        }
        public string GetPQInvitedByForDashboard(string vendorEmail, long clientId, Guid invitationId)
        {
            using (EFDbContext entity = new EFDbContext())
            {
                VendorInviteDetails inviteDetails = entity.VendorInviteDetails.FirstOrDefault(rec => rec.InvitationId == invitationId);

                try
                {
                    long? vendorId = entity.SystemUsers.FirstOrDefault(rec => rec.Email.Trim().Equals(vendorEmail)).SystemUsersOrganizations.FirstOrDefault().OrganizationId;
                    if (vendorId != null)
                    {
                        var prequalification = entity.LatestPrequalification.FirstOrDefault(rec => rec.VendorId == vendorId && rec.ClientId == clientId);
                        if (prequalification != null)
                        {
                            var invitedByDetails = GetPQInviteDetails(prequalification.PrequalificationId);
                            if (invitedByDetails != null && invitedByDetails.InvitationSentByUser != null)
                                return invitedByDetails.InvitationSentByUser;
                            else
                                return Resources.Resources.Common_label_FirstVerify;
                        }
                    }
                }
                catch { }

                return inviteDetails.InvitationSentByUser == null ? Resources.Resources.Common_label_FirstVerify : (inviteDetails.SystemUsers.Contacts.FirstOrDefault() != null ? inviteDetails.SystemUsers.Contacts[0].LastName + ", " + inviteDetails.SystemUsers.Contacts[0].FirstName : inviteDetails.SystemUsers.Email);
            }
        }
        public string DeleteTemplateKey(long organizationid, Guid TemplateId)
        {
            using (EFDbContext context = new EFDbContext())
            {
                var HasKey = context.MultipleInvitationCode.FirstOrDefault(r => r.ClientId == organizationid && r.TemplateId == TemplateId);
                if (HasKey != null)
                {
                    context.MultipleInvitationCode.Remove(HasKey);
                    context.SaveChanges();
                }

                return "Success";
            }

        }
        public string GetClientTemplateKey(long ClientId, Guid TemplateId)
        {
            using (EFDbContext context = new EFDbContext())
            {

                return context.MultipleInvitationCode.FirstOrDefault(r => r.ClientId == ClientId && r.TemplateId == TemplateId).ClientCode;
            }
        }
        public List<VendorPQsSpecificToClient> GetClientUserAccessiblePqs(long clientId, long vendorId, Guid userId)
        {
            using (EFDbContext entity = new EFDbContext())
            {
                List<VendorPQsSpecificToClient> clientAccessiblePqs = new List<VendorPQsSpecificToClient>();
                SystemUsersBUSites userBUSites = entity.SystemUsersBUSites.FirstOrDefault(rec => rec.SystemUserId == userId);

                List<VendorPQsSpecificToClient> vendorPqsSpecificToClient = entity.Prequalification.Where(rec => rec.ClientId == clientId && rec.VendorId == vendorId).OrderByDescending(rec => rec.PrequalificationStart).Select(r => new VendorPQsSpecificToClient { PrequalificationId = r.PrequalificationId, PrequalificationStart = r.PrequalificationStart, ClientName = r.Client.Name }).ToList();
                if (userBUSites != null && !string.IsNullOrEmpty(userBUSites.BUSiteId))
                {
                    var busites = userBUSites.BUSiteId;

                    var userSites = busites.Split(',').Select(long.Parse).ToList();
                    clientAccessiblePqs = (from preQualifications in vendorPqsSpecificToClient
                                           join ps in entity.PrequalificationSites on preQualifications.PrequalificationId equals ps.PrequalificationId
                                           where userSites.Contains(ps.ClientBusinessUnitSiteId.Value)
                                           select new VendorPQsSpecificToClient
                                           {
                                               PrequalificationId = preQualifications.PrequalificationId,
                                               PrequalificationStart = preQualifications.PrequalificationStart,
                                               ClientName = preQualifications.ClientName,
                                               PrequalificationFinish = preQualifications.PrequalificationFinish,
                                               PrequalificationStatusId = preQualifications.PrequalificationStatusId
                                           }).ToList();

                }
                return clientAccessiblePqs;
            }
        }

        public bool IsClientPartOfEri(long clientId)
        {
            using (EFDbContext entity = new EFDbContext())
            {
                return entity.SubClients.Any(r => r.ClientId == clientId);
            }
        }

        public InviteTemplateConfiguration GetClientConfiguredInvitationTemplate(long clientId, int? invitationType)
        {
            using (EFDbContext entity = new EFDbContext())
            {
                if (invitationType != null)
                {
                    return entity.InviteTemplateConfiguration.FirstOrDefault(r => r.ClientId == clientId
                                                                                  && r.InvitationType ==
                                                                                  invitationType);
                }
                else
                {
                    return entity.InviteTemplateConfiguration.FirstOrDefault(r => r.ClientId == clientId && r.InvitationType == LocalConstants.All);
                }
            }
        }

        public List<VendorReviewInputViewModel> GetContractorEvaluations(long pqId, long orgId, string role)
        {
            using (EFDbContext entity = new EFDbContext())
            {
                var pqDetails = entity.Prequalification.FirstOrDefault(rec => rec.PrequalificationId == pqId);

                var totalUsers = new List<Guid>();
                List<VendorReviewInputViewModel> contractorEvaluations = new List<VendorReviewInputViewModel>();
                if (pqDetails != null)
                {
                    var clientUserIds = pqDetails.Client.SystemUsersOrganizations.Select(record => record.UserId)
                        .ToList();

                    totalUsers.AddRange(clientUserIds);
                    if (pqDetails.Client.OrganizationType == OrganizationType.SuperClient)
                    {

                        var clients = pqDetails.PrequalificationClient.ClientIdsList.ToList();
                        if (role == OrganizationType.Client)
                        {
                            clients.Clear();
                            clients.Add(orgId);
                        }

                        if (clients.Any())
                        {
                            var users = entity.Organizations.Where(r => clients.Contains(r.OrganizationID))
                                .SelectMany(r => r.SystemUsersOrganizations).Select(r => r.UserId);
                            totalUsers.AddRange(users);
                        }
                    }

                    var vendorReviews = entity.VendorReviewInput
                        .Where(rec => rec.VendorId == pqDetails.VendorId && totalUsers.Contains(rec.InputUserId))
                        .OrderByDescending(rec => rec.VendorReviewInputId).ToList();

                    foreach (VendorReviewInput review in vendorReviews)
                    {
                        VendorReviewInputViewModel contractorEvaluation = new VendorReviewInputViewModel();

                        contractorEvaluation.InputUserId = review.InputUserId;
                        contractorEvaluation.Comments = review.Comments;
                        contractorEvaluation.InputUserEmail = review.SystemUsers.Email;
                        Contact inputUserContact = review.SystemUsers.Contacts.Any() ? review.SystemUsers.Contacts[0] : null;
                        if (inputUserContact != null)
                            contractorEvaluation.InputUserName = inputUserContact.FullName;
                        contractorEvaluation.ReviewDate = review.ReviewDate;
                        contractorEvaluation.UserRating = review.UserRating;
                        contractorEvaluation.VendorReviewInputId = review.VendorReviewInputId;

                        contractorEvaluations.Add(contractorEvaluation);
                    }
                }
                return contractorEvaluations;
            }
        }

        public LocalScoreCard GetVendorReviewInput(long pqId, long vendorId, long? buId, long? vendorReviewInputId, long orgId, Guid userId)
        {
            using (EFDbContext entity = new EFDbContext())
            {
                if (vendorReviewInputId != null)
                {
                    VendorReviewInput vendorReviewInput =
                        entity.VendorReviewInput.FirstOrDefault(rec => rec.VendorReviewInputId == vendorReviewInputId);
                    if (vendorReviewInput != null)
                    {
                        SystemUsersOrganizations clientSystemUsersOrganization =
                            vendorReviewInput.SystemUsers.SystemUsersOrganizations.FirstOrDefault();

                        if (clientSystemUsersOrganization != null)
                        {
                            orgId = clientSystemUsersOrganization.OrganizationId;
                        }
                    }
                }
                Guid loggedInUserGuid = userId;
                long clientId = 0;
                Prequalification pq = entity.Prequalification.Find(pqId);

                if (pq != null)
                    clientId = pq.ClientId;

                var clientUserIds = entity.SystemUsersOrganizations.Where(rec => rec.OrganizationId == orgId).Select(rec => rec.UserId);

                LocalScoreCard scorecard = new LocalScoreCard();
                var vendorReviews = entity.VendorReviewQuestions.Where(rec => rec.ClientID == clientId).Select(rec => new LocalVendorReview() { QuestionString = rec.QuestionString, VendorReviewQuestionId = rec.VendorReviewQuestionId, QisMandatory = rec.QisMandatory }).ToList();
                scorecard.VendorReviews = vendorReviews;
                scorecard.EvaluatedBy = loggedInUserGuid;
                List<LocalVendorReviewSection> vendorReviewSection =
                    entity.VendorReviewQuestionsForSection3.Where(rec => rec.ClientId == clientId)
                        .OrderBy(rec => rec.Order)
                        .Select(rec => new LocalVendorReviewSection()
                        {
                            SectionHeader = rec.SectionHeader,
                            HasQuestion = rec.HasQuestion,
                            QuestionText = rec.QuestionText,
                            QuestionTypeID = rec.QuestionTypeID,
                            Options = rec.Options,
                            Order = rec.Order,
                            Id = rec.Id,
                            Group = rec.Group
                        }).ToList();
                if (vendorReviewInputId != null)
                {
                    vendorReviewSection.ForEach(rec =>
                    {
                        VendorReviewInputSection3Ans inputSectionAnswer = entity.VendorReviewInputSection3Ans.FirstOrDefault(rec1 => rec1.Section3QuestionId == rec.Id && rec1.VendorReviewInputId == vendorReviewInputId);
                        if (inputSectionAnswer != null)
                            rec.Answers = inputSectionAnswer.Answer;
                    });
                    vendorReviewSection.ForEach(rec =>
                    {
                        rec.IsEditable = !vendorReviewSection.Any(rec1 => rec1.Group == rec.Group && !string.IsNullOrEmpty(rec1.Answers));
                    });
                }

                scorecard.LocalVendorReviewSection = vendorReviewSection;

                var contactUsers = (from users in entity.Contact.Where(rec => clientUserIds.Contains(rec.UserId))
                    .ToList()
                                    select new KeyValuePair<string, string>(users.FullName, users.UserId.ToString())).ToList();


                var contactUserIds = contactUsers.Where(rec => clientUserIds.Contains(Guid.Parse(rec.Value))).Select(rec => Guid.Parse(rec.Value)).ToList();
                var userEmails = (from emails in entity.SystemUsers.Where(rec =>
                                                    clientUserIds.Contains(rec.UserId) && !contactUserIds.Contains(rec.UserId)).ToList()
                                  select new KeyValuePair<string, string>(emails.Email, emails.UserId.ToString())).ToList();


                List<KeyValuePair<string, string>> totalUsers = new List<KeyValuePair<string, string>>();
                totalUsers.AddRange(contactUsers);
                totalUsers.AddRange(userEmails);

                scorecard.ClientUsers = totalUsers;
                scorecard.PreQualificationId = pqId;
                scorecard.SelectedVendorId = vendorId;
                scorecard.VendorName = entity.Organizations.FirstOrDefault(rec => rec.OrganizationID == vendorId)?.Name;

                scorecard.buid = buId;

                if (vendorReviewInputId != null)
                {
                    var vendorReviewInputrec = entity.VendorReviewInput.Find(vendorReviewInputId);
                    if (vendorReviewInputrec != null)
                    {
                        scorecard.ReviewType = vendorReviewInputrec.ReviewType;
                        scorecard.ReviewDate = vendorReviewInputrec.ReviewDate;
                        scorecard.ProjectLocation = vendorReviewInputrec.ProjectLocation;
                        scorecard.ContractWONumber = vendorReviewInputrec.ContractWONumber;

                        scorecard.VendorReviewInputId = vendorReviewInputrec.VendorReviewInputId;
                        scorecard.EvaluatedBy = vendorReviewInputrec.InputUserId;

                        foreach (var vendorReview in scorecard.VendorReviews)
                        {
                            var dbReviewAnswer = entity.VendorReviewInputAnswers.FirstOrDefault(rec =>
                                rec.VendorReviewInputId == vendorReviewInputId && rec.VendorReviewQuestionId ==
                                vendorReview.VendorReviewQuestionId);
                            if (dbReviewAnswer != null)
                            {
                                vendorReview.VendorReviewQuestionId = dbReviewAnswer.VendorReviewQuestionId;
                                if (dbReviewAnswer.InputValue != "")
                                    vendorReview.Rating = Convert.ToInt32(dbReviewAnswer.InputValue);
                            }
                        }
                    }

                }
                else
                    scorecard.VendorReviewInputId = null;
                scorecard.CanUserEdit = (vendorReviewInputId == null ||
                                            loggedInUserGuid == scorecard.EvaluatedBy);

                return scorecard;
            }
        }

        public void PostVendorReviewInput(LocalScoreCard scoreCard, long orgId, Guid userId)
        {
            using (EFDbContext entity = new EFDbContext())
            {
                int reviewNo;
                var recentReviews = entity.VendorReviewInput.Where(rec => rec.InputUserId == userId).OrderByDescending(rec => rec.UserReviewNumber).Select(rec => rec.UserReviewNumber).ToList();
                if (recentReviews.Count == 0)
                    reviewNo = 1;
                else
                    reviewNo = recentReviews[0] + 1;
                VendorReviewInput review = new VendorReviewInput();
                if (scoreCard.VendorReviewInputId != null)
                    review = entity.VendorReviewInput.Find(scoreCard.VendorReviewInputId);
                if (scoreCard.CanUserEdit && review != null)
                {
                    var sum = scoreCard.VendorReviews.Sum(rec => rec.Rating);
                    long vendorId = 0;
                    Prequalification pq =
                        entity.Prequalification.FirstOrDefault(rec => rec.PrequalificationId == scoreCard.PreQualificationId);
                    if (pq != null)
                        vendorId = pq.VendorId;
                    int ratedQuestions = scoreCard.VendorReviews.Where(rec => rec.Rating != 0 && rec.Rating != null).Count();
                    review.ReviewType = scoreCard.ReviewType;
                    review.ProjectLocation = scoreCard.ProjectLocation;
                    review.ContractWONumber = scoreCard.ContractWONumber;
                    review.PrequalificationID = scoreCard.PreQualificationId;
                    review.BuId = scoreCard.buid;

                    review.ReviewDate = scoreCard.ReviewDate;

                    review.InputUserId = scoreCard.EvaluatedBy;
                    review.VendorId = vendorId;
                    review.UserReviewNumber = reviewNo;
                    review.ClientId = orgId;
                    if (ratedQuestions != 0)
                        review.UserRating = (decimal)sum / ratedQuestions;
                    else
                        review.UserRating = 0;
                    if (scoreCard.VendorReviewInputId != null)
                        entity.Entry(review).State = EntityState.Modified;
                    else
                        entity.VendorReviewInput.Add(review);
                    foreach (var reviewRating in scoreCard.VendorReviews)
                    {
                        VendorReviewInputAnswers inputAns = new VendorReviewInputAnswers();
                        var vendorAnsRec =
                            entity.VendorReviewInputAnswers.FirstOrDefault(
                                rec =>
                                    rec.VendorReviewInputId == review.VendorReviewInputId &&
                                    rec.VendorReviewQuestionId == reviewRating.VendorReviewQuestionId);
                        if (vendorAnsRec != null)
                        {
                            vendorAnsRec.InputValue = reviewRating.Rating.ToString();
                            entity.Entry(vendorAnsRec).State = EntityState.Modified;
                        }
                        else
                        {
                            inputAns.VendorReviewQuestionId = reviewRating.VendorReviewQuestionId;
                            inputAns.InputValue = reviewRating.Rating.ToString();
                            inputAns.VendorReviewInputId = review.VendorReviewInputId;
                            entity.VendorReviewInputAnswers.Add(inputAns);
                        }
                    }
                }
                foreach (var sectionans in scoreCard.LocalVendorReviewSection)
                {
                    if (!sectionans.IsEditable) continue;
                    var ans =
                        entity.VendorReviewInputSection3Ans.FirstOrDefault(
                            rec =>
                                rec.VendorReviewInputId == review.VendorReviewInputId &&
                                rec.Section3QuestionId == (long)sectionans.Id);
                    if (ans == null)
                        ans = new VendorReviewInputSection3Ans();
                    ans.FilledBy = userId;
                    ans.Answer = sectionans.Answers;
                    ans.Section3QuestionId = (long)sectionans.Id;
                    ans.VendorReviewInputId = review.VendorReviewInputId;
                    if (ans.Id != 0)
                        entity.Entry(ans).State = EntityState.Modified;
                    else
                        entity.VendorReviewInputSection3Ans.Add(ans);

                }

                entity.SaveChanges();
            }
        }
        public void PostClientBusAndSites(long clientId, string clientName)
        {
            using (EFDbContext entity = new EFDbContext())
            {
                ClientBusinessUnits defaultSuperClientBu = new ClientBusinessUnits();
                defaultSuperClientBu.BusinessUnitName = clientName + " BU";
                defaultSuperClientBu.ClientId = clientId;
                defaultSuperClientBu.InsertDate = DateTime.Now;

                entity.Entry(defaultSuperClientBu).State = EntityState.Added;

                ClientBusinessUnitSites defaultSuperClientBuSite = new ClientBusinessUnitSites();
                defaultSuperClientBuSite.BusinessUnitLocation = "N/A";
                defaultSuperClientBuSite.ClientBusinessUnitId = defaultSuperClientBu.ClientBusinessUnitId;
                defaultSuperClientBuSite.SiteName = clientName + " BU Site";
                defaultSuperClientBuSite.InsertDate = DateTime.Now;
                defaultSuperClientBuSite.BusinessUnitType = 0;

                entity.Entry(defaultSuperClientBuSite).State = EntityState.Added;

            }
        }

        public List<string> GetClientsFromClientTemplates(List<long> clientIds)
        {
            using (EFDbContext entity = new EFDbContext())
            {
                return entity.ClientTemplates.Where(r => clientIds.Contains(r.ClientID)).ToList()
                    .Select(r => r.ClientID.ToString()).ToList();
            }
        }
        public string GetPowerBiReportByName(long ClientId, Guid UserId, string Role, string ReportName)
        {
            using (var context = new EFDbContext())
            {
                var reports = context.PowerBiReports.FirstOrDefault(r => r.DisplayName.ToLower() == ReportName.ToLower() && r.IsActive != false);
                if (reports != null)
                {
                    var reportId = reports.Id;

                    var hasReportUrl = context.PowerBiReportUrls.FirstOrDefault(r => r.OrgId == ClientId && r.PowerBiReportId == reportId);
                    if (hasReportUrl != null)
                    {
                        var permissionType = (int)Enum.Parse(typeof(PermissionType), Role);
                        var permisionTypes = new List<int>();
                        permisionTypes.Add(0);
                        permisionTypes.Add(permissionType);
                        var hasUserPermission = context.PowerBiUserPermissions
                            .FirstOrDefault(r => (r.OrganizationId == ClientId 
                            && r.PowerBiPermissions.PermissionType == permissionType && r.PowerBiPermissions.PowerBiId == reportId 
                            && (r.UserId.Contains(UserId.ToString()) || string.IsNullOrEmpty(r.UserId))) 
                            || (r.OrganizationId == null && r.PowerBiPermissions.PowerBiId == reportId 
                                && permisionTypes.Contains(r.PowerBiPermissions.PermissionType))
                            );
                       if(hasUserPermission!=null)
                             return hasReportUrl.Url;
                        }
                        else return "";
                    }
                
                return "";
            }
        }
       
        public string GetReportFilter(string ReportName)
        {
            using (var context = new EFDbContext())
            {
                string filter = "";
                var report = context.PowerBiReports.FirstOrDefault(r => r.ReportName == ReportName && r.IsActive != false);
                if (report != null && report.Hasfilter!=false)
                    {
                    var hasFilter = context.PowerBiReportFilters.FirstOrDefault(r => r.ReportId == report.Id);
                    if (hasFilter != null)
                        filter= "&filter=" + hasFilter.DatasetName + "/" + hasFilter.FilterName + " " + hasFilter.Condition + " " + hasFilter.FilterTo;
                }
                return filter;
            }
        }
    }
}