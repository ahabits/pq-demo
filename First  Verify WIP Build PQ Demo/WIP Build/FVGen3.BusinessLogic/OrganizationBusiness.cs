﻿/*
Page Created Date:  04/04/2017
Created By: Rajesh Pendela
Purpose: All Business operations related to an organization will be performed here
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using AutoMapper;
using FVGen3.BusinessLogic.Interfaces;
using FVGen3.DataLayer.DTO;
using FVGen3.Domain.Concrete;
using FVGen3.Domain.Entities;
using System.Web;
using FVGen3.BusinessLogic.Extentions;
using System.Data.SqlClient;
using System.IO;
using Resources;
using System.Text.RegularExpressions;
using FVGen3.Domain.ViewModels;
using FVGen3.WebUI.Constants;

namespace FVGen3.BusinessLogic
{
    public class OrganizationBusiness : BaseBusinessClass, IOrganizationBusiness
    {
        IMapper mapper;
        IMapper mapper1;
        //public OrganizationBusiness(HttpContext httpContext):base(httpContext)
        //{
        //    var config = new MapperConfiguration(cfg => cfg.CreateMap<Organizations, OrganizationData>());
        //    mapper = config.CreateMapper();
        //}
        public OrganizationBusiness()
        {
            var config = new MapperConfiguration(cfg => cfg.CreateMap<Organizations, OrganizationData>());
            mapper = config.CreateMapper();

            var config1 = new MapperConfiguration(cfg => cfg.CreateMap<OrganizationData, Organizations>());
            mapper1 = config1.CreateMapper();
        }
        /// <summary>
        /// To get the specific organization information
        /// </summary>
        /// <param name="orgId"></param>
        /// <returns></returns>
        public OrganizationData GetOrganization(long orgId)
        {
            using (var context = new EFDbContext())
            {
                return mapper.Map<OrganizationData>(context.Organizations.Find(orgId));
            }
        }
        /// <summary>
        /// To get the super client organization information of the specifc client
        /// </summary>
        /// <param name="orgId"></param>
        /// <returns></returns>
        public OrganizationData GetSuperClient(long orgId)
        {
            using (var context = new EFDbContext())
            {
                var org = context.SubClients.FirstOrDefault(r => r.ClientId == orgId).SuperClientOrganization;
                if (org == null) org = context.Organizations.Find(orgId);
                return mapper.Map<OrganizationData>(org);
            }
        }
        /// <summary>
        /// To get the organization information based on client secret key
        /// </summary>
        /// <param name="ClientSecret"></param>
        /// <returns></returns>
        public OrganizationData GetOrganization(string ClientSecret)
        {
            using (var context = new EFDbContext())
            {
                var a = context.Organizations.FirstOrDefault(r => r.ClientSecret == ClientSecret);
                return mapper.Map<OrganizationData>(a);
            }
        }
        /// <summary>
        /// To get the user organizations with permissions of specifc role
        /// </summary>
        /// <param name="RoleId"></param>
        /// <returns></returns>
        public List<LocOrganizationRoles> GetOrganizationsWithPermission(string RoleId)
        {
            using (var context = new EFDbContext())
            {
                var sysRole = context.SystemRoles.Find(RoleId);
                var sysOrg = context.Organizations.Where(r => r.OrganizationType == sysRole.OrganizationType).OrderBy(r => r.Name).Select(r => new LocOrganizationRoles() { OrgId = r.OrganizationID, OrgName = r.Name }).ToList();
                var orgIds = context.OrganizationRoles.Where(r => r.RoleId == RoleId).Select(r => r.OrganizationId).ToList();
                var sysorgids = context.SystemUsersOrganizationsRoles.Where(r => r.SysRoleId == RoleId).Select(r => r.SysUserOrgId).ToList();
                //var existingOrgs = context.SystemUsersOrganizations.Where(r => sysorgids.Contains(r.SysUserOrganizationId)).Select(r => r.OrganizationId).ToList();
                //var orgid = context.Organizations.Where(r => orgIds.Contains(r.OrganizationID)).Select(r =>r.OrganizationID).ToList();
                sysOrg.Where(r => orgIds.Contains(r.OrgId)).ForEach(r => r.Status = true);
                //sysOrg.Where(r => existingOrgs.Contains(r.OrgId)).ForEach(r => { r.Status = true; r.IsDisable = true; });
                return sysOrg;
            }
            return null;
        }

        /// <summary>
        /// To get the user organizations based on role
        /// </summary>
        /// <param name="RoleId"></param>
        /// <returns></returns>
        public List<LocOrganizationRoles> GetOrganizationsWithRole(string RoleId)
        {
            using (var context = new EFDbContext())
            {
                var sysRole = context.SystemRoles.Find(RoleId);
                var sysOrg = context.Organizations.Where(r => r.OrganizationType == sysRole.OrganizationType).OrderBy(r => r.Name).Select(r => new LocOrganizationRoles() { OrgId = r.OrganizationID, OrgName = r.Name }).Distinct().ToList();
                var orgIds = context.OrganizationRoles.Where(r => r.RoleId == RoleId).Select(r => r.OrganizationId).ToList();
                var sysorgids = context.SystemUsersOrganizationsRoles.Where(r => r.SysRoleId == RoleId).Select(r => r.SysUserOrgId).ToList();
                var existingOrgs = context.SystemUsersOrganizations.Where(r => sysorgids.Contains(r.SysUserOrganizationId)).Select(r => r.OrganizationId).ToList();
                //var orgid = context.Organizations.Where(r => orgIds.Contains(r.OrganizationID)).Select(r =>r.OrganizationID).ToList();
                sysOrg.Where(r => orgIds.Contains(r.OrgId)).ForEach(r => r.Status = true);
                sysOrg.Where(r => existingOrgs.Contains(r.OrgId)).ForEach(r => { r.Status = true; r.IsDisable = true; });
                return sysOrg;
            }
            return null;
        }

        /// <summary>
        /// To get the organization roles of specific role
        /// </summary>
        /// <param name="RoleId"></param>
        /// <returns></returns>
        public bool HasOrgRoles(string RoleId)
        {
            using (var context = new EFDbContext())
            {
                return context.OrganizationRoles.Any(r => r.RoleId == RoleId);
            }
        }
        /// <summary>
        /// To create roles for the specifc organizations
        /// </summary>
        /// <param name="roleId"></param>
        /// <param name="orgIds"></param>
        /// <returns></returns>
        public string AssignOrgRoles(string roleId, List<long> orgIds)
        {
            using (var context = new EFDbContext())
            {
                var removedData = context.OrganizationRoles.Where(r => r.RoleId == roleId).ToList();
                foreach (var data in removedData)
                {
                    context.OrganizationRoles.Remove(data);
                }
                if (!orgIds.Any(r => r == -1))
                    foreach (var orgId in orgIds)
                    {
                        //List<SqlParameter> param = new List<SqlParameter>();
                        //param.Add(new SqlParameter("@Role", roleId));
                        //if (!context.OrganizationRoles.Any(r => r.RoleId == roleId && r.OrganizationId == orgId))
                        {
                            var orgRole = new OrganizationRoles() { RoleId = roleId, OrganizationId = orgId };
                            context.OrganizationRoles.Add(orgRole);
                        }
                    }
                context.SaveChanges();
            }
            return "Success";
        }
        /// <summary>
        /// To get the users of specific organization
        /// </summary>
        /// <param name="orgId"></param>
        /// <returns></returns>
        public List<UserInfo> GetOrganizationUsers(long orgId)
        {
            //  var EmpRole = "A4D58875-8EFC-4300-8B54-D0A9E732B891";
            var EmpRole = EmployeeRole.EmployeeRoleId;
            using (var context = new EFDbContext())
            {
                return
                    context.SystemUsersOrganizations.Where(
                            r =>
                                r.OrganizationId == orgId &&
                                r.SystemUsersOrganizationsRoles.Any(ro => !EmpRole.Contains(ro.SysRoleId)))
                        .Select(
                            r =>
                                new UserInfo()
                                {
                                    Email = r.SystemUsers.Email,
                                    FirstName =
                                        r.SystemUsers.Contacts.Any()
                                            ? r.SystemUsers.Contacts.FirstOrDefault().FirstName
                                            : "",
                                    LastName =
                                        r.SystemUsers.Contacts.Any()
                                            ? r.SystemUsers.Contacts.FirstOrDefault().LastName
                                            : "",
                                    RoleName =
                                        r.SystemUsersOrganizationsRoles.Any()
                                            ? r.SystemUsersOrganizationsRoles.FirstOrDefault().SystemRoles.RoleName
                                            : ""
                                }).ToList();
            }
        }
        /// <summary>
        /// To get the super user of specifc organization
        /// </summary>
        /// <param name="orgId"></param>
        /// <returns></returns>
        public UserInfo GetSuperuser(long orgId)
        {
            using (var context = new EFDbContext())
            {
                SystemUsers user = null;
                var orgSuperuser =
                    context.SystemUsersOrganizations.FirstOrDefault(
                        r => r.OrganizationId == orgId && r.PrimaryUser == true);
                    if(orgSuperuser != null)
                       user = orgSuperuser.SystemUsers;
                var result = new UserInfo();
                if (user != null)
                {
                    result.Email = user.Email;
                    result.UserId = user.UserId;
                    if (user.Contacts.Any())
                    {
                        result.FirstName = user.Contacts[0].FirstName;
                        result.LastName = user.Contacts[0].LastName;
                        result.MobileNumber = user.Contacts[0].MobileNumber;
                        result.PhoneNumber = user.Contacts[0].PhoneNumber;
                        result.ContactTitle = user.Contacts[0].ContactTitle;

                        result.ContactId = user.Contacts[0].ContactId;
                    }
                }
                //else
                //{
                //    SystemUsersOrganizations vendorSuperUserOrg =
                //        context.SystemUsersOrganizations.FirstOrDefault(rec =>  rec.OrganizationId == orgId && rec.PrimaryUser == true);
                //    if (vendorSuperUserOrg != null)
                //    {
                //        Contact superUserContact = vendorSuperUserOrg.SystemUsers.Contacts.FirstOrDefault();
                //        if(superUserContact != null)
                //            result.ContactId = superUserContact.ContactId;
                //    }
                        
                //}
                return result;
            }
        }
        /// <summary>
        /// Returns the template is locked or not based on invitation code
        /// </summary>
        /// <param name="invitationCode"></param>
        /// <returns></returns>
        public bool IsTemplateLocked(string invitationCode)
        {
            using (var context = new EFDbContext())
            {
                var invitation = context.VendorInviteDetails.FirstOrDefault(r => r.InvitationCode == invitationCode);
                if (invitation != null)
                {
                    var clienttemplate = context.ClientTemplates.FirstOrDefault(r => r.ClientID == invitation.ClientId && r.Templates.RiskLevel == invitation.RiskLevel);
                    if (invitationCode.Length == 12)
                    {
                        var HasInvitation = context.MultipleInvitationCode.FirstOrDefault(r => r.ClientCode == invitationCode);
                        if (HasInvitation != null)
                        {
                            return context.Templates.FirstOrDefault(r => r.TemplateID == HasInvitation.TemplateId).LockedByDefault.HasValue;
                        }
                    }

                    if (clienttemplate != null)
                    {
                        if (clienttemplate.Templates.LockedByDefault == true)
                            return true;
                    }
                }
            }
            return false;
        }
        /// <summary>
        /// returns the client is subclient or not
        /// </summary>
        /// <param name="orgId"></param>
        /// <returns></returns>
        public bool IsSubClient(long orgId)
        {
            using (var context = new EFDbContext())
            {
                return context.SubClients.Any(r => r.ClientId == orgId);
            }
        }
        /// <summary>
        /// To get the countries
        /// </summary>
        /// <returns></returns>
        public List<KeyValuePair<string, string>> GetCountries()
        {
            using (var context = new EFDbContext())
            {
                return context.Countries.ToList().Select(rec =>
                        new KeyValuePair<string, string>(rec.CountryId.ToString(), rec.CountryName.ToString())).ToList();
            }
        }

        public List<KeyValuePair<string, string>> GetRegions()
        {
            using (var context = new EFDbContext())
            {
                return context.Regions.ToList().Select(rec =>
                    new KeyValuePair<string, string>(rec.RegionId.ToString(), rec.RegionName.ToString())).ToList();
            }
        }
        /// <summary>
        /// To get the states of specific country
        /// </summary>
        /// <param name="country"></param>
        /// <returns></returns>
        public List<KeyValuePair<string, string>> GetCountryStates(string country)
        {
            using (var context = new EFDbContext())
            {
                return context.States.Where(r => r.CountryName.Equals(country)).ToList().Select(rec =>
                        new KeyValuePair<string, string>(rec.StatesId.ToString(), rec.StateName.ToString())).ToList();
            }
        }
        /// <summary>
        /// To get the country languages
        /// </summary>
        /// <param name="country"></param>
        /// <returns></returns>
        public List<KeyValuePair<string, string>> GetCountryLanguages(string country)
        {
            using (var context = new EFDbContext())
            {
                return (from cl in context.CountryLanguages
                        join c in context.Countries on cl.CountryId equals c.CountryId
                        join l in context.Language on cl.LanguageId equals l.LanguageId
                        where c.CountryName.Equals(country)
                        orderby cl.IsDefault descending
                        select new { l.LanguageName, id = l.LanguageId }).ToList().Select(r => new KeyValuePair<string, string>(r.LanguageName, r.id.ToString())).ToList();
            }
        }
        /// <summary>
        /// To get the language code
        /// </summary>
        /// <param name="languageId"></param>
        /// <returns></returns>
        public string GetLanguageCode(long languageId)
        {
            using (var context = new EFDbContext())
            {
                return context.Language.Find(languageId)?.LanguageCode;
            }
        }
        public string BulkLocationInsertion(string file, Guid templateId, Guid userId)
        {
            using (var entityDb = new EFDbContext())
            {
                try
                {

                    foreach (var row in file.Split('\n').Skip(1))
                    {
                        var columns = row.Replace("\"", "").Split(',');
                        var TaxId = columns[0].Trim();
                        var Hasvendor = entityDb.Organizations.FirstOrDefault(r => r.TaxID == TaxId);
                        if (Hasvendor != null)
                        {
                            var IsClaycoVendor = entityDb.LatestPrequalification.FirstOrDefault(r => r.ClientId == LocalConstants.Clayco_ClientId && r.VendorId == Hasvendor.OrganizationID);
                            if (IsClaycoVendor != null && IsClaycoVendor.PrequalificationSites.Count() == 0)
                            {
                                var HasBusinessUnits = entityDb.ClientBusinessUnits.Where(r => r.ClientId == LocalConstants.ClientID).ToList()
                     .SelectMany(r => r.ClientBusinessUnitSites)
                     .Select(sites => new PrequalificationSites
                     {
                         PrequalificationId = IsClaycoVendor.PrequalificationId,
                         ClientBusinessUnitId = sites.ClientBusinessUnitId,
                         ClientBusinessUnitSiteId = sites.ClientBusinessUnitSiteId,
                         ClientId = IsClaycoVendor.ClientId,
                         VendorId = IsClaycoVendor.VendorId
                     }).ToList();
                                foreach (var bus in HasBusinessUnits)
                                {
                                    entityDb.PrequalificationSites.Add(bus);
                                }
                                entityDb.SaveChanges();

                            }
                            else continue;

                        }
                        else continue;

                    }
                    return Resources.Resources.Bulk_Locs_insertion_Success_msg;
                }
                catch (Exception e)
                {
                    return Resources.Resources.Unable_process_req;
                }
            }
        }
        public string BulkVendorInsertion(string file, Guid templateId, Guid userId)
        {
            const int numberOfCols = 12;
            using (var entityDb = new EFDbContext())
            {
                try
                {
                    var clientTemplateId = entityDb.ClientTemplates
                             .FirstOrDefault(r => r.TemplateID == templateId && r.ClientID == LocalConstants.ClientID)?
                             .ClientTemplateID;
                    foreach (var row in file.Split('\n').Skip(1))
                    {
                       
                        var columns = Regex.Split(row, ",(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))");
                        
                        var columnCount = columns.Length;
                        if (columnCount < numberOfCols)
                            continue;

                        var name = columns[0].Replace("\"", "").Trim();
                        if (string.IsNullOrEmpty(name))
                        {
                            continue;
                        }

                        var taxId = columns[1].Replace("\"", "").Trim();

                        var hasVendor = entityDb.Organizations.FirstOrDefault(r => r.Name == name || r.TaxID == taxId);
                        if (hasVendor == null)
                        {
                            hasVendor = new Organizations();
                            hasVendor.Name = name;
                            hasVendor.TaxID = taxId;
                            hasVendor.PrincipalCompanyOfficerName = columns[2].Replace("\"", "").Trim();
                            hasVendor.Address1 = columns[3].Replace("\"", "").Trim();
                            hasVendor.City = columns[4].Replace("\"", "").Trim();
                            hasVendor.Zip = columns[5].Replace("\"", "").Trim();
                            hasVendor.Country = columns[6].Replace("\"", "").Trim();
                            hasVendor.State = columns[7].Replace("\"", "").Trim();
                            hasVendor.CopyAnswers = 2;
                            hasVendor.Language = 1;
                            hasVendor.OrganizationType = LocalConstants.Vendor;
                            hasVendor.GeographicArea = "All U.S.";
                            entityDb.Organizations.Add(hasVendor);
                            hasVendor.SystemUsersOrganizations = new List<SystemUsersOrganizations>();
                           entityDb.SaveChanges();
                        }
                            
                       else
                        {
                            
                            hasVendor.Name = name;
                            hasVendor.TaxID = taxId;
                            hasVendor.PrincipalCompanyOfficerName = columns[2].Replace("\"", "").Trim();
                            hasVendor.Address1 = columns[3].Replace("\"", "").Trim();
                            hasVendor.City = columns[4].Replace("\"", "").Trim();
                            hasVendor.Zip = columns[5].Replace("\"", "").Trim();
                            hasVendor.Country = columns[6].Replace("\"", "").Trim();
                            hasVendor.State = columns[7].Replace("\"", "").Trim();
                            entityDb.Entry(hasVendor).State = EntityState.Modified;
                           // entityDb.SaveChanges();
                        }

                        hasVendor = entityDb.Organizations.FirstOrDefault(r => r.Name == name || r.TaxID == taxId);
                        #region Add User
                        var UserName = columns[10].Replace("\"", "").Replace("\r", "").Trim();
                        if (!entityDb.SystemUsers.Any(r => r.UserName == UserName))
                        {
                            var contact = new Contact { FirstName = columns[8].Replace("\"", ""), LastName = columns[9].Replace("\"", "") };

                            var sysUserOrg = new SystemUsersOrganizations
                            {
                                SystemUsers = new SystemUsers()
                                {
                                    Email = columns[10].Trim(),
                                    UserName = columns[10].Replace("\"", "").Trim(),
                                    CreateDate = DateTime.Now,
                                    LastLoginDate = DateTime.Now,
                                    Password = UtilityBusiness.EncryptData(columns[11].Replace("\"", "").Replace("\r", "").Trim()),
                                    Contacts = new List<Contact>() { contact },
                                }
                            };
                            hasVendor.SystemUsersOrganizations.Add(sysUserOrg);
                          //  entityDb.SaveChanges();
                            var role = new SystemUsersOrganizationsRoles
                            {
                                SysRoleId = LocalConstants.VendorSuperUser,
                                SysUserOrgId = sysUserOrg.SysUserOrganizationId
                            };
                            entityDb.SystemUsersOrganizationsRoles.Add(role);
                           // entityDb.SaveChanges();
                        }
                       
                        #endregion

                        
                        var IsClaycoVendor = entityDb.LatestPrequalification.FirstOrDefault(r => r.ClientId == LocalConstants.Clayco_ClientId && r.VendorId == hasVendor.OrganizationID);
                        if (IsClaycoVendor == null)
                        {
                            var currentDate = DateTime.Now;
                            var p = new Prequalification
                            {
                                ClientId = LocalConstants.ClientID,
                                VendorId = hasVendor.OrganizationID,
                                PrequalificationStatusId = 26,
                                PrequalificationStart = currentDate,
                                PrequalificationFinish = currentDate.AddYears(50),
                                PrequalificationCreate = currentDate,
                                PrequalificationSubmit = currentDate,
                                ClientTemplateId = clientTemplateId,
                                UserIdAsSigner = userId,
                                PaymentReceived = true,
                                UserIdLoggedIn = userId
                            };


                            entityDb.Prequalification.Add(p);

                            var hasBusinessUnits = entityDb.ClientBusinessUnits.Where(r => r.ClientId == LocalConstants.ClientID).ToList()
                                .SelectMany(r => r.ClientBusinessUnitSites)
                                .Select(sites => new PrequalificationSites
                                {
                                    PrequalificationId = p.PrequalificationId,
                                    ClientBusinessUnitId = sites.ClientBusinessUnitId,
                                    ClientBusinessUnitSiteId = sites.ClientBusinessUnitSiteId,
                                    ClientId = p.ClientId,
                                    VendorId = p.VendorId
                                }).ToList();
                            foreach (var bus in hasBusinessUnits)
                            {
                                entityDb.PrequalificationSites.Add(bus);
                            }
                           
                        }
                        entityDb.SaveChanges();
                       
                    }

                    return Resources.Resources.Bulk_InsertionSucessMsg;
                }
                catch (Exception e)
                {
                    return Resources.Resources.Unable_process_req;
                }
            }
        }

        public List<KeyValuePair<string, string>> GetOrgUsers(long orgId)
        {
            using (var entity = new EFDbContext())
            {
                var empTrainingRoleId = EmployeeRole.EmployeeRoleId;
                return entity.SystemUsersOrganizations
                    .Where(r => r.OrganizationId == orgId && r.SystemUsers.UserStatus == true &&
                                r.SystemUsersOrganizationsRoles
                                    .Where(rec1 => empTrainingRoleId.Contains(rec1.SysRoleId)).Count() == 0).ToList().Select(
                        rec => new KeyValuePair<string, string>(
                            rec.SystemUsers.Contacts.Count() != 0
                                ? rec.SystemUsers.Contacts.FirstOrDefault().FullName
                                : rec.SystemUsers.Email + " - No Contact details available", rec.UserId.ToString()))
                    .ToList();
            }
        }

        public List<KeyValuePair<string, string>> GetClientOrganizations()
        {
            using (var entity = new EFDbContext())
            {
                return entity.Organizations.Where(r => r.OrganizationType == LocalConstants.Client && r.ShowInApplication == true).OrderBy(r => r.Name).ToList()
                    .Select(r => new KeyValuePair<string, string>(r.Name, r.OrganizationID.ToString())).ToList();
            }
        }

        public List<OrganizationData> GetOrganizations(string companyName, string typeClient, string typeVendor, string typeAdmin)
        {
            using (var entity = new EFDbContext())
            {
                List<OrganizationData> organizationsData = new List<OrganizationData>();
                List<Organizations> organizations = new List<Organizations>();
                if (string.IsNullOrEmpty(companyName) && typeClient.Equals("false") && typeVendor.Equals("false") && typeAdmin.Equals("false"))
                {
                    var query = entity.Organizations.OrderBy(rec => rec.Name).ToList();

                    try
                    {
                        organizations = query.ToList();
                    }
                    catch (Exception ee)
                    {
                        Console.Write(ee);
                    }
                }
                else if (string.IsNullOrEmpty(companyName))
                {
                   var query = entity.Organizations.OrderBy(rec => rec.Name).
                                Where(organization => (typeClient.Equals("true") && (organization.OrganizationType == OrganizationType.Client || organization.OrganizationType == OrganizationType.SuperClient))
                                || (typeVendor.Equals("true") && organization.OrganizationType == OrganizationType.Vendor)
                                || (typeAdmin.Equals("true") && organization.OrganizationType == OrganizationType.Admin)).ToList();
                    try
                    {
                        organizations = query.ToList();
                    }
                    catch (Exception ee) { }
                }
                else if (typeClient.Equals("false") && typeVendor.Equals("false") && typeAdmin.Equals("false"))
                {
                   var query = entity.Organizations.OrderBy(rec => rec.Name).Where(organization => !string.IsNullOrEmpty(companyName) && organization.Name.ToLower().Contains(companyName.ToLower())).ToList();
                    
                    try
                    {
                        organizations = query.ToList();
                    }
                    catch (Exception ee) { }
                }
                else
                {
                   var query = entity.Organizations.OrderBy(rec => rec.Name).
                                Where(organization => ((!string.IsNullOrEmpty(companyName) && organization.Name.ToLower().Contains(companyName.ToLower())) &&
                                        (typeClient.Equals("true") && (organization.OrganizationType.Contains(OrganizationType.Client) || organization.OrganizationType.Contains(OrganizationType.SuperClient))))
                                || ((!string.IsNullOrEmpty(companyName) && organization.Name.ToLower().Contains(companyName.ToLower())) &&
                                    (typeVendor.Equals("true") && organization.OrganizationType.Contains(OrganizationType.Vendor)))
                                || ((!string.IsNullOrEmpty(companyName) && organization.Name.ToLower().Contains(companyName.ToLower())) &&
                                    (typeAdmin.Equals("true") && organization.OrganizationType.Contains(OrganizationType.Admin)))).ToList();

                    try
                    {
                        organizations = query.ToList();
                    }
                    catch (Exception ee) { }
                }

                foreach (var organization in organizations)
                {
                    OrganizationData orgData = new OrganizationData();
                    orgData.OrganizationID = organization.OrganizationID;
                    orgData.Name = organization.Name;
                    orgData.OrganizationType = organization.OrganizationType;

                    organizationsData.Add(orgData);
                }

                return organizationsData;
            }
        }

        public long PostOrganization(OrganizationData newOrganization, long orgId)
        {
            using (var entity = new EFDbContext())
            {
                Organizations organization = mapper1.Map<Organizations>(newOrganization);
                if (orgId == -5 || orgId == -1)
                {
                    entity.Organizations.Add(organization);
                    entity.Entry(organization).State = EntityState.Added;
                }
                else
                {
                    entity.Entry(organization).State = EntityState.Modified;
                }

                entity.SaveChanges();

                return organization.OrganizationID;
            }
        }

        public string CheckAnyOrganizationHasTaxId(string taxId, long? organizationId)
        {
            using (var entity = new EFDbContext())
            {
                Organizations org;
                if (organizationId == null)
                    org = entity.Organizations.FirstOrDefault(record =>
                        record.TaxID.Replace("-", "") == taxId.Replace("-", ""));
                else
                    org = entity.Organizations.FirstOrDefault(record =>
                        record.TaxID.Replace("-", "") == taxId.Replace("-", "") &&
                        record.OrganizationID != organizationId);

                if (org == null)
                    return "";
                else
                    return org.Name;
            }
        }

        public void PostSubClient(long orgId, long subClientId)
        {
            using (var entity = new EFDbContext())
            {
                SubClients hasSubClient = entity.SubClients.FirstOrDefault(rec => rec.SuperClientId == orgId && rec.ClientId == subClientId);
                if (hasSubClient == null || hasSubClient.IsActive == false)
                {
                    if (hasSubClient == null)
                    {
                        SubClients subClient = new SubClients();
                        subClient.SuperClientId = orgId;
                        subClient.ClientId = subClientId;
                        subClient.IsActive = true;

                        entity.Entry(subClient).State = EntityState.Added;
                    }
                    else
                    {
                        hasSubClient.IsActive = true;
                        entity.Entry(hasSubClient).State = EntityState.Modified;
                    }
                }

                if (hasSubClient != null)
                {
                    var hasTemplate = entity.ClientTemplates.FirstOrDefault(r => r.ClientID == subClientId);
                    if (hasTemplate == null)
                    {
                        entity.Entry(hasSubClient).State = EntityState.Deleted;
                    }
                    else
                    {
                        hasSubClient.IsActive = false;
                        entity.Entry(hasSubClient).State = EntityState.Modified;
                    }
                }

                entity.SaveChanges();
            }
        }

        public bool IsVendorHasFinancialData(long vendorId)
        {
            using (var entity = new EFDbContext())
            {
                return entity.FinancialAnalyticsModel.Any(r => r.VendorId == vendorId);
            }
        }

        public List<ArchiveOrganizationDetailsViewModel> GetArchivedArchiveOrganizationDetails(long vendorId)
        {
            using (EFDbContext entity = new EFDbContext())
            {
                {
                    List<ArchiveOrganizationDetails> vendorArchivedDetails = entity.ArchiveOrganizationDetails.Where(rec => rec.OrganizationId == vendorId).ToList();
                    List<ArchiveOrganizationDetailsViewModel> vendorArchivedVersions = new List<ArchiveOrganizationDetailsViewModel>();

                    foreach (ArchiveOrganizationDetails version in vendorArchivedDetails)
                    {
                        ArchiveOrganizationDetailsViewModel vendorArchivedData = new ArchiveOrganizationDetailsViewModel();

                        vendorArchivedData.VersionNo = version.VersionNo;
                        vendorArchivedData.ChangedBy = version.SystemUsers.Contacts.FirstOrDefault() != null
                            ? version.SystemUsers.Contacts[0].FirstName
                            : version.SystemUsers.Email;
                        vendorArchivedData.ChangedDateTime = version.ChangedDateTime;
                        vendorArchivedData.LegalNameOfBusiness = version.LegalNameOfBusiness;
                        vendorArchivedData.Address1 = version.Address1;
                        vendorArchivedData.Address2 = version.Address2;
                        vendorArchivedData.City = version.City;
                        vendorArchivedData.State = version.State;
                        vendorArchivedData.Zip = version.Zip;
                        vendorArchivedData.Country = version.Country;
                        vendorArchivedData.PhoneNumber = version.PhoneNumber;
                        vendorArchivedData.FaxNumber = version.FaxNumber;
                        vendorArchivedData.WebsiteUrl = version.WebsiteURL;
                        vendorArchivedData.FederalIdNumber = version.FederalIDNumber;
                        vendorArchivedData.PrincipalCompanyOfficerName = version.PrincipalCompanyOfficerName;
                        vendorArchivedData.TaxId = version.TaxID;
                        vendorArchivedData.OrgRepresentativeName = version.OrgRepresentativeName;
                        vendorArchivedData.OrgRepresentativeEmail = version.OrgRepresentativeEmail;

                        vendorArchivedVersions.Add(vendorArchivedData);
                    }

                    return vendorArchivedVersions;
                }
            }
        }
    }
}
