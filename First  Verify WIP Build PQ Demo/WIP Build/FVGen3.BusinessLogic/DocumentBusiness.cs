﻿using FVGen3.BusinessLogic.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using FVGen3.Domain.Entities;
using AutoMapper;
using FVGen3.Domain.Concrete;
using FVGen3.Domain.LocalModels;

using System.Data.SqlClient;
using AhaApps.Libraries.Extensions;
using FVGen3.DataLayer.DTO;
using System.Data.Entity;
using Resources;
using System.Web;
using System.IO;

namespace FVGen3.BusinessLogic
{
    public class DocumentBusiness : BaseBusinessClass, IDocumentBusiness
    {
        IMapper mapper;
        public DocumentBusiness()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Document, DocumentLogs>();
                cfg.CreateMap<DocumentLogs, Document>();
            });
            mapper = config.CreateMapper();
        }
        public bool RecordLog(Guid documentId, Guid UserId, string ModificationType)
        {
            using (var entity = new EFDbContext())
            {
                var oldDoc = entity.Document.Find(documentId);
                var docLogs = mapper.Map<DocumentLogs>(oldDoc);
                if (docLogs != null)
                {
                    docLogs.ModificationType = ModificationType;
                    docLogs.ModifiedBy = UserId;
                    docLogs.ModifiedOn = DateTime.Now;
                    entity.DocumentLogs.Add(docLogs);
                    entity.SaveChanges();
                }
            }
            return false; 
        }
        public List<DocumentInfo> GetLogs(Guid documentId)
        {
            using (var entity = new EFDbContext())
            {
                var oldDoc = (from r in entity.DocumentLogs.Where(r => r.DocumentId == documentId).ToList()
                              select new
                              {
                
                    DocumentName = r.DocumentName,
                    Status = r.DocumentStatus,
                    DocumentType = r.DocumentType.DocumentTypeName,
                    ExpirationDate = r.Expires,
                    ModifiedOn = r.ModifiedOn,
                    ModificationType = r.ModificationType,
                    ModifiedBy = r.ModifiedUser.Contacts.Any() ?
                            r.ModifiedUser.Contacts.FirstOrDefault().FirstName + " " +
                            r.ModifiedUser.Contacts.FirstOrDefault().LastName : ""
                            }).ToList()
                            .Select(r => new DocumentInfo()
                {
                    DocumentName = r.DocumentName,
                    Status = r.Status,
                    DocumentType = r.DocumentType,
                    ExpirationDate = r.ExpirationDate!=null? r.ExpirationDate.Value.ToString("MM-dd-yyyy"):"",
                    ModifiedOn = r.ModifiedOn,
                    ModificationType = r.ModificationType,
                    ModifiedBy = r.ModifiedBy
                }).ToList();
                return oldDoc;
            }
            //return false;
        }
        public DataSourceResponse GetRecentDocs(string filters, DataSourceRequest request,string Role)
        {
            using (var entity = new EFDbContext())
            {
                var sql = @"select d.* from Document as d join Prequalification as lp
on lp.PrequalificationId =d.ReferenceRecordID and d.ReferenceType ='PreQualification'
join Organizations as Client on client.OrganizationID=lp.ClientId
join Organizations as vendor on vendor.OrganizationID=lp.VendorId
join PrequalificationStatus ps on ps.PrequalificationStatusId=lp.PrequalificationStatusId
where d.ReferenceType ='PreQualification' and d.Uploaded>=DATEADD(MM,-1,convert(date,GETDATE())) and (ShowOnDashboard=1 or ShowOnDashboard=null) and (d.DocumentStatus is null or d.DocumentStatus!='Approved' and  d.DocumentStatus!='Deleted') 
and (lp.PrequalificationStatusId in (9, 8, 13, 24, 25, 26, 4, 23, 16)
or(client.OrganizationType='Super Client' and lp.PrequalificationStatusId in(5))) and d.ReferenceType ='PreQualification' and (@filter='' or (vendor.Name like '%'+@filter+'%'  or client.Name like '%'+@filter+'%' or ps.PrequalificationStatusName like '%'+@filter+'%' ))
order by Expires,Uploaded";
                var count = entity.Document.SqlQuery(sql, new SqlParameter("@filter", filters));
                var lastSevenDaysDocswithFinalStatusPrequal = entity.Document.SqlQuery(sql, new SqlParameter("@filter", filters));
                // sumanth ends on 10/05/2015.
                // var NumberOfItemsToSkip = pageNumber * 30;
                int totalCount = count.Count();
                List<LocalRecentDocsDetails> recentDocsDetailsList = new List<LocalRecentDocsDetails>();
                foreach (var doc in lastSevenDaysDocswithFinalStatusPrequal.Skip((request.PageSize * (request.Page - 1))).Take(request.PageSize).ToList())
                {
                    //if (recentDocsDetailsList.Count >= 30)
                    //    break;
                    //if (NumberOfItemsToSkip > 0)
                    //{
                    //    NumberOfItemsToSkip--;
                    //    continue;
                    //}

                    var docDetails = new LocalRecentDocsDetails();

                    //Rajesh on 09-10-2013
                    string urlRecentDocs = "#!";
                    try
                    {
                        if (doc.ReferenceType.Equals("PreQualification"))
                        {
                            var preQualification = entity.Prequalification.Find(doc.ReferenceRecordID);
                           
                            docDetails.ExpirationDate = doc.Expires == null ? "" : doc.Expires.Value.ToString("MM/dd/yyyy");
                            docDetails.TemplateID = preQualification.ClientTemplates.TemplateID;
                            docDetails.TemplatesectionId = preQualification.ClientTemplates.Templates.TemplateSections.FirstOrDefault(rec => rec.TemplateSectionType == 4).TemplateSectionID;
                            docDetails.PrequalificationId = doc.ReferenceRecordID.Value;
                            // Kiran on 12/03/2013
                            docDetails.Initials = preQualification.UserIdSigner.Contacts[0].FirstName[0] + "" +
                                                    preQualification.UserIdSigner.Contacts[0].LastName[0];

                            


                        }
                    }
                    catch (Exception ee) { }

                    
                    docDetails.DocumentId = doc.DocumentId.ToString();//sumanth on 10/05/2015 for SMI-226
                    docDetails.UserName = "";
                    try
                    {
                        docDetails.UserName = doc.Systemusers.Contacts[0].LastName + ", " + doc.Systemusers.Contacts[0].FirstName;
                        docDetails.Email = doc.Systemusers.Email;
                       
                    }
                    catch (Exception ee) { if (doc.Systemusers != null) docDetails.UserName = doc.Systemusers.Email; } // Kiran on 03/19/2015

                    docDetails.DocumentTypeName = doc.DocumentType.DocumentTypeName;
                    docDetails.DateUploaded = doc.Uploaded;

                    var prequal = entity.Prequalification.FirstOrDefault(row => row.PrequalificationId == doc.ReferenceRecordID);
                    //Siva on 28th Aug
                    if (prequal != null)
                    {
                        docDetails.VendorName = prequal.Vendor.Name;
                        docDetails.ClientName = prequal.Client.Name;
                        docDetails.VendorStatus = prequal.PrequalificationStatus.PrequalificationStatusName; // Mani on 7/22/2015 for fv-170
                    }
                    else
                    {
                        docDetails.VendorName = "";
                        docDetails.ClientName = "";
                        docDetails.VendorStatus = "";// Mani on 7/22/2015 for fv-170
                    }
                   

                    recentDocsDetailsList.Add(docDetails);
                  
                }
                if (Role=="Admin")
                {
                    recentDocsDetailsList.ForEach(rec => rec.HasDelete = true);
                }
                var result = new DataSourceResponse()
                {
                    Total = totalCount,//RecentDocsDetailsList.Count(),
                    Data = recentDocsDetailsList
                };
                return result;
            }
        }

        public LocalSupportedDocument GetSupportingDocument(long subSectionId, Guid? documentId)
        {
            using (var entity = new EFDbContext())
            {
                LocalSupportedDocument sDoc = new LocalSupportedDocument();

                TemplateSubSections subSection = entity.TemplateSubSections.Find(subSectionId);
                if (subSection != null)
                    sDoc.SectionId = subSection.TemplateSections.TemplateSectionID;
                Document doc = entity.Document.Find(documentId);
                if (doc != null)
                {
                    sDoc.DocumentTypeId = doc.DocumentTypeId + "";
                    sDoc.ClientId = doc.ClientId.ToLongNullSafe();
                    sDoc.DocumentId = doc.DocumentId;
                    sDoc.ClientOrganizations = doc.ClientOrganizations;
                    sDoc.Expires = doc.Expires.ToString();
                }

                return sDoc;
            }
        }

        public void PostDocumentBusiness(LocalSupportedDocument form, string role, Guid userId, HttpPostedFileBase file, Document doc)
        {
            using (var entity = new EFDbContext())
            {
                Prequalification pq = entity.Prequalification.Find(form.PrequalificationId);
                
                if (form.DocumentId != null) //If it is edit or replace mode
                {
                    doc = entity.Document.Find(form.DocumentId);
                    var documentNotifications = entity.OrganizationsNotificationsEmailLogs
                        .Where(rec => rec.DocumentId == form.DocumentId).ToList();
                    if (documentNotifications != null)
                    {
                        foreach (var notification in documentNotifications)
                        {
                            notification.RecordStatus = "Archived";
                            entity.Entry(notification).State = EntityState.Modified;
                            entity.SaveChanges();
                        }
                    }

                    if (doc.DocumentTypeId != Convert.ToInt64(form.DocumentTypeId))
                    {
                        TemplateSubSections subSection =
                            entity.TemplateSubSections.FirstOrDefault(rec => rec.SubSectionID == form.subSectionId);
                        if (subSection != null)
                        {
                            long templateSectionId = subSection.TemplateSectionID;
                            PrequalificationCompletedSections prequalCompletedSectionRecord =
                                entity.PrequalificationCompletedSections.FirstOrDefault(rec =>
                                    rec.TemplateSectionId == templateSectionId &&
                                    rec.PrequalificationId == form.PrequalificationId);
                            if (prequalCompletedSectionRecord != null)
                            {
                                entity.PrequalificationCompletedSections.Remove(prequalCompletedSectionRecord);
                                entity.SaveChanges();
                            }
                        }
                    }
                }
                else if (pq != null && pq.ClientTemplates.Templates.IsERI)
                    doc.ClientId = form.ClientId;

                if (pq != null)
                {
                    long vendorId = pq.VendorId;

                    var statuses = new List<long>() {5, 9, 8, 13, 24, 25, 26, 4, 23, 16};
                    if (role.Equals(LocalConstants.Vendor) && statuses.Contains(pq.PrequalificationStatusId))
                    {
                        doc.ShowOnDashboard =
                            true;
                    }
                    else
                        doc.ShowOnDashboard = false;

                    doc.OrganizationId = vendorId;
                    doc.ReferenceRecordID = form.PrequalificationId;
                    doc.ReferenceType = "PreQualification";
                    doc.SectionId = form.SectionId;

                    if (form.DocumentId == null)
                    {
                        doc.Uploaded = DateTime.Now;
                        doc.SystemUserAsUploader = userId;
                    }

                    if (form.Expires != null)
                        doc.Expires = Convert.ToDateTime(form.Expires);
                    else
                        doc.Expires = null;

                    doc.DocumentTypeId = Convert.ToInt64(form.DocumentTypeId);

                    doc.UpdatedOn = DateTime.Now;
                    doc.UpdatedBy = userId;

                    if (form.Mode != null && form.Mode == 1) ;
                    else if (file != null && file.ContentLength > 0)
                    {
                        var extension = Path.GetExtension(file.FileName);
                        doc.DocumentName = Path.GetFileName(file.FileName);
                    }

                    if (form.DocumentId == null)
                    {
                        entity.Document.Add(doc);
                    }
                    else
                    {
                        entity.Entry(doc).State = EntityState.Modified;
                    }

                    entity.SaveChanges();
                }
            }
        }
    }
}
