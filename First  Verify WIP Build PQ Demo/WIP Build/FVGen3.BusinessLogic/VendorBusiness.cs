﻿/*
Page Created Date:  04/04/2017
Created By: Rajesh Pendela
Purpose: All Business operations related to Vendor will be performed here
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using FVGen3.BusinessLogic.Interfaces;
using FVGen3.DataLayer.DTO;
using FVGen3.Domain.Concrete;
using FVGen3.Domain.Entities;
using System.Data.SqlClient;
using System.IO;
using FVGen3.WebUI.Constants;
using AhaApps.Libraries.Extensions;
using AhaApps.Libraries.IO;
using FVGen3.Domain.LocalModels;
using System.Data.Entity;
using System.Configuration;
using System.IO;
using Resources;
using AhaApps.Libraries.IO;
using FVGen3.Domain.ViewModels;
using FVGen3.Common;
namespace FVGen3.BusinessLogic
{
    public class VendorBusiness : BaseBusinessClass, IVendorBusiness
    {
        IPrequalificationBusiness _PrequalificationBusiness;
        IOrganizationBusiness _orgBusiness;
        IClientBusiness _clientBusiness;
        private ISystemUserBusiness _SysBusiness;
        public VendorBusiness(IPrequalificationBusiness _PrequalificationBusiness, ISystemUserBusiness _SysBusiness, IOrganizationBusiness _orgBusiness)
        {

            this._PrequalificationBusiness = _PrequalificationBusiness;            this._SysBusiness = _SysBusiness;            this._orgBusiness = _orgBusiness;            //this._PrequalificationBusiness = new PrequalificationBusiness(_orgBusiness, _clientBusiness);
        }
        /// <summary>
        /// To get the vendor summary based on client
        /// </summary>
        /// <param name="vendorId"></param>
        /// <param name="Userid"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public VendorSummary GetVendorSummary(long vendorId, Guid Userid, long clientId)
        {
            var summary = new VendorSummary();
            using (var entity = new EFDbContext())
            {
                var vendor = entity.Organizations.Find(vendorId);
                if (vendor != null)
                {
                    summary.Address = vendor.Address1 + (vendor.Address2 ?? "");
                    summary.Address1 = vendor.Address1 ?? "";
                    summary.Address2 = vendor.Address2 ?? "";
                    summary.State = vendor.State;
                    summary.City = vendor.City;
                    summary.Zip = vendor.Zip;
                    summary.Name = vendor.Name.ToString() ?? "";
                    summary.PhoneNumber = vendor.PhoneNumber ?? "";
                    summary.Fax = vendor.FaxNumber ?? "";
                    summary.Website = vendor.WebsiteURL ?? "";
                    if (summary.Website != "" && !summary.Website.StartsWith("http"))
                    {
                        summary.Website = "http://" + summary.Website;
                    }
                }
                summary.Statuses = entity.LatestPrequalification.Where(rec => rec.VendorId == vendorId).ToList().Select(
                     rec => _PrequalificationBusiness.GetPQStatusForBanner(rec.PrequalificationId,
                     rec.PrequalificationStatus.PrequalificationStatusName, clientId, Userid).Value).ToList();
                //   summary.Statuses =
                //     entity.LatestPrequalification.Where(rec => rec.VendorId == vendorId).Select(
                //       rec => rec.PrequalificationStatus.PrequalificationStatusName).ToList();
                // ReSharper disable once InconsistentNaming
                var latestPQIds =
                    entity.LatestPrequalification.Where(rec => rec.VendorId == vendorId)
                        .Select(rec => rec.PrequalificationId);
                summary.Biddings =
                     entity.PrequalificationReportingData.Where(
                            rec => rec.PrequalificationId != null &&
                                   latestPQIds.Contains((long)rec.PrequalificationId) &&
                                   rec.ReportingType == 0)//--0-Biddings
                        .Join(entity.BiddingInterests, p => p.ReportingDataId, b => b.BiddingInterestId, (p, b) => b)
                        .Select(r => r.BiddingInterestName).ToList();
                summary.Proficiancy =
                    entity.PrequalificationReportingData.Where(
                            rec => rec.PrequalificationId != null &&
                                   latestPQIds.Contains((long)rec.PrequalificationId) &&
                                   rec.ReportingType == 1)//--1-Proficiency
                        .Join(entity.ProficiencyCapabilities, p => p.ReportingDataId, b => b.ProficiencyId, (p, b) => b)
                        .Select(r => r.ProficiencyName).ToList();
                summary.RickLevels =
                    entity.LatestPrequalification.Where(r => r.VendorId == vendorId && r.ClientTemplates.Templates.RiskLevel != null && r.ClientTemplates.Templates.RiskLevel.Trim() != "")
                        .Select(r => r.ClientTemplates.Templates.RiskLevel).ToList();
            }
            return summary;
        }
        public List<Dropdown> GetOrganizations(string name, string usertype)
        {
            using (var context = new EFDbContext())
            {
                var orgs = context.Organizations.Where(r => r.OrganizationType == usertype);
                if (!string.IsNullOrEmpty(name))
                    orgs = orgs.Where(r => r.Name.Contains(name));

                return orgs.Select(r => new Dropdown { ID = r.OrganizationID, Name = r.Name }).Distinct().OrderBy(rec => rec.Name).ToList();
            }
        }
        public List<Dropdown> GetVendors(string name, long ClientId)
        {
            using (var context = new EFDbContext())
            {
                var Vendors = context.Prequalification.Where(r => r.ClientId == ClientId).Select(r => r.Vendor)
               .Select(r => new Dropdown { ID = r.OrganizationID, Name = r.Name }).Distinct().OrderBy(rec => rec.Name).ToList();
                if (!string.IsNullOrEmpty(name))
                {
                    Vendors = Vendors.Where(r => r.Name.ToLower().Contains(name.ToLower())).ToList();
                }
                return Vendors;
            }
        }
        public List<KeyValuePair<string, string>> GetAllVendors()
        {
            using (var context = new EFDbContext())
            {
                return context.Organizations.Where(r => r.OrganizationType == "Vendor").Select(r => new { r.OrganizationID, r.Name }).OrderBy(rec => rec.Name).ToList().
                    Select(rec =>
                        new KeyValuePair<string, string>(rec.OrganizationID.ToString(), rec.Name)).ToList();
            }
        }
        /// <summary>
        /// To get the ERI vendor summary
        /// </summary>
        /// <param name="vendorId"></param>
        /// <returns></returns>
        public VendorSummary GetERIVendorSummary(long vendorId)
        {
            var summary = new VendorSummary();
            using (var entity = new EFDbContext())
            {
                var vendor = entity.Organizations.Find(vendorId);
                if (vendor != null)
                {
                    summary.Address = vendor.Address1 + (vendor.Address2 ?? "");
                    summary.Address1 = vendor.Address1 ?? "";
                    summary.Address2 = vendor.Address2 ?? "";
                    summary.State = vendor.State;
                    summary.City = vendor.City;
                    summary.Zip = vendor.Zip;
                    summary.Name = vendor.Name ?? "";
                    summary.PhoneNumber = vendor.PhoneNumber ?? "";
                    summary.Fax = vendor.FaxNumber ?? "";
                    summary.Website = vendor.WebsiteURL ?? "";
                    if (summary.Website != "" && !summary.Website.StartsWith("http"))
                    {
                        summary.Website = "http://" + summary.Website;
                    }
                }
                var vendorERIPrequalifications =
                        entity.LatestPrequalification.Where(rec => rec.VendorId == vendorId && rec.ClientTemplates.Templates.IsERI).ToList();

                foreach (var prequalification in vendorERIPrequalifications)
                {
                    var summaryinfo = prequalification.PrequalificationSites.GroupBy(r => r.ClientId).Select(r => GetPQStatusForERIInvitation(r)).ToList();
                    summary.Statuses.AddRange(summaryinfo);
                }
                // ReSharper disable once InconsistentNaming
                var latestPQIds =
                    entity.LatestPrequalification.Where(rec => rec.VendorId == vendorId && rec.ClientTemplates.Templates.IsERI)
                        .Select(rec => rec.PrequalificationId);
                summary.Biddings =
                     entity.PrequalificationReportingData.Where(
                            rec => rec.PrequalificationId != null &&
                                   latestPQIds.Contains((long)rec.PrequalificationId) &&
                                   rec.ReportingType == 0)//--0-Biddings
                        .Join(entity.BiddingInterests, p => p.ReportingDataId, b => b.BiddingInterestId, (p, b) => b)
                        .Select(r => r.BiddingInterestName).ToList();
                summary.Proficiancy =
                    entity.PrequalificationReportingData.Where(
                            rec => rec.PrequalificationId != null &&
                                   latestPQIds.Contains((long)rec.PrequalificationId) &&
                                   rec.ReportingType == 1)//--1-Proficiency
                        .Join(entity.ProficiencyCapabilities, p => p.ReportingDataId, b => b.ProficiencyId, (p, b) => b)
                        .Select(r => r.ProficiencyName).ToList();
                summary.RickLevels =
                    entity.LatestPrequalification.Where(r => r.VendorId == vendorId && r.ClientTemplates.Templates.RiskLevel != null && r.ClientTemplates.Templates.RiskLevel.Trim() != "" && r.ClientTemplates.Templates.IsERI
                    )
                        .Select(r => r.ClientTemplates.Templates.RiskLevel).ToList();
            }
            return summary;
        }
        /// <summary>
        /// To get the sites status for ERI PQ
        /// </summary>
        /// <param name="sites"></param>
        /// <returns></returns>
        private string GetPQStatusForERIInvitation(IEnumerable<PrequalificationSites> sites)
        {
            var temp = sites.Select(r => r.SiteStatus).Distinct();
            return temp.Count() > 1 ? "Multiple" : temp.FirstOrDefault().PrequalificationStatusName;

        }
        /// <summary>
        /// To search the vendors based on different filters
        /// </summary>
        /// <param name="UserId"></param>
        /// <param name="ERIOnly"></param>
        /// <param name="SingleClient"></param>
        /// <param name="orderBy"></param>
        /// <param name="status"></param>
        /// <param name="isAsc"></param>
        /// <param name="recordsFiltered"></param>
        /// <param name="draw"></param>
        /// <param name="start"></param>
        /// <param name="length"></param>
        /// <param name="clientId"></param>
        /// <param name="vendorName"></param>
        /// <param name="biddings"></param>
        /// <param name="claycoBiddings"></param>
        /// <param name="proficiency"></param>
        /// <param name="city"></param>
        /// <param name="state"></param>
        /// <param name="exceptThisClient"></param>
        /// <param name="pqStatusFilter"></param>
        /// <returns></returns>
        public List<VendorData> GetClientVendors(Guid UserId, bool ERIOnly, bool SingleClient, int orderBy, int status, bool isAsc, out int recordsFiltered, int draw, int start, int length, long clientId, string vendorName, List<long> biddings, List<long> claycoBiddings, List<long> proficiency, string city, string state, string Country, bool exceptThisClient, int pqStatusFilter)
        {
            using (var entity = new EFDbContext())
            {
                var pStatus = new List<long>() { 3, 4, 5, 6, 7, 8, 9, 10, 11, 13, 14, 15, 16, 23, 24, 25, 26, 27 };
                var nStatus = new List<long>() { 10 };
                var pqs = entity.LatestPrequalification.Where(rec => rec.ClientId == clientId);

                if (exceptThisClient)
                {

                    if (ERIOnly)
                    {
                        var eriPQsWithOthers = entity.Database.SqlQuery<PrequalificationClient>("select * from PrequalificationClient where ','+ClientIds+',' not like @p", new SqlParameter("@p", "%," + clientId + ",%")).Select(r => r.PQId).ToList();
                        var legalClients = entity.SubClients.Where(r => r.ClientId == clientId).Select(r => r.SuperClientId).Distinct().ToList();
                        pqs = entity.LatestPrequalification.Where(r => eriPQsWithOthers.Contains(r.PrequalificationId) && legalClients.Contains(r.ClientId));
                    }
                    else if (!ERIOnly && !SingleClient)
                    {
                        var legalClients = entity.SubClients.Where(r => r.ClientId == clientId).Select(r => r.SuperClientId).Distinct().ToList();
                        pqs = entity.LatestPrequalification.Where(rec => rec.ClientId != clientId && !legalClients.Contains(rec.ClientId));
                    }
                    else
                    {
                        pqs = entity.LatestPrequalification.Where(rec => rec.ClientId != clientId);
                    }
                }
                else
                {
                    var eriPQs = entity.Database.SqlQuery<PrequalificationClient>("select * from PrequalificationClient where ','+ClientIds+',' like @p", new SqlParameter("@p", "%," + clientId + ",%")).Select(r => r.PQId).ToList();
                    pqs = entity.LatestPrequalification.Where(rec => rec.ClientId == clientId || eriPQs.Contains(rec.PrequalificationId));
                }
                //var original = pqs;
                switch (pqStatusFilter)
                {
                    case 1:
                        pqs = pqs.Where(rec => pStatus.Contains(rec.PrequalificationStatusId)); break;
                    case 2:
                        pqs = status == 0
                            ? pqs.Where(rec => nStatus.Contains(rec.PrequalificationStatusId))
                            : pqs.Where(rec => rec.PrequalificationStatusId == status);
                        break;
                }
                if (!string.IsNullOrEmpty(vendorName))
                    pqs = pqs.Where(rec => rec.Vendor.Name.Contains(vendorName));
                if (!string.IsNullOrEmpty(city))
                    pqs = pqs.Where(rec => rec.Vendor.City.Contains(city.Trim()));
                if (!string.IsNullOrEmpty(state))
                    pqs = pqs.Where(rec => rec.Vendor.State.Contains(state.Trim()));
                if (!string.IsNullOrEmpty(Country))
                    pqs = pqs.Where(rec => rec.Vendor.Country.Contains(Country.Trim()));
                if (biddings != null && biddings.Any())
                    pqs = exceptThisClient
                        ? pqs.Where(
                            rec =>
                                rec.PrequalificationReportingData.Any(
                                    rec1 =>
                                        rec1.ReportingDataId != null && biddings.Contains((long)rec1.ReportingDataId) &&
                                        rec1.ReportingType == 0 && rec1.ClientId != clientId))
                        : pqs.Where(
                            rec =>
                                rec.PrequalificationReportingData.Any(
                                    rec1 =>
                                        biddings.Contains(rec1.PrequalificationRepDataId) && rec1.ReportingType == 0 &&
                                        rec1.ClientId == clientId));

                if (claycoBiddings != null && claycoBiddings.Any())
                    pqs = exceptThisClient
                        ? pqs.Where(
                            rec =>
                                rec.PrequalificationReportingData.Any(
                                    rec1 =>
                                        rec1.ReportingDataId != null && claycoBiddings.Contains((long)rec1.ReportingDataId) &&
                                        rec1.ReportingType == 3 && rec1.ClientId != clientId))
                        : pqs.Where(
                            rec =>
                                rec.PrequalificationReportingData.Any(
                                    rec1 =>
                                        claycoBiddings.Contains(rec1.PrequalificationRepDataId) && rec1.ReportingType == 3 &&
                                        rec1.ClientId == clientId));

                if (proficiency != null && proficiency.Any())
                    pqs = exceptThisClient
                        ? pqs.Where(
                            rec =>
                                rec.PrequalificationReportingData.Any(
                                    rec1 =>
                                        rec1.ReportingDataId != null &&
                                        proficiency.Contains((long)rec1.ReportingDataId) && rec1.ReportingType == 1 &&
                                        rec1.ClientId != clientId))
                        : pqs.Where(
                            rec =>
                                rec.PrequalificationReportingData.Any(
                                    rec1 =>
                                        proficiency.Contains(rec1.PrequalificationRepDataId) && rec1.ReportingType == 1 &&
                                        rec1.ClientId == clientId));

                var vendors = pqs.Select(rec => new
                {
                    rec.Vendor,
                    rec.PrequalificationId,
                    rec.PrequalificationStatus.PrequalificationStatusName
                }).Where(rec => rec.Vendor.OrganizationType == "Vendor").Distinct();
                if (exceptThisClient)
                {
                    var currentClientVendor =
                        entity.LatestPrequalification.Where(rec => rec.ClientId == clientId).Select(rec => rec.VendorId);
                    vendors = vendors.Where(rec => !currentClientVendor.Contains(rec.Vendor.OrganizationID));
                }
                var result = exceptThisClient ? vendors.Select(rec => new VendorData
                {
                    OrgId = rec.Vendor.OrganizationID,
                    OrgName = rec.Vendor.Name.Trim(),

                }).Distinct().OrderBy(r => r.OrgName) :

                vendors.Select(rec => new VendorData
                {
                    OrgId = rec.Vendor.OrganizationID,
                    OrgName = rec.Vendor.Name.Trim(),
                    PQId = rec.PrequalificationId,
                    CurrentClientLatestStatus = rec.PrequalificationStatusName

                }).OrderBy(r => r.OrgName);
                switch (orderBy)
                {
                    case 0:
                        result = isAsc ? result.OrderBy(r => r.OrgName) : result.OrderByDescending(r => r.OrgName);
                        break;
                    case 1:
                        result = isAsc
                            ? result.OrderBy(r => r.CurrentClientLatestStatus)
                            : result.OrderByDescending(r => r.CurrentClientLatestStatus);
                        break;
                }
                //var d1 = DateTime.Now;
                recordsFiltered = result.Count();
                //var d2 = DateTime.Now;
                var r1 = result.Skip(start).Take(length).ToList();

                //if (!exceptThisClient)
                //{
                //    r1.ForEach(rec =>
                //    rec.CurrentClientLatestStatus = 
                //    _PrequalificationBusiness.GetPQStatusForBanner((long)rec.PQId, rec.CurrentClientLatestStatus, 0L, UserId).Value);
                //}
                //var d3 = DateTime.Now;

                return r1;

            }
        }
        /// <summary>
        /// To search the ERI vendors based on different filters
        /// </summary>
        /// <param name="orderBy"></param>
        /// <param name="status"></param>
        /// <param name="isAsc"></param>
        /// <param name="recordsFiltered"></param>
        /// <param name="draw"></param>
        /// <param name="start"></param>
        /// <param name="length"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public List<VendorData> GetERIClientVendors(int orderBy, int status, bool isAsc, out int recordsFiltered, int draw, int start, int length, long clientId)
        {
            using (var entity = new EFDbContext())
            {
                var ERIPqs = entity.PrequalificationClient.SqlQuery("select * from PrequalificationClient where ','+ClientIds+',' not like @p0", "%," + clientId + ",%").Select(r => r.PQId);

                var pqs = entity.LatestPrequalification.Where(rec => rec.Client.OrganizationType == "Super Client" && ERIPqs.Contains(rec.PrequalificationId)).ToList();
                var result = pqs.Select(rec => new VendorData
                {
                    OrgId = rec.Vendor.OrganizationID,
                    OrgName = rec.Vendor.Name.Trim(),
                    PQId = rec.PrequalificationId,
                    CurrentClientLatestStatus = rec.PrequalificationStatus.PrequalificationStatusName
                }).OrderBy(r => r.OrgName);

                recordsFiltered = result.Count();
                var r1 = result.Skip(start).Take(length).ToList();

                return r1;
            }
        }
        /// <summary>
        /// To search the vendors with specifc prefix
        /// </summary>
        /// <param name="prefix"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public List<string> GetVendors(string prefix, int count)
        {
            using (var context = new EFDbContext())
            {
                return context.Organizations.Where(r => r.OrganizationType == "Vendor" && r.Name.StartsWith(prefix))
                    .Take(count)
                    .Select(r => r.Name)
                    .ToList();
            }
        }
        /// <summary>
        /// To get the vendors specific to the client
        /// </summary>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public List<KeyValuePair<long, string>> GetVendorOrganizationsByClientId(long clientId)
        {
            using (var entity = new EFDbContext())
            {
                return entity.Prequalification.Where(r => r.ClientId == clientId && r.Vendor.OrganizationType == "Vendor")
                    .Select(r => new { r.Vendor.Name, r.Vendor.OrganizationID }).Distinct().OrderBy(r => r.Name)
                    .ToList()
                    .Select(r =>
                        new KeyValuePair<long, string>(r.OrganizationID, r.Name))
                    .ToList();
                //return entity.Organizations.Where(rec => rec.OrganizationType == "Vendor").OrderBy(r => r.Name)
                //    .Select(r =>
                //        new KeyValuePair<long, string>(r.OrganizationID, r.Name)).ToList();
            }
        }
        /// <summary>
        /// To log the PQ email sent
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public bool AddVendorLog(LocalVendorEmail data)
        {

            using (var entity = new EFDbContext())
            {
                var pqs = data.PQIds.Split(',').Select(long.Parse);
                foreach (var PQId in pqs)
                {
                    var log = new VendorEmailLog
                    {
                        BCCAddress = data.BCCAddress,
                        Body = data.Body,
                        CCAddress = data.CCAddress,
                        EmailStatus = data.EmailStatus,
                        PQId = PQId,
                        SendDate = DateTime.Now,
                        ToAddress = data.ToAddress,
                        UserId = data.UserId
                    };
                    entity.VendorEmailLog.Add(log);
                }
                entity.SaveChanges();
                return true;
            }
        }
        /// <summary>
        /// To get the vendors based on different filters
        /// </summary>
        /// <param name="searchType"></param>
        /// <param name="clientId"></param>
        /// <param name="vendorId"></param>
        /// <param name="prequalStatus"></param>
        /// <returns></returns>
        public List<LocalPrequalificationAdvanced> VendorEmailSearch(string searchType, long clientId, long vendorId, string prequalStatus)
        {
            using (var entity = new EFDbContext())
            {
                var result = entity.LatestPrequalification.Where(r => r.ClientId == clientId);
                if (searchType.Equals("Name"))
                {
                    result = result.Where(r => r.VendorId == vendorId);
                }
                else if (!string.IsNullOrEmpty(prequalStatus))
                {
                    var statuses = prequalStatus.Split(',').Select(long.Parse);
                    result = result.Where(r => statuses.Contains(r.PrequalificationStatusId));
                }
                return result.Select(r => new LocalPrequalificationAdvanced()
                {
                    VendorId = r.VendorId,
                    PrequalificationId = r.PrequalificationId,
                    PrequalificationStatusName = r.PrequalificationStatus.PrequalificationStatusName,
                    Name = r.Vendor.Name
                })
                    .ToList();
            }
        }

        public List<LocalinviteVendor> GetInvitedVendors(int pageNumber, string SearchEmail, int InvitationType)
        {
            using (var entity = new EFDbContext())
            {
                var vendors_details = entity.VendorInviteDetails.Where(rec => rec.ShowOnDashboard == true || rec.ShowOnDashboard == null);
                if (!string.IsNullOrEmpty(SearchEmail))
                    vendors_details = vendors_details.Where(rec => rec.VendorEmail.Contains(SearchEmail) || rec.VendorCompanyName.Contains(SearchEmail));
                switch (InvitationType)
                {
                    case 0:
                        vendors_details = vendors_details.Where(r => r.InvitationType == null || r.InvitationType == 0); break;
                    case 1:
                        vendors_details = vendors_details.Where(r => r.InvitationType == 1);
                        break;
                    case 2:
                        vendors_details = vendors_details.Where(r => r.InvitationType == 2); break;

                }
                var vendorsCount = vendors_details.OrderByDescending(rec => rec.InvitationId).Skip(pageNumber * 30).Count();

                return (from vendors in vendors_details.ToList().OrderByDescending(rec => rec.InvitationDate).Skip(pageNumber * 30).Take(30 * (pageNumber + 1)) // Kiran on 9/29/2014 //sumanth on 10/09/2015 for SMI-268
                        select new LocalinviteVendor
                        {
                            HasNext = vendorsCount > 30,
                            HasPrevious = pageNumber != 0,
                            PageNumber = pageNumber,
                            InvitationId = vendors.InvitationId,//sumanth on 10/09/2015 for SMI-268
                            UserEmail = vendors.InvitationSentByUser == null ? "#!" : vendors.SystemUsers.Email,
                            VendorCompanyName = vendors.VendorCompanyName,
                            FullName = GetPQInvitedByForDashboard(vendors.VendorEmail, vendors.ClientId, vendors.InvitationId), // Kiran on 03/18/2015
                                                                                                                                // LastName = vendors.VendorLastName,
                            EmailAddress = vendors.VendorEmail,
                            InvitationDate = vendors.InvitationDate
                        }).ToList();
            }
        }

        public string GetPQInvitedByForDashboard(string vendorEmail, long clientId, Guid invitationId)
        {
            using (var entity = new EFDbContext())
            {
                VendorInviteDetails inviteDetails = entity.VendorInviteDetails.FirstOrDefault(rec => rec.InvitationId == invitationId);

                try
                {
                    long? vendorId = entity.SystemUsers.FirstOrDefault(rec => rec.Email.Trim().Equals(vendorEmail))?.SystemUsersOrganizations.FirstOrDefault()?.OrganizationId;
                    if (vendorId != null)
                    {
                        var prequalification = entity.LatestPrequalification.FirstOrDefault(rec => rec.VendorId == vendorId && rec.ClientId == clientId);
                        if (prequalification != null)
                        {
                            var invitedByDetails = _PrequalificationBusiness.PQInviteInfo(prequalification.PrequalificationId);
                            if (invitedByDetails != null && invitedByDetails.InvitationSentByUser != null)
                                return invitedByDetails.InvitationSentByUser;
                            else
                                return Resources.Resources.Common_label_FirstVerify;
                        }
                    }
                }
                catch { }

                return inviteDetails.InvitationSentByUser == null ? Resources.Resources.Common_label_FirstVerify : (inviteDetails.SystemUsers.Contacts.FirstOrDefault() != null ? inviteDetails.SystemUsers.Contacts[0].LastName + ", " + inviteDetails.SystemUsers.Contacts[0].FirstName : inviteDetails.SystemUsers.Email);
            }
        }
        public DataSourceResponse GetGroupedPrequalifications(long ClientId, string searchType, string SortType, string firstLetterVal, string period, Guid UserId, string Role, string Vendorname, bool HasAnalyticsPermission, long organizationid, DataSourceRequest request)
        {
            using (var entity = new EFDbContext())
            {
                var ERIPqs = entity.PrequalificationClient.SqlQuery("select * from PrequalificationClient where ','+ClientIds+',' like @p0", "%," + ClientId + ",%")
               .Select(r => r.PQId).ToList();

                var prequalificationsGroupedByVendor = entity.LatestPrequalification.Where(rec => rec.ClientId == ClientId || ERIPqs.Contains(rec.PrequalificationId) && rec.PrequalificationStatus.ShowInDashBoard == true);
                if (Role == OrganizationType.Admin)
                {
                    if (_SysBusiness.IsERIUser(UserId))
                    {
                        prequalificationsGroupedByVendor = entity.LatestPrequalification.Where(rec => (rec.ClientId == ClientId || ERIPqs.Contains(rec.PrequalificationId))
                        && rec.ClientTemplates.Templates.IsERI == true);

                    }
                }
                if (Role == OrganizationType.Client || Role == OrganizationType.SuperClient)
                {


                    var sitesRec = entity.SystemUsersBUSites.FirstOrDefault(r => r.SystemUserId == UserId);
                    if (sitesRec != null && !string.IsNullOrEmpty(sitesRec.BUSiteId))
                    {

                        var busites = sitesRec.BUSiteId;
                        var userSites = busites.Split(',').Select(long.Parse).ToList();
                        //var pqids = entityDB.PrequalificationSites.Where(r => userSites.Contains(r.ClientBusinessUnitSiteId.Value)).Select(r => r.PrequalificationId).ToList();

                        prequalificationsGroupedByVendor = entity.LatestPrequalification.Where(r => r.PrequalificationSites.Any(r1 => userSites.Contains(r1.ClientBusinessUnitSiteId.Value)));


                    }
                    if (_SysBusiness.IsERIUser(UserId))
                    {
                        prequalificationsGroupedByVendor = entity.SubClients.FirstOrDefault(r => r.SuperClientId == ClientId) == null ?
                        entity.LatestPrequalification.Where(rec => ERIPqs.Contains(rec.PrequalificationId)) : entity.LatestPrequalification.Where(rec => rec.ClientId == ClientId);


                    }

                }
                prequalificationsGroupedByVendor = prequalificationsGroupedByVendor.Where(r => r.ClientTemplates.Templates.IsHide == false && r.PrequalificationStatus.ShowInDashBoard == true);




                if (!string.IsNullOrEmpty(Vendorname))
                {

                    prequalificationsGroupedByVendor = prequalificationsGroupedByVendor.Where(rec => rec.Vendor.Name.ToLower().Contains(Vendorname.ToLower()));

                    if (period == "searchParticular")
                    {
                        var baseDate = DateTime.Now.AddDays(-25);
                        prequalificationsGroupedByVendor = prequalificationsGroupedByVendor.Where(rec => rec.PrequalificationFinish >= baseDate);
                    }

                }
                if (searchType == "FirstLetter")
                {
                    if (!string.IsNullOrEmpty(firstLetterVal))
                    {
                        prequalificationsGroupedByVendor = prequalificationsGroupedByVendor.Where(rec => rec.Vendor.Name.ToLower().StartsWith(firstLetterVal));

                        if (period == "searchParticular")
                        {
                            var baseDate = DateTime.Now.AddDays(-25);
                            prequalificationsGroupedByVendor = prequalificationsGroupedByVendor.Where(rec => rec.PrequalificationFinish >= baseDate);
                        }
                    }
                }
                if (string.IsNullOrEmpty(SortType) || SortType == "Name")
                {
                    prequalificationsGroupedByVendor = prequalificationsGroupedByVendor.OrderBy(rec => rec.Vendor.Name);
                }
                else if (SortType == "Status")
                {
                    prequalificationsGroupedByVendor = prequalificationsGroupedByVendor.OrderBy(rec => rec.PrequalificationStatusId);
                }
                else if (SortType == "Status Period")
                {
                    prequalificationsGroupedByVendor = prequalificationsGroupedByVendor.OrderBy(rec => rec.PrequalificationStart);
                }
                //var Total = prequalificationsGroupedByVendor.Count();
                List<LocalSortingForNameStatusAndStatusPeriod> Prequalificationlist = new List<LocalSortingForNameStatusAndStatusPeriod>();
                foreach (var latestPrequalification in prequalificationsGroupedByVendor.Skip((request.PageSize * (request.Page - 1))).Take(request.PageSize).ToList())
                {
                    LocalSortingForNameStatusAndStatusPeriod Prequalification = new LocalSortingForNameStatusAndStatusPeriod();


                    Prequalification.VendorName = latestPrequalification.Vendor.Name;
                    var clientId = 0L;
                    if (Role == OrganizationType.Client)
                        clientId = organizationid;

                    Prequalification.Status = _PrequalificationBusiness.GetPQStatusForBanner(latestPrequalification.PrequalificationId, latestPrequalification.PrequalificationStatus.PrequalificationStatusName, clientId, UserId).Value;
                    Prequalification.PrequalificationStatusId = latestPrequalification.PrequalificationStatusId;
                    Prequalification.VendorId = latestPrequalification.VendorId;
                    Prequalification.PrequalificationId = latestPrequalification.PrequalificationId;
                    Prequalification.PrequalificationStart = latestPrequalification.PrequalificationStart;
                    Prequalification.PrequalificationFinish = latestPrequalification.PrequalificationFinish;
                    // ShowInDashBoard = latestPrequalification.PrequalificationStatus.ShowInDashBoard, // Kiran on 6/7/2014
                    Prequalification.ClientId = ClientId;
                    Prequalification.HasPermission = HasAnalyticsPermission;
                    Prequalification.TemplateCode = (latestPrequalification.ClientTemplates != null ? latestPrequalification.ClientTemplates.Templates.TemplateCode : "") ?? "";
                    Prequalificationlist.Add(Prequalification);
                };



                var result = new DataSourceResponse() { Total = prequalificationsGroupedByVendor.Count(), Data = Prequalificationlist };
                return result;

            }
        }

        public DataSourceResponse GetVendorsForAdvancedSearch(DataSourceRequest request, string Subsite, string vendorCity, string Country, string vendorState, string biddingIdList, string statusIdList, string proficiencyIdList, string sortType, long clientId, Guid? clientTemplateid, string Site, string claycoBiddingIdList, string clientContractId, string productCategoryList, string GeographicArea, bool resubOnly, bool HasAnalyticsPermission, string venname, string period, string clientrefid, long varclientId, string role, long orgId, Guid userId)
        {
            using (var entity = new EFDbContext())
            {
                if (vendorState == "select") vendorState = "";

                var strStateCityName = "";
                if ((String.IsNullOrEmpty(venname)) && (String.IsNullOrEmpty(vendorState)) && (String.IsNullOrEmpty(vendorCity)))
                    strStateCityName = "";

                else if ((!String.IsNullOrEmpty(venname)) && (!String.IsNullOrEmpty(vendorState)) && (!String.IsNullOrEmpty(vendorCity)))
                    strStateCityName = " org.State  like @vendorState and  org.City like @vendorCity and org.Name like @venname";

                else if (String.IsNullOrEmpty(venname) && (!String.IsNullOrEmpty(vendorState)) && (!String.IsNullOrEmpty(vendorCity)))
                    strStateCityName = "org.State  like @vendorState and  org.City like @vendorCity";

                else if (String.IsNullOrEmpty(vendorState) && (!String.IsNullOrEmpty(venname)) && (!String.IsNullOrEmpty(vendorCity)))
                    strStateCityName = "org.City like @vendorCity and org.Name like @venname";

                else if (String.IsNullOrEmpty(vendorCity) && (!String.IsNullOrEmpty(vendorState)) && (!String.IsNullOrEmpty(venname)))
                    strStateCityName = " org.State  like @vendorState and org.Name like @venname";

                else if (String.IsNullOrEmpty(venname) && String.IsNullOrEmpty(vendorState) && (!String.IsNullOrEmpty(vendorCity)))
                    strStateCityName = " org.City like @vendorCity";

                else if (String.IsNullOrEmpty(venname) && String.IsNullOrEmpty(vendorCity) && (!String.IsNullOrEmpty(vendorState)))
                    strStateCityName = "org.State  like @vendorState";

                else if (String.IsNullOrEmpty(vendorCity) && String.IsNullOrEmpty(vendorState) && (!String.IsNullOrEmpty(venname)))
                    strStateCityName = "org.Name like @venname";

                if ((!String.IsNullOrEmpty(Country)) && (String.IsNullOrEmpty(strStateCityName)))
                {
                    strStateCityName += " org.Country like @vencountry";
                }
                else if ((!String.IsNullOrEmpty(Country)) && (!String.IsNullOrEmpty(strStateCityName)))
                {
                    strStateCityName += " and org.Country like @vencountry";
                }
                if (!String.IsNullOrEmpty(strStateCityName))
                {
                    strStateCityName = "and(" + strStateCityName + ")";

                }
                var Area = "";
                if (!string.IsNullOrEmpty(GeographicArea) && GeographicArea != "null")
                {
                    var GeoList = GeographicArea.Split(',');
                    var regions = entity.Regions.Where(r => GeoList.Contains(r.RegionName)).Select(r => r.RegionId);
                    var RegionIds = string.Join(",", regions);
                    Area = "and org.GeographicArea in (select RegionName from Regions where RegionId in(" + RegionIds + "))";
                }

                //For StatusIdList
                var strStatusSql = @"case " +
    "when psiteStatuses.cou is null or psiteStatuses.cou = 0 then ps.PrequalificationStatusName " +
    "else dbo.ufnGetPQStatus(p.PrequalificationId, @clientIdForStatus, @userid, ps.PrequalificationStatusName) end in(select PrequalificationStatusName from PrequalificationStatus where PrequalificationStatusId in( select * from dbo.ufn_ParseIntList(@statusIdList))) and";
                if (String.IsNullOrEmpty(statusIdList)) strStatusSql = " ";

                // Kiran on 04/10/2015
                var questionnaireSearch = "";
                if (clientTemplateid != null)
                    questionnaireSearch = "and c.ClientTemplateId=@clientTemplateid";
                // Ends<<<

                // sumanth on 08/12/2015
                var strClientRefidSql = "";
                if (!String.IsNullOrEmpty(clientrefid))
                {
                    long clientid = 0;
                    long vendorid = 0;
                    var clientrefdetails = entity.ClientVendorReferenceIds.FirstOrDefault(rec => rec.ClientVendorReferenceId == clientrefid);
                    if (clientrefdetails != null)
                    {
                        clientid = clientrefdetails.ClientId;
                        vendorid = clientrefdetails.VendorId;
                    }
                    strClientRefidSql = " and p.VendorId in (select VendorId from ClientVendorReferenceIds where ClientId=@varclientId and ClientVendorReferenceId like '%" + @clientrefid + "%')";
                }

                var strClientContractSql = "";
                if (!string.IsNullOrEmpty(clientContractId) && clientContractId.ToLower() != "none")
                {
                    strClientContractSql =
                        " and p.VendorId in (select VendorId from ClientVendorReferenceIds where ClientId=@varclientId  and ClientContractId='" +
                        clientContractId + "')";
                }

                var strMainsql = "select distinct org.Name VendorName," +

                                   "p.PrequalificationStart,p.PrequalificationFinish,p.VendorId ,p.PrequalificationId,p.PrequalificationStatusId,t.TemplateCode,p.ClientId, ps.PrequalificationStatusName as Status from LatestPrequalification AS p " +//Suma on 2/12/2013  //Suma on 4/12/2013,c.TemplateID
                                   "left join (select count(*) cou,PrequalificationId from PrequalificationSites where PQSiteStatus is not null group by PrequalificationId) psiteStatuses on psiteStatuses.PrequalificationId = p.PrequalificationId " +
                                   "INNER JOIN PrequalificationStatus AS ps " +
                                   " ON ps.PrequalificationStatusId = p.PrequalificationStatusId " +
                                   " inner join ClientTemplates c on p.ClientTemplateId = c.ClientTemplateID" +
                                   " inner join Templates t on t.TemplateID=c.TemplateID " +
                                   " INNER JOIN  Organizations AS org ON org.OrganizationID = p.VendorId ";

                if (resubOnly)
                {
                    strMainsql += "INNER JOIN (select PrequalificationId,count(*)  from PrequalificationStatusChangeLog where PrequalificationStatusId = 5 group by PrequalificationId having count(*) > 1) resubbed on resubbed.PrequalificationId = p.PrequalificationId ";
                }

                var strPrequalificationReportingDatasql = "INNER JOIN  PrequalificationReportingData AS prd ON (prd.PrequalificationId = p.PrequalificationId";

                var biddingsql = "prd.ReportingType = 0 and prd.ReportingDataId in (select * from dbo.ufn_ParseIntList(@biddingIdList))";
                var proficiencysql = "prd.ReportingType = 1 and prd.ReportingDataId in (select * from dbo.ufn_ParseIntList(@proficiencyIdList))";
                var claycobiddingsql = "prd.ReportingType = 3 and prd.ReportingDataId in (select * from dbo.ufn_ParseIntList(@claycoBiddingIdList))";
                var productCategoriesSql = "prd.ReportingType = 4 and prd.ReportingDataId in (select * from dbo.ufn_ParseIntList(@productCategoryList))";

                var ProfBidsql = "";

                if (String.IsNullOrEmpty(claycoBiddingIdList) && String.IsNullOrEmpty(biddingIdList) && (String.IsNullOrEmpty(proficiencyIdList)) && String.IsNullOrEmpty(productCategoryList))
                {
                    //Suma on 10/15/2013 >>>
                    strPrequalificationReportingDatasql = " ";
                    ProfBidsql = " ";
                    //<<<Ends
                }
                else if (!String.IsNullOrEmpty(biddingIdList) && !String.IsNullOrEmpty(proficiencyIdList) && !String.IsNullOrEmpty(claycoBiddingIdList))
                {
                    ProfBidsql = "and(" + biddingsql + "or(" + proficiencysql + ") or (" + claycobiddingsql + ")))";
                }
                else if (!String.IsNullOrEmpty(biddingIdList) && !String.IsNullOrEmpty(proficiencyIdList) && String.IsNullOrEmpty(claycoBiddingIdList))
                {
                    ProfBidsql = "and(" + biddingsql + "or(" + proficiencysql + ")))";
                }
                else if (!String.IsNullOrEmpty(biddingIdList) && String.IsNullOrEmpty(proficiencyIdList) && !String.IsNullOrEmpty(claycoBiddingIdList))
                {
                    ProfBidsql = "and(" + biddingsql + "or(" + claycobiddingsql + ")))";
                }
                else if (String.IsNullOrEmpty(biddingIdList) && !String.IsNullOrEmpty(proficiencyIdList) && String.IsNullOrEmpty(claycoBiddingIdList))
                {
                    ProfBidsql = "and(" + proficiencysql + "))";
                }
                else if (!String.IsNullOrEmpty(biddingIdList) && String.IsNullOrEmpty(proficiencyIdList) && String.IsNullOrEmpty(claycoBiddingIdList))
                {
                    ProfBidsql = "and(" + biddingsql + "))";
                }
                else if (String.IsNullOrEmpty(biddingIdList) && String.IsNullOrEmpty(proficiencyIdList) && !String.IsNullOrEmpty(claycoBiddingIdList))
                {
                    ProfBidsql = " and (" + claycobiddingsql + "))";
                }

                if (!String.IsNullOrEmpty(productCategoryList))
                    ProfBidsql = " and (" + productCategoriesSql + "))";

                //For Subsite
                var strsubSitesql = "Left JOIN PrequalificationSites AS psite ON (p.PrequalificationId = psite.PrequalificationId ";

                strsubSitesql += ")";
                var siteCondition = "";
                var subsite = Subsite.Split(',');
                if (!subsite.Contains("Any") && !subsite.Contains(""))
                {
                    siteCondition += "and psite.ClientBusinessUnitSiteId in( " + Subsite + ") ";
                }
                var site = Site.Split(',');
                if (!site.Contains("Any") && !site.Contains(""))
                {
                    siteCondition += "and psite.ClientBusinessUnitId in(" + Site + ") ";
                }
                var clientCondition = "(p.ClientId = @clientId or p.PrequalificationId in(select PQId from PrequalificationClient where ','+ClientIds+',' like @p0))";

                if (role.Equals(OrganizationType.SuperClient))
                {
                    var IsSuperClient = entity.SubClients.FirstOrDefault(r => r.SuperClientId == clientId);
                    if (IsSuperClient != null)
                    {
                        clientCondition = "(p.clientid=@clientId)";
                    }
                    else { clientCondition = "(p.PrequalificationId in(select PQId from PrequalificationClient where ',' + ClientIds + ',' like @p0))"; }
                }
                var sortName = sortType;

                if (String.IsNullOrEmpty(sortType) || (sortType == "Name")) sortName = "org.Name";
                if (sortType == "Status") sortName = "Status";
                if (sortType == "Status Period") sortName = "p.PrequalificationStart";

                var VendorSearchSQL = "";

                var userSites = "";
                var userSitesRec = entity.SystemUsersBUSites.FirstOrDefault(r => r.SystemUserId == userId);
                if (userSitesRec != null)
                    userSites = userSitesRec.BUSiteId;
                if (period == "searchAll")
                {
                    VendorSearchSQL = " " + strMainsql + " " +
                                      " " + strStateCityName + " " +
                                      " " + strClientRefidSql + " " +
                                      " " + strClientContractSql + " " +
                                      " " + strPrequalificationReportingDatasql + " " +
                                      " " + ProfBidsql + " " +
                                      " " + strsubSitesql + " " +


                                       " where( t.IsHide=0 and " + strStatusSql + clientCondition + " and ps.ShowInDashBoard= 1 " + questionnaireSearch +

                                      (string.IsNullOrEmpty(userSites) ? "" : " and psite.ClientBusinessUnitSiteId in (" + userSites + " )")
                                      + siteCondition + Area + ") order by " + sortName + " ";

                } //End of SearchAll

                else if (period == "searchParticular")
                {
                    var baseDate = (DateTime.Now.AddDays(-25)).ToString("MM/dd/yyyy");
                    var currentDate = DateTime.Now.ToString("MM/dd/yyyy");
                    VendorSearchSQL = " " + strMainsql + " " +
                                      " " + strStateCityName + " " +
                                      " " + strClientRefidSql + " " +
                                      " " + strClientContractSql + " " +
                                      " " + strPrequalificationReportingDatasql + " " +
                                      " " + ProfBidsql + " " +
                                      " " + strsubSitesql + " " +

                                       " where( t.IsHide=0 and " + strStatusSql + clientCondition + Area +
                                      " and(p.PrequalificationStart >= " + baseDate + "and ps.ShowInDashBoard = 1 ) " +
                                   (string.IsNullOrEmpty(userSites) ? "" : " and psite.ClientBusinessUnitSiteId in (" + userSites + ")") + questionnaireSearch + siteCondition + " )order by " + sortName + " ";

                }
                long clientIdForStatus = 0;
                if (role.Equals(OrganizationType.Client))
                {
                    clientIdForStatus = orgId;
                }
                var resultList = entity.Database.SqlQuery<LocalSortingForNameStatusAndStatusPeriod>(VendorSearchSQL + "  OFFSET " + ((request.Page - 1) * request.PageSize) + " ROWS FETCH NEXT " + request.PageSize + " ROWS ONLY", new SqlParameter("@Subsite", Subsite.ToStringNullSafe()),
                    new SqlParameter("@Site", Site.ToStringNullSafe()),
                    new SqlParameter("@vendorCity", "%" + vendorCity.ToStringNullSafe().Trim() + "%"),
                    new SqlParameter("@vendorState", "%" + vendorState.ToStringNullSafe() + "%"),
                    new SqlParameter("@biddingIdList", biddingIdList.ToStringNullSafe()),
                    new SqlParameter("@statusIdList", statusIdList.ToStringNullSafe()),
                    new SqlParameter("@proficiencyIdList", proficiencyIdList.ToStringNullSafe()),
                    new SqlParameter("@venname", "%" + venname.ToStringNullSafe() + "%"),
                      new SqlParameter("@vencountry", "%" + Country.ToStringNullSafe() + "%"),
                    new SqlParameter("@p0", "%," + clientId.ToLongNullSafe() + ",%"),
                    new SqlParameter("@clientId", clientId.ToStringNullSafe()),
                    new SqlParameter("@clientTemplateid", clientTemplateid.ToStringNullSafe()),
                    new SqlParameter("@clientrefid", clientrefid.ToStringNullSafe()),
                    new SqlParameter("@claycoBiddingIdList", claycoBiddingIdList.ToStringNullSafe()),
                    new SqlParameter("@varclientId", varclientId.ToStringNullSafe()),
                    new SqlParameter("@clientContractId", clientContractId.ToStringNullSafe()),
                    new SqlParameter("@clientIdForStatus", clientIdForStatus.ToStringNullSafe()), new SqlParameter("@userid", userId.ToStringNullSafe()),
                    new SqlParameter("@productCategoryList", productCategoryList.ToStringNullSafe())
                    );
                var resultListCount = entity.Database.SqlQuery<int>(("select count(*) from(" + VendorSearchSQL + ") as p").Replace("order by " + sortName, ""), new SqlParameter("@Subsite", Subsite.ToStringNullSafe()),
                    new SqlParameter("@Site", Site.ToStringNullSafe()),
                    new SqlParameter("@vendorCity", "%" + vendorCity.ToStringNullSafe().Trim() + "%"),
                    new SqlParameter("@vendorState", "%" + vendorState.ToStringNullSafe() + "%"),
                    new SqlParameter("@biddingIdList", biddingIdList.ToStringNullSafe()),
                    new SqlParameter("@statusIdList", statusIdList.ToStringNullSafe()),
                    new SqlParameter("@proficiencyIdList", proficiencyIdList.ToStringNullSafe()),
                    new SqlParameter("@venname", "%" + venname.ToStringNullSafe() + "%"),
                    new SqlParameter("@vencountry", "%" + Country.ToStringNullSafe() + "%"),
                    new SqlParameter("@p0", "%," + clientId.ToLongNullSafe() + ",%"),
                    new SqlParameter("@clientId", clientId.ToStringNullSafe()),
                    new SqlParameter("@clientTemplateid", clientTemplateid.ToStringNullSafe()),
                    new SqlParameter("@clientrefid", clientrefid.ToStringNullSafe()),
                    new SqlParameter("@claycoBiddingIdList", claycoBiddingIdList.ToStringNullSafe()),
                    new SqlParameter("@varclientId", varclientId.ToStringNullSafe()),
                    new SqlParameter("@clientContractId", clientContractId.ToStringNullSafe()),
                    new SqlParameter("@clientIdForStatus", clientIdForStatus.ToStringNullSafe()), new SqlParameter("@userid", userId.ToStringNullSafe()),
                    new SqlParameter("@productCategoryList", productCategoryList.ToStringNullSafe())
                    );
                var Data = resultList.ToList();

                if (String.IsNullOrEmpty(statusIdList))
                    Data.ForEach(r => { r.TemplateCode = r.TemplateCode ?? ""; r.Status = _PrequalificationBusiness.GetPQStatusForBanner(r.PrequalificationId, r.Status, r.ClientId, userId).Value; r.HasPermission = HasAnalyticsPermission; });
                var result = new DataSourceResponse() { Total = resultListCount.First(), Data = Data };
                return result;

                //return vendorState;
            }
        }

        public bool PostVendorInvite(LocalinviteVendor form, Guid userId)
        {
            using (EFDbContext entity = new EFDbContext())
            {
                var isEriSubClient = entity.SubClients.Any(r => r.ClientId == form.client);
                VendorInviteDetails inviteVen = new VendorInviteDetails();
                inviteVen.InvitationId = Guid.NewGuid();
                inviteVen.Address1 = form.Address1;
                inviteVen.Address2 = form.Address2;
                inviteVen.Address3 = form.Address3;
                inviteVen.City = form.City;
                inviteVen.ClientId = form.client;
                inviteVen.Country = form.Country;
                inviteVen.State = form.State;
                inviteVen.VendorEmail = form.EmailAddress;
                inviteVen.VendorFirstName = form.FullName;
                inviteVen.VendorLastName = form.LastName;
                inviteVen.Zip = form.Zip;
                if (isEriSubClient)
                    inviteVen.InvitationCode = EncryptionDecryption.GetUniqueKey(10);

                var clientUniqueKey = entity.Organizations.FirstOrDefault(rec => rec.OrganizationID == form.client)?.SingleClientCode;
                inviteVen.ClientCode = clientUniqueKey;
                inviteVen.InvitationDate = DateTime.Now;
                inviteVen.VendorCompanyName = form.VendorCompanyName;
                inviteVen.InvitationSentFrom = 0;
                inviteVen.InvitationSentByUser = userId;
                inviteVen.RiskLevel = form.RiskLevel;

                inviteVen.LanguageId = form.Language;
                if (form.usersSendBCC == null) { form.usersSendBCC = new List<String>(); }
                if (form.usersSendCC == null) { form.usersSendCC = new List<String>(); }

                if (form.isSendBCC)
                {
                    form.usersSendBCC.Add(entity.SystemUsers.Find(userId).Email);
                }

                if (form.isSendMeCC)
                {
                    form.usersSendCC.Add(entity.SystemUsers.Find(userId).Email);
                }
                String sendBcc = form.usersSendBCC == null ? "" : String.Join(",", form.usersSendBCC);
                String sendCc = form.usersSendCC == null ? "" : String.Join(",", form.usersSendCC);

                inviteVen.SendBcc = sendBcc;
                inviteVen.SendCc = sendCc;

                if (form.Location != null)
                {
                    inviteVen.VendorInviteLocations = new List<VendorInviteLocations>();
                    form.Location.ForEach(rec =>
                    {
                        var loc = new VendorInviteLocations();
                        loc.ClientBusinessUnitSiteID = rec;
                        inviteVen.VendorInviteLocations.Add(loc);
                    });
                }

                inviteVen.Comments = form.Comments;
                entity.VendorInviteDetails.Add(inviteVen);
                entity.SaveChanges();
                var clientName = entity.Organizations.Find(form.client)?.Name;
                string body = "";
                string subject = "Invitation from " + clientName;
                string inviteVenUrl = ConfigurationManager.AppSettings["InviteVendorInvitationURL"];
                var hasInvitationTemplateConfig = entity.InviteTemplateConfiguration.FirstOrDefault(r => r.ClientId == form.client && r.InvitationType == LocalConstants.New_Invitation);
                InviteTemplateConfiguration hasInvitationTemplateForAll = entity.InviteTemplateConfiguration.FirstOrDefault(r => r.ClientId == form.client && r.InvitationType == LocalConstants.All);
                if (hasInvitationTemplateConfig != null)
                {
                    body = hasInvitationTemplateConfig.EmailTemplates.EmailBody;
                    subject = hasInvitationTemplateConfig.EmailTemplates.EmailSubject;
                }

                else if (hasInvitationTemplateForAll != null)
                {
                    body = hasInvitationTemplateForAll.EmailTemplates.EmailBody;
                    subject = hasInvitationTemplateForAll.EmailTemplates.EmailSubject;
                }
                else
                {
                    var fileName = "~/HtmlTemplates/InviteVendorEmailFormat";
                    if (isEriSubClient)
                        fileName = "~/HtmlTemplates/ERIInviteVendorEmailFormat";
                    var file = Path.Combine(System.Web.HttpContext.Current.Server.MapPath(fileName));

                    var selectedLanguageCode = entity.Language.Find(form.Language)?.LanguageCode;
                    file = file + "_" + selectedLanguageCode + ".htm";
                    if (!System.IO.File.Exists(file))
                    {
                        file = System.Web.HttpContext.Current.Server.MapPath(fileName + "_EN.htm");
                    }
                    body = System.IO.File.ReadAllText(file);
                }
                string URL = "<a href='" + inviteVenUrl + inviteVen.InvitationId + "'>" + inviteVenUrl + inviteVen.InvitationId + "</a>";


                var location = String.Empty;
                location = Location(inviteVen.VendorInviteLocations);
                if (String.IsNullOrWhiteSpace(location))
                {
                    location = "N/A";
                }


                if (String.IsNullOrEmpty(inviteVen.RiskLevel))
                {
                    inviteVen.RiskLevel = "N/A";
                }

                var userName = (entity.SystemUsers.Find(userId)?.UserName);

                var contactname = entity.Contact.FirstOrDefault(rec => rec.UserId == userId);

                var a = RiskFee(inviteVen.ClientId, inviteVen.RiskLevel, false);
                Organizations superClientOrg = null;
                if (isEriSubClient)
                {
                    superClientOrg = entity.SubClients.FirstOrDefault(r => r.ClientId == inviteVen.ClientId)?.SuperClientOrganization;
                    if (superClientOrg == null)
                        superClientOrg = entity.Organizations.Find(inviteVen.ClientId);
                }
                subject = subject.Replace("[ClientName]", clientName);
                subject = subject.Replace("[VendorName]", inviteVen.VendorCompanyName);
                subject = subject.Replace("[Date]", DateTime.Now.ToString());
                subject = subject.Replace("[ClientName]", clientName);
                subject = subject.Replace("[VendorName]", inviteVen.VendorCompanyName);
                subject = subject.Replace("[Client_Name]", clientName);
                subject = subject.Replace("[Vendor_CompanyName]", inviteVen.VendorCompanyName);
                subject = subject.Replace("[Vendor_FirstName]", inviteVen.VendorFirstName);
                subject = subject.Replace("[Vendor_LastName]", inviteVen.VendorLastName);
                subject = subject.Replace("[Vendor_Email]", inviteVen.VendorEmail);
                subject = subject.Replace("[Vendor_FirstLastNames]", inviteVen.VendorLastName + ", " + inviteVen.VendorFirstName);
                subject = subject.Replace("[Vendor_EmailAddress]", inviteVen.VendorEmail);
                subject = subject.Replace("[Client_Comments]", inviteVen.Comments);
                subject = subject.Replace("[Vendor_RiskLevel]", inviteVen.RiskLevel);
                subject = subject.Replace("[Location_Name]", location);
                subject = subject.Replace("[Client_UserName]", (contactname.FirstName + ' ' + contactname.LastName));
                subject = subject.Replace("[Client_EmailAddress]", userName);
                subject = subject.Replace("[Risk_LevelFee]", a);
                subject = subject.Replace("[URL]", URL);
                subject = subject.Replace("[Client_Code]", inviteVen.ClientCode);
                subject = subject.Replace("[Invitation_Code]", inviteVen.InvitationCode);
                body = body.Replace("[Date]", DateTime.Now.ToString());
                body = body.Replace("[ClientName]", clientName);
                body = body.Replace("[VendorName]", inviteVen.VendorCompanyName);
                body = body.Replace("[Client_Name]", clientName);
                body = body.Replace("[Vendor_CompanyName]", inviteVen.VendorCompanyName);
                body = body.Replace("[Vendor_FirstName]", inviteVen.VendorFirstName);
                body = body.Replace("[Vendor_LastName]", inviteVen.VendorLastName);
                body = body.Replace("[Vendor_Email]", inviteVen.VendorEmail);
                body = body.Replace("[Vendor_FirstLastNames]", inviteVen.VendorLastName + ", " + inviteVen.VendorFirstName);
                body = body.Replace("[Vendor_EmailAddress]", inviteVen.VendorEmail);
                body = body.Replace("[Client_Comments]", inviteVen.Comments);
                body = body.Replace("[Vendor_RiskLevel]", inviteVen.RiskLevel);
                body = body.Replace("[Location_Name]", location);
                body = body.Replace("[Client_UserName]", (contactname.FirstName + ' ' + contactname.LastName));
                body = body.Replace("[Client_EmailAddress]", userName);
                body = body.Replace("[Risk_LevelFee]", a);
                body = body.Replace("[URL]", URL);
                body = body.Replace("[Client_Code]", inviteVen.ClientCode);
                body = body.Replace("[Invitation_Code]", inviteVen.InvitationCode);
                if (superClientOrg != null)
                {
                    body = body.Replace("[SuperClient_Name]", superClientOrg.Name);
                    body = body.Replace("[SuperClient]", superClientOrg.Name);
                    subject = subject.Replace("[SuperClient]", superClientOrg.Name);
                }

                //if (form.usersSendBCC == null) { form.usersSendBCC = new List<String>(); }
                //if (form.usersSendCC == null) { form.usersSendCC = new List<String>(); }

                //if (form.isSendBCC)
                //{
                //    form.usersSendBCC.Add(entity.SystemUsers.Find(userId).Email);
                //}
                form.usersSendBCC.Add("info@firstverify.com");

                //if (form.isSendMeCC)
                //{
                //    form.usersSendCC.Add(entity.SystemUsers.Find(userId).Email);
                //}

                String bcc = form.usersSendBCC == null ? "" : String.Join(",", form.usersSendBCC);
                String cc = form.usersSendCC == null ? "" : String.Join(",", form.usersSendCC);
                var HasClientUsers = _SysBusiness.CheckInviteClients(inviteVen.ClientId);
                if (HasClientUsers)
                    cc += string.IsNullOrEmpty(cc) ? _SysBusiness.CheckClientUsersforInvitation(inviteVen.ClientId).UserEmails : "," + _SysBusiness.CheckClientUsersforInvitation(inviteVen.ClientId).UserEmails;
                _SysBusiness.UsersInInvitation(inviteVen.InvitationId, bcc, cc);
                string strMessage = "";
                strMessage = Common.SendMail.sendMail(form.EmailAddress, body, subject, bcc, cc);
                bool successMessage;

                if (strMessage == "Success")
                    successMessage = true;
                else
                {
                    successMessage = false;
                    entity.VendorInviteDetails.Remove(inviteVen);
                    entity.SaveChanges();
                }

                return successMessage;
            }
        }

        public string RiskFee(long client, string riskLevel, bool isERI)
        {

            List<decimal?> a = new List<decimal?>();
            var groupingPriceType = 0;
            if (isERI)
                groupingPriceType = 1;
            using (EFDbContext entityDB = new EFDbContext())
            {
                if (riskLevel != null)
                {
                    var fee = from t in entityDB.Templates
                              join ct in entityDB.ClientTemplates on t.TemplateID equals ct.TemplateID
                              join ctgp in entityDB.ClientTemplatesBUGroupPrice on ct.ClientTemplateID equals ctgp.ClientTemplateID
                              where
                              ctgp.GroupingFor == 0 && ct.ClientID == client && ctgp.GroupPriceType == groupingPriceType &&
                              t.RiskLevel == riskLevel
                              select ctgp.GroupPrice;
                    a = fee.ToList();
                }
            }
            return a.Count == 0 ? "" : a.FirstOrDefault().ToString();
        }

        public string Location(List<VendorInviteLocations> InviteVen)
        {
            string location = string.Empty;
            using (EFDbContext entityDB = new EFDbContext())
            {
                if (InviteVen != null)
                {
                    InviteVen.ForEach(rec =>
                    {
                        var loc = entityDB.ClientBusinessUnitSites.Find(rec.ClientBusinessUnitSiteID);

                        if (loc != null)
                        {
                            if (!string.IsNullOrWhiteSpace(location))
                            {
                                location += ", ";
                            }
                            location = location + loc.SiteName + " " + loc.BusinessUnitLocation;
                        }
                    });
                }
            }
            return location;
        }
        public VendorInviteDetails GetInviteInfo(Guid invitationId)
        {
            using (var context = new EFDbContext())
            {
                var Invitation = context.VendorInviteDetails.Find(invitationId);
                return Invitation;
            }
        }
        public void UpdateInviteComments(LocalinviteVendor inviteVendor, Guid userId)
        {
            using (var context = new EFDbContext())
            {
                var Invitation = context.VendorInviteDetails.Find(inviteVendor.InvitationId);

                Invitationlogs log = new Invitationlogs();
                log.OldComment = Invitation.Comments;
                log.NewComment = inviteVendor.Comments;
                log.Modifieddate = DateTime.Now;
                log.Createddate = Invitation.InvitationDate;
                log.ChangedBy = userId;
                log.InvitationId = Invitation.InvitationId;
                context.Invitationlogs.Add(log);
                Invitation.Comments = inviteVendor.Comments;

                context.Entry(Invitation).State = EntityState.Modified;
                context.SaveChanges();

            }
        }
        public string SendNewInvitation(Guid invitationId, Guid userid)
        {
            var entityDB = new EFDbContext();
            {
                var InviteVen = GetInviteInfo(invitationId);
                var LoginUser = entityDB.SystemUsers.Find(userid);
                var IsERISubClient = entityDB.SubClients.Any(r => r.ClientId == InviteVen.ClientId);

                var clientName = entityDB.Organizations.Find(InviteVen.ClientId).Name;
                string body = "";
                string subject = "Invitation from " + clientName;
                string inviteVenUrl = ConfigurationManager.AppSettings["InviteVendorInvitationURL"];
                var HasInvitationTemplateConfig = entityDB.InviteTemplateConfiguration.FirstOrDefault(r => r.ClientId == InviteVen.ClientId && r.InvitationType == LocalConstants.New_Invitation);
                InviteTemplateConfiguration HasInvationTemplateforall = entityDB.InviteTemplateConfiguration.FirstOrDefault(r => r.ClientId == InviteVen.ClientId && r.InvitationType == LocalConstants.All);
                if (HasInvitationTemplateConfig != null)
                {
                    body = HasInvitationTemplateConfig.EmailTemplates.EmailBody;
                    subject = HasInvitationTemplateConfig.EmailTemplates.EmailSubject;
                }

                else if (HasInvationTemplateforall != null)
                {
                    body = HasInvationTemplateforall.EmailTemplates.EmailBody;
                    subject = HasInvationTemplateforall.EmailTemplates.EmailSubject;
                }
                else
                {
                    var fileName = "~/HtmlTemplates/InviteVendorEmailFormat";
                    if (IsERISubClient)
                        fileName = "~/HtmlTemplates/ERIInviteVendorEmailFormat";
                    // Kiran on 12/23/2014
                    //string strClientCode = "</p><P style='float: left;width:100%;'><b>When you log in, click Start New and select Client_Name from the List of Potential Clients. <p style='float: left;width:100%;'> If Client_Name does not appear on the list, enter the following Client Code at the top of the page: " + "<span style='color:#A61417;'>" + InviteVen.ClientCode + "</b></P>";
                    var file = Path.Combine(System.Web.HttpContext.Current.Server.MapPath(fileName));
                    //var file = "~/HtmlTemplates/InviteVendorEmailFormat";
                    var LanguageId = InviteVen.LanguageId;
                    if (String.IsNullOrEmpty(LanguageId.ToString()) || LanguageId == 0) LanguageId = 1;
                    var selectedLanguageCode = _orgBusiness.GetLanguageCode(LanguageId.Value);
                    file = file + "_" + selectedLanguageCode + ".htm";
                    if (!System.IO.File.Exists(file))
                    {
                        file = System.Web.HttpContext.Current.Server.MapPath(fileName + "_EN.htm");
                    }
                    body = System.IO.File.ReadAllText(file);
                }
                /*@Resources.Resources.InviteVendor_MessagePart1 +  + strClientCode + @Resources.Resources.InviteVendor_MessagePart2;*/
                // Ends<<<
                string URL = "<a href='" + inviteVenUrl + InviteVen.InvitationId + "'>" + inviteVenUrl + InviteVen.InvitationId + "</a>";


                var location = String.Empty;
                var VendorInviteLocations = entityDB.VendorInviteLocations.Where(r => r.InvitationId == InviteVen.InvitationId).ToList();
                location = Location(VendorInviteLocations);
                if (String.IsNullOrWhiteSpace(location))
                {
                    location = "N/A";
                }


                if (String.IsNullOrEmpty(InviteVen.RiskLevel))
                {
                    InviteVen.RiskLevel = "N/A";
                }

                var userName = LoginUser.UserName;

                // var userid = (Guid)Session["UserId"];
                var contactname = entityDB.Contact.FirstOrDefault(rec => rec.UserId == userid);

                var a = RiskFee(InviteVen.ClientId, InviteVen.RiskLevel, false);
                OrganizationData superClientOrg = null;
                if (IsERISubClient)
                {
                    superClientOrg = _orgBusiness.GetSuperClient(InviteVen.ClientId);
                }
                subject = subject.Replace("[ClientName]", clientName);
                subject = subject.Replace("[VendorName]", InviteVen.VendorCompanyName);
                subject = subject.Replace("[Date]", DateTime.Now.ToString());
                subject = subject.Replace("[ClientName]", clientName);
                subject = subject.Replace("[VendorName]", InviteVen.VendorCompanyName);
                subject = subject.Replace("[Client_Name]", clientName);
                subject = subject.Replace("[Vendor_CompanyName]", InviteVen.VendorCompanyName);
                subject = subject.Replace("[Vendor_FirstName]", InviteVen.VendorFirstName);
                subject = subject.Replace("[Vendor_LastName]", InviteVen.VendorLastName);
                subject = subject.Replace("[Vendor_Email]", InviteVen.VendorEmail);
                subject = subject.Replace("[Vendor_FirstLastNames]", InviteVen.VendorLastName + ", " + InviteVen.VendorFirstName);
                subject = subject.Replace("[Vendor_EmailAddress]", InviteVen.VendorEmail);
                subject = subject.Replace("[Client_Comments]", InviteVen.Comments);
                subject = subject.Replace("[Vendor_RiskLevel]", InviteVen.RiskLevel);
                subject = subject.Replace("[Location_Name]", location);
                subject = subject.Replace("[Client_UserName]", (contactname.FirstName + ' ' + contactname.LastName));
                subject = subject.Replace("[Client_EmailAddress]", userName);
                subject = subject.Replace("[Risk_LevelFee]", a);
                subject = subject.Replace("[URL]", URL);
                subject = subject.Replace("[Client_Code]", InviteVen.ClientCode);
                subject = subject.Replace("[Invitation_Code]", InviteVen.InvitationCode);
                body = body.Replace("[Date]", DateTime.Now.ToString());
                body = body.Replace("[ClientName]", clientName);
                body = body.Replace("[VendorName]", InviteVen.VendorCompanyName);
                body = body.Replace("[Client_Name]", clientName);
                body = body.Replace("[Vendor_CompanyName]", InviteVen.VendorCompanyName);
                body = body.Replace("[Vendor_FirstName]", InviteVen.VendorFirstName);
                body = body.Replace("[Vendor_LastName]", InviteVen.VendorLastName);
                body = body.Replace("[Vendor_Email]", InviteVen.VendorEmail);
                body = body.Replace("[Vendor_FirstLastNames]", InviteVen.VendorLastName + ", " + InviteVen.VendorFirstName);
                body = body.Replace("[Vendor_EmailAddress]", InviteVen.VendorEmail);
                body = body.Replace("[Client_Comments]", InviteVen.Comments);
                body = body.Replace("[Vendor_RiskLevel]", InviteVen.RiskLevel);
                body = body.Replace("[Location_Name]", location);
                body = body.Replace("[Client_UserName]", (contactname.FirstName + ' ' + contactname.LastName));
                body = body.Replace("[Client_EmailAddress]", userName);
                body = body.Replace("[Risk_LevelFee]", a);
                body = body.Replace("[URL]", URL);
                body = body.Replace("[Client_Code]", InviteVen.ClientCode);
                body = body.Replace("[Invitation_Code]", InviteVen.InvitationCode);
                if (superClientOrg != null)
                {
                    body = body.Replace("[SuperClient_Name]", superClientOrg.Name);
                    body = body.Replace("[SuperClient]", superClientOrg.Name);
                    subject = subject.Replace("[SuperClient]", superClientOrg.Name);
                }
                // Ends<<<
                var Bcc = new List<String>();
                var CC = new List<String>();
                if (!string.IsNullOrEmpty(InviteVen.SendBcc))
                {
                    //  Bcc.Add(LoginUser.Email);
                    Bcc.Add(InviteVen.SendBcc);
                }
                Bcc.Add("info@firstverify.com");
                if (!string.IsNullOrEmpty(InviteVen.SendCc))
                {
                    // CC.Add(LoginUser.Email);
                    CC.Add(InviteVen.SendCc);
                }

                String bcc = Bcc == null ? "" : String.Join(",", Bcc);
                String cc = CC == null ? "" : String.Join(",", CC);
                var HasClientUsers = _SysBusiness.CheckInviteClients(InviteVen.ClientId);
                if (HasClientUsers)
                    cc += string.IsNullOrEmpty(cc) ? _SysBusiness.CheckClientUsersforInvitation(InviteVen.ClientId).UserEmails : "," + _SysBusiness.CheckClientUsersforInvitation(InviteVen.ClientId).UserEmails;


                //  string strMessage = ""; // Kiran on 4/10/2014
                //  strMessage = SendMail.sendMail(InviteVen.VendorEmail, body, subject, bcc, cc);
                string strMessage = "";
                strMessage = Common.SendMail.sendMail(InviteVen.VendorEmail, body, subject, bcc, cc);
                //Mail mail = new Mail();
                //mail.body = body;
                //mail.toAddress = InviteVen.VendorEmail;
                //mail.subject = subject;
                //mail.bcc = bcc;
                //mail.cc = cc;
                return strMessage;


            }
        }
        //public string Location(List<VendorInviteLocations> InviteVen)
        //{
        //    string location = string.Empty;
        //    using (var entityDB = new EFDbContext())
        //    {
        //        if (InviteVen != null)
        //        {
        //            InviteVen.ForEach(rec =>
        //            {
        //                var loc = entityDB.ClientBusinessUnitSites.Find(rec.ClientBusinessUnitSiteID);

        //                if (loc != null)
        //                {
        //                    if (!string.IsNullOrWhiteSpace(location))
        //                    {
        //                        location += ", ";
        //                    }
        //                    location = location + loc.SiteName + " " + loc.BusinessUnitLocation;
        //                }
        //            });
        //        }
        //    }
        //    return location;
        //}
        //public string RiskFee(long client, string riskLevel, bool isERI)
        //{

        //    List<decimal?> a = new List<decimal?>();
        //    var groupingPriceType = 0;
        //    if (isERI)
        //        groupingPriceType = 1;
        //    using (var entityDB = new EFDbContext())
        //    {
        //        if (riskLevel != null)
        //        {
        //            var fee = from t in entityDB.Templates
        //                      join ct in entityDB.ClientTemplates on t.TemplateID equals ct.TemplateID
        //                      join ctgp in entityDB.ClientTemplatesBUGroupPrice on ct.ClientTemplateID equals ctgp.ClientTemplateID
        //                      where
        //                      ctgp.GroupingFor == 0 && ct.ClientID == client && ctgp.GroupPriceType == groupingPriceType &&
        //                      t.RiskLevel == riskLevel
        //                      select ctgp.GroupPrice;
        //            a = fee.ToList();
        //        }
        //    }
        //    return a.Count == 0 ? "" : a.FirstOrDefault().ToString();
        //}
        public string SendOutsideInvitation(Guid invitationId, Guid userid)
        {
            using (var entityDB = new EFDbContext())
            {
                var Invitation = GetInviteInfo(invitationId);
                var Invitationtype = Invitation.InvitationType;
                var fileName = "";
                bool isERI = false;
                if (Invitation.InvitationCode != null && Invitation.InvitationCode.Length == 10)
                    isERI = true;
                if (Invitationtype == 2) //outdide Invitation
                {


                    if (isERI)
                        fileName = "~/HtmlTemplates/OutsideERIVendorTemplate";
                    else
                        fileName = "~/HtmlTemplates/OutsideVendorTemplate";
                }
                else
                    fileName = "~/HtmlTemplates/ExpiredVendorTemplate"; //expired
                var clientInfo = _orgBusiness.GetOrganization(Invitation.ClientId);
                var ven = _orgBusiness.GetOrganization(Invitation.VendorId.Value);
                var languageCode = _orgBusiness.GetLanguageCode(ven.Language);

                var file = Path.Combine(System.Web.HttpContext.Current.Server.MapPath(fileName));
                file = file + "_" + languageCode + ".html";
                if (!System.IO.File.Exists(file))
                {

                    file = file + "_EN.html";
                }

                string template = "";

                string subject = "Invitation from FIRST, VERIFY";

                var HasInvitationTemplateConfig = entityDB.InviteTemplateConfiguration.FirstOrDefault(r => r.ClientId == Invitation.ClientId
             && r.InvitationType == Invitationtype);
                var HasInvationTemplateforall = entityDB.InviteTemplateConfiguration.FirstOrDefault(r => r.ClientId == Invitation.ClientId && r.InvitationType == LocalConstants.All);

                if (HasInvitationTemplateConfig != null)
                {
                    template = HasInvitationTemplateConfig.EmailTemplates.EmailBody;
                    subject = HasInvitationTemplateConfig.EmailTemplates.EmailSubject;

                }
                else if (HasInvationTemplateforall != null)
                {
                    template = HasInvationTemplateforall.EmailTemplates.EmailBody;
                    subject = HasInvationTemplateforall.EmailTemplates.EmailSubject;
                }

                else
                {
                    template = System.IO.File.ReadAllText(file);
                }

                var clientUser = _SysBusiness.GetUserInfo(userid);
                var venInviteLocations = entityDB.VendorInviteLocations.Where(r => r.InvitationId == invitationId).ToList();

                var a = RiskFee(clientInfo.OrganizationID, Invitation.RiskLevel, isERI);
                var loc = Location(venInviteLocations);
                var clientName = clientInfo.Name;
                OrganizationData superClientOrg = null;
                if (isERI)
                {
                    //clientName = _organization.GetOrganization(LocalConstants.ERI).Name;
                    clientName = clientInfo.Name;
                    superClientOrg = _orgBusiness.GetSuperClient(Invitation.ClientId);
                }
                loc = string.IsNullOrEmpty(loc) ? "N/A" : loc;
                template = template.Replace("[ClientName]", clientName);
                template = template.Replace("[VendorName]", ven.Name);
                template = template.Replace("[Date]", DateTime.Now.ToString());
                template = template.Replace("[Vendor_CompanyName]", ven.Name);
                template = template.Replace("[Client_UserName]", clientUser.FullName);
                template = template.Replace("[Client_EmailAddress]", clientUser.Email);
                template = template.Replace("[Client_Name]", clientName);
                template = template.Replace("[Location_Name]", loc);
                template = template.Replace("[Vendor_RiskLevel]", Invitation.RiskLevel);
                template = template.Replace("[Client_Comments]", Invitation.Comments);
                template = template.Replace("[Risk_LevelFee]", a);
                subject = subject.Replace("[ClientName]", clientName);
                subject = subject.Replace("[VendorName]", ven.Name);
                subject = subject.Replace("[Date]", DateTime.Now.ToString());
                subject = subject.Replace("[Vendor_CompanyName]", ven.Name);
                subject = subject.Replace("[Client_UserName]", clientUser.FullName);
                subject = subject.Replace("[Client_EmailAddress]", clientUser.Email);
                subject = subject.Replace("[Client_Name]", clientName);
                subject = subject.Replace("[Location_Name]", loc);
                subject = subject.Replace("[Vendor_RiskLevel]", Invitation.RiskLevel);
                subject = subject.Replace("[Client_Comments]", Invitation.Comments);
                subject = subject.Replace("[Risk_LevelFee]", a);
                subject = subject.Replace("[ClientName]", clientName);
                subject = subject.Replace("[VendorName]", ven.Name);
                if (superClientOrg != null)
                {
                    template = template.Replace("[SuperClient_Name]", superClientOrg.Name);
                    template = template.Replace("[SuperClient]", superClientOrg.Name);
                    subject = subject.Replace("[SuperClient]", superClientOrg.Name);
                }
                //template = template.Replace("[InvitationCode]", a);
                var error = "";
                string inviteVenUrl = ConfigurationSettings.AppSettings["InviteVendorInvitationURL"];

                var users = _SysBusiness.GetUserInfo(Invitation.VendorEmail) ?? new UserInfo();
                var body = template;
                body = body.Replace("[ClientName]", clientName);
                body = body.Replace("[SubClient]", clientName);
                if (superClientOrg != null)
                {
                    body = body.Replace("[SuperClient]", superClientOrg.Name);
                    subject = subject.Replace("[SuperClient]", superClientOrg.Name);
                }
                body = body.Replace("[VendorName]", ven.Name);
                subject = subject.Replace("[ClientName]", clientName);
                subject = subject.Replace("[VendorName]", ven.Name);
                body = body.Replace("[Date]", DateTime.Now.ToString());
                subject = subject.Replace("[Vendor_FirstLastNames]", users.FullName);
                subject = subject.Replace("[Vendor_EmailAddress]", Invitation.VendorEmail);
                body = body.Replace("[Vendor_FirstLastNames]", users.FullName);
                body = body.Replace("[Vendor_EmailAddress]", Invitation.VendorEmail);
                body = body.Replace("[Invitation_Code]", Invitation.InvitationCode + "");
                body = body.Replace("[Client_Name]", clientName);
                body = body.Replace("[URL]",
                    "<a href='" + inviteVenUrl + invitationId + "'>" + inviteVenUrl + invitationId +
                    "</a>");
                var cc = Invitation.SendCc;
                var HasClientUsers = _SysBusiness.CheckInviteClients(Invitation.ClientId);
                if (HasClientUsers)
                    cc += string.IsNullOrEmpty(cc) ? _SysBusiness.CheckClientUsersforInvitation(Invitation.ClientId).UserEmails : "," + _SysBusiness.CheckClientUsersforInvitation(Invitation.ClientId).UserEmails;

                var bcc = Invitation.SendBcc;

                if (!string.IsNullOrEmpty(bcc))
                    bcc += ",";
                bcc += "info@firstverify.com";
                string strMessage = "";
                strMessage = Common.SendMail.sendMail(Invitation.VendorEmail, body, subject, bcc, cc);

                return strMessage;
            }
        }
    }
}
