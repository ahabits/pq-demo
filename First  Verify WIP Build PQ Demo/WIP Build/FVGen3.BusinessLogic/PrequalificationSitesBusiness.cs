﻿/*
Page Created Date:  04/03/2018
Created By: Rajesh Pendela
Purpose: All Business operations related to prequalification sites will be performed here
Version: 1.0
****************************************************
*/

using FVGen3.BusinessLogic.Interfaces;
using System.Collections.Generic;
using FVGen3.Domain.ViewModels;
using FVGen3.Domain.Concrete;
using System;
using System.Web;
using FVGen3.Domain.Entities;
using System.Linq;
using FVGen3.Domain.LocalModels;
using System.Data.Entity;
using Resources;

namespace FVGen3.BusinessLogic
{
    public class PrequalificationSitesBusiness : BaseBusinessClass, IPrequalificationSitesBusiness
    {
        //public PrequalificationSitesBusiness(HttpContext httpContext) : base(httpContext)
        //{
        //}
        /// <summary>
        /// To get the specific site status
        /// </summary>
        /// <param name="PqSiteId"></param>
        /// <returns></returns>
        public SiteStatus GetSiteStatuses(long PqSiteId)
        {
            using (var entity = new EFDbContext())
            {
                var PqSite = entity.PrequalificationSites.Find(PqSiteId);
                return new SiteStatus() { SiteId = PqSite.PrequalificationSitesId, Status = PqSite.SiteStatusId };
            }
        }
        /// <summary>
        /// To update the sites status
        /// </summary>
        /// <param name="Sites"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public bool UpdateSiteStatuses(List<SiteStatus> Sites, Guid userId)
        {
            try
            {
                using (var entity = new EFDbContext())
                {
                    foreach (var site in Sites)
                    {
                        var PqSite = entity.PrequalificationSites.Find(site.SiteId);
                        PqSite.SiteStatusId = site.Status;
                        entity.Entry(PqSite).State = EntityState.Modified;
                        LocationStatusLog log = new LocationStatusLog();
                        log.pqSiteId = site.SiteId;
                        log.pqSiteStatusId = site.Status;
                        log.StatusChangeDate = DateTime.Now;
                        log.StatusChangedBy = userId;
                        entity.LocationStatusLog.Add(log);
                    }
                    entity.SaveChanges();
                }
            }
            catch
            {
                return false;
            }
            return true;
        }

        public List<PQBusinessUnitSites> GetPQSites(long PqId, Guid UserId)
        {
            using (var entity = new EFDbContext())
            {
                List<PQBusinessUnitSites> pqSites = entity.PrequalificationSites.Where(r => r.PrequalificationId == PqId).Select(r => new PQBusinessUnitSites
                {
                    ClientId = r.ClientId,
                    ClientName = r.Client.Name,
                    ClientBusinessUnitId = r.ClientBusinessUnitId,
                    BusinessUnitName = r.ClientBusinessUnits.BusinessUnitName,
                    ClientBusinessUnitSiteID = r.ClientBusinessUnitSiteId,
                    BusinesUnitsiteName = r.ClientBusinessUnitSites.SiteName,
                    PrequalificationSitesId = r.PrequalificationSitesId,
                    PQSiteStatus = r.PQSiteStatus,
                    SiteStatus = (r.PrequalificationStatus != null ? r.PrequalificationStatus.PrequalificationStatusName : r.Prequalification.PrequalificationStatus.PrequalificationStatusName),
                    Comment = r.Comment,
                    PrequalificationId = r.PrequalificationId
                }).ToList();

                var orgType = entity.SystemUsersOrganizations.FirstOrDefault(r => r.UserId == UserId).Organizations.OrganizationType;
                bool IsClient = false;
                if(orgType.ToString().ToLower().Equals(Resources.LocalConstants.Client) 
                    || orgType.ToString().ToLower().Equals(Resources.LocalConstants.SuperClient))
                {
                    IsClient = true;
                }   

                if(IsClient)
                {
                    SystemUsersBUSites userAssociatedSites = entity.SystemUsersBUSites.FirstOrDefault(rec => rec.SystemUserId == UserId);
                    if(userAssociatedSites != null && userAssociatedSites.BUSiteIDsList.Count() > 0)
                    {
                        var SiteIds = userAssociatedSites.BUSiteIDsList;
                        pqSites = pqSites.Where(r => SiteIds.Contains((long)r.ClientBusinessUnitSiteID)).ToList();
                    }                     
                }
                return pqSites;
            }
        }

        public PrequalificationSites GetPqSite(long? pqSiteId, long? buId, long? buSiteId, long? pqId)
        {
            using (var entity = new EFDbContext())
            {
                if (pqSiteId != null && pqId == null)
                {
                    return entity.PrequalificationSites.Find(pqSiteId);
                }
                else
                {
                    return entity.PrequalificationSites.FirstOrDefault(rec => rec.ClientBusinessUnitId == buId && rec.ClientBusinessUnitSiteId == buSiteId && rec.PrequalificationSitesId != pqSiteId && rec.PrequalificationId == pqId);
                }
            }
        }

        public List<KeyValuePair<string, string>> GetBusinessUnitSites(long? clientBuId)
        {
            using (var entity = new EFDbContext())
            {
                ClientBusinessUnits busUnit = entity.ClientBusinessUnits.FirstOrDefault(record => record.ClientBusinessUnitId == clientBuId);
                if (busUnit != null)
                {
                    return busUnit.ClientBusinessUnitSites.ToList().Select(r =>
                        new KeyValuePair<string, string>(r.SiteName, r.ClientBusinessUnitSiteId.ToString())).ToList();
                }
                else
                {
                    return null;
                }
            }
        }

        public void PostBuSites(LocalAddPrequalificationSite form)
        {
            using (var entity = new EFDbContext())
            {
                var helper = new PrequalificationHelper();
                foreach (var prequalificationSite in form.ClientBusinessUnitSites)
                {
                    var prequalSite = entity.PrequalificationSites.FirstOrDefault(rec =>
                        rec.PrequalificationId == form.preQualificationId &&
                        rec.ClientBusinessUnitSiteId == prequalificationSite.BusinessUnitSiteId &&
                        rec.ClientBusinessUnitId == form.BusinessUnit);
                    if (prequalSite == null)
                    {
                        PrequalificationSites preSite = new PrequalificationSites();
                        if (prequalificationSite.isChecked && prequalificationSite.BusinessUnitSiteId != 999)
                        {
                            preSite.ClientBusinessUnitSiteId = Convert.ToInt64(prequalificationSite.BusinessUnitSiteId);

                            preSite.ClientBusinessUnitId = Convert.ToInt64(form.BusinessUnit);
                            preSite.PrequalificationId = form.preQualificationId;
                            preSite.Comment = form.Comments;

                            var preQualification =
                                entity.Prequalification.Find(form.preQualificationId);
                            preSite.ClientId = form.ClientId;
                            if(preQualification != null)
                                preSite.VendorId = preQualification.VendorId;
                            //To implement group wise payment insted of BU wise Payment.
                            if ( preQualification != null && preQualification.ClientTemplates.Templates.IsERI)
                            {
                                var paymentId = helper.GetBuPaymentId((long) preSite.ClientBusinessUnitId,
                                    form.preQualificationId);
                                if (paymentId != null)
                                    preSite.PrequalificationPaymentsId = paymentId;
                            }
                            
                            entity.PrequalificationSites.Add(preSite);

                            entity.SaveChanges();
                        }
                    }
                }

                var locationsSec = entity.PrequalificationCompletedSections.FirstOrDefault(rec =>
                    rec.PrequalificationId == form.preQualificationId &&
                    rec.TemplateSections.TemplateSectionType == SectionTypes.IsBuSites);
                if (locationsSec != null)

                    entity.PrequalificationCompletedSections.Remove(locationsSec);
                
                entity.SaveChanges();
            }
        }
    }
}
