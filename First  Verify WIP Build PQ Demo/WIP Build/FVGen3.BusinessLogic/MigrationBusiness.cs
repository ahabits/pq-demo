﻿/*
Page Created Date:  03/01/2019
Created By: Kiran Talluri
Purpose: All Business operations related to vendor migration from single client to multi client will be performed here
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using FVGen3.BusinessLogic.Interfaces;
using FVGen3.DataLayer.DTO;
using FVGen3.Domain.Concrete;
using FVGen3.Domain.Entities;
using System.Resources;
using System.Web;

using FVGen3.Domain.LocalModels;
using AutoMapper;
using System.Data.SqlClient;
using Resources;
using System.IO;
using System.Configuration;

namespace FVGen3.BusinessLogic
{
    public class MigrationBusiness : IMigrationBusiness
    {
        IPrequalificationBusiness _prequalificationBusiness;
        private PrequalificationHelper helper = new PrequalificationHelper();

        public MigrationBusiness(IPrequalificationBusiness _prequalificationBusiness)
        {


            this._prequalificationBusiness = _prequalificationBusiness;
        }
        /// <summary>
        /// To migrate the emailed comments
        /// </summary>
        /// <param name="sourcePQ"></param>
        /// <param name="destPQ"></param>
        public void NotificationEmailComments(long sourcePQ, long destPQ)
        {
            using (var entityDB = new EFDbContext())
            {
                var emailcomments = entityDB.OrganizationsNotificationsEmailLogs.Where(r => r.PrequalificationId == sourcePQ);
                foreach (var Comments in emailcomments.ToList())
                {
                    var orgNotification = entityDB.OrganizationsNotificationsSent.FirstOrDefault(r => r.Client.OrganizationType == "Super Client" && r.SetupType == Comments.OrganizationsNotificationsSent.SetupType);
                    if (orgNotification == null)
                    {
                        orgNotification = new OrganizationsNotificationsSent();
                        orgNotification.ClientId = -1;
                        orgNotification.SetupType = Comments.OrganizationsNotificationsSent.SetupType;
                        orgNotification.SetupTypeId = Comments.OrganizationsNotificationsSent.SetupTypeId;

                        entityDB.OrganizationsNotificationsSent.Add(orgNotification);

                        entityDB.SaveChanges();
                    }
                    OrganizationsNotificationsEmailLogs NotificationComments = new OrganizationsNotificationsEmailLogs();
                    NotificationComments.PrequalificationId = destPQ;
                    NotificationComments.CommentsUpdateDateTime = Comments.CommentsUpdateDateTime;
                    // NotificationComments.Document = Comments.Document;
                    NotificationComments.DocumentId = Comments.DocumentId;
                    NotificationComments.EmailBcc = Comments.EmailBcc;
                    NotificationComments.EmailCc = Comments.EmailCc;
                    // NotificationComments.EmailRecordId = Comments.EmailRecordId;
                    NotificationComments.EmailSentDateTime = Comments.EmailSentDateTime;
                    NotificationComments.EmailSentTo = Comments.EmailSentTo;
                    NotificationComments.EmailSentToNames = Comments.EmailSentToNames;
                    NotificationComments.Comments = Comments.Comments;
                    NotificationComments.CommentsUpdateDateTime = Comments.CommentsUpdateDateTime;
                    NotificationComments.ExpiringDate = Comments.ExpiringDate;
                    NotificationComments.EmailTemplateId = Comments.EmailTemplateId;
                    NotificationComments.MailSentBy = Comments.MailSentBy;
                    NotificationComments.NotificationId = orgNotification.NotificationId;
                    NotificationComments.NotificationNo = Comments.NotificationNo;
                    NotificationComments.NotificationStatus = Comments.NotificationStatus;
                    NotificationComments.NotificationType = Comments.NotificationType;
                    NotificationComments.VendorId = Comments.VendorId;
                    NotificationComments.ReadByAdmin = Comments.ReadByAdmin;
                    NotificationComments.ReadByClient = Comments.ReadByClient;
                    NotificationComments.ReadByVendor = Comments.ReadByVendor;
                    NotificationComments.RecordStatus = Comments.RecordStatus;

                    entityDB.OrganizationsNotificationsEmailLogs.Add(NotificationComments);

                }
                entityDB.SaveChanges();
            }



        }
        /// <summary>
        /// To migrate the manual comments
        /// </summary>
        /// <param name="sourcePQ"></param>
        /// <param name="destPQ"></param>
        /// <param name="Data"></param>
        public void MoveComments(long sourcePQ, long destPQ, List<QuestionsForMigration> Data)
        {
            using (var entityDB = new EFDbContext())
            {
                var comments = entityDB.PrequalificationComments.Where(r => r.PrequalificationId == sourcePQ);
                foreach (var comment in comments)
                {
                    PrequalificationComments pqcomments = new PrequalificationComments();
                    comment.PrequalificationId = destPQ;
                    pqcomments.Comments = comment.Comments;
                    pqcomments.CommentsBy = comment.CommentsBy;
                    pqcomments.CommentsDate = comment.CommentsDate;
                    pqcomments.CommentsRecordId = comment.CommentsRecordId;
                    pqcomments.CommentsTime = comment.CommentsTime;
                    pqcomments.TemplateSectionID = Data.FirstOrDefault(r => r.SectionID == comment.TemplateSectionID).DestinationSectionId;
                    pqcomments.SubSectionID = Data.FirstOrDefault(r => r.SourceSubsectionId == comment.SubSectionID).DestinationSubsectionId;
                    entityDB.PrequalificationComments.Add(pqcomments);
                    //entityDB.Entry(comment).State = EntityState.Modified;
                }
                entityDB.SaveChanges();
            }
        }
        /// <summary>
        /// To Migrate the reporting data
        /// </summary>
        /// <param name="sourcePQ"></param>
        /// <param name="destPQ"></param>
        public void MoveReportingData(long sourcePQ, long destPQ)
        {
            using (var entityDB = new EFDbContext())
            {
                var reportingData = entityDB.PrequalificationReportingData.Where(r => r.PrequalificationId == sourcePQ);
                foreach (var data in reportingData)
                {
                    PrequalificationReportingData reportData = new PrequalificationReportingData();
                    reportData.PrequalificationId = destPQ;
                    reportData.ClientId = -1;
                    reportData.VendorId = data.VendorId;
                    reportData.ReportingType = data.ReportingType;
                    reportData.ReportingDataId = data.ReportingDataId;
                    entityDB.PrequalificationReportingData.Add(reportData);


                }
                entityDB.SaveChanges();
            }
        }
        /// <summary>
        /// To record the payment
        /// </summary>
        /// <param name="Sourcepqid"></param>
        /// <param name="Destpqid"></param>
        /// <param name="Data"></param>
        public void RecordPayment(long Sourcepqid, long Destpqid, List<QuestionsForMigration> Data)
        {
            using (var entityDB = new EFDbContext())
            {
                RecordZeroPQPayment(Destpqid, entityDB);

                var sourcePQ = entityDB.Prequalification.Find(Sourcepqid);
                var destPQ = entityDB.Prequalification.Find(Destpqid);
                if (sourcePQ.PrequalificationPayments.Any(r => r.PaymentCategory == 0))
                {
                    var paymentLogId = sourcePQ.PrequalificationPayments.FirstOrDefault(r => r.PaymentCategory == 0).PrequalificationPaymentsId;
                    if (destPQ.PrequalificationSites.Any(r => r.ClientId == sourcePQ.ClientId))
                        foreach (var sites in destPQ.PrequalificationSites.Where(r => r.ClientId == sourcePQ.ClientId).ToList())
                        {
                            sites.PrequalificationPaymentsId = paymentLogId;
                            entityDB.Entry(sites).State = EntityState.Modified;
                        }
                    else
                    {
                        //Have to add BU's in destination for payments.
                        var busites = entityDB.ClientBusinessUnits.Where(r => r.ClientId == sourcePQ.ClientId).SelectMany(r => r.ClientBusinessUnitSites).ToList();
                        foreach (var site in busites)
                        {
                            var pqSite = new PrequalificationSites()
                            {
                                ClientId = sourcePQ.ClientId,
                                VendorId = sourcePQ.VendorId,
                                PrequalificationId = destPQ.PrequalificationId,
                                ClientBULocation = site.BusinessUnitLocation,
                                ClientBusinessUnitId = site.ClientBusinessUnitId,
                                ClientBusinessUnitSiteId = site.ClientBusinessUnitSiteId,
                                PrequalificationPaymentsId = paymentLogId,
                            };
                        }

                    }
                    foreach (var paymentLog in sourcePQ.PrequalificationPayments.ToList())
                    {
                        paymentLog.PaymentCategory = 1;
                        paymentLog.PrequalificationId = Destpqid;
                        entityDB.Entry(paymentLog).State = EntityState.Modified;
                    }

                }
                entityDB.SaveChanges();
            }
        }

        /// <summary>
        /// To record zero payments if any
        /// </summary>
        /// <param name="Destpqid"></param>
        /// <param name="entityDB"></param>
        private void RecordZeroPQPayment(long Destpqid, EFDbContext entityDB)
        {
            var destPQ = entityDB.Prequalification.Find(Destpqid);
            var buGroupIds = entityDB.ClientTemplatesBUGroupPrice.Where(rec => rec.ClientTemplateID == destPQ.ClientTemplateId && rec.GroupingFor == 0 && rec.GroupPrice != 0 && rec.GroupPriceType == 0).Select(rec => rec.ClientTemplateBUGroupId).ToList();
            var buIds = entityDB.ClientTemplatesForBU.Where(rec => buGroupIds.Contains((long)rec.ClientTemplateBUGroupId)).Select(rec => rec.ClientBusinessUnitId).ToList();
            var UnPaymentSites = entityDB.PrequalificationSites.Where(rec => buIds.Contains(rec.ClientBusinessUnitId) && rec.PrequalificationId == Destpqid && rec.PrequalificationPaymentsId == null).ToList();
            destPQ.PaymentReceived = true;
            PrequalificationPayments paymentLog = entityDB.PrequalificationPayments.FirstOrDefault(rec => rec.PrequalificationId == Destpqid && rec.PaymentCategory == 0);
            if (paymentLog == null)
            {
                paymentLog = new PrequalificationPayments();

                try
                {
                    paymentLog.InvoiceAmount = entityDB.ClientTemplatesBUGroupPrice.FirstOrDefault(rec => rec.ClientTemplateID == destPQ.ClientTemplateId && rec.GroupPriceType == 0 && rec.GroupingFor == 0).GroupPrice;
                }
                catch { }
                paymentLog.PaymentReceivedAmount = 0;
                paymentLog.PaymentReceivedDate = DateTime.Now;
                paymentLog.PaymentCategory = 0;
                //paymentLog.PaymentTransactionId = transactionId;
                paymentLog.PrequalificationId = Destpqid;
                //paymentLog.TransactionMetaData = strRequest;
                destPQ.TotalPaymentReceivedAmount = 0;
                entityDB.Entry(paymentLog).State = EntityState.Added;
                helper.RecordFeecapLog(Destpqid, paymentLog.PaymentReceivedAmount ?? 0, entityDB);
                // for 0 group bu's update in preq.sites
                foreach (var site in UnPaymentSites)
                {
                    site.PrequalificationPaymentsId = paymentLog.PrequalificationPaymentsId;
                    entityDB.Entry(site).State = EntityState.Modified;
                }
            }
            else
            {
                paymentLog.PaymentReceivedAmount = 0;
                paymentLog.PaymentReceivedDate = DateTime.Now;
                entityDB.Entry(paymentLog).State = EntityState.Modified;
                destPQ.TotalPaymentReceivedAmount = 0;
            }
            entityDB.Entry(destPQ).State = EntityState.Modified;
            entityDB.SaveChanges();
        }


        //public void RecordPayment(long Sourcepqid, long Destpqid, List<QuestionsForMigration> Data)
        //{
        //    using (var entityDB = new EFDbContext())
        //    {
        //        RecordZeroPQPayment(Destpqid, entityDB);
        //        var sourcePQ = entityDB.Prequalification.Find(Sourcepqid);
        //        var destPQ = entityDB.Prequalification.Find(Destpqid);

        //        foreach (var paymentLog in sourcePQ.PrequalificationPayments.ToList())
        //        {
        //            PrequalificationPayments paylog = new PrequalificationPayments();
        //            paylog.PaymentCategory = 1;
        //            paylog.PrequalificationId = Destpqid;
        //            paylog.InvoiceAmount = paymentLog.InvoiceAmount;
        //            paylog.PaymentReceivedAmount = paymentLog.PaymentReceivedAmount;
        //            paymentLog.PaymentReceivedDate = paymentLog.PaymentReceivedDate;
        //            paymentLog.PaymentTransactionId = paymentLog.PaymentTransactionId;
        //            paymentLog.SkipPayment = paymentLog.SkipPayment;
        //            paymentLog.TransactionMetaData = paymentLog.TransactionMetaData;
        //            entityDB.PrequalificationPayments.Add(paylog);

        //        }

        //        entityDB.SaveChanges();
        //        if (sourcePQ.PrequalificationPayments.Any())
        //        {
        //            var paymentLogId = sourcePQ.PrequalificationPayments.FirstOrDefault().PrequalificationPaymentsId;
        //            if (destPQ.PrequalificationSites.Any())
        //            {
        //                var paymentId = destPQ.PrequalificationPayments.FirstOrDefault(r => r.PaymentCategory == 1).PrequalificationPaymentsId;
        //                foreach (var sites in destPQ.PrequalificationSites.ToList())
        //                {
        //                    sites.PrequalificationPaymentsId = paymentId;
        //                    entityDB.Entry(sites).State = EntityState.Modified;
        //                }
        //            }
        //            entityDB.SaveChanges();
        //        }
        //    }
        //}
        //private void RecordZeroPQPayment(long Destpqid, EFDbContext entityDB)
        //{
        //    var destPQ = entityDB.Prequalification.Find(Destpqid);
        //    var buGroupIds = entityDB.ClientTemplatesBUGroupPrice.Where(rec => rec.ClientTemplateID == destPQ.ClientTemplateId && rec.GroupingFor == 0 && rec.GroupPrice != 0 && rec.GroupPriceType == 0).Select(rec => rec.ClientTemplateBUGroupId).ToList();
        //    var buIds = entityDB.ClientTemplatesForBU.Where(rec => buGroupIds.Contains((long)rec.ClientTemplateBUGroupId)).Select(rec => rec.ClientBusinessUnitId).ToList();
        //    var UnPaymentSites = entityDB.PrequalificationSites.Where(rec => buIds.Contains(rec.ClientBusinessUnitId) && rec.PrequalificationId == Destpqid && rec.PrequalificationPaymentsId == null).ToList();
        //    destPQ.PaymentReceived = true;
        //    PrequalificationPayments paymentLog = entityDB.PrequalificationPayments.FirstOrDefault(rec => rec.PrequalificationId == Destpqid && rec.PaymentCategory == 0);
        //    if (paymentLog == null)
        //    {
        //        paymentLog = new PrequalificationPayments();
        //        try
        //        {
        //            paymentLog.InvoiceAmount = entityDB.ClientTemplatesBUGroupPrice.FirstOrDefault(rec => rec.ClientTemplateID == destPQ.ClientTemplateId && rec.GroupPriceType == 0 && rec.GroupingFor == 0).GroupPrice;
        //        }
        //        catch
        //        {
        //        }
        //        paymentLog.PaymentReceivedAmount = 0;
        //        paymentLog.PaymentReceivedDate = DateTime.Now;
        //        paymentLog.PaymentCategory = 0;
        //        paymentLog.PrequalificationId = Destpqid;
        //        destPQ.TotalPaymentReceivedAmount = 0;
        //        entityDB.Entry(paymentLog).State = EntityState.Added;
        //        helper.RecordFeecapLog(Destpqid, paymentLog.PaymentReceivedAmount ?? 0, entityDB);

        //        var paymentId = destPQ.PrequalificationPayments.FirstOrDefault(r => r.PaymentCategory == 0).PrequalificationPaymentsId;
        //        foreach (var site in UnPaymentSites)
        //        {
        //            site.PrequalificationPaymentsId = paymentId;
        //            entityDB.Entry(site).State = EntityState.Modified;
        //        }
        //    }
        //    else
        //    {
        //        paymentLog.PaymentReceivedAmount = 0;
        //        paymentLog.PaymentReceivedDate = DateTime.Now;
        //        entityDB.Entry(paymentLog).State = EntityState.Modified;
        //        destPQ.TotalPaymentReceivedAmount = 0;
        //    }
        //    entityDB.Entry(destPQ).State = EntityState.Modified;
        //    entityDB.SaveChanges();
        //}
        /// <summary>
        /// To migrate the vendor details
        /// </summary>
        /// <param name="Sourcepqid"></param>
        /// <param name="Destpqid"></param>
        public void CopyVendorDetails(long Sourcepqid, long Destpqid)
        {
            using (var entity = new EFDbContext())
            {
                var details = entity.VendorDetails.Where(r => r.PrequalificationId == Sourcepqid);
                foreach (var Vendetails in details)
                {
                    VendorDetails vendorDetails = new VendorDetails();
                    vendorDetails.EmailAddress = Vendetails.EmailAddress;
                    vendorDetails.FullName = Vendetails.FullName;
                    vendorDetails.GroupNumber = Vendetails.GroupNumber;
                    vendorDetails.PrequalificationId = Destpqid;
                    vendorDetails.TelephoneNumber = Vendetails.TelephoneNumber;
                    vendorDetails.Title = Vendetails.Title;
                    entity.VendorDetails.Add(vendorDetails);

                }

                entity.SaveChanges();
            }
        }

        /// <summary>
        /// To migrate the answers
        /// </summary>
        /// <param name="Sourcepqid"></param>
        /// <param name="Destpqid"></param>
        /// <param name="Data"></param>
        public void CopyAnswers(long Sourcepqid, long Destpqid, List<QuestionsForMigration> Data)
        {
            using (var entity = new EFDbContext())
            {
                foreach (var values in Data)
                {
                    if (!string.IsNullOrEmpty(values.SourceQuestioncolumnId.ToString()))
                    {

                        var UserInput = entity.PrequalificationUserInput.FirstOrDefault(r => r.PreQualificationId == Sourcepqid && r.QuestionColumnId == values.SourceQuestioncolumnId);
                        if (UserInput != null)
                        {

                            PrequalificationUserInput preuserinput = new PrequalificationUserInput();
                            preuserinput.PreQualificationId = Destpqid;
                            preuserinput.QuestionColumnId = values.DestinationQuestioncolumnId.Value;
                            preuserinput.UserInput = UserInput.UserInput;
                            entity.PrequalificationUserInput.Add(preuserinput);
                        }
                    }
                }
                entity.SaveChanges();
            }
        }
        /// <summary>
        /// To migrate the EMR values
        /// </summary>
        /// <param name="sourcepqid"></param>
        /// <param name="destinationpqid"></param>
        /// <param name="Vendorid"></param>
        /// <param name="Data"></param>
        public void CopyEmRValues(long sourcepqid, long destinationpqid, long Vendorid, List<QuestionsForMigration> Data)
        {
            using (var entity = new EFDbContext())
            {
                var emr = entity.PrequalificationEMRStatsYears.Where(r => r.PrequalificationId == sourcepqid).ToList();

                foreach (var pq in emr)
                {
                    PrequalificationEMRStatsYears LatestpqstrtYr = new PrequalificationEMRStatsYears();
                    LatestpqstrtYr.PrequalificationId = destinationpqid;
                    LatestpqstrtYr.EMRStatsYear = pq.EMRStatsYear;
                    LatestpqstrtYr.ColumnNo = pq.ColumnNo;
                    entity.PrequalificationEMRStatsYears.Add(LatestpqstrtYr);
                    entity.SaveChanges();

                    var destinationEmryrId = entity.PrequalificationEMRStatsYears.FirstOrDefault(r => r.PrequalificationId == destinationpqid && r.EMRStatsYear == LatestpqstrtYr.EMRStatsYear).PrequalEMRStatsYearId;
                    var sourceEmrValues = entity.PrequalificationEMRStatsYears.FirstOrDefault(r => r.PrequalificationId == sourcepqid && r.EMRStatsYear == pq.EMRStatsYear).PrequalificationEMRStatsValues.ToList();
                    foreach (var emrvalues in sourceEmrValues)
                    {

                        PrequalificationEMRStatsValues Emrvalues = new PrequalificationEMRStatsValues();

                        Emrvalues.PrequalEMRStatsYearId = destinationEmryrId;

                        var QuestionId = Data.FirstOrDefault(r => r.SourceQuestionId == emrvalues.QuestionId);
                        if (QuestionId != null)
                        {
                            Emrvalues.QuestionId = Data.FirstOrDefault(r => r.SourceQuestionId == emrvalues.QuestionId).DestinationQuestionId.Value;
                        }

                        Emrvalues.QuestionColumnId = Data.FirstOrDefault(r => r.SourceQuestioncolumnId == emrvalues.QuestionColumnId).DestinationQuestioncolumnId.Value;
                        Emrvalues.QuestionColumnIdValue = emrvalues.QuestionColumnIdValue;
                        entity.PrequalificationEMRStatsValues.Add(Emrvalues);
                    }
                    entity.SaveChanges();
                }


            }


        }

        /// <summary>
        /// To migrate the documents
        /// </summary>
        /// <param name="sourcepqid"></param>
        /// <param name="destinationpqid"></param>
        /// <param name="Vendorid"></param>
        /// <param name="Data"></param>
        public void CopyDocuments(long sourcepqid, long destinationpqid, long Vendorid, List<QuestionsForMigration> Data)
        {
            using (var entity = new EFDbContext())
            {
                var docs = entity.Document.Where(r => r.ReferenceRecordID == sourcepqid && r.ReferenceType == "PreQualification").ToList();
                var sourcePQ = entity.Prequalification.Find(sourcepqid);
                if (docs != null)
                    foreach (var Doc in docs)
                    {

                        //Document D = new Document();
                        Doc.ReferenceRecordID = destinationpqid;
                        Doc.OrganizationId = Vendorid;
                        //D.DocumentName = Doc.DocumentName;
                        //D.DocumentStatus = Doc.DocumentStatus;
                        //D.DocumentTypeId = Doc.DocumentTypeId;
                        //D.EmailSent = Doc.EmailSent;
                        //D.OrderBy = Doc.OrderBy;
                        //D.Expires = Doc.Expires;
                        //D.EmailSentDateTime = Doc.EmailSentDateTime;
                        //D.ShowOnDashboard = Doc.ShowOnDashboard;
                        //D.StatusChangedBy = Doc.StatusChangedBy;
                        //D.StatusChangedUser = Doc.StatusChangedUser;
                        //D.UpdatedBy = Doc.UpdatedBy;
                        //D.UpdatedOn = Doc.UpdatedOn;
                        //D.ReferenceType = Doc.ReferenceType;
                        //D.StatusChangeDate = Doc.StatusChangeDate;
                        //D.SystemUserAsUploader = Doc.SystemUserAsUploader;
                        //D.ClientId = Doc.ClientId;
                        //D.StatusChangeDate = Doc.StatusChangeDate;
                        //D.DocumentId = new Guid();
                        //D.Uploaded = Doc.Uploaded;
                        Doc.ClientId = (Doc.DocumentTypeId == 2 || Doc.DocumentTypeId == 14) ? sourcePQ.ClientId : -1;
                        Doc.SectionId = Doc.SectionId != null ? Data.FirstOrDefault(r => r.SectionID == Doc.SectionId).DestinationSectionId : Doc.SectionId;
                        entity.Entry(Doc).State = EntityState.Modified;
                    }
                entity.SaveChanges();
            }
        }
        //private long GetClientIdForDocs(long srcclientId,Guid destTemplateId,Document d)
        //{
        //    using (var entity = new EFDbContext())
        //    {

        //    return LocalConstants.ERI;
        //    }
        //}
        //private void CopyDoc(Guid sourceDocId, Guid destinationDocId)
        //{
        //    string oldFilePath = Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["ClientUploadsPath"]), sourceDocId + ".pdf");
        //    string newFilePath = Path.Combine(HttpContext.Server.MapPath(ConfigurationSettings.AppSettings["ClientUploadsPath"]), destinationDocId + ".pdf");

        //    System.IO.File.Move(oldFilePath, newFilePath);
        //    System.IO.File.Delete(newFilePath);
        //}

        /// <summary>
        /// To igrate the sites/locations
        /// </summary>
        /// <param name="sourcepqid"></param>
        /// <param name="destinationpqid"></param>
        public void CopySites(long sourcepqid, long destinationpqid)
        {
            using (var entity = new EFDbContext())
            {
                var pqsites = entity.PrequalificationSites.Where(r => r.PrequalificationId == sourcepqid);
                if (pqsites != null)
                    foreach (var sites in pqsites)
                    {
                        PrequalificationSites psites = new PrequalificationSites();
                        psites.PrequalificationId = destinationpqid;
                        psites.PrequalificationSitesId = sites.PrequalificationSitesId;
                        psites.PrequalificationStatus = sites.PrequalificationStatus;
                        psites.PrequalificationSitesId = sites.PrequalificationSitesId;
                        psites.ClientId = sites.ClientId;
                        psites.VendorId = sites.VendorId;
                        psites.ClientBusinessUnitId = sites.ClientBusinessUnitId;
                        psites.ClientBusinessUnitSiteId = sites.ClientBusinessUnitSiteId;
                        psites.ClientBULocation = sites.ClientBULocation;
                        psites.Comment = sites.Comment;
                        psites.PrequalificationPaymentsId = sites.PrequalificationPaymentsId;
                        psites.PQSiteStatus = sites.PQSiteStatus;

                        entity.PrequalificationSites.Add(psites);

                    }
                entity.SaveChanges();
            }
        }

        /// <summary>
        /// To record the clients selected
        /// </summary>
        /// <param name="destinationpqid"></param>
        /// <param name="clientid"></param>
        public void AddPQClient(long destinationpqid, long clientid)
        {
            using (var entity = new EFDbContext())
            {
                var pqClient = entity.PrequalificationClient.FirstOrDefault(r => r.PQId == destinationpqid);
                if (pqClient == null)
                {
                    pqClient = new PrequalificationClient();
                    pqClient.PQId = destinationpqid;
                    pqClient.ClientIds = clientid.ToString();
                    pqClient.Region = "1";
                    entity.PrequalificationClient.Add(pqClient);
                }
                else
                {
                    pqClient.ClientIds = pqClient.ClientIds + "," + clientid;
                    if (!string.IsNullOrEmpty(pqClient.Region) && pqClient.Region.Split(',').Any(r => r != "1"))
                    {
                        pqClient.Region = pqClient.Region + ",1";
                    }
                    entity.Entry(pqClient).State = EntityState.Modified;
                }
                entity.SaveChanges();
            }
        }

        /// <summary>
        /// To migrate the completed sections
        /// </summary>
        /// <param name="Sourcepqid"></param>
        /// <param name="destinationpqid"></param>
        /// <param name="Data"></param>
        public void PQCompletedSections(long Sourcepqid, long destinationpqid, List<QuestionsForMigration> Data)
        {
            using (var entity = new EFDbContext())
            {
                var comsecs = entity.PrequalificationCompletedSections.Where(r => r.PrequalificationId == Sourcepqid);
                foreach (var com in comsecs)
                {

                    if (!Data.Any(r => r.SectionID == com.TemplateSectionId))
                        continue;
                    PrequalificationCompletedSections comsec = new PrequalificationCompletedSections();
                    comsec.PrequalificationId = destinationpqid;

                    comsec.TemplateSectionId = Data.FirstOrDefault(r => r.SectionID == com.TemplateSectionId).DestinationSectionId.Value;

                    entity.PrequalificationCompletedSections.Add(comsec);

                    var sourceSec = Data.FirstOrDefault(r => r.SectionID == com.TemplateSectionId);

                }
                entity.SaveChanges();

            }

        }

        /// <summary>
        /// To migrate the information related to sections that needs to be displayed
        /// </summary>
        /// <param name="Sourcepqid"></param>
        /// <param name="destinationpqid"></param>
        /// <param name="Data"></param>
        public void SectionstoDisplay(long Sourcepqid, long destinationpqid, List<QuestionsForMigration> Data)
        {
            using (var entity = new EFDbContext())
            {
                var comsecs = entity.PrequalificationSectionsToDisplay.Where(r => r.PrequalificationId == Sourcepqid);
                foreach (var com in comsecs)
                {
                    //var PQsectoDisplay = entity.PrequalificationSectionsToDisplay.FirstOrDefault(r => r.PrequalificationId == Sourcepqid && r.TemplateSectionId == sourceSec.SectionID);
                    //if (PQsectoDisplay == null)
                    {
                        com.PrequalificationId = destinationpqid;
                        com.TemplateSectionId = Data.FirstOrDefault(r => r.SectionID == com.TemplateSectionId).DestinationSectionId.Value;
                        entity.Entry(com).State = EntityState.Modified;
                    }
                }
            }
        }

        /// <summary>
        /// To delete the old prequalification
        /// </summary>
        /// <param name="prequalId"></param>
        public void deletePrequalification(long prequalId)
        {
            using (var entityDB = new EFDbContext())
            {
                Prequalification prequal = entityDB.Prequalification.Find(prequalId);

                if (prequal != null)
                {
                    List<Document> prequalDocs = entityDB.Document.Where(rec => rec.ReferenceType == "PreQualification" && rec.ReferenceRecordID == prequalId).ToList();
                    {
                        foreach (var StatusLog in prequal.PrequalificationStatusChangeLog.ToList())
                            entityDB.PrequalificationStatusChangeLog.Remove(StatusLog);

                        if (prequal.PrequalificationComments != null)
                            foreach (var Comments in prequal.PrequalificationComments.ToList())
                                entityDB.PrequalificationComments.Remove(Comments);
                        if (prequal.PrequalificationCompletedSections != null)
                            foreach (var PCompletedSection in prequal.PrequalificationCompletedSections.ToList())
                                entityDB.PrequalificationCompletedSections.Remove(PCompletedSection);
                        if (prequal.PrequalificationReportingData != null)
                            foreach (var ReportingData in prequal.PrequalificationReportingData.ToList())
                                entityDB.PrequalificationReportingData.Remove(ReportingData);
                        if (prequal.PrequalificationSites != null)
                            foreach (var PreSites in prequal.PrequalificationSites.ToList())
                                entityDB.PrequalificationSites.Remove(PreSites);
                        if (prequal.PrequalificationUserInputs != null)
                            foreach (var UserInput in prequal.PrequalificationUserInputs.ToList())
                                entityDB.PrequalificationUserInput.Remove(UserInput);
                        if (prequal.PrequalificationClient.Id != 0)
                            entityDB.PrequalificationClient.Remove(prequal.PrequalificationClient);

                        if (prequal.VendorDetails != null)
                            foreach (var vendorContact in prequal.VendorDetails.ToList())
                                entityDB.VendorDetails.Remove(vendorContact);
                        if (prequal.PrequalificationPayments != null)
                            foreach (var prequalPayment in prequal.PrequalificationPayments.ToList())
                                entityDB.PrequalificationPayments.Remove(prequalPayment);
                        if (prequal.PrequalificationTrainingAnnualFees != null)
                            foreach (var TrainingPayment in prequal.PrequalificationTrainingAnnualFees.ToList())
                                entityDB.PrequalificationTrainingAnnualFees.Remove(TrainingPayment);
                        if (prequalDocs != null)
                            foreach (var Docs in prequalDocs.ToList())
                                entityDB.Document.Remove(Docs);

                        if (prequal.PrequalificationEMRStatsYears != null)
                            foreach (var prequalEmrStatYear in prequal.PrequalificationEMRStatsYears.ToList())
                            {
                                if (prequalEmrStatYear.PrequalificationEMRStatsValues != null)
                                {
                                    foreach (var values in prequalEmrStatYear.PrequalificationEMRStatsValues.ToList())
                                    {
                                        entityDB.PrequalificationEMRStatsValues.Remove(values);
                                    }
                                }
                                entityDB.PrequalificationEMRStatsYears.Remove(prequalEmrStatYear);
                            }

                        if (prequal.OrganizationsNotificationsEmailLogs != null)
                            foreach (var prequalNotificationEmailLogs in prequal.OrganizationsNotificationsEmailLogs.ToList())
                                entityDB.OrganizationsNotificationsEmailLogs.Remove(prequalNotificationEmailLogs);
                        if (prequal.PrequalificationNotifications != null)
                            foreach (var prequalNotification in prequal.PrequalificationNotifications.ToList())
                                entityDB.PrequalificationNotifications.Remove(prequalNotification);

                        if (prequal.PrequalificationSectionsToDisplay != null)
                            foreach (var section in prequal.PrequalificationSectionsToDisplay.ToList())
                                entityDB.PrequalificationSectionsToDisplay.Remove(section);


                        if (prequal.FeeCapLog != null)
                            foreach (var feecap in prequal.FeeCapLog.ToList())
                                entityDB.FeeCapLog.Remove(feecap);
                        if (prequal.PrequalificationSubsection.Any())
                            foreach (var pqSubSec in prequal.PrequalificationSubsection.ToList())
                            {
                                foreach (var pqQues in pqSubSec.PrequalificationQuestion.ToList())
                                {
                                    entityDB.PrequalificationQuestion.Remove(pqQues);
                                }
                                entityDB.PrequalificationSubsection.Remove(pqSubSec);
                            }
                        entityDB.Prequalification.Remove(prequal);


                    }
                    entityDB.SaveChanges();
                }
            }
        }
    }

}
