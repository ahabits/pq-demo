﻿/*
Page Created Date:  03/15/2017
Created By: Rajesh Pendela
Purpose: All Business operations related to employees quiz module will be performed here
Version: 1.0
****************************************************
*/

using FVGen3.Domain.Concrete;
using FVGen3.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FVGen3.BusinessLogic
{
    public class QuizHelper
    {
        /// <summary>
        /// To log the specific employee quiz
        /// </summary>
        /// <param name="suQuiz"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        public bool RecordQuizHistory(SystemUsersOrganizationsQuizzes suQuiz, EFDbContext entity)
        {
            var history = new SysUserOrgQuizHistory
            {
                SysUserOrgQuizId = suQuiz.SysUserOrgQuizId,
                CreatedDate = DateTime.Now,
                QuizExpiration = suQuiz.QuizExpirationDate ?? DateTime.Now,
                QuizResult = suQuiz.QuizResult ?? true,
                QuizSubmitted = suQuiz.QuizDateSubmitted ?? DateTime.Now,
                QuizStart = suQuiz.QuizDateStart ?? DateTime.Now
            };
            entity.SysUserOrgQuizHistory.Add(history);
            return true;
        }
        /// <summary>
        /// To get the specific employee training payment
        /// </summary>
        /// <param name="userQuizId"></param>
        /// <param name="vendroId"></param>
        /// <returns></returns>
        public decimal GetTrainingPayment(long userQuizId, long vendroId)
        {
            using (var entity = new EFDbContext())
            {
                var sysUserOrgQuiz = entity.SystemUsersOrganizationsQuizzes.Find(userQuizId);
                var latestPrequalification =
                    entity.LatestPrequalification.Where(
                            rec => rec.VendorId == vendroId && rec.ClientId == sysUserOrgQuiz.ClientTemplates.ClientID)
                        .OrderByDescending(rec => rec.PrequalificationStart)
                        .FirstOrDefault();

                List<long?> selectedPrequalBusinessUnitSites =
                    entity.PrequalificationSites.Where(
                            rec =>
                                rec.PrequalificationId == latestPrequalification.PrequalificationId &&
                                rec.ClientBusinessUnitSiteId != null)
                        .Select(rec => rec.ClientBusinessUnitSiteId)
                        .Distinct()
                        .ToList();


                var AnnualClientTemplateForBU =
                    entity.ClientTemplatesForBU.Where(
                            rec =>
                                rec.ClientTemplateId == latestPrequalification.ClientTemplateId &&
                                rec.ClientTemplatesBUGroupPrice.GroupingFor == 1 &&
                                rec.ClientTemplatesBUGroupPrice.GroupPriceType != -1 &&
                                rec.ClientTemplatesBUGroupPrice.GroupPrice != 0)
                        .Select(rec => rec.ClientBusinessUnitId)
                        .ToList(); // Kiran on 11/25/2015 for FV-232

                var applicableClientTemplatesForBU =
                    entity.ClientTemplatesForBU.Where(
                        rec =>
                            rec.ClientTemplateId == sysUserOrgQuiz.ClientTemplateId &&
                            rec.ClientTemplatesBUGroupPrice.GroupingFor == 2 &&
                            rec.ClientTemplatesBUGroupPrice.GroupPriceType != -1).ToList();

                var applicableClientTemplateBuIds = applicableClientTemplatesForBU.Where(
                        rec => !AnnualClientTemplateForBU.Contains(rec.ClientBusinessUnitId))
                    .SelectMany(rec => rec.ClientTemplatesForBUSites)
                    .Where(rec => selectedPrequalBusinessUnitSites.Contains(rec.ClientBusinessUnitSiteId))
                    .Select(rec => rec.ClientTemplateForBUId)
                    .Distinct()
                    .ToList();
                var applicableBusinessUnitsGroups =
                    entity.ClientTemplatesForBU.Where(
                            rec =>
                                rec.ClientTemplateId == sysUserOrgQuiz.ClientTemplateId &&
                                applicableClientTemplateBuIds.Contains(rec.ClientTemplateForBUId))
                        .Select(rec => rec.ClientTemplateBUGroupId)
                        .Distinct()
                        .ToList();
               
                   if(entity.ClientTemplatesBUGroupPrice.Any(
                            rec => applicableBusinessUnitsGroups.Contains(rec.ClientTemplateBUGroupId)))
                       return entity.ClientTemplatesBUGroupPrice.Where(
                           rec => applicableBusinessUnitsGroups.Contains(rec.ClientTemplateBUGroupId)).Select(rec => rec.GroupPrice ?? 0).Sum();
                //foreach (var clientTemplatesForBU in applicableClientTemplatesForBU)
                //{
                //    if (!AnnualClientTemplateForBU.Contains(clientTemplatesForBU.ClientBusinessUnitId))
                //    {
                //        var clientTemplatesForBUSites = clientTemplatesForBU.ClientTemplatesForBUSites.Where(rec => selectedPrequalBusinessUnitSites.Contains(rec.ClientBusinessUnitSiteId)).ToList();
                //        var clientTemplatesForBuIds = clientTemplatesForBUSites.Select(rec => rec.ClientTemplateForBUId).Distinct().ToList();
                //        applicableClientTemplateBuIds.AddRange(clientTemplatesForBuIds);
                //    }
                //}
                //var applicableBusinessUnitsGroups = entityDB.ClientTemplatesForBU.Where(rec => rec.ClientTemplateId == clientTrainingTemplate.ClientTemplateId && applicableClientTemplateBuIds.Contains(rec.ClientTemplateForBUId)).Select(rec => rec.ClientTemplateBUGroupId).Distinct().ToList();

                //var clientTemplatesForBu =
                //    entity.ClientTemplatesForBU.Where(rec => rec.ClientTemplateId == clientTemplateId);
                //foreach (var clientTemplateForBu in clientTemplatesForBu)
                //{

                //        //try
                //        //{
                //        //    var annualFeeBUs = AnnualBus.Select(rec => rec.ClientBusinessUnitId).ToList();
                //        //    if (!annualFeeBUs.Contains(clientTemplateForBu.ClientBusinessUnitId) &&
                //        //        prequalBusinessUnits.Contains(clientTemplateForBu.ClientBusinessUnitId))
                //        //        // Sandhya on 08/20/2015
                //        //    {
                //        //        templateTotalPrice = templateTotalPrice +
                //        //                             (decimal)
                //        //                             clientTemplateForBu.ClientTemplatesBUGroupPrice.GroupPrice;
                //        //    }
                //        //}
                //        //catch
                //        //{
                //        //}
                //    }
                //}
                return 0;
            }
        }


    }
}
