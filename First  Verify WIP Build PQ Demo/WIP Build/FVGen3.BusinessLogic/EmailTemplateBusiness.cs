﻿/*
Page Created Date:  06/30/2017
Created By: Rajesh Pendela
Purpose: All Business operations related to email templates will be performed here
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FVGen3.BusinessLogic.Interfaces;
using FVGen3.Domain.Concrete;
using FVGen3.Domain.Entities;
using FVGen3.Domain.LocalModels;
using System.Data.Entity;
using FVGen3.DataLayer.DTO;
using AutoMapper;

namespace FVGen3.BusinessLogic
{
    public class EmailTemplateBusiness : BaseBusinessClass, IEmailTemplateBusiness
    {

        IMapper mapper;
        public EmailTemplateBusiness()
        {
            var config = new MapperConfiguration(cfg => cfg.CreateMap<Organizations, OrganizationData>());
            mapper = config.CreateMapper();
        }
        /// <summary>
        /// To get the Email templates
        /// </summary>
        /// <returns></returns>
        public List<KeyValuePair<int, string>> GetGenericEmailTemplates()
        {
            using (var context = new EFDbContext())
            {
                return context.EmailTemplates.Where(r => r.EmailUsedFor == 0).Select(r => new { r.EmailTemplateID, r.Name }).OrderBy(r => r.Name).ToList().Select(r => new KeyValuePair<int, string>(r.EmailTemplateID, r.Name)).ToList();
            }
        }

        public List<EmailTemplates> GetClientEmailTemplates(List<long?> clientIds)
        {
            using (var context = new EFDbContext())
            {
                return context.EmailTemplates.Where(r => r.IsDocumentSpecific == false && r.EmailUsedFor == 0 && (r.ClientOrganizationID == null || clientIds.Contains(r.ClientOrganizationID))).OrderBy(r => r.Name).ToList();
            }
        }
        public string SaveLanguageEmailTemplate(int EmailTemplateId, long languageId, string EmailSubject, string EmailBody, string comments)
        {
            using (var context = new EFDbContext())
            {
                // var languageId = Int64.Parse(LanguageID);
                //EmailTemplateContent EmailContent = new EmailTemplateContent();
                var EmailTemplate = context.EmailTemplateContent.FirstOrDefault(r => r.EmailTemplateId == EmailTemplateId && r.LanguageId == languageId);
                if (languageId == 0)
                {
                    var Email = context.EmailTemplates.Find(EmailTemplateId);
                    Email.EmailBody = EmailBody;
                    Email.EmailSubject = EmailSubject;
                    context.Entry(Email).State = EntityState.Modified;
                    Email.CommentText = comments;
                }
                else
                {
                    if (EmailTemplate == null)
                    {
                        EmailTemplate = new EmailTemplateContent();
                        EmailTemplate.EmailBody = EmailBody;
                        EmailTemplate.EmailSubject = EmailSubject;
                        EmailTemplate.LanguageId = languageId;
                        EmailTemplate.EmailTemplateId = EmailTemplateId;

                        context.EmailTemplateContent.Add(EmailTemplate);
                    }
                    else
                    {
                        EmailTemplate.EmailBody = EmailBody;
                        EmailTemplate.EmailSubject = EmailSubject;
                        context.Entry(EmailTemplate).State = EntityState.Modified;
                    }
                    EmailTemplate.Comments = comments;
                }
                context.SaveChanges();
            }
            return "Save Successfully";
        }
        public EmailTemplates GetEmailData(int EmailTemplateId, string LanguageID)
        {
            using (var context = new EFDbContext())
            {
                EmailTemplates Template = new EmailTemplates();

                var languageId = Int64.Parse(LanguageID);
                var EmailTemplate = context.EmailTemplates.Find(EmailTemplateId);

                if (languageId == 0)
                {
                    Template.SelectedLanguageIds = string.Join(",", EmailTemplate.EmailTemplateContent.Select(r => r.LanguageId));
                }
                if (EmailTemplate != null && languageId == 0)
                {
                    Template.EmailBody = EmailTemplate.EmailBody;
                    Template.EmailSubject = EmailTemplate.EmailSubject;
                    Template.EmailTemplateID = EmailTemplateId;
                    Template.CommentText = EmailTemplate.CommentText;
                }
                if (LanguageID != "0")
                {
                    var EmailTemplatecontent = EmailTemplate.EmailTemplateContent.FirstOrDefault(r => r.LanguageId == languageId);
                    if (EmailTemplatecontent != null)
                    {
                        Template.EmailBody = EmailTemplatecontent.EmailBody;
                        Template.EmailSubject = EmailTemplatecontent.EmailSubject;
                        Template.EmailTemplateID = EmailTemplateId;
                        Template.CommentText = EmailTemplatecontent.Comments;
                    }

                }
                return Template;
            }
        }

        public long? GetEmailTemplateClientId(int emailTemplateId)
        {
            using (var entity = new EFDbContext())
            {
                return entity.EmailTemplates.Find(emailTemplateId).ClientOrganizationID;
            }
        }

        public LocalEmailComposeModel GetEmailTemplateContent(int templateId, long pqId, long? selectedSubClientId, Guid userId)
        {
            using (var entity = new EFDbContext())
            {
                EmailTemplates template = entity.EmailTemplates.Find(templateId);

                Prequalification pq = entity.Prequalification.Find(pqId);
                LocalEmailComposeModel newEmailForm = new LocalEmailComposeModel();
                if (pq != null)
                {
                    OrganizationData superClientOrganization =
                        mapper.Map<OrganizationData>(entity.Organizations.Find(pq.ClientId));
                    OrganizationData subClientOrganization = new OrganizationData();
                    if (selectedSubClientId != null)
                        subClientOrganization = mapper.Map<OrganizationData>(entity.Organizations.Find((long) selectedSubClientId));
                    var selectedPqVendorSuperUserEmail = "";
                    try
                    {
                        selectedPqVendorSuperUserEmail = entity.SystemUsersOrganizations
                            .FirstOrDefault(record =>
                                record.OrganizationId == pq.VendorId && record.PrimaryUser == true)?
                            .SystemUsers.Email;
                    }
                    catch (Exception e)
                    {
                    }

                    var clientEmail = "";
                    var clientSysUserOrgs = pq.Client.SystemUsersOrganizations;
                    foreach (var clientUserOrg in clientSysUserOrgs)
                    {
                        var clientOrgRoles = clientUserOrg.SystemUsersOrganizationsRoles;
                        foreach (var clientOrgRole in clientOrgRoles)
                        {
                            if (clientOrgRole.SystemRoles.RoleName.Equals("Super User"))
                            {
                                clientEmail = clientUserOrg.SystemUsers.Email;
                            }
                        }
                    }

                    try
                    {
                        var vendorDetailsContacts = entity.VendorDetails
                            .Where(rec =>
                                rec.PrequalificationId == pq.PrequalificationId &&
                                rec.EmailAddress != null).Select(rec => rec.EmailAddress).ToList();
                        if (vendorDetailsContacts.Count() != 0)
                        {
                            foreach (var contact in vendorDetailsContacts)
                            {
                                if (selectedPqVendorSuperUserEmail != null)
                                {
                                    var toAddress = "";
                                    ;
                                    try
                                    {
                                        toAddress = selectedPqVendorSuperUserEmail.ToString();
                                    }
                                    catch
                                    {
                                        toAddress = "";
                                    }

                                    if ((!selectedPqVendorSuperUserEmail.ToLower()
                                            .Contains(contact.ToLower()) && !toAddress.ToString().Equals(contact)) ||
                                        toAddress.ToString().Equals(""))
                                    {
                                        if (selectedPqVendorSuperUserEmail == "")
                                            selectedPqVendorSuperUserEmail = contact;
                                        else if (contact != null)
                                            selectedPqVendorSuperUserEmail =
                                                selectedPqVendorSuperUserEmail + "," + contact;
                                        else
                                            selectedPqVendorSuperUserEmail =
                                                selectedPqVendorSuperUserEmail + contact + ",";
                                    }
                                }
                            }

                        }

                    }
                    catch
                    {
                    }
                    newEmailForm.ToAddress = selectedPqVendorSuperUserEmail;
                    newEmailForm.Subject = template.EmailSubject;
                    newEmailForm.Body = template.EmailBody;
                    newEmailForm.EmailComment = template.CommentText;

                    newEmailForm.SendCopyToMySelf = true;
                    Guid loggedInUserId = userId;
                    newEmailForm.BCCAddress =
                        entity.SystemUsers.FirstOrDefault(rec => rec.UserId == loggedInUserId).Email;

                    newEmailForm.Subject = newEmailForm.Subject.Replace("[Date]", DateTime.Now + "");
                    newEmailForm.Subject = newEmailForm.Subject.Replace("[VendorName]", pq.Vendor.Name);
                    newEmailForm.Subject = newEmailForm.Subject.Replace("[ClientName]", superClientOrganization.Name);
                    newEmailForm.Subject = newEmailForm.Subject.Replace("[SuperClient]", superClientOrganization.Name);
                    if (subClientOrganization != null)
                        newEmailForm.Subject = newEmailForm.Subject.Replace("[SubClient]", subClientOrganization.Name);
                    newEmailForm.Subject =
                        newEmailForm.Subject.Replace("[CustomerServicePhone]", pq.Client.PhoneNumber);
                    newEmailForm.Subject = newEmailForm.Subject.Replace("[CustomerServiceEmail]", clientEmail);


                    newEmailForm.Body = newEmailForm.Body.Replace("[Date]", DateTime.Now + "");
                    newEmailForm.Body = newEmailForm.Body.Replace("[VendorName]", pq.Vendor.Name);
                    newEmailForm.Body = newEmailForm.Body.Replace("[ClientName]", superClientOrganization.Name);
                    newEmailForm.Body = newEmailForm.Body.Replace("[SuperClient]", superClientOrganization.Name);
                    if (subClientOrganization != null)
                        newEmailForm.Body = newEmailForm.Body.Replace("[SubClient]", subClientOrganization.Name);
                    newEmailForm.Body =
                        newEmailForm.Body.Replace("[CustomerServicePhone]", pq.Client.PhoneNumber);
                    newEmailForm.Body = newEmailForm.Body.Replace("[CustomerServiceEmail]", clientEmail);

                    

                    var AttachmentsList = (from attachment in template.EmailTemplateAttachments.ToList()
                        select new LocalAttachmetList
                        {
                            AttachmentName = attachment.FileName,
                            AttachmentId = attachment.AttachmentId,
                            isActive = 1
                        }).ToList();
                    newEmailForm.AttachmentsList = AttachmentsList;
                }

                return newEmailForm;
            }
        }
    }
}
