﻿/*
Page Created Date:  03/30/2016
Created By: Rajesh Pendela
Purpose: All Business operations related to Roles & Permissions module will be performed here
Version: 1.0
****************************************************
*/

using FVGen3.Domain.Concrete;
using FVGen3.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace FVGen3.BusinessLogic
{
    public class RolesHelper
    {
        /// <summary>
        /// To check whether the user has specific permissions
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="permissionID"></param>
        /// <param name="permissionType"></param>
        /// <param name="clientId"></param>
        /// <param name="vendorID"></param>
        /// <returns></returns>
        public static bool IsUserInRole(Guid userId, long permissionID, string permissionType,long clientId, long vendorID)
        {             
            EFDbContext entityDB;        
            entityDB = new EFDbContext();
            SystemUsers CurrentUser = entityDB.SystemUsers.Find(userId);
            SystemUsersOrganizations orgUser = entityDB.SystemUsersOrganizations.FirstOrDefault(rec => rec.UserId == CurrentUser.UserId);// && (rec.OrganizationId == clientId || rec.OrganizationId == vendorID));
            if (orgUser == null)
            {
                return false;
            }
            List<SystemUsersOrganizationsRoles> orgRoles = entityDB.SystemUsersOrganizationsRoles.Where(rec => rec.SysUserOrgId == orgUser.SysUserOrganizationId).ToList();
            if (orgRoles != null)
            {
                foreach (SystemUsersOrganizationsRoles role in orgRoles)
                {
                    var result = entityDB.SystemRolesPermissions.FirstOrDefault(rec => rec.SysRoleId == role.SysRoleId && rec.SysViewsId == permissionID);
                    if (result != null)
                    {

                        if ((permissionType.ToLower() == "read" && result.ReadPermission == true) ||
                            (permissionType.ToLower() == "delete" && result.DeletePermission == true) ||
                            (permissionType.ToLower() == "insert" && result.ModifyPermission == true) ||
                            (permissionType.ToLower() == "modify" && result.InsertPermission == true))
                        {
                            return true;
                        }
                    }
                }

                
            }
            return false;
        }
    }
}
