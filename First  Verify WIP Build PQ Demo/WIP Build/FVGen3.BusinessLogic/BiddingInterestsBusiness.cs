﻿/*
Page Created Date:  04/04/2017
Created By: Rajesh Pendela
Purpose: All Business operations related to bidding interests will be performed here
Version: 1.0
****************************************************
*/

using System.Collections.Generic;
using System.Linq;
using System.Web;
using FVGen3.BusinessLogic.Interfaces;
using FVGen3.Domain.Concrete;

namespace FVGen3.BusinessLogic
{
    public class BiddingInterestsBusiness : BaseBusinessClass,IBiddingInterestsBusiness
    {
      
        /// <summary>
        /// To get the Bidding Interests
        /// </summary>
        /// <returns></returns>
        public List<KeyValuePair<long, string>> GetBiddings()
        {
            using (var context=new EFDbContext())
            {
                return context.BiddingInterests.Where(rec=>rec.Status!=null && rec.Status==1).OrderBy(rec => rec.BiddingInterestName).Select(rec=>new{rec.BiddingInterestId,rec.BiddingInterestName}).AsEnumerable()
                    .Select(rec => new KeyValuePair<long, string>(rec.BiddingInterestId, rec.BiddingInterestName)).ToList();
            }
        }
    }
}
