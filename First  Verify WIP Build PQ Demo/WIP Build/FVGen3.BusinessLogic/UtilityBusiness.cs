﻿/*
Page Created Date:  08/21/2018
Created By: Kiran Talluri
Purpose: All Business operations related to error log will be performed here
Version: 1.0
****************************************************
*/

using FVGen3.DataLayer.DTO;
using FVGen3.Domain.Concrete;
using FVGen3.Domain.Entities;
using FVGen3.BusinessLogic.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Security.Cryptography;
using System.Text;
using FVGen3.Domain.ViewModels;
using System.Linq;

namespace FVGen3.BusinessLogic
{
    public class UtilityBusiness : BaseBusinessClass, IUtilityBusiness
    {
        /// <summary>
        /// To log the application error
        /// </summary>
        /// <param name="error"></param>
        public void RecordApplicationError(ErrorModel error)
        {
            using (var context = new EFDbContext())
            {
                ApplicationError ar = new ApplicationError();
                ar.ControllerName = error.ControllerName;
                ar.ActionName = error.ActionName;
                ar.CreatedDateTime = error.CreatedDateTime;
                ar.HasSession = error.HasSession;
                ar.Session = error.Session;
                ar.RequestData = error.RequestData;
                context.ApplicationError.Add(ar);
                context.SaveChanges();
            }
        }
        public static string EncryptData(string password)
        {
            try
            {
                var objDesCrypto = new TripleDESCryptoServiceProvider();
                var objHashMd5 = new MD5CryptoServiceProvider();
                var strTempKey = ConfigurationManager.AppSettings["encryptionkey"].ToString();
                var byteHash = objHashMd5.ComputeHash(Encoding.ASCII.GetBytes(strTempKey));
                objDesCrypto.Key = byteHash;
                objDesCrypto.Mode = CipherMode.ECB; //CBC, CFB
                var byteBuff = Encoding.ASCII.GetBytes(password);
                return Convert.ToBase64String(objDesCrypto.CreateEncryptor().TransformFinalBlock(byteBuff, 0, byteBuff.Length));
            }
            catch (Exception ex)
            {
                return "Wrong Input. " + ex.Message;
            }
        }
        public void Info(string message)
        {
            using (var context = new EFDbContext())
            {
                SystemLogs logs = new SystemLogs();
                logs.Date = DateTime.Now;
                logs.Message = message;
                context.SystemLogs.Add(logs);
                context.SaveChanges();
            }
        }
        public void Info(string message,string param,string screen)
        {
            using (var context = new EFDbContext())
            {
                SystemLogs logs = new SystemLogs();
                logs.Date = DateTime.Now;
                logs.Message = message;
                logs.Params = param;
                logs.Screen = screen;
                context.SystemLogs.Add(logs);
                context.SaveChanges();
            }
        }

        public List<StatusesDescription> GetStatusDescriptions()
        {
            using (var context = new EFDbContext())
            {
                List<StatusesDescription> statusDescriptions = new List<StatusesDescription>();
                List<PrequalificationStatus> pqStatuses = context.PrequalificationStatus
                    .Where(rec => rec.ShowInDashBoard == true).OrderBy(r => r.DisplayOrder).ToList();
                foreach(PrequalificationStatus pqStatus in pqStatuses)
                {
                    StatusesDescription status = new StatusesDescription();

                    status.StatusName = pqStatus.PrequalificationStatusName;
                    status.Description = pqStatus.Description;
                    status.DisplayOrder = pqStatus.DisplayOrder;

                    statusDescriptions.Add(status);

                }
                StatusesDescription multipleStatus = new StatusesDescription();
                multipleStatus.StatusName = "Multiple";
                multipleStatus.Description = "Vendor has a different status for multiple locations";
                multipleStatus.DisplayOrder = 6;
                statusDescriptions.Add(multipleStatus);

                return statusDescriptions.OrderBy(rec => rec.DisplayOrder).ToList();
            }
        }
    }
}
