﻿/*
Page Created Date:  03/30/2016
Created By: Rich Bronson
Purpose: All Business operations related to states will be performed here
Version: 1.0
****************************************************
*/

using FVGen3.DataLayer;
using FVGen3.DataLayer.Interfaces;
using System;
using System.Collections.Generic;
using FVGen3.DataLayer.DTO;


namespace FVGen3.BusinessLogic
{
    public class DropdDownDataSupplier
    {
        public IDynamicDataTableDAL Dal { get; set; }
        
        public DropdDownDataSupplier()
        {
            string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["EFDbContext"].ConnectionString;
            Dal = new DynamicDataTableDAL(connectionString);
        }
        
        public List<KeyValuePair<string,string>> GetDynamicTableData(string tableName,string keyField, string  valueField, string sortField, Guid? templateId,int vendorID = -1,bool showIDs = false,int clientId=-1)
        {
            return Dal.GetDynamicTableData(tableName, keyField, valueField, sortField, templateId, vendorID, showIDs, clientId);

            //if (tableName == "UnitedStates")
            //{
            //    return GetUnitedStatesTableData(vendorID, showIDs);
            //}
            //else
            //{
                
            //}

        }
        /// <summary>
        /// To get the UnitedStates table data
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="vendorID"></param>
        /// <param name="showIDs"></param>
        /// <returns></returns>
        public List<KeyValuePair<string, string>> GetDynamicTableData(string tableName, int vendorID = -1, bool showIDs = false)
        {
            //This will gradually migrate to being fully dynamic. Until then I will call seperate methods for each table type.
            if (tableName == "UnitedStates") 
            {
                return GetUnitedStatesTableData(vendorID, showIDs); 
            }
            throw new Exception("Invalid Table Name");
        }
        /// <summary>
        /// To get the United States table data with default items
        /// </summary>
        /// <param name="vendorID"></param>
        /// <param name="showIDs"></param>
        /// <param name="includeCanada"></param>
        /// <param name="country"></param>
        /// <returns></returns>
        public List<KeyValuePair<string,string>> GetUnitedStatesTableData(long vendorID, bool showIDs, bool includeCanada = false, string country="")
        {
            var results = Dal.GetUnitedStatesTableData(vendorID, showIDs, includeCanada, country);
            if (showIDs)
                results.Insert(0,new KeyValuePair<string, string>("0", "N/A"));
            else
                results.Insert(0, new KeyValuePair<string, string>("N/A", "N/A"));
            return results;           
        }
        /// <summary>
        /// To get the client risk levels
        /// </summary>
        /// <param name="clientID"></param>
        /// <returns></returns>
        public List<KeyValuePair<string, string>> GetRiskLevels(int clientID)
        {
            return Dal.GetRiskLevels(clientID);
        }

        /// <summary>
        /// To get the client risk levels with additional information
        /// </summary>
        /// <param name="clientID"></param>
        /// <returns></returns>
        public List<RiskLevelInfo> GetRiskLevelWithDetails(int clientID)
        {
            return Dal.GetRiskLevelWithDetails(clientID);
        }
        /// <summary>
        /// To get the client locations
        /// </summary>
        /// <param name="clientID"></param>
        /// <returns></returns>
        public List<KeyValuePair<string, string>> GetLocations(int clientID)
        {
            return Dal.GetLocations(clientID);
        }
        /// <summary>
        /// To get the client user emails
        /// </summary>
        /// <param name="clientID"></param>
        /// <returns></returns>
        public List<KeyValuePair<string, string>> GetUserEmails(int clientID,long Vendorid)
        {
            return Dal.GetUserEmails(clientID, Vendorid);
        }
        
    }
}
