﻿/*
Page Created Date:  05/18/2017
Created By: Rajesh Pendela
Purpose: All Business operations related to user will be performed here
Version: 1.0
****************************************************
*/

using System;
using System.Data.Entity;
using System.Linq;
using System.Web;
using FVGen3.BusinessLogic.Interfaces;
using FVGen3.DataLayer.DTO;
using FVGen3.Domain.Concrete;
using FVGen3.Domain.Entities;
using System.Collections.Generic;
using System.Web.ModelBinding;
using AutoMapper;
using Resources;
using FVGen3.Domain.LocalModels;
using FVGen3.WebUI.Constants;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using FVGen3.Domain.ViewModels;

namespace FVGen3.BusinessLogic
{

    public class SystemUserBusiness : BaseBusinessClass, ISystemUserBusiness
    {
        IMapper _mapper;
        IMapper _mapper1;
        //public SystemUserBusiness(HttpContext httpContext) : base(httpContext)
        //{
        //}
        public SystemUserBusiness()
        {
            var config = new MapperConfiguration(cfg => cfg.CreateMap<LocalMyCompanyDetails, Organizations>());
            _mapper = config.CreateMapper();

            var config1 = new MapperConfiguration(cfg => cfg.CreateMap<OrganizationData, Organizations>());
            _mapper1 = config1.CreateMapper();
        }
        /// <summary>
        /// To get the information specific to the user
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public UserInfo GetUserInfo(Guid UserId)
        {
            using (var context = new EFDbContext())
            {
                var user = context.SystemUsers.Find(UserId);
                var userInfo = new UserInfo()
                {
                    Email = user.Email,
                    FirstName = user.Contacts.Any()
                                           ? user.Contacts.FirstOrDefault().FirstName
                                           : "",
                    LastName =
                        user.Contacts.Any()
                            ? user.Contacts.FirstOrDefault().LastName
                            : "",
                    UserName = user.Contacts.Any() ? user.Contacts.FirstOrDefault().FirstName + ", "
                                + user.Contacts.FirstOrDefault().LastName : user.Email,
                    UserId = user.UserId,
                    OrgType = user.SystemUsersOrganizations.FirstOrDefault().Organizations.OrganizationType,
                    OrgId = user.SystemUsersOrganizations.FirstOrDefault().OrganizationId,
                    OrgName = user.SystemUsersOrganizations.FirstOrDefault().Organizations.Name,
                    Password = user.Password,
                    EmpUserName = user.UserName
                };
                return userInfo;
            }
        }

        /// <summary>
        /// To get the users with specific role
        /// </summary>
        /// <param name="role"></param>
        /// <returns></returns>
        public List<UserInfo> GetUsers(string role)
        {
            using (var context = new EFDbContext())
            {
                var users = context.Organizations.Where(r => r.OrganizationType == role).
                SelectMany(r => r.SystemUsersOrganizations.Select(r1 => r1.SystemUsers)).
                Select(r => new UserInfo()
                {
                    FirstName = r.Contacts.Any() ? r.Contacts.FirstOrDefault().FirstName : ""
                ,
                    LastName = r.Contacts.Any() ? r.Contacts.FirstOrDefault().LastName : "",
                    Email = r.Email,
                    UserId = r.UserId,
                    OrgId = r.SystemUsersOrganizations.FirstOrDefault().OrganizationId,
                    OrgName = r.SystemUsersOrganizations.FirstOrDefault().Organizations.Name
                }).ToList();
                return users;
            }
        }
        /// <summary>
        /// To get the information of user based on email
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public UserInfo GetUserInfo(string email)
        {
            using (var context = new EFDbContext())
            {
                var user = context.SystemUsers.FirstOrDefault(r => r.Email == email);
                if (user != null && user.UserStatus == true)
                {
                    var userInfo = new UserInfo()
                    {
                        Email = user.Email,
                        FirstName = user.Contacts.Any()
                            ? user.Contacts.FirstOrDefault().FirstName
                            : "",
                        LastName =
                            user.Contacts.Any()
                                ? user.Contacts.FirstOrDefault().LastName
                                : "",
                        UserName = user.Contacts.Any() ? user.Contacts.FirstOrDefault().FirstName + ", "
                                    + user.Contacts.FirstOrDefault().LastName : user.Email,
                        UserId = user.UserId,
                        OrgType = user.SystemUsersOrganizations.FirstOrDefault().Organizations.OrganizationType,
                        OrgId = user.SystemUsersOrganizations.FirstOrDefault().OrganizationId,
                        OrgName = user.SystemUsersOrganizations.FirstOrDefault().Organizations.Name
                    };
                    return userInfo;
                }
            }
            return null;
        }
        /// <summary>
        /// To get the specific organization super user
        /// </summary>
        /// <param name="orgId"></param>
        /// <returns></returns>
        public UserInfo GetSuperUserInfo(long orgId)
        {
            using (var context = new EFDbContext())
            {
                var sysUserOrg = context.SystemUsersOrganizations.FirstOrDefault(r => r.PrimaryUser.Value && r.OrganizationId == orgId);
                if (sysUserOrg != null)
                {
                    var user = sysUserOrg.SystemUsers;
                    if (user != null)
                    {
                        var userInfo = new UserInfo()
                        {
                            Email = user.Email,
                            FirstName = user.Contacts.Any()
                                ? user.Contacts.FirstOrDefault().FirstName
                                : "",
                            LastName =
                                user.Contacts.Any()
                                    ? user.Contacts.FirstOrDefault().LastName
                                    : ""
                        };
                        return userInfo;
                    }
                }
            }
            return null;
        }
        /// <summary>
        /// To check the view is related to specific header
        /// </summary>
        /// <param name="headerName"></param>
        /// <param name="sideMenuName"></param>
        /// <param name="ViewName"></param>
        /// <param name="headerMenu"></param>
        /// <returns></returns>
        public bool HasScreenPermission(string headerName, string sideMenuName, string ViewName, List<LocalHeaderMenuModel> headerMenu)
        {
            var ViewPermissions = "";
            try
            {
                if (headerMenu != null && !headerName.Equals(""))
                {
                    headerMenu.ForEach(record => record.isChecked = false);
                    headerMenu.FirstOrDefault(record => record.menuName == headerName).isChecked = true;
                    //if (!sideMenuName.Equals(""))
                    {
                        headerMenu.FirstOrDefault(record => record.menuName == headerName).sideMenus.ForEach(record => record.isChecked = false);
                        headerMenu.FirstOrDefault(record => record.menuName == headerName).sideMenus.FirstOrDefault(record => record.menuName == sideMenuName).isChecked = true;
                        try
                        {
                            ViewPermissions = headerMenu.FirstOrDefault(record => record.menuName == headerName).sideMenus.FirstOrDefault(record => record.menuName == sideMenuName).ViewsAndPermissions.FirstOrDefault(rec => rec.ViewName == ViewName).ViewPermission;
                        }
                        catch
                        {
                            ViewPermissions = ViewName == null ? "NoViewName" : "UnhandledException";
                        }
                        //ViewPermissions = headerMenu.FirstOrDefault(record => record.menuName == headerName).sideMenus.FirstOrDefault(record => record.menuName == sideMenuName).ViewPermission;
                    }
                }
            }
            catch
            {
                // ignored
            }

            if (!(ViewPermissions + "").Contains("R"))
            {
                return false;
            }
            return true;
        }
        /// <summary>
        /// To log the user checkin
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public bool UpdateUserLog(Guid UserId)
        {
            using (var context = new EFDbContext())
            {
                SystemUsers user = context.SystemUsers.Find(UserId);

                user.LastLoginDate = DateTime.Now;

                SystemUserLogs addUserLog = new SystemUserLogs();
                addUserLog.SystemUserId = UserId;
                addUserLog.SystemActionId = 1;
                addUserLog.SystemActionWhen = DateTime.Now;

                context.Entry(addUserLog).State = EntityState.Added;
                context.Entry(user).State = EntityState.Modified;

                context.SaveChanges();
                return true;
            }
        }
        /// <summary>
        /// To check whether the user has permission to access the specific view
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public bool CheckUserCanLoginAsAnyUser(Guid UserId)
        {
            using (var context = new EFDbContext())
            {
                var userRoles =
                    context.SystemUsersOrganizations.FirstOrDefault(row => row.UserId == UserId)
                        .SystemUsersOrganizationsRoles.Select(r => r.SysRoleId).ToList();

                return context.SystemRolesPermissions.Any(
                                row => userRoles.Contains(row.SysRoleId) && row.SysViewsId == 1003);

            }
        }
        /// <summary>
        /// To check whether the user has permission to the specific view
        /// </summary>
        /// <param name="UserId"></param>
        /// <param name="PermissionId"></param>
        /// <returns></returns>
        public bool IsUserHasPermission(Guid UserId, long PermissionId)
        {
            using (var context = new EFDbContext())
            {
                var userRoles =
                    context.SystemUsersOrganizations.Where(row => row.UserId == UserId)
                        .SelectMany(r => r.SystemUsersOrganizationsRoles).Select(r => r.SysRoleId).ToList();

                return context.SystemRolesPermissions.Any(
                                row => userRoles.Contains(row.SysRoleId) && row.SysViewsId == PermissionId);

            }
        }
        /// <summary>
        /// To check whether the specific user belongs to multiclient user
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public bool IsERIUser(Guid UserId)
        {
            using (var context = new EFDbContext())
            {
                try
                {
                    return
                        context.SystemUsersOrganizations.FirstOrDefault(row => row.UserId == UserId).Organizations.OrganizationType == OrganizationType.SuperClient;
                }
                catch { return false; }
                //var userRoles =
                //    context.SystemUsersOrganizations.FirstOrDefault(row => row.UserId == UserId)
                //        .SystemUsersOrganizationsRoles.Select(r => r.SysRoleId).ToList();

                //return context.SystemRolesPermissions.Any(
                //                row => userRoles.Contains(row.SysRoleId) && row.SysViewsId == 1004);

            }
        }
        /// <summary>
        /// Find User using Userid
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        public SystemUsers FindUser(Guid userID)
        {
            using (var context = new EFDbContext())
            {
                return context.SystemUsers.Find(userID);
            }

        }
        /// <summary>
        /// Created By Vasavi
        /// Change Password By User using Userid and Newpassword
        /// </summary>
        /// <param name="modelLocalPassword"></param>
        /// <param name="encrptNewPassword"></param>
        /// <param name="encrptOldPassword"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public void UpdateUserPassword(string encrptNewPassword, Guid userId)
        {
            using (var context = new EFDbContext())
            {
                SystemUsers SysUser_currentUser = FindUser(userId);

                SysUser_currentUser.Password = encrptNewPassword;

                SysUser_currentUser.LastPasswordChangedDate = DateTime.Now;

                context.Entry(SysUser_currentUser).State = EntityState.Modified;
                context.SaveChanges();
            }
        }
        /// <summary>
        /// Get Users using Userid
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        public SystemUsers GetUserAccount(Guid userID)
        {
            using (var context = new EFDbContext())
            {
                return context.SystemUsers.First(m => m.UserId == userID);
            }
        }
        /// <summary>
        /// Get CommpanyOfficer name in Myaccount using userid
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public string GetUserOfficeName(Guid userId)
        {
            using (var context = new EFDbContext())
            {
                return context.SystemUsersOrganizations.FirstOrDefault(record => record.UserId == userId).Organizations.PrincipalCompanyOfficerName;
            }
        }
        /// <summary>
        /// Get ClientBusinessUnits based on Clientid
        /// </summary>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public IEnumerable<ClientBusinessUnits> GetClientBuUnits(long clientId)
        {
            using (var context = new EFDbContext())
            {
                return context.ClientBusinessUnits.ToList().Where(rec => rec.ClientId == clientId);
            }

        }
        /// <summary>
        /// Remove ClientBusinessUnitsites Based on siteid
        /// </summary>
        /// <param name="oldSiteId"></param>
        public void RemoveClientBusinessUnitSiteRepresentatives(long oldSiteId)
        {
            using (var context = new EFDbContext())
            {
                var oldSiteRepresentativeRecord = context.ClientBusinessUnitSiteRepresentatives.FirstOrDefault(record => record.ClientBUSiteID == oldSiteId);

                context.ClientBusinessUnitSiteRepresentatives.Remove(oldSiteRepresentativeRecord);
                context.SaveChanges();
            }
        }
        /// <summary>
        /// Get ClientBusites based on siteid
        /// </summary>
        /// <param name="currentSiteId"></param>
        /// <returns></returns>
        public ClientBusinessUnitSites GetClientBusinessUnitSites(long currentSiteId)
        {
            using (var context = new EFDbContext())
            {
                return context.ClientBusinessUnitSites.FirstOrDefault(record => record.ClientBusinessUnitSiteId == currentSiteId);
            }

        }
        /// <summary>
        /// Get Clientbusites based on Userid
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public List<ClientBusinessUnitSiteRepresentatives> GetClientBuSiteRepresentatives(Guid userId)
        {
            using (var context = new EFDbContext())
            {
                return context.ClientBusinessUnitSiteRepresentatives.Where(record => record.SafetyRepresentative == userId).ToList();
            }
        }
        /// <summary>
        /// Add ClientBusiness unit Site based on Clientbuinessunitid and userid
        /// </summary>
        /// <param name="ClientBusinessUnitSiteId"></param>
        /// <param name="userId"></param>
        public void InsertClientBuSiteRepresentatives(long ClientBusinessUnitSiteId, Guid userId)
        {
            using (var context = new EFDbContext())
            {
                ClientBusinessUnitSiteRepresentatives currentSiteRepresentative = new ClientBusinessUnitSiteRepresentatives();

                currentSiteRepresentative.ClientBUSiteID = ClientBusinessUnitSiteId;
                currentSiteRepresentative.SafetyRepresentative = userId;

                context.ClientBusinessUnitSiteRepresentatives.Add(currentSiteRepresentative);
                context.SaveChanges();
            }
        }
        /// <summary>
        ///  Get ClientBusinessUnits based on Businessunitid
        /// </summary>
        /// <param name="busUnitId"></param>
        /// <returns></returns>
        public ClientBusinessUnits GetClientBusinessUnits(long busUnitId)
        {
            using (var context = new EFDbContext())
            {
                return context.ClientBusinessUnits.FirstOrDefault(record => record.ClientBusinessUnitId == busUnitId);
            }
        }
        public List<ClientBusinessUnitSites> GetClientBusinessUnitSitesList(long busUnitId)
        {
            using (var context = new EFDbContext())
            {
                return context.ClientBusinessUnits.FirstOrDefault(r => r.ClientBusinessUnitId == busUnitId).ClientBusinessUnitSites.ToList();
            }

        }
        /// <summary>
        /// Find Prequalification based on prequalificationid
        /// </summary>
        /// <param name="prequlificationId"></param>
        /// <returns></returns>
        public Prequalification FindPq(long prequlificationId)
        {
            using (var context = new EFDbContext())
            {
                return context.Prequalification.Find(prequlificationId);
            }
        }
        public PrequalificationClient GetPQClients(long prequlificationId)
        {
            using (var context = new EFDbContext())
            {
                return context.PrequalificationClient.FirstOrDefault(r => r.PQId == prequlificationId);
            }
        }
        /// <summary>
        /// Get ClientBusinessUnitSiteIds in prequalification based on prequalificationid
        /// </summary>
        /// <param name="prequlificationId"></param>
        /// <returns></returns>
        public List<long> GetClientBusinessUnitSiteIdsInPq(long prequlificationId)
        {
            using (var context = new EFDbContext())
            {
                return context.PrequalificationSites.Where(rec => rec.PrequalificationId == prequlificationId).Select(rec => rec.ClientBusinessUnitSiteId.Value).ToList();
            }
        }
        /// <summary>
        /// States based on selected regions
        /// </summary>
        /// <param name="selectedRegions"></param>
        /// <returns></returns>
        public List<string> GetStates(IEnumerable<long> selectedRegions)
        {
            using (var context = new EFDbContext())
            {
                var statesList = context.RegionStates.Where(r => selectedRegions.Contains(r.Regionid)).Select(r => r.StateName).ToList();
                if (selectedRegions.Contains(RegionsConstant.INTERNATIONAL))
                {
                    var interNationalRegions = context.ClientBusinessUnitSites.Where(r => !context.RegionStates.Any(r1 => r1.StateName == r.BusinessUnitLocation)).Select(r => r.BusinessUnitLocation).Distinct().ToList();
                    statesList.AddRange(interNationalRegions);
                }
                return statesList;
            }

        }
        /// <summary>
        /// SystemUsersOrganizations based on userid
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public SystemUsersOrganizations GetSystemUsersOrgs(Guid userId)
        {
            using (var context = new EFDbContext())
            {
                return context.SystemUsersOrganizations.FirstOrDefault(rec => rec.UserId == userId);

            }
        }
        /// <summary>
        /// Get Organizations using organizationid
        /// </summary>
        /// <param name="OrgId"></param>
        /// <returns></returns>
        public Organizations GetOrgs(long OrgId)
        {
            using (var context = new EFDbContext())
            {
                return context.Organizations.FirstOrDefault(rec => rec.OrganizationID == OrgId);
            }
        }
        /// <summary>
        /// Get Admin super user Email
        /// </summary>
        /// <returns></returns>
        public string GetAdminSuperUserEmail()
        {
            using (var context = new EFDbContext())
            {
                var adminSuperUserRoleId = context.SystemRoles.FirstOrDefault(record => record.OrganizationType == "Admin" && record.RoleName == "Super User").RoleId;
                return context.SystemUsersOrganizationsRoles.FirstOrDefault(record => record.SysRoleId == adminSuperUserRoleId).SystemUsersOrganizations.SystemUsers.Email;
            }
        }
        /// <summary>
        /// Add OrgDetails in MYaccount using organizationid and userid
        /// </summary>
        /// <param name="OrganizationID"></param>
        /// <param name="userId"></param>
        public void AddArchiveOrgDetails(long OrganizationID, Guid userId)
        {
            using (var context = new EFDbContext())
            {
                var archivedRecordExists = context.ArchiveOrganizationDetails.OrderByDescending(rec => rec.ArchiveOrgId).FirstOrDefault(record => record.OrganizationId == OrganizationID);
                var userOrganizationData = GetOrgs(OrganizationID);
                ArchiveOrganizationDetails archiveVendorData = new ArchiveOrganizationDetails();

                archiveVendorData.ChangedBy = userId;
                archiveVendorData.ChangedDateTime = DateTime.Now;
                archiveVendorData.OrganizationId = userOrganizationData.OrganizationID;
                archiveVendorData.LegalNameOfBusiness = userOrganizationData.Name;
                archiveVendorData.Address1 = userOrganizationData.Address1;
                archiveVendorData.Address2 = userOrganizationData.Address2;
                archiveVendorData.Address3 = userOrganizationData.Address3; // Kiran on 9/19/2014
                archiveVendorData.City = userOrganizationData.City;
                archiveVendorData.State = userOrganizationData.State;
                archiveVendorData.Zip = userOrganizationData.Zip;
                archiveVendorData.Country = userOrganizationData.Country;
                archiveVendorData.PhoneNumber = userOrganizationData.PhoneNumber;
                archiveVendorData.ExtNumber = userOrganizationData.ExtNumber; // Kiran on 8/26/2014
                archiveVendorData.FaxNumber = userOrganizationData.FaxNumber;
                archiveVendorData.WebsiteURL = userOrganizationData.WebsiteURL;
                archiveVendorData.FederalIDNumber = userOrganizationData.FederalIDNumber;
                archiveVendorData.PrincipalCompanyOfficerName = userOrganizationData.PrincipalCompanyOfficerName;
                archiveVendorData.TaxID = userOrganizationData.TaxID;
                archiveVendorData.OrgRepresentativeName = userOrganizationData.OrgRepresentativeName;
                archiveVendorData.OrgRepresentativeEmail = userOrganizationData.OrgRepresentativeEmail;
                archiveVendorData.OrgInfusionSoftContactId = userOrganizationData.OrgInfusionSoftContactId;

                if (archivedRecordExists == null)
                {
                    archiveVendorData.VersionNo = 1;
                }
                else
                {
                    var latestVersionNoOfCurrentOrg = archivedRecordExists.VersionNo;
                    archiveVendorData.VersionNo = latestVersionNoOfCurrentOrg + 1;
                }
                context.Entry(archiveVendorData).State = EntityState.Added;
                context.SaveChanges();
            }
        }
        /// <summary>
        /// Update User Account
        /// </summary>
        /// <param name="form"></param>
        public void UpdateSystemUsersAccount(LocalMyAccountModel form)
        {
            using (var context = new EFDbContext())
            {
                SystemUsers userAccountToUpdate = context.SystemUsers.Find(form.UserId);  //Moved Business Layer By Vasavi

                if (userAccountToUpdate != null && userAccountToUpdate.Contacts.Count == 0)
                    userAccountToUpdate.Contacts.Add(new Contact());
                if (userAccountToUpdate != null)
                {
                    userAccountToUpdate.Contacts[0].UserId = userAccountToUpdate.UserId;

                    userAccountToUpdate.Email = form.Email;
                    userAccountToUpdate.Contacts[0].FirstName = form.FirstName.Trim();
                    userAccountToUpdate.Contacts[0].LastName = form.LastName.Trim();
                    userAccountToUpdate.Contacts[0].ContactTitle = form.ContactTitle;
                    userAccountToUpdate.Contacts[0].PhoneNumber = form.PhoneNumber;
                    userAccountToUpdate.Contacts[0].ExtNumber = form.ExtNumber;
                    userAccountToUpdate.Contacts[0].MobileNumber = form.MobileNumber;
                    userAccountToUpdate.Contacts[0].AdditionalDescription = form.AdditionalDescription;
                    context.Entry(userAccountToUpdate).State = EntityState.Modified;
                    context.SaveChanges();
                }
            }
        }
        /// <summary>
        /// Update User Organization data 
        /// </summary>
        /// <param name="RoleName"></param>
        /// <param name="userOrganizationData"></param>
        /// <param name="form"></param>
        public void UpdateUserOrgnizationdata(string RoleName, Organizations userOrganizationData, LocalMyAccountModel form)
        {
            using (var context = new EFDbContext())
            {
                if (RoleName.Equals("Vendor"))
                {
                    userOrganizationData.PrincipalCompanyOfficerName = form.PrincipalCompanyOfficerName;
                }
                context.Entry(userOrganizationData).State = EntityState.Modified;
                context.SaveChanges();
            }
        }
        /// <summary>
        /// Get User Based on UserName
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        public SystemUsers GetSystemUser(string userName)
        {
            using (var context = new EFDbContext())
            {

                return context.SystemUsers.FirstOrDefault(a => a.UserName == userName);
            }

        }
        /// <summary>
        ///First SystemUsersOrganizations based on userid
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public SystemUsersOrganizations GetSystemUserOrganizations(Guid userId)
        {
            using (var context = new EFDbContext())
            {
                return context.SystemUsersOrganizations.FirstOrDefault(rec => rec.SystemUsers.UserId == userId);

            }
        }
        /// <summary>
        ///  If Password is Wrong update wrongpassword attempts 
        /// </summary>
        /// <param name="varlogMail"></param>
        public void UpdateWrongPasswordAttempts(SystemUsers varlogMail)
        {
            using (var context = new EFDbContext())
            {
                int? FailedCount = varlogMail.FailedPasswordAttemptCount;
                varlogMail.FailedPasswordAttemptWindowStart = DateTime.Now;
                varlogMail.FailedPasswordAttemptCount = FailedCount + 1;
                context.Entry(varlogMail).State = EntityState.Modified;
                context.SaveChanges();

            }
        }
        /// <summary>
        ///  Add SystemUserLogs based on userid
        /// </summary>
        /// <param name="userId"></param>
        public void UpdateSystemUserLogs(Guid userId)
        {
            using (var context = new EFDbContext())
            {
                SystemUserLogs userlog = new SystemUserLogs();
                userlog.SystemUserId = userId;
                userlog.SystemActionId = 1;//Login
                userlog.SystemActionWhen = DateTime.Now;
                context.SystemUserLogs.Add(userlog);
                context.SaveChanges();

            }
        }
        /// <summary>
        /// Get the First User Based on UserName and Password
        /// </summary>
        /// <param name="UserName"></param>
        /// <param name="PassWord"></param>
        /// <returns></returns>
        public SystemUsers GetUser(string UserName, string PassWord)
        {
            using (var context = new EFDbContext())
            {
                return context.SystemUsers.FirstOrDefault(a => a.UserName == UserName && a.Password == PassWord);
            }
        }
        /// <summary>
        /// Update User Login Date
        /// </summary>
        /// <param name="Userid"></param>
        public void UpdateLoginDate(Guid Userid)
        {
            using (var context = new EFDbContext())
            {
                var varlogMail = FindUser(Userid);
                varlogMail.LastLoginDate = DateTime.Now;
                context.Entry(varlogMail).State = EntityState.Modified;
                context.SaveChanges();
            }
        }
        /// <summary>
        /// Get the FirstName of the User
        /// </summary>
        /// <returns></returns>
        public Contact GetName(Guid UserId)
        {
            using (var context = new EFDbContext())
            {
                return context.Contact.FirstOrDefault(m => m.UserId == UserId);
            }
        }
        /// <summary>
        /// Login User Name if Login Success 
        /// </summary>
        /// <param name="varlogMail"></param>
        /// <returns></returns>
        public string LoginUserName(SystemUsers varlogMail)
        {
            using (var context = new EFDbContext())
            {

                var Contacts = context.Contact.FirstOrDefault(r => r.UserId == varlogMail.UserId);
                if (Contacts != null)
                    return Contacts.FirstName + ", " + Contacts.LastName;
                else
                    return varlogMail.UserName;

            }

        }
        /// <summary>
        /// Get Login User Account
        /// </summary>
        /// <param name="guidRecordId"></param>
        /// <returns></returns>
        public LocalMyAccountModel GetAccount(Guid guidRecordId)
        {
            using (var context = new EFDbContext())
            {
                var systemUser_AccountToUpdate = context.SystemUsers.First(m => m.UserId == guidRecordId);          //Move to Business Layer By Vasavi
                                                                                                                    //entityDB.SystemUsers.First(m => m.UserId == guidRecordId);

                //If no contacts exists, add a new empty contact to SystemUser object.
                if (systemUser_AccountToUpdate.Contacts.Count == 0)
                    systemUser_AccountToUpdate.Contacts.Add(new Contact());

                LocalMyAccountModel accToUpdate = new LocalMyAccountModel();

                accToUpdate.UserId = systemUser_AccountToUpdate.UserId;
                accToUpdate.FirstName = systemUser_AccountToUpdate.Contacts[0].FirstName;
                accToUpdate.LastName = systemUser_AccountToUpdate.Contacts[0].LastName;
                accToUpdate.ContactTitle = systemUser_AccountToUpdate.Contacts[0].ContactTitle;
                accToUpdate.PhoneNumber = systemUser_AccountToUpdate.Contacts[0].PhoneNumber;
                accToUpdate.ExtNumber = systemUser_AccountToUpdate.Contacts[0].ExtNumber; // kiran on 8/26/2014
                accToUpdate.MobileNumber = systemUser_AccountToUpdate.Contacts[0].MobileNumber;
                accToUpdate.AdditionalDescription = systemUser_AccountToUpdate.Contacts[0].AdditionalDescription;

                accToUpdate.Email = systemUser_AccountToUpdate.Email;

                accToUpdate.LastLoginDate = systemUser_AccountToUpdate.LastLoginDate;
                accToUpdate.LastPasswordChangedDate = systemUser_AccountToUpdate.LastPasswordChangedDate;

                // Kiran on 3/20/2014
                var userOrgCompanyOfficerName = GetUserOfficeName(guidRecordId);      //Move to Business Layer By Vasavi

                accToUpdate.PrincipalCompanyOfficerName = userOrgCompanyOfficerName;
                // Ends<<<
                var SysUserOrg = systemUser_AccountToUpdate.SystemUsersOrganizations.FirstOrDefault(m => m.SystemUsers.UserId == systemUser_AccountToUpdate.UserId);
                if (SysUserOrg != null)
                {
                    accToUpdate.CompanyName = SysUserOrg.Organizations.Name;
                }
                return accToUpdate;
            }
        }
        /// <summary>
        ///Login User Having First  Financial Analytics Based On OrganizationId
        /// </summary>
        /// <param name="OrganizationID"></param>
        /// <returns></returns>
        public FinancialAnalyticsModel IsUserHasFinancialAnalytics(long OrganizationID)
        {
            using (var context = new EFDbContext())
            {
                return context.FinancialAnalyticsModel.FirstOrDefault(r => r.VendorId == OrganizationID);
            }
        }
        /// <summary>
        /// Check Vendor Has Financial data
        /// </summary>
        /// <param name="OrganizationID"></param>
        /// <returns></returns>
        public bool HasFinancialData(long OrganizationId)
        {
            using (var context = new EFDbContext())
            {
                return context.FinancialAnalyticsModel.Any(r => r.VendorId == OrganizationId);
            }

        }
        /// <summary>
        /// Organizations Based on Taxid
        /// </summary>
        /// <param name="TaxId"></param>
        /// <param name="OrganizationId"></param>
        /// <returns></returns>
        public Organizations OrganizationWithTaxId(string TaxId, long OrganizationId)
        {
            using (var context = new EFDbContext())
            {
                return context.Organizations.FirstOrDefault(record => record.TaxID.Replace("-", "") == TaxId.Replace("-", "") && record.OrganizationID != OrganizationId);
            }

        }
        /// <summary>
        /// Get Organizations with in the SystemuseOrganizations based on Userid
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public Organizations OrgsInUserOrganization(Guid userId)
        {
            using (var context = new EFDbContext())
            {
                return context.SystemUsersOrganizations.FirstOrDefault(rec => rec.SystemUsers.UserId == userId).Organizations;
                //if (Sys_Orgs != null)
                //    return Sys_Orgs.Organizations;
                //else return new Organizations();

            }
        }
        /// <summary>
        /// Company Add Organizations
        /// </summary>
        /// <param name="orgForm"></param>
        /// <param name="varSysUserOrg"></param>
        public void AddOrganizations(LocalMyCompanyDetails orgForm, SystemUsersOrganizations varSysUserOrg)
        {
            using (var context = new EFDbContext())
            {

                Organizations org_data = new Organizations();
                org_data.Name = orgForm.Name;
                org_data.Address1 = orgForm.Address1;
                org_data.Address2 = orgForm.Address2;
                org_data.Address3 = orgForm.Address3; // Kiran on 9/19/2014
                org_data.City = orgForm.City;
                org_data.State = orgForm.State;
                org_data.FinancialMonth = orgForm.FinancialMonth;
                org_data.FinancialDate = orgForm.FinancialDate;
                org_data.Zip = orgForm.Zip;
                org_data.Country = orgForm.Country;
                org_data.PhoneNumber = orgForm.PhoneNumber;
                org_data.ExtNumber = orgForm.ExtNumber; // Kiran on 8/26/2014
                org_data.FaxNumber = orgForm.FaxNumber;
                org_data.WebsiteURL = orgForm.WebsiteURL;
                org_data.FederalIDNumber = orgForm.FederalIDNumber;

                context.Organizations.Add(org_data);
                org_data.CountryType = orgForm.CountryType;
                varSysUserOrg.OrganizationId = org_data.OrganizationID;
                context.Entry(varSysUserOrg).State = EntityState.Modified;
                context.SaveChanges();
            }
        }
        /// <summary>
        ///Update oganization Data of login User Company Details
        /// </summary>
        /// <param name="orgForm"></param>
        /// <param name="org_updata"></param>
        public void UpdateOrganizations(LocalMyCompanyDetails orgForm, Organizations org_updata)
        {
            using (var context = new EFDbContext())
            {
                org_updata.Name = orgForm.Name;
                org_updata.Address1 = orgForm.Address1;
                org_updata.Address2 = orgForm.Address2;
                org_updata.Address3 = orgForm.Address3; // Kiran on 9/19/2014
                org_updata.City = orgForm.City;
                org_updata.State = orgForm.State;
                org_updata.CountryType = orgForm.CountryType;
                org_updata.Zip = orgForm.Zip;
                org_updata.FinancialMonth = orgForm.FinancialMonth;
                org_updata.FinancialDate = orgForm.FinancialDate;
                org_updata.Country = orgForm.Country;
                org_updata.PhoneNumber = orgForm.PhoneNumber;
                org_updata.FaxNumber = orgForm.FaxNumber;
                org_updata.WebsiteURL = orgForm.WebsiteURL;
                org_updata.TaxID = orgForm.TaxID;
                //org_updata.FederalIDNumber = orgForm.FederalIDNumber; // Kiran on 12/24/2014

                context.Entry(org_updata).State = EntityState.Modified;
                context.SaveChanges();
            }
        }
        public void AddArchiveVendorData(LocalMyCompanyDetails orgForm, Guid userId, OrganizationData org)
        {
            using (var context = new EFDbContext())
            {
                long orgId;
                Organizations organization;
                if (orgForm != null)
                {
                    orgId = orgForm.OrganizationID;
                    organization = _mapper.Map<Organizations>(orgForm);
                }
                else
                {
                    orgId = org.OrganizationID;
                    organization = _mapper1.Map<Organizations>(org);
                }

                var archivedRecordExists = context.ArchiveOrganizationDetails.OrderByDescending(rec => rec.ArchiveOrgId).FirstOrDefault(record => record.OrganizationId == orgId);

                ArchiveOrganizationDetails archiveVendorData = new ArchiveOrganizationDetails();

                archiveVendorData.ChangedBy = userId;
                archiveVendorData.ChangedDateTime = DateTime.Now;
                archiveVendorData.OrganizationId = organization.OrganizationID;
                archiveVendorData.LegalNameOfBusiness = organization.Name;
                archiveVendorData.Address1 = organization.Address1;
                archiveVendorData.Address2 = organization.Address2;
                archiveVendorData.Address3 = organization.Address3;
                archiveVendorData.City = organization.City;
                archiveVendorData.State = organization.State;
                archiveVendorData.Zip = organization.Zip;
                archiveVendorData.FinancialMonth = organization.FinancialMonth;
                archiveVendorData.FinancialDate = organization.FinancialDate;
                archiveVendorData.Country = organization.Country;
                archiveVendorData.PhoneNumber = organization.PhoneNumber;
                archiveVendorData.ExtNumber = organization.ExtNumber;
                archiveVendorData.FaxNumber = organization.FaxNumber;
                archiveVendorData.WebsiteURL = organization.WebsiteURL;
                archiveVendorData.FederalIDNumber = organization.FederalIDNumber;
                archiveVendorData.PrincipalCompanyOfficerName = organization.PrincipalCompanyOfficerName;
                archiveVendorData.TaxID = organization.TaxID;
                archiveVendorData.GeographicArea = organization.GeographicArea;
                archiveVendorData.OrgRepresentativeName = organization.OrgRepresentativeName;
                archiveVendorData.OrgRepresentativeEmail = organization.OrgRepresentativeEmail;
                archiveVendorData.OrgInfusionSoftContactId = organization.OrgInfusionSoftContactId;
                if (archivedRecordExists == null)
                {
                    archiveVendorData.VersionNo = 1;
                }
                else
                {
                    var latestVersionNoOfCurrentOrg = archivedRecordExists.VersionNo;
                    archiveVendorData.VersionNo = latestVersionNoOfCurrentOrg + 1;
                }
                context.Entry(archiveVendorData).State = EntityState.Added;
                context.SaveChanges();
            }
        }
        /// <summary>
        /// User Information based on Email
        /// </summary>
        /// <param name="Email"></param>
        /// <returns></returns>
        public SystemUsers GetSystemUserInfo(string Email)
        {
            using (var context = new EFDbContext())
            {
                return context.SystemUsers.FirstOrDefault(m => m.Email == Email);
            }
        }
        /// <summary>
        /// Reset the PassWord Based on Userid
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="PassWord"></param>
        public void ResetPassword(Guid UserID, string PassWord)
        {
            using (var context = new EFDbContext())
            {
                var currentUser = FindUser(UserID);
                currentUser.Password = PassWord;  // 07/04/2013 Suma 
                // Rajesh 07/08/2013 
                // Change last password change date >>>
                currentUser.LastPasswordChangedDate = DateTime.Now;
                // Change Last password change date Ends <<<

                context.Entry(currentUser).State = EntityState.Modified;
                context.SaveChanges();
            }
        }
        /// <summary>
        /// Get Organizations Based On The Current UserOrganizationId
        /// </summary>
        /// <param name="currentUserOrgId"></param>
        /// <returns></returns>
        public List<KeyValuePair<string, long>> GetOrganizations(long currentUserOrgId)
        {
            using (var context = new EFDbContext())
            {
                return (from orgs in context.Organizations

                        where orgs.OrganizationID.Equals(currentUserOrgId)

                        select new { orgs.Name, orgs.OrganizationID }).ToList().Select(r => new KeyValuePair<string, long>(r.Name, r.OrganizationID)).ToList();

            }
        }
        /// <summary>
        /// Add New User into SystemUserOrganizations and Roles
        /// </summary>
        /// <param name="form"></param>
        /// <param name="PassWord"></param>
        /// <returns></returns>
        public LocalNewUserContactDetails AddNewUser(LocalAddNewUser form, string PassWord)
        {
            using (var context = new EFDbContext())
            {
                SystemApplicationId sysAppId = context.SystemApplicationId.FirstOrDefault();
                SystemUsersOrganizations sysUserOrg_currentUser = new SystemUsersOrganizations();
                LocalNewUserContactDetails local = new LocalNewUserContactDetails();

                sysUserOrg_currentUser.SystemUsers = new SystemUsers();

                //string strPassword = Utilities.EncryptionDecryption.EncryptData(form.Password);

                sysUserOrg_currentUser.SystemUsers.ApplicationId = sysAppId.ApplicationId;
                sysUserOrg_currentUser.SystemUsers.Email = form.Email;
                sysUserOrg_currentUser.SystemUsers.Password = PassWord;
                sysUserOrg_currentUser.SystemUsers.UserName = form.Email;
                sysUserOrg_currentUser.SystemUsers.CreateDate = DateTime.Now;
                sysUserOrg_currentUser.SystemUsers.LastLoginDate = DateTime.Now;
                sysUserOrg_currentUser.SystemUsers.UserStatus = true;

                sysUserOrg_currentUser.OrganizationId = form.OrganizationId;
                context.SystemUsers.Add(sysUserOrg_currentUser.SystemUsers);
                context.SystemUsersOrganizations.Add(sysUserOrg_currentUser);

                var userRecordInSysUserOrg = context.SystemUsersOrganizations.FirstOrDefault(record => record.OrganizationId == form.OrganizationId);
                if (userRecordInSysUserOrg == null)
                {
                    var orgType = context.Organizations.FirstOrDefault(record => record.OrganizationID == form.OrganizationId).OrganizationType;
                    var currentOrgSuperUserRoleId = context.SystemRoles.FirstOrDefault(record => record.OrganizationType == orgType && record.RoleName == "Super User").RoleId;
                    SystemUsersOrganizationsRoles currentUserRole = new SystemUsersOrganizationsRoles();
                    currentUserRole.SysRoleId = currentOrgSuperUserRoleId;
                    local.isSuperUser = true;
                    sysUserOrg_currentUser.PrimaryUser = true;
                    context.SystemUsersOrganizationsRoles.Add(currentUserRole);
                }

                context.SaveChanges();

                SystemUsers sysUser_currentUserData = GetSystemUserInfo(form.Email);

                local.userId = sysUser_currentUserData.UserId;
                local.UserSearchEdit = false;
                return local;
            }
        }
        /// <summary>
        /// Get All Organizations Based On Organizationtype
        /// </summary>
        /// <param name="OrganizationType"></param>
        /// <returns></returns>
        public List<Organizations> OrganizationsList(string OrganizationType)
        {
            using (var context = new EFDbContext())
            {
                var orgs = context.Organizations.Where(Rec => Rec.OrganizationType == OrganizationType).OrderBy(rec => rec.Name).ToList();
                //if(OrganizationType== LocalConstants.Client)
                //{ orgs.Where(r => r.ShowInApplication == true); }
                return orgs;

            }

        }
        /// <summary>
        /// Organizations in the systemuserorganizations based on userid
        /// </summary>
        /// <param name="currentUserId"></param>
        /// <returns></returns>
        public List<KeyValuePair<string, long>> GetOrganizationsInSystemUserOrgs(Guid currentUserId)
        {

            using (var context = new EFDbContext())
            {
                return (from userorgs in context.SystemUsersOrganizations

                        where userorgs.UserId.Equals(currentUserId)

                        select new { userorgs.Organizations.Name, userorgs.Organizations.OrganizationID }).ToList().Select(r => new KeyValuePair<string, long>(r.Name, r.OrganizationID)).ToList();

            }
        }
        /// <summary>
        /// Get User Organizations Based On UserorgId
        /// </summary>
        /// <param name="userOrgID"></param>
        /// <returns></returns>
        public SystemUsersOrganizations FindUserOrgs(long userOrgID)
        {
            using (var context = new EFDbContext())
            {

                return context.SystemUsersOrganizations.Find(userOrgID);

            }
        }
        /// <summary>
        /// Get User Organizations Roles Based on UserOrganizationid
        /// </summary>
        /// <param name="userOrgID"></param>
        /// <returns></returns>
        public List<SystemUsersOrganizationsRoles> GetUserOrganizationRoles(long userOrgID)
        {
            using (var context = new EFDbContext())
            {

                return context.SystemUsersOrganizationsRoles.Where(rec => rec.SysUserOrgId == userOrgID).ToList();

            }
        }
        /// <summary>
        /// Get UserORganizations Based on OrganizationId and UserId
        /// </summary>
        /// <param name="OrganizationId"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public SystemUsersOrganizations UserOrganizations(long OrganizationId, Guid UserId)
        {
            using (var context = new EFDbContext())
            {

                return context.SystemUsersOrganizations.FirstOrDefault(m => m.OrganizationId == OrganizationId && m.UserId == UserId);

            }

        }
        /// <summary>
        /// save all the roles selected selected by user
        /// </summary>
        /// <param name="form"></param>
        public void SaveOrganizationRoles(LocalCompaniesList form)
        {
            using (var context = new EFDbContext())
            {
                SystemUsersOrganizations sysUserOrg_currentUser = new SystemUsersOrganizations();

                if (form.RolesList.Count > 0)
                {
                    for (int i = 0; i < form.RolesList.Count; i++)
                    {
                        if (form.Roles_CheckBoxStatus[i] == true)
                        {
                            SystemUsersOrganizationsRoles sysUserOrgRoles_currentUser = new SystemUsersOrganizationsRoles();
                            sysUserOrgRoles_currentUser.SysRoleId = form.RolesList[i].RoleId;
                            context.SystemUsersOrganizationsRoles.Add(sysUserOrgRoles_currentUser);
                        }
                    }
                }
                sysUserOrg_currentUser.UserId = form.userId;
                sysUserOrg_currentUser.OrganizationId = Convert.ToInt64(form.OrganizationId);

                context.SystemUsersOrganizations.Add(sysUserOrg_currentUser);
                context.SaveChanges();

            }
        }
        //public IEnumerable<SystemRoles> GetSystemRoles(string OrgType)
        //{
        //    using (var context = new EFDbContext())
        //    {
        //        return context.SystemRoles.Where(record => record.OrganizationType == OrgType && record.RoleName != "employee training");
        //    }
        //}
        /// <summary>
        /// Get UserRoles Except SuperUserRole
        /// </summary>
        /// <param name="OrgType"></param>
        /// <returns></returns>
        public List<SystemRoles> GetRoles(string OrgType)
        {
            using (var context = new EFDbContext())
            {
                var EmployeeRoles = EmployeeRole.VendorEmpRoleId;
                return context.SystemRoles.Where(r => r.OrganizationType == OrgType && !EmployeeRoles.Contains(r.RoleId) && r.RoleName.ToLower() != "super user").ToList();
                //.Where(record => record. .org.OrganizationType == OrgType && record.RoleName != "employee training"&& record=>record.RoleName.ToLower() != "super user").ToList();
            }

        }

        /// <summary>
        /// remove the old organization roles selected by user based on oldorganizationid
        /// </summary>
        /// <param name="oldOrgId"></param>
        public void RemoveOldOrganizationRoles(long oldOrgId)
        {
            using (var context = new EFDbContext())
            {
                var sysUserOrgRoles_currentUser = context.SystemUsersOrganizationsRoles.Where(roles => roles.SysUserOrgId == oldOrgId).ToList();
                foreach (var role in sysUserOrgRoles_currentUser)
                {
                    context.SystemUsersOrganizationsRoles.Remove(role);
                }
                context.SaveChanges();
            }

        }
        public void UpdateSelectedUserCompanyAccess(LocalCompaniesList form)
        {
            using (var context = new EFDbContext())
            {
                SystemUsersOrganizations sysuserOrgOld_currentUser = context.SystemUsersOrganizations.Find(form.oldOrgId);


                sysuserOrgOld_currentUser.SystemUsersOrganizationsRoles = new List<SystemUsersOrganizationsRoles>();



                // ============ To Update the User selected new Organization & respective roles============= //
                for (int i = 0; i < form.RolesList.Count; i++)
                {
                    if (form.Roles_CheckBoxStatus[i] == true)
                    {
                        SystemUsersOrganizationsRoles sysUserOrgRoles = new SystemUsersOrganizationsRoles();
                        sysUserOrgRoles.SysRoleId = form.RolesList[i].RoleId;
                        sysuserOrgOld_currentUser.SystemUsersOrganizationsRoles.Add(sysUserOrgRoles);
                    }
                }

                sysuserOrgOld_currentUser.UserId = form.userId;
                sysuserOrgOld_currentUser.OrganizationId = Convert.ToInt64(form.OrganizationId);
                var UserId = form.userId;
                //entityDB.SystemUsersOrganizations.Add(sysuserOrg_currentUser);
                context.SaveChanges();
            }
        }
        /// <summary>
        /// Get Superuser Roleid based on login User and  Organization Type
        /// </summary>
        /// <param name="currentUser_OrgType"></param>
        /// <returns></returns>
        public string SuperUserRoleId(string currentUser_OrgType)
        {
            using (var context = new EFDbContext())
            {
                return context.SystemRoles.FirstOrDefault(record => record.RoleName == "Super User" && record.OrganizationType == currentUser_OrgType).RoleId;
            }
        }
        /// <summary>
        /// Check Current User Is superUser
        /// </summary>
        /// <param name="currUserSysUserOrgId"></param>
        /// <param name="superUserRoleId"></param>
        /// <returns></returns>
        public bool isSuperUser(long currUserSysUserOrgId, string superUserRoleId)
        {
            using (var context = new EFDbContext())
            {
                return context.SystemUsersOrganizationsRoles.Any(record => record.SysUserOrgId == currUserSysUserOrgId && record.SysRoleId == superUserRoleId);

            }

        }
        /// <summary>
        /// Get User Details From Contact Based On UserId
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public List<Contact> UserContacts(Guid userId)
        {
            using (var context = new EFDbContext())
            {
                return context.SystemUsers.Find(userId).Contacts.ToList();

            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Email"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public SystemUsers emailExists(string Email, Guid userId)
        {
            using (var context = new EFDbContext())
            {
                return context.SystemUsers.FirstOrDefault(m => m.Email == Email && m.UserId != userId);

            }
        }
        /// <summary>
        /// Update The User Details
        /// </summary>
        /// <param name="local"></param>
        /// <param name="NewPassword"></param>
        public void UpdateNewUserContactDetails(LocalNewUserContactDetails local)
        {
            using (var context = new EFDbContext())
            {
                SystemUsers sysUser_currentUser = context.SystemUsers.Find(local.userId);
                if (local.Email.Trim() != sysUser_currentUser.Email.Trim())
                {
                    try
                    {
                        var invitation_CurrentEmail = context.VendorInviteDetails.FirstOrDefault(rec => rec.VendorEmail == sysUser_currentUser.Email.Trim());
                        invitation_CurrentEmail.VendorEmail = local.Email.Trim();
                        context.Entry(invitation_CurrentEmail).State = EntityState.Modified;
                    }
                    catch
                    { }
                }
                // Ends<<<
                if (local.currentUser_OrgType.ToLower().Equals("client"))
                {
                    var userBUSite = context.SystemUsersBUSites.FirstOrDefault(row => row.SystemUserId == local.userId);
                    if (userBUSite == null)
                        userBUSite = new SystemUsersBUSites();

                    userBUSite.SystemUserId = local.userId;

                    userBUSite.BUId = local.ClientBU != null ? string.Join(",", local.ClientBU.ToArray()) : "";
                    userBUSite.BUSiteId = local.ClientBUSite != null ? string.Join(",", local.ClientBUSite.ToArray()) : "";

                    if (userBUSite.SystemUserBUSiteId == 0)
                        context.Entry(userBUSite).State = EntityState.Added;
                    else
                        context.Entry(userBUSite).State = EntityState.Modified;

                    var UserDepartment = context.UserDepartments.FirstOrDefault(r => r.UserId == local.userId);
                    if (UserDepartment == null)
                    {
                        if (local.DepartmentId != -1)
                        {
                            UserDepartments Dep = new UserDepartments();
                            Dep.DepartmentId = local.DepartmentId;
                            Dep.UserId = local.userId;
                            context.UserDepartments.Add(Dep);
                        }
                    }
                    else
                    {
                        UserDepartment.DepartmentId = local.DepartmentId;
                        context.Entry(UserDepartment).State = EntityState.Modified;

                    }
                }
                if (local.UserStatus == false)
                {
                    var userConfiguredNotifications = sysUser_currentUser.TemplateSubSectionsNotifications.Where(rec => rec.NotifyUserDisabled == false).ToList();
                    foreach (var configuredNotification in userConfiguredNotifications)
                    {
                        configuredNotification.NotifyUserDisabled = true;
                        context.Entry(configuredNotification).State = EntityState.Modified;
                    }
                }
                sysUser_currentUser.UserStatus = local.UserStatus;
                // string strPassword = NewPassword;// Utilities.EncryptionDecryption.EncryptData(local.Password);
                //   sysUser_currentUser.Password = strPassword;
                sysUser_currentUser.Email = local.Email;
                sysUser_currentUser.UserName = local.Email;
                if (sysUser_currentUser.Contacts.Count == 0)
                {
                    sysUser_currentUser.Contacts = new List<Contact>();
                    sysUser_currentUser.Contacts.Add(new Contact());
                }
                sysUser_currentUser.Contacts[0].UserId = local.userId;
                sysUser_currentUser.Contacts[0].FirstName = local.FirstName.Trim();
                sysUser_currentUser.Contacts[0].LastName = local.LastName.Trim();
                sysUser_currentUser.Contacts[0].ContactTitle = local.ContactTitle;
                sysUser_currentUser.Contacts[0].PhoneNumber = local.PhoneNumber;
                sysUser_currentUser.Contacts[0].PhoneNumberInternational = local.PhoneNumberInternational;
                sysUser_currentUser.Contacts[0].ExtNumber = local.ExtNumber; // Kiran on 8/26/2014
                sysUser_currentUser.Contacts[0].AdditionalDescription = local.AdditionalDescription;

                context.Entry(sysUser_currentUser).State = EntityState.Modified;
                context.SaveChanges();


            }
        }
        public UserDepartments GetUserDepartment(Guid UserId)
        {
            using (var context = new EFDbContext())
            {
                return context.UserDepartments.FirstOrDefault(r => r.UserId == UserId);
            }
        }
        public SystemUsersBUSites UserBuSites(Guid userId)
        {
            using (var context = new EFDbContext())
            {
                return context.SystemUsersBUSites.FirstOrDefault(row => row.SystemUserId == userId);
            }
        }
        public string UpdateUser(Guid NewUserId, Guid CurrentuserId, bool Isdelete)
        {
            using (var context = new EFDbContext())
            {
                var prequalification = context.Prequalification.Where(r => r.UserIdAsCompleter == CurrentuserId || r.UserIdAsSigner == CurrentuserId || r.InvitationSentByUser == CurrentuserId || r.UserIdLoggedIn == CurrentuserId || r.UnlockedBy == CurrentuserId);
                if (prequalification != null)
                {
                    foreach (var currentpq in prequalification)
                    {
                        if (currentpq.UserIdAsCompleter == CurrentuserId) currentpq.UserIdAsCompleter = NewUserId;
                        else if (currentpq.UserIdAsSigner == CurrentuserId) currentpq.UserIdAsSigner = NewUserId;

                        else if (currentpq.UserIdLoggedIn == CurrentuserId) currentpq.UserIdLoggedIn = NewUserId;

                        else if (currentpq.UnlockedBy == CurrentuserId) currentpq.UnlockedBy = NewUserId;
                        else currentpq.InvitationSentByUser = NewUserId;
                        context.Entry(currentpq).State = EntityState.Modified;
                    }
                }
                var aggrementSubsection = context.AgreementSubSectionLog.Where(r => r.SentBy == CurrentuserId);
                if (aggrementSubsection != null)
                {
                    foreach (var Aggrement in aggrementSubsection)
                    {
                        Aggrement.SentBy = NewUserId;
                        context.Entry(Aggrement).State = EntityState.Modified;

                    }

                }
                var ArchiveOrgs = context.ArchiveOrganizationDetails.Where(r => r.ChangedBy == CurrentuserId);
                if (ArchiveOrgs != null)
                {
                    foreach (var Archiveorg in ArchiveOrgs)
                    {
                        Archiveorg.ChangedBy = NewUserId;
                        context.Entry(Archiveorg).State = EntityState.Modified;
                    }
                }



                var pqStatusLog = context.PrequalificationStatusChangeLog.Where(r => r.StatusChangedByUser == CurrentuserId);
                if (pqStatusLog != null)
                {
                    foreach (var PqStatus in pqStatusLog)
                    {
                        PqStatus.StatusChangedByUser = NewUserId;
                        context.Entry(PqStatus).State = EntityState.Modified;
                    }
                }
                var TsubSecnotification = context.TemplateSubSectionsNotifications.Where(r => r.RecipientUserId == CurrentuserId);
                if (TsubSecnotification != null)
                {
                    foreach (var Tsubsecnotify in TsubSecnotification)
                    {
                        Tsubsecnotify.RecipientUserId = NewUserId;
                        context.Entry(Tsubsecnotify).State = EntityState.Modified;
                    }
                }
                var UserorgId = context.SystemUsersOrganizations.FirstOrDefault(r => r.UserId == CurrentuserId).OrganizationId;
                var orgnotifications = context.OrganizationsEmailSetupNotifications.Where(r => r.OrganizationId == UserorgId && !string.IsNullOrEmpty(r.SendNotificationTo)).ToList();
                foreach (var notify in orgnotifications)
                {
                    var notifications = notify.SendNotificationTo.Split(',');
                    notifications = notifications.Select(s => s.Replace(CurrentuserId.ToString(), NewUserId.ToString())).ToArray();

                    notify.SendNotificationTo = string.Join(",", notifications);
                    context.Entry(notify).State = EntityState.Modified;
                }

                var Docs = context.Document.Where(r => r.StatusChangedBy == CurrentuserId || r.SystemUserAsUploader == CurrentuserId || r.UpdatedBy == CurrentuserId);
                if (Docs != null)
                {
                    foreach (var Doc in Docs)
                    {
                        if (Doc.StatusChangedBy == CurrentuserId) Doc.StatusChangedBy = NewUserId;
                        else if (Doc.SystemUserAsUploader == CurrentuserId) Doc.SystemUserAsUploader = NewUserId;
                        else Doc.UpdatedBy = NewUserId;
                        context.Entry(Doc).State = EntityState.Modified;
                    }
                }
                var Doclogs = context.DocumentLogs.Where(r => r.StatusChangedBy == CurrentuserId || r.SystemUserAsUploader == CurrentuserId || r.UpdatedBy == CurrentuserId || r.ModifiedBy == CurrentuserId);
                if (Doclogs != null)
                {
                    foreach (var Doc in Doclogs)
                    {
                        if (Doc.StatusChangedBy == CurrentuserId) Doc.StatusChangedBy = NewUserId;
                        else if (Doc.SystemUserAsUploader == CurrentuserId) Doc.SystemUserAsUploader = NewUserId;
                        else if (Doc.UpdatedBy == CurrentuserId) Doc.UpdatedBy = NewUserId;
                        else Doc.ModifiedBy = NewUserId;
                        context.Entry(Doc).State = EntityState.Modified;
                    }
                }
                var Invitation = context.VendorInviteDetails.Where(r => r.InvitationSentByUser == CurrentuserId);
                if (Invitation != null)
                {
                    foreach (var Invite in Invitation)
                    {
                        Invite.InvitationSentByUser = NewUserId;
                        context.Entry(Invite).State = EntityState.Modified;
                    }
                }
                var VendorEmailLog = context.VendorEmailLog.Where(r => r.UserId == CurrentuserId);
                if (VendorEmailLog != null)
                {
                    foreach (var vemail in VendorEmailLog)
                    {
                        vemail.UserId = NewUserId;
                        context.Entry(vemail).State = EntityState.Modified;
                    }
                }
                var tsubsec = context.TemplateSubSectionsNotifications.Where(r => r.RecipientUserId == CurrentuserId);
                if (tsubsec != null)
                {
                    foreach (var tsec in tsubsec)
                    {
                        tsec.RecipientUserId = NewUserId;
                        context.Entry(tsec).State = EntityState.Modified;
                    }
                }
                var templogs = context.TemplateLog.Where(r => r.ChangedBy == CurrentuserId);
                if (templogs != null)
                {
                    foreach (var templog in templogs)
                    {
                        templog.ChangedBy = NewUserId;
                        context.Entry(templog).State = EntityState.Modified;
                    }
                }
                var orgnotify = context.OrganizationsNotificationsEmailLogs.Where(r => r.MailSentBy == CurrentuserId);
                if (tsubsec != null)
                {
                    foreach (var org in orgnotify)
                    {
                        org.MailSentBy = NewUserId;
                        context.Entry(org).State = EntityState.Modified;
                    }
                }
                var vendorReview = context.VendorReviewInput.Where(r => r.InputUserId == CurrentuserId);
                if (vendorReview != null)
                {
                    foreach (var vreview in vendorReview)
                    {
                        vreview.InputUserId = NewUserId;
                        context.Entry(vreview).State = EntityState.Modified;
                    }
                }
                var LocationStatusLog = context.LocationStatusLog.Where(r => r.StatusChangedBy == CurrentuserId);
                if (LocationStatusLog != null)
                {
                    foreach (var LocStatuslog in LocationStatusLog)
                    {
                        LocStatuslog.StatusChangedBy = NewUserId;
                        context.Entry(LocStatuslog).State = EntityState.Modified;
                    }
                }
                var OrganizationsNotificationsEmailLogs = context.OrganizationsNotificationsEmailLogs.Where(r => r.MailSentBy == CurrentuserId);
                if (OrganizationsNotificationsEmailLogs != null)
                {
                    foreach (var OrgNotifyEmailLog in OrganizationsNotificationsEmailLogs)
                    {
                        OrgNotifyEmailLog.MailSentBy = NewUserId;
                        context.Entry(OrganizationsNotificationsEmailLogs).State = EntityState.Modified;
                    }
                }
                var PQComments = context.PrequalificationComments.Where(r => r.CommentsBy == CurrentuserId);
                if (PQComments != null)
                {
                    foreach (var PqComment in PQComments)
                    {
                        PqComment.CommentsBy = NewUserId;
                        context.Entry(PqComment).State = EntityState.Modified;
                    }
                }
                var TSubSecUserPermissions = context.TemplateSubSectionsUserPermissions.Where(r => r.UserId == CurrentuserId);
                if (TSubSecUserPermissions != null)
                {
                    foreach (var TSubSecUserPermission in TSubSecUserPermissions)
                    {
                        TSubSecUserPermission.UserId = NewUserId;
                        context.Entry(TSubSecUserPermission).State = EntityState.Modified;
                    }
                }
                var AgreementSubSectionConfiguration = context.AgreementSubSectionConfiguration.Where(R => R.CreatedBy == CurrentuserId || R.UpdatedBy == CurrentuserId);
                if (AgreementSubSectionConfiguration != null)
                {
                    foreach (var AgreementSubSec in AgreementSubSectionConfiguration)
                    {
                        if (AgreementSubSec.CreatedBy == CurrentuserId) AgreementSubSec.CreatedBy = NewUserId;
                        else
                            AgreementSubSec.UpdatedBy = NewUserId;
                        context.Entry(AgreementSubSec).State = EntityState.Modified;
                    }
                }
                var AgreementSubSectionLogs = context.AgreementSubSectionLog.Where(R => R.SentBy == CurrentuserId);

                if (AgreementSubSectionLogs != null)
                {
                    foreach (var AgreementSubSectionLog in AgreementSubSectionLogs)
                    {
                        AgreementSubSectionLog.SentBy = NewUserId;

                        context.Entry(AgreementSubSectionLog).State = EntityState.Modified;
                    }
                }
                var ClientNotifications = context.ClientNotifications.Where(r => r.UserId.Contains(CurrentuserId.ToString()));
                if (ClientNotifications != null)
                {
                    foreach (var CNotification in ClientNotifications)
                    {
                        var notifications = CNotification.UserId.Split(',');
                        notifications = notifications.Select(s => s.Replace(CurrentuserId.ToString(), NewUserId.ToString())).ToArray();

                        CNotification.UserId = string.Join(",", notifications);
                        context.Entry(CNotification).State = EntityState.Modified;
                    }
                }

                try
                {
                    context.SaveChanges();
                    if (Isdelete)
                    {
                        var Data = DeleteUser(CurrentuserId);
                        return Data;
                    }
                    //else
                    //{
                    //  var  sysUser_currentUser = context.SystemUsers.Find(CurrentuserId);
                    //    sysUser_currentUser.UserStatus = false;
                    //    context.Entry(sysUser_currentUser).State = EntityState.Modified;

                    //    context.SaveChanges();
                    //}
                    return "updated Successfully";
                }
                catch (Exception e)
                {
                    var message = e.Message;
                    return "Error";
                }
            }
        }
        /// <summary>
        /// Delete The Selected USer Based on UserId 
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public string DeleteUser(Guid UserId)
        {
            using (var context = new EFDbContext())
            {
                LocalNewUserContactDetails form = new LocalNewUserContactDetails();
                LocalCompaniesList local = new LocalCompaniesList();

                form.userId = UserId;

                // Changed code on 2/7/2014 by kiran
                var sysUserOrgs_currentUser = context.SystemUsersOrganizations.FirstOrDefault(m => m.UserId == form.userId);

                SystemUsers sysUser_currentUser = context.SystemUsers.Find(UserId);

                var sysUserOrgRoles_currentUser = context.SystemUsersOrganizationsRoles.Where(roles => roles.SysUserOrgId == sysUserOrgs_currentUser.SysUserOrganizationId).ToList();

                foreach (var sysUserOrgRole in sysUserOrgRoles_currentUser)
                {
                    context.SystemUsersOrganizationsRoles.Remove(sysUserOrgRole);
                }

                context.SystemUsersOrganizations.Remove(sysUserOrgs_currentUser);

                // Ends<<<

                if (sysUser_currentUser.Contacts.Count != 0)
                {
                    context.Contact.Remove(sysUser_currentUser.Contacts[0]);
                }

                context.SystemUsers.Remove(sysUser_currentUser);

                var currentUser_logs = context.SystemUserLogs.Where(record => record.SystemUserId == UserId);

                foreach (var currentUserLog in currentUser_logs)
                {
                    context.SystemUserLogs.Remove(currentUserLog);
                }

                var currentUser_SiteRepresentatives = context.ClientBusinessUnitSiteRepresentatives.Where(record => record.SafetyRepresentative == UserId);

                if (currentUser_SiteRepresentatives != null)
                {
                    foreach (var siteRepresentative in currentUser_SiteRepresentatives)
                    {
                        context.ClientBusinessUnitSiteRepresentatives.Remove(siteRepresentative);

                    }
                }
                var CurrentUser_Busites = context.SystemUsersBUSites.Where(r => r.SystemUserId == UserId);
                if (CurrentUser_Busites != null)
                {
                    foreach (var Busite in CurrentUser_Busites)
                    {
                        context.SystemUsersBUSites.Remove(Busite);
                    }
                }
                var UserDepartments = context.UserDepartments.Where(r => r.UserId == UserId);
                if (UserDepartments != null)
                {
                    foreach (var UserDepartment in UserDepartments)
                    {
                        context.UserDepartments.Remove(UserDepartment);
                    }
                }

                try
                {
                    context.SaveChanges();
                    return "Success";
                }
                catch (Exception e)
                {
                    var Message = e.Message;
                    sysUser_currentUser = context.SystemUsers.Find(UserId);
                    sysUser_currentUser.UserStatus = false;
                    context.Entry(sysUser_currentUser).State = EntityState.Modified;

                    context.SaveChanges();
                    return "Error";
                }
            }
        }
        /// <summary>
        /// Get UserORganizationData Based On Userid
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public List<SystemUsersOrganizations> userOrganizationsList(Guid UserId)
        {
            using (var context = new EFDbContext())
            {
                return context.SystemUsersOrganizations.Where(row => row.UserId == UserId).ToList();
            }
        }
        public DataSourceResponse UsersList(bool searchType, long SuperClientId, string fname, string lname, string email, bool userStatus_Active, bool userStatus_InActive, long clientId, long vendorId, long adminId, DataSourceRequest request)
        {
            using (var context = new EFDbContext())
            {
                var emptrainingRoleId = EmployeeRole.VendorEmpRoleId;
                // var emptrainingRoleId = context.SystemRoles.FirstOrDefault(rec => rec.RoleName == LocalConstants.Employee_Traning).RoleId;
                var sysUserOrgIds = context.SystemUsersOrganizationsRoles.Where(rec => rec.SysRoleId == emptrainingRoleId).Select(rec => rec.SysUserOrgId).ToList();
                //phani on 1-08-2015
                IQueryable<SystemUsersOrganizations> usersList = null;

                // Ends<<<

                // ======================== For Normal User Search ============================= //

                if (searchType == true)
                {

                    usersList = context.SystemUsers.Where(r => (string.IsNullOrEmpty(email) || r.Email.Contains(email)) &&
                    (string.IsNullOrEmpty(fname) || r.Contacts.Any(r1 => r1.FirstName == fname)) &&
                    (string.IsNullOrEmpty(lname) || r.Contacts.Any(r1 => r1.LastName == lname))
                    ).SelectMany(r => r.SystemUsersOrganizations);
                    //.Where(rec => !sysUserOrgIds.Contains(rec.SysUserOrganizationId));
                    //ViewBag.Count = ViewBag.VBsysUserOrgsList.Count; // Kiran on 2/7/2014
                }

                // ======================== For Advanced User Search ============================= //
                else
                {
                    bool? temp = null;
                    var sysUserOrgsList = context.SystemUsersOrganizations.AsQueryable(); //Wrong

                    if (userStatus_Active == true)
                    {
                        temp = true;
                    }
                    if (userStatus_InActive == true)
                    {
                        temp = false;
                    }

                    // Kiran on 12/9/2013
                    if (userStatus_Active == true || userStatus_InActive == true)
                    {
                        sysUserOrgsList = sysUserOrgsList.Where(rec => rec.SystemUsers.UserStatus == temp);

                    }
                    if (adminId != -1 || clientId != -1 ||
                        vendorId != -1 || SuperClientId != 0)
                    {

                        sysUserOrgsList = sysUserOrgsList
                            .Where(sysUserOrg => (adminId != -1 && sysUserOrg.OrganizationId == adminId)

                            || (clientId != -1 && sysUserOrg.OrganizationId == clientId) ||
                            (vendorId != -1 && sysUserOrg.OrganizationId == vendorId) ||
                            (SuperClientId != 0 && sysUserOrg.OrganizationId == SuperClientId
                            ));
                    }
                    //else if ((adminId != -1 && temp == null) || 
                    //    (clientId != -1 && temp == null) || 
                    //    (vendorId != -1 && temp == null) || 
                    //    (SuperClientId != 0 && temp == null))
                    //{
                    //    sysUserOrgsList = context.SystemUsersOrganizations.Where(sysUserOrg => (adminId != -1 && sysUserOrg.OrganizationId == adminId) 
                    //    || (clientId != -1 && sysUserOrg.OrganizationId == clientId) 
                    //    || (vendorId != -1 && sysUserOrg.OrganizationId == vendorId) || 
                    //    (SuperClientId != 0 && sysUserOrg.OrganizationId == SuperClientId));
                    //}
                    // Ends<<<

                    //if ((userStatus_Active == true && userStatus_InActive == true) || 
                    //    (userStatus_Active == false && userStatus_InActive == false && clientId == -1 && vendorId == -1 
                    //    && adminId == -1 && SuperClientId == 0))
                    //{
                    //    sysUserOrgsList = context.SystemUsersOrganizations;

                    //}


                    usersList = sysUserOrgsList;

                    //ViewBag.Count = ViewBag.VBsysUserOrgsList.Count;
                }
                var finalUserDetails = usersList.ToList().OrderBy(r => r.SystemUsers.Contacts.Count() != 0 ? (r.SystemUsers.Contacts[0].LastName + ", " + r.SystemUsers.Contacts[0].FirstName) : r.SystemUsers.Email)
                        .Where(rec => !sysUserOrgIds.Contains(rec.SysUserOrganizationId));
                var TotalCount = finalUserDetails.Count();
                //phani on 1-08-2015
                List<LocalUsersList> finalUsersList = new List<LocalUsersList>();
                foreach (var user in finalUserDetails.Skip((request.PageSize * (request.Page - 1))).Take(request.PageSize).ToList())
                {
                    LocalUsersList userDetails = new LocalUsersList();

                    if (user.SystemUsers.Contacts.Count() != 0)
                        userDetails.Name = user.SystemUsers.Contacts[0].LastName + ", " + user.SystemUsers.Contacts[0].FirstName;
                    else
                        userDetails.Name = user.SystemUsers.Email;

                    userDetails.UserId = user.UserId;
                    userDetails.Email = user.SystemUsers.Email;
                    userDetails.OrgName = user.Organizations.Name;
                    userDetails.OrganizationType = user.Organizations.OrganizationType;
                    userDetails.UserStatus = user.SystemUsers.UserStatus;

                    var RoleNames = user.SystemUsersOrganizationsRoles.Select(r => r.SystemRoles.RoleName);
                    if (RoleNames != null && RoleNames.Any())
                    {
                        userDetails.UserRoles = string.Join(",", RoleNames);
                    }
                    else userDetails.UserRoles = "";
                    finalUsersList.Add(userDetails);
                }

                // To get ghost users
                #region Ghost Users
                if (searchType == true && !string.IsNullOrEmpty(email) && string.IsNullOrEmpty(fname) && string.IsNullOrEmpty(lname))
                {
                    var Users = context.SystemUsers.SqlQuery("select * from SystemUsers where UserId not in (select UserId from SystemUsersOrganizations)")
                        .Where(r => string.IsNullOrEmpty(email) || r.Email.ToLower().Contains(email.ToLower())).ToList();


                    //var GhostUsers = Users.Select(r => r.UserId).ToList();
                    //var Sys_Orgs_Not_Exist = context.SystemUsersOrganizations.Where(r => GhostUsers.Contains(r.UserId)).ToList();
                    //if (Sys_Orgs_Not_Exist.Count > 0)
                    //{
                    //    var USersExclude = Sys_Orgs_Not_Exist.Select(r => r.UserId).ToList();
                    //    Users.Where(r => !USersExclude.Contains(r.UserId)).ToList();
                    //}
                    foreach (var user in Users.Skip((request.PageSize * (request.Page - 1))).Take(request.PageSize).ToList())
                    {
                        LocalUsersList userDetails = new LocalUsersList();

                        if (user.Contacts.Count() != 0)
                            userDetails.Name = user.Contacts[0].LastName + ", " + user.Contacts[0].FirstName;
                        else
                            userDetails.Name = user.Email;

                        userDetails.UserId = user.UserId;
                        userDetails.Email = user.Email;
                        userDetails.OrgName = "";
                        userDetails.OrganizationType = "";
                        userDetails.UserStatus = false;
                        finalUsersList.Add(userDetails);
                    }
                }
                #endregion
                var Data = finalUsersList.OrderBy(rec => rec.Name);
                var result = new DataSourceResponse() { Total = TotalCount, Data = Data };
                //return finalUsersList;
                return result;
            }
        }
        /// <summary>
        /// Get All BidddingInterests
        /// </summary>
        /// <returns></returns>
        public List<BiddingInterests> GetBiddings()
        {
            using (var context = new EFDbContext())
            {
                return context.BiddingInterests.OrderBy(m => m.BiddingInterestName).ToList();
            }
        }
        /// <summary>
        /// Get All ClaycoBiddingInterests
        /// </summary>
        /// <returns></returns>
        public List<ClaycoBiddingInterests> GetClaycoBiddingsList()
        {
            using (var context = new EFDbContext())
            {
                return context.ClaycoBiddingInterests.OrderBy(m => m.BiddingInterestName).ToList();
            }
        }
        /// <summary>
        /// Get List Of All Languages
        /// </summary>
        /// <returns></returns>
        public List<Language> GetLanguages()
        {
            using (var context = new EFDbContext())
            {
                return context.Language.OrderBy(rec => rec.LanguageName).ToList();
            }
        }
        /// <summary>
        /// First Clayco Biddings Based on BiddingInterestid
        /// </summary>
        /// <param name="biddingId"></param>
        /// <returns></returns>
        public ClaycoBiddingInterests GetFirstClaycoBiddings(long biddingId)
        {
            using (var context = new EFDbContext())
            {
                return context.ClaycoBiddingInterests.FirstOrDefault(record => record.BiddingInterestId == biddingId);
            }
        }
        /// <summary>
        /// Get First Bidding Interest Based on Bidding Interestid
        /// </summary>
        /// <param name="biddingId"></param>
        /// <returns></returns>
        public BiddingInterests GetFirstBiddings(long biddingId)
        {
            using (var context = new EFDbContext())
            {
                return context.BiddingInterests.FirstOrDefault(record => record.BiddingInterestId == biddingId);
            }
        }
        /// <summary>
        /// Insert Bidding Interest based on selected biddingid
        /// </summary>
        /// <param name="bidding"></param>
        public void AddBiddings(LocalAddBiddingInterest bidding)
        {
            using (var context = new EFDbContext())
            {
                if (bidding.isEdit == false)
                {
                    BiddingInterests currentBidding = new BiddingInterests();
                    currentBidding.BiddingInterestName = bidding.BiddingInterestName;
                    currentBidding.LanguageId = bidding.LanguageId;
                    bool UserStatus = bidding.Status;
                    if (UserStatus == true)
                    {
                        currentBidding.Status = 1;
                    }
                    else
                    {
                        currentBidding.Status = 0;
                    }

                    context.BiddingInterests.Add(currentBidding);
                    context.SaveChanges();
                }
                else
                {
                    BiddingInterests currentBiddingInterestRecord = GetFirstBiddings(bidding.BiddingInterestId);
                    currentBiddingInterestRecord.BiddingInterestName = bidding.BiddingInterestName;
                    currentBiddingInterestRecord.LanguageId = bidding.LanguageId;
                    bool UserStatus = bidding.Status;
                    if (UserStatus == true)
                    {
                        currentBiddingInterestRecord.Status = 1;
                    }
                    else
                    {
                        currentBiddingInterestRecord.Status = 0;
                    }

                    context.Entry(currentBiddingInterestRecord).State = EntityState.Modified;
                    context.SaveChanges();
                }
            }
        }
        /// <summary>
        /// Insert Clayco Bidding Interest based on selected biddingid
        /// </summary>
        /// <param name="bidding"></param>
        public void AddClaycoBidddings(LocalAddClaycoBiddingInterest bidding)
        {
            using (var context = new EFDbContext())
            {
                if (bidding.isEdit == false)
                {
                    ClaycoBiddingInterests currentBidding = new ClaycoBiddingInterests();
                    currentBidding.BiddingInterestName = bidding.BiddingInterestName;
                    currentBidding.LanguageId = bidding.LanguageId;
                    currentBidding.BiddingInterestCode = bidding.BiddingInterestCode;

                    bool UserStatus = bidding.Status;
                    if (UserStatus == true)
                    {
                        currentBidding.Status = 1;
                    }
                    else
                    {
                        currentBidding.Status = 0;
                    }

                    context.ClaycoBiddingInterests.Add(currentBidding);
                    context.SaveChanges();
                }
                else
                {
                    ClaycoBiddingInterests currentBiddingInterestRecord = GetFirstClaycoBiddings(bidding.BiddingInterestId); //entityDB.ClaycoBiddingInterests.FirstOrDefault(record => record.BiddingInterestId == bidding.BiddingInterestId);
                    currentBiddingInterestRecord.BiddingInterestName = bidding.BiddingInterestName;
                    currentBiddingInterestRecord.BiddingInterestCode = bidding.BiddingInterestCode;
                    currentBiddingInterestRecord.LanguageId = bidding.LanguageId;

                    bool UserStatus = bidding.Status;
                    if (UserStatus == true)
                    {
                        currentBiddingInterestRecord.Status = 1;
                    }
                    else
                    {
                        currentBiddingInterestRecord.Status = 0;
                    }

                    context.Entry(currentBiddingInterestRecord).State = EntityState.Modified;
                    context.SaveChanges();
                }

            }
        }
        /// <summary>
        /// Delete selected Bidding interest
        /// </summary>
        /// <param name="BiddingId"></param>
        public void DeleteBidding(long BiddingId)
        {
            using (var context = new EFDbContext())
            {
                var currentBiddingInterestRecord = context.BiddingInterests.FirstOrDefault(record => record.BiddingInterestId == BiddingId);

                var currentBiddingPrequalificationReportingRecords = context.PrequalificationReportingData.Where(record => record.ReportingDataId == BiddingId && record.ReportingType == 0).ToList();
                if (currentBiddingPrequalificationReportingRecords == null || currentBiddingPrequalificationReportingRecords.Count == 0)
                {
                    context.BiddingInterests.Remove(currentBiddingInterestRecord);
                    context.SaveChanges();
                }

            }
        }
        public void DeleteTemplatePrice(long TemplatePriceId)
        {
            using (var context = new EFDbContext())
            {
                var currentTemplatePricingRecord = context.TemplatePrices.Find(TemplatePriceId);
                context.TemplatePrices.Remove(currentTemplatePricingRecord);
                context.SaveChanges();
            }
        }
        public List<LocalHeaderMenuModel> setUpMenus(string role, Guid UserId, long CurrentOrgId)
        {

            using (var context = new EFDbContext())
            {


                var systemUsersOrgRolesList = context.SystemUsersOrganizations.FirstOrDefault(rec => rec.UserId == UserId && rec.OrganizationId == CurrentOrgId).SystemUsersOrganizationsRoles;


                List<LocalHeaderMenuModel> headerMenus = new List<LocalHeaderMenuModel>();
                var presentHeaderMenu = new LocalHeaderMenuModel();
                var presentSideMenu = new LocalSideMenuModel();
                foreach (var sysUserOrgRole in systemUsersOrgRolesList)
                {
                    var sysRolesPermissions = sysUserOrgRole.SystemRoles.SystemRolesPermissions;
                    foreach (var rolePermission in sysRolesPermissions)//In here we have all views related to login user
                    {
                        if (rolePermission.SysViewsId >= 1000) { continue; }

                        presentHeaderMenu = headerMenus.FirstOrDefault(rec => rec.menuName == rolePermission.SystemViews.SystemSideMenus.SystemHeaderMenus.SysMenuName);
                        if (presentHeaderMenu == null)
                        {
                            presentHeaderMenu = new LocalHeaderMenuModel(rolePermission.SystemViews.SystemSideMenus.SystemHeaderMenus);
                            headerMenus.Add(presentHeaderMenu);
                        }
                        string permission = "";
                        if (rolePermission.ReadPermission != null && rolePermission.ReadPermission == true)
                            permission += "R";
                        if (rolePermission.InsertPermission != null && rolePermission.InsertPermission == true)
                            permission += "I";
                        if (rolePermission.ModifyPermission != null && rolePermission.ModifyPermission == true)
                            permission += "M";
                        if (rolePermission.DeletePermission != null && rolePermission.DeletePermission == true)
                            permission += "D";

                        if (permission.Equals(""))//If User has no permissions on this view
                        {
                            continue;
                        }
                        if (presentHeaderMenu.sideMenus == null)
                        {
                            presentHeaderMenu.sideMenus = new List<LocalSideMenuModel>();
                            presentSideMenu = new LocalSideMenuModel(rolePermission.SystemViews.SystemSideMenus);
                            presentSideMenu.ViewsAndPermissions = new List<ViewAndPermissions>();
                            ViewAndPermissions viewPermission = new ViewAndPermissions();
                            viewPermission.ViewPermission = permission;
                            viewPermission.ViewName = rolePermission.SystemViews.SystemViewCSHTML;
                            viewPermission.ControllerName = rolePermission.SystemViews.SystemController;   // Rajesh on 08/19/2013
                            presentSideMenu.ViewsAndPermissions.Add(viewPermission);
                            presentHeaderMenu.sideMenus.Add(presentSideMenu);


                            if (presentSideMenu.controllerName != presentHeaderMenu.controllerName || presentHeaderMenu.actionName != presentSideMenu.actionName)
                            {
                                presentHeaderMenu.actionName = presentSideMenu.actionName;
                                presentHeaderMenu.controllerName = presentSideMenu.controllerName;
                            }
                        }
                        else if (presentHeaderMenu.sideMenus.FirstOrDefault(rec => rec.menuName == rolePermission.SystemViews.SystemSideMenus.SysMenuNameSM) == null)
                        {
                            presentSideMenu = new LocalSideMenuModel(rolePermission.SystemViews.SystemSideMenus);
                            presentSideMenu.ViewsAndPermissions = new List<ViewAndPermissions>();
                            ViewAndPermissions viewPermission = new ViewAndPermissions();
                            viewPermission.ViewPermission = permission;
                            viewPermission.ViewName = rolePermission.SystemViews.SystemViewCSHTML;
                            viewPermission.ControllerName = rolePermission.SystemViews.SystemController;   // Rajesh on 08/19/2013
                            presentSideMenu.ViewsAndPermissions.Add(viewPermission);
                            presentHeaderMenu.sideMenus.Add(presentSideMenu);
                        }
                        else
                        {
                            presentSideMenu = presentHeaderMenu.sideMenus.FirstOrDefault(rec => rec.menuName == rolePermission.SystemViews.SystemSideMenus.SysMenuNameSM);// Rajesh On 8/12/2013
                            ViewAndPermissions viewPermission = new ViewAndPermissions();
                            viewPermission.ViewPermission = permission;
                            viewPermission.ViewName = rolePermission.SystemViews.SystemViewCSHTML;
                            viewPermission.ControllerName = rolePermission.SystemViews.SystemController;   // Rajesh on 08/19/2013
                            presentSideMenu.ViewsAndPermissions.Add(viewPermission);
                        }

                    }

                }


                //Rajesh on 8/19/2013
                foreach (var header in headerMenus.ToList())
                {
                    foreach (var sideMenu in header.sideMenus.ToList().OrderBy(rec => rec.DisplayOrder))
                    {
                        if (sideMenu.ViewsAndPermissions.FirstOrDefault(rec => rec.ViewName == sideMenu.actionName && rec.ControllerName == sideMenu.controllerName) == null)
                        {
                            var flag = false;
                            if (header.actionName == sideMenu.actionName && header.controllerName == sideMenu.controllerName && header.hasSideMenu)
                                flag = true;
                            headerMenus.FirstOrDefault(rec => rec.headerMenuId == header.headerMenuId).sideMenus.Remove(sideMenu);
                            try
                            {
                                if (flag)//IF this is 1st side menu then assign 2nd side menu action as a header action
                                {
                                    headerMenus.FirstOrDefault(rec => rec.headerMenuId == header.headerMenuId).actionName = headerMenus.FirstOrDefault(rec => rec.headerMenuId == header.headerMenuId).sideMenus.OrderBy(rec => rec.DisplayOrder).FirstOrDefault().actionName;
                                    headerMenus.FirstOrDefault(rec => rec.headerMenuId == header.headerMenuId).controllerName = headerMenus.FirstOrDefault(rec => rec.headerMenuId == header.headerMenuId).sideMenus.OrderBy(rec => rec.DisplayOrder).FirstOrDefault().controllerName;
                                }
                            }
                            catch (Exception ee)//Means no side menus in this menu
                            {
                                headerMenus.Remove(headerMenus.FirstOrDefault(rec => rec.headerMenuId == header.headerMenuId));
                            }
                        }
                    }
                }
                // Ends <<<


                return headerMenus.OrderBy(rec => rec.DisplayOrder).ToList();
            }
        }
        /// <summary>
        /// Get All ProficiencyCapabilities  list
        /// </summary>
        /// <returns></returns>
        public List<ProficiencyCapabilities> GetProficiencyCategory()
        {
            using (var context = new EFDbContext())
            {
                return context.ProficiencyCapabilities.ToList();

            }
        }
        /// <summary>
        /// Get ProficiencyCategory Based on ProficiencyId
        /// </summary>
        /// <param name="ProficiencyId"></param>
        /// <returns></returns>
        public ProficiencyCapabilities FindProficiencyCategory(long ProficiencyId)
        {
            using (var context = new EFDbContext())
            {
                return context.ProficiencyCapabilities.Find(ProficiencyId);

            }
        }
        /// <summary>
        /// Check PrequalificationReportingData Having Data with ProficiencyId
        /// </summary>
        /// <param name="ProficiencyId"></param>
        /// <returns></returns>
        public bool HasPrequalificationReportingData(long ProficiencyId)
        {
            using (var context = new EFDbContext())
            {
                return context.PrequalificationReportingData.Any(
                    r => r.ReportingType == 1 && r.ReportingDataId == ProficiencyId);

            }
        }
        /// <summary>
        /// Get Proficiency CategoryNames Based on ProficiencyCategory
        /// </summary>
        /// <param name="ProficiencyCategory"></param>
        /// <returns></returns>
        public List<string> GetProficiencyCategoryNames(string ProficiencyCategory)
        {
            using (var context = new EFDbContext())
            {
                return context.ProficiencyCapabilities.Where(rec => rec.ProficiencyCategory == ProficiencyCategory).Select(rec => rec.ProficiencyCategory).Distinct().ToList();

            }
        }

        public string AddProficiency(LocalAddProficiencyCapability proficiency)
        {
            using (var context = new EFDbContext())
            {
                if (proficiency.isEdit == false)
                {
                    ProficiencyCapabilities addProficiency = new ProficiencyCapabilities();
                    if (proficiency.ProficiencyCategory == "OTHERS" || proficiency.ProficiencyCategory == null)
                    {
                        proficiency.ProficiencyCategoryName.Trim();
                        addProficiency.ProficiencyCategory = proficiency.ProficiencyCategoryName;
                    }
                    else
                    {
                        addProficiency.ProficiencyCategory = proficiency.ProficiencyCategory;
                    }

                    if (proficiency.ProficiencySubCategory == "OTHERS")
                    {
                        proficiency.ProficiencySubCategoryName.Trim();
                        addProficiency.ProficiencySubCategroy = proficiency.ProficiencySubCategoryName;
                    }
                    else if (proficiency.ProficiencySubCategory == "NONE")
                    {
                        addProficiency.ProficiencySubCategroy = "";
                    }
                    else
                    {
                        addProficiency.ProficiencySubCategroy = proficiency.ProficiencySubCategory;
                    }

                    addProficiency.ProficiencyName = proficiency.ProficiencyName;

                    bool ProficiencyStatus = proficiency.Status;
                    if (ProficiencyStatus == true)
                    {
                        addProficiency.Status = 1;
                    }
                    else
                    {
                        addProficiency.Status = 0;
                    }
                    context.ProficiencyCapabilities.Add(addProficiency);
                    context.SaveChanges();
                    return "CloseProficiencyCapability";
                }
                else
                {
                    var currentProficiencyRecord = context.ProficiencyCapabilities.Find(proficiency.ProficiencyId);
                    currentProficiencyRecord.ProficiencyName = proficiency.ProficiencyName;
                    bool ProficiencyStatus = proficiency.Status;
                    if (ProficiencyStatus == true)
                    {
                        currentProficiencyRecord.Status = 1;
                    }
                    else
                    {
                        currentProficiencyRecord.Status = 0;
                    }
                    context.Entry(currentProficiencyRecord).State = EntityState.Modified;
                    context.SaveChanges();
                    return "CloseProficiencyCapability";
                }

            }
        }
        /// <summary>
        /// Get ProficiencySubCategoryNames Based on ProficiencyId
        /// </summary>
        /// <param name="ProficiencyId"></param>
        /// <returns></returns>
        public List<string> GetProficiencySubCategoryNames(long ProficiencyId)
        {
            using (var context = new EFDbContext())
            {
                return context.ProficiencyCapabilities.Where(rec => rec.ProficiencyId == ProficiencyId).Select(rec => rec.ProficiencySubCategroy).Distinct().ToList();

            }
        }
        public List<ProficiancyWithIsEdit> GetProficiencyData()
        {
            using (var context = new EFDbContext())
            {
                return context.ProficiencyCapabilities.OrderBy(rec => rec.ProficiencyCategory)
                      .ThenBy(r => r.ProficiencySubCategroy)
                      .Select(
                          r =>
                              new ProficiancyWithIsEdit()
                              {
                                  ProficiencyCategory = r.ProficiencyCategory,
                                  ProficiencyId = r.ProficiencyId,
                                  ProficiencySubCategroy = r.ProficiencySubCategroy,
                                  ProficiencyName = r.ProficiencyName,
                                  Status = r.Status,
                                  IsEdit = !context.PrequalificationReportingData.Any(rec => rec.ReportingType == 1 && rec.ReportingDataId == r.ProficiencyId)
                              })
                      .ToList();

            }
        }
        public List<string> GetProficiencySubCategoryNamesWithCategory(string Category)
        {
            using (var context = new EFDbContext())
            {
                return context.ProficiencyCapabilities.Where(subCategories => subCategories.ProficiencyCategory == Category && subCategories.ProficiencySubCategroy != "").Select(rec => rec.ProficiencySubCategroy).Distinct().ToList();

            }
        }

        public List<PrequalificationReportingData> GetPrequalificationReportingData(long proficiencyId)
        {
            using (var context = new EFDbContext())
            {
                return context.PrequalificationReportingData.Where(rec => rec.ReportingType == 1 && rec.ReportingDataId == proficiencyId).ToList();
            }
        }
        /// <summary>
        /// update ProficiencyData Based on proficiencyId
        /// </summary>
        /// <param name="proficiencyId"></param>
        /// <param name="category"></param>
        /// <param name="SubCategroy"></param>
        /// <returns></returns>
        public string EditProficiency(long proficiencyId, string category, string SubCategroy)
        {
            using (var context = new EFDbContext())
            {
                try
                {
                    var proficiencydata = context.ProficiencyCapabilities.Find(proficiencyId);
                    proficiencydata.ProficiencyCategory = category == null ? null : category.Trim();
                    proficiencydata.ProficiencySubCategroy = SubCategroy == null ? null : SubCategroy.Trim();
                    context.Entry(proficiencydata).State = EntityState.Modified;
                    context.SaveChanges();
                    return "OK";
                }
                catch (Exception ee)
                {
                    return "NO";
                }
            }

        }
        /// <summary>
        /// Display All Document Types
        /// </summary>
        /// <returns></returns>
        public List<DocumentType> GetDocuments()
        {
            using (var context = new EFDbContext())
            {
                return context.DocumentType.ToList();
            }

        }
        public LocalAddDocumentType GetDocumentTypeBasedOnId(long DocumentTypeId)
        {
            using (var context = new EFDbContext())
            {
                LocalAddDocumentType addDocument = new LocalAddDocumentType();
                if (DocumentTypeId == -1)
                {
                    addDocument.isEdit = false;
                }
                else
                {
                    addDocument.isEdit = true;
                    var currentDocumentTypeRecord = context.DocumentType.Find(DocumentTypeId);
                    addDocument.DocumentTypeName = currentDocumentTypeRecord.DocumentTypeName;
                    addDocument.DocumentTypeId = currentDocumentTypeRecord.DocumentTypeId;
                    addDocument.RestrictAccess = currentDocumentTypeRecord.RestrictAccess;
                    if (currentDocumentTypeRecord.DocumentExpires == true)
                    {
                        addDocument.DocumentExpires = true;
                    }
                }
                return addDocument;
            }
        }
        public bool HasDocument(string DocumentTypeName, long DocumentTypeId)
        {
            using (var context = new EFDbContext())
            {
                return context.DocumentType.Any(record => record.DocumentTypeName.ToUpper() == DocumentTypeName.Trim().ToUpper() && record.DocumentTypeId != DocumentTypeId);
            }

        }
        public void AddDocumentType(LocalAddDocumentType addDocumentType)
        {
            using (var context = new EFDbContext())
            {

                if (addDocumentType.isEdit == false)
                {
                    DocumentType documentType = new DocumentType();
                    documentType.DocumentTypeName = addDocumentType.DocumentTypeName.Trim();
                    documentType.DocumentExpires = addDocumentType.DocumentExpires;
                    documentType.RestrictAccess = addDocumentType.RestrictAccess;
                    documentType.DocumentInstruction = addDocumentType.DocumentTypeName.Trim();
                    documentType.PrequalificationUseOnly = true;

                    context.DocumentType.Add(documentType);
                    context.SaveChanges();
                }
                else
                {
                    var currentDocumentTypeRecord = context.DocumentType.Find(addDocumentType.DocumentTypeId);
                    currentDocumentTypeRecord.DocumentTypeName = addDocumentType.DocumentTypeName.Trim();
                    currentDocumentTypeRecord.DocumentExpires = addDocumentType.DocumentExpires;
                    currentDocumentTypeRecord.RestrictAccess = addDocumentType.RestrictAccess;
                    currentDocumentTypeRecord.DocumentInstruction = addDocumentType.DocumentTypeName.Trim();

                    context.Entry(currentDocumentTypeRecord).State = EntityState.Modified;
                    context.SaveChanges();
                }

            }

        }
        public long AssignUserRoles(List<LocalAssignRoles> UserRoles)
        {

            using (var context = new EFDbContext())
            {
                long OrgId = -1;
                var SuperUserRole = "";
                var sysUserOrgs = UserRoles.Select(rec => rec.SysUserOrgId);
                var oldSuperUsersList = context.SystemUsersOrganizations.Where(rec => sysUserOrgs.Contains(rec.SysUserOrganizationId) && rec.PrimaryUser == true).ToList();

                foreach (var UserRole in UserRoles)
                {
                    var OrgRoles = context.SystemUsersOrganizationsRoles.Where(rec => rec.SysUserOrgId == UserRole.SysUserOrgId).ToList();
                    foreach (var OrgRole in OrgRoles)
                    {

                        context.SystemUsersOrganizationsRoles.Remove(OrgRole);

                    }

                    for (int i = 0; i < UserRole.Roles.Count; i++)
                    {
                        if (UserRole.IsRoleSelected[i] == false)
                            continue;
                        SystemUsersOrganizationsRoles OrgRoleLoc = new SystemUsersOrganizationsRoles();
                        OrgRoleLoc.SysRoleId = UserRole.Roles[i];
                        OrgRoleLoc.SysUserOrgId = UserRole.SysUserOrgId;
                        context.SystemUsersOrganizationsRoles.Add(OrgRoleLoc);

                        if (OrgId == -1)
                        {
                            OrgId = context.SystemUsersOrganizations.Find(UserRole.SysUserOrgId).OrganizationId;
                            var OrgType = context.SystemUsersOrganizations.Find(UserRole.SysUserOrgId).Organizations.OrganizationType;
                            SuperUserRole = context.SystemRoles.FirstOrDefault(rec => rec.OrganizationType == OrgType && rec.RoleName == "Super User").RoleId;
                        }

                        if (UserRole.Roles[i].ToLower().Equals(SuperUserRole.ToLower()))
                        {
                            if (oldSuperUsersList.Any())
                            {
                                foreach (var oldSuperUser in oldSuperUsersList)
                                {
                                    oldSuperUser.PrimaryUser = false;
                                    context.Entry(oldSuperUser).State = EntityState.Modified;
                                }
                            }
                            var superUser = context.SystemUsersOrganizations.Find(UserRole.SysUserOrgId);
                            superUser.PrimaryUser = true;
                            context.Entry(superUser).State = EntityState.Modified;
                        }
                        context.SaveChanges();
                    }

                }
                return OrgId;
            }
        }
        public List<LocalAssignRoles> UserWithPermissions(long OrgId)
        {
            using (var context = new EFDbContext())
            {
                var UserRoles = new List<LocalAssignRoles>();
                var SelectedOrg = context.Organizations.Find(OrgId);

                var SelectedSysUserOrgList = SelectedOrg.SystemUsersOrganizations.ToList();
                var OrgType = SelectedOrg.OrganizationType;
                var EmployeeRoles = EmployeeRole.VendorEmpRoleId;
                var SysRoles = context.SystemRoles.Where(rec => rec.OrganizationType == OrgType && !EmployeeRoles.Contains(rec.RoleId));
                foreach (var SysUserOrg in SelectedSysUserOrgList)
                {
                    var temp = SysUserOrg.SystemUsersOrganizationsRoles.FirstOrDefault(rec => EmployeeRoles.Contains(rec.SystemRoles.RoleId));
                    if (temp != null)
                        continue;
                    LocalAssignRoles Role = new LocalAssignRoles();

                    Role.Roles = SysRoles.Select(rec => rec.RoleId).ToList();
                    Role.RoleNames = SysRoles.Select(rec => rec.RoleName).ToList();
                    Role.SysUserOrgId = SysUserOrg.SysUserOrganizationId;
                    var contact = SysUserOrg.SystemUsers.Contacts;
                    if (contact != null && contact.Count != 0)
                        Role.UserName = contact[0].LastName + ", " + contact[0].FirstName;
                    else
                        Role.UserName = SysUserOrg.SystemUsers.Email;

                    Role.IsRoleSelected = new List<bool>();
                    foreach (var RoleLoc in Role.Roles)
                    {
                        var flag = false;
                        try { flag = SysUserOrg.SystemUsersOrganizationsRoles.FirstOrDefault(rec => rec.SysRoleId == RoleLoc) != null; }
                        catch { }
                        Role.IsRoleSelected.Add(flag);
                    }
                    UserRoles.Add(Role);
                }
                return UserRoles;
            }
        }
        public List<Organizations> GetClientsList(string OrganizationType)
        {
            using (var context = new EFDbContext())
            {
                var orgs = context.Organizations.Where(Rec => Rec.OrganizationType == OrganizationType && Rec.ShowInApplication == true).OrderBy(rec => rec.Name).ToList();

                return orgs;

            }

        }
        public List<CompanyAccessRoles> GetUserRoles(Guid UserId)
        {
            using (var context = new EFDbContext())
            {
                var sysUserOrgs = context.SystemUsersOrganizations.Where(m => m.UserId == UserId).ToList();
                List<CompanyAccessRoles> CompanyRoles = new List<CompanyAccessRoles>();
                //var sysUserOrgs = context.SystemUsersOrganizations.Where(m => m.UserId == UserId).Select(m => new CompanyAccessRoles()
                //{
                //    Name = m.Organizations.Name,
                //    Userid = m.UserId,
                //    SysUserOrganizationId = m.SysUserOrganizationId,
                //    RoleName = m.SystemUsersOrganizationsRoles.Select(r => r.SystemRoles).Select(r => r.RoleName);
                //}).ToList();
                foreach (var orgs in sysUserOrgs)
                {
                    CompanyAccessRoles Roles = new CompanyAccessRoles();
                    Roles.Name = orgs.Organizations.Name;
                    Roles.Userid = orgs.UserId;
                    Roles.SysUserOrganizationId = orgs.SysUserOrganizationId;
                    var RoleNames = new List<string>();
                    foreach (var orgRoles in orgs.SystemUsersOrganizationsRoles)
                    {
                        var RoleName = orgRoles.SystemRoles.RoleName;
                        RoleNames.Add(RoleName);

                    }
                    Roles.RoleName = RoleNames;
                    CompanyRoles.Add(Roles);
                }

                return CompanyRoles;
            }
        }
        public List<SiteRepresents> GetSiteRepresents(Guid registeredUserId)
        {
            using (var context = new EFDbContext())
            {
                return context.ClientBusinessUnitSiteRepresentatives.Where(record => record.SafetyRepresentative == registeredUserId).Select(r => new
               SiteRepresents()
                {
                    ClientId = r.ClientBusinessUnitSites.ClientBusinessUnit.ClientId,
                    ClientBUSiteID = r.ClientBUSiteID,
                    ClientBusinessUnitId = r.ClientBusinessUnitSites.ClientBusinessUnitId,
                    BusinessUnitname = r.ClientBusinessUnitSites.ClientBusinessUnit.BusinessUnitName,
                    SiteName = r.ClientBusinessUnitSites.SiteName
                }).ToList();



            }

        }
        public long ErrorHandler(ErrorModel error, string params1)
        {
            using (var context = new EFDbContext())
            {
                // var x = HttpRequest.QueryString;
                ApplicationError ar = new ApplicationError();
                ar.ControllerName = error.ControllerName;
                ar.ActionName = error.ActionName;
                ar.CreatedDateTime = error.CreatedDateTime;
                ar.HasSession = error.HasSession;
                ar.Session = error.Session;
                ar.RequestData = params1;
                ar.Exception = error.Exception.StackTrace;
                ar.Type = "Application Error";
                context.ApplicationError.Add(ar);
                context.SaveChanges();
                return ar.id;

            }
        }
        public string UpdateUserEmail(Guid UserId, string Email)
        {
            using (var context = new EFDbContext())
            {
                var User = context.SystemUsers.Find(UserId);
                User.Email = Email;
                User.UserName = Email;
                context.Entry(User).State = EntityState.Modified;
                context.SaveChanges();
                return "Success";
            }
        }
        public List<KeyValuePair<string, int>> GetDepartments()
        {
            using (var context = new EFDbContext())
            {
                return context.Department.Select(rec => new { rec.DepartmentName, rec.Id }).ToList().Select(r => new KeyValuePair<string, int>(r.DepartmentName, r.Id)).ToList();
            }
        }
        public string UserInsertion(string file, long OrgId, string RoleId, int DepartmentId)
        {
            using (var entityDb = new EFDbContext())
            {
                try
                {
                    const int numberOfCols = 4;
                    foreach (var row in file.Split('\n').Skip(1))
                    {
                        var columns = Regex.Split(row, ",(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))");
                        if (columns.Count() != numberOfCols) continue;
                        var email = columns[2].Trim();
                        Regex regex = new Regex(LocalConstants.EMAIL_FORMAT);

                        Match match = regex.Match(email);
                        if (match.Success)
                        {
                            var TaxId = columns[0].Trim();
                            var Org = entityDb.Organizations.Find(OrgId);

                            var UserName = columns[2].Replace("\"", "").Replace("\r", "").Trim();
                            if (!entityDb.SystemUsers.Any(r => r.UserName == UserName))
                            {
                                var contact = new Contact { FirstName = columns[0].Replace("\"", ""), LastName = columns[1].Replace("\"", "") };

                                var sysUserOrg = new SystemUsersOrganizations
                                {
                                    SystemUsers = new SystemUsers()
                                    {
                                        Email = email,
                                        UserName = columns[2].Replace("\"", "").Trim(),
                                        UserStatus = true,
                                        CreateDate = DateTime.Now,
                                        LastLoginDate = DateTime.Now,
                                        Password = UtilityBusiness.EncryptData(columns[3].Replace("\"", "").Replace("\r", "").Trim()),
                                        Contacts = new List<Contact>() { contact },
                                    }
                                };
                                Org.SystemUsersOrganizations.Add(sysUserOrg);

                                var role = new SystemUsersOrganizationsRoles
                                {
                                    SysRoleId = RoleId,
                                    SysUserOrgId = sysUserOrg.SysUserOrganizationId
                                };
                                entityDb.SystemUsersOrganizationsRoles.Add(role);
                                if (Org.OrganizationType == LocalConstants.Client && DepartmentId != -1)
                                {
                                    var UserDepartments = new UserDepartments()
                                    {
                                        DepartmentId = DepartmentId,
                                        UserId = sysUserOrg.UserId
                                    };
                                    entityDb.UserDepartments.Add(UserDepartments);
                                }
                                entityDb.SaveChanges();
                            }
                        }
                        else continue;

                    }
                    return Resources.Resources.Bulk_User_Sucess_Msg;
                }
                catch (Exception e)
                {
                    return Resources.Resources.Unable_process_req;
                }

            }
        }
        public bool CheckIsOrmatUser(Guid UserId)
        {
            using (var context = new EFDbContext())
            {
                return context.SystemUsersOrganizations.Any(r => r.UserId == UserId && r.OrganizationId == LocalConstants.OrmatID);
            }
        }
        public DataSourceResponse GetAllDepartments(DataSourceRequest request, string filters)
        {
            using (var context = new EFDbContext())
            {
                var Departments = context.Department.ToList();
                if (!string.IsNullOrEmpty(filters))
                    Departments = Departments.Where(r => r.DepartmentName.ToLower().Contains(filters.ToLower()) || r.Organizations.Name.ToLower().Contains(filters.ToLower())).ToList();

                var depart = Departments.Skip((request.PageSize * (request.Page - 1))).Take(request.PageSize).ToList()
                    .Select(r => new LocalDepartments
                    {
                        ClientId = r.ClientID,
                        ClientName = r.Organizations.Name,
                        Id = r.Id,
                        DepartmentName = r.DepartmentName
                    }).ToList();
                var result = new DataSourceResponse() { Total = Departments.Count(), Data = depart };
                return result;
            }
        }
        public string UpdateDepartmentName(int id, string Name)
        {
            using (var context = new EFDbContext())
            {
                var Department = context.Department.Find(id);
                Department.DepartmentName = Name;
                context.Entry(Department).State = EntityState.Modified;
                context.SaveChanges();
                return "Sucess";
            }
        }
        public List<KeyValuePair<string, string>> GetAllEmailTemplates()
        {
            using (var context = new EFDbContext())
            {
                return context.EmailTemplates.OrderBy(r => r.Name).Select(rec => new { rec.Name, rec.EmailTemplateID }).ToList().Select(r => new KeyValuePair<string, string>(r.Name, r.EmailTemplateID.ToString())).ToList();
            }

        }
        public List<KeyValuePair<string, string>> GetClientDepartments(long ClientId)
        {
            using (var context = new EFDbContext())
            {
                return context.Department.Where(r => r.ClientID == ClientId).
                    Select(rec => new { rec.DepartmentName, rec.Id }).ToList().Select(r => new KeyValuePair<string, string>(r.DepartmentName, r.Id.ToString())).ToList();
            }

        }
        public List<KeyValuePair<string, string>> GetSineproSites()
        {
            using (var context = new EFDbContext())
            {
                return context.SignProCheckinCheckout.
                    Select(rec => new { rec.SiteName, rec.SiteId }).Distinct().ToList().Select(r => new KeyValuePair<string, string>(r.SiteName, r.SiteId)).ToList();
            }

        }

        public string AddDepartment(string Name, long ClientID)
        {
            using (var context = new EFDbContext())
            {
                var Department = context.Department.FirstOrDefault(r => r.DepartmentName == Name && r.ClientID == ClientID);
                if (Department == null)
                {
                    Department = new Department();
                    Department.DepartmentName = Name;
                    Department.ClientID = ClientID;
                    context.Department.Add(Department);
                    context.SaveChanges();
                    return "Sucess";
                }
                else
                {
                    return "This department is already exist";
                }

            }

        }
        public DataSourceResponse GetEmailtemplates(DataSourceRequest request, string filters)
        {
            using (var context = new EFDbContext())
            {
                var EmailTemplate = context.InviteTemplateConfiguration.OrderBy(r => r.Organizations.Name).ToList();
                if (!string.IsNullOrEmpty(filters))
                    EmailTemplate = EmailTemplate.Where(r => r.Organizations.Name.ToLower().Contains(filters.ToLower()) || r.EmailTemplates.Name.ToLower().Contains(filters.ToLower())).ToList();
                var Template = EmailTemplate.Skip((request.PageSize * (request.Page - 1))).Take(request.PageSize).ToList().Select(r => new LocalEmailtemplateConfig
                {
                    ID = r.Id,
                    ClientId = r.ClientId,
                    ClientName = r.Organizations.Name,
                    TemplateName = r.EmailTemplates.Name,
                    EmailTemplateId = r.TemplateId,
                    InvitationType = r.InvitationType,
                    InvitationName = r.InvitationType == 0 ? "New Invitation" : r.InvitationType == 1 ? "Expired Invitation" : r.InvitationType == 2 ? "OutSide Invitation" : "All",
                }).ToList();
                var result = new DataSourceResponse() { Total = EmailTemplate.Count(), Data = Template };
                return result;
            }
        }
        public string UpdateEmailTemplate(int Id, long ClientId, int TemplateId, int? Type)
        {
            using (var context = new EFDbContext())
            {


                var Template = context.InviteTemplateConfiguration.Find(Id);
                if (Template.ClientId != ClientId || Template.InvitationType != Type)
                {
                    var Inviatationtype = Type == 0 ? "New Invitation" : Type == 1 ? "Expired Invitation" : Type == 2 ? "OutSide Invitation" : "All";
                    var ClientName = context.Organizations.Find(ClientId).Name;
                    var ClientExist = context.InviteTemplateConfiguration.Any(r => r.ClientId == ClientId && r.InvitationType == Type);
                    if (ClientExist) return "Configuration for" + ClientName + " with " + Inviatationtype + " is already exist.";
                }
                Template.ClientId = ClientId;
                Template.TemplateId = TemplateId;
                Template.InvitationType = Type;

                context.Entry(Template).State = EntityState.Modified;

                context.SaveChanges();
                return "Success";

            }
        }
        public string DeleteEmailTemplate(int ID)
        {
            using (var context = new EFDbContext())
            {
                var Template = context.InviteTemplateConfiguration.Find(ID);
                context.InviteTemplateConfiguration.Remove(Template);
                context.SaveChanges();
                return "Success";
            }
        }
        public List<LocalUsersList> GetOrganizationUsers(Guid UserId)
        {
            using (var context = new EFDbContext())
            {
                var userOrgID = context.SystemUsersOrganizations.FirstOrDefault(r => r.UserId == UserId).OrganizationId;
                var Users = context.SystemUsersOrganizations.Where(r => r.OrganizationId == userOrgID && r.UserId != UserId && r.SystemUsers.UserStatus != false).Select(r => new LocalUsersList()
                {
                    UserId = r.UserId,
                    Name = r.SystemUsers.Contacts.FirstOrDefault() == null ? r.SystemUsers.Email : r.SystemUsers.Contacts.FirstOrDefault().LastName + " " + r.SystemUsers.Contacts.FirstOrDefault().FirstName,
                });
                return Users.OrderBy(r => r.Name).ToList();
            }

        }
        public int CheckOrgUsersCount(Guid UserId)
        {
            using (var context = new EFDbContext())
            {
                var userOrgID = context.SystemUsersOrganizations.FirstOrDefault(r => r.UserId == UserId).OrganizationId;
                var Userscount = context.SystemUsersOrganizations.Where(r => r.OrganizationId == userOrgID && r.UserId != UserId && r.SystemUsers.UserStatus != false).Count();
                return Userscount;
            }

        }
        public string AddEmailTemplate(long ClientId, int TemplateId, int? Type)
        {
            using (var context = new EFDbContext())
            {
                var Inviatationtype = Type == 0 ? "New Invitation" : Type == 1 ? "Expired Invitation" : Type == 2 ? "OutSide Invitation" : "All";

                var ClientName = context.Organizations.Find(ClientId).Name;
                var ClientExist = context.InviteTemplateConfiguration.Any(r => r.ClientId == ClientId && r.InvitationType == Type);
                if (ClientExist)
                    return "Configuration for" + ClientName + " with " + Inviatationtype + " is already exist.";

                else
                {
                    var Template = new InviteTemplateConfiguration();
                    Template.ClientId = ClientId;
                    Template.TemplateId = TemplateId;
                    Template.InvitationType = Type;

                    context.Entry(Template).State = EntityState.Added;

                    context.SaveChanges();
                    return "Success";
                }

            }
        }

        public List<Dropdown> GetClientUserEmails(long ClientId, string name)
        {
            using (var context = new EFDbContext())
            {
                var Users = context.SystemUsersOrganizations.Where(r => r.OrganizationId == ClientId);

                //   Users.Where(r => r.SystemUsers.Contacts.FirstOrDefault().LastName.Contains(name) || r.SystemUsers.Contacts.FirstOrDefault().FirstName.Contains(name));
                var ClientUsers = Users.Select(r => new Dropdown { UserId = r.UserId, Email = r.SystemUsers.UserName, Name = r.SystemUsers.Contacts.FirstOrDefault().LastName + " " + r.SystemUsers.Contacts.FirstOrDefault().FirstName });
                if (!string.IsNullOrEmpty(name)) ClientUsers = ClientUsers.Where(r => r.Name.ToLower().Contains(name.ToLower()));
                var Clents = ClientUsers.Distinct().OrderBy(rec => rec.Name).ToList();
                return Clents;
            }
        }

        public List<ClientUsersInfo> GetClientUsersData(string userId, long orgId)
        {
            using (var context = new EFDbContext())
            {
                var userIds = userId.Split(',').Where(r => !string.IsNullOrEmpty(r)) .Select(r => new Guid(r));
              return  context.SystemUsersOrganizations.Where(r => r.OrganizationId == orgId && userIds.Contains(r.UserId)).Select(r => new ClientUsersInfo(){UsersEmails = r.SystemUsers.Email, UserIds = r.UserId.ToString()}).ToList();
            }
        }
        public string SaveClientInvitaionEmails(string UserEmails, string UserIds, long ClientId, long? Id, int Type)
        {
            using (var context = new EFDbContext())
            {
                var usersData = GetClientUsersData(UserIds, ClientId);
                UserIds = string.Join(", ", usersData.Select(r => r.UserIds));
                UserEmails= string.Join(", ", usersData.Select(r => r.UsersEmails));
                var HasClientInvitation = context.ClientNotifications.FirstOrDefault(r => r.ClientId == ClientId && r.Type == Type);
                if (HasClientInvitation != null)
                {

                    if (Id == null)
                    {
                        HasClientInvitation.UserId = HasClientInvitation.UserId + "," + UserIds;
                        HasClientInvitation.UserEmails = HasClientInvitation.UserEmails + "," + UserEmails;

                    }
                    else
                    {
                        HasClientInvitation.UserId = UserIds;
                        HasClientInvitation.UserEmails = UserEmails;
                    }

                    context.Entry(HasClientInvitation).State = EntityState.Modified;

                }
                else
                {
                    ClientNotifications Invite = new ClientNotifications();
                    Invite.ClientId = ClientId;
                    Invite.UserEmails = UserEmails;
                    Invite.UserId = UserIds;
                    Invite.Type = Type;
                    context.ClientNotifications.Add(Invite);
                }
                context.SaveChanges();
                return "Success";
            }
        }

        public DataSourceResponse GetInviteUsers(DataSourceRequest request, string filters, int Type)
        {
            using (var context = new EFDbContext())
            {
                var Invite = context.ClientNotifications.Where(r => r.Type == Type).ToList();

                if (string.IsNullOrEmpty(filters))
                {
                    Invite = Invite.Skip((request.PageSize * (request.Page - 1))).Take(request.PageSize).ToList();
                }
                var Users = Invite.Select(r => new ClientUsersInfo
                {
                    ClientID = r.ClientId,
                    ClientName = r.Organizations.Name,
                    Id = r.Id,
                    UsersEmails = r.UserEmails,
                    UserIds = r.UserId,
                    UserNames = GetUserName(r.UserId)
                });
                if (!string.IsNullOrEmpty(filters))
                {
                    Users = Users.Where(r => r.ClientName.ToLower().Contains(filters.ToLower()) || r.UserNames.ToLower().Contains(filters.ToLower()) || r.UsersEmails.ToLower().Contains(filters.ToLower()));
                }

                var result = new DataSourceResponse() { Total = Invite.Count(), Data = Users.ToList() };
                return result;
            }
        }
        private string GetUserName(string userIds)
        {
            using (var context = new EFDbContext())
            {
                var UserName = new List<string>();
                var usersId = userIds.Split(',');
                foreach (var userid in usersId)
                {
                    if (string.IsNullOrEmpty(userid))
                        continue;
                   
                    var Contact = context.SystemUsers.Find(new Guid(userid)).Contacts.FirstOrDefault();

                    UserName.Add(Contact.LastName + ' ' + Contact.FirstName);
                }
                return string.Join(",", UserName);
            }
        }
        public List<long> GetClients()
        {
            using (var context = new EFDbContext())
            {
                return context.ClientNotifications.Select(r => r.ClientId).Distinct().ToList();
            }
        }
        public ClientNotifications CheckClientUsersforInvitation(long ClientId)
        {
            using (var context = new EFDbContext())
            {
                return context.ClientNotifications.FirstOrDefault(r => r.ClientId == ClientId && r.Type == (int)ClientNotificationType.Inviatation);
            }
        }
        public bool CheckInviteClients(long ClientId)
        {
            using (var context = new EFDbContext())
            {
                return context.ClientNotifications.Any(r => r.ClientId == ClientId && r.Type == (int)ClientNotificationType.Inviatation);
            }
        }
        public List<long> NotificationClients()
        {
            using (var context = new EFDbContext())
            {
                return context.ClientNotifications.Select(r => r.ClientId).Distinct().ToList();
            }
        }
        public string DeleteClientNotification(long Id)
        {
            using (var context = new EFDbContext())
            {
                var Invitation = context.ClientNotifications.Find(Id);
                context.ClientNotifications.Remove(Invitation);
                context.SaveChanges();
                return "Success";
            }
        }
        public PqBannerComments GetPQcomments(long pqId)
        {
            using (var context = new EFDbContext())
            {
                return context.PqComments.Where(r => r.PQId == pqId).OrderByDescending(r => r.CreatedDate).FirstOrDefault();
                //if (PQComments != null) return PQComments.Comments;
                //else return "";
            }
        }
        public void UpdatePqComments(long pqId, string Comment, Guid UserId, bool IsChecked)
        {
            using (var context = new EFDbContext())
            {


                PqBannerComments PQComments = new PqBannerComments();
                PQComments.Comments = Comment;
                PQComments.PQId = pqId;
                PQComments.CreatedDate = DateTime.Now;
                PQComments.IsChecked = IsChecked;
                PQComments.CreatedBy = UserId;
                context.PqComments.Add(PQComments);
                context.SaveChanges();

            }
        }
        public long GetSuperClientId(long ClientId)
        {
            using (var context = new EFDbContext())
            {
                return context.SubClients.FirstOrDefault(r => r.ClientId == ClientId && r.IsActive == true).SuperClientId;
            }
        }
        /// <summary>
        /// todo:Have to work on the filters like organization,users,etc..
        /// </summary>
        /// <param name="request"></param>
        /// <param name="filters"></param>
        /// <returns></returns>
        public DataSourceResponse GetAllPowerBiUserPermissions(DataSourceRequest request, string filters)
        {
            using (var context = new EFDbContext())
            {
                var powerbiUserPermissions = context.PowerBiUserPermissions.Where(r => r.PowerBiPermissions.PowerBiReports.IsActive != false).OrderBy(r => r.Id).Skip((request.PageSize * (request.Page - 1))).Take(request.PageSize).ToList();

                var users = powerbiUserPermissions.Select(r => new LocalPowerBiUserPermissions
                {
                    OrgId = r.OrganizationId,
                    PermissionType = r.PowerBiPermissions.PermissionType,
                    ReportId = r.PowerBiPermissions.PowerBiReports.Id,
                    PermissionTo = Enum.GetName(typeof(PermissionType), r.PowerBiPermissions.PermissionType),
                    ReportName = r.PowerBiPermissions.PowerBiReports.DisplayName,
                    OrgName = r.OrganizationId != null ? r.Organizations.Name : LocalConstants.Allstr,
                    ID = r.Id,
                    UserId = r.UserId,
                    Users = (r.UserId == "" || r.UserId == null) ? LocalConstants.Allstr : GetUserName(r.UserId)
                });

                var result = new DataSourceResponse() { Total = powerbiUserPermissions.Count(), Data = users.ToList() };
                return result;
            }
        }
        public List<KeyValuePair<string, long>> GetPowerBiReportNames()
        {
            using (var context = new EFDbContext())
            {
                return context.PowerBiReports.Where(r => r.IsActive != false)
                    .Select(rec => new { rec.DisplayName, rec.Id }).ToList()
                    .Select(r => new KeyValuePair<string, long>(r.DisplayName, r.Id)).ToList();
            }
        }

        public List<KeyValuePair<string, string>> GetOrganizations(int Type)
        {
            using (EFDbContext context = new EFDbContext())
            {
                var orgType = Enum.GetName(typeof(PermissionType), Type);
                return context.Organizations.Where(r => r.OrganizationType == orgType).Select(r => new { r.OrganizationID, r.Name }).OrderBy(rec => rec.Name).ToList().
                    Select(rec =>
                        new KeyValuePair<string, string>(rec.OrganizationID.ToString(), rec.Name)).ToList();
            }
        }
        public string PostPowerBiUsersData(string UserIds, long? OrgId, long? Id, int Type, long ReportId)
        {
            
               UserIds = string.IsNullOrEmpty(UserIds)|| !OrgId.HasValue ? UserIds : string.Join(",", GetClientUsersData(UserIds, OrgId.Value).Select(r=>r.UserIds));
            using (var context = new EFDbContext())
            {
                if (Id != null)
                {
                    var hasUser = context.PowerBiUserPermissions.Find(Id);
                    context.PowerBiUserPermissions.Remove(hasUser);
                }
                var hasReportPermission = context.PowerBiPermissions.FirstOrDefault(r => r.PermissionType == Type && r.PowerBiId == ReportId);
                if (hasReportPermission == null)
                {
                    hasReportPermission = new PowerBiPermissions();
                    hasReportPermission.PowerBiId = ReportId;
                    hasReportPermission.CreatedOn = DateTime.Now;
                    hasReportPermission.PermissionType = Type;
                    context.PowerBiPermissions.Add(hasReportPermission);

                }
                PowerBiUserPermissions PowerBiUserPermission = new PowerBiUserPermissions();
                PowerBiUserPermission.UserId = UserIds;
                PowerBiUserPermission.OrganizationId = OrgId;
                PowerBiUserPermission.PowerBiPermissionId = hasReportPermission.Id;
                context.PowerBiUserPermissions.Add(PowerBiUserPermission);

                try
                {
                    context.SaveChanges();
                    return "Success";
                }
                catch (Exception e)
                {
                    var msg = e.Message;
                    return "Error";
                }

            }
        }
        public string DeletePowerBiUserpermission(long Id)
        {
            using (var context = new EFDbContext())
            {
                var powerbiUser = context.PowerBiUserPermissions.Find(Id);
                if (powerbiUser != null)
                {
                    context.PowerBiUserPermissions.Remove(powerbiUser);
                    try
                    {
                        context.SaveChanges();
                        return "Success";
                    }
                    catch (Exception e)
                    {
                        var msg = e.Message;
                        return "Error";
                    }
                }
                return "Success";
            }
        }
        public List<LocalPowerBiReports> GetUserReports(long OrgId, Guid UserId, string Role)
        {
            using (var context = new EFDbContext())
            {
                List<int> permissiontypes = new List<int>();
                permissiontypes.Add(0);


                permissiontypes.Add((int)Enum.Parse(typeof(PermissionType), Role));
                var userReportIDs = context.PowerBiUserPermissions
                     .Where(r => r.OrganizationId == 0 || r.OrganizationId == null && permissiontypes.Contains(r.PowerBiPermissions.PermissionType) || r.OrganizationId == OrgId).Select(r => new { r.UserId, r.PowerBiPermissions.PowerBiId }).ToList()
                    .Where(r => string.IsNullOrEmpty(r.UserId) || r.UserId.Contains(new Guid().ToString()) || r.UserId.Contains(UserId.ToString())).Select(r => r.PowerBiId);
                var reportsUrls = context.PowerBiReportUrls.Where(r => r.OrgId == OrgId && userReportIDs.Contains(r.PowerBiReportId) && r.PowerBiReports.IsActive != false &&r.PowerBiReports.IsPrequalificationAnalytics)
                    .Select(r => new LocalPowerBiReports
                    {
                        ReportId = r.PowerBiReportId,
                        ReportName = r.PowerBiReports.ReportName,
                        DisplayName = r.PowerBiReports.DisplayName,
                        Url = r.Url,
                        Order = r.PowerBiReports.Order
                    });
                return reportsUrls.OrderBy(r => r.Order).ToList();
            }
        }
       public void UsersInInvitation(Guid InviatationId,string Bcc,string Cc)
        {
            using (var context = new EFDbContext())
            {
                var invitation = context.VendorInviteDetails.Find(InviatationId);
                if (invitation!= null)
                {
                    invitation.CcSendTo = Cc;
                    invitation.BccSendTo = Bcc;
                    context.Entry(invitation).State = EntityState.Modified;
                    context.SaveChanges();
                }
            }
           
        }

    }
}