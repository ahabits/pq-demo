﻿/*
Page Created Date:  11/14/2019
Created By: Kiran Talluri
Purpose: All Business operations related to Quiz module will be performed here
Version: 1.0
****************************************************
*/ 

using FVGen3.BusinessLogic.Interfaces;
using FVGen3.Domain.Concrete;
using FVGen3.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FVGen3.BusinessLogic
{
    public class QuizBusiness : BaseBusinessClass, IQuizBusiness
    {
        public QuizDetails getQuizDetails(long quizId)
        {
            var quizData = new QuizDetails();
            using (var entity = new EFDbContext())
            {
                var quiz = entity.SystemUsersOrganizationsQuizzes.Find(quizId);
                quizData.VendorId = quiz.VendorId;
                quizData.ClientId = quiz.ClientId;
                quizData.UserId = quiz.SystemUsersOrganizations.UserId;
                ClientTemplatesQuizDetails ctqd = quiz.ClientTemplates.ClientTemplatesQuizDetails.FirstOrDefault();
                quizData.WaiverForm = ctqd.WaiverForm;
                quizData.WaiverFormContent = ctqd.WaiverFormContent;
                quizData.QuizSubmittedDate = quiz.QuizDateSubmitted;            
            }
            return quizData;
        }

        public QuizWaiverFormCompletionLog getWaiverFormLog(long quizId, DateTime? submittedDate)
        {
            using (var entity = new EFDbContext())
            {
                submittedDate = submittedDate.Value.Date;
                return entity.QuizWaiverFormCompletionLog.FirstOrDefault(rec => rec.QuizId == quizId && rec.SubmittedDate >= submittedDate);
            }
        }
    }
}
