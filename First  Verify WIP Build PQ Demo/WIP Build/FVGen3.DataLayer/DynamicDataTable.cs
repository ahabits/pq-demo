﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using AhaApps.Libraries.Extensions;
using FVGen3.DataLayer.DTO;

namespace FVGen3.DataLayer
{
    public class DynamicDataTableDAL : Interfaces.IDynamicDataTableDAL
    {
        public string ConnectionString { get; set; }
        
        public DynamicDataTableDAL(string connectionString)
        {
            this.ConnectionString = connectionString;
        }
        public List<KeyValuePair<string, string>> GetDynamicTableData(string tableName, long vendorID = -1)
        {
            throw new NotImplementedException();
        }

        public List<KeyValuePair<string, string>> GetUnitedStatesTableData(long vendorID,  bool showIDs = false, bool includeCanada = false, string country="")
        {
            string sql = String.Empty;
            List<KeyValuePair<string, string>> result = new List<KeyValuePair<string, string>>();

            if (includeCanada)
            {
                //sql = @"select us.StatesID,StateName,1 as tablekey  from UnitedStates us left outer join StateCodeVendorRestrictionList r on us.StatesId = r.StatesID	where (r.VendorID is null or r.VendorID=1)
                //    and us.StatesID <> 0  UNION
                //    select 100 + cp.ProvinceID as StateID, cp.ProvinceName as StateName, 2 as tablekey from CanadianProvinces cp 
                //    order by tablekey,StateName ";

                sql = @"select us.StatesID,StateName,1 as tablekey  from States us left outer join StateCodeVendorRestrictionList r on us.StatesId = r.StatesID	where (r.VendorID is null or r.VendorID=1)
                    and us.StatesID <> 0 ";
                if (string.IsNullOrEmpty(country) || country== "United States")
                {
                    sql += "and (us.CountryName='United States' or us.CountryName='Canada')";
                }
                //if (!string.IsNullOrEmpty(country))
                else
                {
                    sql+="and us.CountryName = '" + country + "'";
                }
                sql += " order by tablekey,StateName";
            }
            else { 
            sql = @"select us.StatesID,StateName from States us left outer join StateCodeVendorRestrictionList r on us.StatesId = r.StatesID	where (r.VendorID is null or r.VendorID=@vendorID)" +
                    " and us.StatesID <> 0  ";

                if (!string.IsNullOrEmpty(country))
                {
                    sql += "and us.CountryName = '" + country + "'";
                }
                sql += " order by StateName";
            }
            try
            {
                //Use a using statement. It will make sure the connection is closed even in the case of an error.
                using (SqlConnection conn = new SqlConnection(this.ConnectionString))
                {
                    conn.Open();

                    var cmd = new SqlCommand(sql, conn);


                    cmd.Parameters.AddWithValue("@vendorID", vendorID);

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            string id = reader["StatesID"].ToStringNullSafe();
                            string state = reader["StateName"].ToStringNullSafe();
                            if (showIDs)
                            {
                                result.Add(new KeyValuePair<string, string>(id, state));
                            }
                            else
                            {
                                result.Add(new KeyValuePair<string, string>(state, state));
                            }
                        }
                    }
                    return result;

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public List<KeyValuePair<string, string>> GetDynamicTableData(string tableName, string keyField, string valueField, string sortField, Guid? templateId, int vendorID, bool showIDs, int clientID)
        {
            
            if (String.IsNullOrEmpty(tableName)) throw new Exception("tableName is required.");
            if (String.IsNullOrEmpty(valueField)) throw new Exception("valueField is required.");
            if (String.IsNullOrEmpty(keyField)) throw new Exception("keyField is required.");

            List<KeyValuePair<string, string>> result = new List<KeyValuePair<string, string>>();
            string sqlTogetLanguageId = "select t.LanguageId from templates t where t.templateid = @templateid" ;
            long languageId;
            using (SqlConnection conn = new SqlConnection(this.ConnectionString))
            {
                conn.Open();

                var cmd = new SqlCommand(sqlTogetLanguageId, conn);
                cmd.Parameters.AddWithValue("@templateid", templateId);
                languageId = (long)cmd.ExecuteScalar();                
            }

            string sql = "Select " + keyField + "," + valueField + " From " + tableName + " ";
            if(tableName == "BiddingInterests" || tableName == "ClaycoBiddingInterests")
            {
                sql += " where Status=1 and LanguageId=" + languageId;
            }
            if (tableName == "ProductCategories")
            {
                sql += " where Status=1 ";
            }
            if (tableName == "ClientContractTypes")
            {
                sql += " where ClientId=" + clientID;
            }
            if (!string.IsNullOrEmpty(sortField))
            {
                sql += " order by "+sortField;
            }
            try
            {
                //Use a using statement. It will make sure the connection is closed even in the case of an error.
                using (SqlConnection conn = new SqlConnection(this.ConnectionString))
                {
                    conn.Open();

                    var cmd = new SqlCommand(sql, conn);

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            result.Add(new KeyValuePair<string, string>(reader[0].ToStringNullSafe(), reader[1].ToStringNullSafe()));
                        }
                    }
                    if (!tableName.ToString().Equals("ProductCategories"))
                        result.Insert(0, new KeyValuePair<string, string>("", "None"));
                    return result;

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public List<RiskLevelInfo> GetRiskLevelWithDetails(int clientID)
        {
            List<RiskLevelInfo> result = new List<RiskLevelInfo>();
            string sql = "select Distinct RiskLevel,ct.ClientTemplateID,LockedByDefault From ClientTemplates ct join templates t on ct.TemplateID = t.TemplateID where  RiskLevel is not null and RiskLevel != '' and clientID = @clientID order by RiskLevel";
            try
            {
                //Use a using statement. It will make sure the connection is closed even in the case of an error.
                using (SqlConnection conn = new SqlConnection(this.ConnectionString))
                {
                    conn.Open();

                    var cmd = new SqlCommand(sql, conn);
                    cmd.Parameters.AddWithValue("@clientID", clientID);

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            result.Add(new RiskLevelInfo(){Risk = reader[0].ToStringNullSafe(), ClientTemplateId = reader[1].ToString(),IsLocked = reader[2].ToBoolNullSafe()});
                        }
                    }
                    return result;

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<KeyValuePair<string, string>> GetRiskLevels(int clientID)
        {
            List<KeyValuePair<string, string>> result = new List<KeyValuePair<string, string>>();
            string sql = "select Distinct RiskLevel From ClientTemplates ct join templates t on ct.TemplateID = t.TemplateID where  RiskLevel is not null and RiskLevel != '' and clientID = @clientID order by RiskLevel";
            try
            {
                //Use a using statement. It will make sure the connection is closed even in the case of an error.
                using (SqlConnection conn = new SqlConnection(this.ConnectionString))
                {
                    conn.Open();

                    var cmd = new SqlCommand(sql, conn);
                    cmd.Parameters.AddWithValue("@clientID", clientID);

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            result.Add(new KeyValuePair<string, string>(reader[0].ToStringNullSafe(), reader[0].ToStringNullSafe()));
                        }
                    }
                    return result;

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<KeyValuePair<string, string>> GetLocations(int clientID)
        {
            List<KeyValuePair<string, string>> result = new List<KeyValuePair<string, string>>();
            string sql = "select ClientBusinessUnitSiteID,sitename + ' ' + BusinessUnitLocation from ClientBusinessUnits cbu join ClientBusinessUnitSites cbs on cbu.ClientBusinessUnitId = cbs.ClientBusinessUnitId where ClientId= @clientID order by sitename + ' ' + BusinessUnitLocation";
            try
            {
                //Use a using statement. It will make sure the connection is closed even in the case of an error.
                using (SqlConnection conn = new SqlConnection(this.ConnectionString))
                {
                    conn.Open();

                    var cmd = new SqlCommand(sql, conn);
                    cmd.Parameters.AddWithValue("@clientID", clientID);

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            result.Add(new KeyValuePair<string, string>(reader[1].ToStringNullSafe(), reader[0].ToStringNullSafe()));
                        }
                    }
                    return result;

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public List<KeyValuePair<string, string>> GetUserEmails(int clientID,long PqId)
        {
            List<KeyValuePair<string, string>> result = new List<KeyValuePair<string, string>>();
            
            string sql = "select email,c.LastName + ', ' +  c.FirstName from SystemUsers su join Contact c on su.UserId = c.UserId join SystemUsersOrganizations suo on su.UserId = suo.UserId where OrganizationId in(select ClientIds from PrequalificationClient  where PQId=@PqId)or  OrganizationId in(@clientID)order by c.LastName,c.FirstName";
            try
            {
                //Use a using statement. It will make sure the connection is closed even in the case of an error.
                using (SqlConnection conn = new SqlConnection(this.ConnectionString))
                {
                    conn.Open();

                    var cmd = new SqlCommand(sql, conn);
                    cmd.Parameters.AddWithValue("@clientID", clientID);
                    cmd.Parameters.AddWithValue("@PqId", PqId);
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            result.Add(new KeyValuePair<string, string>(reader[1].ToStringNullSafe(), reader[0].ToStringNullSafe()));
                        }
                    }
                    return result;

                }
            }
            catch (Exception ex)
            {
                throw ex;

            }
        }

    }
}
