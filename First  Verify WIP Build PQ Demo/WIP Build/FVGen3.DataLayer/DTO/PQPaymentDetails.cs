﻿/*
Page Created Date:  01/10/2017
Created By: Rajesh Pendela
Purpose: To tansfer the data from business layer to other layer
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FVGen3.DataLayer.DTO
{
   public  class PQPaymentDetails
    {
        public decimal? InvoiceAmount { get; set; }
        public decimal? PaymentReceivedAmount { get; set; }
        public DateTime? PaymentReceivedDate { get; set; }
        public string PaymentTransactionId { get; set; }
        public string Name { get; set; }
        public string TemplateName { get; set; }
        public int PaymentCategory { get; set; }
        public IEnumerable<string> ClientNames { get; set; }
        public string FeeCapPeriod { get; set; }
        public int? PeriodStatus { get; set; }
    }
}
