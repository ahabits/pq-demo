﻿/*
Page Created Date:  04/28/2017
Created By: Rajesh Pendela
Purpose: To tansfer the data from business layer to other layer
Version: 1.0
****************************************************
*/

using System.Collections.Generic;

namespace FVGen3.DataLayer.DTO
{
    public class VendorSummary
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string PhoneNumber { get; set; }
        public string Fax { get; set; }
        public string Website { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public List<string> Biddings { get; set; }
        public List<string> Proficiancy { get; set; }
        public List<string> Statuses { get; set; }
        public List<string> RickLevels { get; set; }
    }
}
