/*
Page Created Date:  04/12/2017
Created By: Rajesh Pendela
Purpose: To tansfer the data from business layer to other layer
Version: 1.0
****************************************************
*/

using System;

namespace FVGen3.DataLayer.DTO
{
   public class PQData
    {
       public DateTime? PrequalificationStart { get; set; }
       public DateTime? PrequalificationFinish { get; set; }
       public string PrequalificationStatusName { get; set; }
       public long PrequalificationStatusId { get; set; }
    }
}
