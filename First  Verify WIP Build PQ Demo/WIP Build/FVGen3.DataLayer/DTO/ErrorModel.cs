﻿/*
Page Created Date:  08/21/2018
Created By: Kiran Talluri
Purpose: To log the error data
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.DataLayer.DTO
{
    public class ErrorModel
    {
        public string ControllerName
        {
            get;
            set;
        }
        public string ActionName
        {
            get;
            set;
        }

        /*SessionDetail*/
        public bool? HasSession
        {
            get;
            set;
        }
        public string Session
        {
            get;
            set;
        }

        /*RequestDetail*/
        public Dictionary<string, string> RequestHeader
        {
            get;
            set;
        }
        public string RequestData
        {
            get;
            set;
        }

        /*ExceptionDetail*/

        public Exception Exception
        {
            get;
            set;
        }
        public DateTime CreatedDateTime
        {
            get;
            set;
        }

    }
}