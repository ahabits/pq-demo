﻿/*
Page Created Date:  08/18/2017
Created By: Rajesh Pendela
Purpose: To tansfer the data from business layer to other layer
Version: 1.0
****************************************************
*/

namespace FVGen3.DataLayer.DTO
{
    public class ClientSignature
    {
        public string Title { get; set; }
        public string Sign { get; set; }
        public long SubSectionId { get; set; }
    }
}
