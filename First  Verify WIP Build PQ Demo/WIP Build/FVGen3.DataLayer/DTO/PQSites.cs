﻿/*
Page Created Date:  10/05/2018
Created By: Rajesh Pendela
Purpose: To tansfer the data from business layer to other layer
Version: 1.0
****************************************************
*/

using FVGen3.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.DataLayer.DTO
{
    public class PQSites
    {
        public long PQSiteId { get; set; }
        public long? PQSiteStatusId { get; set; }
        public string PQSiteStatusName { get; set; }
        public string PQSiteName { get; set; }
        public long? PQSiteClientId { get; set; }
        public long? BUSiteId { get; set; }
        public bool NotificationExistForSite { get; set; }
        public string ClientName { get; set; }
        public string VendorIndicationText { get; set; }
    }
}
