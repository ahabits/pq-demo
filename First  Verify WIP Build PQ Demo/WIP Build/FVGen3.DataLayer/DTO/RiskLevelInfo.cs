﻿/*
Page Created Date:  11/13/2017
Created By: Rajesh Pendela
Purpose: To tansfer the data from business layer to other layer
Version: 1.0
****************************************************
*/

namespace FVGen3.DataLayer.DTO
{
    public class RiskLevelInfo
    {
        public string Risk { get; set; }
        public string ClientTemplateId { get; set; }
        public bool IsLocked { get; set; }
    }
}
