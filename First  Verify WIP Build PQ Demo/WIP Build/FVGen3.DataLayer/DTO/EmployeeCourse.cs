﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.DataLayer.DTO
{
    public class EmployeeCourse
    {
        public int EmployeeId { get; set; }
        public int CourseId { get; set; }
    }
}
