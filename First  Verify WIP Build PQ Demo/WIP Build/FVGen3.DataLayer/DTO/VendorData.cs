﻿/*
Page Created Date:  04/04/2017
Created By: Rajesh Pendela
Purpose: To tansfer the data from business layer to other layer
Version: 1.0
****************************************************
*/

using System;

namespace FVGen3.DataLayer.DTO
{
    public class VendorData
    {
        public string OrgName { get; set; }
        public long OrgId { get; set; }
        public string CurrentClientLatestStatus { get; set; }
        public string SuperUserName { get; set; }
        public string SuperUserEmail { get; set; }
        public bool HasSentInvitation { get; set; }
        public DateTime InvitationSentDate { get; set; }
        
        public long? PQId { get; set; }
       
    }
}
