﻿/*
Page Created Date:  04/04/2017
Created By: Rajesh Pendela
Purpose: To tansfer the data from business layer to other layer
Version: 1.0
****************************************************
*/

using System;

namespace FVGen3.DataLayer.DTO
{
    public class OrganizationData
    {
        // ReSharper disable once InconsistentNaming
        public long OrganizationID { get; set; }

        public string Name { get; set; }
        public int CountryType { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; } 
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Country { get; set; }
        public string PhoneNumber { get; set; }

        public string FaxNumber { get; set; }
        // ReSharper disable once InconsistentNaming
        public string WebsiteURL { get; set; }
        // ReSharper disable once InconsistentNaming
        public string FederalIDNumber { get; set; }
        public bool? IsClientInviteOnly { get; set; }
        public string ClientIntroContent { get; set; }
        public bool? EnableSites { get; set; }
        public bool? EnableEmployees { get; set; }
        public bool? ShowDashboardOnLogin { get; set; }
        public string SafetyStatisticsPageNote { get; set; }
        public DateTime? SubscriptionSignupDate { get; set; }
        public DateTime? SubscriptionCurrentTermEndDate { get; set; }
        public DateTime? InsertDateTime { get; set; }
        public DateTime? UpdateDateTime { get; set; }
        public bool? EnableSiteContacts { get; set; }
        public string OrganizationType { get; set; }
        public int? CopyAnswers { get; set; }
        public bool? TrainingCertificate { get; set; }

        public string SingleClientCode { get; set; }
        public string OrgRepresentativeName { get; set; }
        public string OrgRepresentativeEmail { get; set; }
        public string OrgInfusionSoftContactId { get; set; }
        public string PrincipalCompanyOfficerName { get; set; } 
        public bool? ShowInApplication { get; set; } 
        // ReSharper disable once InconsistentNaming
        public string TaxID { get; set; }

        public string ExtNumber { get; set; } 

        public bool? TrainingRequired { get; set; } 
        public Decimal? TrainingFeeCap { get; set; }
        public long Language { get; set; }
        public int? FinancialMonth { get; set; }
        public int? FinancialDate { get; set; }
        public string GeographicArea { get; set; }
        public string ClientSecret { get; set; }
        public int? ContractorEvaluationExpirationPeriod { get; set; }
    }
}
