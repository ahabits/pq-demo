﻿/*
Page Created Date:  06/02/2016
Created By: Rich Bronson
Purpose: To tansfer the data from business layer to other layer
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.DataLayer.DTO
{
    public class QuestionColumnData
    {
        public string Key { get; set; }
        public List<QcdQuestions> Questions { get; set; }
    }
    public class QcdQuestions
    {
        public string QuestionText { get; set; }
        public string PrequalificationUserInput { get; set; }        
    }
}
