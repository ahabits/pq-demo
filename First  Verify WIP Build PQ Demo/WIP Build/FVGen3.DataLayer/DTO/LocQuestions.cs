﻿/*
Page Created Date:  10/05/2018
Created By: Rajesh Pendela
Purpose: To tansfer the data from business layer to other layer
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.DataLayer.DTO
{
    public class LocQuestions
    {
        public string QuestionText
        {
            get;
            set;
        }
        public long QuestionId
        {
            get;
            set;
        }
        public string SubSectionName
        {
            get;
            set;
        }
        public bool IsExist
        {
            get;
            set;
        }
        public decimal? Value { get; set; }
        public bool? IsMax
        {
            get;
            set;
        }
    }
}
