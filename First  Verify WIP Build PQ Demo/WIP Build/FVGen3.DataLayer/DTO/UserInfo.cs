﻿/*
Page Created Date:  05/18/2017
Created By: Rajesh Pendela
Purpose: To tansfer the data from business layer to other layer
Version: 1.0
****************************************************
*/

using System;

namespace FVGen3.DataLayer.DTO
{
    public class UserInfo
    {
        public string FullName
        {
            get
            {
                return string.IsNullOrEmpty(FirstName) 
                    ? LastName
                    : string.IsNullOrEmpty(LastName) ? FirstName : FirstName + " " + LastName;
            }
        }

        public string Email { get; set; }
        public string EmpUserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string RoleName { get; set; }
        public string UserName { get; set; }
        public Guid UserId { get; set; }
        public string OrgType { get; set; }
        public long OrgId { get; set; }
        public string OrgName { get; set; }

        public string MobileNumber { get; set; }
        public string PhoneNumber { get; set; }
        public string ContactTitle { get; set; }
        public string Password { get; set; }
        public long ContactId { get; set; }
    }
}
