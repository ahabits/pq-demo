﻿/*
Page Created Date:  03/01/2017
Created By: Rajesh Pendela
Purpose: To tansfer the data from business layer to other layer
Version: 1.0
****************************************************
*/

using System;

namespace FVGen3.DataLayer.DTO
{
    public class VendorOpenPeriod
    {
        public DateTime PeriodStart { get; set; }
        public DateTime PeriodClose { get; set; }
        public int PeriodStatus { get; set; }
        public decimal TotalPaymentRecieved { get; set; }
    }
}
