﻿/*
Page Created Date:  10/30/2018
Created By: Rajesh Pendela
Purpose: To tansfer the data from business layer to other layer
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.DataLayer.DTO
{
    public class ClientRegionsDTO
    {
        public long ClientId { get; set; }
        public long RegionId { get; set; }
        public string RegionName { get; set; }
    }
}
