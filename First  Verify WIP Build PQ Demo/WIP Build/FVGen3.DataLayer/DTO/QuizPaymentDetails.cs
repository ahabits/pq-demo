﻿/*
Page Created Date:  01/10/2017
Created By: Rajesh Pendela
Purpose: To tansfer the data from business layer to other layer
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FVGen3.DataLayer.DTO
{
   public  class QuizPaymentDetails
    {
        public decimal? PaymentReceivedAmount { get; set; }       
        public DateTime? PaymentReceivedDate { get; set; }
        public string PaymentTransactionId { get; set; }
        public string EmployeeName { get; set; }
        public string TransactionMetaData { get; set; }
        public string ClientName { get; set; }
        public string QuizName { get; set; }
        public string Feeperiod { get; set; }
        public long prequalificationid { get; set; }
    }
}
