﻿/*
Page Created Date:  07/17/2018
Created By: Kiran Talluri
Purpose: To log the exception data
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.DataLayer.DTO
{
  public  class ExceptionModel
    { public string Message { get; set; }
        public string ExceptionMessage { get; set; }
    }
}
