﻿/*
Page Created Date:  06/30/2017
Created By: Rajesh Pendela
Purpose: To tansfer the data from business layer to other layer
Version: 1.0
****************************************************
*/

using System;

namespace FVGen3.DataLayer.DTO
{
    public class AgreementSubsectionModel
    {
        public long SubSectionId { get; set; }
        public bool IsVendor { get; set; }
        public bool IsClient { get; set; }
        public bool IsAdmin { get; set; }
        public int EmailTemplateId { get; set; }
        public Guid LoginUser { get; set; }
    }
}