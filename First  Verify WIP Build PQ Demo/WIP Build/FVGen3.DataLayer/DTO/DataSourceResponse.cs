﻿/*
Page Created Date:  07/24/2017
Created By: Kiran Talluri
Purpose: To tansfer the data from business layer to other layer
Version: 1.0
****************************************************
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.DataLayer.DTO
{
    public class DataSourceResponse
    {
        public IEnumerable Data { get; set; }
        public int Total { get; set; }
    }
}
