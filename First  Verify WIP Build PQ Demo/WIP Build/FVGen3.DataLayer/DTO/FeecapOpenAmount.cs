﻿/*
Page Created Date:  03/01/2017
Created By: Rajesh Pendela
Purpose: To tansfer the data from business layer to other layer
Version: 1.0
****************************************************
*/

namespace FVGen3.DataLayer.DTO
{
    public class FeecapOpenAmount
    {
        public string TemplateName { get; set; }
        public decimal Amount { get; set; } 
    }
}
