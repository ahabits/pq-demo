﻿/*
Page Created Date:  07/24/2017
Created By: Rajesh Pendela
Purpose: To tansfer the data from business layer to other layer
Version: 1.0
****************************************************
*/

using System.Collections.Generic;

namespace FVGen3.DataLayer.DTO
{
    public class AddendumDTO
    {
        public AddendumDTO()
        {
            Questions=new List<AddendumQuestions>();
        }
        public List<AddendumQuestions> Questions { get; set; }
        public string SubSectionName { get; set; }
        public string Header { get; set; }
        public string Footer { get; set; }
        public bool VisibleToVendor { get; set; }
        public long PQSubSectionId { get; set; }
        public long PqId { get; set; }
        public long SubSecId { get; set; }
        public string SubSectionType { get; set; }
    }

    public class AddendumQuestions
    {
        public long PQQuestionId { get; set; }
        public string QuestionText { get; set; }
        public long QuestionType { get; set; }
        public string InputVal { get; set; }
    }
}
