﻿/*
Page Created Date:  05/18/2017
Created By: Rajesh Pendela
Purpose: To tansfer the data from business layer to other layer
Version: 1.0
****************************************************
*/

namespace FVGen3.DataLayer.DTO
{
    public class ProficiencyData
    {
        public long Id { get; set; }
        public string Category { get; set; }
        public string SubCategory { get; set; }
        public string ProficiencyName { get; set; }
    }
}
