﻿/*
Page Created Date:  07/17/2018
Created By: Rajesh Pendela
Purpose: To tansfer the data from business layer to other layer
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace FVGen3.DataLayer.DTO
{
  public  class VendorEmployeeTraining
    {
      
            public string Vendor
            {
                get;
                set;
            }
            public string VendorStatus
            {
                get;
                set;
            }

            public string EmpName
            {
                get;
                set;
            }
            public string QuizName
            {
                get;
                set;
            }
            public string QuizResult
            {
                get;
                set;
            }
            public System.DateTime? QuizExpireDate
            {
                get;
                set;
            }
        }
    }

