/*
Page Created Date:  04/12/2017
Created By: Rajesh Pendela
Purpose: To tansfer the data from business layer to other layer
Version: 1.0
****************************************************
*/

using System;
namespace FVGen3.DataLayer.DTO
{
    public class OnSiteOffSiteCheckin
    {
        public string VendorName { get; set; }
        public string VisitorName { get; set; }        
        public DateTime? Checkin { get; set; }        
        public string CompanyName { get; set; }
        public Int32? TagNumber { get; set; }
        public Guid EmployeeOrVisitorId { get; set; }
        public Int32? VendorId { get; set; }
        public Guid OnsiteOffsiteId { get; set; }
        public Int32? BadgeNumber { get; set; }
        public int? UserType { get; set; }

    }
}
