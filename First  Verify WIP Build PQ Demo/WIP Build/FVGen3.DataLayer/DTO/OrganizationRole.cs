﻿/*
Page Created Date:  07/20/2018
Created By: Kiran Talluri
Purpose: To tansfer the data from business layer to other layer
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.DataLayer.DTO
{
    public class LocOrganizationRoles
    {
        public long OrgId { get; set; }
        public string OrgName { get; set; }
        public bool Status { get; set; }
        public bool IsDisable { get; set; }
    }
}
