﻿/*
Page Created Date:  01/08/2018
Created By: Kiran Talluri
Purpose: To tansfer the data from business layer to other layer
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FVGen3.DataLayer.DTO
{
    public class DataSourceRequest
    {
        public int PageSize { get; set; }
        public int Page { get; set; }
          
    }
}
