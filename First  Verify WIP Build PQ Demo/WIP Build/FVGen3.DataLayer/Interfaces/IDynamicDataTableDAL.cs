﻿/*
Page Created Date:  03/30/2016
Created By: Rich Bronson
Purpose: All business methods such as states, risk level, locations etc will be declared here
Version: 1.0
****************************************************
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FVGen3.DataLayer.DTO;

namespace FVGen3.DataLayer.Interfaces
{
    public interface IDynamicDataTableDAL
    {        
        List<KeyValuePair<string, string>> GetDynamicTableData(string tableName, long vendorID = -1);
        List<KeyValuePair<string, string>> GetUnitedStatesTableData(long vendorID, bool showIDs = false, bool includeCanada = false, string country="");

        List<KeyValuePair<string, string>> GetDynamicTableData(string tableName, string keyField, string valueField, string sortField, Guid? templateId, int vendorID, bool showIDs, int clientID );

        List<KeyValuePair<string, string>> GetRiskLevels(int clientID);
        List<RiskLevelInfo> GetRiskLevelWithDetails(int clientID);

        List<KeyValuePair<string, string>> GetLocations(int clientID);
        List<KeyValuePair<string, string>> GetUserEmails(int clientID,long PqId);
        
    }
}
