﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using AhaApps.Libraries.Extensions;

namespace FVGen3.DataLayer
{
    public class PrequalificationDAL
    {
        public string ConnectionString { get; set; }
        public PrequalificationDAL(string connectionString)
        {
            this.ConnectionString = connectionString;
        }

        public List<DTO.QuestionColumnData> GetQuestionColumnData(long preQualificationId, string key)
        {
            List<DTO.QuestionColumnData> result = new List<DTO.QuestionColumnData>();
            
            
            
            string sql = @"select q.GroupAsOneRecord,QuestionText,UserInput from QuestionColumnDetails qcd 
                        join questions q on q.QuestionID = qcd.QuestionId
                        join PrequalificationUserInput pqu on pqu.QuestionColumnId = qcd.QuestionColumnId and pqu.PreQualificationId = @prequalificationID
                        where q.ReportForKeyword = @key order by q.GroupAsOneRecord";

            try
            {
                //Use a using statement. It will make sure the connection is closed even in the case of an error.
                using (SqlConnection conn = new SqlConnection(this.ConnectionString))
                {
                    conn.Open();

                    var cmd = new SqlCommand(sql, conn);

                    //add params here
                    cmd.Parameters.AddWithValue("@prequalificationID", preQualificationId);
                    cmd.Parameters.AddWithValue("@key", key);

                    using (var reader = cmd.ExecuteReader())
                    {
                        
                        var questionGroup = new DTO.QuestionColumnData();
                        questionGroup.Questions = new List<DTO.QcdQuestions>();
                        string prevGroupID = "NEW";

                        DTO.QuestionColumnData val = new DTO.QuestionColumnData();
                        val.Questions = new List<DTO.QcdQuestions>();

                        while (reader.Read())
                        {
                            var groupID = reader["GroupAsOneRecord"].ToStringNullSafe();
                            var questionText = reader["QuestionText"].ToStringNullSafe();
                            var userInput = reader["UserInput"].ToStringNullSafe();

                            if (prevGroupID != groupID && prevGroupID != "NEW")
                            {
                                questionGroup.Key = prevGroupID;
                                result.Add(questionGroup);
                                questionGroup = new DTO.QuestionColumnData();
                                questionGroup.Questions = new List<DTO.QcdQuestions>();
                                
                            }
                            var question = new DTO.QcdQuestions();
                            question.PrequalificationUserInput = userInput;
                            question.QuestionText = questionText;
                            prevGroupID = groupID;
                            questionGroup.Questions.Add(question);

                        }
                        questionGroup.Key = prevGroupID;
                        result.Add(questionGroup);
                    }


                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
            return result;
        }
    }
}
