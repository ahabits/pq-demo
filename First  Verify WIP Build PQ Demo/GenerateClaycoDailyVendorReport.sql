-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
alter PROCEDURE GenerateClaycoDailyVendorReport
	--exec GenerateClaycoDailyVendorReport
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
		select o.OrganizationID,cli.LegacyID,o.Name as OrganizationName, o.Address1,o.Address2,o.City,o.State,o.zip,
	o.FederalIDNumber,ps.PrequalificationStatusName, p.PrequalificationStart, p.PrequalificationFinish,
	--MISSING CONTACT INFORMATION
	spca.SingleProjectContractAmount,apca.AggregateContractAmount,pvc.PrologVendorCode,
	mry.MostRecentPolicyYearModificationRate,oyp.OneYearPreviousModificationRate, typ.TowYearPreviousModificationRate,
	mrye.MostRecentPolicyYearModificationRateEffectiveDate,oype.OneYearPreviousModificationRateEffectiveDate,type.TowYearPreviousModificationRateEffectiveDate,
	mryy.MostRecentPolicyYearModificationYear,oypy.OneYearPreviousModificationRateYear, typy.TwoYearPreviousModificationRateYear,
	onel.Comments,cn.ContactName1,ct.ContactTitle1,ce.ContactEmail1,cp.ContactPhone1
	 from 
		Organizations o
		left outer join ClaycoLegacyIDs cli on o.OrganizationID = cli.OrganizatoinID
		left outer join Prequalification p on o.OrganizationID =  p.vendorID and p.ClientId=58
		left outer join PrequalificationStatus ps on p.PrequalificationStatusId = ps.PrequalificationStatusId
--Contact Info
--Full Name:
		left outer join (select pqi.UserInput as ContactName1, p.VendorId from 
			Prequalification p 
			join ClientTemplates ct on p.ClientTemplateID = ct.ClientTemplateID --and p.ClientID = 62
			join Templates t on t.TemplateID = ct.TemplateID
			join TemplateSections ts on ts.TemplateID = t.TemplateID
			join TemplateSubSections tss on tss.TemplateSectionID = ts.TemplateSectionID
			join questions q on q.SubSectionId = tss.SubSectionID
			join QuestionColumnDetails qcd on q.QuestionID = qcd.QuestionId
			join PrequalificationUserInput pqi on qcd.QuestionColumnId = pqi.QuestionColumnId
			where p.ClientId=58 and q.QuestionText = 'Full Name:') as cn on cn.VendorId = o.OrganizationID
--Title:
		left outer join (select pqi.UserInput as ContactTitle1, p.VendorId from 
			Prequalification p 
			join ClientTemplates ct on p.ClientTemplateID = ct.ClientTemplateID --and p.ClientID = 62
			join Templates t on t.TemplateID = ct.TemplateID
			join TemplateSections ts on ts.TemplateID = t.TemplateID
			join TemplateSubSections tss on tss.TemplateSectionID = ts.TemplateSectionID
			join questions q on q.SubSectionId = tss.SubSectionID
			join QuestionColumnDetails qcd on q.QuestionID = qcd.QuestionId
			join PrequalificationUserInput pqi on qcd.QuestionColumnId = pqi.QuestionColumnId
			where p.ClientId=58 and q.QuestionText = 'Title:') as ct on ct.VendorId = o.OrganizationID
--Email Address:
		left outer join (select pqi.UserInput as ContactEmail1, p.VendorId from 
			Prequalification p 
			join ClientTemplates ct on p.ClientTemplateID = ct.ClientTemplateID --and p.ClientID = 62
			join Templates t on t.TemplateID = ct.TemplateID
			join TemplateSections ts on ts.TemplateID = t.TemplateID
			join TemplateSubSections tss on tss.TemplateSectionID = ts.TemplateSectionID
			join questions q on q.SubSectionId = tss.SubSectionID
			join QuestionColumnDetails qcd on q.QuestionID = qcd.QuestionId
			join PrequalificationUserInput pqi on qcd.QuestionColumnId = pqi.QuestionColumnId
			where p.ClientId=58 and q.QuestionText = 'Email Address:') as ce on ce.VendorId = o.OrganizationID
--Telephone Number:
		left outer join (select pqi.UserInput as ContactPhone1, p.VendorId from 
			Prequalification p 
			join ClientTemplates ct on p.ClientTemplateID = ct.ClientTemplateID --and p.ClientID = 62
			join Templates t on t.TemplateID = ct.TemplateID
			join TemplateSections ts on ts.TemplateID = t.TemplateID
			join TemplateSubSections tss on tss.TemplateSectionID = ts.TemplateSectionID
			join questions q on q.SubSectionId = tss.SubSectionID
			join QuestionColumnDetails qcd on q.QuestionID = qcd.QuestionId
			join PrequalificationUserInput pqi on qcd.QuestionColumnId = pqi.QuestionColumnId
			where p.ClientId=58 and q.QuestionText = 'Telephone Number:') as cp on cp.VendorId = o.OrganizationID


--
		left outer join (select pqi.UserInput as SingleProjectContractAmount, p.VendorId from 
			Prequalification p 
			join ClientTemplates ct on p.ClientTemplateID = ct.ClientTemplateID --and p.ClientID = 62
			join Templates t on t.TemplateID = ct.TemplateID
			join TemplateSections ts on ts.TemplateID = t.TemplateID
			join TemplateSubSections tss on tss.TemplateSectionID = ts.TemplateSectionID
			join questions q on q.SubSectionId = tss.SubSectionID
			join QuestionColumnDetails qcd on q.QuestionID = qcd.QuestionId
			join PrequalificationUserInput pqi on qcd.QuestionColumnId = pqi.QuestionColumnId
			where p.ClientId=58 and q.QuestionText = 'Single Project Contract Amount:') as spca on spca.VendorId = o.OrganizationID
		left outer join (select pqi.UserInput as AggregateContractAmount , p.VendorId from 
			Prequalification p 
			join ClientTemplates ct on p.ClientTemplateID = ct.ClientTemplateID --and p.ClientID = 62
			join Templates t on t.TemplateID = ct.TemplateID
			join TemplateSections ts on ts.TemplateID = t.TemplateID
			join TemplateSubSections tss on tss.TemplateSectionID = ts.TemplateSectionID
			join questions q on q.SubSectionId = tss.SubSectionID
			join QuestionColumnDetails qcd on q.QuestionID = qcd.QuestionId
			join PrequalificationUserInput pqi on qcd.QuestionColumnId = pqi.QuestionColumnId
			where p.ClientId=58 and q.QuestionText = 'Aggregate Contract Amount:') as apca on apca.VendorId = o.OrganizationID
		left outer join (select pqi.UserInput as PrologVendorCode , p.VendorId from 
			Prequalification p 
			join ClientTemplates ct on p.ClientTemplateID = ct.ClientTemplateID --and p.ClientID = 62
			join Templates t on t.TemplateID = ct.TemplateID
			join TemplateSections ts on ts.TemplateID = t.TemplateID
			join TemplateSubSections tss on tss.TemplateSectionID = ts.TemplateSectionID
			join questions q on q.SubSectionId = tss.SubSectionID
			join QuestionColumnDetails qcd on q.QuestionID = qcd.QuestionId
			join PrequalificationUserInput pqi on qcd.QuestionColumnId = pqi.QuestionColumnId
			where p.ClientId=58 and q.QuestionText = 'Prolog Vendor Code') as pvc on pvc.VendorId = o.OrganizationID
		left outer join (select pqi.UserInput as MostRecentPolicyYearModificationRate , p.VendorId   from 
			Prequalification p 
			join ClientTemplates ct on p.ClientTemplateID = ct.ClientTemplateID --and p.ClientID = 62
			join Templates t on ct.TemplateID = t.TemplateID
			join TemplateSections ts on ts.TemplateID = t.TemplateID
			join TemplateSubSections tss on tss.TemplateSectionID = ts.TemplateSectionID
			join questions q on q.SubSectionId = tss.SubSectionID
			join QuestionColumnDetails qcd on q.QuestionID = qcd.QuestionId
			join QuestionsDependants qd on qd.DependantId = q.QuestionId 
			join questions q2 on q2.QuestionID = qd.QuestionId and q2.SubSectionId = tss.SubSectionID
			join PrequalificationUserInput pqi on qcd.QuestionColumnId = pqi.QuestionColumnId
			where p.ClientId=58 and q.QuestionText = 'EMR:'and q2.QuestionText = 'Most Recent Policy Year') 
				as  mry on mry.VendorId = o.OrganizationID
		left outer join (select pqi.UserInput as OneYearPreviousModificationRate  , p.VendorId   from 
			Prequalification p 
			join ClientTemplates ct on p.ClientTemplateID = ct.ClientTemplateID --and p.ClientID = 62
			join Templates t on ct.TemplateID = t.TemplateID
			join TemplateSections ts on ts.TemplateID = t.TemplateID
			join TemplateSubSections tss on tss.TemplateSectionID = ts.TemplateSectionID
			join questions q on q.SubSectionId = tss.SubSectionID
			join QuestionColumnDetails qcd on q.QuestionID = qcd.QuestionId
			join QuestionsDependants qd on qd.DependantId = q.QuestionId 
			join questions q2 on q2.QuestionID = qd.QuestionId and q2.SubSectionId = tss.SubSectionID
			join PrequalificationUserInput pqi on qcd.QuestionColumnId = pqi.QuestionColumnId
			where p.ClientId=58 and q.QuestionText = 'EMR:'and q2.QuestionText = 'One Year Previous') 
				as  oyp on oyp.VendorId = o.OrganizationID
		left outer join (select pqi.UserInput as TowYearPreviousModificationRate  , p.VendorId   from 
			Prequalification p 
			join ClientTemplates ct on p.ClientTemplateID = ct.ClientTemplateID --and p.ClientID = 62
			join Templates t on ct.TemplateID = t.TemplateID
			join TemplateSections ts on ts.TemplateID = t.TemplateID
			join TemplateSubSections tss on tss.TemplateSectionID = ts.TemplateSectionID
			join questions q on q.SubSectionId = tss.SubSectionID
			join QuestionColumnDetails qcd on q.QuestionID = qcd.QuestionId
			join QuestionsDependants qd on qd.DependantId = q.QuestionId 
			join questions q2 on q2.QuestionID = qd.QuestionId and q2.SubSectionId = tss.SubSectionID
			join PrequalificationUserInput pqi on qcd.QuestionColumnId = pqi.QuestionColumnId
			where p.ClientId=58 and q.QuestionText = 'EMR:'and q2.QuestionText = 'Two Year Previous') 
				as  typ on typ.VendorId = o.OrganizationID
--Effective Dates
		left outer join (select pqi.UserInput as MostRecentPolicyYearModificationRateEffectiveDate , p.VendorId   from 
			Prequalification p 
			join ClientTemplates ct on p.ClientTemplateID = ct.ClientTemplateID --and p.ClientID = 62
			join Templates t on ct.TemplateID = t.TemplateID
			join TemplateSections ts on ts.TemplateID = t.TemplateID
			join TemplateSubSections tss on tss.TemplateSectionID = ts.TemplateSectionID
			join questions q on q.SubSectionId = tss.SubSectionID
			join QuestionColumnDetails qcd on q.QuestionID = qcd.QuestionId
			join QuestionsDependants qd on qd.DependantId = q.QuestionId 
			join questions q2 on q2.QuestionID = qd.QuestionId and q2.SubSectionId = tss.SubSectionID
			join PrequalificationUserInput pqi on qcd.QuestionColumnId = pqi.QuestionColumnId
			where p.ClientId=58 and q.QuestionText = 'EMR effective date:'and q2.QuestionText = 'Most Recent Policy Year') 
				as  mrye on mrye.VendorId = o.OrganizationID
		left outer join (select pqi.UserInput as OneYearPreviousModificationRateEffectiveDate  , p.VendorId   from 
			Prequalification p 
			join ClientTemplates ct on p.ClientTemplateID = ct.ClientTemplateID --and p.ClientID = 62
			join Templates t on ct.TemplateID = t.TemplateID
			join TemplateSections ts on ts.TemplateID = t.TemplateID
			join TemplateSubSections tss on tss.TemplateSectionID = ts.TemplateSectionID
			join questions q on q.SubSectionId = tss.SubSectionID
			join QuestionColumnDetails qcd on q.QuestionID = qcd.QuestionId
			join QuestionsDependants qd on qd.DependantId = q.QuestionId 
			join questions q2 on q2.QuestionID = qd.QuestionId and q2.SubSectionId = tss.SubSectionID
			join PrequalificationUserInput pqi on qcd.QuestionColumnId = pqi.QuestionColumnId
			where p.ClientId=58 and q.QuestionText = 'EMR effective date:'and q2.QuestionText = 'One Year Previous') 
				as  oype on oype.VendorId = o.OrganizationID
		left outer join (select pqi.UserInput as TowYearPreviousModificationRateEffectiveDate  , p.VendorId   from 
			Prequalification p 
			join ClientTemplates ct on p.ClientTemplateID = ct.ClientTemplateID --and p.ClientID = 62
			join Templates t on ct.TemplateID = t.TemplateID
			join TemplateSections ts on ts.TemplateID = t.TemplateID
			join TemplateSubSections tss on tss.TemplateSectionID = ts.TemplateSectionID
			join questions q on q.SubSectionId = tss.SubSectionID
			join QuestionColumnDetails qcd on q.QuestionID = qcd.QuestionId
			join QuestionsDependants qd on qd.DependantId = q.QuestionId 
			join questions q2 on q2.QuestionID = qd.QuestionId and q2.SubSectionId = tss.SubSectionID
			join PrequalificationUserInput pqi on qcd.QuestionColumnId = pqi.QuestionColumnId
			where p.ClientId=58 and q.QuestionText = 'EMR effective date:'and q2.QuestionText = 'Two Year Previous') 
				as  type on type.VendorId = o.OrganizationID
--Effective Dates
		left outer join (select pqi.UserInput as MostRecentPolicyYearModificationYear , p.VendorId   from 
			Prequalification p 
			join ClientTemplates ct on p.ClientTemplateID = ct.ClientTemplateID --and p.ClientID = 62
			join Templates t on ct.TemplateID = t.TemplateID
			join TemplateSections ts on ts.TemplateID = t.TemplateID
			join TemplateSubSections tss on tss.TemplateSectionID = ts.TemplateSectionID
			join questions q on q.SubSectionId = tss.SubSectionID
			join QuestionColumnDetails qcd on q.QuestionID = qcd.QuestionId
			join QuestionsDependants qd on qd.DependantId = q.QuestionId 
			join questions q2 on q2.QuestionID = qd.QuestionId and q2.SubSectionId = tss.SubSectionID
			join PrequalificationUserInput pqi on qcd.QuestionColumnId = pqi.QuestionColumnId
			where p.ClientId=58 and q.QuestionText = 'Year:'and q2.QuestionText = 'Most Recent Policy Year') 
				as  mryy on mryy.VendorId = o.OrganizationID
		left outer join (select pqi.UserInput as OneYearPreviousModificationRateYear  , p.VendorId   from 
			Prequalification p 
			join ClientTemplates ct on p.ClientTemplateID = ct.ClientTemplateID --and p.ClientID = 62
			join Templates t on ct.TemplateID = t.TemplateID
			join TemplateSections ts on ts.TemplateID = t.TemplateID
			join TemplateSubSections tss on tss.TemplateSectionID = ts.TemplateSectionID
			join questions q on q.SubSectionId = tss.SubSectionID
			join QuestionColumnDetails qcd on q.QuestionID = qcd.QuestionId
			join QuestionsDependants qd on qd.DependantId = q.QuestionId 
			join questions q2 on q2.QuestionID = qd.QuestionId and q2.SubSectionId = tss.SubSectionID
			join PrequalificationUserInput pqi on qcd.QuestionColumnId = pqi.QuestionColumnId
			where p.ClientId=58 and q.QuestionText = 'Year:'and q2.QuestionText = 'One Year Previous') 
				as  oypy on oypy.VendorId = o.OrganizationID
		left outer join (select pqi.UserInput as TwoYearPreviousModificationRateYear  , p.VendorId   from 
			Prequalification p 
			join ClientTemplates ct on p.ClientTemplateID = ct.ClientTemplateID --and p.ClientID = 62
			join Templates t on ct.TemplateID = t.TemplateID
			join TemplateSections ts on ts.TemplateID = t.TemplateID
			join TemplateSubSections tss on tss.TemplateSectionID = ts.TemplateSectionID
			join questions q on q.SubSectionId = tss.SubSectionID
			join QuestionColumnDetails qcd on q.QuestionID = qcd.QuestionId
			join QuestionsDependants qd on qd.DependantId = q.QuestionId 
			join questions q2 on q2.QuestionID = qd.QuestionId and q2.SubSectionId = tss.SubSectionID
			join PrequalificationUserInput pqi on qcd.QuestionColumnId = pqi.QuestionColumnId
			where p.ClientId=58 and q.QuestionText = 'Year:'and q2.QuestionText = 'Two Year Previous') 
				as  typy on typy.VendorId = o.OrganizationID

--Comments
		left outer join 
		(
			Select PrequalificationID,
				STUFF((SELECT ',' + Comments FROM OrganizationsNotificationsEmailLogs WHERE (
				PrequalificationID=Result.PrequalificationID) FOR XML PATH ('')),1,1,'') AS Comments
				From OrganizationsNotificationsEmailLogs AS Result
				GROUP BY PrequalificationID		
		) onel on p.PrequalificationId = onel.PrequalificationId 
		where p.ClientId = 58
END
GO
